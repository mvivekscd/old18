package com.mvivekweb.commonutils;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class userAgentCheck {

	private static String filePath;
	private InputStream in;

	public static boolean isMobile(String userAgent) throws IOException {
		Properties prop = new Properties();
		FileReader reader = null;

		System.out.println("filePath=" + filePath);

		if (filePath.equals("true")) {
			System.out.println("true=true");
			if (userAgent.indexOf("Windows Phone") > -1 || userAgent.indexOf("iPhone") > -1
					|| userAgent.indexOf("Android") > -1 || userAgent.indexOf("webOS") > -1
					|| userAgent.indexOf("iPad") > -1 || userAgent.indexOf("SymbianOS") > -1
					|| userAgent.indexOf("IEMobile") > -1 || userAgent.indexOf("Opera Mini") > -1
					|| userAgent.indexOf("iPod") > -1 || userAgent.indexOf("BlackBerry") > -1
					|| userAgent.indexOf("Mobile") > -1) {
				return true;
			}

			return false;
		}

		return false;
	}

	public static boolean checkBrowser(String userAgent) {
		String IE9 = "MSIE 9";
		String IE8 = "MSIE 8";
		String IE7 = "MSIE 7";
		String IE6 = "MSIE 6";
		if (regex(IE8, userAgent) || regex(IE7, userAgent) || regex(IE6, userAgent) || regex(IE9, userAgent)) {
			return true;
		}

		return false;
	}

	public static boolean regex(String regex, String str) {
		Pattern p = Pattern.compile(regex, 8);
		Matcher m = p.matcher(str);
		return m.find();
	}
}
