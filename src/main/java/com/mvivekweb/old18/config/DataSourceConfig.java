package com.mvivekweb.old18.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;


@Configuration
public class DataSourceConfig {
	
	@Value("${app.dsNumber}")
	private String getDsNumber;
	
	@Bean(name="mysqlDB")
	public DataSource  getDataSource(Environment env) {
		if(!getDsNumber.toString().isEmpty()) {
			return DataSourceBuilder.create().url(env.getProperty("spring.datasource"+getDsNumber+".url"))
					.username(env.getProperty("spring.datasource"+getDsNumber+".username"))
					.password(env.getProperty("spring.datasource"+getDsNumber+".password")).build();
		} else {
			return DataSourceBuilder.create().url(env.getProperty("spring.datasource.url"))
					.username(env.getProperty("spring.datasource.username"))
					.password(env.getProperty("spring.datasource.password")).build();
		}
		
	}
	
	@Bean(name="mysqljdbcTemplate")
	@Primary
	public JdbcTemplate todosJdbcTemplate(@Qualifier("mysqlDB") DataSource dataSource) {
	    return new JdbcTemplate(dataSource);
	}

}
