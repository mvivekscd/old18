package com.mvivekweb.old18.pojo;

public class SpHome {
	private String pageno;
	private String next;
	private String previous;
	private String perpage;
	private String searchsel;
	private String searchval;

	public String getPageno() {
		return pageno;
	}

	public void setPageno(String pageno) {
		this.pageno = pageno;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public String getPerpage() {
		return perpage;
	}

	public void setPerpage(String perpage) {
		this.perpage = perpage;
	}

	public String getSearchsel() {
		return searchsel;
	}

	public void setSearchsel(String searchsel) {
		this.searchsel = searchsel;
	}

	public String getSearchval() {
		return searchval;
	}

	public void setSearchval(String searchval) {
		this.searchval = searchval;
	}


}
