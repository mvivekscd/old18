package com.mvivekweb.old18.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mvivekweb.old18.vos.StorepassVO;

public class StorepassMapper implements RowMapper<StorepassVO> {

	@Override
	public StorepassVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		StorepassVO storepassVO = new StorepassVO();
		storepassVO.setSpSeqId(rs.getString("spSeqId"));
		storepassVO.setSpSno(rs.getString("spSno"));
		storepassVO.setUsername(rs.getString("username"));
		storepassVO.setName(rs.getString("name"));
		storepassVO.setPin(rs.getString("pin"));
		storepassVO.setUpts(rs.getTimestamp("upts"));
		storepassVO.setSecondDetail(rs.getString("secondDetail"));
		storepassVO.setFirstpass(rs.getString("firstpass"));
		storepassVO.setUrl(rs.getString("url"));
		storepassVO.setUpBy(rs.getString("upBy"));
		storepassVO.setAutologin(rs.getString("autologin"));
		storepassVO.setComments(rs.getString("comments"));
		return storepassVO;
	}



}
