package com.mvivekweb.old18.vos;

import java.util.Date;

public class HStorePassVO {
	    private String spSeqId;
	 /*     */   private String spSno;
	 /*     */   private String username;
	 /*     */   private String firstpass;
	 /*     */   private String secondDetail;
	 /*     */   private String url;
	 /*     */   private Date upts;
	 /*     */   private String upBy;
	 /*     */   private String autologin;
	 /*     */   private String status;
	 /*     */   private String name;
	 /*     */   private String pin;
	 /*     */   private String comments;
	 /*     */   private String hspId;
	 /*     */   
	 /*     */   public String getSpSeqId() {
	 /*  24 */     return this.spSeqId;
	 /*     */   }
	 /*     */   
	 /*     */   public void setSpSeqId(String spSeqId) {
	 /*  28 */     this.spSeqId = spSeqId;
	 /*     */   }
	 /*     */   
	 /*     */   public String getSpSno() {
	 /*  32 */     return this.spSno;
	 /*     */   }
	 /*     */   
	 /*     */   public void setSpSno(String spSno) {
	 /*  36 */     this.spSno = spSno;
	 /*     */   }
	 /*     */   
	 /*     */   public String getUsername() {
	 /*  40 */     return this.username;
	 /*     */   }
	 /*     */   
	 /*     */   public void setUsername(String username) {
	 /*  44 */     this.username = username;
	 /*     */   }
	 /*     */   
	 /*     */   public String getFirstpass() {
	 /*  48 */     return this.firstpass;
	 /*     */   }
	 /*     */   
	 /*     */   public void setFirstpass(String firstpass) {
	 /*  52 */     this.firstpass = firstpass;
	 /*     */   }
	 /*     */   
	 /*     */   public String getSecondDetail() {
	 /*  56 */     return this.secondDetail;
	 /*     */   }
	 /*     */   
	 /*     */   public void setSecondDetail(String secondDetail) {
	 /*  60 */     this.secondDetail = secondDetail;
	 /*     */   }
	 /*     */   
	 /*     */   public String getUrl() {
	 /*  64 */     return this.url;
	 /*     */   }
	 /*     */   
	 /*     */   public void setUrl(String url) {
	 /*  68 */     this.url = url;
	 /*     */   }
	 /*     */   
	 /*     */   public Date getUpts() {
	 /*  72 */     return this.upts;
	 /*     */   }
	 /*     */   
	 /*     */   public void setUpts(Date upts) {
	 /*  76 */     this.upts = upts;
	 /*     */   }
	 /*     */   
	 /*     */   public String getUpBy() {
	 /*  80 */     return this.upBy;
	 /*     */   }
	 /*     */   
	 /*     */   public void setUpBy(String upBy) {
	 /*  84 */     this.upBy = upBy;
	 /*     */   }
	 /*     */   
	 /*     */   public String getAutologin() {
	 /*  88 */     return this.autologin;
	 /*     */   }
	 /*     */   
	 /*     */   public void setAutologin(String autologin) {
	 /*  92 */     this.autologin = autologin;
	 /*     */   }
	 /*     */   
	 /*     */   public String getStatus() {
	 /*  96 */     return this.status;
	 /*     */   }
	 /*     */   
	 /*     */   public void setStatus(String status) {
	 /* 100 */     this.status = status;
	 /*     */   }
	 /*     */   
	 /*     */   public String getName() {
	 /* 104 */     return this.name;
	 /*     */   }
	 /*     */   
	 /*     */   public void setName(String name) {
	 /* 108 */     this.name = name;
	 /*     */   }
	 /*     */   
	 /*     */   public String getPin() {
	 /* 112 */     return this.pin;
	 /*     */   }
	 /*     */   
	 /*     */   public void setPin(String pin) {
	 /* 116 */     this.pin = pin;
	 /*     */   }
	 /*     */   
	 /*     */   public String getComments() {
	 /* 120 */     return this.comments;
	 /*     */   }
	 /*     */   
	 /*     */   public void setComments(String comments) {
	 /* 124 */     this.comments = comments;
	 /*     */   }
	 /*     */   
	 /*     */   public String getHspId() {
	 /* 128 */     return this.hspId;
	 /*     */   }
	 /*     */   
	 /*     */   public void setHspId(String hspId) {
	 /* 132 */     this.hspId = hspId;
	 /*     */   }
}
