package com.mvivekweb.old18.vos;

import java.util.Date;

public class StorepassVO {
	private String spSeqId;
	/*     */   private String spSno;
	/*     */   private String username;
	/*     */   private String firstpass;
	/*     */   private String secondDetail;
	/*     */   private String url;
	/*     */   private Date upts;
	/*     */   private String upBy;
	/*     */   private String autologin;
	/*     */   private String name;
	/*     */   private String pin;
	/*     */   private String comments;
	/*     */   
	/*     */   public String getSpSeqId() {
	/*  21 */     return this.spSeqId;
	/*     */   }
	/*     */   
	/*     */   public void setSpSeqId(String spSeqId) {
	/*  25 */     this.spSeqId = spSeqId;
	/*     */   }
	/*     */   
	/*     */   public String getSpSno() {
	/*  29 */     return this.spSno;
	/*     */   }
	/*     */   
	/*     */   public void setSpSno(String spSno) {
	/*  33 */     this.spSno = spSno;
	/*     */   }
	/*     */   
	/*     */   public String getUsername() {
	/*  37 */     return this.username;
	/*     */   }
	/*     */   
	/*     */   public void setUsername(String username) {
	/*  41 */     this.username = username;
	/*     */   }
	/*     */   
	/*     */   public String getFirstpass() {
	/*  45 */     return this.firstpass;
	/*     */   }
	/*     */   
	/*     */   public void setFirstpass(String firstpass) {
	/*  49 */     this.firstpass = firstpass;
	/*     */   }
	/*     */   
	/*     */   public String getSecondDetail() {
	/*  53 */     return this.secondDetail;
	/*     */   }
	/*     */   
	/*     */   public void setSecondDetail(String secondDetail) {
	/*  57 */     this.secondDetail = secondDetail;
	/*     */   }
	/*     */   
	/*     */   public String getUrl() {
	/*  61 */     return this.url;
	/*     */   }
	/*     */   
	/*     */   public void setUrl(String url) {
	/*  65 */     this.url = url;
	/*     */   }
	/*     */   
	/*     */   public Date getUpts() {
	/*  69 */     return this.upts;
	/*     */   }
	/*     */   
	/*     */   public void setUpts(Date upts) {
	/*  73 */     this.upts = upts;
	/*     */   }
	/*     */   
	/*     */   public String getUpBy() {
	/*  77 */     return this.upBy;
	/*     */   }
	/*     */   
	/*     */   public void setUpBy(String upBy) {
	/*  81 */     this.upBy = upBy;
	/*     */   }
	/*     */   
	/*     */   public String getAutologin() {
	/*  85 */     return this.autologin;
	/*     */   }
	/*     */   
	/*     */   public void setAutologin(String autologin) {
	/*  89 */     this.autologin = autologin;
	/*     */   }
	/*     */   
	/*     */   public String getName() {
	/*  93 */     return this.name;
	/*     */   }
	/*     */   
	/*     */   public void setName(String name) {
	/*  97 */     this.name = name;
	/*     */   }
	/*     */   
	/*     */   public String getPin() {
	/* 101 */     return this.pin;
	/*     */   }
	/*     */   
	/*     */   public void setPin(String pin) {
	/* 105 */     this.pin = pin;
	/*     */   }
	/*     */   
	/*     */   public String getComments() {
	/* 109 */     return this.comments;
	/*     */   }
	/*     */   
	/*     */   public void setComments(String comments) {
	/* 113 */     this.comments = comments;
	/*     */   }

}
