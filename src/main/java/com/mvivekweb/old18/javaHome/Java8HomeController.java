package com.mvivekweb.old18.javaHome;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("Java8Home1")
public class Java8HomeController {
	
	@RequestMapping( {"/api8","/obj01", "/gencoll", "/obj02", "/obj03", "/obj04", "/obj05", "/obj06", "/obj07", "/obj1_1", "/obj12", "/obj13", "/obj14", "/obj15", "/obj16", "/obj21", "/obj22", "/obj23", "/obj24", "/obj26", "/obj31", "/obj32", "/obj33", "/obj34", "/obj35", "/obj41", "/obj42", "/obj43", "/obj44", "/obj51", "/obj52", "/obj53", "/obj54", "/obj55", "/obj61", "/obj62", "/obj63", "/obj64", "/obj65", "/obj71", "/obj72", "/obj73", "/obj74", "/obj75", "/obj76", "/otables", "/exam11", "/exam12", "/exam13", "/exam14", "/exam15", "/exam16", "/exam21", "/exam22", "/exam23", "/exam24", "/exam25", "/exam26", "/exam31", "/exam32", "/exam33", "/exam34", "/exam35", "/exam41", "/exam42", "/exam43", "/exam44", "/exam51", "/exam52", "/exam53", "/exam54", "/exam55", "/exam61", "/exam62", "/exam63", "/exam64", "/exam65", "/exam71", "/exam72", "/exam73", "/exam74", "/exam75", "/exam76"})
	public String java8Urls(Model model, HttpServletRequest request, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
			//System.err.println("Loggedin=" + Loggedin);
		} catch (Exception e) {
			return "redirect:logout";
		}
		 String userPath = request.getServletPath();
		 //System.err.println("userPath="+userPath);
		 /* 50 */     request.setAttribute("pageurl", userPath.replaceAll("/Java8Home1/", ""));
		 /* 51 */     String pageCode = userPath.replaceAll("/Java8Home1/", "");
		 //System.err.println("pageCode="+pageCode);
		 /* 52 */     request.setAttribute("pageName", Java8PageNameEnum.valueOf(pageCode).getPageName());
		 return "/home/java8/jpages";
	}
}
