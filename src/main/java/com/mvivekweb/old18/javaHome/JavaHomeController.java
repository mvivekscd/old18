package com.mvivekweb.old18.javaHome;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class JavaHomeController {
	
	@RequestMapping({"/Java6Home", "/Java8Home"})
	public String welcome(Model model, HttpServletRequest request, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
			//System.err.println("Loggedin=" + Loggedin);
		} catch (Exception e) {
			return "redirect:logout";
		}
		String userPath = request.getServletPath();
		if (userPath.equals("/Java6Home")) {
			return "/home/java6/jhome";
		}
		if (userPath.equals("/Java8Home")) {
			return "/home/java8/jhome";
		}
		return "welcome";
	}
	
	@RequestMapping( {"/obj1code", "/obj1", "/obj2", "/obj3", "/obj4", "/obj5", "/obj6", "/obj7", "/obj1_1", "/obj12", "/obj13", "/obj14", "/obj15", "/obj16", "/obj21", "/obj22", "/obj23", "/obj24", "/obj26", "/obj31", "/obj32", "/obj33", "/obj34", "/obj35", "/obj41", "/obj42", "/obj43", "/obj44", "/obj51", "/obj52", "/obj53", "/obj54", "/obj55", "/obj61", "/obj62", "/obj63", "/obj64", "/obj65", "/obj71", "/obj72", "/obj73", "/obj74", "/obj75", "/obj76", "/otables", "/exam11", "/exam12", "/exam13", "/exam14", "/exam15", "/exam16", "/exam21", "/exam22", "/exam23", "/exam24", "/exam25", "/exam26", "/exam31", "/exam32", "/exam33", "/exam34", "/exam35", "/exam41", "/exam42", "/exam43", "/exam44", "/exam51", "/exam52", "/exam53", "/exam54", "/exam55", "/exam61", "/exam62", "/exam63", "/exam64", "/exam65", "/exam71", "/exam72", "/exam73", "/exam74", "/exam75", "/exam76"})
	public String java6Urls(Model model, HttpServletRequest request, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
			//System.err.println("Loggedin=" + Loggedin);
		} catch (Exception e) {
			return "redirect:logout";
		}
		 String userPath = request.getServletPath();
		 /* 50 */     request.setAttribute("pageurl", userPath.replaceAll("/", ""));
		 /* 51 */     String pageCode = userPath.replaceAll("/", "");
		 /* 52 */     request.setAttribute("pageName", Java6PageNameEnum.valueOf(pageCode).getPageName());
		 return "/home/java6/jpages";
	}
	
	@RequestMapping({"/User","/editor"})
	public String usereditor(Model model, HttpServletRequest request, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
			//System.err.println("Loggedin=" + Loggedin);
		} catch (Exception e) {
			return "redirect:logout";
		}
		return "/home/Home/editorht";
	}
	

}
