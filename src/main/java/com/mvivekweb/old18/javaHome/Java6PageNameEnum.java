package com.mvivekweb.old18.javaHome;

public enum Java6PageNameEnum {
	obj1("Objective 1"), obj1code("Objective 1 code"), obj2("Objective 2"), obj3("Objective 3"), obj4("Objective 4"),
	obj5("Objective 5"), obj6("Objective 6"), obj7("Objective 7"), otables("TABLES"), obj1_1("Objective 1.1"),
	obj12("Objective 1.2"), obj13("Objective 1.3"), obj14("Objective 1.4"), obj15("Objective 1.5"),
	obj16("Objective 1.6"), obj21("Objective 2.1"), obj22("Objective 2.2"), obj23("Objective 2.3"),
	obj24("Objective 2.4"), obj26("Objective 2.6"), obj31("Objective 3.1"), obj32("Objective 3.2"),
	obj33("Objective 3.3"), obj34("Objective 3.4"), obj35("Objective 3.5"), obj41("Objective 4.1"),
	obj42("Objective 4.2"), obj43("Objective 4.3"), obj44("Objective 4.4"), obj51("Objective 5.1"),
	obj52("Objective 5.2"), obj53("Objective 5.3"), obj54("Objective 5.4"), obj55("Objective 5.5"),
	obj61("Objective 6.1"), obj62("Objective 6.2"), obj63("Objective 6.3"), obj64("Objective 6.4"),
	obj65("Objective 6.5"), obj71("Objective 7.1"), obj72("Objective 7.2"), obj73("Objective 7.3"),
	obj74("Objective 7.4"), obj75("Objective 7.5"), obj76("Objective 7.6"), /* 58 */ exam11("Exam 1.1"),
	/* 59 */ exam12("Exam 1.2"), /* 60 */ exam13("Exam 1.3"), /* 61 */ exam14("Exam 1.4"), /* 62 */ exam15("Exam 1.5"),
	/* 63 */ exam16("Exam 1.6"), /* 64 */ exam21("Exam 2.1"), /* 65 */ exam22("Exam 2.2"), /* 66 */ exam23("Exam 2.3"),
	/* 67 */ exam24("Exam 2.4"), /* 68 */ exam25("Exam 2.5"), /* 69 */ exam26("Exam 2.6"), /* 70 */ exam31("Exam 3.1"),
	/* 71 */ exam32("Exam 3.2"), /* 72 */ exam33("Exam 3.3"), /* 73 */ exam34("Exam 3.4"), /* 74 */ exam35("Exam 3.5"),
	/* 75 */ exam41("Exam 4.1"), /* 76 */ exam42("Exam 4.2"), /* 77 */ exam43("Exam 4.3"), /* 78 */ exam44("Exam 4.4"),

	exam51("Exam 5.1"), exam52("Exam 5.2"), exam53("Exam 5.3"), exam54("Exam 5.4"), exam55("Exam 5.5"),
	exam61("Exam 6.1"), exam62("Exam 6.2"), exam63("Exam 6.3"), exam64("Exam 6.4"), exam65("Exam 6.5"),
	exam71("Exam 7.1"), exam72("Exam 7.2"), exam73("Exam 7.3"), exam74("Exam 7.4"), exam75("Exam 7.5"),
	exam76("Exam 7.6");

	private final String pageName;

	public String getPageName() {
		return this.pageName;
	}

	Java6PageNameEnum(String pageName) {
		this.pageName = pageName;
	}
}
