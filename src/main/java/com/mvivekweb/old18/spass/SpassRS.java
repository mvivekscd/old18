package com.mvivekweb.old18.spass;

import java.util.List;

import com.mvivekweb.old18.vos.StorepassVO;

public class SpassRS {
	
	private List<StorepassVO> storepassList;
	private int totalcount;
	

	public List<StorepassVO> getStorepassList() {
		return storepassList;
	}

	public void setStorepassList(List<StorepassVO> storepassList) {
		this.storepassList = storepassList;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

}
