package com.mvivekweb.old18.spass;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.mvivekweb.old18.daos.SpassDao;
import com.mvivekweb.old18.pojo.SpHome;
import com.mvivekweb.old18.pojo.SpView;
import com.mvivekweb.old18.pojo.User;
import com.mvivekweb.old18.vos.HStorePassVO;
import com.mvivekweb.old18.vos.StorepassVO;



@Controller
public class SpassController {
	
	@Autowired
	SpassDao spassDao;

	@GetMapping("/Spass")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
			Model model, HttpServletRequest request, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		SpHome sphome = new SpHome();
		sphome.setPageno("1");
		sphome.setPerpage("10");
		model.addAttribute("sphome", sphome);
		SpassRS spassRS = spassDao.getSpass(1, sphome);
		model.addAttribute("name", name);
		model.addAttribute("counttest",spassRS.getStorepassList().size());
		model.addAttribute("getTotalcount",spassRS.getTotalcount());
		model.addAttribute("storepass", spassRS.getStorepassList());

		return "/home/spass/sphome";
	}
	
	@PostMapping("/Spass1")
	public String submitForm(@ModelAttribute("sphome") SpHome sphome, HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		

		int nextpage=0;
		if(null != sphome.getNext() && sphome.getNext().equalsIgnoreCase("true")) {
			nextpage = Integer.parseInt(sphome.getPageno()) + 1;
		}
		if(null != sphome.getPrevious() && sphome.getPrevious().equalsIgnoreCase("true")) {
			nextpage = Integer.parseInt(sphome.getPageno()) - 1;
		}
		
		if(null == sphome.getNext() && null==sphome.getPrevious()) {
			sphome.setPageno("1");
			nextpage =1;
		}
		SpassRS spassRS = spassDao.getSpass(nextpage, sphome);
		sphome.setSearchsel(sphome.getSearchsel());
		if(nextpage>0) {
		sphome.setPageno(""+nextpage+"");
		}
		model.addAttribute("sphome", sphome);
		model.addAttribute("counttest",spassRS.getStorepassList().size());
		model.addAttribute("getTotalcount",spassRS.getTotalcount());
		model.addAttribute("storepass", spassRS.getStorepassList());
		return "/home/spass/sphome";
	}
	
	@PostMapping("/Spass/View")
	public String viewForm(@ModelAttribute("spview") SpView spview, HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		request.getSession().setAttribute("idselected", spview.getImp());
		return "/home/spass/viewdetail";
	}
	
	@GetMapping("/Spass/vi1ewPage")
	public String viewPageForm(HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		String idsel= (String) request.getSession().getAttribute("idselected");
		StorepassVO spvo=spassDao.findByPrimaryKey(Long.parseLong(idsel));
		List<HStorePassVO> storepasshistory = spassDao.findHistoryforKey(Long.parseLong(idsel));
		model.addAttribute("storepassOneRow", spvo);
		model.addAttribute("storepassHistory", storepasshistory);
		return "/home/spass/viewdetail";
	}
	
	@GetMapping("/Spass/editSP")
	public String editSPPageForm(HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		String idsel= (String) request.getSession().getAttribute("idselected");
		StorepassVO spvo=spassDao.findByPrimaryKey(Long.parseLong(idsel));
		model.addAttribute("storepassOneRow", spvo);
		return "/home/spass/editSP";
	}	
	
	@GetMapping("/Spass/addNew")
	public String addNewPageForm(HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		return "/home/spass/addnewsp";
	}
	
	@PostMapping("/Spass/upDateSp")
	public String upDateSpForm(@ModelAttribute("spview") StorepassVO spview, HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		String idsel= (String) request.getSession().getAttribute("idselected");
		User user= (User) request.getSession().getAttribute("user");
		String updated = spassDao.updateSP(spview, Long.parseLong(idsel.trim()), user.getName());
		return "redirect:/Spass";
	}
	
	@PostMapping("/Spass/addNew")
	public String addNewForm(@ModelAttribute("spview") StorepassVO spview, HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		User user= (User) request.getSession().getAttribute("user");
		String inserted = spassDao.insertSP(spview, user.getName());
		return "redirect:/Spass";
	}
	
	@GetMapping("/Spass/deleteSP")
	public String deleteSPForm(HttpServletRequest request, HttpSession session, Model model) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
		} catch (Exception e) {
			return "redirect:logout";
		}
		String idsel= (String) request.getSession().getAttribute("idselected");
		String deleted = spassDao.deleteSP(Long.parseLong(idsel.trim()));
		return "redirect:/Spass";
	}
	
}
