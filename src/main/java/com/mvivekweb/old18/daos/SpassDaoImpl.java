package com.mvivekweb.old18.daos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mvivekweb.old18.mappers.HStorepassMapper;
import com.mvivekweb.old18.mappers.StorepassCountMapper;
import com.mvivekweb.old18.mappers.StorepassMapper;
import com.mvivekweb.old18.pojo.SpHome;
import com.mvivekweb.old18.spass.SpassRS;
import com.mvivekweb.old18.vos.HStorePassVO;
import com.mvivekweb.old18.vos.StorepassVO;

@Repository
public class SpassDaoImpl implements SpassDao {

	@Autowired
	JdbcTemplate mysqljdbcTemplate;

	public int validateLogin(String username, String passwd) {
		String query1 = "select count(*) from user where username=? and password=?";
		return mysqljdbcTemplate.queryForObject(query1, Integer.class, username, passwd);
	}
	
	public Long getmaxSno() {
		String query1 = "select max(spSno) from storepass";
		return mysqljdbcTemplate.queryForObject(query1, Long.class);
	}
	
	public Long getmaxspSeqId() {
		String query1 = "select max(spSeqId) from storepass";
		return mysqljdbcTemplate.queryForObject(query1, Long.class);
	}

	@Override
	public SpassRS getSpass(int pageno, SpHome sphome) {
		StringBuilder sp = getSPQuery(false, pageno, sphome);
		StringBuilder spcount = getSPQuery(true, pageno, sphome);
		Integer totalCount = mysqljdbcTemplate.queryForObject(spcount.toString(), new StorepassCountMapper());
		List<StorepassVO> storepass = mysqljdbcTemplate.query(sp.toString(), new StorepassMapper());

		SpassRS spassRS = new SpassRS();
		spassRS.setStorepassList(storepass);
		spassRS.setTotalcount(totalCount);
		return spassRS;
	}

	private StringBuilder getSPQuery(boolean getCount, int pageno, SpHome sphome) {
		int pagesel = Integer.parseInt(sphome.getPerpage());
		String searchsel = sphome.getSearchsel();
		String searchval = sphome.getSearchval();
		boolean anysearch= false;
		if(null!= searchsel && searchsel.equalsIgnoreCase("any")) {
			anysearch = true;
		}
		StringBuilder spbuild = new StringBuilder();
		if (getCount) {
			spbuild.append("select count(*) as totalCount from ( ");

		}
		spbuild.append("select * from storepass ");
		if(anysearch) {
			spbuild.append(" where username LIKE '%"+searchval+"%' or spSeqId like '%"+searchval+"%' or name LIKE '%"+searchval+"%' ");
			spbuild.append(" or url like '%"+searchval+"%' ");
		}
		if (!getCount) {
			if (pageno > 1) {
				spbuild.append(" limit " + (pageno - 1) * pagesel + ", "+pagesel);
			} else {
				spbuild.append(" limit 0, "+pagesel);
			}
		}
		if (getCount) {
			spbuild.append(" ) t");
		}

		return spbuild;
	}

	@Override
	public StorepassVO findByPrimaryKey(Long paramLong) {
		StringBuilder sp = getSPByIdQuery(paramLong);
		StorepassVO storepass = mysqljdbcTemplate.queryForObject(sp.toString(), new StorepassMapper(), paramLong);
		return storepass;
	}

	private StringBuilder getSPByIdQuery(Long paramLong) {
		StringBuilder spbuild = new StringBuilder();
		spbuild.append("select * from storepass where spSeqId=?");
		return spbuild;
	}
	
	private StringBuilder getSPinsertQuery() {
		StringBuilder spbuild = new StringBuilder();
		spbuild.append("INSERT INTO storepass (spSno,name, username, firstpass, secondDetail,url,pin,autologin,comments,upBy,upts, spSeqId ) ");
		spbuild.append("VALUES (?, ?, ? ,? ,?, ?, ?, ? ,? ,? , CURRENT_TIMESTAMP(), ?)");
		return spbuild;
	}
	
	private StringBuilder getdeleteQuery() {
		StringBuilder spbuild = new StringBuilder();
		spbuild.append("delete from storepass where spSeqId=?");
		return spbuild;
	}

	@Override
	public String updateSP(StorepassVO spvo, Long paramLong, String loggedusername) {
		String SQL = "update storepass set name = ?, spSno=? , username=?,firstpass=?, secondDetail=?, url=?, pin=?, autologin=?, comments=? ,upBy=?, upts=CURRENT_TIMESTAMP() where spSeqId = ?";
		mysqljdbcTemplate.update(SQL, spvo.getName(), spvo.getSpSno(), spvo.getUsername(), spvo.getFirstpass(),
				spvo.getSecondDetail(), spvo.getUrl(), spvo.getPin(), spvo.getAutologin(), spvo.getComments(),
				loggedusername, paramLong);
		System.out.println("Updated Record with ID = " + paramLong);
		return "success";
	}

	@Override
	public String insertSP(StorepassVO spvo, String loggedusername) {
		Long maxSno = getmaxSno();
		Long maxspSeqid = getmaxspSeqId();
		StringBuilder spinsertbuild = getSPinsertQuery();
		int insertDone = mysqljdbcTemplate.update(spinsertbuild.toString(),maxSno+1, spvo.getName(), spvo.getUsername(), spvo.getFirstpass(),
				spvo.getSecondDetail(), spvo.getUrl(), spvo.getPin(), spvo.getAutologin(), spvo.getComments(),
				loggedusername, (maxspSeqid+1));
		return "success";
	}

	@Override
	public String deleteSP(Long paramLong) {
		StringBuilder spdeletebuild = getdeleteQuery();
		mysqljdbcTemplate.update(spdeletebuild.toString(), paramLong);
		return "success";
	}

	@Override
	public List<HStorePassVO> findHistoryforKey(Long paramLong) {
		StringBuilder spbuild = new StringBuilder();
		spbuild.append("select * from hstorepass where spSeqId=?");
		List<HStorePassVO> storepasshistory = mysqljdbcTemplate.query(spbuild.toString(), new HStorepassMapper(), paramLong);
		return storepasshistory;
	}

}
