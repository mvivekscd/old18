package com.mvivekweb.old18.daos;

import java.util.List;

import com.mvivekweb.old18.pojo.SpHome;
import com.mvivekweb.old18.spass.SpassRS;
import com.mvivekweb.old18.vos.HStorePassVO;
import com.mvivekweb.old18.vos.StorepassVO;

public interface SpassDao {
	public int validateLogin(String username, String passwd);
	public SpassRS getSpass(int pageno, SpHome sphome);
	StorepassVO findByPrimaryKey(Long paramLong);
	public String updateSP(StorepassVO spvo,Long id, String username);
	public String insertSP(StorepassVO spvo,String username);
	public String deleteSP(Long paramLong);
	List<HStorePassVO> findHistoryforKey(Long paramLong);
}
