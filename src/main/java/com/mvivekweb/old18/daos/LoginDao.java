package com.mvivekweb.old18.daos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDao {
	
	@Autowired
	JdbcTemplate mysqljdbcTemplate;
	
	public int validateLogin(String username, String passwd) {
		String query1="select count(*) from user where username=? and password=?";
		return mysqljdbcTemplate.queryForObject(query1, Integer.class, username, passwd);
	}

}
