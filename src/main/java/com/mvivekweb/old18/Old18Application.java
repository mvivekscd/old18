package com.mvivekweb.old18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Old18Application {

	public static void main(String[] args) {
		SpringApplication.run(Old18Application.class, args);
	}

}
