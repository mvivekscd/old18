package com.mvivekweb.old18.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvivekweb.old18.daos.LoginDao;
import com.mvivekweb.old18.pojo.User;

@Controller
public class WelcomeController {
	@Autowired
	LoginDao logindao;

	@RequestMapping("/")
	public String welcome(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "welcome";
	}

	@GetMapping("/greeting")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
			Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}

	@RequestMapping("/userLogin")
	public String userLogin(Model model, HttpServletRequest request) {
		User user = new User();
		model.addAttribute("user", user);
		request.getSession().setAttribute("errormessage", "");
		return "/home/login/login";
	}

	@PostMapping("/registersubmit")
	public String submitForm(@ModelAttribute("user") User user, HttpServletRequest request, HttpSession session) {
		session.invalidate();
		HttpSession newSession = request.getSession(); // create session

		String password = request.getParameter("password");
		String pass1 = password.substring(0, password.length() - 4);
		String pass2 = password.substring(password.length() - 4);
		boolean passverified = validatepass2(pass2);

		if (passverified) {
			int count = logindao.validateLogin(user.getName(), pass1);
			if (count == 1) {
				newSession.setAttribute("loggedin", true);
				newSession.setAttribute("user", user);
				return "redirect:Home";
			} else {
				return "greeting";
			}
		} else {
			return "greeting";
		}

	}

	@GetMapping("/Home")
	public String home(Model model, HttpSession session) {
		try {
			boolean Loggedin = (boolean) session.getAttribute("loggedin");
			if (Loggedin) {
				return "/home/Home/home";
			} else {
				return "redirect:logout";
			}
		} catch (Exception e) {
			session.setAttribute("loggedin", false);
			session.setAttribute("user", null);
			return "redirect:logout";
		}

	}

	@GetMapping("/logout")
	public String logout(Model model, HttpSession session, HttpServletRequest request) {
		/*
		 * create new session if session is not new
		 */
		session.removeAttribute("loggedin");
		if (!session.isNew()) {
			session.invalidate();
		}
		session = request.getSession(true);
		model.addAttribute("user", null);
		User user = new User();
		model.addAttribute("user", user);
		return "/home/logout/logout";
	}

	private boolean validatepass2(String pass2) {
		/* 172 */ String finval = "";
		/* 173 */ String hour = (new SimpleDateFormat("hh")).format(Calendar.getInstance().getTime());
		/* 174 */ String day = (new SimpleDateFormat("dd")).format(Calendar.getInstance().getTime());
		/* 175 */ for (int i = 0; i < hour.length(); i++) {
			/* 176 */ char c = hour.charAt(i);
			/* 177 */ finval = finval + c;
			/* 178 */ char d = day.charAt(i);
			/* 179 */ finval = finval + d;
			/*     */ }
		/* 181 */ // LOG.info("localservice=" + finval);
		/* 182 */ if (finval.equalsIgnoreCase(pass2)) {
			/* 183 */ return true;
			/*     */ }
		/* 185 */ return false;
		/*     */ }

}
