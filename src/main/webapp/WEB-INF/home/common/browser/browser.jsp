<%@page import="com.mvivekweb.commonutils.userAgentCheck"%>
<%
    String userAgent = request.getHeader("user-agent");
    boolean isie8orless = userAgentCheck.checkBrowser(userAgent);
      
    if(isie8orless){
        %>
        
        <jsp:forward page="/WEB-INF/home/common/browser/ieless.jsp" />
        <%
    }
    %>