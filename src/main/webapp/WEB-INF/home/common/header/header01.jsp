<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="header">
    <table class="tbhead">
        <tr>
            <td class="hdnav">
                <div class="acti" style="padding:2px;width:40px;" onclick="openNav()">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div> 
            </td >
            <td class="hdtitle">${param.title}</td>
            <td class="hdnav">
                <c:if test="${param.enableLogout=='true'}">
                    <button id="logout" onclick="location.href = '<%=request.getContextPath()%>/logout';" class="flrig">LOGOUT</button>
                </c:if>
            </td>
        </tr>

    </table>
</div>
