 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	why method references
	</div>
	<div class="back">
show how to make code more compact.way to make the code shorter by reducing some of the code that
can be inferred and simply mentioning the name of the method
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	Example for method references
	</div>
	<div class="back">
<pre> package obj22methodrefernce;

public class Duck implements Comparable&lt;Duck&gt; {
	private String name;
	private int weight;

	public Duck(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public int getWeight() {
		return weight;
	}

	public String toString() {
		return name;
	}

	public int compareTo(Duck d) {
		return name.compareTo(d.name);
	}
}

package obj22methodrefernce;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DuckHelper {
	public static int compareByWeight(Duck d1, Duck d2) {
		return d1.getWeight() - d2.getWeight();
	}

	public static int compareByName(Duck d1, Duck d2) {
		return d1.getName().compareTo(d2.getName());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		<mark>Comparator&lt;Duck&gt; byWeight = (d1, d2) -&gt; DuckHelper.compareByWeight(d1, d2);</mark>
		List&lt;Duck&gt; ducks = new ArrayList&lt;&gt;();
		ducks.add(new Duck("Quack", 7));
		ducks.add(new Duck("Puddles", 10));
		Collections.sort(ducks);
		System.out.println(ducks); // [Puddles, Quack]
		<mark>//Collections.sort(ducks, byWeight);</mark>
		<mark>Collections.sort(ducks, DuckHelper::compareByWeight);</mark>
		System.out.println(ducks);
	}

}
</pre>
       

<pre class='out'>[Puddles, Quack]
[Quack, Puddles]</pre>

The <mark>::</mark> operator tells Java to pass the parameters automatically


	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
      what are the 4 formats of method references
	</div>
	<div class="back">
1) Static methods
2) Instance methods on a particular instance
3) Instance methods on an instance to be determined at runtime
4) Constructors
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
example of static method
	</div>
	<div class="back">
	<pre>
import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;

public class methodrefexample {
	LocalDate birthday;

    public<mark> static</mark> int compareByAge(vivek a) {
		return 10;
         
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		vivek byWeight = <mark>methodrefexample::compareByAge</mark>;
		vivek m = null;
		System.err.<mark>println(""+byWeight.vivekm(m));</mark>
		
	}

}

<mark>interface vivek{
	int vivekm(vivek m);
}</mark>
</pre>

<pre class='out'>10</pre>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
  what is the difference between 4 method references
	</div>
	<div class="back">
	<table border="1"><tr><th>Kind</th><th>Example</th></tr>
<tr>
<td>Reference to a static method</td>
<td><mark>ContainingClass</mark>::staticMethodName</td>
</tr>
<tr>
<td>Reference to an instance method of a particular object</td>
<td><mark>containingObject</mark>::instanceMethodName</td>
</tr>
<tr>
<td>Reference to an instance method of an arbitrary object of a particular type</td>
<td><mark>ContainingType</mark>::methodName</td>
</tr>
<tr>
<td>Reference to a constructor</td>
<td>ClassName::<mark>new</mark></td>
</tr>
</table>   

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the difference between Predicate, Consumer and Supplier
	</div>
	<div class="back">
<table border="1">
<tr>
<th>Functional Interface</th>
<th>Type</th>
</tr>
<tr>
<td>Predicate</td>
<td>takes a <mark><mark>single parameter</mark></mark> of any type and returns a <mark>boolean</mark></td>
</tr>
<tr>
<td>Consumer</td>
<td>which takes a <mark>single parameter of </mark>any type and has a <mark>void</mark> return</td>
</tr>
<tr>
<td>Supplier</td>
<td><mark>doesn't take</mark> any parameters and <mark>returns any</mark> type.</td>
</tr>
</table>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
How does java know about the overloaded methods
	</div>
	<div class="back">
<pre><mark><mark>Consumer</mark></mark>&lt;List&lt;Integer&gt;&gt; methodRef1 = <mark>Collections::sort</mark>;
15: Consumer&lt;List&lt;Integer&gt;&gt; lambda1 =<mark> l -> Collections.sort(l);</mark></pre>


With both lambdas and method references,
Java is <mark>inferring information from the context.</mark> In this case, we said that we were declaring a
<mark>Consumer , </mark>which <mark>takes only one parameter</mark>.

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
Example of 2) Instance methods on a particular instance
	</div>
	<div class="back">
<pre>package obj22methodrefernce;

import java.util.function.Predicate;

public class methodRefofInstance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "abc";
		<mark>Predicate&lt;String&gt;</mark> methodRef2 = <mark>str::startsWith</mark>;
		Predicate&lt;String&gt; lambda2 = s -&gt; str.startsWith(s);
		
		System.err.println("methodRef2="+<mark>methodRef2.test("a")</mark>);
		System.err.println("methodRef2="+methodRef2.test("b"));
		
		System.err.println("lambda2="+lambda2.test("c"));
		System.err.println("lambda2="+lambda2.test("a"));
	}

}</pre>
string is input and output is boolean

<pre class='out'>methodRef2=true
methodRef2=false
lambda2=false
lambda2=true</pre>


	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
Example of 3) Instance methods on an instance to be determined at runtime
	</div>
	<div class="back">
<pre>package obj22methodrefernce;

import java.util.function.Predicate;

public class methodRefNoInstance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Predicate&lt;String&gt; methodRef3 = <mark>String::isEmpty;</mark>
		Predicate&lt;String&gt; lambda3 = <mark>s -&gt; s.isEmpty();</mark>
		
		System.err.println("methodRef3="+<mark>methodRef3.test("")</mark>);
		System.err.println("methodRef3="+methodRef3.test("1"));
		System.err.println("methodRef3 space="+<mark>methodRef3.test(" ")</mark>);
		
		System.err.println("lambda3 space="+lambda3.test(" "));
	}

}</pre>

<pre class='out'>methodRef3=true
methodRef3=false
methodRef3 space=false
lambda3 space=false</pre>

we call
an instance method without knowing the instance in advance:
Instead, Java knows that isEmpty is an instance method that does not
take any parameters. Java uses the parameter supplied at runtime as the instance on which
the method is called.
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
Example of 4) Constructors
	</div>
	<div class="back">
<pre>package obj22methodrefernce;

import java.util.ArrayList;
import java.util.function.Supplier;

public class MethdoRefConst {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Supplier&lt;ArrayList&gt; methodRef4 = <mark>ArrayList::new;</mark>
		Supplier&lt;ArrayList&gt; lambda4 = <mark>() -&gt; new ArrayList();</mark>
		
		System.err.println("methodRef4 cons="+<mark>methodRef4.get()</mark>.toString());
		
		<mark>ArrayList via=new ArrayList&lt;&gt;();</mark>
		System.err.println("methodRef4 cons="+via);
		via.add("10");
		System.err.println("methodRef4 cons="+via.toString());
		via.add(10);
	}

}</pre>

<pre class='out'>methodRef4 cons=[]
methodRef4 cons=[]
methodRef4 cons=[10]</pre>

A constructor reference is a special type of method reference that uses new instead of a
method, and it <mark>creates a new object.</mark>
	</div>
</div>   
    
</div>
</body>
</html>    