 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            example for lambda
	</div>
	<div class="back">
            
              <pre>package obj20lambda;

//class
class Animal {

    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public String toString() {
        return species;
    }
}

<mark>//functional interface</mark>
interface CheckTrait {

    public <mark>boolean test(Animal a)</mark>;
}

public class FindMatchingAnimals {

    private static void print(Animal animal, CheckTrait trait) {
        if (<mark>trait.test(animal)</mark>) {
            System.out.println(animal);
        }
    }

    public static void main(String[] args) {
        print(new Animal("fish", false, true), <mark>a -> a.canHop()</mark>);
        print(new Animal("kangaroo", true, false), a -> a.canHop());
    }

}
</pre>
            
 <pre class='out'>run:
kangaroo
BUILD SUCCESSFUL (total time: 0 seconds)</pre>   
            
<pre>Since we are <mark>passing a lambda</mark> instead,
Java tr<mark>eats CheckTrait as a functional interface</mark> and tries to <mark>map it to the single
abstract method</mark>:
boolean test(Animal a);
Since this interface’s m<mark>ethod takes an Animal </mark>, it means the <mark>lambda parameter has to be
an Animal</mark> . And since that interface’s method returns a boolean , we know that the lambda
returns a boolean .</pre>              
            
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is deferred execution
	</div>
	<div class="back">
            Deferred execution means that code is specified now but runs later.Even though the execution
is deferred, the compiler will still validate that the code syntax is properly
formed.
	</div>
</div>
  

 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            how to check the flow of lambda expression
	</div>
	<div class="back">
        <pre>package obj20lambda;

//class
class Animal {

    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        System.err.println("Animal const");
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() {
        System.err.println("Animal canHop");
        return canHop;
    }

    public boolean canSwim() {
        System.err.println("Animal canswim");
        return canSwim;
    }

    public <mark>String toString() {</mark>
        return species;
    }
}

//functional interface
interface CheckTrait {

    public <mark>boolean test(Animal a);</mark>
}

public class FindMatchingAnimals {

    private static void print(Animal animal,<mark> CheckTrait trait</mark>) {
       
            System.err.println(animal);
      
    }

    public static void main(String[] args) {
        print(new Animal("fish", false, true),<mark> a -> a.toString()</mark>);
        print(new Animal("kangaroo", true, false), a -> a.canHop());
    }

}
</pre>    
<pre class='out'>
C:\vivek\java7\java 8\ocpjp8upgrade\src>javac obj20lambda\FindMatchingAnimals.java
obj20lambda\FindMatchingAnimals.java:47: error: incompatible types: bad return type in lambda expression
        print(new Animal("fish", false, true), a -> a.toString());
                                                              ^
    String cannot be converted to boolean
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error

C:\vivek\java7\java 8\ocpjp8upgrade\src>FindMatchingAnimals
</pre>            
	</div>
</div>
   
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is lambda syntax
	</div>
	<div class="back">
<pre> left side of the arrow operator -> indicates
the <mark>input parameters</mark> for the lambda expression. It <mark>can be consumed</mark> by a <mark>functional
interface</mark> whose <mark>abstract method has the same number of parameters</mark> and <mark>compatible
data types. </mark>      </pre>
<pre>
                                           The right side is referred to as the body of the lambda expression. It can be
                                            consumed by a functional interface whose <mark>abstract method returns a compatible data
                                             type.</mark>


</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  what is required in  lambda syntax
	</div>
	<div class="back">
<pre>a -> a.canHop()

equivalent to

(Animal a) -> { return a.canHop(); }

<b>has three parts:
</b>

<mark><b>required</b></mark>

a) <mark>parameter name</mark>   a
b) <mark>arrow</mark>            -> 
c) <mark>body</mark>             a.canHop()


d)expressions that have <mark>zero or more than one</mark> input parameter will still <mark>require parentheses</mark>
<mark><b>optional</b></mark>
a) when you add braces {}, you must explicitly terminate each statement in the body with a semicolon;
b) When using {} in the body of the
lambda expression, you must use the <mark>return statement if the functional interface method</mark>
that lambda implements <mark>returns a value.</mark>
c) return statement is optional when
the return type of the method is void
d) When one parameter has a data type listed, though, all parameters must provide
a data type.

e) Java doesn't allow us to re-declare a local variable that is passed but allows if different variable name other than input

</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid lambda expressions assuming that there are valid
functional interfaces
	</div>
	<div class="back">
<pre><mark><mark>(</mark>)</mark> -> new <mark>Duck</mark>()
d -> <mark>{return</mark> d.quack()<mark>;}</mark>
(Duck d) -> d.quack()
(Animal a, Duck d) -> d.quack()</pre>

<pre>() -> <mark>true</mark> // 0 parameters
a -> {return a.startsWith("test");} // 1 parameter
(String a) -> a.startsWith("test") // 1 parameter
(int x) -> <mark>{}</mark> // 1 parameter
(int y) -> <mark>{return;}</mark> // 1 parameter</pre>

<pre>(a, b) -> a.startsWith("test") // 2 parameters
(String a, String b) -> a.startsWith("test") // 2 parameters</pre>

<pre>(a, b) -> a.startsWith("test")
c -> { return 10; }
a -> { return a.startsWith("test"); }</pre>

<pre>(y, z) -> {int x=1; return y+10; }
(String s, int z) -> { return s.length()+z; }
(a, b, c) -> a.getName()</pre>

<pre>(a, b) -> { <mark>int c = 0</mark>; return 5;}</pre>
	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
  invalid Lambdas
	</div>
	<div class="back">
<pre>Duck d -> d.quack() // DOES NOT COMPILE <mark>require parentheses</mark> ()!
a,d -> d.quack() // DOES NOT COMPILE  require parentheses ()
Animal a, Duck d -> d.quack() // DOES NOT COMPILE require parentheses ()</pre>

<pre>a, b -> a.startsWith("test") // DOES NOT COMPILE no parenthesis
c -> return 10; // DOES NOT COMPILE no curly bracket
a -> { return a.startsWith("test") } // DOES NOT COMPILE no semicolon</pre> 

<pre>(<mark>int y</mark>, z) -> {int x=1; return y+10; } // DOES NOT COMPILE
(<mark>String s</mark>, z) -> { return s.length()+z; } // DOES NOT COMPILE
(a, <mark>Animal b</mark>, c) -> a.getName() // DOES NOT COMPILE</pre>

<pre>(a, b) -> { int a = 0; return 5;}</pre>


	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
       what is the output
	</div>
	<div class="back">
<pre>//class
class Animal {

    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public String toString() {
        return species;
    }
  
  public int viv(int a) {
        System.out.println(a);
   <mark> return 0;</mark>
    }
}

//functional interface
interface CheckTrait {

    public <mark>int test(int a)</mark>;
}

public class FindMatchingAnimals {

    private static void print(Animal animal, CheckTrait trait) {
       // if (trait.test(animal)) {
            System.out.println(animal);
        //}
       System.out.println(<mark>trait.test(2)</mark>);
      int v= trait.test(3);
       System.out.println(v);
    }

    public static void main(String[] args) {
      print(new Animal("fish", false, true), <mark>a -> a</mark>);
       // print(new Animal("kangaroo", true, false), a -> a.canHop());
    }

}</pre>

<pre class='out'>fish
2
3</pre>

why return 0 is not working
	</div>
</div>
   
    
</div>
</body>
</html>