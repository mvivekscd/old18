 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >
    
 
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is Collection.removeIf(Predicate)
	</div>
	<div class="back">
            <pre>boolean <mark>removeIf</mark>(<mark>Predicate</mark>&lt;? super E&gt; filter)</pre>
            
            we can specify
what should be deleted using a block of code.It uses a Predicate, which is a lambda that takes one parameter and returns a boolean.
Since <mark>lambdas use deferred execution</mark>, <mark>this allows specifying logic to run when that point in
the code is reached.</mark>
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Example for Collection.removeIf(Predicate)
	</div>
	<div class="back">
            <pre>package obj27ImprovementsCollection;

import java.util.ArrayList;
import java.util.List;

public class Removeif {

    public static void main(String[] args) {
        List&lt;String&gt; list = new ArrayList&lt;&gt;();
        list.add("Magician");
        list.add("Assistant");
        System.out.println(list); // [Magician, Assistant]
        //<mark>list.removeIf(s -&gt; s.startsWith("A"));</mark>
        <mark>list.removeIf(s -&gt; "Assistant".startsWith(s));</mark>
        System.out.println(list); // [Magician]
    }

}</pre>
            
<pre class='out'>run:
[Magician, Assistant]
[Magician]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

The most important thing to remember about removeIf is
that <mark>it is one of two methods that are on a collection </mark>and it <mark>takes a lambda parameter</mark>.            
	</div>
</div>  

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is List.replaceAll(UnaryOperator&lt;E&gt; o))
	</div>
	<div class="back">
            lets you pass a lambda
expression and <mark>have it applied to each element </mark>in the list.It uses a <mark>UnaryOperator,</mark> which <mark>takes one parameter and returns a value of the same
type.
</mark>
	</div>
</div>
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Example for List.replaceAll(UnaryOperator&lt;E&gt; o))
	</div>
	<div class="back">
            <pre>package obj27ImprovementsCollection;

import java.util.Arrays;
import java.util.List;

public class ReplaceAll {

    public static void main(String[] args) {

        List&lt;Integer&gt; list = Arrays.asList(1, 2, 3);
        list.<mark>replaceAll(x -&gt; x * 2);
</mark>        System.out.println(list);
    }

}</pre>
            
<pre class='out'>run:
[2, 4, 6]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            How to Loop through a collection using Lambda
	</div>
	<div class="back">
            <pre>
package obj27ImprovementsCollection;

import java.util.Arrays;
import java.util.List;


public class LoopLambda {


    public static void main(String[] args) {
        List&lt;String&gt; cats = Arrays.asList("Annie", "Ripley");
       <mark> cats.forEach(c -&gt; System.out.println(c));</mark>
        <mark>cats.forEach(System.out::println);</mark>
    }

}</pre>
            
<pre class='out'>run:
Annie
Ripley
Annie
Ripley
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is List.forEach(Consumer))
	</div>
	<div class="back">
            we've used a Consumer, which takes a single parameter and doesn't return
anything.
	</div>
</div> 
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what id the difference between put and putIfAbsent in a Map
	</div>
	<div class="back">
            <pre>package obj27ImprovementsCollection;

import java.util.HashMap;
import java.util.Map;

public class Mapputdiff {

    public static void main(String[] args) {
        Map&lt;String, String&gt; favorites = new HashMap&lt;&gt;();
        <mark>favorites.put("Jenny", "Bus Tour");
        favorites.put("Jenny", "Tram");
</mark>        System.out.println(favorites);

        Map&lt;String, String&gt; favorites1 = new HashMap&lt;&gt;();
        favorites1.put("Jenny", "Bus Tour");
        <mark>favorites1.put("Tom", null);</mark>
        favorites1.putIfAbsent("Jenny", "Tram");
        favorites1.putIfAbsent("Sam", "Tram");
        <mark>favorites1.putIfAbsent("Tom", "Tram");</mark>
        System.out.println(favorites1); // {Tom=Tram, Jenny=Bus Tour, Sam=Tram}
    }

}</pre>

            <pre class='out'>run:
{Jenny=Tram}
{Tom=Tram, Jenny=Bus Tour, Sam=Tram}
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is computeIfPresent(key) method
	</div>
	<div class="back">
            compute if key is present. 
<pre>package obj27ImprovementsCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Computeifkeypresnt {

    public static void main(String[] args) {
        Map&lt;String, Integer&gt; <mark>counts</mark> = new HashMap&lt;&gt;();
        counts.put(<mark>"Jenny", </mark>1);
        BiFunction&lt;String, Integer, Integer&gt; mapper = (k, v) -&gt; v + 1;
        Integer jenny = <mark>counts.computeIfPresent</mark>("Jenny", mapper);
        Integer sam = counts.computeIfPresent("Sam", mapper);
        System.out.println(counts); // {Jenny=2}
        System.out.println(jenny); // 2
        System.out.println(sam); // null
    }

}</pre>

<pre class='out'>run:
{Jenny=2}
2
null
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
s time the key and value are
passed rather than two values. Just like with merge(), the return value is the result of what
changed in the map or null if that doesn't apply.            
	</div>
</div>

    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is computeIfAbsent(key) method
	</div>
	<div class="back">
            compute only if key is absent or null. 
<pre>package obj27ImprovementsCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class computeifAbsentorNull {

    public static void main(String[] args) {
        Map&lt;String, Integer&gt; counts = new HashMap&lt;&gt;();
        counts.put("Jenny", 15);
        counts.put("Tom", null);
        Function&lt;String, Integer&gt; mapper = (k) -&gt; 1;
        Integer jenny = counts.computeIfAbsent("Jenny", mapper); // 15
        Integer sam = <mark>counts.computeIfAbsent("Sam", </mark>mapper); // 1
        Integer tom = <mark>counts.computeIfAbsent("Tom",</mark> mapper); // 1
        System.out.println(counts); // {Tom=1, Jenny=15, Sam=1}
    }

}</pre>

<pre class='out'>run:
{Tom=1, Jenny=15, Sam=1}
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            if mapping function is null. how computeIfPresent and computeIfAbsent behaves
	</div>
	<div class="back">
            <pre>package obj27ImprovementsCollection;

import java.util.HashMap;
import java.util.Map;

public class ComputeIfNull {

    public static void main(String[] args) {
        Map&lt;String, Integer&gt; counts = new HashMap&lt;&gt;();
        counts.put("Jenny", 1);
        counts.computeIfPresent("Jenny", (k, v) <mark>-&gt; null</mark>);
        counts.computeIfAbsent("Sam", <mark>k -&gt; null</mark>);
        System.out.println(counts); // {}
    }

}</pre>
            
<pre class='out'>run:
{}
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            if null value is present
	</div>
	<div class="back">
 <pre>package obj27ImprovementsCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Computeifkeypresnt {

    public static void main(String[] args) {
        Map&lt;String, Integer&gt; counts = new HashMap&lt;&gt;();
        counts.put("Jenny", <mark>null);</mark>
        BiFunction&lt;String, Integer, Integer&gt; mapper = (k, v) -&gt; v + 1;
        <mark>Integer jenny = counts.computeIfPresent("Jenny", mapper);
</mark>        Integer sam = counts.computeIfPresent("Sam", mapper);
        System.out.println(counts); // {Jenny=2}
        System.out.println(jenny); // 2
        System.out.println(sam); // null
    }

}</pre>   
            
<pre class='out'>run:
{Jenny=null}
null
null
BUILD SUCCESSFUL (total time: 0 seconds)</pre>            
	</div>
</div>    
    
</div>
</body>
</html>      