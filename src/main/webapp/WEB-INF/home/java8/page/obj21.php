 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>
<button id="next" onclick="disableflash()" type="button">Disable flash</button>
<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is the package name 
	</div>
	<div class="back">
     java.util.function
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is predicate interface
	</div>
	<div class="back">
<pre>public interface Predicate<T> {
     public <mark>boolean</mark> test(<mark>T t</mark>);
}</pre>

similar to functional interface
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
      Example for Predicate
	</div>
	<div class="back">
<pre>import java.util.function.Predicate;

//class
class Animal {

    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public String toString() {
        return species;
    }
  
  public int viv(int a) {
        System.out.println(a);
    return 0;
    }
}


public class FindMatchingAnimals {

private static void print(Animal animal, <mark>Predicate&lt;Animal&gt; trait</mark>) {
if(trait.test(animal))
System.out.println(animal);
}
public static void main(String[] args) {
print(new Animal("fish", false, true), a -> a.canHop());
print(new Animal("kangaroo", true, false), a -> a.canHop());
}


}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
  what variables lambdha expressions can acess
	</div>
	<div class="back">
<pre>Lambda expressions can access 
a) <mark>static variables, </mark>
b) <mark>instance variables, </mark>
c) <mark>effectively final
method parameters, and </mark>
d)<mark>effectively final local variables.</mark> </pre>
            
<pre>/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj21builtininterface;

/**
 *
 * @author manju
 */
interface Gorilla {

    String move();
}

public class GorillaFamily {

    String <mark>walk = "walk";</mark>

    void everyonePlay(<mark>boolean baby</mark>) {
        String <mark>approach = "amble";</mark>
//approach = "run";

        play(() -> walk);// instance variable
        play(() -> baby ? "hitch a ride" : "run");// method paramter
        play(() -> approach);//<mark>effectively final local variable</mark>
    }

    void play(Gorilla g) {
        System.out.println(<mark>g.move());</mark>
    }
}</pre>
a lambda can’t access private variables in another class            
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            compare the built in functional interfaces
	</div>
	<div class="back">
            <table border="1">
<tr>
<th>Functional interafaces</th>
<th>#parameters</th>
<th><mark>R</mark>eturn type</th>
<th>method</th>
</tr>
<tr>
<td>Supplier<T></td>
<td>0</td>
<td>T</td>
<td>get</td>
</tr>
<tr>
<td>Consumer<T></td>
<td>1</td>
<td></td>
<td>accept</td>
</tr>
<tr>
<td>Predicate<T></td>
<td>1</td>
<td>boolean</td>
<td>test</td>
</tr>
<tr>
<td>Function<T, R></td>
<td>1</td>
<td>R</td>
<td>apply</td>
</tr>
<tr>
<td>UnaryOperator<T></td>
<td>1</td>
<td>T</td>
<td>apply</td>
</tr>
</table>

<p>If a distinct
return type is needed,<mark> R for return is used for the </mark>generic type.</p>
SCPFU
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            supplier example
	</div>
	<div class="back">
            
            <pre>package obj21builtininterface;

import java.time.LocalDate;
import java.util.function.Supplier;

public class SupplierExample {

    public static void main(String[] args) {
        <mark>Supplier&lt;LocalDate</mark>&gt; s1 = LocalDate::now;
        Supplier&lt;LocalDate&gt; s2 = () -&gt; LocalDate.now();
        LocalDate d1 = s1.get();
        LocalDate d2 = s2.get();
        System.out.println(d1);
        System.out.println(d2);
    }

}</pre>
            
<pre class='out'>run:
2018-02-17
2018-02-17
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>            
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
           what is the supplier interface syntax. when it is used
	</div>
	<div class="back">
            <pre>@FunctionalInterface public class Supplier&lt;<mark>T</mark>&gt; {
public<mark> T get</mark>();
}</pre>
            A Supplier is often used when constructing
new objects.we use a constructor reference to create the object.A Supplier is used when you want to <mark>generate or supply values without taking any input.</mark>
	</div>
</div>    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the consumer interface syntax. when it is used
	</div>
	<div class="back">
            
            <pre>@FunctionalInterface public class Consumer&lt;T&gt; {
<mark>void</mark> accept(T t);
}
@FunctionalInterface public class BiConsumer&lt;T, U&gt; {
<mark>void accept</mark>(T t, U u);
}</pre>
<p>use a Consumer when you want to do <mark>something with a parameter but not return anything.
</mark>BiConsumer does the same thing except that it takes two parameters.</p>            
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            example for Consumer
	</div>
	<div class="back">
            <pre>package obj21builtininterface;

import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {
        <mark>Consumer&lt;String&gt</mark>; c1 = System.out::println;
        Consumer&lt;String&gt; c2 = x -&gt; System.out.println(x);
        c1.accept("Annie");
        c2.accept("Annie");
    }

}</pre>
<pre class='out'>run:
Annie
Annie
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
            
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Example for Biconsumer
	</div>
	<div class="back">
       <pre>package obj21builtininterface;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class BiConsumerExample {

    public static void main(String[] args) {
        Map&lt;String, Integer&gt; <mark>map</mark> = new HashMap&lt;&gt;();
        <mark>BiConsumer&lt;String, Integer&gt; </mark>b1 = map::put;
        BiConsumer&lt;String, Integer&gt; b2 = (k, v) -&gt; map.put(k, v);
        b1.accept("chicken", 7);
       <mark> b2.accept("chick", 1);
</mark>        System.out.println(map);
    }

}</pre>   
            
<pre class='out'>run:
{chicken=7, chick=1}
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

            
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Bi Predicate example
	</div>
	<div class="back">
            <pre>package obj21builtininterface;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateExample {

    public static void main(String[] args) {
        Predicate&lt;String&gt; p1 = <mark>String::isEmpty;</mark>
        Predicate&lt;String&gt; p2 = x -&gt; x.isEmpty();
        System.out.println(p1.test(""));
        System.out.println(p2.test(""));

        BiPredicate&lt;String, String&gt; b1 = <mark>String::startsWith;</mark>
        BiPredicate&lt;String, String&gt; b2 =<mark> (string, prefix) -&gt; string.startsWith(prefix);</mark>
        System.out.println(b1.test("chicken", "chick"));
        System.out.println(b2.test("chicken", "chick"));
    }

}</pre>
            
<pre class='out'>run:
true
true
true
true
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            How to reuse predicate and reduce the code size.
	</div>
	<div class="back">
            <pre>package obj21builtininterface;

import java.util.function.Predicate;

public class ReusePredicateExample {

    public static void main(String[] args) {
        Predicate&lt;String&gt; egg = s -&gt; s.contains("egg");
        Predicate&lt;String&gt; brown = s -&gt; s.contains("brown");

        Predicate&lt;String&gt; brownEggs = <mark>egg.and(brown);</mark>
        Predicate&lt;String&gt; otherEggs = <mark>egg.and(brown.negate());</mark>
        
        System.err.println(brownEggs.test("brown egg"));
        System.err.println(otherEggs.test("other egg"));
    }

}</pre>
            
<pre class='out'>run:
true
true
BUILD SUCCESSFUL (total time: 0 seconds)</pre>            
	</div>
</div> 
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is a function and bifuncation interface
	</div>
	<div class="back">
            Function is responsible for turning one parameter into a value of a potentially different
type and returning it.

<pre>@FunctionalInterface public class Function&lt<mark>;T</mark>, R&gt; {
<mark>R apply</mark>(T t);
}

@FunctionalInterface public class BiFunction&lt;T, U, R&gt; {
R apply(T t, U u);
}</pre>
	</div>
</div> 
    
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Example for function and bifunction
	</div>
	<div class="back">
            <pre>package obj21builtininterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionandBifunction {

    public static void main(String[] args) {
        <mark>Function&lt;String, Integer&gt;</mark> f1 = String::length;
        Function&lt;String, Integer&gt; f2 = x -&gt; x.length();
        System.out.println(f1.apply("cluck")); // 5
        System.out.println(f2.apply("cluck")); // 5

       <mark> BiFunction&lt;String, String, String&gt;</mark> b1 = String::concat;
        BiFunction&lt;String, String, String&gt; b2 = (string, toAdd) -&gt; string.concat(toAdd);
        System.out.println(b1.apply("baby ", "chick")); // baby chick
        System.out.println(b2.apply("baby ", "chick")); // baby chick
    }

}</pre>
            
<pre class='out'>run:
5
5
baby chick
baby chick
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

            
	</div>
</div>   
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            How to create your own functional interface
	</div>
	<div class="back">
<pre>package obj21builtininterface;

public class VivekInterface {

    public static void main(String[] args) {
       <mark> VivekFunction&lt;String, String, String, String&gt;</mark> vif= (string, toAdd, check) -&gt; string.concat(toAdd);
        System.err.println(vif.apply("obne", "two", "three"));
    }

}

interface VivekFunction&lt;T, U, V, <mark><mark>R</mark>&gt;</mark> {

    <mark>R apply</mark>(T t, U u, V v);
}
</pre> 

<pre class='out'>run:
obnetwo
BUILD SUCCESSFUL (total time: 0 seconds)

R is return type
</pre>

            
            
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">

                <div class="front">
what are built-in interface for functions
                </div>

                <div class="back">
Java provides a built-in interface for functions <mark>with one or two parameters</mark>.Java’s built-in interfaces are meant to facilitate the most common functional interfaces
that you’ll need. It is by no means an exhaustive list. Remember that you can add any
functional interfaces you’d like, and Java matches them when<mark> you use lambdas or
method references.</mark>

                </div>

</div>

<div class="flipper" onclick="toggleflip(this)">

                <div class="front">
what is UnaryOperator and BinaryOperator
                </div>

                <div class="back">
<p>They require all type
parameters to be the same type.</p>

<p>A <mark>UnaryOperator</mark> transforms its value into<mark> one of the
same type</mark>. For example, <mark>incrementing by one </mark>is a unary operation. In fact,<mark> UnaryOperator
    extends Function</mark>. </p>
<p>A <mark>BinaryOperator merges two values into one of the same type</mark>. Adding
two numbers is a binary operation. Similarly, <mark>BinaryOperator extends BiFunction</mark>.
</p>

<pre>
    @FunctionalInterface public class UnaryOperator<T>
extends Function<T, T> { }
@FunctionalInterface public class BinaryOperator<T>
extends BiFunction<T, T, T> { }
This means that method signatures look like this:
T apply(T t);
T apply(T t1, T t2);
</pre>
                </div>

</div>

<div class="flipper" onclick="toggleflip(this)">

                <div class="front">
Example of UnaryOperator and BinaryOperator
                </div>

                <div class="back">
<pre>package obj21builtininterface;

import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class UnaryOperatorExample {

    public static void main(String[] args) {
        <mark>UnaryOperator&lt;String&gt;</mark> u1 = String::toUpperCase;
        UnaryOperator&lt;String&gt; u2 = x -&gt; x.toUpperCase();
        System.out.println(<mark>u1.apply</mark>("chirp"));
        System.out.println(u2.apply("chirp"));

        <mark>BinaryOperator&lt;String&gt;</mark> b1 = String::concat;
        BinaryOperator&lt;String&gt; b2 = (string, toAdd) -&gt; string.concat(toAdd);

        System.out.println(<mark>b1.apply</mark>("baby ", "chick")); // baby chick
        System.out.println(b2.apply("baby ", "chick")); // baby chick
    }

}</pre>
                    
<pre class='out'>run:
CHIRP
CHIRP
baby chick
baby chick
BUILD SUCCESSFUL (total time: 0 seconds)</pre>                    
                </div>

</div>
    
    
</div>
</body>
</html>
