
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Objective 1 Diamond operator</title>
<!-- <script src="js/card.js"></script>
<link rel="stylesheet" type="text/css" href="css/flash.css"> -->
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='../Java8Home'" type="button">Back</button>
<button id="next" onclick="disableflash()" type="button">Disable flash</button>

<div class="flip-container" >
   
<div class="flipper" onclick="toggleflip(this)">
		<div class="front">
		what is literals
			</div>
			<div class="back">
When a <mark>number is present in the code, </mark>it is a type of literal. By default, Java assumes that
you are defining an int value with a literal.
			</div>
		
</div>

<div class="flipper" onclick="toggleflip(this)">
		<div class="front">
		what are valid literals
			</div>
			<div class="back">

<pre>long max = 3123456789<mark>L</mark>;
		long max1 = 3123456789<mark>l</mark>;
		
		int octal= <mark>017;</mark>
		int hexadecimal= <mark>0xfe;</mark>
		int binary= <mark>0b11;</mark>
		
		System.out.println(octal+" "+ hexadecimal +" "+ binary);</pre>

		Format
<pre> <mark>0 as a prefix</mark> + Octal (Digits 0-7) .
Hexadecimal  <mark>0 followed by x or X as a prefix</mark>+ (<mark>Digits 0-9</mark> and <mark>Letters A-F</mark>) .
Binary  <mark>0 followed by b or B as a prefix</mark>+(Digits 0-1).</pre>
			
			You won't need to convert between number systems on the exam
			</div>


</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
      what is Underscore Characters
	</div>
	<div class="back">
	<pre>
	int million2 = 1_000_00_0;
		System.out.println("million2="+million2);
</pre>

<pre class='out'>million2=1000000</pre>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	invalid Underscore Characters
	</div>
	<div class="back">
	You
cannot add underscores at the <mark>beginning of a literal</mark>, the <mark>end of a literal</mark>, right
<mark>before a decimal point</mark>, or <mark>right after a decimal point</mark>.

<pre>double notAtStart = <mark>_</mark>1000.00; 
		double notAtEnd = 1000.00<mark>_;</mark> 
		double notByDecimal = 100<mark>0_</mark>.00;
		double notafterDecimal = 1000<mark>._</mark>00;</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	valid Underscore Characters
	</div>
	<div class="back">
<pre>		double notAtStart = 1_0_0_0.0_0; 
		System.out.println("notAtStart="+notAtStart);</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  what is the structure for switch
	</div>
	<div class="back">
<pre>switch(variableToTest){
case <mark>constantExpression1</mark>:
  break;
}
</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  what is the required and optional in a switch switch
	</div>
	<div class="back">
<pre><b>Required</b>

<mark>Parentheses
curly brace</mark>

<b>Optional</b>
0 or more case branches
optional break
Optional default and anywhere within switch</pre>

	</div>
</div>

    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            datetypes for switch
	</div>
	<div class="back">
          <pre><mark>  cbyesis</mark>
char, byte, enum, short, int, String</pre>
            
            
	</div>
</div>   
    
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the output
            <pre>package obj01literalSwitch;

public class switch1 {

    public static void main(String[] args) {
        int dayOfWeek = 5;
        <mark>switch (dayOfWeek)</mark> {
            default:
                System.out.println("Weekday");
                break;
            case <mark>0_0</mark>:
                System.out.println("Sunday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
        }
    }

}</pre>
	</div>
	<div class="back">
       Weekday     
	</div>
</div>   
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what are the condition for case statement
	</div>
	<div class="back">
            <pre><mark>literal</mark>, 
<mark>enum c</mark>onstant, 
or <mark>final</mark> constant variable <mark>except <b>method argument</b></mark></pre>
	</div>
</div>
    
    <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            why following does not compile
            <pre>package obj01literalSwitch;

public class Switch2 {

    public static void main(String[] args) {

    }

    private int getSortOrder(String firstName, <mark>final String lastName</mark>) {
        String middleName = "Patricia";
        final String suffix = "JR";
        int id = 0;
        switch (firstName) {
            case "Test":
                return 52;
            case middleName: // DOES NOT COMPILE
                id = 5;
                break;
            case suffix:
                id = 0;
                break;
            case lastName: // DOES NOT COMPILE
                id = 8;
                break;
            case 5: // DOES NOT COMPILE
                id = 7;
                break;
            case 'J': // DOES NOT COMPILE
                id = 10;
                break;
            case Month.JANUARY: // DOES NOT COMPILE
                id = 15;
                break;
        }
        return id;
    }
}
</pre>
            
	</div>
	<div class="back">
            switch needs string and final variable
	</div>
</div>
    
</div>
</body>
</html>