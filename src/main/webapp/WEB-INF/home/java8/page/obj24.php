 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>obj24</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	How convert arraylist to array
	</div>
	<div class="back">
<pre>package objcollection;

import java.util.ArrayList;
import java.util.List;

public class ListToArray {


	public static void main(String[] args) {

		List&lt;String&gt; list = new ArrayList&lt;&gt;(); // empty list
		list.add("Fluffy"); // [Fluffy]
		list.add("Webby"); // [Fluffy, Webby]
		String[] array = new String[list.size()]; // empty array
		array[0] = list.get(1); // [Webby]
		array[1] = list.get(0); // [Webby, Fluffy]
		for (int i = 0; i &lt; array.length; i++)
		System.out.print(array[i] + "-");
	}

}</pre>

<pre class='out'>Webby-Fluffy-</pre>


	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	How convert array to arrayList
	</div>
	<div class="back">
<pre>package obj24;

import java.util.Arrays;
import java.util.List;

public class ArrayToArrayList {

	public static void main(String[] args) {

		String[] array = { "gerbil", "mouse" }; // [gerbil, mouse]
		for(String val:array){
			System.err.println("val=" + val);
		}
		List&lt;String&gt; list = Arrays.asList(array); // returns fixed size list
		System.err.println("list=" + list);
		<mark>list.set(1, "test"); // [gerbil, test]</mark>
		System.err.println("list=" + list);
		array[0] = "new"; // [new, test]
		for(String val:array){
			System.err.println("val=" + val);
		}
		String[] array2 = (String[]) list.toArray(); // [new, test]
		for(String val:array2){
			System.err.println("val2=" + val);
		}
		<mark>list.remove(1);</mark>
		System.err.println("list=" + list);
		for(String val:array){
			System.err.println("val=" + val);
		}
		for(String val:array2){
			System.err.println("val2=" + val);
		}
	}

}
</pre>
runtime exception
<pre class='out'>val=gerbil
val=mouse
list=[gerbil, mouse]
list=[gerbil, test]
val=new
val=test
val2=new
val2=test
Exception in thread "main" java.lang.UnsupportedOperationException
	at java.util.AbstractList.remove(AbstractList.java:172)
	at obj24.ArrayToArrayList.main(ArrayToArrayList.java:26)
</pre>

list is not resizable because it is backed by the underlying
array.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
how to sort arrays and search for index
	</div>
	<div class="back">
<pre>package obj24;

import <mark>java.util.Arrays</mark>;

public class SortAndSearch {


	public static void main(String[] args) {
		int[] numbers = {6,9,1,8};
		 <mark>Arrays.sort(numbers); </mark>// [1,6,8,9]
		 for(int val:numbers){
				System.err.println("val=" + val);
			}
		 System.out.println(<mark>Arrays.binarySearch(numbers, 6)</mark>); // 1
		 System.out.println(<mark>Arrays.binarySearch(numbers, 3))</mark>; // -2

	}

}</pre>

<pre class='out'>val=1
val=6
val=8
val=9
1
-2
</pre>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
how to sort list and search for index
	</div>
	<div class="back">
<pre>package obj24;

import java.util.Arrays;
import java.util.<mark>Collections</mark>;
import java.util.List;

public class SortAndSearch1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List&lt;Integer&gt; list = Arrays.asList(9,7,5,3);
		 Collections.sort(list); // [3, 5, 7, 9]
		 System.err.println(list);
		 System.out.println(<mark>Collections.binarySearch(list, 3)</mark>); // 0
		 System.out.println(Collections.binarySearch(list, 2)); // -1

	}

}
</pre>

<pre class='out'>[3, 5, 7, 9]
0
-1
</pre>

We call sort() and binarySearch() on Collections rather than
Collection . In the past, Collection could not have concrete methods
because it is an interface. Some were added in Java 8.
	</div>
</div>

    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            what is the order of sorting
	</div>
	<div class="back">
            numbers
            letters
            and
            uppercase letters sort before
            lowercase letters
            
	</div>
</div>    
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            How to sort objects
	</div>
	<div class="back">
             <p>Use an <mark>interface called </mark><mark>Comparable</mark></p>
           <p> a <mark>class called Comparator </mark>, which is used to specify that you want
to use a different order than the object itself provides.</p>
	</div>
</div>   


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is comparable interface
	</div>
	<div class="back">
<pre>public <mark>int</mark>erface Comparable&lt;T&gt; {
public<mark> int compareTo</mark>(T o);
}</pre>
	</div>
</div>    
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            How to use comparable interface
	</div>
	<div class="back">
            <pre>package obj24sortfilteriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Duck implements <mark>Comparable&lt;Duck&gt; </mark>{

    private String name;

    public Duck(String name) {
        System.err.println("Duck Const");
        this.name = name;
    }

    public static void main(String[] args) {
        System.err.println("main start");
        List&lt;Duck&gt; ducks = new ArrayList&lt;&gt;();
        ducks.add(new Duck("Quack"));
        ducks.add(new Duck("Puddles"));
        Collections.sort(ducks); // sort by name
        System.err.println("main after sort");
        System.err.println(ducks); // [Puddles, Quack]
        System.err.println("main ends");
    }

    public int compareTo(Duck o) {
        System.err.println("---compareTo---method ");
        <mark>return name.compareTo(o.name);
</mark>    }

    public String toString() { // use readable output
        return name;
    }

}
</pre>
            
   <pre class='out'>run:
main start
Duck Const
Duck Const
---compareTo---method 
main after sort
[Puddles, Quack]
main ends
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
            
  the String class already has a compareTo() method, it can just delegate          
	</div>
</div>    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
           what are the rules for compareTo() method
	</div>
	<div class="back">
            <p>The <mark>number zero</mark> is returned when the <mark>current object is equal to the argument </mark>to compareTo().</p>
<p>A <mark>number less than zero </mark>is returned when the <mark>current object is smaller </mark>than the argument
to compareTo().</p>
<p>A <mark>number greater than zero </mark>is returned when the <mark>current object is larger </mark>than the argument
to compareTo().</p>

<pre>package obj24sortfilteriter;

public class Animal implements Comparable&lt;Animal&gt; {

    private int id;

    public static void main(String[] args) {
        Animal a1 = new Animal();
        Animal a2 = new Animal();

        a1.id = 5;
        a2.id = 7;
        System.out.println(a1.compareTo(a2)); // -2
        System.out.println(a1.compareTo(a1)); // 0
        System.out.println(a2.compareTo(a1)); // 2
    }

    @Override
    public int compareTo(Animal a) {
        return id-a.id;
    }
}</pre>

<pre class='out'>run:
-2
0
2
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
Remember that id – a.id sorts in ascending order and a.id – id sorts in
descending order.
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the legacy comparator
	</div>
	<div class="back">
            <pre>package obj24sortfilteriter;

public class LegacyDuck implements java.lang.Comparable {

    private String name;

    public static void main(String[] args) {

    }

    public int compareTo(<mark>Object o</mark>) {
        LegacyDuck d =<mark> (LegacyDuck) o; // cast because no generics
</mark>        return name.compareTo(d.name);
    }

}</pre>
            Since we don't specify a generic type for Comparable , Java assumes that we want an
Object , which means that we have to cast to LegacyDuck before accessing instance variables
on it.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is compareTo() and equals() Consistency
	</div>
	<div class="back">
            <p>The <mark>compareTo() method returns 0</mark> if two objects are equal, while
your <mark>equals() method returns true</mark> if two objects are equal. A natural ordering that
<mark>uses compareTo() is said to be consistent with equals </mark>if, and only if, <mark>x.equals(y) is true</mark>
whenever <mark>x.compareTo(y) equals 0. </mark>You are strongly encouraged to <mark>make your Comparable
classes consistent with equals </mark>because not all collection classes behave predictably if
the compareTo() and equals() methods are not consistent.</p>
            
For example, the following Product class defines a compareTo() method that is not consistent
with equals:
<pre>public class Product implements Comparable&lt;Product&gt; {
int id;
String name;
<mark>public boolean equals(Object obj) {
if(!(obj instanceof Product)) {
return false;
}
Product other = (Product) obj;
return this.id == other.id;
}
</mark>
<mark>public int compareTo(Product obj) {
return this.name.compareTo(obj.name);
} }</</mark>pre>            
	</div>
</div>
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            when to use a comparator
	</div>
	<div class="back">
            you want to sort an object that did not implement Comparable, or you want to
sort objects in different ways at different times.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            how to sort with multiple variables
	</div>
	<div class="back">
<pre>package obj24sortfilteriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DuckW implements Comparable&lt;DuckW&gt; {

    private String name;

    <mark>private int weight;</mark>

    public DuckW(String name, int weight) {
         System.err.println("DuckW Const");
        this.name = name;
        <mark>this.weight = weight;</mark>
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String toString() {
        return name;
    }

    public int compareTo(DuckW d) {
        return name.compareTo(d.name);
    }

    public static void main(String[] args) {
        <mark>Comparator&lt;DuckW&gt; byWeight = new Comparator&lt;DuckW&gt;() {
            public int compare(DuckW d1, DuckW d2) {
                return d1.getWeight() - d2.getWeight();
            }
        };
</mark>        List&lt;DuckW&gt; ducks = new ArrayList&lt;&gt;();
        ducks.add(new DuckW("Quack", 7));
        ducks.add(new DuckW("Puddles", 10));
        Collections.sort(ducks);
        System.err.println(ducks); // [Puddles, Quack]
        <mark>Collections.sort(ducks, byWeight);</mark>
        System.err.println(ducks); // [Quack, Puddles]
    }

}
        </pre>

            need to add comparator for new variable comparison .defined an <mark>inner class with the comparator.</mark>



<pre class='out'>run:
DuckW Const
DuckW Const
[Puddles, Quack]
[Quack, Puddles]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
	</div>
</div>    
 
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            is comparator a functional interface
	</div>
	<div class="back">
            yes
	</div>
</div>
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            how to use lambda in a comparator
	</div>
	<div class="back">
            <pre>package obj24sortfilteriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DuckW implements Comparable&lt;DuckW&gt; {

    private String name;

    private int weight;

    public DuckW(String name, int weight) {
         System.err.println("DuckW Const");
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String toString() {
        return name;
    }

    public int compareTo(DuckW d) {
        return name.compareTo(d.name);
    }

    public static void main(String[] args) {
//        Comparator&lt;DuckW&gt; byWeight = new Comparator&lt;DuckW&gt;() {
//            public int compare(DuckW d1, DuckW d2) {
//                return d1.getWeight() - d2.getWeight();
//            }
//        };
        
        Comparator&lt;DuckW&gt; byWeight = <mark>(d1, d2) -&gt; d1.getWeight()-d2.getWeight();</mark>
Comparator&lt;DuckW&gt; byWeight1 = <mark>(DuckW d1, DuckW d2) -&gt; d1.getWeight()-d2.getWeight();
</mark>Comparator&lt;DuckW&gt; byWeight2 = <mark>(d1, d2) -&gt; { return d1.getWeight()-d2.getWeight(); };
</mark>Comparator&lt;DuckW&gt; byWeight3 = <mark>(DuckW d1, DuckW d2) -&gt; {return d1.getWeight()-d2.getWeight(); };
</mark>
        List&lt;DuckW&gt; ducks = new ArrayList&lt;&gt;();
        ducks.add(new DuckW("Quack", 7));
        ducks.add(new DuckW("Puddles", 10));
        Collections.sort(ducks);
        System.err.println(ducks); // [Puddles, Quack]
        Collections.sort(ducks, byWeight);
        System.err.println(ducks); // [Quack, Puddles]
    }

}
</pre>
 <pre class='out'>run:
DuckW Const
DuckW Const
[Puddles, Quack]
[Quack, Puddles]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>               
    Remember that the type is optional            
	</div>
</div>   

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            can we use lambda in comparable
	</div>
	<div class="back">
            
Comparable is also a functional interface since it also has a <mark>single abstract method.</mark> However,
using a <mark>lambda for Comparable would be silly</mark>. The point of <mark>Comparable is to implement
it inside the object being compared.</mark>
	</div>
</div>
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            difference between comparable and comparator with lambda
	</div>
	<div class="back">
            <table border="1"><tr>
<th>Difference</th><th>Comparable</th><th>Com<mark>parat</mark>or</th>
<tr>
<td>Package name</td>
<td><mark>java.lang</mark></td>
<td><mark>java.util</mark></td>
</tr>
<td><mark>Interface must be implemented by class</mark> comparing?</td>
<td><mark>Yes</mark></td>
<td>No</td>
</tr>
<td>Method name in interface</td>
<td>compare<mark>T</mark>o</td>
<td>compare</td>
</tr>
<td>Number of parameters</td>
<td>1</td>
<td><mark>2</mark></td>
</tr>
<td>Common to declare using a lambda</td>
<td>No</td>
<td>Yes</td>
</tr>
</table>
	</div>
</div> 
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the problem in the code
            
            <pre>/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj24sortfilteriter;

import java.util.Comparator;

/**
 *
 * @author manju
 */
public class Squirrel {

    private int weight;
    private String species;

    public Squirrel(String theSpecies) {
        if (theSpecies == null) {
            throw new IllegalArgumentException();
        }
        species = theSpecies;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getSpecies() {
        return species;
    }
}

class MultiFieldComparator implements Comparator&lt;Squirrel&gt; {

    public int compare(Squirrel s1, Squirrel s2) {
        int result = s1.getSpecies().compareTo(s2.getSpecies());
       <mark> if (result != 0) {</mark>
            return result;
        }
        return s1.getWeight() - s2.getWeight();
    }
}</pre>
	</div>
	<div class="back">
            This <mark>isn’t that easy to read</mark>, though. It is also <mark>easy to get
wrong. Changing != to == </mark>breaks the sort completely.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            How java 8 lambda solves the comparator problem
	</div>
	<div class="back">
            <pre>/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj24sortfilteriter;

import java.util.Comparator;

/**
 *
 * @author manju
 */
public class Squirrel {

    private int weight;
    private String species;

    public Squirrel(String theSpecies) {
        if (theSpecies == null) {
            throw new IllegalArgumentException();
        }
        species = theSpecies;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getSpecies() {
        return species;
    }
}

class MultiFieldComparator implements Comparator&lt;Squirrel&gt; {

//    public int compare(Squirrel s1, Squirrel s2) {
//        int result = s1.getSpecies().compareTo(s2.getSpecies());
//        if (result != 0) {
//            return result;
//        }
//        return s1.getWeight() - s2.getWeight();
//    }

    public int compare(Squirrel s1, Squirrel s2) {
        Comparator&lt;Squirrel&gt; c = Comparator<mark>.comparing(</mark>s -&gt; s.getSpecies());
        c = c.<mark>thenComparingInt</mark>(s -&gt; s.getWeight());
        <mark>return c.compare(s1, s2);
</mark>    }
}
</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is a easy way to use Comparator
	</div>
	<div class="back">
            <pre>package obj24sortfilteriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortRabbits {

    static class Rabbit {

        int id;
    }

    public static void main(String[] args) {
        List&lt;Rabbit&gt; rabbits = new ArrayList&lt;&gt;();
        rabbits.add(new Rabbit());
        rabbits.add(new Rabbit());
        <mark>Comparator&lt;Rabbit&gt; c = (r1, r2) -&gt; r1.id - r2.id;</mark>
        Collections.sort(rabbits, c);
        System.err.println("rabbits"+rabbits);
    }

}</pre>
<pre class='out'>run:
rabbits[obj24sortfilteriter.SortRabbits$Rabbit@1218025c, obj24sortfilteriter.SortRabbits$Rabbit@816f27d]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

Java knows that the Rabbit class is not Comparable.            
            
	</div>
</div>    
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the output
            <pre>package obj24sortfilteriter;

import java.util.Set;
import java.util.TreeSet;

public class UseTreeSet {

    static class Rabbit {

        int id;
    }

    public static void main(String[] args) {
        Set&lt;Duck&gt; ducks = new TreeSet&lt;&gt;();
        ducks.add(new Duck("Puddles"));
        Set&lt;Rabbit&gt; rabbit = new TreeSet&lt;&gt;();
        rabbit.add(new Rabbit()); // throws an exception
    }

}

package obj24sortfilteriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Duck implements Comparable&lt;Duck&gt; {

    private String name;

    public Duck(String name) {
        System.err.println("Duck Const");
        this.name = name;
    }

    public static void main(String[] args) {
        System.err.println("main start");
        List&lt;Duck&gt; ducks = new ArrayList&lt;&gt;();
        ducks.add(new Duck("Quack"));
        ducks.add(new Duck("Puddles"));
        Collections.sort(ducks); // sort by name
        System.err.println("main after sort");
        System.err.println(ducks); // [Puddles, Quack]
        System.err.println("main ends");
    }

    public int compareTo(Duck o) {
        System.err.println("---compareTo---method ");
        return name.compareTo(o.name);
    }

    public String toString() { // use readable output
        return name;
    }

}

</pre>
	</div>
	<div class="back">
     <pre class='out'>run:
Duck Const
---compareTo---method 
Exception in thread "main" java.lang.ClassCastException: obj24sortfilteriter.UseTreeSet$Rabbit cannot be cast to java.lang.Comparable
	at java.util.TreeMap.compare(TreeMap.java:1290)
	at java.util.TreeMap.put(TreeMap.java:538)
	at java.util.TreeSet.add(TreeSet.java:255)
	at obj24sortfilteriter.UseTreeSet.main(UseTreeSet.java:17)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 1 second)
</pre>       
            
  fact that Rabbit does not implement Comparable. Java throws an exception that looks like
this:


	</div>
</div>  
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the solution for runtime error
	</div>
	<div class="back">
            <pre>package obj24sortfilteriter;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class UseTreeSet {

    static class Rabbit {

        int id;
    }

    public static void main(String[] args) {
//        Set&lt;Duck&gt; ducks = new TreeSet&lt;&gt;();
//        ducks.add(new Duck("Puddles"));
       <mark> Set&lt;Rabbit&gt; rabbit = new TreeSet&lt;&gt;(new Comparator&lt;Rabbit&gt;() {
            public int compare(Rabbit r1, Rabbit r2) {
                return r1.id = r2.id;
            }
        });</mark>
        rabbit.add(new Rabbit()); // throws an exception
        rabbit.add(new Rabbit());
        System.err.println("rabbit="+rabbit);
    }

}</pre>
            
 <pre class='out'>run:
rabbit=[obj24sortfilteriter.UseTreeSet$Rabbit@15db9742]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
    <mark>you can tell collections that require sorting that you wish
to use a specific Comparator</mark>, for example:        
  . Now Java knows that you want to sort by id and all is well. Comparators are helpful
<mark>object</mark>s. They <mark>let you separate sort order </mark>from the <mark>object to be sorted</mark>.         
	</div>
</div>    
    
</div>
</body>
</html>
