 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>@Override</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	data and time in which package
	</div>
	<div class="back">
            <mark>java.time</mark> package
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what are the four types
	</div>
	<div class="back">
<pre>LocalDate - LD
LocalTime  - LT
LocalDateTime- LDT
ZonedDateTime-  ZDT</pre>
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Local date time example
	</div>
	<div class="back">
            <pre>package obj11LocalDateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;

public class Ex001 {

    public static void main(String[] args) {
        System.out.println("LocalDate.now()="+LocalDate.now());
        System.out.println("LocalTime.now()="+LocalTime.now());
        System.out.println("LocalDateTime.now()="+<mark>LocalDateTime.now());</mark>
        System.out.println("ZonedDateTime.now<mark>()="+ZonedDateTime.now());</mark>

    }

}</pre>
<pre class='out'>run:
LocalDate.now()=<mark>2018-01-14</mark>
Local<mark>T</mark>ime.now()=00:02:10<mark>.582</mark>
LocalDateTime.now()=2018-01-<mark>14T00</mark>:02:10.583
ZonedDateTime.now()=2018-01-14T00:02:10.586<mark>-06:00[America/Chicago]
</mark>BUILD SUCCESSFUL (total time: 1 second)
the fourth adds the time zone offset and time zone.
</pre>            
            
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is time zone recommendation
	</div>
	<div class="back">
            Oracle recommends avoiding time zones unless you really need them. Try to act as if
everyone is in the same time zone when you can.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            how to convert to GMT
	</div>
	<div class="back">
            <pre> 2015-06-20T07:50+02:00[Europe/Paris]
7:50- 2:00= 5:50
            
2015-06-20T07:50 GMT-04:00 // GMT 2015-06-20 11:50
7:50+4:00= 11:50            
            </pre>
            
	</div>
</div>

    

</div>
</body>
</html>
