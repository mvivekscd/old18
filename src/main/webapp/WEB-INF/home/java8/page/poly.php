 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">
<button id="back" onClick="location.href='polymorphism.pptx'" type="button">polymorphism presentation</button>
Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='index.html'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	rules for polymorphism
	</div>
	<div class="back">
<pre>

1)The <mark>type of the object</mark> determines <mark>which properties exist within the object</mark> in memory.

2) The <mark>type of the reference</mark> to the object determines <mark>which methods and variables are
accessible</mark> to the Java program.</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	output of code
	</div>
	<div class="back">
<pre>package polymorphism;

class Primate {
	public boolean hasHair() {
		return true;
	}
}

interface HasTail {
	public boolean isTailStriped();
}

public class Lemur extends Primate implements HasTail {

	public int age = 10;

	public boolean isTailStriped() {
		return false;
	}

	public static void main(String[] args) {

		Lemur lemur = new Lemur();
		System.out.println(lemur.age);
		HasTail hasTail = lemur;
		System.out.println(hasTail.isTailStriped());
		Primate primate = lemur;
		System.out.println(primate.hasHair());
	}

}
</pre>

<pre class='out'>10
false
true</pre>

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
output of implict cast code
	</div>
	<div class="back">
<pre>package polymorphism;

public class poly1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         C c= new C();
         System.err.println("c.v1="+c.v1 );
         System.err.println("c.v1="+c.v2 );
         System.err.println("c.v1="+c.v3 );
         
         B b= c;
         
         System.err.println("b.v1="+b.v1 );
         System.err.println("b.v1="+b.v2 );
       //  System.err.println("b.v1="+<mark>b.v3 </mark>); 
         
         System.err.println("c.v1="+c.v1 );
         System.err.println("c.v1="+c.v2 );
         System.err.println("c.v1="+c.v3 );
         
         A a= c;
         
         System.err.println("a.v1="+a.v1 );
       //  System.err.println("a.v1="<mark>+a.v2</mark> );
       //  System.err.println("a.v1="<mark>+a.v3</mark> ); 
         
	}

}

class A {
	int v1=1;
	public void m1() {

	}
}

class B extends A{
	int v2=2;
	public void m2() {

	}
}

class C extends B{
	int v3 =3;
	public void m3() {

	}
}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
  what about instance of super class
	</div>
	<div class="back">
<pre>package polymorphism;

public class poly1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         A a= new A();
         System.err.println("c.v1="+a.v1 );
        // System.err.println("c.v1="+a.v2 );
     //   System.err.println("c.v1="+a.v3 );
         
         <mark>B b= (B)a;</mark> // compiles but throws error in runtime ClassCastException.java:58
         
          
        <mark> B b= a;</mark> // compiler error
       
         
         C c= (C)a;
         
       
         
       
         
         
	}

}

class A {
	int v1=1;
	public void m1() {

	}
}

class B extends A{
	int v2=2;
	public void m2() {

	}
}

class C extends B{
	int v3 =3;
	public void m3() {

	}
}</pre>
	</div>
</div>


</div>
</body>
</html>


