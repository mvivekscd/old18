 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is merge method
	</div>
	<div class="back">
            method allows <mark>adding logic</mark> to the problem of what to choose
	</div>
</div>
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is BiFunction
	</div>
	<div class="back">
            case, it <mark>takes two parameters
</mark>of the same type and returns <mark>a value of that type</mark>.
	</div>
</div> 
    
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Example of merge with Bifunction
	</div>
	<div class="back">
            <pre>import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class MergeBifunction {

    public static void main(String[] args) {
        BiFunction<mark>&lt;String, String, String&gt;</mark> mapper = (v1, v2)
                -&gt; v1.length() &gt; v2.length() ? v1 : v2;

        Map&lt;String, String&gt; favorites = new HashMap&lt;&gt;();
        favorites.<mark>put("Jenny",</mark> "Bus Tour");
        favorites.put("Tom", "Tram");

        String jenny = favorites<mark>.merge("</mark>Jenny", "Skyride<mark>", mapper</mark>);
        String tom = favorites.merge("Tom", "Skyride", mapper);

        System.out.println(favorites); // {Tom=Skyride, Jenny=Bus Tour}
        System.out.println(jenny); // Bus Tour
        System.out.println(tom);
    }

}</pre>
            
<pre class='out'>run:
{Tom=Skyride, Jenny=Bus Tour}
Bus Tour
Skyride
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
implementation returns the one
with the longest name.            
	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what happens to merge with nulls and missing keys
	</div>
	<div class="back">
            <pre>package obj28stream;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Mergenullkeys {

    public static void main(String[] args) {
        BiFunction&lt;String, String, String&gt;<mark> mapper</mark> = (v1, v2) -&gt; v1.length()
                &gt; v2.length() ? v1 : v2;
        Map&lt;String, String&gt; favorites = new HashMap&lt;&gt;();
        <mark>favorites.put("Sam", null);</mark>
        System.out.println(favorites);
        favorites<mark>.merge("Tom", </mark>"Skyride", <mark>mapper);
</mark>        System.out.println(favorites);
        favorites.merge("Sam", "Skyride", mapper);
        System.out.println(favorites); // {Tom=Skyride, Sam=Skyride}
    }

}</pre>
            
<pre class='out'>run:
{Sam=null}
{Tom=Skyride, Sam=null}
{Tom=Skyride, Sam=Skyride}
BUILD SUCCESSFUL (total time: 0 seconds)</pre>      
            
 In this case, it doesn't call the BiFunction at all, and it simply uses the new
value.if it were we’d have a
NullPointerException. The mapping function is used only when there are two actual values
to decide between.          
	</div>
</div> 
    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what if merge returns null. will the key be removed.
	</div>
	<div class="back">
            <pre>package obj28stream;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class MergereturnNull {

    public static void main(String[] args) {
        BiFunction&lt;String, String, String&gt; mapper = (v1, v2)<mark> -&gt; null;</mark>
        Map&lt;String, String&gt; favorites = new HashMap&lt;&gt;();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", "Bus Tour");
<mark>        favorites.merge("Jenny", "Skyride", mapper);
         favorites.merge("Tom", "Skyride", mapper);</mark>
        favorites.merge<mark>("Sam", </mark>"Skyride", mapper);
        System.out.println(favorites); // {Tom=Bus Tour, Sam=Skyride}
    }

}</pre>
            
<pre class='out'>run:
{Sam=Skyride}
BUILD SUCCESSFUL (total time: 0 seconds)</pre>      
            
            
	</div>
</div>    
    
 </div>
</body>
</html>     