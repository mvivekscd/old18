 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title></title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='index.html'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is NIO2
	</div>
	<div class="back">
Nonblocking Input/Output API. The basic idea is that you <mark>load the data from a file channel into</mark>
a temporary buffer that, unlike byte streams, can be <mark>read forward and backward</mark> w<mark>ithout</mark>
<mark>blocking on the underlying resource. actually a replacement for the java.io.File class. provides a number of
notable performance improvements over the existing java.io.File class.</mark>.
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	api for Path
	</div>
	<div class="back">
java.nio.file.Path interface

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is path object
	</div>
	<div class="back">
<p>A <mark>Path object</mark> represents a <mark><b>hierarchical path on the
storage system</b></mark> to a file or directory. In this manner, Path is a <mark>direct replacement</mark> for the
legacy java.io.File class, and conceptually it contains many of the same properties. For
example, both File and Path objects may <mark>refer to a file or a directory</mark>. Both also may <mark>refer
to an absolute path or relative path</mark> within the file system.</p>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    what is symbolic link
	</div>
	<div class="back">
A symbolic
link is a special file within an operating system that serves as a <mark>reference or pointer</mark> to another
file or directory.
NIO.2 API includes full support
for <mark>creating, detecting, and navigating symbolic links</mark> within the file system.

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    NIO2 uses which design pattern
	</div>
	<div class="back">
a factory class can be implemented using static
methods to create instances of another class. For example, you can create an instance of a
Path interface using a static method available in the Paths factory class.

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    NIO2 uses which design pattern and its advantage.
	</div>
	<div class="back">
a factory class can be implemented using static
methods to create instances of another class. For example, you can create an instance of a
Path interface using a static method available in the Paths factory class.The advantage of using the factory pattern here is that you can write the same code
that will run on a variety of different platforms.(e.g Linux/Windows)

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    what are the helper classes in nio2
	</div>
	<div class="back">
<p>java.nio.file<mark>.Files</mark>, whose primary purpose
is to operate on instances of Path objects. Helper or utility classes are similar to factory classes
in that they are <mark>often composed primarily of static methods </mark>that operate on a particular
class. They differ in that helper classes are focused on <mark>manipulating or creating new objects
from existing instances</mark>, whereas factory classes are focused primarily on object creation.</p>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    interfaces, factory and helper classes
	</div>
	<div class="back">

interface= java.nio.file.Path,
factory=ava.nio.files.Paths,
helper=java.nio.file.Files

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	    
	</div>
	<div class="back">


	</div>
</div>


</div>
</body>
</html>
