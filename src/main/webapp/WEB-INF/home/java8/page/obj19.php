 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Functional Interface</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is functional interface
	</div>
	<div class="back">
	interface that contains a <mark>single abstract method</mark>.
Functional interfaces are used as the basis for lambda expressions in functional programming.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
     example of functional interface
	</div>
	<div class="back">
<pre><mark>@FunctionalInterface // optional</mark>
public interface Sprint {
<mark>public void sprint(Animal animal);</mark>
}
public class Tiger implements Sprint {
public void sprint(Animal animal) {
System.out.println("Animal is sprinting fast! "+animal.toString());
}
}</pre>

Java compiler
implicitly assumes that any interface that contains <mark>exactly one abstract method is
a functional interface</mark>. Conversely, if a <mark>class marked with the @FunctionalInterface</mark>
annotation contains <mark>more than one abstract method, or no abstract methods at all</mark>, then
the compiler will detect this error and <mark>not compile</mark>.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	other examples of functional interfaces
	</div>
	<div class="back">
<pre><mark>@FunctionalInterface</mark>
public interface <mark>Sprint</mark> {
   public void sprint(Animal animal);
}

public interface <mark>Run</mark> extends Sprint {}

public interface <mark>SprintFaster</mark> extends Sprint {
public void sprint(Animal animal);
}

public interface <mark>Skip</mark> extends Sprint {
public <mark>default</mark> int getHopCount(Kangaroo kangaroo) {return 10;}
public <mark>static v</mark>oid skip(int speed) {}
}</pre>

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	Invalid Functional Interfaces
	</div>
	<div class="back">
<pre>@FunctionalInterface
public interface Sprint {
   public void sprint(Animal animal);
}


public interface <mark>Walk</mark> {}
public interface <mark>Dance</mark> extends Sprint {
public void dance(Animal animal);
}
public interface <mark>Crawl</mark> {
public void crawl();
public int getCount();
}

</pre>
applying the @FunctionalInterface annotation to any of these
interfaces would result in a compiler error,
	</div>
</div>




</div>
</body>
</html>
