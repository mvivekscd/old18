 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>@Override</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>
<button id="next" onclick="disableflash()" type="button">Disable flash</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
      what does the new interface have
	</div>
	<div class="back">
interface may also include constant <br>
<mark>public static final </mark>variables, <br>
<mark>default</mark> methods, and<br>
<mark>static methods</mark>.
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	syntax for static and default methods 
	</div>
	<div class="back">

	<pre>package obj04interfcae;

public interface Fly {
	public int getWingSpan() throws Exception;
	public static final int MAX_SPEED = 100;
	public <mark>default</mark> void land() <mark>{
	System.out.println("Animal is landing");</mark>
	}
	public <mark>static d</mark>ouble calculateSpeed(float distance, double time) <mark>{
		return distance/time;</mark>
		}
}

public class Eagle implements Fly {
	public int getWingSpan() {
		return 15;
	}
	public void land() {
		System.out.println("Eagle is diving fast");
	}
}
</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is marker interface
	</div>
	<div class="back">

	interfaces that have neither methods nor class members

<pre>public interface Hop {
}</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	rules for interfaces
	</div>
	<div class="back">

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	rules for default method
	</div>
	<div class="back">
	Java fails to compile if a class or
interface inherits two default methods with the same signature and doesn't provide its
own implementation.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	Purpose of an Interface
	</div>
	<div class="back">
	An interface provides a way for one individual to develop code that uses another
individual's code, without having access to the other individual's underlying
implementation. Interfaces can facilitate rapid application development by enabling
development teams to create applications in parallel, rather than being directly
dependent on each other.
	</div>
</div>

</div>
</body>
</html>
