 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>@Override</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">
    

   <!-- <iframe width="100" height="500"  src="polymorphism.pptx">Your browser does not support.</iframe> -->
 
<!--    <button id="back" onClick="location.href='collectionsand generics.pptx'" type="button">collection and generic presentation</button>-->
Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	rules for Generic Interfaces
	</div>
	<div class="back">
	three ways to implement
<pre>package objGenerics;

public interface Shippable&lt;T&gt; {
	void ship(T t);
}

class ShippableRobotCrate implements <mark>Shippable&lt;Robot&gt;</mark> {
	public void ship(Robot t) {
	}
}
//to create a generic class
class <mark>ShippableAbstractCrate&lt;U&gt; implements Shippable&lt;U&gt;</mark> {
	public void ship(U t) {
	}
}
//It generates a compiler warning about Shippable being a raw type, but it does compile
class ShippableCrate implements Shippable {
	public void shi<mark>p(Object t) {</mark>
	}
}
</pre>
	</div>
</div>


<!--<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	Things you cannot do with generic type
	</div>
	<div class="back">

limitations are due to type erasure . so cannot
<pre>Call the constructor
Create an array of that static type
Call instanceof
Use a primitive type as a generic type parameter
Create a static variable as a generic type parameter</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid generic class declaration
	</div>
	<div class="back">
<pre>package objGenerics;

public class GenericClass<<mark>T</mark>> {

}

class GenericClass1<<mark>T extends Vivek</mark>>{
	
}

class Vivek{
	
}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
   valid generics variables
	</div>
	<div class="back">
<pre>package objGenerics;

public class GenericClass&lt;T&gt; {
 <mark> T var;</mark>
}

class GenericClass1&lt;T extends Vivek, U&gt;{
	<mark>T var;
	U var1;</mark>
	
	
}

class Vivek{
	
}
</pre>

<pre>class GenericClass1&lt;T extends Vivek, U&gt; {
	
	//variable
	T var;
	<mark>protected U var1;</mark>

}</pre>


	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid generics constructor
	</div>
	<div class="back">
package objGenerics;
<pre>
import java.util.List;

public class GenericClass&lt;T&gt; {
	T var;

<mark>// Constructor</mark>
	GenericClass(T t, List&lt;T&gt; t1) {
		System.err.println("Const");
	}

	public static void main(String[] args) {
		String t = null;

		List&lt;String&gt; t1 = null;
		<mark>GenericClass&lt;String&gt; gc = new GenericClass&lt;String&gt;(t, t1);</mark>
	}
}

class GenericClass1&lt;T extends Vivek, U&gt; {
	T var;
	U var1;

}

class Vivek {

}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
    valid interface and implementation
	</div>
	<div class="back">
<pre>package objGenerics;

public interface Shippable&lt;T&gt; {
	void ship(T t);
}

class ShippableRobotCrate implements <mark>Shippable&lt;Robot&gt;</mark> {
	public void ship(Robot t) {
	}
}

class ShippableAbstractCrate<mark>&lt;U&gt;</mark> implements <mark>Shippable&lt;U&gt;</mark> {
	public void ship(U t) {
	}
}
//
class ShippableCrate implements Shippable {
	public void <mark>ship(Object t) {</mark>
	}
}

interface Shippable1&lt;T extends <mark>Shippable&lt;?&gt;&gt;</mark> {
	void ship(T t);
}

interface Shippable2&lt;<mark>T extends Shippable&lt;T&gt;&gt;</mark> {
	void ship(T t);
}

interface Shippable3&lt;<mark>T extends Shippable&gt;</mark> {
	void ship(T t);
}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid method example 1
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.List;

public class GenericClass&lt;T&gt; {
	T var;
<mark>// const</mark>
	GenericClass(T t, List&lt;T&gt; t1) {
		System.err.println("Const");
	}
	// const
	&lt;T&gt; GenericClass(T t){
		System.err.println("Const1");
	}
	<mark>// method</mark>
    T GenericClass(T t){
    	System.err.println("Method");
	    return t;
		
	}

	public static void main(String[] args) {
		String t = null;

		List&lt;String&gt; t1 = null;
		GenericClass&lt;String&gt; gc = new GenericClass&lt;String&gt;(t, t1);
		GenericClass&lt;String&gt; gc1 = new GenericClass&lt;String&gt;(t);
		<mark>gc1.GenericClass(t);</mark>
	}
}

class GenericClass1&lt;T extends Vivek, U&gt; {
	T var;
	U var1;

}

class Vivek {

}
</pre>

<pre class='out'>Const
Const1
Method
</pre>
	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid void method
	</div>
	<div class="back">
<pre>package objGenerics;

public class GenericMethods {

        <mark>// type is necessary for argument. this is not a return type</mark>
	<mark>&lt;T&gt;</mark> void Method1(T t){
		System.err.println("Method1");
	}
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
	}

}
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
valid method generics . not class level
	</div>
	<div class="back">
<pre>package objGenerics;

public class GenericMethods {


	&lt;T&gt; void Method1(T t){
		System.err.println("Method1");
	}
	
<mark>// method can have type eventhough class doest not have type. there is problem if no type is specified see next example</mark>
	<mark>&lt;T&gt; T</mark> Method2(T t){
		System.err.println("Method2");
		return t;
	}
	
	/*T Method3(T t){
		System.err.println("Method3");
		return t;
	}*/
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
		gm.Method2(t);
		//gm.Method3(t);
	}

}
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
invalid method type
	</div>
	<div class="back">
<pre>package objGenerics;

public class <mark>GenericMethods</mark> {


	&lt;T&gt; void Method1(T t){
		System.err.println("Method1");
	}
	
	&lt;T&gt; T Method2(T t){
		System.err.println("Method2");
		return t;
	}
	
	<mark>T Method3(T t){</mark>
		System.err.println("Method3");
		return t;
	}
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
		gm.Method2(t);
		gm.Method3(t);
	}

}
</pre>

<pre class='out'>Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
	The method Method3(T) from the type GenericMethods refers to the missing type T

	at objGenerics.GenericMethods.main(GenericMethods.java:25)</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
   what are the two solutions for method missing type
	</div>
	<div class="back">
<pre>package objGenerics;

public class GenericMethods<mark>&lt;T&gt;</mark> {


	&lt;T&gt; void Method1(T t){
		System.err.println("Method1");
	}
	
	static &lt;T&gt; T Method2(T t){
		System.err.println("Method2");
		return t;
	}
	
	 <mark>T Method3(T t){</mark>
		System.err.println("Method3");
		return t;
	}
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
		gm.Method2(t);
		gm.Method3(t);
	}

}</pre>

or

<pre>package objGenerics;

public class <mark>GenericMethods</mark> {


	&lt;T&gt; void Method1(T t){
		System.err.println("Method1");
	}
	
	static &lt;T&gt; T Method2(T t){
		System.err.println("Method2");
		return t;
	}
	
	<mark>&lt;T&gt; T Method3</mark>(T t){
		System.err.println("Method3");
		return t;
	}
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
		gm.Method2(t);
		gm.Method3(t);
	}

}
</pre>


	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what are legal ways to call generic method
	</div>
	<div class="back">
            <pre>package objGenerics;

public class GenericMethods {


	&lt;T&gt; void Method1(T t){
		System.err.println("Method1");
	}
	
	static &lt;T&gt; T Method2(T t){
		System.err.println("Method2");
		return t;
	}
	
	&lt;T&gt; T Method3(T t){
		System.err.println("Method3");
		return t;
	}
	
	public static void main(String[] args) {
		GenericMethods gm= new GenericMethods();
		Object t = null;
		gm.Method1(t);
		gm.Method2(t);
		<mark>gm.&lt;String&gt;Method3("package");
                gm.&lt;String[]&gt;Method3(args);
</mark>	}

}
</pre>
 <pre class='out'>run:
Method1
Method2
Method3
Method3
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>           
            
	</div>
</div>

 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            when will compiler warning show up
	</div>
	<div class="back">
            A compiler warning is different from a compiler error. Due to type erasure, Java doesn't know this is a problem until
runtime, when it attempts to cast.

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
	</div>
</div>   
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            legacy code issue 1. non generic call to generic method argument
	</div>
	<div class="back">
            <pre>
package objGenerics;

import java.util.ArrayList;
import java.util.List;

class Dragon {}
class Unicorn { }
public class LegacyDragons {

    public static void main(String[] args) {
        List unicorns = new ArrayList();
        unicorns.add(new Unicorn());
        printDragons(unicorns);
    }

    private static void printDragons(List&lt;Dragon&gt; dragons) {
        for (Dragon dragon : dragons) { // ClassCastException
            System.out.println(dragon);
        }
    }
}
</pre>
            
no compiler error.compiler warning . and runtime classcastexception  .you have to identify when a compiler warning will occur  

<pre class='out'>
C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;javac objGenerics\LegacyDragons.java
Note: objGenerics\LegacyDragons.java uses unchecked or unsafe operations.
Note: <mark>Recompile with <mark>-Xlint:unchecked</mark> for details.</mark>

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;<mark>javac -Xlint:unchecked objGenerics</mark>\LegacyDragons.java
objGenerics\LegacyDragons.java:13: warning: [unchecked] unchecked call to add(E) as a member of the raw type List
        unicorns.add(new Unicorn());
                    ^
  where E is a type-variable:
    E extends Object declared in interface List
objGenerics\LegacyDragons.java:14: warning: [unchecked] unchecked method invocation: method printDragons in class LegacyDragons is applied to given types
        printDragons(unicorns);
                    ^
  required: List&lt;Dragon&gt;
  found: List
objGenerics\LegacyDragons.java:14: warning: [unchecked] unchecked conversion
        printDragons(unicorns);
                     ^
  required: List&lt;Dragon&gt;
  found:    List
3 warnings

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;</pre>
	</div>
</div>    
  
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
             legacy code issue 3. generic call to non generic method argument
	</div>
	<div class="back">
            <pre>package objGenerics;

import java.util.List;

public class LegacyUnicorns {

    public static void main(String[] args) {
        java.util.List&lt;Unicorn&gt; unicorns = new java.util.ArrayList&lt;&gt;();
        addUnicorn(unicorns);
        Unicorn unicorn = unicorns.get(0);
    }

    private static void addUnicorn(<mark>List unicorn</mark>) {
        unicorn.add(new Dragon());
    }
}
</pre>
  no compiler error.compiler warning . and runtime classcastexception  .you have to identify when a compiler warning will occur            
            <pre class='out'>
C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;javac objGenerics\LegacyUnicorns.java
Note: objGenerics\LegacyUnicorns.java uses unchecked or unsafe operations.
<mark>Note: Recompile with -Xlint:unchecked for details.</mark>

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;<mark>javac -Xlint:unchecked objGenerics\</mark>LegacyUnicorns.java
objGenerics\LegacyUnicorns.java:14: warning: [unchecked] unchecked call to add(E) as a member of the raw type List
        unicorn.add(new Dragon());
                   ^
  where E is a type-variable:
    E extends Object declared in interface List
1 warning

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;java objGenerics/LegacyUnicorns
Exception in thread "main" java.lang.ClassCastException: objGenerics.Dragon cannot be cast to objGenerics.Unicorn
        at objGenerics.LegacyUnicorns.main(LegacyUnicorns.java:10)

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;
</pre>
	</div>
</div>    
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
             legacy code issue 2. Using Autobox.
	</div>
	<div class="back">
            
       <pre>package objGenerics;

public class LegacyAutoboxing {

    public static void main(String[] args) {
        java.util.List numbers = new java.util.ArrayList();
        <mark>numbers.add(5);</mark>
        int result = numbers.<mark>get(0);</mark>
    }

}
</pre>     
      Note : compiler error for autoboxing only.
             
           <pre class='out'>
C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;javac objGenerics\LegacyAutoboxing.java
objGenerics\LegacyAutoboxing.java:8: error: incompatible types: Object cannot be converted to int
        int result = numbers.get(0);
                                ^
Note: objGenerics\LegacyAutoboxing.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error

C:\vivek\java7\java 8\ocpjp8upgrade\src&gt;

        </pre> 
            
            
	</div>
</div> 
    
    <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is bounds.Bounded Type Parameters
	</div>
	<div class="back">
            
          <pre>

package objGenerics;


public class Box&lt;T&gt; {

 private T t;          

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public <mark>&lt;U extends Number&gt;</mark> void inspect(U u){
        System.out.println("T: " + t.getClass().getName());
        System.out.println("U: " + u.getClass().getName());
    }

    public static void main(String[] args) {
        Box&lt;Integer&gt; integerBox = new Box&lt;Integer&gt;();
        integerBox.set(new Integer(10));
        integerBox.inspect("some text"); // error: this is still String!
    }


}
</pre>
	</div>
</div>
    
 <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is the problem in the generic method
	</div>
	<div class="back">
            <pre>
package objGenerics;


public class GenMethod1 {

public static &lt;T&gt; int countGreaterThan(T[] anArray, T elem) {
    int count = 0;
    for (T e : anArray)
       <mark> if (e &gt; elem)</mark>  // compiler error
            ++count;
    return count;
}

    public static void main(String[] args) {
        
    }

}

        </pre>
            
 <pre class='out'>
C:\vivek\java7\java 8\ocpjp8upgrade\src>javac objGenerics\GenMethod1.java
objGenerics\GenMethod1.java:11: <mark>error: bad operand types for binary operator '>'</mark>
        if (e > elem)  // compiler error
              ^
  first type:  T
  second type: T
  where T is a type-variable:
    T extends Object declared in method <T>countGreaterThan(T[],T)
1 error

C:\vivek\java7\java 8\ocpjp8upgrade\src>
</pre>  
            
but it does not compile because the greater than operator (&gt;) applies only to primitive types such as short, int, double, long, float, byte, and char. You cannot use the &gt; operator to compare objects. To fix the problem, use a type parameter bounded by the Comparable&lt;T&gt; interface:
	</div>
</div>   
    
 
    <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            how to use comparator and bounded parameter
	</div>
	<div class="back">
            <pre>
package objGenerics;


public class GenMethod1 {

public static <mark>&lt;T extends Comparable&lt;T&gt;&gt;</mark> int countGreaterThan(T[] anArray, T elem) {
    int count = 0;
    for (T e : anArray)
        if (<mark>e.compareTo(elem) &gt; 0</mark>) 
            ++count;
    return count;
}

    public static void main(String[] args) {
        
    }

}
<mark>interface Comparable&lt;T&gt; {
    public int compareTo(T o);
</mark>}</pre>
	</div>
</div>
    
    <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
             what is unbounded type erasure
	</div>
	<div class="back">
            <pre>public class <mark>Node&lt;T&gt;</mark> {

    private <mark>T </mark>data;
    private Node&lt;T&gt; next;

    public Node<mark>(T</mark> data, Node&lt;T&gt; next) }
        this.data = data;
        this.next = next;
    }

    public <mark>T ge</mark>tData() { return data; }
    // ...
}</pre>
            
            changes to
            <pre>public class <mark>Node</mark> {

    private <mark><mark>Object</mark></mark> data;
    private <mark>Node </mark>next;

    public Node(<mark><mark>Object </mark>data</mark>, Node next) {
        this.data = data;
        this.next = next;
    }

    public <mark>Object g</mark>etData() { return data; }
    // ...
}</pre>
	</div>
</div>
 
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is generic class with bounded type parameter type erasure
	</div>
	<div class="back">
  <pre>public class <mark>Node&lt;T extends Comparable&lt;T&gt;&gt;</mark> {

    private T data;
    private Node&lt;T&gt; next;

    public Node(T data, Node&lt;T&gt; next) {
        this.data = data;
        this.next = next;
    }

    public T getData() { return data; }
    // ...
}</pre>          
        changes to
<pre>public class Node {

    private <mark>Comparable</mark> data;
    private Node next;

    public Node(<mark>Comparable data</mark>, Node next) {
        this.data = data;
        this.next = next;
    }

    public <mark>Comparable ge</mark>tData() { return data; }
    // ...
}</pre>        
        
	</div>
</div>    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            what is Erasure of Generic Methods
	</div>
	<div class="back">
            
            <pre>public static &lt;T&gt; int count(T[] anArray, T elem) {
    int cnt = 0;
    for (T e : anArray)
        if (e.equals(elem))
            ++cnt;
        return cnt;
}</pre>
            
            changes to
<pre>public static int count(<mark>Object[]</mark> anArray, Object elem) {
    int cnt = 0;
    for <mark>(Object</mark> e : anArray)
        if (e.equals(elem))
            ++cnt;
        return cnt;
}</pre>          
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what are the Types of bounds
	</div>
	<div class="back">
<pre>a) Unbounded wildcard   <mark>?</mark> 
b) Wildcard with an upper bound <mark>? extends type</mark>
c) Wildcard with a lower bound <mark>? super type</mark>
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
          valid Unbounded wildcard
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class Genericunboundwilcard {
	<mark><mark>List&lt;?&gt;</mark></mark> t1=null;

	&lt;T&gt;  T Method3<mark>(List&lt;?&gt;</mark> t){
		System.err.println("Method3");
		
		return <mark>(T)</mark> t;
	}
	
	<mark>List&lt;?&gt; M</mark>ethod4(){
		System.err.println("Method4");
		<mark>List&lt;?&gt; t</mark>2=null;
		return t2;
		
		
	}
	
	public static void main(String[] args) {
		Genericunboundwilcard gm= new Genericunboundwilcard();
		
		List&lt;?&gt; t=null;
		
		gm.Method3(t);
		gm.Method4();
		<mark>List&lt;?&gt; l</mark> =new	<mark>ArrayList&lt;String&gt;();</mark>
		
	}

}
class Vivek1{
	
}</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">

valid unbounded wildcard

	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class Genericunboundwilcard1 {

	public static void printList(<mark>List&lt;?&gt;</mark> list) {
		for (Object x : list)
			System.out.println(x);
	}

	public static void main(String[] args) {
		List<mark>&lt;String&gt;</mark> keywords = new ArrayList&lt;&gt;();
		keywords.add("java");
		printList(keywords);
		
		List<mark>&lt;Integer&gt;</mark> keywordsint = new ArrayList&lt;&gt;();
		keywordsint.add(12);
		printList(keywordsint);
	}

}</pre>

When using a wildcard, List<?>, any generic type can be assigned to the reference, but for access only, no modifications.
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is Upper-Bounded Wildcards
	</div>
	<div class="back">
List<mark>&lt;? extends Number&gt;</mark> list = new ArrayList&lt;Integer&gt;();
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is erasure of Upper-Bounded Wildcards
	</div>
	<div class="back">
<pre>public static long total(<mark>List&lt;? extends Number&gt;</mark> list) {
long count = 0;
for (<mark>Number </mark>number: list)
count += number.longValue();
return count;
}</pre>

changes to 

<pre>public static long total(<mark>List</mark> list) {
long count = 0;
for (<mark>Object obj</mark>: list) {
<mark>Number number = (Number) obj;</mark>
count += number.longValue();
}
return count;
}</pre>

	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
  what is the output 
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsUpperboundWildcard {

	static class Sparrow extends Bird { }
	static class Bird { }
	
	public static void main(String[] args) {
		List&lt;Bird&gt; birds = new ArrayList&lt;Bird&gt;();
<mark>		 birds.add(new Sparrow()); // DOES NOT COMPILE
		 birds.add(new Bird());</mark>
		 System.err.println(birds);

	}

}</pre>

<pre class='out'>[objGenerics.GenericsUpperboundWildcard$Sparrow@2c40fd79, objGenerics.GenericsUpperboundWildcard$Bird@bf098a37]</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output with upper bounds or unbounded wildcards.declaration
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsUpperboundWildcard {

	static class Sparrow extends Bird {
	}

	static class Bird {
	}

	public static void main(String[] args) {
		List<mark>&lt;?&gt;</mark> birds = new ArrayList&lt;Bird&gt;();
		birds<mark>.add(new Sparrow()); // DOES NOT COMPILE
		birds.add(new Bird());</mark>
		System.err.println(birds);

		List<mark>&lt;? extends Bird&gt;</mark> birds1 = new ArrayList&lt;Bird&gt;();
		bird<mark>s1.add(new Sparrow()); // DOES NOT COMPILE
		birds1.add(new Bird());</mark>
		System.err.println(birds1);

	}

}</pre>

Java doesn't know what type List&lt;? extends
Bird&gt; or List<mark>&lt;?&gt;</mark> really is.It could be List&lt;Bird&gt; or List&lt;Sparrow&gt; or some other generic type that
hasn't even been written yet
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output with upper bounds interface.declaration
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsUpperboundWildcardInterface {

	private static void anyFlyer(List&lt;Flyer&gt; flyer) {}
	private static void groupOfFlyers(<mark>List&lt;? extends Flyer&gt;</mark> flyer) {
		System.err.println(flyer);
		
	}
	public static void main(String[] args) {
	
		<mark>List&lt;HangGlider&gt; flyer = new ArrayList&lt;HangGlider&gt;();</mark>
		flyer.add(new HangGlider());
		groupOfFlyers(flyer);
	}

}
interface Flyer { void fly(); }
class HangGlider implements Flyer { public void fly() {} }
class Goose implements Flyer { public void fly() {} }</pre>

<pre class='out'>[objGenerics.HangGlider@1c86e0a1]</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output with upper bounds interface.declaration
	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsUpperboundWildcardInterface {

	private static void anyFlyer(List&lt;Flyer&gt; flyer) {}
	private static void groupOfFlyers(<mark>List&lt;? extends Flyer&gt;</mark> flyer) {
		<mark>flyer.add(new HangGlider());</mark>
		System.err.println(flyer);
		
	}
	public static void main(String[] args) {
	
		List&lt;HangGlider&gt; flyer = new ArrayList&lt;HangGlider&gt;();
		flyer.add(new HangGlider());
		groupOfFlyers(flyer);
	}

}
interface Flyer { void fly(); }
class HangGlider implements Flyer { public void fly() {} }
class Goose implements Flyer { public void fly() {} }</pre>
cannot modify. any generic type can be assigned to the reference, but for access only, no modifications.upper bounds interface and unbounded wildcards
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the lower bound wildcard


	</div>
	<div class="back">
<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard {

	public static void main(String[] args) {
		List&lt;String&gt; strings = new ArrayList&lt;String&gt;();
		strings.add("tweet");
		List&lt;Object&gt; objects = new ArrayList&lt;Object&gt;();
		addSound(strings);
		addSound(objects);

	}

	public static void addSound(<mark>List&lt;? super String&gt;</mark> list) {
		<mark>list.add("quack");</mark>
		System.err.println(list);
	}
}
</pre>

<pre class='out'>[tweet, quack]
[quack]</pre>
you can modify the list in the method with lower bounds
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the ouput for

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard {

	public static void main(String[] args) {
		List&lt;String&gt; <mark>strings</mark> = new ArrayList&lt;String&gt;();
		strings.add("tweet");
		List&lt;Object&gt; objects = new ArrayList&lt;Object&gt;<mark>(strings</mark>);
		addSound(strings);
		addSound(objects);

	}

	public static void addSound(List&lt;? super String&gt; list) {
		list.add("quack");
		System.err.println(list);
	}
}</pre>
	</div>
	<div class="back">
<pre class='out'><mark>[tweet</mark>, quack]
<mark>[tweet,</mark> quack]
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
is this valid

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard {



	public static &lt;T&gt; void main(String[] args) {
		List&lt;String&gt; strings = new ArrayList&lt;String&gt;();
		strings.add("tweet");
		List&lt;Object&gt; objects = new ArrayList&lt;Object&gt;(strings);
		addSound(strings);
		addSound(objects);
		<mark>Comparable t = null;
		Method3(t);</mark>
	}

	public static void addSound(List&lt;? super String&gt; list) {
		list.add("quack");
		System.err.println(list);
	}
	
	
	static <mark>&lt;T extends Comparable&lt;? super T&gt;&gt;</mark>  void Method3(T t){
		System.err.println("Method3");
		//return t;
	}
}
</pre>
	</div>
	<div class="back">
<pre class='out'>[tweet, quack]
[tweet, quack]
Method3</pre>

the erasure changes it to 

<pre>static void Method3(<mark>Comparable </mark>t){</pre>


	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard {



	public static &lt;T&gt; void main(String[] args) {
		List&lt;String&gt; strings = new ArrayList&lt;String&gt;();
		strings.add("tweet");
		List&lt;Object&gt; objects = new ArrayList&lt;Object&gt;(strings);
		addSound(strings);
		addSound(objects);
		Comparable t = null;
		Method3(t);
	}

	/*public static void addSound(List&lt;? super String&gt; list) {
		list.add("quack");
		System.err.println(list);
	}
	*/
	<mark>public static void addSound(List list) {
		list.add("quack");
		System.err.println(list);
	}</mark>
	
	static &lt;T extends Comparable&lt;? super T&gt;&gt;  void Method3(T t){
		System.err.println("Method3");
		//return t;
	}
	

}
</pre>
	</div>
	<div class="back">
<pre class='out'>[tweet, quack]
[tweet, quack]
Method3</pre>

output similar to lower bounds
	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output 

<pre>package objGenerics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard1 {

	public static void main(String[] args) {

		List<mark>&lt;? super IOException&gt;</mark> exceptions = new ArrayList<mark>&lt;FileNotFoundException&gt;</mark>();
		exceptions.add(new Exception()); // DOES NOT COMPILE
		exceptions.add(new IOException());
		exceptions.add(new FileNotFoundException());
	}

}</pre>
	</div>
	<div class="back">
<pre class='out'>Exception in thread "main" java.lang.Error: Unresolved compilation problems: 
	Type mismatch: <mark>cannot convert from ArrayList&lt;FileNotFoundException&gt; to List&lt;? super IOException&gt;</mark>
	The method add(capture#1-of ? super IOException) in the type List&lt;capture#1-of ? super IOException&gt; is not applicable for the arguments (Exception)

	at objGenerics.GenericsLowerboundWildcard1.main(GenericsLowerboundWildcard1.java:12)
</pre>
	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is the output
<pre>package objGenerics;

import java.io.<mark>FileNotFoundException</mark>;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenericsLowerboundWildcard1 {

	public static void main(String[] args) {

		List<mark>&lt;? super FileNotFoundException&gt;</mark> exceptions = new ArrayList<mark>&lt;FileNotFoundException&gt;()</mark>;
		exceptions.add(<mark>new Exception()</mark>); // DOES NOT COMPILE
		exceptions.add(<mark>new IOException()</mark>);
		exceptions.add(new FileNotFoundException());
	}

}
</pre>
	</div>
	<div class="back">
<pre class='out'>Exception in thread "main" java.lang.Error: Unresolved compilation problems: 
	The method add(capture#1-of ? super FileNotFoundException) in the type List&lt;capture#1-of ? super FileNotFoundException&gt; is not applicable for the arguments (Exception)
	The method add(capture#2-of ? super FileNotFoundException) in the type List&lt;capture#2-of ? super FileNotFoundException&gt; is not applicable for the arguments (IOException)

	at objGenerics.GenericsLowerboundWildcard1.main(GenericsLowerboundWildcard1.java:13)
</pre>

only subclass is allowed in the type

	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
why this does not compile

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsAllBoundTest {


	public static void main(String[] args) {
		List&lt;?&gt; unbounglist = new ArrayList&lt;A&gt;();
		<mark>unbounglist.add(new A());
		unbounglist.add(new B());
		unbounglist.add(new C());</mark>
		System.err.println(unbounglist);
		
		List&lt;? extends A&gt; upperboundlist = new ArrayList&lt;A&gt;();
		<mark>upperboundlist.add(new A());
		upperboundlist.add(new B());
		upperboundlist.add(new C());</mark>
		System.err.println(upperboundlist);
		
		List&lt;? super A&gt; lowerboundlist = new ArrayList&lt;A&gt;();
		lowerboundlist.add(new A());
		lowerboundlist.add(new B());
		lowerboundlist.add(new C());
		System.err.println(lowerboundlist);
		
		List&lt;A&gt; boundedList = new ArrayList&lt;A&gt;();
		boundedList.add(new A());
		boundedList.add(new B());
		boundedList.add(new C());
		System.err.println(boundedList);
		
		List noGenList = new ArrayList();
		noGenList.add(new A());
		noGenList.add(new B());
		noGenList.add(new C());	
		System.err.println(noGenList);
		
		
	}

}
class A {}
class B extends A { }
class C extends B { }</pre>
	</div>
	<div class="back">
cannot add/modify the unbounded and upper bound values
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the problem in the higlighted code

<pre>import java.util.List;

public class GenericsAllBoundTest {


	public static void main(String[] args) {
		/*List&lt;?&gt; unbounglist = new ArrayList&lt;A&gt;();
		unbounglist.add(new A());
		unbounglist.add(new B());
		unbounglist.add(new C());
		System.err.println(unbounglist);*/
		
		<mark>List&lt;? extends B&gt; upperboundlist = new ArrayList&lt;A&gt;();</mark>
		upperboundlist.add(new A());
		upperboundlist.add(new B());
		upperboundlist.add(new C());
		System.err.println(upperboundlist);
		
		List&lt;? super B&gt; lowerboundlist = new ArrayList&lt;A&gt;();
		<mark>lowerboundlist.add(new A());</mark>
		lowerboundlist.add(new B());
		lowerboundlist.add(new C());
		System.err.println(lowerboundlist);
		
		/*List&lt;B&gt; boundedList = new ArrayList&lt;A&gt;();
		boundedList.add(new A());
		boundedList.add(new B());
		boundedList.add(new C());
		System.err.println(boundedList);*/
		
		List noGenList = new ArrayList();
		noGenList.add(new A());
		noGenList.add(new B());
		noGenList.add(new C());	
		System.err.println(noGenList);
		
		
	}

}
class A {}
class B extends A { }
class C extends B { }</pre>
	</div>
	<div class="back">
<pre>a) Type mismatch: cannot convert from ArrayList&lt;A&gt; to List&lt;? extends B&gt; 

b) super on left can accept only sub classes</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the problem in the higlighted code

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsAllBoundTest {


	public static void main(String[] args) {
		List&lt;?&gt; unbounglist = new ArrayList&lt;B&gt;();
		<mark>unbounglist.add(new A());
		unbounglist.add(new B());
		unbounglist.add(new C());</mark>
		System.err.println(unbounglist);
		
		List&lt;? extends B&gt; upperboundlist = new ArrayList&lt;B&gt;();
		<mark>upperboundlist.add(new A());
		upperboundlist.add(new B());
		upperboundlist.add(new C());</mark>
		System.err.println(upperboundlist);
		
		List&lt;? super B&gt; lowerboundlist = new ArrayList&lt;B&gt;();
		<mark>lowerboundlist.add(new A());</mark>
		lowerboundlist.add(new B());
		lowerboundlist.add(new C());
		System.err.println(lowerboundlist);
		
		List&lt;B&gt; boundedList = new ArrayList&lt;B&gt;();
		<mark>boundedList.add(new A());</mark>
		boundedList.add(new B());
		boundedList.add(new C());
		System.err.println(boundedList);
		
		List noGenList = new ArrayList();
		noGenList.add(new A());
		noGenList.add(new B());
		noGenList.add(new C());	
		System.err.println(noGenList);
		
		
	}

}
class A {}
class B extends A { }
class C extends B { }</pre>

	</div>
	<div class="back">
<pre>a) & b)  cannot add/modify the unbounded and upper bound values

c) & d) only B and subclasses of B can be added</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
what is the output

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsAll<mark>B</mark>oundTest {


	public static void main(String[] args) {

		GenericsAllBoundTest ga= new GenericsAllBoundTest();
		List&lt;A&gt; list = new ArrayList&lt;&gt;();
		ga.method3(list);
		
		
	}
	&lt;T extends A&gt; <mark>B m</mark>ethod3(List&lt;T&gt; list) {
		System.err.println(list);
		return <mark>new B()</mark>; // DOES NOT COMPILE
		}

}
class A {}
class B extends A { }
class C extends B { }</pre>
	</div>
	<div class="back">
[]
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
why this does not compile

<pre>package objGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsAllBoundTest {


	public static void main(String[] args) {

		GenericsAllBoundTest ga= new GenericsAllBoundTest();
		List&lt;A&gt; list = new ArrayList&lt;&gt;();
		ga.method3(list);
		
		
	}
	<mark>&lt;B</mark> extends A&gt; B method3(<mark>List&lt;B&gt;</mark> list) {
		System.err.println(list);
		return new B(); // DOES NOT COMPILE
		}

}
class A {}
class B extends A { }
class C extends B { }</pre>
	</div>
	<div class="back">

B is considered as type and not the class in this example

	</div>
</div>-->
    
    
</div>
</body>
</html>


