 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>@Override</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is DecimalFormat
	</div>
	<div class="back">

	a special subclass of NumberFormat.
used when you want specific formatting.

<pre>package obj12DateFormat;

import <mark>java.text.DecimalFormat</mark>;

public class DecimalFormatExample {
	public static void main(String[] args) {
		double d = 1234567.437;
		DecimalFormat one = <mark>new DecimalFormat</mark>("###,###,###.###");
		System.out.println(one.format(d));
	}
}</pre>

<pre class='out'>1,234,567.437</pre>

<pre>package obj12DateFormat;

import java.text.DecimalFormat;

public class DecimalFormatExample {
	public static void main(String[] args) {
		float d = 1234567.437f;
		DecimalFormat one = new DecimalFormat("###,###,###.###");
		System.out.println(one.format(d));
	}
}
</pre>

<pre>package obj12DateFormat;

import java.text.DecimalFormat;

public class DecimalFormatExample {
	public static void main(String[] args) {
		int d = 1234567;
		DecimalFormat one = new DecimalFormat("###,###,###.###");
		System.out.println(one.format(d));
	}
}
</pre>

<pre class='out'>1,234,567</pre>


String java.text.NumberFormat.format(long number)

<pre>package obj12DateFormat;

import java.text.DecimalFormat;

public class DecimalFormatExample {
	public static void main(String[] args) {
		double d = 1234567.1;
		DecimalFormat two = new DecimalFormat("000,000,000.00000");
		 System.out.println(two.format(d)); // <mark>001</mark>,234,567.<mark>10000</mark>
	
		DecimalFormat three = new DecimalFormat("<mark>$</mark>#,###,###.##");
		 System.out.println(three.format(d)); // $1,234,567.1
	}
}
</pre>

	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	what is DecimalFormat Formatting Characters
	</div>
	<div class="back">
<pre><mark>#</mark> means to <mark>omit </mark>the position <mark>if no digit exists</mark> for it.

<mark>0 </mark>means to <mark>put a 0 in the position if no digit exists</mark> for it.</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	   DecimalFormat syntax
	</div>
	<div class="back">
<pre>   String java.text.NumberFormat.format(long number)

	   df format (number)</pre>
	</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  factory or constructor for decimal Format
	</div>
	<div class="back">
DecimalFormat uses constructor
</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  factory or constructor for Date Format
	</div>
	<div class="back">
DateFormat uses factory method
</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  common methods for Date Format
	</div>
	<div class="back">
DecimalFormat uses factory method

<pre>getDateInstance()
getTimeInstance()
getDateTimeInstance()</pre>


</div>
</div>




<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  overloaded methods for Date Format
	</div>
	<div class="back">
<pre>
       getDateInstance(style)
	getDateInstance(style, locale)

	getTimeInstance(style)
	getTimeInstance(style, locale)

	getDateTimeInstance(dstyle, tstyle)
	getDateTimeInstance(dstyle, tstyle,  locale)
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  style values ?
	</div>
	<div class="back">
<pre>
     FULL, LONG, MEDIUM, SHORT
	<mark> flms</mark>
</pre>
	</div>
</div>


<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  Format and parse meaning
	</div>
	<div class="back">
<pre>
    Format= number - string  ns
	Parse= String- number    sn
</pre>
	</div>
</div>

    <div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	  Format example
	</div>
	<div class="back">
<pre>
        package obj12DateFormat;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FormatExample {

    public static void main(String[] args) {
        DateFormat s = DateFormat.getDateInstance(DateFormat.SHORT);
        DateFormat m = DateFormat.getDateInstance(DateFormat.MEDIUM);
        DateFormat l = DateFormat.getDateInstance(DateFormat.LONG);
        DateFormat f = DateFormat.getDateInstance(DateFormat.FULL);
        <mark>Date d </mark>= new GregorianCalendar(2015, Calendar.JULY, 4).getTime();
        System.out.println(<mark>s.<mark>format(d)</mark></mark>); // 7/4/15
        System.out.println(<mark>m.format(d))</mark>; // Jul 4, 2015
        System.out.println(l.format(d)); // July 4, 2015
        System.out.println(f.format(d));
    }

}</pre>
            
            <pre class='out'>run:
7/4/15
Jul 4, 2015
July 4, 2015
Saturday, July 4, 2015
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
            <p>don’t need to memorize the differences in output formatting.need to know that there are four styles</p>
	</div>
</div>
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            time with date format example
	</div>
	<div class="back">
            <pre>package obj12DateFormat;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeFormatExample {

    public static void main(String[] args) {

        Date d = new GregorianCalendar(2015, Calendar.JULY, 4).getTime();
        DateFormat dtf = <mark>DateFormat.getDateTimeInstance(
                DateFormat.MEDIUM, DateFormat.FULL);</mark>
        System.out.println(dtf.format(d));
    }

}</pre>
<pre class='out'>run:
Jul 4, 2015 12:00:00 AM CDT
BUILD SUCCESSFUL (total time: 0 seconds)</pre>            
            code using a <mark>MEDIUM date style</mark> and a <mark>FULL time</mark> style:. also changes with timezone.
            Jul 4, 2015 12:00:00 AM EDT
	</div>
</div>   
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Locale and time with date format example
	</div>
	<div class="back">
            <pre>package obj12DateFormat;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class LocaleTimeFormatExample {

    public static void main(String[] args) {

        Date d = new GregorianCalendar(2015, Calendar.JULY, 4).getTime();
        DateFormat de = DateFormat.getDateTimeInstance(
                DateFormat.MEDIUM, DateFormat.FULL, <mark>Locale.GERMANY</mark>);
        System.out.println(de.format(d));
    }

}</pre>
            
<pre class='out'>run:
04.07.2015 00:00 Uhr CDT
BUILD SUCCESSFUL (total time: 0 seconds)</pre>     
            
	</div>
</div> 
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            
            method syntax for format and parse
	</div>
	<div class="back">
<pre>
        public final String format(Date date) - format
            public Date parse(String source) throws ParseException - parse</pre>
            the ParseException is thrown when
the <mark>beginning of the string</mark> cannot be parsed into a date successfully
	</div>
</div>    
 
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            example for parse with style and locale in dateformat
	</div>
	<div class="back">
            <pre>package obj12DateFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class ParseDateformat {

    public static void main(String[] args) throws ParseException {
        DateFormat shortFormat = DateFormat.getDateInstance(
                <mark>DateFormat.SHORT, Locale.US</mark>);
        String s = "01/31/1984";
        Date date = <mark>shortFormat.parse(s);</mark>
        DateFormat fullFormat = DateFormat.getDateInstance(
                <mark>DateFormat.FULL, Locale.FRANCE</mark>);
        System.out.println(<mark>fullFormat.format(date));</mark>

    }

}</pre>
            
<pre class='out'>run:
mardi 31 janvier 1984
BUILD SUCCESSFUL (total time: 1 second)</pre>
The format of the String object depends on both the <mark>style and the locale </mark>of the
DateFormat object.            
	</div>
</div>    

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            Note about Parseexception
	</div>
	<div class="back">
            The parse method throws a ParseException if the beginning of the string
cannot be parsed. As with the parse method in NumberFormat , the parse
method in DateFormat successfully parses a string if the beginning of the
string is in the proper format.
	</div>
</div>     
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            how to do a custom date format
	</div>
	<div class="back">
            use the SimpleDateFormat subclass.
<pre><b>MMMM</b>M- represents month
example, <mark>M outputs 1 , MM outputs 01 , MMM outputs Jan , and MMMM outputs January </mark>.
dd means to <mark>include the leading zero </mark>for a single-digit day.
yy outputs a <mark>two-digit year,</mark> and yyyy outputs a four-digit year.
hh
mm
ss</pre>            
	</div>
</div>    
    
    
<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
            example for simple date format
	</div>
	<div class="back">
            
<pre>
package obj12DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SimpleFormatExample {


    public static void main(String[] args) throws ParseException {
        SimpleDateFormat f1 = new SimpleDateFormat("MM dd yyyy hh:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("MMMM yyyy");
        SimpleDateFormat f3 = new SimpleDateFormat("hh");
        <mark>Date date = f1.parse("01 26 2016 01:22:33");</mark>
        System.out.println(f2.format(date));
        System.out.println(f3.format(date));
    }

}</pre>
 <pre class='out'>run:
January 2016
01
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>           
            
	</div>
</div>

    

</div>
</body>
</html>
