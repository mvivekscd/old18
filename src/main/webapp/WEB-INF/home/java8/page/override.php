 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>@Override</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>

<div class="flip-container" >

<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	 what is @Override annotation
	</div>
	<div class="back">
	The @Override annotation is used to express that you, the programmer, intend for this
method to override one in a superclass or implement one from an interface.

<pre>package override;

class Bobcat {
	public void findDen() {
	}
}

public class BobcatMother extends Bobcat {
	<mark>@Override</mark>
	public void findDen(boolean b) {
	} // DOES NOT COMPILE
}
</pre>

	</div>
</div>



<div class="flipper" onclick="toggleflip(this)">
	<div class="front">
	@Override .what should be checked.
	</div>
	<div class="back">
	check carefully that
the method is doing one of three things:

<p>Implementing a method from an interface</p>
<p>Overriding a superclass method of a class shown in the example</p>
<p>Overriding a method declared in Object, such as hashCode, equals, or toString</p>
	</div>
</div>


</div>
</body>
</html>

