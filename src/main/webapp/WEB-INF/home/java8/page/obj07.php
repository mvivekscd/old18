<?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Objective 1 Diamond operator</title>
        <script src="card.js"></script>
        <link rel="stylesheet" type="text/css" href="flash.css">
    </head>

    <body onload="myFunction()">

        Total FlashCards <span id="cou"></span>
        <br>
        <input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

        <button id="prev" onclick="decr()" type="button">prev</button>
        <button id="next" onclick="incr()" type="button">next</button>
        <button id="back" onClick="location.href = 'home.php'" type="button">Back</button>
        <button id="next" onclick="disableflash()" type="button">Disable flash</button>

        <div class="flip-container" >

            <div class="flipper" onclick="toggleflip(this)">
                <div class="front">
                    what is concurrency
                </div>
                <div class="back">
                    <img src="../../../imag/jp8/concur001.png" class="imgw"> 
                </div>

            </div>
            <div class="flipper" onclick="toggleflip(this)">

                <div class="front">
what is difference between thread and process
                </div>

                <div class="back">
<p>A <mark>thread
</mark>is the <mark>smallest unit of execution </mark>that can be scheduled by the operating system. A <mark>process</mark>
is a <mark>group of associated threads </mark>that execute in the same, shared environment. It follows,
then, that a <mark>single-threaded process </mark>is one that contains exactly <mark>one thread</mark>, whereas a
<mark>multi-threaded process </mark>is one that contains <mark>one or more threads</mark>.</p>
 <img src="../../../imag/jp8/1.png" class="imgw">
                </div>

            </div>
        </div>
    </body>
</html>