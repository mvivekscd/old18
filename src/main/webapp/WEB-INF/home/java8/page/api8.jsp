<%-- 
    Document   : objUpgrade
    Created on : May 6, 2017, 11:11:07 PM
    Author     : manju
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>java 8 upgrade</title>
    </head>
    <body>
        <h3> Language Enhancements</h3>

        <p>Develop code that uses String objects in the switch statement, binary literals, and numeric literals, including underscores in literals</p>
        <p>Develop code that uses try-with-resources statements, including using classes that implement the AutoCloseable interface</p>
        <p>Develop code that handles multiple Exception types in a single catch block</p>
        <p>Use static and default methods of an interface including inheritance rules for a default method</p>
<h3>Concurrency</h3>

<p>Use classes from the java.util.concurrent package including CyclicBarrier and CopyOnWriteArrayList with a focus on the advantages over and differences from the traditional java.util collections 
</p>
<p>Use Lock, ReadWriteLock, and ReentrantLock classes in the java.util.concurrent.locks and java.util.concurrent.atomic packages to support lock-free thread-safe programming on single variables</p>
<p>Use Executor, ExecutorService, Executors, Callable, and Future to execute tasks using thread pools</p>
<p>Use the parallel Fork/Join Framework</p>
<h3>Localization</h3>

<p>Describe the advantages of localizing an application and developing code that defines, reads, and sets the locale with a Locale object</p>
<p>Build a resource bundle for a locale and call a resource bundle from an application</p>
<p>Create and manage date- and time-based events by using LocalDate, LocalTime, LocalDateTime, Instant, Period, and Duration, including a combination of date and time in a single object</p>
<p>Format dates, numbers, and currency values for localization with the NumberFormat and DateFormat classes, including number and date format patterns</p>
<p>Work with dates and times across time zones and manage changes resulting from daylight savings</p>
<h3>Java File I/O (NIO.2)
</h3>
<p>Operate on file and directory paths by using the Paths class</p>
<p>Check, delete, copy, or move a file or directory by using the Files class </p>
<p>Recursively access a directory tree by using the DirectoryStream and FileVisitor interfaces</p>
<p>Find a file by using the PathMatcher interface, and use Java SE 8 I/O improvements, including Files.find(), Files.walk(), and lines() methods</p>
<p>Observe the changes in a directory by using the WatchService interface</p>
<h3>Lambda</h3>

<p>Define and write functional interfaces and describe the interfaces of the java.util.function package</p>
<p>Describe a lambda expression; refactor the code that uses an anonymous inner class to use a lambda expression; describe type inference and target typing</p>
<p>Develop code that uses the built-in interfaces included in the java.util.function package, such as Function, Consumer, Supplier, UnaryOperator, Predicate, and Optional APIs, including the primitive and binary variations of the interfaces</p>
<p>Develop code that uses a method reference, including refactoring a lambda expression to a method reference</p>
<h3>Java Collections</h3>

<p>Develop code that uses diamond with generic declarations</p>
<button type="button" onclick="location.href = '<%=request.getContextPath()%>/Java8Home/objUpgrade'">Flashcard</button>
<p>Develop code that iterates a collection, filters a collection, and sorts a collection by using lambda expressions</p>
<p>Search for data by using methods, such as findFirst(), findAny(), anyMatch(), allMatch(), and noneMatch()</p>
<p>Perform calculations on Java Streams by using count, max, min, average, and sum methods and save results to a collection by using the collect method and Collector class, including the averagingDouble, groupingBy, joining, partitioningBy methods</p>
<p>Develop code that uses Java SE 8 collection improvements, including the Collection.removeIf(), List.replaceAll(), Map.computeIfAbsent(), and Map.computeIfPresent() methods</p>
<p>Develop  code that uses the merge(), flatMap(), and map() methods on Java Streams</p>
<h3>Java Streams</h3>

<p>Describe the Stream interface and pipelines; create a stream by using the Arrays.stream() and  IntStream.range() methods; identify the lambda operations that are lazy</p>
<p>Develop code that uses parallel streams, including decomposition operation and reduction operation in streams</p>
    </body>
</html>
