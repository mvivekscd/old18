 <?php include '../../../webinf/config/config.php'; ?> 
<?php include '../../../webinf/session/session.php'; ?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Objective 23 Diamond operator</title>
<script src="card.js"></script>
<link rel="stylesheet" type="text/css" href="flash.css">
</head>

<body onload="myFunction()">

Total FlashCards <span id="cou"></span>
<br>
<input type="text" id="quest" name="fname" value="1" ><button id="goto" onclick="gotop()" type="button">Select</button>

<button id="prev" onclick="decr()" type="button">prev</button>
<button id="next" onclick="incr()" type="button">next</button>
<button id="back" onClick="location.href='home.php'" type="button">Back</button>
<button id="next" onclick="disableflash()" type="button">Disable flash</button>

<div class="flip-container" >
    <div class="flipper" onclick="toggleflip(this)">
        <div class="front">
            what is Diamond Operator
        </div>
        <div class="back">
            In Java SE 7 and later, you can replace the type arguments required to invoke the constructor of a generic class with an empty set of type arguments (<>) 

			<pre>
			Box&lt;Integer&gt; integerBox = new Box<mark>&lt;&gt;</mark>();

			</pre>
        </div>
    </div>

	<div class="flipper" onclick="toggleflip(this)">
			<div class="front">
				<pre>Map&lt;Map&lt;String,Integer&gt;,List&lt;Double&gt;&gt; map3
= new HashMap&lt;&lt;&gt;,&lt;&gt;&gt;();</pre>

will this compile
			</div>
			<div class="back">
				No. should be = new HashMap<>(); Nesting diamond
operators is not allowed.
			</div>
		</div>
	

<div class="flipper" onclick="toggleflip(this)">
		<div class="front">
		which side diamond operator is allowed
			</div>
			<div class="back">
			it is allowed only
on the <mark>right side </mark>of an assignment operation. else <mark>compilation error</mark>.
			</div>
		</div>


<div class="flipper" onclick="toggleflip(this)">
		<div class="front">
		how to use diamond operator in a method
		</div>
		<div class="back">

		valid 
		<pre>        List&lt;List&lt;String&gt;&gt; generic = new ArrayList();
		List&lt;List&lt;String&gt;&gt; generic1 = new <mark>ArrayList&lt;&gt;()</mark>;

java 7
         testGenericParam(new ArrayList&lt;&gt;());
		private static void testGenericParam(<mark>ArrayList&lt;Object&gt;</mark> list) {
		System.out.println("testGenericParam v");</pre>

verify in java 8 invalid in java 7

<pre>testGenericParam(new ArrayList<mark>&lt;&gt;</mark>());
private static void testGenericParam(<mark>ArrayList&lt;String&gt;</mark> list) {
		System.out.println("testGenericParam v");
		
	}</pre>

		</div>
</div>

<div class="flipper" onclick="toggleflip(this)">
		<div class="front">
<pre>Pair&lt;String, Integer&gt; p1 = new OrderedPair&lt;String, Integer&gt;("Even", 8);
Pair&lt;String, String&gt;  p2 = new OrderedPair&lt;String, String&gt;("hello", "world");
</pre>

		OrderedPair&lt;String, Box&lt;Integer&gt;&gt; p = new OrderedPair&lt;&gt;("primes", new Box&lt;Integer&gt;(...));

		is this valid
		</div>
		<div class="back">
		<pre>public class Box&lt;T&gt; {
    // T stands for "Type"
    private T t;

    public void set(T t) { this.t = t; }
    public T get() { return t; }
}
</pre>

<pre>
public interface Pair&lt;K, V&gt; {
    public K getKey();
    public V getValue();
}

public class OrderedPair&lt;K, V&gt; implements Pair&lt;K, V&gt; {

    private K key;
    private V value;

    public OrderedPair(K key, V value) {
	this.key = key;
	this.value = value;
    }

    public K getKey()	{ return key; }
    public V getValue() { return value; }
}
</pre>
valid
<pre>		OrderedPair&lt;String, Integer&gt; p1 = new OrderedPair&lt;&gt;("Even", 8);
		OrderedPair&lt;String, String&gt;  p2 = new OrderedPair&lt;&gt;("hello", "world");
		OrderedPair&lt;String, Box&lt;Integer&gt;&gt; p = new OrderedPair&lt;&gt;("primes", new Box&lt;Integer&gt;());

		
		
		List<List<String>> generic = new ArrayList();
		List<List<String>> generic1 = new ArrayList<>();

java 7
         testGenericParam(new ArrayList<>());
		private static void testGenericParam(ArrayList<Object> list) {
		System.out.println("testGenericParam v");
		
	}
</pre>

invalid
<pre><pre>OrderedPair&lt;String, Box&lt;Integer&gt;&gt; p = new OrderedPair<mark>&lt;&gt;</mark>("primes", new Box<mark>&lt;&gt;</mark>());</pre></pre>

		</div>
</div>

</div>
</body>
</html>