<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="css/base.css"/>
        <link rel="stylesheet" href="css/java.css"/>
        <script src="js/basemenu.js"></script>

    </head>
    <body>

        
    <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
    <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
        <jsp:param name="title" value="JAVA 8 HOME"/>
        <jsp:param name="enableLogout" value="true"/>
    </jsp:include>
    
    <button type="button" onclick="location.href = '<%=request.getContextPath()%>/Java8Home/objUpgrade'">Objectives</button>
    
          <h1>Objectives</h1> 
        <button type="button" onclick="location.href = '/Java8Home1/api8'">API Objective</button>
         <button type="button" onclick="location.href = 'quest8index.php'">quest</button>
        <button type="button" onclick="location.href = 'obj.php?page=objm&title=objectives'">obj</button>


  <button type="button" onclick="location.href = '/Java8Home1/override'">override</button>
<button type="button" onclick="location.href = 'obj.php?page=poly&title=polymorphism'">polymorphism</button>
<button type="button" onclick="location.href = '/Java8Home1/gencoll'">generics and collection</button>
<h3>1. Language Enhancements 1-4</h3>
  <button type="button" class="orange" onclick="location.href = '/Java8Home1/obj01'">obj 01 literal Switch</button>
   <button type="button" onclick="location.href = 'obj.php?page=obj02&title=obj02'">obj 02 try with resources</button>
    <button type="button" onclick="location.href = 'obj.php?page=obj03&title=obj03'">obj 03 multiple exceptions</button>
  <button type="button" class="orange" onclick="location.href = '/Java8Home1/obj04'">obj 04 static and default interface</button>

<h3>2. Concurrency 5-8</h3>
 <button type="button" onclick="location.href = 'obj.php?page=obj05&title=obj05'">obj 05 CyclicBarrier, CopyOnWriteArrayList</button>
  <button type="button" onclick="location.href = 'obj.php?page=obj06&title=obj06'">obj 06 Lock </button>
   <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj07&title=obj07'">obj ExecutorService,Caller, future</button>
    <button type="button"  onclick="location.href = 'obj.php?page=obj08&title=obj08'">obj 08 Fork/Join</button>

<h3>3. Localization 9-13</h3>
<button type="button" onclick="location.href = 'obj.php?page=obj09&title=obj09'">obj 09 Locale</button>
<button type="button" onclick="location.href = 'obj.php?page=obj10&title=obj10'">obj 10 Resource Bundle</button>
  <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj11&title=obj11'">obj 11 date and time</button>
   <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj12&title=obj12'">obj 12 Number format and date format</button>
   
    <button type="button" onclick="location.href = 'obj.php?page=obj13&title=obj13'">obj 13 daylight saving</button>
     
<h3>4. Java File I/O (NIO.2) 14-18 </h3>
<button type="button" class="orange" onclick="location.href = 'obj.php?page=obj14&title=obj14'">obj 14 Paths class</button>
<button type="button" onclick="location.href = 'obj.php?page=obj15&title=obj15'">obj 15 Files class</button>
<button type="button" onclick="location.href = 'obj.php?page=obj16&title=obj16'">obj 16 DirectoryStream FileVisitor</button>
<button type="button" onclick="location.href = 'obj.php?page=obj17&title=obj17'">obj 17 PathMatcher</button>
<button type="button" onclick="location.href = 'obj.php?page=obj18&title=obj18'">obj 18 Watchservice</button>

<h3>5. Lambda 19-22</h3>
  <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj19&title=obj19'">obj 19 functional interfaces </button>
  <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj20&title=obj20'">obj 20 lambda expression</button>
  <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj21&title=obj21'">obj 21 Function, Consumer, Supplier, UnaryOperator, Predicate, and Optional </button>
  <button type="button" class="orange" onclick="location.href = 'obj.php?page=obj22&title=obj22'">obj 22 method reference</button>

<h3>6. Java Collections 23-28</h3>
<button type="button" class="orange" onclick="location.href = 'obj.php?page=obj23&title=obj23'">obj 23 diamond </button>
<button type="button" class="orange" onclick="location.href = 'obj.php?page=obj24&title=obj24'">obj 24 iterate ,filter,sort</button>
<button type="button" onclick="location.href = 'obj.php?page=obj25&title=obj25'">obj 25 Search </button>
<button type="button" onclick="location.href = 'obj.php?page=obj26&title=obj26'">obj 26  Perform calculations</button>
<button type="button" class="orange" onclick="location.href = 'obj.php?page=obj27&title=obj27'">obj 27 collection improvements</button>
<button type="button" class="orange" onclick="location.href = 'obj.php?page=obj28&title=obj28'">obj 28 map,merge</button>

<h3>7. Java Streams 29-30</h3>

<button type="button" onclick="location.href = 'obj.php?page=obj29&title=obj29'">obj 29 create stream, lazy lambda</button>
<button type="button" onclick="location.href = 'obj.php?page=obj30&title=obj30'">obj 30 parallel streams, decomposition ,reduction </button>
    
     <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
</body>
</html>
