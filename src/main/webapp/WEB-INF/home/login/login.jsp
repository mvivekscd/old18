<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="css/base.css"/>
        <script src="js/basemenu.js"></script>
    </head>
    <body>
        <jsp:include page="/WEB-INF/home/common/menu/menu01.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="LOGIN"/>
        </jsp:include>
        ${errormessage}
        <div class="std">
<!--             <form name="loginForm" method="post" action="loginHome" id="form1">
                Username: <input type="text" name="username" required/> <br><br>
                Password: <input type="password" name="password" required/> <br><br>
                <button type="submit" form="form1" value="Submit">Submit</button>
            </form> -->
            
	<form:form action="registersubmit" method="post" modelAttribute="user">
		<form:label path="name">Full name:</form:label>
		<form:input path="name" />
		<br />


		<form:label path="password">Password:</form:label>
		<form:password path="password"  />
		<br />
		
		<form:button>Submit</form:button>
	</form:form>
        </div>
         <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>
