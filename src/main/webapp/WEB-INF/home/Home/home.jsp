<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="css/base.css"/>
        <script src="js/basemenu.js"></script>
        
    </head>
    <body>
        <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/menu/menu02.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="HOME MAIN"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>

        <button type="button" onclick="location.href = 'User'">HTML Editor</button>
        <button type="button" onclick="location.href = 'Spass?d='+new Date();">Password Store</button>
        
        <button type="button" onclick="location.href = 'Java6Home'">java 6</button>
        
        <button type="button" onclick="location.href = 'Java8Home'">java 8</button>
        
                <p>Db</p>
         <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/dbcrud/home.php'">DB CRud</button>
        <p>Tools</p>
        <button type="button" onclick="location.href = 'editor'">Editor</button>
        <p>UI</p>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/angular/home.php'">Angular 4</button>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/react/home.php'">React</button>
        <p>Middleware</p>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/Restful/home.php'">Restful</button>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/java6/home.php'">Java 6</button>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/java8/home.php'">Java 8</button>

        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/jpa/home.php'">JPA</button>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/projhome/home.php'">Project File</button>
        <br>

        <p>Docker Node Kubernetes</p>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/nodeexpress/home.php'">Node.js express js</button>
        
        <p>Docker Gradle Kubernetes</p>
        <button type="button" onclick="location.href = '<?php echo $environment ?>/gview/pages/gradle/home.php'">Gradle</button>      

        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>