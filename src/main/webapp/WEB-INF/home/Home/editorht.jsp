<%-- 
    Document   : editorht
    Created on : Feb 26, 2017, 6:32:31 PM
    Author     : manju
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>

            #vedit textarea#in{
                min-height:400px;
                width:100%;
                resize: both;
                overflow: auto;
            }

        </style>
        <script>
           function getSel(e) // javascript
{
    // obtain the object reference for the <textarea>
    var txtarea = document.getElementById("in");
    // obtain the index of the first selected character
    var start = txtarea.selectionStart;
    // obtain the index of the last selected character
    var finish = txtarea.selectionEnd;
    // obtain the selected text
    var sel = txtarea.value.substring(start, finish);
    // do something with the selected content
    //alert(sel);
    var finalval;
    var buttonhtml = e.innerHTML;
    var repl="";
   switch (e.innerHTML) {
    case 'p':
        repl = "<p>"+sel+"</p>";
        break;
    case 'pre':
        repl = "<pre>"+sel+"</pre>";
        break;
   case 'h3':
        repl = "<h3>"+sel+"</h3>";
        break;    
    case 'preout':
        repl = "<pre class='out'>"+sel+"</pre>";
        break;     
   case 'mark':
        repl = "<mark>"+sel+"</mark>";
        break;   
   case 'break':
        repl = sel+"<br>";
        break;
       case 'bold':
        repl = "<b>"+sel+"</b>";
        break;
   case 'table':
        repl = sel+"<table><tr><th></th></tr><tr><td></td></tr></table>";
        break; 
case 'del':
        repl = "<del>"+sel+"</del>";
        break;
case 'ascii':
 var sel1=   sel.replace(/</g, "&lt;");
 var sel2=   sel1.replace(/>/g, "&gt;");
repl = sel2;
    break;
 case 'preview':   
    break;
     case 'reset':
        document.getElementById("in").value='';
    break;
        }
    finalval = txtarea.value.replace(sel, repl);
    //alert(finalval);
    var txtareaoutut = document.getElementById("in");
    txtareaoutut.value=finalval;
    var divprev = document.getElementById("preview");
    divprev.innerHTML=finalval;
} 
            
        </script>
        
    </head>


    <body id="vedit">
        <button onclick="getSel(this);">p</button>
        <button onclick="getSel(this);">pre</button>
         <button onclick="getSel(this);">h3</button>
          <button onclick="getSel(this);">preout</button>
           <button onclick="getSel(this);">mark</button>
            <button onclick="getSel(this);">img</button>
             <button onclick="getSel(this);">break</button>
             <button onclick="getSel(this);">bold</button>
             <button onclick="getSel(this);">table</button>
              <button onclick="getSel(this);">del</button>
               <button onclick="getSel(this);">ascii</button>
               <button onclick="getSel(this);">preview</button>
               <button onclick="getSel(this);">reset</button>
        <textarea id="in">

        </textarea>
              <div id="preview">
                  
              </div>             
    </body>
</html>
