<%-- 
    Document   : home
    Created on : Dec 23, 2016, 1:14:24 PM
    Author     : manju
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/base.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
        <script src="<%=request.getContextPath()%>/js/basemenu.js"></script>
    </head>
    <body>
         <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="Question"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>
        <jsp:include page="/WEB-INF/home/java6/quest/page/index.html" />
        
        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>
