<%-- 
    Document   : pageq.jsp
    Created on : Dec 23, 2016, 1:40:28 PM
    Author     : manju
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <script src="<%=request.getContextPath()%>/quest/quiz.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
    </head>
    <body>
        <jsp:include page="/WEB-INF/home/java6/quest/page/${pageName}.html" />
    </body>
</html>
