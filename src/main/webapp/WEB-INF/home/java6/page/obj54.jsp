<h3>Given a scenario, develop code that declares and/or invokes overridden or overloaded
    methods and code that declares and/or invokes superclass, overridden, or overloaded
    constructors.</h3>

<h3>Invoking a Superclass Version of an Overridden Method</h3>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.supercall;
    
public class Superclass {
    <mark>public void printMethod() {</mark>
        System.out.println("Printed in Superclass.");
    }
        
}
</pre>
<pre>
public class Subclass extends Superclass {
    
    <mark>public void printMethod() { //overrides printMethod in Superclass</mark>
        System.out.println("Printed in Subclass");
        <mark>super.printMethod();</mark>
            
    }
        
    public static void main(String[] args) {
        
        Subclass s = new Subclass();
        s.printMethod();
    }
        
}</pre>
<pre class='out'>
run:
Printed in Subclass
Printed in Superclass.
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>
    Using <mark>super</mark> to invoke an overridden method <mark>only applies to instance
    methods</mark>. (Remember, <mark>static methods can't be overridden</mark>.)</p>
<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.supercall;
    
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class Dog2 extends Animal {
    
    public void eat() {
        /* no Exceptions */
        System.out.println("sub eat");
    }
        
    public static void main(String[] args) {
        Animal a = new Dog2();
        Dog2 d = new Dog2();
        d.eat(); // ok
            <mark>a.eat();</mark>
    }
        
}
    
class Animal {
    
    public void eat() <mark>throws FileNotFoundException</mark> {
        
    }
}
</pre>
<pre class='out'>
run:
sub eat
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - unreported exception java.io.FileNotFoundException; must be caught or declared to be thrown
	at com.mvivekweb.ocpjp6.obj5.sect4.supercall.Dog2.main(Dog2.java:18)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>



<p>If the supertype version
    declares a checked exception, but the overriding subtype method does not, the compiler
    still thinks you are calling a method that declares an exception</p>
<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.supercall;
    
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class Dog2 extends Animal {
    
    <mark>public void eat() {</mark>
        /* no Exceptions */
        System.out.println("sub eat");
    }
        
    public static void main(String[] args) <mark>throws FileNotFoundException</mark> {
        Animal a = new Dog2();
        Dog2 d = new Dog2();
        d.eat(); // ok
            a.eat();
    }
        
}
    
class Animal {
    
    public void eat() <mark>throws FileNotFoundException {</mark>
        
    }
}
</pre>
<pre class='out'>
run:
sub eat
sub eat
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<h3>Invoking Overloaded Methods</h3>

<p><mark>boxing</mark> and
    <mark>var-args</mark>-both of which have a <mark>huge impact on overloading</mark>.</p>

<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect4.overloadinvoke;
    
public class TestAdder {
    
    public static void main(String[] args) {
        Adder a = new Adder();
        int b = 27;
        int c = 3;
        int result = a.addThem(b, c); // Which addThem is invoked?
        double doubleResult = a.addThem(22.5, 9.3); // Which addThem?
        System.out.println("result=" + result);
        System.out.println("doubleResult=" + doubleResult);
    }
        
}
    
class Adder {
    
    <mark>public int addThem(int x, int y) {</mark>
        return x + y;
    }
// Overload the addThem method to add doubles instead of ints
    
  <mark>  public double addThem(double x, double y) {</mark>
        return x + y;
    }
}
    
    
</pre>
<pre class='out'>
run:
result=30
doubleResult=31.8
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>Invoking overloaded methods that <mark>take object references rather than primitives</mark> is
    a little more interesting</p>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.overloadinvoke;
    
public class UseAnimals {
    
    public void doStuff(Animal a) {
        System.out.println("In the Animal version");
    }
        
    public void doStuff(Horse h) {
        System.out.println("In the Horse version");
    }
        
    public static void main(String[] args) {
        UseAnimals ua = new UseAnimals();
        <mark>Animal animalObj = new Horse();</mark>
        Horse horseObj = new Horse();
        <mark>ua.doStuff(animalObj);</mark>
        ua.doStuff(horseObj);
    }
        
}
    
class Animal {
}
    
class Horse <mark>extends Animal</mark> {
}
    
</pre>
<pre class='out'>
run:
<mark>In the Animal version</mark>
In the Horse version
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<p>
    which <mark>overloaded version</mark> of the method to call is based on the <mark>reference type</mark> of
    the argument passed at <mark>compile time</mark>.</p>

<p>for overriding. which <mark>overridden version of the method to call</mark> (in other words, from
    which class in the inheritance tree) is <mark>decided at runtime</mark> based on <mark>object type</mark></p>


<h3>Overloaded Constructors</h3>
<pre>
class Foo {
Foo() { }
Foo(String s) { }
}
    
</pre>

<p>class has <mark>two overloaded constructors</mark>, one that takes a string,
    and one with no arguments.</p>

<p>Overloading a constructor is typically used to provide <mark>alternate ways for clients
    to instantiate objects of your class</mark>.</p>

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Animal1 {
    
    String name;
        
    Animal1(String name) {
        this.name = name;
    }
        
    Animal1() {
        this(makeRandomName());
    }
        
       String makeRandomName() {
        double v= Math.random() * 5;
        System.out.println("v="+v);
        int x = (int) (v);
        System.out.println("x="+x);
        String name = new String[]{"Fluffy", "Fido",
            "Rover", "Spike",
            "Gigi"}[x];
        return name;
    }
        
    public static void main(String[] args) {
        Animal1 a = new Animal1();
        System.out.println(a.name);
        Animal1 b = new Animal1("Zeus");
        System.out.println(b.name);
    }
}
    
    </pre>
    
<pre class='out'>    
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - cannot reference this before supertype constructor has been called
	at com.mvivekweb.ocpjp6.obj1.sect6.constructor.Animal1.<init>(Animal1.java:12)
	at com.mvivekweb.ocpjp6.obj1.sect6.constructor.Animal1.main(Animal1.java:27)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)    
</pre>
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Animal1 {
    
    String name;
        
    Animal1(String name) {
        this.name = name;
    }
        
    Animal1() {
        this(<mark>makeRandomName()</mark>);
    }
        
       <mark>static</mark> String makeRandomName() {
        double v= Math.random() * 5;
        System.out.println("v="+v);
        int x = (int) (v);
        System.out.println("x="+x);
        String name = new String[]{"Fluffy", "Fido",
            "Rover", "Spike",
            "Gigi"}[x];
        return name;
    }
        
    public static void main(String[] args) {
        Animal1 a = new Animal1();
        System.out.println(a.name);
        Animal1 b = new Animal1("Zeus");
        System.out.println(b.name);
    }
}
    
    </pre>
    
 <pre class='out'>   
 run:
v=1.6985034099243679
x=1
Fido
Zeus
BUILD SUCCESSFUL (total time: 1 second)   

    </pre>
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Animal1 {
    
    String name;
        
    Animal1(String name) {
        this.name = name;
    }
        
    Animal1() {
       <mark> this.makeRandomName();</mark>
    }
        
       String makeRandomName() {
        double v= Math.random() * 5;
        System.out.println("v="+v);
        int x = (int) (v);
        System.out.println("x="+x);
        String name = new String[]{"Fluffy", "Fido",
            "Rover", "Spike",
            "Gigi"}[x];
        return name;
    }
        
    public static void main(String[] args) {
        Animal1 a = new Animal1();
        System.out.println(a.name);
        Animal1 b = new Animal1("Zeus");
        System.out.println(b.name);
    }
}
    
    </pre>
  <pre class='out'>  
run:
v=0.5285514748605774
x=0
<mark>null</mark>
Zeus
BUILD SUCCESSFUL (total time: 1 second)
    
    </pre>

    <pre>
    
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Animal1 {
    
    String name;
        
    Animal1(String name) {
        <mark>System.out.println("Animal1 const stri");</mark>
        this.name = name;
    }
        
    Animal1() {
        <mark>this(makeRandomName());</mark>
    }
        
       static String makeRandomName() {
        double v= Math.random() * 5;
        System.out.println("v="+v);
        int x = (int) (v);
        System.out.println("x="+x);
        String name = new String[]{"Fluffy", "Fido",
            "Rover", "Spike",
            "Gigi"}[x];
        return name;
    }
        
    public static void main(String[] args) {
        Animal1 a = new Animal1();
        System.out.println(a.name);
        Animal1 b = new Animal1("Zeus");
        System.out.println(b.name);
    }
}
    
    </pre>
 <pre class='out'>   
run:
v=1.865653037274998
x=1
<mark>Animal1 const stri</mark>
Fido
Animal1 const stri
Zeus
BUILD SUCCESSFUL (total time: 1 second)    

    </pre> 
  <pre>      
 package com.mvivekweb.ocpjp6.obj1.sect6.constructor;

public class Animal1 {

    String name;

    Animal1(String name) {
        System.out.println("Animal1 const stri");
        <mark>name = name;</mark>
    }

    Animal1() {
        this(makeRandomName());
    }

       static String makeRandomName() {
        double v= Math.random() * 5;
        System.out.println("v="+v);
        int x = (int) (v);
        System.out.println("x="+x);
        String name = new String[]{"Fluffy", "Fido",
            "Rover", "Spike",
            "Gigi"}[x];
        return name;
    }

    public static void main(String[] args) {
        Animal1 a = new Animal1();
        System.out.println(a.name);
        Animal1 b = new Animal1("Zeus");
        System.out.println(b.name);
    }
}       
    </pre>
    
 <pre class='out'>   
run:
v=4.210981620137882
x=4
Animal1 const stri
<mark>null</mark>
Animal1 const stri
<mark>null</mark>
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
    
    
    <p>makeRandomName() method is marked <mark>static</mark>!
        That's because <mark>you cannot invoke an instance</mark> (in other words, nonstatic)
        <mark> method</mark> (or access an instance variable) <mark>until after the super constructor has
        run.</mark>   </p>
    
    <pre>    
---------------    
  4.Object()
----------------
3.Animal(String s) calls super()
--------------------------
2.Animal() calls this(randomlyChosenNameString)
-----------------------
1.main() calls new Animal() 
---------------------
    
    </pre>

 
    <p>Rather <mark>than calling
        super(), we're calling this()</mark>, and <mark>this() always means a call to another
        constructor</mark> in the same class. </p>  
    
    <p>The first line in a constructor must be <mark>a call to super() or a call to
        this()</mark>.  </p>  
    
    <p><mark>If you have neither of those calls</mark> in your constructor, the compiler
        will <mark>insert the no-arg call to super()</mark>.   </p> 
    
    <p><mark>if constructor A() has a call
        to this()</mark>, the compiler knows that <mark>constructor A() will not be the one to invoke
        super()</mark>.</p>    
    
        <p>a constructor can <mark>never have both a call to super()
            and a call to this()</mark>. </p>
        
        
        <p>you <mark>can't legally use this() and super() in the same constructor</mark>.</p>        
    
        <p>compiler will <mark>not put a call to super() in any constructor that has a call to this()</mark>.</p>
        <pre>     
 package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
     
public class A {
    
    A() {
        this("foo");
    }
        
    A(String s) {
        this();
    }
}       
        </pre> 
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/constructor/A.java
com\mvivekweb\ocpjp6\obj1\sect6\constructor\A.java:13: <mark>recursive constructor invocation</mark>
    A(String s) {
    ^
1 error

C:\vivek\docs\cert\ocpjp6\src>        
    </pre> 
        
        