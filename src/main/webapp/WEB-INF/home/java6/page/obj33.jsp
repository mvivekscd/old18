<h3>Develop code that serializes and/or de-serializes objects using the following APIs from
    java.io: DataInputStream, DataOutputStream, FileInputStream, FileOutputStream,
    ObjectInputStream, ObjectOutputStream, and Serializable.</h3>

<p><mark>save the state</mark> of one or more <mark>objects</mark></p>
<p>save this object and all of its instance variables</p>

<h3>Working with ObjectOutputStream and ObjectInputStream</h3>

<p>two methods: one to <mark>serialize objects</mark>
    and <mark>write</mark> them to a <mark>stream</mark>, and a <b>second</b> to <mark>read</mark> the stream and deserialize objects.</p>

<pre><mark>ObjectOutputStream</mark>.writeObject() // serialize and write
<mark>ObjectInputStream</mark>.<mark>readObject()</mark> // read and deserialize</pre>
<p>ObjectOutputStream and java.io.ObjectInputStream classes are
    considered to be <mark>higher-level classes</mark> in the java.io package</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
    
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
    
class <mark>Cat</mark> implements <mark>Serializable</mark> {
} // 1
    
public class SerializeCat {
    
    public static void main(String[] args) {
        Cat c = new Cat(); // 2
        try {
            <mark>FileOutputStream</mark> fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(<mark>fs</mark>);
            os.writeObject(c); // 3
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            <mark>FileInputStream</mark> fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(<mark>fis</mark>);
            c = (Cat) ois.readObject(); // 4
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
}
</pre>

<p>Serializable is a <mark>marker interface</mark>; it has <mark>no methods to implement</mark>.</p>

<h3>Object Graphs</h3>

<p>if you
    tried to <mark>restore the object</mark> in <mark>another instance of the JVM</mark>, even running on the same
    computer on which the object was originally serialized, the <mark>reference would be useless</mark></p>

<p>When you
    serialize an object, Java serialization takes care of saving that object's entire<mark> "object
        graph."</mark></p>
        <pre>       
 package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
     
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
    
class Dog {
    
    private Collar theCollar;
    private int dogSize;
        
    public Dog(Collar collar, int size) {
        theCollar = collar;
        dogSize = size;
    }
        
    public Collar getCollar() {
        return theCollar;
    }
}
    
class Collar {
    
    private int collarSize;
        
    public Collar(int size) {
        collarSize = size;
    }
        
    public int getCollarSize() {
        return collarSize;
    }
}
    
public class SerializeDog {
    
    public static void main(String[] args) {
        Collar c = new Collar(3);
        Dog d = new Dog(c, 8);
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
}
        </pre>
 <pre class='out'>       
 run:
java.io.NotSerializableException: com.mvivekweb.ocpjp6.obj3.sec2.serialization.Dog
	at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1184)
	at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:348)
	at com.mvivekweb.ocpjp6.obj3.sec2.serialization.SerializeDog.main(SerializeDog.java:42)
BUILD SUCCESSFUL (total time: 1 second)
 </pre>
        
        <pre>        package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
            
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
    
class Dog <mark>implements Serializable</mark>{
    
    private Collar theCollar;
    private int dogSize;
        
    public Dog(Collar collar, int size) {
        theCollar = collar;
        dogSize = size;
    }
        
    public Collar getCollar() {
        return theCollar;
    }
}
    
class Collar {
    
    private int collarSize;
        
    public Collar(int size) {
        collarSize = size;
    }
        
    public int getCollarSize() {
        return collarSize;
    }
}
    
public class SerializeDog {
    
    public static void main(String[] args) {
        Collar c = new Collar(3);
        Dog d = new Dog(c, 8);
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
}
        </pre>
<pre class='out'>        
run:
java.io.NotSerializableException: com.mvivekweb.ocpjp6.obj3.sec2.serialization.Collar
	at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1184)
	at java.io.ObjectOutputStream.defaultWriteFields(ObjectOutputStream.java:1548)
	at java.io.ObjectOutputStream.writeSerialData(ObjectOutputStream.java:1509)
	at java.io.ObjectOutputStream.writeOrdinaryObject(ObjectOutputStream.java:1432)
	at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1178)
	at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:348)
	at com.mvivekweb.ocpjp6.obj3.sec2.serialization.SerializeDog.main(SerializeDog.java:43)
        BUILD SUCCESSFUL (total time: 1 second)   </pre>     
        
        <p>if we <mark>didn't have access</mark> to the <mark>Collar class</mark> source code  </p>      
        
        <p>we could subclass the Collar class, mark the subclass as Serializable,
            and then use the Collar subclass instead of the Collar class. <mark>But that's not always an
                option </mark>   </p>    
        
        <p>If you mark the Dog's <mark>Collar
                instance variable</mark> with <mark>transient</mark>, then serialization will simply skip the Collar
            during serialization:  </p>      
        <pre>       
 package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
     
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
    
class Dog implements Serializable {
    
    private <mark>transient</mark> Collar theCollar;
    private int dogSize;
        
    public Dog(Collar collar, int size) {
        theCollar = collar;
        dogSize = size;
    }
        
    public Collar getCollar() {
        return theCollar;
    }
}
    
class Collar {
    
    private int collarSize;
        
    public Collar(int size) {
        collarSize = size;
    }
        
    public int getCollarSize() {
        return collarSize;
    }
}
    
public class SerializeDog {
    
    public static void main(String[] args) throws FileNotFoundException {
        Collar c = new Collar(3);
        Dog d = new Dog(c, 5);
        System.out.println("before: collar size is "
                + d.getCollar().getCollarSize());
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
                
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            d = (Dog) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        System.out.println(
                "after: collar size is "
                + d.getCollar().getCollarSize());
    }
}
        </pre>
<pre class='out'>
run:
before: collar size is 3
Exception in thread "main" java.lang.NullPointerException
	at com.mvivekweb.ocpjp6.obj3.sec2.serialization.SerializeDog.main(SerializeDog.java:66)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 1 second)</pre>
        
        <h3>Using writeObject and readObject</h3>

        <p>Java serialization has a special mechanism just for this-a <mark>set of private methods</mark>
            you can <mark>implement in your class</mark> that, <mark>if present</mark>, will be <mark>invoked automatically
            during serialization and deserialization</mark>. </p>     
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
    
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
    
class Dog implements Serializable {
    
    private <mark>transient</mark> Collar theCollar;
    private int dogSize;
        
    public Dog(Collar collar, int size) {
        theCollar = collar;
        dogSize = size;
    }
        
    public Collar getCollar() {
        return theCollar;
    }
        
    <mark>private void writeObject(ObjectOutputStream os) {
// throws IOException { // 1
        try {
            os.defaultWriteObject(); // 2
            os.writeInt(theCollar.getCollarSize()); // 3
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
    private void readObject(ObjectInputStream is) {
// throws IOException, ClassNotFoundException { // 4
        try {
            is.defaultReadObject(); // 5
            theCollar = new Collar(is.readInt()); // 6
        } catch (Exception e) {
            e.printStackTrace();
        }
    }</mark>
}
    
class Collar {
    
    private int collarSize;
        
    public Collar(int size) {
        collarSize = size;
    }
        
    public int getCollarSize() {
        return collarSize;
    }
}
    
public class SerializeDog {
    
    public static void main(String[] args) throws FileNotFoundException {
        Collar c = new Collar(3);
        Dog d = new Dog(c, 5);
        System.out.println("before: collar size is "
                + d.getCollar().getCollarSize());
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
                
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            d = (Dog) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        System.out.println(
                "after: collar size is "
                + d.getCollar().getCollarSize());
    }
}
        </pre>
<pre class='out'>        
run:
before: collar size is 3
after: collar size is 3
BUILD SUCCESSFUL (total time: 1 second) </pre>       
        
        <p>If anyone invokes <mark>writeObject() or
                readObject()</mark> concerning a Dog object, use this code as part of the read and write  </p>      

        
        <p>invoke <mark>defaultWriteObject()</mark> from within writeObject()
            you're telling the JVM to do the normal serialization process for this object </p>

        <p>You can write extra stuff <mark>before and/or
                after</mark> you invoke defaultWriteObject(). BUT...<mark>when you read it back</mark> in,
            you have to <mark>read the extra stuff in the same order you wrote it</mark>.    </p>    

        <p>We had to invoke <mark>readInt()</mark> after we invoked
            <mark>defaultReadObject()</mark> or the streamed data would be out of sync</p>        
 
        <p>when you want <mark>to do only a part of the serialization/deserialization yourself</mark>, <mark>you
                MUST invoke the defaultReadObject() and defaultWriteObject() methods</mark>
            to do the rest    </p>  

        <p>Things like <mark>streams, threads, runtime,
                etc. and even some GUI classes</mark> (which are connected to the underlying OS) cannot
            be serialized   </p>     
        
        <h3> How Inheritance Affects Serialization </h3>      
        
        <p>have to
            understand how your <mark>class's superclasses</mark> affect serialization   </p>     

        <p>If a superclass is Serializable, then according to normal Java interface
            rules, <mark>all subclasses of that class</mark> <mark>automatically</mark> <mark>implement Serializable</mark> implicitly</p>  
        <p>when an object
            is constructed using new (as opposed to being deserialized), the following things
            happen (in this order):  </p>
        
        <br> 1. <mark>All instance variables</mark> are assigned <mark>default values</mark>.
<br>2. The <mark>constructor is invoked</mark>, which immediately <mark>invokes the superclass
    constructor</mark> (or another overloaded constructor, until one of the overloaded
constructors invokes the superclass constructor).
<br>3. All <mark>superclass constructors complete</mark>.
<br>4. <mark>Instance variables that are initialized</mark> as part of their declaration are assigned
their initial value (as opposed to the default values they're given prior to
the superclass constructors completing).
<br>5. The <mark>constructor completes</mark>.       

<p>But <mark>these things do NOT happen when an object is deserialized</mark> </p>  

<p>the constructor does not run, and instance variables
    are NOT given their initially assigned values</p>

<p>if you have variables marked <mark>transient</mark>, they will not be restored to
    their original state (unless you implement readObject()), but will instead be given
    the default value for that data type</p>

<pre>class Bar implements Serializable {
    <mark>transient</mark> int x = <mark>42</mark>;
}</pre>

<p>when the Bar instance is deserialized, the variable x will be set to a value of <mark>0</mark>.</p>

<p><mark>Object references</mark> marked <mark>transient</mark> will always be reset to <mark>null</mark></p>

<p>It gets a little <mark>trickier</mark> when the <mark>serializable class</mark> has <mark>one or more
        non-serializable superclasses</mark></p>
        
        <p>For the exam, you'll need to be able to<mark> recognize which variables will and will not
                be restored</mark> with the appropriate values when an object is deserialized </p>  
        <pre>
package com.mvivekweb.ocpjp6.obj3.sec2.serialization;
    
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
    
public class SuperNotSerial {
    
    public static void main(String[] args) {
        Dog1 d = new Dog1(35, "Fido");
        System.out.println("before: " + d.name + " "
                + d.weight);
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            d = (Dog1) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("after: " + d.name + " "
                + d.weight);
    }
}
    
    
    
    
class Dog1 <mark>extends Animal implements Serializable</mark> {
    
    String name;
        
    Dog1(int w, String n) {
        weight = w; // inherited
        name = n; // not inherited
    }
}
    
<mark>class Animal { // not serializable !</mark>
    
    int weight = 42;
}
        </pre>
<pre class='out'>        
run:
before: Fido 35
after: Fido 42
BUILD SUCCESSFUL (total time: 1 second)
</pre>  
        
        <p> If you serialize a <mark>collection</mark> or <mark>an array</mark>, <mark>every element must be
                serializable</mark>! A single <mark>non-serializable element will cause serialization to fail</mark>.   </p>

        <p>collection interfaces are <mark>not serializable</mark>, the <mark>concrete collection classes</mark> in
            the Java API are. </p> 
        
        <h3>Serialization Is Not for Statics</h3>
        
        <p>think of <mark>static variables</mark> purely as <mark>CLASS variables</mark> </p> 
        
        <p>serialization <mark>applies</mark> only to <mark>OBJECTS</mark></p>

        <p>As simple as serialization code is to write, <mark>versioning problems</mark> can occur
            in the real world. If you <mark>save a Dog object using one version of the class</mark>,
            but attempt to deserialize it using a newer, different version of the class,
            deserialization might fail</p>        