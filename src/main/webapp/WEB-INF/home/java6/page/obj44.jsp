<h3> Given a scenario, write code that makes appropriate use of wait, notify. or notifyAll.</h3>
        
        
        <p>how threads can <mark>interact with one another</mark>
            to communicate about-among other things-their locking status</p>
        
        <p>The Object
            class has three methods, <mark>wait(), notify(), and notifyAll()</mark> that help threads
            communicate about the <mark>status</mark> of an event that the threads care about</p>
        
        
        <p>Using the wait and notify mechanism, the mail-processor thread could check for
            mail</p>
        
        
        <p><mark>wait(), notify(), and notifyAll()</mark> <mark>must be called</mark> from within a <mark>synchronized
            context</mark>! A thread can't invoke a wait or notify method on an object unless it owns
            that object's lock.</p>
        
        
        <p>A simple solution is to <mark>separate the processes into two different threads</mark>, one of
            them interacting with the user and another managing the hardware.    </p>
        
        
        <p>Both <mark>threads use a common
            object to communicate</mark>, which holds the current design being processed   </p>    
        
        <p>The solution is to <mark>use wait() and notify()</mark>,
            and also to <mark>synchronize some of the code</mark>. </p>      
        
        <p>In the same way that <mark>every object has a lock</mark>, every <mark>object</mark> can have a <mark>list of threads
            that are waiting for a signal</mark> (a notification) from the object  </p>
        

        <p>A thread <mark>gets on
            this waiting list</mark> by <mark>executing the wait() method</mark> of the target object </p>       
        
        <p>From that
            moment, it <mark>doesn't execute</mark> any further instructions <mark>until the notify() method</mark> of
            the target object is called </p>      
        <pre>      
 package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
     
class ThreadA {
    
    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();       <mark>//one start</mark>                          
        System.out.println("1 main");
        <mark>synchronized (b) {</mark>
            System.out.println("2 main sync start");
            try {
                System.out.println("Waiting for b to complete...");
                <mark>b.wait();</mark>
                System.out.println("Waiting for b over...");
                    
            } catch (InterruptedException e) {
            }
            System.out.println("Total is: " + b.total);  <mark>//one end</mark>
        }
    }
}
    
class ThreadB extends Thread {
    
    int total;
        
    public void run() {
        System.out.println("1 threadb run");
        <mark>synchronized (this) {</mark>
             System.out.println("2 threadb run sync start");
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            System.out.println("3 threadb run");
            <mark>notify();</mark>
        }
    }
}       
    
        </pre>
 <pre class='out'>       
run:
1 main
<mark>2 main sync start
1 threadb run</mark>
Waiting for b to complete...
2 threadb run sync start
3 threadb run
Waiting for b over...
Total is: 4950
BUILD SUCCESSFUL (total time: 0 seconds)        
    </pre> 
   
        <p>or</p>
        
 <pre class='out'>       
run:
1 main
<mark>1 threadb run
2 main sync start</mark>
Waiting for b to complete...
2 threadb run sync start
3 threadb run
Waiting for b over...
Total is: 4950
BUILD SUCCESSFUL (total time: 1 second)        
    </pre> 
        
<img src="/imag/jp6/threadw.png" class="imgw">
<img src="/imag/jp6/threadw1.png" class="imgw">
<img src="/imag/jp6/threadw2.png" class="imgw">
        <p>if <mark>wait was not there</mark></p>
        
        
        <pre>
 package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
     
class ThreadA {
    
    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();
        System.out.println("1 main");
        synchronized (b) {
            System.out.println("2 main sync start");
           // try {
                System.out.println("Waiting for b to complete...");
              <mark> // b.wait();</mark>
                System.out.println("Waiting for b over...");
                    
           // } catch (InterruptedException e) {
           // }
            System.out.println("Total is: " + b.total);
        }
    }
}
    
class ThreadB extends Thread {
    
    int total;
        
    public void run() {
        System.out.println("1 threadb run");
        synchronized (this) {
             System.out.println("2 threadb run sync start");
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            System.out.println("3 threadb run");
            notify(); <mark>// no effect</mark>
        }
    }
}
        </pre>
<pre class='out'>        
run:
1 main
1 threadb run
<mark>2 main sync start
Waiting for b to complete...
Waiting for b over...
Total is: 0</mark>
2 threadb run sync start
3 threadb run
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>  
        
        <p>ThreadA will continue
            with the next line of code in its own class, which means it could get to line <mark>//one end</mark>
            before ThreadB has finished the calculation.</p>       
        
        <p> the <mark>code synchronizes</mark> itself with the <mark>object b</mark>-this is because <mark>in
            order to call wait()</mark> on the object  </p>     
        <pre>        
synchronized(anotherObject) { // this has the lock on anotherObject
try {
anotherObject.wait();
<mark>// the thread releases the lock and waits
// To continue, the thread needs the lock,
// so it may be blocked until it gets it.</mark>
} catch(InterruptedException e){}
}        
        </pre> 
        
        <p>The preceding code waits until notify() is called on anotherObject.</p>
        
        <pre> synchronized(this) { <mark>notify();</mark> }      </pre>
        
        <p><mark>notifies a single thread</mark> currently <mark>waiting</mark> on the <mark>this object</mark>.  </p>   
        
        <p> if not synchronized.notify has runtimeexception.This exception is not a checked exception,so you don't have to catch it explicitly </p>
        
        <pre>
  package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
      
class ThreadA {
    
    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();
        System.out.println("1 main");
        synchronized (b) {
            System.out.println("2 main sync start");
            try {
                System.out.println("Waiting for b to complete...");
                b.wait();
                System.out.println("Waiting for b over...");
                    
            } catch (InterruptedException e) {
            }
            System.out.println("Total is: " + b.total);
        }
    }
}
    
class ThreadB extends Thread {
    
    int total;
        
    public void run() {
        System.out.println("1 threadb run");
        //synchronized (this) {
             System.out.println("2 threadb run sync start");
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            System.out.println("3 threadb run");
            <mark>notify();</mark>
        //}
    }
}      
    
        </pre>
        
 <pre class='out'> 
run:
1 main
1 threadb run
2 main sync start
Waiting for b to complete...
2 threadb run sync start
3 threadb run
<mark>Exception in thread "Thread-2" java.lang.IllegalMonitorStateException</mark>
Waiting for b over...
<mark>Total is: 4950</mark>
	at com.mvivekweb.ocpjp6.obj4.sect4.threadnotification.ThreadB.run(ThreadA.java:35)
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>
        
       
        <p>You should always be clear <mark>whether a thread
            has the lock of an object</mark> in any given block of code.  </p>
        <p>      
            There is also a second form of wait() that <mark>accepts a number of milliseconds</mark>
            as a maximum time to wait       
    
        </p> 
   <pre class='out'>     
synchronized(a){ // The thread gets the lock on 'a'
    a.wait(2000); // Thread releases the lock and waits for notify
    // <mark>only for a maximum of two seconds, then goes back to Runnable
    // The thread reacquires the lock</mark>
    // More instructions here
}        
    </pre> 
    
        <p>So just because <mark>notify() is called doesn't mean the lock becomes
            available at that moment</mark>. </p>
        
        <p>However, when notify() is called,
            that doesn't mean the thread gives up its lock at that moment. If the thread is<mark> still
            completing synchronized code, the lock is not released until the thread moves out of
            synchronized code</mark>.</p>
        
            <pre>
 package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
     
class ThreadA {
    
    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();
        System.out.println("1 main");
        synchronized (b) {
            System.out.println("2 main sync start");
            try {
                System.out.println("Waiting for b to complete...");
                b.wait(2000);
                System.out.println("Waiting for b over...");
                    
            } catch (InterruptedException e) {
            }
            System.out.println("Total is: " + b.total);
        }
    }
}
    
class ThreadB extends Thread {
    
    int total;
        
    public void run() {
        System.out.println("1 threadb run");
        synchronized (this) {
             System.out.println("2 threadb run sync start");
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            System.out.println("3 threadb run");
           <mark> notify();
               
            total = total+ 100000;</mark>
                
        }
    }
}           
    
            </pre>
            
  <pre class='out'>          
 run:
1 main
2 main sync start
1 threadb run
Waiting for b to complete...
2 threadb run sync start
3 threadb run
Waiting for b over...
<mark>Total is: 104950</mark>
BUILD SUCCESSFUL (total time: 1 second)
    </pre>   
            
            <h3>Using notifyAll( ) When Many Threads May Be Waiting  </h3>
            
 
            <p>it's preferable to notify all of the threads that are waiting on a
                particular object. </p>
            
            <p>you can use notifyAll() on the object to let all the threads
                <mark> rush out of the waiting area and back to runnable</mark>  </p>
            
            
            <p><mark>All of the threads will be notified</mark> and <mark>start competing</mark> to get the lock. As the lock
                is used and released by each thread, all of them will get into action without a need
                for further notification.</p>         
            
            
            <p>Which <mark>one, exactly</mark>, is not specified and
                <mark>depends on the JVM implementation</mark>, so you should never rely on a particular
                thread being notified in preference to another. </p>        
            
            
            <p>In cases in which there might be a lot more waiting, the best way to do this is by
                using <mark>notifyAll()</mark>     </p>
            
            <p>In this example, there is
                one class that performs a calculation and many readers that are waiting to receive
                the completed calculation.  </p>
            <pre>          
 package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
     
class Reader extends Thread{
    
    Calculator c;
        
    public Reader(Calculator calc) {
        c = calc;
    }
        
    public void run() {
        synchronized (c) {
            try {
                System.out.println("Waiting for calculation...");
                c.wait();
                System.out.println("Waiting done...");
            } catch (InterruptedException e) {
            }
            System.out.println("Total is: " + c.total);
        }
    }
        
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        new Reader(calculator).start();
        new Reader(calculator).start();
        new Reader(calculator).start();
        calculator.start();
    }
}
    
class Calculator extends Thread {
    
    int total;
        
    public void run() {
        synchronized (this) {
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            <mark>notifyAll();</mark>
        }
    }
}           
            </pre>
 <pre class='out'>           
run:
Waiting for calculation...
Waiting for calculation...
Waiting for calculation...
<mark>Waiting done...
Total is: 4950   </mark>         
    </pre> 
            
            <p>  or</p>
   <pre class='out'>         
 run:
Waiting for calculation...
Waiting for calculation...
Waiting for calculation...
Waiting done...
Total is: 4950
Waiting done...
Total is: 4950
Waiting done...
Total is: 4950
BUILD SUCCESSFUL (total time: 1 second)
    </pre> 
            
            <pre>         
 package com.mvivekweb.ocpjp6.obj4.sect4.threadnotification;
     
class Reader extends Thread{
    
    Calculator c;
        
    public Reader(Calculator calc) {
        c = calc;
    }
        
    public void run() {
        synchronized (c) {
            try {
                System.out.println("Waiting for calculation...");
                c.wait();
                System.out.println("Waiting done...");
            } catch (InterruptedException e) {
            }
            System.out.println("Total is: " + c.total);
        }
    }
        
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        new Reader(calculator).start();
        //System.out.println(" main cal reader start...");
        new Reader(calculator).start();
       // System.out.println(" main cal reader start...");
        new Reader(calculator).start();
       // System.out.println(" main cal reader start...");
        System.out.println(" main cal start...");
        calculator.start();
        System.out.println(" main cal end...");
    }
}
    
class Calculator extends Thread {
    
    int total;
        
    public void run() {
        synchronized (this) {
            System.out.println(" calculation start...");
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            System.out.println(" calculation end1...");
            notifyAll();
             System.out.println(" calculation end2...");
        }
    }
}           
            </pre>
  <pre class='out'>          
 run:
Waiting for calculation...
Waiting for calculation...
 main cal start...
 main cal end...
Waiting for calculation...
<mark> calculation start...
 calculation end1...
 calculation end2...</mark>
Waiting done...
Total is: 4950
Waiting done...
Total is: 4950
Waiting done...
Total is: 4950
BUILD SUCCESSFUL (total time: 1 second)           
    </pre>
            
<img src="/imag/jp6/threadwna.png" class="imgw">
<img src="/imag/jp6/threadwna1.png" class="imgw">   
<img src="/imag/jp6/threadwna2.png" class="imgw">   
<img src="/imag/jp6/threadwna3.png" class="imgw">   
<img src="/imag/jp6/threadwna4.png" class="imgw">   
            
            
            <p>            
                that if the run() method at line 30 used notify() instead of notifyAll(), <mark>only
                one reader would be notified</mark> instead of all the readers.    </p>     
            
            <h3>Using wait( ) in a Loop   </h3>
            
            
            <p>Unfortunately, when the Readers run, they just start waiting
                right away. They don't do anything to see if the event they're waiting for has already
                happened. <mark>So if the Calculator has already called notifyAll(), it's not going to
                call notifyAll() again-and the waiting Readers will keep waiting forever. </mark>     </p>
            
            
            <p>Almost always, when
                you want to wait for something, you also <mark>need to be able to check if it has already
                happened</mark>.</p>
            
                <p>Generally the best way to solve this is to put in some sort of loop that
                    checks on some sort of conditional expressions, and <mark>only waits if the thing you're
                    waiting for has not yet happened</mark>.   </p>        
            
            
                <p>However, it's a good idea to require the thread to
                    recheck the <mark>isEmpty()</mark> condition whenever it's been woken up, because it's possible
                    that a thread has accidentally sent an extra <mark>notify()</mark> that was not intended.   </p>
                
                
  
                <p>There's also a possible situation called <mark>spontaneous wakeup</mark> that may exist in some
                    situations-a thread may wake up even though no code has called notify()
                    or notifyAll().  </p>           
                
                
                
                <p>Sometimes the JVM may call notify() for reasons of its own, or code in some other
                    class calls it for reasons you just don't know.) What this means is, when your thread
                    wakes up from a wait(), you don't know for sure why it was awakened.   </p>
                
                
                <p>The moral here is that <mark>when you use wait() and notify() or notifyAll(),</mark>
                    you <mark>should almost always also have a while loop around the wait()</mark> that checks a
                    condition and forces continued waiting until the condition is met. </p>
                
                
                <p>Of the key methods in Thread, be sure you know which are static-
                    <mark>sleep() and yield()</mark>, and which are <mark>not static</mark>-<mark>join() and start()</mark>. </p>      
                
                
                
