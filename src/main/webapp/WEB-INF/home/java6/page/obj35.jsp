 <h3>Write code that uses standard J2SE APIs in the java.util and java.util.regex packages to
            format or parse strings or streams. For strings, write code that uses the Pattern and Matcher
            classes and the String.split method. Recognize and use regular expression patterns for matching
            (limited to: .(dot), *(star), +(plus), ?, \d, \s, \w, [ ], () ). The use of *, + , and ? will
            be limited to greedy quantifiers, and the parenthesis operator will only be used as a grouping
            mechanism, not for capturing content during matching. For streams, write code using the
            Formatter and Scanner classes and the PrintWriter.format/printf methods. Recognize and use
            formatting parameters (limited to: %b, %c, %d, %f, %s) in format strings.</h3>
        
        <p> three basic ideas:</p>
        
        <br>        1.Finding stuff
    
        <br>        2. Tokenizing stuff
        <br>        3. Formatting stuff
    
        <h3>   A Search Tutorial</h3>
        
        <p>Regular expressions (regex for short) are a kind of language within a language,
            designed to help programmers with these <mark>searching tasks</mark>.</p>
        
        <p>Every language that
            provides regex capabilities uses one or more regex engines. Regex engines search
            through textual data using instructions that are coded into expressions.</p>
        
        <h3>Simple Searches</h3>
        <pre>        
        package com.mvivekweb.ocpjp6.obj3.sec5.regex;
            
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
public class RegexSmall {
    
    public static void main(String[] args) {
        Pattern p = <mark>Pattern.compile</mark>("ab"); // the expression
        Matcher m = <mark>p.matcher</mark>("abaaaba"); // the source
        while (<mark>m.find()</mark>) {
            System.out.print(m.start() + " ");
        }
    }
}
        </pre>
<pre class='out'>        
run:
0 4 BUILD SUCCESSFUL (total time: 1 second)        
    </pre>     
        
        
        <pre>
    
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class RegexSmall1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("aba"); // the expression
        Matcher m = p.matcher("abababa"); // the source
                             <mark>//01234567</mark>
                                 
        while (m.find()) {
            System.out.print(<mark>m.start()</mark> + " ");
        }
    }
        
}
        </pre>  
        
        <pre class='out'>        
run:
0 4 BUILD SUCCESSFUL (total time: 1 second)        
        </pre>     

        <p>In general, a regex search runs from left to right, and once a <mark>source's character has
            been used in a match, it cannot be reused</mark>. </p>      

        <h3>Searches Using Metacharacters  </h3>      
        

        <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class NumericSearch {
    
    
    public static void main(String[] args) {
        
        Pattern p = Pattern.compile("<mark>\d</mark>"); // the expression
        Matcher m = p.matcher("a12c3e456f"); // the source
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
    }
        
}
        </pre>
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec5/regex/NumericSearch.java
com\mvivekweb\ocpjp6\obj3\sec5\regex\NumericSearch.java:14: illegal escape character
        Pattern p = Pattern.compile("\d"); // the expression
                                      ^
1 error

C:\vivek\docs\cert\ocpjp6\src>        
        
    </pre>
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class NumericSearch {
    
    
    public static void main(String[] args) {
        
        Pattern p = Pattern.compile("<mark>\\d</mark>"); // the expression
        Matcher m = p.matcher("a12c3e456f"); // the source
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
    }
        
}
        </pre>   
<pre class='out'>        
run:
1 2 4 6 7 8 BUILD SUCCESSFUL (total time: 1 second)        
    </pre>     
        <pre> 
need for the exam       
\d A <mark>digit</mark>
\s A white<mark>space</mark> character
\w A <mark>word</mark> character (<mark>letters, digits, or "_" (underscore)</mark>) 
        </pre>
  
        <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class WordSearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>\\w</mark>"); // the expression
        Matcher m = p.matcher("a 1 56 _Z"); // the source
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
    }
        
}
        </pre>      
<pre class='out'>
run:
0 2 4 5 7 8 BUILD SUCCESSFUL (total time: 1 second)        
    </pre>  
        
        
        <p>You can also specify sets of characters to search for using square brackets and
            ranges of characters to search for using square brackets and a dash:</p>
        
        <pre>[<mark>abc</mark>] Searches only for a's, b's or c's
[<mark>a-f</mark>] Searches only for a, b, c, d, e, or f characters        
        </pre>
        
        
        <p> you can search across several ranges at once  </p>
        
        <pre><mark>[a-fA-F]</mark> Searches for the first six letters of the alphabet, both cases.      </pre>  

        
        <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class RangeSearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>[a-cA-C]</mark>"); // the expression
        Matcher m = p.matcher("cafeBABE"); // the source
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
    }
        
}
        </pre>
 <pre class='out'>       
run:
0 1 4 5 6 BUILD SUCCESSFUL (total time: 1 second)
    </pre> 
        
        
        <p><mark>"^"</mark> to
            negate the characters specified, <mark>nested brackets</mark> to create a union of sets,
            and <mark>"&&"</mark> to specify the intersection of sets. While these constructs are not
            on the exam     </p>   
 
        <h3>Searches Using Quantifiers </h3>
        
        
        <pre> 
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class HexaSearch {
    
    
    public static void main(String[] args) {
         Pattern p = Pattern.compile("<mark>0[xX][0-9a-fA-F]</mark>"); // the expression
        Matcher m = p.matcher("12 0x 0x12 0Xf 0xg"); // the source
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
    }
        
}
        </pre>   
 <pre class='out'>       
 run:
6 11 BUILD SUCCESSFUL (total time: 1 second)       
    </pre>     
        
        <p>the quantifier that represents "<mark>one or more</mark>" is the <mark>"+"</mark> character . remember +1  </p>     
        
        <p>for each return we're going to get back <mark>the starting
            position AND then the group</mark></p>
            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class GroupSearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>\\d+</mark>"); // the expression
        Matcher m = p.matcher("1 a12 234b"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ <mark>m.group()</mark>+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>            
run:
0 1
3 12
6 234
BUILD SUCCESSFUL (total time: 1 second)
    </pre>        
            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class HexaSearch1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("0[xX]<mark>([0-9a-fA-F])+</mark>"); // the expression
        Matcher m = p.matcher("12 0x 0x12 0Xf 0xg"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>
 <pre class='out'>           
run:
6 0x12
11 0Xf
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>        

            <p>The other two quantifiers we're going to look at are    </p>

            <pre>* Zero or more occurrences
? Zero or one occurrence       </pre>    
            

            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class ZeroMoreSearch {
    
    
    public static void main(String[] args) {
         Pattern p = Pattern.compile("proj1<mark>([^,])*</mark>"); // the expression
        Matcher m = p.matcher("proj3.txt,proj1sched.pdf,proj1,proj2,proj1.java"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>            
run:
10 proj1sched.pdf
25 proj1
37 proj1.java
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>   
            
            <p> give me zero or more characters that aren't
                a comma.   </p>        
 
            <pre>

"valid" phone numbers:
<mark>1234567
123 4567
123-4567</mark>

package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class ZeroOneSearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>\\d\\d\\d([-\\s])?\\d\\d\\d\\d</mark>"); // the expression
        Matcher m = p.matcher("1234-4567"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
            
                Pattern p1 = Pattern.compile("<mark>\\d{3,3}([-\\s])?\\d\\d\\d\\d</mark>"); // the expression
        Matcher m1 = p1.matcher("1324-45679"); // the source
        while (m1.find()) {
            System.out.print(m1.start() + " "+ m1.group()+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>            
run:
1 234-4567
1 324-4567
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>    
            
            <h3>The Predefined Dot    </h3>
            
            <p>it means <mark>"any character can serve here."</mark>  </p>          
            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class DotSearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("a<mark>.</mark>c"); // the expression
        Matcher m = p.matcher("ac abc a c"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>            
run:
3 abc
7 a c
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>  
            
            <h3>Greedy Quantifiers </h3>           

            <p>you use the <mark>*, +, and ?</mark> quantifiers, you can fine tune them a bit to produce
                behavior that's known as <mark>"greedy", "reluctant", or "possessive"</mark>  </p>          
            <pre>
? is greedy, ?<mark>?</mark> is reluctant, for zero or once
* is greedy, *<mark>?</mark> is reluctant, for zero or more
+ is greedy, +<mark>?</mark> is reluctant, for one or more            
            </pre>    
            

            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class GreedySearch {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile(".<mark>*</mark>xx"); // the expression
        Matcher m = p.matcher("yyxxxyxx"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>            
run:
0 yyxxxyxx
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>      
            
            
            <p>the regex engine would have to look <mark>(greedily) at the
                entire source data</mark> before it could determine that there was an xx at the end </p>  
            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class ReluctantSearch {
    
    
    public static void main(String[] args) {
         Pattern p = Pattern.compile(".<mark>*?</mark>xx"); // the expression
        Matcher m = p.matcher("yyxxxyxx"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>         
<pre class='out'>
run:
0 yyxx
4 xyxx
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>         

            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class ReluctantSearch {
    
    
    public static void main(String[] args) {
         Pattern p = Pattern.compile(".<mark>??</mark>xx"); // the expression
        Matcher m = p.matcher("yyxxxyxx"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>        
<pre class='out'>            
run:
1 yxx
5 yxx
BUILD SUCCESSFUL (total time: 1 second)
    </pre>          
 
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class ReluctantSearch {
    
    
    public static void main(String[] args) {
         Pattern p = Pattern.compile(".+?xx"); // the expression
        Matcher m = p.matcher("yyxxxyxx"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>     
<pre class='out'>            
run:
0 yyxx
4 xyxx
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>    
            
            
            <p>  <mark>.++xx</mark> does not return </p>  
            
            <p>The greedy quantifier does in fact read the entire source data, and then <mark>it works
                backward (from the right) </mark>until it finds the rightmost match </p>           
            
            <h3>When Metacharacters and Strings Collide </h3> 
 
            <p>The problem is that metacharacters and Strings
                don't mix too well </p>          
            
            <pre> 
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class DotSearch1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>.</mark>"); // the expression
        Matcher m = p.matcher("ac abc .a c"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>       
 <pre class='out'>
 run:
0 a
1 c
2  
3 a
4 b
5 c
6  
7 .
8 a
9  
10 c
BUILD SUCCESSFUL (total time: 1 second)           
    </pre> 
            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class DotSearch2 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>\\.</mark>"); // the expression
        Matcher m = p.matcher("ac abc .a c"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+"\n");
        }
    }
        
}
            </pre>
<pre class='out'>
run:
7 .
BUILD SUCCESSFUL (total time: 1 second)
    </pre>         
            
            <p>Don't
                worry, you won't see any command-line metacharacters on the exam! </p>    
            
            <h3>Locating Data via Pattern Matching  </h3>          
            
            <p>The <mark>Pattern
                class</mark> is used to hold a representation of a <mark>regex expression</mark>, so that it can be used
                and reused by instances of the Matcher class.  </p> 
            
            <p>The Matcher class is <mark>used to invoke
                the regex engine</mark> with the intention of performing match operations    </p>        
            <pre> 
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
public class Regex {
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile(args[0]);
        Matcher m = p.matcher(args[1]);
        System.out.println("Pattern is " + m.pattern());
        while (m.find()) {
            System.out.println(m.start() + " " + m.group());
        }
    }
}
            </pre>         
   
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec5/regex/Regex.java

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/regex/Regex <mark>"\d\w" "ab4 56_7ab"</mark>
Pattern is \d\w
4 56
7 7a

C:\vivek\docs\cert\ocpjp6\src>            
    </pre>
            
            <p><mark>find()</mark> method 
                that actually cranks up the regex engine and does some searching</p>            
            <p>To provide the most flexibility, Matcher.find(), when coupled with the
                greedy quantifiers <mark>? or *</mark>, allow for (somewhat unintuitively) the idea of a <mark>zero-length
                match.</mark>  </p>          
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class Regex1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>a?</mark>"); // the expression
        Matcher m = p.matcher("aba"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+" "+ <mark>m.end()</mark>+"\n");
        }
    }
        
}
    
            </pre>       

 <pre class='out'>           
run:
0 a 1
1<mark>  </mark>1
2 a 3
3<mark>  </mark>3
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>
            
            <p>The lines of output 1 1 and 3 3 are examples of <mark>zero-length matches</mark>.  </p>          
            <p> <mark>Zero-length matches</mark> can occur in several places:</p>
            <br>1. After the last character of source data (the 3 3 example)
            <br>            2 In between characters after a match has been found (the 1 1 example)
            <br>            3 At the beginning of source data (try java Regex "a?" "baba")
            <br>            4 At the beginning of zero-length source data     
            
            
            <pre> 
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class Regex1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>b?</mark>"); // the expression
        Matcher m = p.matcher("aba"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+" "+ m.end()+"\n");
        }
    }
        
}
            </pre>   
            
<pre class='out'>            
run:
0  0
1 b 2
2  2
3  3
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>  
            

            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
    
public class Regex1 {
    
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile("<mark>c?</mark>"); // the expression
        Matcher m = p.matcher("aba"); // the source
        while (m.find()) {
            System.out.print(m.start() + " "+ m.group()+" "+ m.end()+"\n");
        }
    }
        
}
            </pre>          
<pre class='out'>
run:
0  0
1  1
2  2
3  3
BUILD SUCCESSFUL (total time: 3 seconds)            
    </pre>      

            <p>A common reason to use regex is to <mark>perform search and replace</mark>
                operations.  </p>          
            
            <p>See the <mark>appendReplacement(), appendTail(), and
                replaceAll()</mark> methods in the Matcher API for more details.   </p> 
            
            <p>The Matcher class allows you to look at subsets of your source data by using a
                concept called <mark>regions</mark>. In real life, <mark>regions can greatly improve performance </mark></p>           
            
            <h3>Searching Using the Scanner Class</h3>
            
            <p>the java.util.Scanner
                class is <mark>primarily intended for tokenizing data</mark> (which we'll cover next), it can <mark>also
                be used to find stuff</mark>, just like the Pattern and Matcher classes.</p>
            
            <p>you can
                use it to apply regex expressions to source data to tell<mark> you how many instances of an
                expression</mark> exist in a given piece of source data.</p>            
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.Scanner;
    
public class ScanIn {
    
    public static void main(String[] args) {
        System.out.print("input: ");
        System.out.flush();
        try {
            Scanner s = new <mark>Scanner</mark>(<mark>System.in</mark>);
            String token;
            do {
                token = <mark>s.findInLine(args[0]);</mark>
                System.out.println("found " + token);
            } while (token != null);
        } catch (Exception e) {
            System.out.println("scan exc");
        }
    }
}
    
            </pre>      
            
            
 <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec5/regex/ScanIn.java

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/regex/ScanIn <mark>"\d\d"</mark>
input: 1b2c335f456
found 33
found 45
<mark>found null</mark>

C:\vivek\docs\cert\ocpjp6\src>           
    </pre>  
           
            <h3>Tokenizing  </h3>
            
            <p>Tokenizing is the process of taking big pieces of source data, breaking them into
                little pieces, and <mark>storing the little pieces in variables</mark>.  </p>          
 
            <p>moved into useful places like <mark>objects, arrays or collections</mark> </p>  
            
            <p>two classes
                in the API that provide tokenizing capabilities: String (using the <mark>split()</mark> method)
                and <mark>Scanner</mark>, which has <mark>many methods </mark>that are useful for tokenizing.          </p>  
 
            <h3>Tokenizing with String.split()  </h3>
            
            <p>This is a handy way to tokenize relatively <mark>small pieces of data</mark>. </p>           
            <pre>            
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
public class SplitTest {
    
    public static void main(String[] args) {
        <mark>String[] tokens</mark> = args[0]<mark>.split</mark>(args[1]);
        System.out.println("count " + tokens.length);
        for (String s : tokens) {
            System.out.println(">" + s + "<");
        }
    }
}
    
            </pre>    
            
<pre class='out'> 
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec5/regex/SplitTest.java

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/regex/SplitTest "ab5 ccc 45 @" "\d"
count 4
>ab<
> ccc <
><
> @<

C:\vivek\docs\cert\ocpjp6\src>           
            
    </pre>   
            
            
            <p>One<mark> drawback</mark> to using the String.split() method is that often <mark>you'll want
                to look at tokens as they are produced</mark>, and possibly<mark> quit a tokenization</mark> operation
                early when you've created the tokens you need.   </p>        

            <p>if you try <mark>\.</mark> ? Now the Java compiler thinks <mark>you're trying to create an escape sequence
                that doesn't exist</mark>. </p>           
            <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
    
public class SplitTest1 {
    
    
    public static void main(String[] args) {
        System.out.println("<mark>\"</mark> <mark>\\</mark>");
        String s = "ab.cde.fg";
        String[] tokens = s.split("<mark>\\.</mark>");
         for (String s1 : tokens) {
            System.out.println(">" + s1 + "<");
        }
    }
        
}
            </pre>       

 <pre class='out'>           
run:
" \
>ab<
>cde<
>fg<
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>    
            
            <h3>Tokenizing with Scanner  </h3>
            
            <p>The java.util.Scanner class is the <mark>Cadillac</mark> of tokenizing </p> 
            
            <p>When you need to do some
                <mark>serious tokenizing</mark>, look no further than Scanner-this beauty <mark>has it all</mark>. </p>           

            <p>the Scanner class
                offers the following features:  </p>
            
            <br>Scanners can be <mark>constructed</mark> using <mark>files, streams, or Strings</mark> as a source.
            <br>Tokenizing is <mark>performed within a loop</mark> so that you can <mark>exit the process at any
                      point</mark>.
            <br>            <mark>Tokens</mark> <mark>can be converted</mark> to their appropriate <mark>primitive types automatically</mark>.            
    
            <p>Scanner's <mark>default delimiter</mark> is <mark>whitespace</mark>  </p> 
            
            
            <p>s1 is iterated over with the more generic
                <mark>next()</mark> method, which returns every token as a String, while <mark>s2 is analyzed with
                several of the specialized nextXxx() methods (where Xxx is a primitive type)</mark>:    </p>        
            
            <pre>            
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
import java.util.Scanner;
    
public class ScanNext {
    
    public static void main(String[] args) {
        boolean b2, b;
        int i;
        String s, hits = " ";
        Scanner s1 = new Scanner(args[0]);
        Scanner s2 = new Scanner(args[0]);
        while (b = s1.<mark>hasNext()</mark>) {
            s = <mark>s1.next()</mark>;
            hits += "s";
        }
        while (<mark>b = s2.hasNext()</mark>) {
            if (<mark>s2.hasNextInt()</mark>) {
                i = <mark>s2.nextInt();</mark>
                hits += "i";
            } else if (<mark>s2.hasNextBoolean()</mark>) {
                b2 = <mark>s2.nextBoolean()</mark>;
                hits += "b";
            } else {
                s2.next();
                hits += "s2";
            }
        }
        System.out.println("hits " + hits);
    }
}
            </pre>
            
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec5/regex/ScanNext.java

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/regex/ScanNext "1 true 34 hi"
hits  ssssibis2

C:\vivek\docs\cert\ocpjp6\src>            
    </pre>     
 
            <p>methods named <mark>hasNextXxx()</mark> test the value of the next token but <mark>do not actually get the token</mark>, <mark>nor do they move to the next token</mark> in the source
                data </p>  
            
            <p>The <mark>nextXxx() </mark> methods all perform two functions: <mark>they get the next token</mark>,
                and then <mark>they move to the next token</mark>.</p>
            
            <p>The Scanner class has <mark>nextXxx()</mark> (for instance nextLong()) and hasNextXxx()
                (for instance hasNextDouble()) methods for <mark>every primitive</mark> type <mark>except char</mark>.</p>
            
            <p>Scanner class has a <mark>useDelimiter()</mark> method that allows you to set
                the delimiter to be any valid regex expression </p>    


            <h3>Formatting with printf() and format()  </h3>          
            
<p>The format() and printf() methods were added to
    java.io.PrintStream in <mark>Java 5</mark>.</p>

<p>These two methods behave exactly the same way, so
    anything we say about one of these methods applies to both of them.</p>

<p>Behind the scenes, the<mark> format()</mark> method uses the <mark>java.util.Formatter</mark> class
    to do the heavy formatting work</p>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
    
public class printf {
    
    
    public static void main(String[] args) {
        System.out.printf("%<mark>2$</mark>d + %<mark>1$</mark>d", 123, 456);
    }
        
}
</pre>

<pre class='out'>
run:
456 + 123BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>construction of
    format strings:</p>

<pre><mark>%</mark>[arg_index$][flags][width][.precision]conversion <mark>char</mark></pre>

<p>The values within <mark>[ ] are optional</mark></p>

<p>the only <mark>required</mark> elements of
    a format string are the <mark>%</mark> and a <mark>conversion character</mark>.</p>


<p><b>arg_index </b>- An <mark>integer</mark> followed directly by a $, this indicates which argument
    should be printed in this position.</p>


<p><b>flags</b></p>

<br>"-" <mark>Left justify</mark> this argument
<br>"+" <mark>Include a sign (+ or -)</mark> with this argument

<br>"0" <mark>Pad this argument with zeroes</mark>
<br>"," <mark>Use locale-specific</mark> grouping separators (i.e., the comma in 123,456)
<br>"(" <mark>Enclose negative numbers</mark> in parentheses

<p><b>width </b>-the <mark>minimum number of characters</mark> to print. </p>

<p><b>precision</b> - only need this when <mark>formatting a floating-point
    number</mark>, and in the case of floating point numbers, <mark>precision indicates the number of
    digits to print after the decimal point</mark>.</p>
    
    <p><b>conversion</b> - The type of argument you'll be formatting. You'll need to know:  </p>  
    <br>    b boolean
    <br>    c char
    <br>    <mark>d</mark> integer
    <br>    f floating point
    <br>    <mark>s</mark> string

    <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
    
public class printf {
    
    
    public static void main(String[] args) {
        System.out.printf("%2$d + %1$d \n", 123, 456);
            
        int i1 = -123;
        int i2 = 12345;
        System.out.printf(">%<mark>1$</mark>(<mark>7</mark>d< \n", i1);
        System.out.printf(">%<mark>+0,</mark>7d< \n", i2);
        System.out.<mark>format</mark>(">%<mark>+-</mark>7d< \n", i2);
        System.out.printf(">%<mark>2$</mark>b + %1$5d< \n", i1, false);
            
            
            
    }
        
}
    </pre> 
<pre class='out'>    
run:
456 + 123 
>  (123)< 
>+12,345< 
>+12345 < 
>false +  -123< 
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    
    <p>Finally, it's important to remember that if you
        have a <mark>mismatch between the type specified in your conversion</mark> character and your
        argument, you'll get a <mark>runtime exception</mark>:    </p>

    <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.regex;
    
    
public class printfException {
    
    
    public static void main(String[] args) {
        System.out.format("<mark>%d</mark>", <mark>12.3</mark>);
    }
        
}
    </pre>
    
<pre class='out'>    
run:
Exception in thread "main" java.util.<mark>IllegalFormatConversionException</mark>: d is incompatible with java.lang.Double
	at java.util.Formatter$Transformer.transformFromInteger(Unknown Source)
	at java.util.Formatter$Transformer.transform(Unknown Source)
	at java.util.Formatter.format(Unknown Source)
	at java.io.PrintStream.format(PrintStream.java:933)
	at com.mvivekweb.ocpjp6.obj3.sec5.regex.printfException.main(printfException.java:10)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
    