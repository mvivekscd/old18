 <pre>     
   Given a directory structure:
- baseDir
<mark>TestFileload.java</mark>
    - testDir
        - subDir2
            - <mark>Shackelton.txt </mark>    
        </pre> 
        
      
       
       
       
        
        <pre>
import java.io.File;
    
    
public class TestFileload {
    
    
    public static void main(String[] args) {
        String name = "testDir" + File.pathSeparator + "subDir2"
+ File.pathSeparator + "Shackelton.txt";
        System.out.println("name="+name);
 File f = new File(name);
 System.out.println("exists " + f.exists());
     
     
        String name1 = "testDir" + <mark>File.separator</mark> + "subDir2"
+ File.separator + "Shackelton.txt";
        System.out.println("name="+name1);
 File f1 = new File(name1);
 System.out.println("exists " + f1.exists()); 
    }
        
}
    
        </pre>
        
        
        <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src\<mark>baseDir</mark>>javac -source 1.6 TestFileload.java
    
C:\vivek\docs\cert\ocpjp6\src\<mark>baseDir</mark>>java TestFileload
name=testDir;subDir2;Shackelton.txt
exists false
name=<mark>testDir\subDir2\Shackelton.txt</mark>
<mark>exists true</mark>
    
C:\vivek\docs\cert\ocpjp6\src\baseDir>        
    
        </pre>
        
        <pre>      
 Given the following directory structure:
test    -|
         | - Finder.class
         | - testdir -|
                      | - subdir
                      | - subdir2
                      | - testfile.txt       
                          
        </pre>
        
        <pre>
import java.io.File;
import java.io.IOException;
    
    
public class Finder {
public static void main(String[] args) throws IOException {
String[] files = new String[100];
File <mark>dir</mark> = new File(args[0]);
files = <mark>dir.list();</mark>
System.out.println(<mark>files.length</mark>);
} }       
        </pre>
        
 <pre class='out'>       

C:\vivek\docs\cert\ocpjp6\src\<mark>test</mark>>javac -source 1.6 Finder.java

C:\vivek\docs\cert\ocpjp6\src\test>java Finder testdir
3

C:\vivek\docs\cert\ocpjp6\src\test>        
        
    </pre>

        <pre>
Given the proper import statements and:
23. try {
24. File file = new File("myFile.txt");
25. PrintWriter pw = new PrintWriter(file);
26. pw.println("line 1");
27. pw.close();
28. PrintWriter pw2 = new PrintWriter("myFile.txt");
29. pw2.println("line 2");
30. pw2.close();
31. } catch (IOException e) { }
What is the result? (Choose all that apply.)
A. No file is created.
B. A file named "myFile.txt" is created.
C. Compilation fails due to an error on line 24.
D. Compilation fails due to an error on line 28.
E. "myFile.txt" contains only one line of data, "line 1"
F. "myFile.txt" contains only one line of data, "line 2"
G. "myFile.txt" contains two lines of data, "line 1" then "line 2"
Answer (for Objective 3.2):
B and F are correct. Both PrintWriter constructors are valid. <mark>When the second
constructor is called, it truncates the existing file to zero size, then "line 2" is added to</mark>
the file by the second println().        
    
        </pre>
        
        <pre>  
Given the proper import statement(s) and:
4. Console c = System.console();
5. char[] pw;
6. if(c == null) return;
7. pw = c.readPassword("%s", "pw: ");
8. System.out.println(c.readLine("%s", "input: "));
Which statements are true? (Choose all that apply.)
A. An exception will be thrown on line 8.
B. The variable pw must be a String, not a char[].
C. No import statements are necessary for the code to compile.        
 D. If the code compiles and is invoked, line 7 might never run.
E. Without a third argument, the readPassword() method will echo the password the user
types.
F. If line 4 was replaced with the following code, the program would still compile: "Console
c = new Console();".
Answer (for Objective 3.2):
D is correct. The code is all legal, but it's possible to invoke a Java program in an
environment that doesn't have a Console object�??therefore, the if test on line 6 can
sometimes be true. The readPassword() method ALWAYS disables echoing. The
Console object is ALWAYS constructed using System.console(), but the Console
class is in java.io.   
        </pre>
        <pre>        
Given:
3. import java.io.*;
4. public class Kesey {
5. public static void main(String[] args) throws Exception {
6. File file = new File("bigData.txt");
7. FileWriter w = new FileWriter(file);
8. w.println("lots o' data");
9. w.flush();
10. w.close();
11. } }        
 What is the result? (Choose all that apply.)
A. An empty file named "bigData.txt" is created.
B. Compilation fails due only to an error on line 5.
C. Compilation fails due only to an error on line 6.
D. Compilation fails due only to an error on line 7.
E. Compilation fails due only to an error on line 8.
F. Compilation fails due to errors on multiple lines.
G. A file named "bigData.txt" is created, containing one line of data.
Answer (for Objective 3.2):
E is correct. It's legal for main() to declare an exception and not handle it. Line 8 fails
because the println() method is one of the easier-to-use <mark>methods included in the
PrintWriter</mark> class but not in the more low-level FileWriter class.       
        </pre>
        
    
        <pre>
            
package com.mvivekweb.ocpjp6.obj3.sec2.exam;
    
import java.io.File;
import java.io.PrintWriter;
    
    
public class Uboat {
    
    
    public static void main(String[] args) {
        try {
 File f1 = new File("sub1");
 f1.mkdir();
 File f2 = new File(f1,"sub2");
File f3 = new File(f1,"sub3");
 PrintWriter pw = new PrintWriter(f3);
 } catch (Exception e) { System.out.println("ouch"); }
    }
        
}
        </pre>
        <pre>        
 sub1
     - sub3
         
        </pre>
        
        
<p>The program creates a new directory called "sub1", and adds a new file
            called "sub3" to "sub1". The program creates a File object called "sub2", but it is
            never used to create a physical file. The second invocation of the program runs without
            exception. The call to mkdir() returns a boolean indicating whether or not a new
            directory was made. <mark>The second invocation of Uboat truncates the file "sub3" to zero
            length, so any data added to the file before the second invocation is lost. </mark>    </p>   
<pre>        
package com.mvivekweb.ocpjp6.obj3.sec2.exam;
    
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class methodChaining {
    
    public static void main(String[] args) {
        try {
            FileWriter fw1 =
    new FileWriter(new File("f1.txt"));
        
 FileWriter fw2 =
new FileWriter(new BufferedWriter(new PrintWriter("f2.txt")));
 PrintWriter pw1 =
new PrintWriter(new BufferedWriter(new FileWriter("f3.txt")));
 PrintWriter pw2 =
new PrintWriter(new FileWriter(new File("f4.txt")));
 } catch (IOException ex) {
            Logger.getLogger(methodChaining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    
</pre>

<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - no suitable constructor found for FileWriter(java.io.BufferedWriter)
    constructor java.io.FileWriter.FileWriter(java.io.FileDescriptor) is not applicable
      (actual argument java.io.BufferedWriter cannot be converted to java.io.FileDescriptor by method invocation conversion)
    constructor java.io.FileWriter.FileWriter(java.io.File,boolean) is not applicable
      (actual and formal argument lists differ in length)
    constructor java.io.FileWriter.FileWriter(java.io.File) is not applicable
      (actual argument java.io.BufferedWriter cannot be converted to java.io.File by method invocation conversion)
    constructor java.io.FileWriter.FileWriter(java.lang.String,boolean) is not applicable
      (actual and formal argument lists differ in length)
    constructor java.io.FileWriter.FileWriter(java.lang.String) is not applicable
      (actual argument java.io.BufferedWriter cannot be converted to java.lang.String by method invocation conversion)
	at com.mvivekweb.ocpjp6.obj3.sec2.exam.methodChaining.main(methodChaining.java:18)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

