<pre>package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Two {
    
    public static void main(String[] args) {
        int y = 0;
        assert y == 0;
        if (args.length > 0) 
            new One();
                
    }
        
}
    
class One {
    
    int x = 0;
        
    {
        <mark>assert x == 1;</mark>
    }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Two.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java Two
Error: Could not find or load main class Two

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect3/exam/Two

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Two

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x
Exception in thread "main" java.lang.AssertionError
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.One.<init>(Two.java:20)
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.Two.main(Two.java:9)

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:One com/mvivekweb/ocpjp6/obj2/sect3/exam/Two

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:One com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/One com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x
Exception in thread "main" java.lang.AssertionError
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.One.<init>(Two.java:20)
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.Two.main(Two.java:9)

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/Two com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Two {
    
    public static void main(String[] args) {
        int y = 0;
        assert y == 0;
        if (args.length > 0) 
            new One();
                
    }
        
}
    
class One {
    
   <mark> int x = 0;</mark>
        
    {
        <mark>assert x == 0</mark>;
    }
}
    
</pre>

<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Two.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/One com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/Two com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Two {
    
    public static void main(String[] args) {
        int y = 0;
        assert y == 0;
        if (args.length > 0) 
            new One();
                
    }
        
}
    
class One {
    
    int x = 0;
        
    {
        System.out.println("x="+x);
        assert x == 0;
    }
}
    
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Two.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/One com/mvivekweb/ocpjp6/obj2/sect3/exam/Two x
x=0

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/Two com/mvivekweb/ocpjp6/obj2/sect3/exam/Two 1
x=0

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea:com/mvivekweb/ocpjp6/obj2/sect3/exam/Two com/mvivekweb/ocpjp6/obj2/sect3/exam/Two 1 2 4
x=0

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
    
public class Later {
    
    
    <mark>public static</mark> void main(String[] args) {
        boolean earlyExit = new Later().test1(args);
<mark> if(earlyExit) assert false;</mark>
 new Later().test2(args);
    }
boolean test1(String[] a) {
if (a.length == 0) return false;
 return true;
}
private void test2(String[] a) {
 if (a.length == 2) assert false;
}
}
    
</pre>
<pre class='out'>


C:\Users\manju>cd C:\vivek\java7\ocpjp6\ocpjp6\src

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Later.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning


C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect3/exam/Later

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Later

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Later  x
Exception in thread "main" java.lang.AssertionError
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.Later.main(Later.java:11)

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Baskin {
    
    public static void main(String[] args) {
int i = 4;
int j = 1;
    
 assert(i > Integer.valueOf(args[0]));
assert(j > Integer.valueOf(args[0])): "error 1";
 assert(j > i): "error 2": "passed";
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Baskin.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect3\exam\Baskin.java:11: error: ';' expected
 assert(j > i): "error 2": "passed";
                         ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Epoch {
static int jurassic = 0;
    public static void main(String[] args) {
assert(doStuff(5));
    }
static boolean doStuff(int x) {
 jurassic += x;
 return true;
 }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Epoch.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.4 com/mvivekweb/ocpjp6/obj2/sect3/exam/Epoch.java
warning: [options] bootstrap class path not set in conjunction with -source 1.4
warning: [options] source value 1.4 is obsolete and will be removed in a future release
warning: [options] target value 1.4 is obsolete and will be removed in a future release
warning: [options] To suppress warnings about obsolete options, use -Xlint:-options.
4 warnings

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>javac -source 1.3 com/mvivekweb/ocpjp6/obj2/sect3/exam/Epoch.java</mark>
warning: [options] bootstrap class path not set in conjunction with -source 1.3
warning: [options] source value 1.3 is obsolete and will be removed in a future release
warning: [options] target value 1.4 is obsolete and will be removed in a future release
warning: [options] To suppress warnings about obsolete options, use -Xlint:-options.
com\mvivekweb\ocpjp6\obj2\sect3\exam\Epoch.java:6: warning: as of release 1.4, 'assert' is a keyword, and may not be used as an identifier
assert(doStuff(5));
^
  (use -source 1.4 or higher to use 'assert' as a keyword)
com\mvivekweb\ocpjp6\obj2\sect3\exam\Epoch.java:6: error: cannot find symbol
assert(doStuff(5));
^
  symbol:   method assert(boolean)
  location: class Epoch
1 error
5 warnings

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
2. public class Payroll {
3. int salary;
4. int getSalary() { return salary; }
5. void setSalary(int s) {
6. assert(s > 30000);
7. salary = s;
8. } }
    
</pre>


<p>line 6 currently <mark>uses the assert mechanism to
    validate a non-private method's argument, which is not considered appropriate</mark>.
    
</p>

<p>the assert mechanism is mainly <mark>independent of the OO
        concepts</mark> of cohesion and encapsulation.</p>

<p>You can, at the command line, enable assertions for a specific class.
</p>

<p>You can, at the command line, disable assertions for a specific package.</p>

<p>You can <mark>programmatically test whether assertions have been enabled</mark> without throwing an
    AssertionError.because the assert statement's first expression must
    result in a boolean value, which could be a boolean assignment.</p>

<p>catching an AssertionError is considered <mark>inappropriate</mark>.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Alamo {
    
    public static void main(String[] args) {
try {
assert(!args[0].equals("x")): "kate";
} catch(Error e) { System.out.print("ae "); }
finally {
try {
<mark>assert(!args[0].equals("y")): "jane";</mark>
 } catch(Exception e2) { System.out.print("ae2 "); }
finally {
throw new IllegalArgumentException();
 } }
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Alamo.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj2/sect3/exam/Alamo y
Exception in thread "main" java.lang.IllegalArgumentException
        at com.mvivekweb.ocpjp6.obj2.sect3.exam.Alamo.main(Alamo.java:14)

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<p>
    throws an AssertionError (which the second catch statement
    cannot catch). Before it can be reported, the second finally statement MUST run,
    which throws IllegalArgumentException, so the AssertionError never gets
    reported.</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect3.exam;
    
public class Argue {
static boolean b;
 static int x = 0;
    public static void main(String[] args) {
int guess = (int)(Math.random() * 5);
if(guess < 0) assert false;
 assert b = true;
<mark> assert x = 0;</mark>
 assert x == 0;
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/exam/Argue.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect3\exam\Argue.java:10: error: incompatible types: int cannot be converted to boolean
 assert x = 0;
          ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
Given:
1. class Hotel {
2. static void doStuff(int x) {
3. assert (x < 0) : "hotel";
4. }
5. }
6. public class Motel13 extends Hotel {
7. public static void main(String[] args) {
8. doStuff(-5);
9. int y = 0;
10. assert (y < 0) : "motel";
11. } }
Which of the following invocations will run without exception? (Choose all that apply.)
<mark>A. java Motel13</mark>
B. java -ea Motel13
<mark>C. java -da:Hotel Motel13
D. java -da:Motel13 Motel13</mark>
E. java -ea -da:Hotel Motel13
<mark>F. java -ea -da:Motel13 Motel13</mark>
</pre>