<h3>Given a code example, determine if a method is correctly overriding or overloading
    another method, and identify legal return values (including covariant returns), for the
    method.</h3>

<h3>Overridden Methods</h3>

<p>The key benefit of overriding is the ability to <mark>define behavior</mark> that's
    specific to a <mark>particular subclass type</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    public void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse <mark>extends</mark> Animal {
    
    public void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
}
</pre>

<p>For <mark>abstract methods</mark> you inherit from a superclass, you have no choice. You must
    implement the method in the subclass unless the subclass is also abstract.</p>


<p><mark>Abstract
        methods</mark> must be implemented by the concrete subclass, but this is a lot like saying
    that the concrete subclass overrides the abstract methods of the superclass.</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    public void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    public void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void <mark>buck()</mark> { }
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class TestAnimals {
    
    public static void main(String[] args) {
        Animal a = new Animal();
        Animal b = new Horse(); //Animal ref, but a Horse object
        a.eat(); // Runs the Animal version of eat()
        b.eat(); // Runs the Horse version of eat() <mark>Animal Has eat method</mark>
       <mark> b.buck(); // animal does not have buck</mark>
    }
        
}
</pre>

<pre>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/override/TestAnimals.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\override\TestAnimals.java:10: error: cannot find symbol
        b.buck(); // animal does not have buck
         ^
<mark>  symbol:   method buck()
  location: variable b of type Animal</mark>
1 error
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src>
    
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    public void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    public void <mark>eat() {</mark>
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { }
}
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class TestAnimals {
    
    public static void main(String[] args) {
        Animal a = new Animal();
        Animal b = new Horse(); //Animal ref, but a Horse object
        Horse c = new Horse(); //Animal ref, but a Horse object
        a.eat(); // Runs the Animal version of eat()
<mark>        b.eat(); // Runs the Horse version of eat() Animal Has eat method and call horse eat
        c.eat();</mark>
        //b.buck(); // animal does not have buck
    }
        
}</pre>
<pre class='out'>
run:
Generic Animal Eating Generically
<mark>Horse eating hay, oats, and horse treats
Horse eating hay, oats, and horse treats</mark>
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    public void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse <mark>extends</mark> Animal {
    
    public void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
   <mark> public void buck() { </mark>
        System.out.println("bck");
    }
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class TestAnimals {
    
    public static void main(String[] args) {
        Animal a = new Animal();
        Animal b = new Horse(); //Animal ref, but a Horse object
        Horse c = new Horse(); //Animal ref, but a Horse object
        a.eat(); // Runs the Animal version of eat()
        b.eat(); // Runs the Horse version of eat() Animal Has eat method
        c.eat();
        <mark>c.buck(); // horse does have buck method</mark>
    }
        
}
</pre>
<pre class='out'>
run:
Generic Animal Eating Generically
Horse eating hay, oats, and horse treats
Horse eating hay, oats, and horse treats
bck
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<p>The overriding method <mark>cannot have a more restrictive access modifier</mark> than the
    <mark>method being overridden</mark></p>
<pre>
<mark>Superclass inst= new Subclass();
    
if
superclass has same method
    
then call subclass same method if not present call superclass method</mark>
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    <mark>public</mark> void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    <mark>protected</mark> void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
}
    
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/override/Animal.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\override\Animal.java:12: error: eat() in Horse cannot override eat() in Animal
    protected void eat() {
                   ^
  attempting to assign weaker access privileges; was public
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<h3>inheritance and static issue</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    <mark>protected</mark> void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse <mark>extends</mark> Animal {
    
    <mark>protected</mark> void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
}</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
    
public class TestAnimal1 {
    
    
    public static void main(String[] args) {
        <mark>Animal b = new Horse();
        b.eat();</mark>
    }
        
}
</pre>
<pre class='out'>
run:
Horse eating hay, oats, and horse treats
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    protected void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    <mark>private</mark> void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/override/Animal.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\override\Animal.java:12: error: eat() in Horse cannot override eat() in Animal
    private void eat() {
                 ^
  attempting to assign weaker access privileges; was protected
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    <mark>static</mark> void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse<mark> extends</mark> Animal {
    
    <mark>static</mark> void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
}
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
    
public class TestAnimal1 {
    
    
    public static void main(String[] args) {
<mark>        Animal b = new Horse(); 
        b.eat();    // animal has eat and call animal static method </mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
<mark>Generic Animal Eating Generically</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
import java.sql.SQLException;
    
public class Animal {
    
    <mark>static void eat(){</mark>
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
   <mark> void eat() {</mark>
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
        
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/override/Animal.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\override\Animal.java:14: error: eat() in Horse cannot override eat() in Animal
    void eat() {
         ^
  overridden method is static
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
<mark>Superclass inst= new Subclass();
    
if
superclass has same static method
    
and call superclass same static method</mark>
    
</pre>


<p>an overriding method <mark>must fulfill the
    contract of the superclass</mark>.</p>
    <pre>
import java.sql.SQLException;
    
public class Animal {
    
    <mark>private void eat(){</mark>
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    <mark>void eat() {</mark>
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
        
        
}</pre>
    
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
    
    
public class TestAnimal1 {
    
    public static void main(String[] args) throws FileNotFoundException {
        <mark>Animal b</mark> = new Horse();
        b.eat();
    }
        
}
    
    </pre>
  <pre class='out'>  
 run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - <mark>eat() has private access in com.mvivekweb.ocpjp6.obj1.sect5.override.Animal</mark>
	at com.mvivekweb.ocpjp6.obj1.sect5.override.TestAnimal1.main(TestAnimal1.java:12)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)   
</pre>
    
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
    
    
public class TestAnimal1 {
    
    public static void main(String[] args) throws FileNotFoundException {
        Horse b = new Horse();
        b.eat();
    }
        
}
    </pre>
<pre class='out'>    
run:
Horse eating hay, oats, and horse treats
BUILD SUCCESSFUL (total time: 0 seconds)    
</pre> 
    
    
    <h3> rules for overriding a method  </h3>
    
    <p>The <mark>argument list must exactly match</mark> that of the overridden method</p>
    
    <p>The return type must be the same as, or a subtype of, the return type declared
        in the original overridden method in the superclass. </p>
    
    <p>The <mark>access level can't be more restrictive</mark> than the overridden method's.</p>    
    
    <p>The access level <mark>CAN be less restrictive</mark> than that of the overridden method.</p>


    <p>A subclass within the <mark>same package</mark> as the instance's superclass <mark>can override
        any superclass method that is not marked private or final</mark>. A subclass in a
        different package can override only those non-final methods marked public
        or protected (since protected methods are inherited by the subclass).</p>

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Animal {
    
    <mark>private</mark> void eat() {
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    <mark>final</mark> void eat() {
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
        
}
    
    
    </pre>
    
 
<pre class='out'>
package com.mvivekweb.ocpjp6.obj1.sect5.override;


public class TestAnimal1 {


    public static void main(String[] args) {
        <mark>Horse b = new Horse();</mark>
        b.eat();
    }

}
   
</pre>
  <pre class='out'>  
 run:
Horse eating hay, oats, and horse treats
BUILD SUCCESSFUL (total time: 0 seconds)   

</pre>
    
    
    <p>The overriding method <mark>CAN throw any unchecked</mark> (runtime) exception,
        <mark>regardless of whether the overridden method declares the exception</mark></p>
    
    
    <p>The overriding method <mark>must NOT throw checked exceptions</mark> that are new
        or broader than those declared by the overridden method.</p>
    
    <p> For example, a
        method that declares a FileNotFoundException <mark>cannot be overridden by a
            method that declares a SQLException, Exception,</mark> or any other non-runtime
        exception <mark>unless it's a subclass of FileNotFoundException</mark>.  </p>
    
    
    
    
    
    
    
    
    <pre>  
  import java.sql.SQLException;
      
public class Animal {
    
    public void eat() <mark>throws FileNotFoundException{</mark>
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    <mark>public void eat(){</mark>
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
        
}  
    </pre>
    
<pre class='out'>
package com.mvivekweb.ocpjp6.obj1.sect5.override;

import java.io.FileNotFoundException;


public class TestAnimal1 {


    public static void main(String[] args) <mark>throws FileNotFoundException</mark> {
        <mark>Animal b = new Horse();
        b.eat();</mark>
    }

}    
</pre>
  <pre class='out'>  
  run:
Horse eating hay, oats, and horse treats
BUILD SUCCESSFUL (total time: 0 seconds)  
</pre>
    <pre>   
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
import java.sql.SQLException;
    
public class Animal {
    
    public void eat()<mark> throws FileNotFoundException</mark>{
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    public void eat() <mark>throws SQLException</mark>{
        System.out.println("Horse eating hay, oats, "
                + "and horse treats");
    }
    public void buck() { 
        System.out.println("bck");
    }
        
}
    
    </pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/override/Animal.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\override\Animal.java:15: error: eat() in Horse cannot override eat() in Animal
    public void eat() throws SQLException{
                ^
  overridden method does not throw SQLException
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
    
</pre>

    <p>The overriding method can throw narrower or fewer exceptions. Just because
        an overridden method "takes risks" doesn't mean that the overriding subclass'
        exception takes the same risks. Bottom line: <mark>an overriding method doesn't    
            have to declare any exceptions that it will never throw, regardless of what the
            overridden method declares.</mark>
    </p>
    
    
    <p>You <mark>cannot override a method marked final</mark>. </p>
    
    <p>You <mark>cannot override a method marked static</mark> </p>
    
    <p><mark>If a method can't be inherited, you cannot override it</mark>. Remember that
        overriding implies that you're reimplementing a method you inherited!  </p>
    <pre>   
import java.io.FileNotFoundException;
import java.sql.SQLException;
    
public class Animal {
    
    public void eat() throws FileNotFoundException{
        System.out.println("Generic Animal Eating Generically");
    }
}
    
<mark>class Horse extends Animal {
    
    
}   </mark> 
    </pre>
    

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
    
    
public class TestAnimal1 {
    
    
    public static void main(String[] args) throws FileNotFoundException {
        Animal b = new Horse();
        <mark>b.eat();  animal has eat , call horse eat if not call animal mathod</mark>
    }
        
}
    </pre>
  <pre class='out'>  
run:
Generic Animal Eating Generically
BUILD SUCCESSFUL (total time: 0 seconds)    
</pre>
    
    
    <pre>  
   import java.io.FileNotFoundException;
import java.sql.SQLException;
    
public class Animal {
    
    public void eat(){
        System.out.println("Generic Animal Eating Generically");
    }
}
    
class Horse extends Animal {
    
    
} 
    </pre>
    

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
import java.io.FileNotFoundException;
    
    
public class TestAnimal1 {
    
    public static void main(String[] args) throws FileNotFoundException {
        <mark>Horse b = new Horse();</mark>
       <mark> b.eat();// inherits from Animal not override</mark>
    }
        
}    
    
    </pre>
 <pre class='out'>   
run:
Generic Animal Eating Generically
BUILD SUCCESSFUL (total time: 0 seconds)    
</pre> 
    
    
    <h3>Overloaded Methods  </h3>  
    
    <p>Overloaded methods let you <mark>reuse the same method name</mark> in a class, but with
        <mark>different arguments</mark> (and <mark>optionally, a different return</mark> type)</p>
    
    
    <h3>Rules for Overloaded method</h3>
    
    <p>Overloaded methods<mark> MUST change the argument list</mark>. </p>
    
    
    <p>Overloaded methods <mark>CAN change the return type</mark>.</p>

    
    <p>Overloaded methods <mark>CAN change the access modifier</mark>. </p>
    
    <p>Overloaded methods <mark>CAN declare new or broader checked exceptions.</mark>   </p>
    
    
    <p>A method can be <mark>overloaded in the same class or in a subclass</mark>.  </p>
    
    
    <pre>    
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
import java.io.IOException;
    
public class Foo {
    
    <mark>public void doStuff(int y, String s) {</mark>
    }
        
    public void moreThings(int x) {
    }
}
    
class Bar extends Foo {
    
    <mark>public void doStuff(int y, long s) throws IOException {</mark>
    }
        
        
}
    
    </pre>
    
    <pre>  
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
import java.io.IOException;
    
public class Foo {
    
    public void doStuff(int y, String s) {
    }
        
    public void moreThings(int x) {
    }
}
    
class Bar extends Foo {
    
    public void doStuff(int y, long s) throws IOException {
    }
        
    public void doStuff(int y, String s) <mark>throws Exception</mark>{
    }
}
    
    </pre>
    
 <pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/overload/Foo.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\overload\Foo.java:19: error: doStuff(int,String) in Bar cannot override doStuff(int,String) in Foo
    public void doStuff(int y, String s) throws Exception{
                ^
  <mark>overridden method does not throw Exception</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>   
    
</pre>
    
    <h3> Legal Return Types  </h3>
    
    
    <p>covers two aspects of return types </p>
    

    what you can <mark>declare</mark> as a
return type,

<br>what you can <mark>actually return</mark> as a value

<h3>Return Type Declarations</h3>

<h3>Return Types on Overloaded Methods</h3>
<pre>
public class Foo{
void go() { }
}
public class Bar extends Foo {
String go() { // Not legal! Can't change only the return type
return null;
}
}
</pre>


<h3>Overriding and Return Types, and Covariant Returns</h3>

<p>as of Java 5, you're <mark>allowed to change the return type in the
        overriding</mark> method as long as the <mark>new return type is a subtype of the declared return
            type of the overridden (superclass)</mark> method
</p>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.override;
    
public class Alpha {
    <mark>Alpha</mark> doStuff(char c) {
        return new Alpha();
    }
}
    
class Beta extends Alpha {
    <mark>Beta</mark> doStuff(char c) { // legal override in Java 1.5
        return new Beta();
    }
}
    
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac <mark>-source 1.4</mark> com/mvivekweb/ocpjp6/obj1/sect5/override/Alpha.java
warning: [options] bootstrap class path not set in conjunction with -source 1.4
warning: [options] source value 1.4 is obsolete and will be removed in a future release
warning: [options] target value 1.4 is obsolete and will be removed in a future release
warning: [options] To suppress warnings about obsolete options, use -Xlint:-options.
com\mvivekweb\ocpjp6\obj1\sect5\override\Alpha.java:10: error: doStuff(char) in Beta cannot override doStuff(char) in Alpha
    Beta doStuff(char c) { // legal override in Java 1.5
         ^
  return type Beta is not compatible with Alpha
1 error
4 warnings

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<h3>Returning a Value</h3>


<p>You have to remember only six rules for returning a value:</p>

<p>1. You <mark>can return null</mark> in a method with an object reference return type.</p>
<pre>
public Button doStuff() {
    return <mark>null</mark>;
}
</pre>


<p>2. An array is a perfectly legal return type.</p>
<pre>
public String[] go() {
    return <mark>new String[] {"Fred", "Barney", "Wilma"};</mark>
}
</pre>


<p>3. In a method with a primitive return type, you can return any value or
    variable that can be <mark>implicitly converted to the declared return type</mark>.</p>
<pre>
public <mark>int</mark> foo() {
        char c = 'c';
    <mark>return c;</mark> // char is compatible with int
}
    
</pre>

<p>4. In a method with a primitive return type, you can return any value or
    variable that can be <mark>explicitly cast</mark> to the declared return type.</p>

<pre>
public int foo () {
    float f = 32.5f;
    return <mark>(int)</mark> f;
}
    
</pre>


<p>5. You must not return anything from a method with a void return type.</p>

<p>6. In a method with an object reference return type, you can return any
    object type that can be implicitly cast to the declared return type.</p>

<pre>
public Animal getAnimal() {
    return new Horse(); // Assume Horse extends Animal
}
</pre>
<pre>
public Object getObject() {
int[] nums = {1,2,3};
return nums; // Return an int array,
// which is still an object
}
</pre>
<pre>
public interface <mark>Chewable</mark> { }
public class Gum implements Chewable { }
public class TestChewable {
// Method with an interface return type
public <mark>Chewable</mark> getChewable() {
return <mark>new Gum();</mark> // Return interface implementer
}
}

    
</pre>


<p>Watch for methods that declare an abstract class or interface return
    type, and know that any object that passes the IS-A test (in other words, would test true
    using the instanceof operator) can be returned from that method </p>

<pre>
public abstract class <mark>Animal</mark> { }
public class Bear extends Animal { }
public class Test {
public <mark>Animal</mark> go() {
return <mark>new Bear()</mark>; // OK, Bear "is-a" Animal
}
}
</pre>
    
<h3>Overloading Made Hard-Method Matching</h3>    
    
<p>three factors that can make overloading a little tricky:  </p>
<pre>
Widening
Autoboxing
Var-args
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class EasyOver {
    
    static void go(int x) {
        System.out.print("int ");
    }
        
    static void go(long x) {
        System.out.print("long ");
    }
        
    static void go(double x) {
        System.out.print("double ");
    }
        
    public static void main(String[] args) {
        byte b = 5;
        short s = 5;
        long l = 5;
        float f = 5.0f;
<mark>        go(b);
        go(s);</mark>
        go(l);
        <mark>go(f);</mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
<mark>int int</mark> long double BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>the calls that use byte and the short
    arguments are <mark>implicitly widened</mark> to match the version of the go() method that
    takes an int.</p>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class EasyOver {
    
    
    
    static void <mark>go(double x) {</mark>
        System.out.print("double ");
    }
        
    public static void main(String[] args) {
        byte b = 5;
        short s = 5;
        long l = 5;
        float f = 5.0f;
        go(b);
        go(s);
        go(l);
        go(f);
    }
        
}
    
</pre>
<pre class='out'>
run:
double double double double BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>if there is only one version of the go() method,
    and <mark>it takes a double</mark>, it will be used to match all four invocations of go().</p>


<h3>Overloading with Boxing and Var-args</h3>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class AddBoxing {
    
    static void <mark>go(Integer x)</mark> {
        System.out.println("Integer");
    }
        
//    static void go(long x) {
//        System.out.println("long");
//    }
    
    public static void main(String[] args) {
        int i = 5;
        go(i);
    }
        
}
    
</pre>
<pre class='out'>
run:
<mark>Integer</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class AddBoxing {
    
    static void<mark> go(Integer x) {</mark>
        System.out.println("Integer");
    }
        
    static void <mark>go(long x) {</mark>
        System.out.println("long");
    }
        
    public static void main(String[] args) {
        int i = 5;
        go(i);
    }
        
}
    
</pre>
<pre class='out'>
run:
<mark>long</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>the compiler will <mark>choose widening</mark> over <mark>boxing</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class AddVarargs {
    
    static void go(int x, int y) {
        System.out.println("int,int");
    }
        
    static void <mark>go(byte... x)</mark> {
        System.out.println("byte... ");
    }
        
    public static void main(String[] args) {
        byte b = 5;
        go(b, b);
    }
        
}
    
</pre>
<pre class='out'>
run:
int,int
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>the compiler <mark>will choose the older style</mark> before it chooses the newer
    style, keeping existing code more robust</p>

<p><mark>Widening</mark> beats boxing</p>

<p>Widening beats var-args</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class BoxOrVararg {
    
    static void <mark>go(Byte x, Byte y) {</mark>
        System.out.println("Byte, Byte");
    }
        
    static void go(byte... x) {
        System.out.println("byte... ");
    }
        
    public static void main(String[] args) {
        byte b = 5;
        go(b, b); // which go() will be invoked?
    }
        
}
    
</pre>
<pre class='out'>
run:
Byte, Byte
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>A var-args method is more like a catch-all method</p>

<p>for <mark>catch-all capabilities to be used as a last resort</mark></p>


<h3>Widening Reference Variables</h3>

<p>it's legal to widen a primitive</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class Dog3 extends Animal {
    
    public static void main(String[] args) {
        Dog3 d = new Dog3();
        d.go(d);
    }
        
    void go(<mark>Animal a</mark>) {
        System.out.println("Animal param");
    }
}
    
class Animal {
    
    static void eat() {
    }
}
    
</pre>
<pre class='out'>
run:
<mark>Animal param</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>the compiler <mark>widens the Dog3 reference to an Animal</mark></p>

<p>The key point here is that reference widening depends on
    inheritance, in other words the <mark>IS-A test</mark>.</p>

<p>it's <mark>not legal to widen
        from one wrapper class to another</mark>, because the wrapper classes are peers to one
    another.</p>

<p>For instance, it's <mark>NOT valid to say that Short IS-A Integer</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class Dog4 {
    
    public static void main(String[] args) {
        Dog4 d = new Dog4();
        d.test(<mark>new Integer(5)</mark>); //
    }
        
    void test(<mark>Long x</mark>) {
    }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect5\overload>javac -source 1.6 Dog4.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
Dog4.java:7: error: incompatible types: <mark>Integer cannot be converted to Long</mark>
        d.test(new Integer(5)); //
               ^
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect5\overload>

</pre>

<h3>Overloading When Combining Widening and Boxing</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class WidenAndBox {
    
    //widen and box
    //from byte to long then Long
        
    static void go(<mark>Long x</mark>) {
        System.out.println("Long");
    }
        
    public static void main(String[] args) {
        byte b = 5;
       <mark> go(b);</mark>
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect5\overload>javac -source 1.6 WidenAndBox.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
WidenAndBox.java:14: error: incompatible types: byte cannot be converted to Long
        go(b);
           ^
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect5\overload>

</pre>

<p>it IS possible for the compiler to perform a <mark>boxing operation
        followed by a widening operation</mark> in order to match an invocation to a method
    
</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class BoxAndWiden {
    
    //<mark> byte to Byte then widen to Object</mark>
        
        
    <mark>static void go(Object o) {</mark>
        Byte b2 = (Byte) o; // ok - it's a Byte object
        System.out.println(b2);
    }
        
    public static void main(String[] args) {
        byte b = 5;
        <mark>go(b);</mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
5
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<h3>Overloading in Combination with Var-args</h3>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.overload;
    
public class Vararg {
    
    static void wide_vararg(long... x) {
        System.out.println("long...");
    }
        
    static void box_vararg(Integer... x) {
        System.out.println("Integer...");
    }
        
    public static void main(String[] args) {
        int i = 5;
        wide_vararg(i, i); // needs to widen and use var-args
        box_vararg(i, i); // needs to box and use var-args
    }
        
}
    
</pre>
<pre class='out'>
run:
long...
Integer...
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>you can <mark>successfully combine var-args with either widening</mark> or
    <mark>boxing</mark>.</p>

<h3>rules for overloading methods using widening, boxing,
    and var-args:</h3>


<p>Primitive widening uses the "smallest" method argument possible.</p>

<p>Used individually, <mark>boxing and var-args are compatible</mark> with overloading.</p>

<p>You <mark>CANNOT widen from one wrapper type to another</mark>. (IS-A fails.)</p>

<p>You <mark>CANNOT widen and then box</mark>. (An int can't become a Long.)</p>

<p>You can box and then widen. (An int can become an Object, via Integer.)</p>

<p>You can combine var-args with either widening or boxing.</p>

<h3>important</h3>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
class Plant {
    String getName(){return "plant";
        
    }
    <mark>Plant</mark> getName(){return <mark>this</mark>;
        
    }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Plant.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\exam\Plant.java:8: error: method getName() is already defined in class Plant
    Plant getName(){return this;
          ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
    
public class Plant1 {
     String getName(){return "plant";
         
    }
        
    Plant1 getType(){return this;
        
    }
     Tulip getType1(){return null;
         
    }   
        
}
class Flower extends Plant1{
        String getType1(){return "";
            
    }
}
class Tulip extends Flower{
    
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Plant1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect5\exam\Plant1.java:19: error: getType1() in Flower cannot override getType1() in Plant1
        String getType1(){return "";
               ^
  return type String is not compatible with Tulip
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>










