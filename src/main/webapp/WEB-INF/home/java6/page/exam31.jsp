<pre>      
 package com.mvivekweb.ocpjp6.obj3.sec1.exam;
     
public class Twine {
    
    public static void main(String[] args) {
        String s = "";
        StringBuffer sb1 = new StringBuffer("hi");
        StringBuffer sb2 = new StringBuffer("hi");
        StringBuffer sb3 = new StringBuffer(sb2);
        StringBuffer sb4 = sb3;
       if(sb1.equals(sb2)) s += "1 ";
       if(sb2.equals(sb3)) s += "2 ";
        if(sb3.equals(sb4)) s += "3 ";
        String s2 = "hi";
        String s3 = "hi";
        String s4 = s3;
        if(s2.equals(s3)) s += "4 ";
        if(s3.equals(s4)) s += "5 ";
        System.out.println(s);
    }
}       
        </pre>
<pre class='out'>        
run:
3 4 5 
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>  
        
<p>The <mark>StringBuffer class doesn't override the equals()</mark> method,<mark> so two
    different StringBuffer objects with the same value will not be equal</mark> according to the
    equals() method. On the other hand, the String class's equals() method has been
    overridden so that two different String objects with the same value will be considered
    equal according to the equals() method.
</p>
        
<pre>       
    
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
    
public class TwineBuilder {
    
    
    public static void main(String[] args) {
        String s = "";
        StringBuilder sb1 = new StringBuilder("hi");
        StringBuilder sb2 = new StringBuilder("hi");
        StringBuilder sb3 = new StringBuilder(sb2);
        StringBuilder sb4 = sb3;
       if(sb1.equals(sb2)) s += "1 ";
       if(sb2.equals(sb3)) s += "2 ";
        if(sb3.equals(sb4)) s += "3 ";
        String s2 = "hi";
        String s3 = "hi";
        String s4 = s3;
        if(s2.equals(s3)) s += "4 ";
        if(s3.equals(s4)) s += "5 ";
        System.out.println(s);
    }
        
}
</pre>
<pre class='out'>
run:
3 4 5 
BUILD SUCCESSFUL (total time: 1 second)

    </pre>  
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class Dec26 {
    
    public static void main(String[] args) {
        short a1 = 6;
        new Dec26().go(a1);
        new Dec26().go(new Integer(7));
    }
    void go(Short x) { System.out.print("S "); }
    void go(Long x) { System.out.print("L "); }
    void go(int x) { System.out.print("i "); }
    void go(Number n) { System.out.print("N "); }
        
}
    
</pre>
<pre class='out'>
run:
i N BUILD SUCCESSFUL (total time: 1 second)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class BuilderTest {
    public static void main(String[] args) {
        String s = "1234";
 StringBuilder sb =
 new StringBuilder(s.substring(2).concat("56").replace("7","6"));
 System.out.println(sb.append("89").<mark>insert</mark>(3,"x"));
    }
        
}
    
</pre>
<pre class='out'>
run:
345x689
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>The keys to remember are that indexes are 0-based, and that chained methods
    work from left to right. Also, the replace() method's first argument is the character to be
    replaced, so in this case the replace() invocation has no effect on the StringBuilder.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class equalstringTest {
    
    public static void main(String[] args) {
String s = "";
 if(011 == 9) s += 4;
 if(0x11 == 17) s += 5;
 Integer I = 12345;
 if(I.intValue() == Integer.valueOf("12345")) s += 6;
 System.out.println(s);
    }
        
}
    
    
</pre>
<pre class='out'>
run:
456
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
decimal to octal
    
99
    
    
  1  
  -
8|10   
   8
      
<mark>  1|2
  0|1</mark>
      
  ans-  012


<mark>99
    
   12|3
    1|4
    0|1
        
ans - 143</mark>
</pre>

<p>
    Line 43 is comparing an octal int to an int. Line 44 is comparing a
    hexadecimal int to an int. Line 46 is comparing an int to an Integer, which is
    unboxed before the comparison is made.
    
</p>
<pre>
Given:
42. String s1 = " ";
43. StringBuffer s2 = new StringBuffer(" ");
44. StringBuilder s3 = new StringBuilder(" ");
45. for(int i = 0; i < 1000; i++) // Loop #1
46. s1 = s1.concat("a");
47. for(int i = 0; i < 1000; i++) // Loop #2
48. s2.append("a");
49. for(int i = 0; i < 1000; i++) // Loop #3
50. s3.append("a");
Which statements will typically be true? (Choose all that apply.)
A. Compilation fails.
B. Loop #3 will tend to execute faster than Loop #2.
C. Loop #1 will tend to use less memory than the other two loops.
D. Loop #1 will tend to use more memory than the other two loops.
E. All three loops will tend to use about the same amount of memory.
F. In order for this code to compile, the java.util package is required.
G. <mark>If multiple threads need to use an object, the s2 object should be safer than the s3 object.</mark>
    
B, D, and G are correct. StringBuffer object's methods are synchronized, which makes
them better for multiple threads, and typically a little slower. Because String objects are not
mutable, Loop #1 is likely to create about 1000 different String objects in memory.
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class IntegerEqualsTest {
    
    public static void main(String[] args) {
<mark>Integer i1 = 2001; // set 1
 Integer i2 = 2001;</mark>
 System.out.println((<mark>i1 == i2</mark>) + " " + i1.equals(i2)); // output 1
Integer i3 = 21; // set 2
 Integer i4 = new Integer(21);
 System.out.println((i3 == i4) + " " + i3.equals(i4)); // output 2
 Integer i5 = 21; // set 3
 Integer i6 = 21;
 System.out.println((i5 == i6) + " " + i5.equals(i6));
    }
        
}
    
</pre>
<pre class='out'>
run:
false true
false true
true true
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    Yes, this is a bit of memorization, but it's a fairly common construct. The rule
    is that, for small values, wrappers created through boxing will be pooled, so i5 and i6
    actually point to the same pooled instance of Integer. In each of the first two sets, two
    meaningfully equivalent instances are created.
    
</p>
<pre>
    package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
    public class IntegerEqualsTest {
    
    public static void main(String[] args) {
    <mark>    Integer i1 = 19; // set 1
        Integer i2 = 19;</mark>
    System.out.println((i1 == i2) + " " + i1.equals(i2)); // output 1
    Integer i3 = 21; // set 2
    Integer i4 = new Integer(21);
    System.out.println((i3 == i4) + " " + i3.equals(i4)); // output 2
    Integer i5 = 22; // set 3
    Integer i6 = 22;
    System.out.println((i5 == i6) + " " + i5.equals(i6));
    }
        
    }
    
</pre>
<pre class='out'>
run:
<mark>true true</mark>
false true
true true
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class IntegerEqualsTest {
    
    public static void main(String[] args) {
<mark>Integer i1 = 19; // set 1
 Integer i2 = 19;</mark>
 System.out.println(<mark>(i1 == i2</mark>) + " " + <mark>i1.equals(i2)</mark>); // output 1
Integer i3 = 21; // set 2
 Integer i4 = new Integer(21);
 System.out.println((i3 == i4) + " " + i3.equals(i4)); // output 2
 Integer i5 = 22; // set 3
 Integer i6 = 22;
 System.out.println((i5 == i6) + " " + i5.equals(i6));
<mark> Integer i7 = new Integer(21);
Integer i8 = new Integer(21);</mark>
 System.out.println((i7 == i8) + " " + <mark>i7.equals(i8)</mark>);
    }
        
}
</pre>
<pre class='out'>
run:
true true
false true
true true
<mark>false true</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;

public class KaChung {

    public static void main(String[] args) {
String s = "";
 if(Integer.parseInt("011") == Integer.parseInt("9")) s += 1;
  if(Integer.parseInt("011") == Integer.parseInt("11")) s += 4;
 if(021 == Integer.valueOf("17")) s += 2;
 if(1024 == new Integer(1024)) s += 3;
 System.out.println(s);
    }

}

</pre>
<pre class='out'>
run:
423
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>On line 6, <mark>the parseInt() method assumes a radix of 10 if a radix is not
        specified.</mark> Line 7 is comparing an octal int to an int. Line 8 is comparing an int to an
    Integer, and autoboxing converts the Integer to an int to do the comparison.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;
    
public class Maize {
    
    public static void main(String[] args) {
String s = "12";
 s.concat("ab");
 s = go(s);
 System.out.println(s);
    }
static String go(String s) {
 s.concat("56");
 return s;
 }
}
    
</pre>
<pre class='out'>
run:
12
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
