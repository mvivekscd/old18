<pre>package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
public class Choosy {
    
    public static void main(String[] args) {
        String result = "";
        int x = 7, y = 8;
        if (x == 3) {
            result += "1";
        } else if (x > 9) {
            result += "2";
        } else if (y < 9) {
            result += "3";
        } else if (x == 7) {
            result += "4";
        } else {
            result += "5";
        }
        System.out.println(result);
    }
        
}
</pre>
<pre class='out'>
run:
3
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
public class BirdHouse {
    
    public static void main(String[] args) {
        String r = "0";
        int x = -1, y = -5;
        if(x < 5)
        <mark>if(y > 0)
        if(x > y)
        r += "1";
        else r += "2";
        else r += "3";</mark>
        else r += "4";
        System.out.println(r);
    }
        
}
    
</pre>
<pre class='out'>
run:
03
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
public class Humping {
    
    public static void main(String[] args) {
        String r = "-";
        char[] c = {'a', 'b', 'c', 'z'};
        for(char c1: c)
            <mark>switch (c1) {
            case 'a': r += "a";
            case 'b': r += "b"; break;
            default: r += "X";
            case 'z': r+= "z";
            }</mark>
        System.out.println(r);
    }
        
}
    
</pre>
<pre class='out'>
run:
-abbXzz
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
Given:
3. class Holder {
4. <mark>enum Gas { ARGON, HELIUM };</mark>
5. }
6. public class Basket extends Holder {
7. public static void main(String[] args) {
8. short s = 7; long l = 9L; float f = 4.0f;
9. int i = 3; char c = 'c'; byte b = 5;
10. // insert code here
11. default: System.out.println("howdy");
12. } } }
Which line(s) of code (if any), inserted independently at line 10, will compile? (Choose all
that apply.)
A. switch (s) {
B. switch (l) {
C. switch (f) {
D. switch (i) {
E. switch (c) {
F. switch (b) {
G. switch (Gas.ARGON) {
H. The code will not compile due to additional error(s).
Answer (for Objective 2.1):
A, D, E, F, and G are correct. It's legal to use an enum in a switch.
    
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
    
public class SwitchTest {
    
    
    public static void main(String[] args) {
        int x = 0;
 int[] primes = {1,2,3,5};
 for(int i: primes)
 switch(i) {
 case 1: x += i;
 case 5: x += i;
 default: x += i;
 case 2: x += i;
 }
 System.out.println(x);
    }
        
}
    
</pre>
<pre class='out'>
run:
27
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
    
public class States {
    
static String s;
 static Boolean b;
 static Boolean t1() { return new Boolean("howdy"); }
 static boolean t2() { return new Boolean(s); }
    public static void main(String[] args) {
        System.out.println(<mark>new Boolean("true")</mark>);
        System.out.println(<mark>new Boolean("howdy")</mark>);
    if(t1()) System.out.print("t1 ");
    if(!t2()) System.out.print("t2 ");
    if(t1() != t2()) System.out.print("!= ");    
    }
        
}
    
</pre>
<pre class='out'>
run:
true
false
t2 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj2.sect1.exam;
    
    
public class count {
    
    
    public static void main(String[] args) {
        int y, count = 0;
for(int x = 3; x < 6; x++) {
 try {
switch(x) {
case 3: count++;
 case 4: count++;
case 7: count++;
 case 9: { y = 7 / (x - 4); count += 10; }
}
} catch (Exception ex) { count++; }
 }
System.out.println(count);
    }
        
}
    
</pre>
<pre class='out'>
run:
16
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
    
public class Gauntlet {
    
    
    public static void main(String[] args) {
      String r = "0";
int x = 3, y = 4;
 boolean test = false;
if((x > 2) || (<mark>test = true</mark>))
 if((y > 5) || (++x == 4))
 if((test == true) || (++y == 4))
 r += "1";
 else if(y == 5) r += "2";
 else r += "3";
 else r += "4";
 // else r += "5";
 System.out.println(r);  
    }
        
}
    
</pre>
<pre class='out'>
run:
02
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p><mark>doesn't change the value of test because of the short
    circuit operator</mark>.</p>



