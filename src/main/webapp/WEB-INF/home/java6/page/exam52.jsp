<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class Cool extends SuperCool{
    
    
    public static void main(String[] args) {
        new Cool().go();
    }
void go() {
 SuperCool s = new Cool();
 Cool c = (Cool)s;
 // insert code here
 }
 void doStuff() { os += "cool "; }
}
class SuperCool {
 static String os = "";
 void doStuff() { os += "super "; }
 }</pre>


<pre class='out'>
If the rest of the code compiles, which line(s) of code, inserted independently at line 13,
compile? (Choose all that apply.)
<mark>A. c.doStuff();
B. s.doStuff();
C. this.doStuff();
D. super.doStuff();</mark>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class Supplier extends Contact{
String doStuff() { return "send money "; }
    
    public static void main(String[] args) {
        Supplier s1 = new Supplier();
 Contact c3 = new Contact();
 Contact c1 = s1;
 Supplier s2 = (Supplier) c1;
 Supplier s3 = (Supplier) c3;
 Supplier s4 = new Contact();
    }
        
}
class Contact {
 String doStuff() { return "howdy "; }
 }
</pre>
<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/Supplier.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect2\exam\Supplier.java:15: error: incompatible types: Contact cannot be converted to Supplier
 Supplier s4 = new Contact();
               ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class Supplier extends Contact{
String doStuff() { return "send money "; }
    
    public static void main(String[] args) {
        Supplier s1 = new Supplier();
 Contact c3 = new Contact();
 Contact c1 = s1;
 Supplier s2 = (Supplier) c1;
 Supplier s3 = (Supplier) c3;
 //Supplier s4 = new Contact();
    }
        
}
class Contact {
 String doStuff() { return "howdy "; }
 }
     
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/Supplier.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj5/sect2/exam/Supplier
Exception in thread "main" java.lang.ClassCastException: com.mvivekweb.ocpjp6.obj5.sect2.exam.Contact cannot be cast to com.mvivekweb.ocpjp6.obj5.sect2.exam.Supplier
        at com.mvivekweb.ocpjp6.obj5.sect2.exam.Supplier.main(Supplier.java:14)

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class PolyTest extends Super{
    
    
    public static void main(String[] args) {
        new PolyTest().go();
    }
void go() {
Super s = new PolyTest();
 PolyTest p = (PolyTest)s;
 p.doStuff();
 s.doStuff();
 p.doPoly();
 s.doPoly();
 System.out.println(os);
 }
 void doStuff() { os += "over "; }
 void doPoly() { os += "poly "; }
}
class Super {
 static String os = "";
 void doStuff() { os += "super "; }
 }
</pre>

<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/PolyTest.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect2\exam\PolyTest.java:18: error: cannot find symbol
 s.doPoly();
  ^
  symbol:   method doPoly()
  location: variable s of type Super
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class Andi extends Horse{
    
void beBrisk() { s += "tolt "; }
    public static void main(String[] args) {
        Horse x0 = new Horse();
 Horse x1 = new Andi(); x1.beBrisk();
 Andi x2 = (Andi)x1; x2.beBrisk();
<mark> Andi x3 = (Andi)x0; x3.beBrisk();</mark>
 System.out.println(s);
    }
        
}
class Horse {
 static String s = "";
 void beBrisk() { s += "trot "; }
 }</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/Andi.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj5/sect2/exam/Andi
Exception in thread "main" java.lang.ClassCastException: com.mvivekweb.ocpjp6.obj5.sect2.exam.Horse cannot be cast to com.mvivekweb.ocpjp6.obj5.sect2.exam.Andi
        at com.mvivekweb.ocpjp6.obj5.sect2.exam.Andi.main(Andi.java:13)

        C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre >
package com.mvivekweb.ocpjp6.obj5.sect2.exam;

class Pancake {
}

class BlueberryPancake extends Pancake {
}

public class SourdoughBlueberryPancake2 extends BlueberryPancake {

    public static void main(String[] args) {
        Pancake p4 = new SourdoughBlueberryPancake2();
        Pancake p5 = p4;
 Pancake p6 = (BlueberryPancake)p4;
 BlueberryPancake b2 = (BlueberryPancake)p4;
 BlueberryPancake b3 = (SourdoughBlueberryPancake2)p4;
<mark> SourdoughBlueberryPancake2 s1 = (BlueberryPancake)p4;</mark>
 SourdoughBlueberryPancake2 s2 = (SourdoughBlueberryPancake2)p4;
    }

}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/SourdoughBlueberryPancake2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect2\exam\SourdoughBlueberryPancake2.java:17: error: incompatible types: BlueberryPancake cannot be converted to SourdoughBlueberryPancake2
 SourdoughBlueberryPancake2 s1 = (BlueberryPancake)p4;
                                 ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
class Ball {
static String s = "";
 void doStuff() { s += "bounce "; }
 }
class Basketball extends Ball {
 void doStuff() { s += "swish "; }
 }
public class Golfball extends Ball{
    
    
    public static void main(String[] args) {
        Ball b = new Golfball();
 <mark>Basketball bb = (Basketball)b;</mark>
 b.doStuff();
 bb.doStuff();
 System.out.println(s);
    }
void doStuff() { s += "fore "; }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/exam/Golfball.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj5/sect2/exam/Golfball
Exception in thread "main" java.lang.ClassCastException: com.mvivekweb.ocpjp6.obj5.sect2.exam.Golfball cannot be cast to com.mvivekweb.ocpjp6.obj5.sect2.exam.Basketball
        at com.mvivekweb.ocpjp6.obj5.sect2.exam.Golfball.main(Golfball.java:17)

        C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>
Variable <mark>b refers to a subtype
(Golfball), and it can be cast only to another Golfball reference variable</mark>.</p>

<pre>
class Bird {
public static String s = "";
 public <mark>static</mark> void fly() { s += "fly "; }
 }
public class Hummingbird extends Bird{
    
public <mark>static</mark> void fly() { s += "hover "; }
    public static void main(String[] args) {
        Bird b1 = new Bird();
Bird b2 = new Hummingbird();
 Bird b3 = (Hummingbird)b2;
 Hummingbird b4 = (Hummingbird)b2;
     
 b1.fly(); b2.fly(); b3.fly(); b4.fly();
 System.out.println(s);
    }
        
}
</pre>
<pre class='out'>
run:
fly fly fly hover 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>Remember, polymorphic invocations apply only to instance methods, <mark>not
    static methods</mark>.</p>
    
    <pre>
package com.mvivekweb.ocpjp6.obj5.sect2.exam;
    
    
public class Clover extends Harrier{
String bark() { return "feed me "; }
    
    public static void main(String[] args) {
        Dog[] dogs = new Dog[3];
dogs[0] = new Harrier();
 dogs[1] = (Dog)new Clover();
 dogs[2] = (Dog)new Harrier();
 for(Dog d: dogs) System.out.print(d.bark());
    }
        
}
class Dog { String bark() { return "bark "; } }
 class Harrier extends Dog { String bark() { return "woof "; } }    
     
     
    </pre>
 <pre class='out'>   
run:
woof feed me woof BUILD SUCCESSFUL (total time: 0 seconds)    </pre>