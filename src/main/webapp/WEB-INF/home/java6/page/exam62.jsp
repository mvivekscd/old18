<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
import java.util.HashMap;
import java.util.Map;
    
    
public class Birthdays {
    
    
    public static void main(String[] args) {
        Map&lt;Friends, String&gt; hm = new <mark>HashMap</mark>&lt;<mark>Friends</mark>, String&gt;();
 hm.put(new Friends("Charis"), "Summer 2009");
 hm.put(new Friends("Draumur"), "Spring 2002");
 Friends f = new Friends(args[0]);
 System.out.println(hm.get(f));
    }
        
}
class Friends {
 String name;
 Friends(String n) { name = n; }
 }
 </pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect2/exam/Birthdays.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj6/sect2/exam/Birthdays Draumur
<mark>null</mark>

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>The Friends class doesn't override equals() and hashCode(), so the
    key to the HashMap is a specific instance of Friends, not the value of a given Friends
    instance's name.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
    
public class Chilis {
    Chilis(String c, int h) { color = c; hotness = h; }
 String color;
 int hotness;
 <mark>public boolean equals(Object o) {
 if(this == (Chilis)o) return true;
 return false;
 }</mark>
 public String toString() { return color + " " + hotness; }
}
</pre>

<p>As it stands, the equals() method has been legally overridden.</p>

<p>It's possible for such keys to find the correct entries in the Map.</p>

<p>As it stands, the Chilis class legally supports the equals() and hashCode() contracts.</p>

<p>If a class does NOT override equals() and hashCode(), class
    Object provides a default implementation. The default implementation's logic is that only
    two references to THE SAME OBJECT will be considered equal.
    Given that, it might appear that equals() has been overridden, but in practice this
    overridden equals() method performs exactly as the default method performs. Therefore,
    the equals() and hashCode() methods will support their contracts, although it will be
    hard for a programmer to use this class for keys in a Map.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
import java.util.PriorityQueue;
    
    
public class Priorities {
    
    
    public static void main(String[] args) {
        PriorityQueue toDo = new PriorityQueue();
 toDo.add("dishes");
 toDo.add("laundry");
 toDo.add("bills");
<mark> toDo.offer("bills");</mark>
 System.out.print(toDo.size() + " " + toDo.poll());
 System.out.print(" " + toDo.peek() + " " + toDo.poll());
 System.out.println(" " + toDo.poll() + " " + toDo.poll());
    }
        
}
</pre>
<pre class='out'>
run:
4 bills bills bills dishes laundry
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>The <mark>add() and offer() methods have the same functionality</mark>, and it's okay
    for PriorityQueues to have duplicate elements. Remembering that PriorityQueues
    are sorted, the poll() method removes the first element in the queue and returns it. The
    peek() method returns the first element from the queue but does NOT remove it.</p>

<pre>Given:
1. class MyClass { }
And given that MyClass2 has properly overridden equals() and hashCode(), objects from
which classes make good hashing keys? (Choose all that apply.)</pre>
<pre class='out'>
MyClass2
C. java.lang.String
D. java.lang.Integer</pre>

<p>In order for a class's instances to make good hashing keys, the
    class must properly override equals() and hashCode(). The String class and wrapper
    classes override these methods; MyClass and StringBuilder do not.</p>


<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
    
public class Chilis1 {
    
    
        Chilis1(String c, int h) { color = c; hotness = h; }
 String color;
 private int hotness;
 public boolean equals(Object o) {
 Chilis1 c = (Chilis1)o;
 if(color.equals(c.color) && (hotness == c.hotness)) return true;
 return false;
 }
     
     
     
}
</pre>
<pre class='out'>
public int hashCode() { return 7; }
 public int hashCode() { return hotness; }
 public int hashCode() { return color.length(); }

 public int hashCode() { return (color.length() + hotness); }</pre>

<p>They all guarantee that two objects that equals() says are
    equal, will return the same integer from hashCode(). The fact that hotness is private
    has no bearing on the legality or effectiveness of the hashCode() method.</p>

<p><mark>public int hashCode() { return (int)(Math.random() * 200); }</mark></p>

<p>incorrect because the random() method will usually return different integers for two
    objects that equals() says are equal.</p>

<pre>It would also be good for you to know the following: If a map ended up
containing a large number of Chilis-based keys, which of the legal hashCode() methods
(above) would provide the fastest retrievals?
 Of all the legal hashCode() methods, this one will create the most
hash buckets, which will lead to the fastest retrieval times.</pre>

<pre class='out'><mark>public int hashCode() { return (color.length() + hotness); }</mark></pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
import java.util.HashSet;
import java.util.Set;
    
    
public class MyFriends {
    
String name;
 MyFriends(String s) { name = s; }
    public static void main(String[] args) {
      Set&lt;MyFriends&gt; ms = new <mark>HashSet</mark>&lt;MyFriends&gt;();
 ms.add(<mark>new MyFriends("Bob")</mark>);
 System.out.print(ms + " ");
 ms.add(new MyFriends("Bob"));
 System.out.print(ms + " ");
 ms.add(new MyFriends("Eden"));
 System.out.print(ms + " "); 
    }
<mark>public String toString() { return name; }</mark>
}
</pre>
<pre class='out'>
run:
[Bob] [Bob, Bob] [Eden, Bob, Bob] BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>The MyFriends class doesn't override equals(), so the two "Bob"
    instances are NOT seen as equal when added to the HashSet.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
import java.util.HashMap;
import java.util.Map;
    
    
public class Enchilada {
    
    
    public static void main(String[] args) {
        Map&lt;Chilis2, String&gt; m = new HashMap&lt;Chilis2, String&gt;();
Chilis2 myC = new Chilis2("green", 4);
m.put(new Chilis2("red", 4), "4 alarm");
m.put(new Chilis2("green", 2), "mild");
m.put(myC, "wow");
<mark>Chilis2 c = new Chilis2(args[0], Integer.parseInt(args[1]));</mark>
<mark>System.out.println(m.get(c));</mark>
    }
        
}
class Chilis2 {
Chilis2(String c, int h) { color = c; hotness = h; }
String color;
private int hotness;
<mark>public boolean equals(Object o) {
Chilis2 c = (Chilis2)o;
if(color.equals(c.color) && (hotness == c.hotness)) return true;
return false;
}</mark>
<mark>public int hashCode() { return hotness; }</mark>
}
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect2/exam/Enchilada.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj6/sect2/exam/Enchilada green 4
wow

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
Given:
2. class Chilis {
3. Chilis(String c, int h) { color = c; hotness = h; }
4. String color;
5. int hotness;
6. public boolean equals(Object o) {
7. Chilis c = (Chilis)o;
8. if(this.color == c.color) return true;
9. return false;
10. }
11. public int hashCode() { return (color.length() + hotness); }
12. }
If instances of class Chilis are to be used as keys in a Map, which are true? (Choose all that apply.)</pre>

<pre class='out'>C. The equals() method is reflexive, symmetric, and transitive.
D. The Chilis class CANNOT be used to create keys to reliably add and retrieve entries in a Map.
E. If the hashCode() method is not considered, the equals() method could be an override
that supports the equals() contract.
F. If the equals() method is not considered, the hashCode() method could be an override
that supports the hashCode() contract.</pre>

<p>Taken individually, the equals() and hashCode() methods
    are legal, potentially contract-fulfilling overrides. The problem is that they don't match
    each other. In other words, two objects that are equal according to equals(), do not
    necessarily return the same result from hashCode().</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.exam;
    
class Noodle {
String name;
 Noodle(String n) { name = n; }
}
 class AsianNoodle extends Noodle {
 public boolean equals(Object o) {
 AsianNoodle n = (AsianNoodle)o;
if(name.equals(n.name)) return true;
 return false;
 }
 public int hashCode() { return name.length(); }
 AsianNoodle(String s) { super(s); }
 }
public class Soba extends AsianNoodle{
    
    
    public static void main(String[] args) {
        Noodle n1 = new Noodle("bob"); Noodle n2 = new Noodle("bob");
AsianNoodle a1 = new AsianNoodle("fred");
 AsianNoodle a2 = new AsianNoodle("fred");
 Soba s1 = new Soba("jill"); Soba s2 = new Soba("jill");
System.out.print(n1.equals(n2) + " " + (n1 == n2) + " | ");
System.out.print(<mark>a1.equals(a2)</mark> + " " + (a1 == a2) + " | ");
 System.out.println(<mark>s1.equals(s2)</mark> + " " + (s1 == s2));
    }
Soba(String s) { super(s); }
}
    
    
</pre>
<pre class='out'>
run:
false false | true false | true false
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>Noodle uses the default equals() method that returns true only when
    comparing two references to the same object. <mark>AsianNoodle overrides equals() (and
        Soba inherits the overridden method) and uses the name variable to determine equality.</mark>
    The == operator ALWAYS compares to see whether two reference variables refer to the
    same object.</p>