<pre>Given that the root directory contains a subdirectory called "office" that contains some files
for a Java application, if "X" and "Y" are unknown arguments, and the following command is
invoked from the root directory in order to create a jar file containing the office directory:
jar -cf X Y
Which are true? (Choose all that apply.)
</pre>

<pre class='out'>X should be the file name of the jar file, and Y should be "office".</pre>

<p>when the <mark>"f" option is used, you must specify a file
        name</mark> for the jar file.</p>

<p>it's <mark>not required that the file name end with
        ".jar"</mark></p>

<p>the <mark>META-INF directory is automatically created</mark>
            by this command.</p>

<p>The Java SDK installation directory tree on a Java developer's computer <mark>usually includes a
    subdirectory tree named jre/lib/ext</mark>.</p>

<p>When javac is invoked with a classpath option, and you need to find files in a JAR
        file, the name of the JAR file must be included at the end of the path.</p>
    
<p>JARs can contain many packages, and of course can be used
    on many machines.    </p>

<p>directory structure will be a
    peer to the META-INF directory.</p>
<pre>
Given the following two files containing Light.java and Dark.java:
2. package ec.ram;
3. public class Light{}
4. class Burn{}
2. package ec.ram;
3. public class Dark{}
4. class Melt{}
And if those files are located in the following directory structure:
$ROOT
   |-- Light.java
   |-- Dark.java
   |-- checker
          |-- dira
          |-- dirb
And the following commands are executed, in order, from the ROOT directory:
    
</pre>


<pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src>javac Light.java -cp checker/dira -d checker/dirb

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
$ROOT
   |-- Light.java
   |-- Dark.java
   |-- checker
          |-- dira
          |-- dirb
               |--ec
                   |--ram
                       |--Light.class
                       |--Burn.class</pre>



<pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src>javac Dark.java -cp checker/dirb -d checker/dira

     C:\vivek\java7\ocpjp6\ocpjp6\src></pre>



<pre>
$ROOT
   |-- Light.java
   |-- Dark.java
   |-- checker
          |-- dira
                |--ec
                   |--ram
                       |--Dark.class
                       |--Melt.class
          |-- dirb
               |--ec
                   |--ram
                       |--Light.class
                       |--Burn.class</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac Dark.java -cp checker/dirb -d checker/dira

C:\vivek\java7\ocpjp6\ocpjp6\src>jar -cf checker/dira/a.jar checker

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre class=''out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac Dark.java -cp checker/dirb -d checker/dira

C:\vivek\java7\ocpjp6\ocpjp6\src>jar -cf checker/dira/a.jar checker

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>jar tf checker/dira/a.jar</mark>
META-INF/
META-INF/MANIFEST.MF
checker/
checker/dira/
checker/dira/ec/
checker/dira/ec/ram/
checker/dira/ec/ram/Dark.class
checker/dira/ec/ram/Melt.class
checker/dirb/
checker/dirb/ec/
checker/dirb/ec/ram/
<mark>checker/dirb/ec/ram/Burn.class</mark>
checker/dirb/ec/ram/Light.class

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
        
<p>the -d switch is used to specify the destination directory, so there
    will be <mark>no impact on the classpath directory</mark> during compilation or JAR file creation.  </p>

<pre>
Given:
2. package w.x;
3. import w.y.Zoom;
4. public class Zip {
5. public static void main(String[] args) {
6. new Zoom().doStuff();
7. } }
And the following command to compile Zip.java: javac -cp w.jar -d . Zip.java
And another class w.y.Zoom (with a doStuff() method), which has been compiled and
deployed into a JAR file w.jar:
Fill in the blanks using the following fragments to show the directory structure and the contents
of any directory(s) necessary to compile successfully from $ROOT. Note: Not all the blanks must
be filled, not all the fragments will be used, and fragments cannot be used more than once.
</pre>
<pre class='out'>
Answer (for Objective 7.5):
$ROOT
   |-- Zip.java
   |-- [w.jar]
          |-- w
              |-- y
                   |-- Zoom.class</pre>
<pre>
Given the following three files:
2. package apollo;
3. import apollo.modules.Lunar;
4. public class Saturn {
5. public static void main(String[] args){
6. Lunar lunarModule = new Lunar();
7. System.out.println(lunarModule);
8. } }
2. package apollo.modules;
3. public interface Module { /* more code */ }
2. package apollo.modules;
3. public class Lunar implements Module { /* more code */ }
And given that Module.java and Lunar.java were successfully compiled and the directory
structure is shown below:
$ROOT
  |-- apollo
  |     |-- modules
  |            |-- Lunar.class
  |-- controls.jar
  |      |-- apollo
  |            |-- modules
  |                  |-- Module.class
  |-- Saturn.java
Which are correct about compiling and running the Saturn class from the $ROOT directory?
(Choose all that apply.)</pre>

<pre class='out'>A. The command for compiling is javac -d . -cp . Saturn.java</pre>

<pre class='out'>The command for compiling is javac -d . -cp <mark>.:controls.jar</mark> Saturn.java</pre>

<pre class='out'>The command for running is java -cp .:controls.jar <mark>apollo.Saturn</mark></pre>

<p>In order to successfully compile the Saturn class, you ONLY need
    the Lunar class to be on the classpath</p>

<p>because in order to successfully run the Saturn class, the classpath should
    include BOTH the JAR file and the base location of the Saturn and Lunar classes. Here,
    the JAR file is required because the Lunar class has to access the Module class at runtime.
    When specifying multiple locations for the classpath, each location should be separated
    with the ":" symbol, although in real life that character depends on the platform.</p>

<pre>
Given:
2. package pack.clients;
3. import pack.banking.Bank;
4. public class Client{
5. public static void main(String[] args){
6. Bank bank = new Bank();
7. System.out.println(bank.getMoney(2000L));
8. } }
And given that Client.java resides in the $ROOT directory and is not yet compiled. There
is another class named pack.banking.Bank, which has a method called getMoney(long)
that returns a value. Bank class is compiled and deployed into a JAR file called pack.jar, as
shown in the following directory structure
$ROOT
   |-- Client.java
   |-- [pack.jar]
        |-- pack
            |-- banking
                    |-- Bank.class
                        
Which are correct about compiling or running the Client class from the $ROOT directory?
(Choose all that apply.)
</pre>

<pre class='out'>To compile, use javac -cp pack.jar -d . Client.java
</pre>

<pre class='out'>To run, use java -cp .:pack.jar pack.clients.Client</pre>

<pre>
Given:
2. import rt.utils.Remote;
3. public class Controller{
4. public static void main(String[] args){
5. Remote remote = new Remote();
6. } }
And rt.utils.Remote class is properly bundled into a JAR file called rtutils.jar.
And given the following steps:
P. Place rtutils.jar in the $ROOT directory.
Q. Extract rtutils.jar and put rt directory with its subdirectories in the $ROOT directory.
R. Extract rtutils.jar and place Remote.class in the $ROOT directory.
S. Place rtutils.jar in the $JAVA_HOME/jre/lib/ext directory.
X. Compile using: javac -cp rtutils.jar Controller.java
Y. Compile using: javac Controller.java
Z. Compile using: javac -cp . Controller.java
If Controller.java resides in the $ROOT directory, which set(s) of steps will compile the
Controller class? (Choose all that apply.)
    
</pre>
<pre class='out'>
A. P and X
B. Q and  Y
F. S -> X
G. S -> Z</pre>

<p>When the JAR is placed in any directory other than $JAVA_
    HOME/jre/lib/ext directory, it should be included in the classpath. If the JAR file is in
    the $JAVA_HOME/jre/lib/ext directory, it's NOT necessary to include the JAR in the
    classpath. If the JAR file is extracted, the complete directory structure should exist on the
    classpath.
</p>