<h3>Use capabilities in the java.util package to write code to manipulate a list by sorting,
    performing a binary search, or converting the list to an array. Use capabilities in the
    java.util package to write code to manipulate an array by sorting, performing a binary
    search, or converting the array to a list. Use the java.util.Comparator and java.lang.
    Comparable interfaces to affect the sorting of lists and arrays. Furthermore, recognize the
    effect of the "natural ordering" of primitive wrapper classes and java.lang.String on sorting.</h3>


<h3>ArrayList Basics</h3>

<p><mark>advantages</mark> ArrayList has <mark>over arrays</mark> are</p>

It can grow dynamically.
<br>It provides more powerful insertion and search mechanisms than arrays.

<p>ArrayList <mark>polymorphically</mark></p>
<pre>
<mark>List</mark> myList = new <mark>ArrayList</mark>();
</pre>
<pre>
List&lt;String&gt; myList = new ArrayList&lt;String&gt;();
        </pre>

<p>This kind of declaration follows the object oriented programming principle of
    <mark>"coding to an interface"</mark>, and it makes use of generics.</p>


<p>ArrayList&lt;String&gt; is similar to a String[] in that it declares a
    container that can hold only Strings, but <mark>it's more powerful than a String[]</mark>.</p>


<pre>
import java.util.ArrayList;
import java.util.List;
    
public class Test001 {
    
    public static void main(String args[]) {
        List&lt;String&gt; test = new ArrayList&lt;String&gt;();
        String s = "hi";
        test.<mark>add</mark>("string");
        test.add(s);
        test.add(s + s);
        System.out.println(test.<mark>size()</mark>);
        System.out.println(test.<mark>contains(42)</mark>);
        System.out.println(test.contains("hihi"));
        test.<mark>remove</mark>("hi");
        System.out.println(test.size());
    }
}
    
</pre>
<pre class='out'>
run:
3
false
true
2
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>
    ask the ArrayList for
    its size, we were able to ask it whether it <mark>contained specific objects</mark>, we <mark>removed an
        object right out from the middle</mark> of it, and then we rechecked its size.
    
</p>

<h3>Autoboxing with Collections</h3>

<p>In general, <mark>collections can hold Objects</mark> but <mark>not primitives</mark>.</p>

<p>primitives <mark>still have to be wrapped, but autoboxing</mark>
    takes care of it for you.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
    
import java.util.ArrayList;
import java.util.List;
    
public class Autoboxexample {
    
    public static void main(String args[]) {
        List myInts = new ArrayList(); // pre Java 5 declaration
        myInts.add(<mark>new Integer(42)</mark>);// had to wrap an int
        myInts.add(<mark>42); // autoboxing handles it!</mark>
            
        System.out.println("myInts.indexOf(42)="+myInts<mark>.indexOf(42)</mark>);
    }
}
    
</pre>

<pre class='out'>
run:
myInts.indexOf(42)=0
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>



<h3>Sorting Collections and Arrays</h3>
<p>
    Both
    <mark>collections and arrays</mark> can be <mark>sorted and searched</mark> using methods</p>

<h3>
    Sorting Collections</h3>


<p>ArrayList <mark>doesn't give you any way to sort</mark> its contents</p>

<p>but the <mark>java.util.Collections class does</mark></p>

<pre>
import java.util.*;
    
public class TestSort1 {
    
    public static void main(String[] args) {
        ArrayList&lt;String&gt; stuff = new ArrayList&lt;String&gt;(); // #1
        stuff.add("Denver");
        stuff.add("Boulder");
        stuff.add("Vail");
        stuff.add("Denver");
        stuff.add("Aspen");
        stuff.add("Telluride");
        System.out.println("unsorted " + stuff);
        <mark>Collections.sort</mark>(stuff); // #2
        System.out.println("sorted " + stuff);
    }
}
</pre>
<pre class='out'>
run:
unsorted [Denver, Boulder, Vail, Denver, Aspen, Telluride]
sorted [Aspen, Boulder, Denver, Denver, Telluride, Vail]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<p>sort() method takes a <mark>List argument</mark>, and that the <mark>objects in
        the List</mark> <mark>must implement</mark> an interface called <mark>Comparable</mark>.</p>

<p>
    It turns out that <mark>String
        implements Comparable</mark>, and <mark>that's why we were able to sort a list of Strings</mark> using
    the Collections.sort() method.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.ArrayList;
import java.util.Collections;
    
public class TestSort2 {
    
    public static void main(String[] args) {
        ArrayList<mark>&lt;Vivek&gt;</mark> stuff = new ArrayList&lt;Vivek&gt;(); // #1
        Vivek vi = new Vivek();
        vi.age = 35;
        stuff.add(vi);
            
        Vivek vi1 = new Vivek();
        vi1.age = 36;
        stuff.add(vi1);
            
        Vivek vi2 = new Vivek();
        vi2.age = 35;
        stuff.add(vi2);
        System.out.println("unsorted " + stuff);
        for (Vivek v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getAge());
        }
        Collections.sort(stuff); // #2
        System.out.println("sorted " + stuff);
            
        for (Vivek v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getAge());
        }
    }
        
}
    
class Vivek {
    
    public int getAge() {
        return age;
    }
        
    public void setAge(int age) {
        this.age = age;
    }
    int age;
        
//    public int compareTo(Vivek o) {
//        return age - o.getAge();
//    }
    
}
    
    
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect5/arraylist/TestSort2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect5\arraylist\TestSort2.java:25: error: no suitable method found for sort(ArrayList&lt;Vivek&gt;)
        Collections.sort(stuff); // #2
                   ^
   method Collections.&lt;T#1&gt;sort(List&lt;T#1&gt;) is not applicable
      (inferred type does not conform to upper bound(s)
        inferred: Vivek
        upper bound(s): Comparable&lt;? super Vivek&gt;)
    method Collections.&lt;T#2&gt;sort(List&lt;T#2&gt;,Comparator&lt;? super T#2&gt;) is not applicable
      (cannot infer type-variable(s) T#2
        (actual and formal argument lists differ in length))
  where T#1,T#2 are type-variables:
   T#1 extends Comparable&lt;? super T#1&gt; declared in method &lt;T#1&gt;sort(List&lt;T#1&gt;)
 T#2 extends Object declared in method &lt;T#2&gt;sort(List&lt;T#2&gt;,Comparator&lt;? super T#2&gt;)
   
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;

import java.util.ArrayList;
import java.util.Collections;

public class TestSort2 {

    public static void main(String[] args) {
       ArrayList&lt;Vivek&gt; stuff = new ArrayList&lt;Vivek&gt;(); // #1
        Vivek vi = new Vivek();
        vi.age = 35;
        stuff.add(vi);

        Vivek vi1 = new Vivek();
        vi1.age = 36;
        stuff.add(vi1);

        Vivek vi2 = new Vivek();
        vi2.age = 35;
        stuff.add(vi2);
        System.out.println("unsorted " + stuff);
        for (Vivek v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getAge());
        }
        Collections.sort(stuff); // #2
        System.out.println("sorted " + stuff);

        for (Vivek v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getAge());
        }
    }

}

class Vivek <mark>implements Comparable&lt;Vivek&gt; </mark>{

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    int age;

    <mark>public int compareTo(Vivek o) {
        return age - o.getAge();
    }</mark>

}


</pre>
<pre class='out'>
run:
unsorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@15db9742, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@6d06d69c, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@7852e922]
v1.getAge()=35
v1.getAge()=36
v1.getAge()=35
sorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@15db9742, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@7852e922, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Vivek@6d06d69c]
v1.getAge()=35
v1.getAge()=35
v1.getAge()=36
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>



<h3>The Comparable Interface</h3>

<p>The Comparable interface is used by the <mark>Collections.sort()</mark> method</p>


<p>To implement Comparable, a class <mark>must implement</mark> a single method,
    <mark>compareTo()</mark>.</p>

<pre>
int x = thisObject.compareTo(anotherObject);
    
</pre>

<p>The compareTo() method returns an int with the following characteristics:</p>
<pre>
negative- If thisObject < anotherObject
zero - If thisObject == anotherObject
positive - If thisObject > anotherObject
    
</pre>

<p>The sort() method uses compareTo() to determine how the List or object array
    should be sorted.</p>


<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.ArrayList;
import java.util.Collections;
    
public class TestSortObjectString {
    
    public static void main(String[] args) {
        ArrayList&lt;DVDInfo&gt; stuff = new ArrayList&lt;DVDInfo&gt;(); // #1
            
        DVDInfo info1 = new DVDInfo();
        DVDInfo info2 = new DVDInfo();
        DVDInfo info3 = new DVDInfo();
            
        info1.title = "viv";
        info2.title = "manj";
        info3.title = "maan";
            
        stuff.add(info1);
        stuff.add(info2);
        stuff.add(info3);
            
        System.out.println("unsorted " + stuff);
         for (DVDInfo v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getTitle());
        }
        Collections.sort(stuff);
        System.out.println("unsorted " + stuff);
         for (DVDInfo v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getTitle());
        }
    }
        
}
    
class DVDInfo implements Comparable&lt;DVDInfo&gt;{
    
    public String getTitle() {
        return title;
    }
        
    public void setTitle(String title) {
        this.title = title;
    }
    String title;
        
    public int compareTo(DVDInfo o) {
        return <mark>title.compareTo(o.getTitle());</mark>
    }
        
}
    
    
</pre>
<pre class='out'>
run:
unsorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@15db9742, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@6d06d69c, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@7852e922]
v1.getAge()=viv
v1.getAge()=manj
v1.getAge()=maan
unsorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@7852e922, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@6d06d69c, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo@15db9742]
v1.getAge()=maan
v1.getAge()=manj
v1.getAge()=viv
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>titles are Strings, and that String implements Comparable, this is an
    easy way to sort our DVDInfo objects, by title.
</p>
<p>
    It's important to remember that when you <mark>override equals() you MUST
        take an argument of type Object</mark>, but that when you <mark>override compareTo()</mark> you
    should <mark>take an argument of the type you're sorting</mark>.
    
</p>
<p>
    We <mark>can only implement compareTo() once</mark> in a class
</p>

<h3>Sorting with Comparator</h3>

<p>
    there is an overloaded version of sort() that takes both a <mark>List</mark> AND
    something called a <mark>Comparator</mark>.</p>

<p>
    The Comparator interface gives you the capability
    to <mark>sort a given collection</mark> any number of <mark>different ways</mark>.
    
</p>

<p>
    The other handy thing about
    the Comparator interface is that you <mark>can use it to sort</mark> <mark>instances of any class</mark>-<mark>even</mark>
    <mark>classes you can't modify</mark>
    
</p>

<p>
    The Comparator interface is also
    very easy to implement, having only <mark>one method</mark>, <mark>compare()</mark>
    
</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
    
public class TestSortComparator {
    
    public static void main(String[] args) {
        ArrayList&lt;DVDInfo1&gt; stuff = new ArrayList&lt;DVDInfo1&gt;(); // #1
            
        DVDInfo1 info1 = new DVDInfo1();
        DVDInfo1 info2 = new DVDInfo1();
        DVDInfo1 info3 = new DVDInfo1();
            
        info1.title = "viv";
        info2.title = "manj";
        info3.title = "maan";
            
        stuff.add(info1);
        stuff.add(info2);
        stuff.add(info3);
            
        System.out.println("unsorted " + stuff);
         for (DVDInfo1 v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getTitle());
        }
        GenreSort gs = new GenreSort();
        <mark>Collections.sort(stuff, gs);</mark>
            
        System.out.println("sorted " + stuff);
         for (DVDInfo1 v1 : stuff) {
            System.out.println("v1.getAge()=" + v1.getTitle());
        }
    }
        
}
    
class DVDInfo1 {
    
    public String getTitle() {
        return title;
    }
        
    public void setTitle(String title) {
        this.title = title;
    }
    String title;
        
}
    
class GenreSort <mark>implements Comparator&lt;DVDInfo1&gt;</mark> {
    
    public int <mark>compare(DVDInfo1 one, DVDInfo1 two)</mark> {
        return <mark>one.getTitle().compareTo(two.getTitle())</mark>;
    }
}
    
</pre>

<pre class='out'>
run:
unsorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@15db9742, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@6d06d69c, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@7852e922]
v1.getAge()=viv
v1.getAge()=manj
v1.getAge()=maan
sorted [com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@7852e922, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@6d06d69c, com.mvivekweb.ocpjp6.obj6.sect5.arraylist.DVDInfo1@15db9742]
v1.getAge()=maan
v1.getAge()=manj
v1.getAge()=viv
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<h3>java.lang.Comparable</h3>
<pre>
int objOne.compareTo(objTwo)
</pre>
<pre>
Returns
negative if objOne < objTwo
zero if objOne == objTwo
positive if objOne > objTwo
</pre>
<pre>
You must modify the class whose
instances you want to sort.
    
</pre>
<pre>
Only <mark>one sort sequence can be created</mark>
    
</pre>
<pre>
Implemented frequently in the API by:
<mark>String, Wrapper classes, Date, Calendar</mark>...
    
</pre>

<h3>java.util.Comparator</h3>
<pre>
int compare(objOne, objTwo)
</pre>

<pre>
Returns
negative if objOne < objTwo
zero if objOne == objTwo
positive if objOne > objTwo
</pre>
<pre>
    You build a class separate from the class whose instances you
    want to sort.
</pre>
<pre>
<mark>Many sort sequences </mark>can be created
</pre>
<pre>Meant to be <mark>implemented to sort instances of third-party
classes.</mark></pre>

<h3>Sorting with the Arrays Class</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
    
public class TestArraySort {
    
    
    public static void main(String[] args) {
        <mark>String[] sa = {"one","one", "two", "three", "four"};</mark>
        System.out.println("unsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
        <mark>Arrays.sort</mark>(sa); // #1
        System.out.println("\nsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
one one two three four 
sorted
four one one three two BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    The good news is that <mark>sorting arrays</mark>
    of <mark>objects</mark> is just like <mark>sorting collections of objects</mark>.
    
</p>
<p>
    The Arrays.sort() method is
    overridden in the same way the Collections.sort() method is.
</p>
<pre>
Arrays.sort(arrayToSort)
Arrays.sort(arrayToSort, <mark>Comparator</mark>)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
    
public class TestArrayobject {
    
    
    public static void main(String[] args) {
         <mark>Object</mark>[] sa = {<mark>"one",1</mark>, "two", "three", "four"};
             
         System.out.println("unsorted");
        for (<mark>Object</mark> s : sa) {
            System.out.print(s + " ");
        }
//        Arrays.sort(sa); // #1
//        System.out.println("\nsorted");
//        for (Object s : sa) {
//            System.out.print(s + " ");
//        }
    }
        
}
</pre>
<pre class='out'>
run:
unsorted
one 1 two three four BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
    
public class TestArrayobject {
    
    
    public static void main(String[] args) {
         Object[] sa = {<mark>"one",1</mark>, "two", "three", "four"};
             
         System.out.println("unsorted");
        for (Object s : sa) {
            System.out.print(s + " ");
        }
       <mark> Arrays.sort(sa); // #1</mark>
        System.out.println("\nsorted");
        for (Object s : sa) {
            System.out.print(s + " ");
        }
    }
        
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect5/arrays/TestArrayobject.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj6/sect5/arrays/TestArrayobject
unsorted
one 1 two three four Exception in thread "main" java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
        at java.lang.Integer.compareTo(Unknown Source)
        at java.util.ComparableTimSort.countRunAndMakeAscending(Unknown Source)
        at java.util.ComparableTimSort.sort(Unknown Source)
        at java.util.Arrays.sort(Unknown Source)
        at com.mvivekweb.ocpjp6.obj6.sect5.arrays.TestArrayobject.main(TestArrayobject.java:18)

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class TestArraySortComparator {
    
    
    public static void main(String[] args) {
        String[] sa = {"one","one", "two", "three", "four"};
        System.out.println("unsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
        ReSortComparator rs = new ReSortComparator();
        <mark>Arrays.sort(sa,rs);</mark> // #1
        System.out.println("\nsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
class ReSortComparator implements Comparator&lt;String&gt;{
    
    
    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
one one two three four 
sorted
four one one three two BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class TestArraySortComparator {
    
    
    public static void main(String[] args) {
        String[] sa = {"one","one", "two", "three", "four"};
        System.out.println("unsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
        ReSortComparator rs = new ReSortComparator();
        Arrays.sort(sa,rs); // #1
        System.out.println("\n reverse sorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
class ReSortComparator implements Comparator&lt;String&gt;{
    
    
    public int compare(String o1, String o2) {
        return <mark>o2.compareTo(o1);</mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
one one two three four 
 reverse sorted
two three one one four BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    The Arrays.sort()
    methods that <mark>sort primitives always sort based on natural order</mark>.
    
</p>
<p>
    Don't be fooled by
    an exam question that tries to sort a primitive array using a Comparator.
</p>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
    
public class TestArraySortprimitive {
    
    
    public static void main(String[] args) {
       <mark> int[] sa = {10,90, 80};</mark>
        System.out.println("unsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
        Arrays.sort(sa); // #1
        System.out.println("\nsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
10 90 80 
sorted
10 80 90 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
import java.util.Arrays;
import java.util.Comparator;
    
    
public class TestArraySortComparatorPrimitive {
    
    
    public static void main(String[] args) {
        <mark>int[]</mark> sa = {10,90, 80};
        System.out.println("unsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
        ReSortComparator1 rs = new ReSortComparator1();
        Arrays.sort(sa,rs); // #1
        System.out.println("\nsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
class ReSortComparator1 implements Comparator&lt;Integer&gt;{
    public int compare(<mark>Integer o1, Integer o2</mark>) {
        return o2-o1;
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect5/arrays/TestArraySortComparatorPrimitive.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect5\arrays\TestArraySortComparatorPrimitive.java:19: error: no suitable method found for sort(int[],ReSortComparator1)
        Arrays.sort(sa,rs); // #1
              ^
    method Arrays.&lt;T#1&gt;sort(T#1[],Comparator&lt;? super T#1&gt;) is not applicable
      (inferred type does not conform to upper bound(s)
        inferred: int
        upper bound(s): Integer,Object)
    method Arrays.&lt;T#2&gt;sort(T#2[],int,int,Comparator&lt;? super T#2&gt;) is not applicable
      (cannot infer type-variable(s) T#2
        (actual and formal argument lists differ in length))
  where T#1,T#2 are type-variables:
   T#1 extends Object declared in method &lt;T#1&gt;sort(T#1[],Comparator&lt;? super T#1&gt;)
    T#2 extends Object declared in method &lt;T#2&gt;sort(T#2[],int,int,Comparator&lt;? super T#2&gt;)
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class TestArraySortComparatorPrimitive {
    
    
    public static void main(String[] args) {
        <mark>Integer[]</mark> sa = {10,90, 80};
        System.out.println("unsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
        ReSortComparator1 rs = new ReSortComparator1();
        Arrays.sort(sa,rs); // #1
        System.out.println("\nsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
    }
        
}
class ReSortComparator1 implements Comparator&lt;Integer&gt;{
    public int compare(Integer o1, Integer o2) {
        return o2-o1;
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
10 90 80 
sorted
90 80 10 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    the<mark> sort() methods</mark> for both the <mark>Collections class</mark> and
    the <mark>Arrays class</mark> <mark>are static methods</mark>, and that <mark>they alter the objects they are sorting</mark>,
    instead of <mark>returning a different sorted object</mark>.
</p>
<p>
    The last rule you'll need to burn in is that, whenever you want to sort an array
    or a collection, <mark>the elements inside must all be mutually comparable</mark>. In other words, if you
    <mark>have an Object[] and you put Cat and Dog objects</mark> into it, <mark>you won't be able to sort</mark>
    it.
    
</p>


<h3>Searching Arrays and Collections</h3>
<p>
    The <mark>Collections</mark> class and the <mark>Arrays</mark> class both provide methods that allow you
    to <mark>search for a specific element</mark>.
    
</p>

<p>
    Searches are performed using the <mark>binarySearch() method</mark>.
    
</p>
<p>
    Successful searches <mark>return the int index</mark> of the element being searched.
    
</p>

<p><mark>Unsuccessful searches</mark> return an int <mark>index</mark> that <mark>represents the insertion point</mark>.
</p>
<p>
    The insertion point <mark>is the place in</mark> the collection/array where the element
    would be inserted <mark>to keep the collection/array properly sorted</mark></p>


<p>Because <mark>positive return values</mark> and <mark>0</mark> indicate successful searches, the binarySearch()
    method uses <mark>negative numbers to indicate insertion points</mark>.</p>


<p>Since 0 is a valid
    result for a successful search, the <mark>first available insertion point</mark> is <mark>-1</mark>.</p>

<p>For
    instance, if the insertion point of a search is at element 2, the <mark>actual insertion
    point returned will be -3</mark>.</p>

<p>The collection/array being searched <mark>must be sorted before you can search</mark> it.</p>

<p>If you attempt to search an array or collection that has not already been
    sorted, <mark>the results of the search will not be predictable</mark>.</p>

<p>
    If the collection/array you want to <mark>search was sorted in natural order, it must
        be searched in natural order.</mark> (Usually this is accomplished by <mark>NOT sending
            a Comparator</mark> as an argument to the binarySearch() method.)
</p>

<p>
    If the collection/array you want to search <mark>was sorted</mark> using a Comparator, it
    <mark>must be searched using the same Comparator</mark>, which is passed as the second
    argument to the binarySearch() method.
    
</p>
<p>
    Remember that Comparators
    cannot be used when searching arrays of primitives.
    
</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
public class TestArraySearch {
    
    public static void main(String[] args) {
        String[] sa = {"two", "three", "four", "one", "one"};
        System.out.println("unsorted");
        for (String s : sa) {
            System.out.print(s + " ");
        }
        System.out.println("\none = "
                + <mark>Arrays.binarySearch(sa, "one")</mark>);
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
two three four one one 
one = 3
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
    
import java.util.Arrays;
import java.util.Comparator;
    
public class SearchObjArray {
    
    public static void main(String[] args) {
        String[] sa = {"one", "two", "three", "four"};
        <mark>Arrays.sort(sa);</mark> // #1
        for (String s : sa) {
            System.out.print(s + " ");
        }
        System.out.println("\none = "
                + Arrays.binarySearch(sa, "one")); // #2
        System.out.println("now reverse sort");
       <mark> ReSortComparator rs = new ReSortComparator(); // #3</mark>
       <mark> Arrays.sort(sa, rs);</mark>
        for (String s : sa) {
            System.out.print(s + " ");
        }
        System.out.println("\none = "
                + <mark>Arrays.binarySearch(sa, "one")</mark>); // #4
        System.out.println("one = "
                + <mark>Arrays.binarySearch(sa, "one", rs)</mark>); // #5
    }
        
    static class ReSortComparator
            implements Comparator&lt;String&gt; { // #6
        public int compare(String a, String b) {
            return b.compareTo(a); // #7
        }
    }
}
    
</pre>
<pre class='out'>
run:
four one three two 
one = 1
now reverse sort
two three one four 
one = -1
one = 2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>When solving searching and sorting questions, two big gotchas are</p>

1. Searching an array or collection that<mark> hasn't been sorted</mark>.
<br>2. Using a <mark>Comparator in either the sort or the search,
but not both</mark>.
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
public class TestArraySearch {
    
    public static void main(String[] args) {
        int[] sa = {34, 23, 36};
        System.out.println("unsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
       // Arrays.sort(sa);
        System.out.println("\none = "
                + <mark>Arrays.binarySearch(sa, 34)</mark>);
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
34 23 36 
<mark>one = -3</mark>
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
    
public class TestArraySearch {
    
    public static void main(String[] args) {
        int[] sa = {34, 23, 36};
        System.out.println("unsorted");
        for (int s : sa) {
            System.out.print(s + " ");
        }
        <mark>Arrays.sort(sa);</mark>
        System.out.println("\none = "
                + <mark>Arrays.binarySearch(sa, 23)</mark>);
    }
        
}
    
</pre>
<pre class='out'>
run:
unsorted
34 23 36 
<mark>one = 0</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Converting Arrays to Lists to Arrays</h3>

<p>There are a <mark>couple of methods</mark> that allow you to convert arrays to Lists, and Lists to
    arrays.</p>

<p>
    The <mark>List</mark> and <mark>Set</mark> classes have <mark>toArray()</mark> methods
    
</p>

<p>the Arrays class has a
    method called <mark>asList()</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arrays;
    
import java.util.Arrays;
import java.util.List;
    
public class ConvertArraytoList {
    
    public static void main(String[] args) {
        String[] sa = {"one", "two", "three", "four"};
        List sList = <mark>Arrays.asList</mark>(sa); // make a List
        System.out.println("size " + sList.size());
        System.out.println("idx2 " + sList.get(2));
        sList.set(3, "six"); // change List
        sa[1] = "five"; // change array
        for (String s : sa) {
            System.out.print(s + " ");
        }
        System.out.println("\nsl[1] " + sList.get(1));
    }
        
}
    
</pre>
<pre class='out'>
run:
size 4
idx2 three
one five three six 
sl[1] five
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
import java.util.Arrays;
import java.util.List;
    
public class ConvertArraytoList {
    
    public static void main(String[] args) {
        String[] sa = {"one", "two", "three", "four"};
        List sList = Arrays.asList(sa); // make a List
        System.out.println("size " + sList.size());
        System.out.println("idx2 " + sList.get(2));
        sList.set(3, "six"); // change List
       <mark> sList.add("vivek");</mark>
        sa[1] = "five"; // change array
        for (String s : sa) {
            System.out.print(s + " ");
        }
        System.out.println("\nsl[1] " + sList.get(1));
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect5/arrays/ConvertArraytoList.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
Note: com\mvivekweb\ocpjp6\obj6\sect5\arrays\ConvertArraytoList.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj6/sect5/arrays/ConvertArraytoList
size 4
idx2 three
Exception in thread "main" java.lang.UnsupportedOperationException
        at java.util.AbstractList.add(Unknown Source)
        at java.util.AbstractList.add(Unknown Source)
        at com.mvivekweb.ocpjp6.obj6.sect5.arrays.ConvertArraytoList.main(ConvertArraytoList.java:15)

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<p>
    when we print the <mark>final state of the array and the List, they have both
    been updated with each other's changes</mark>.</p>
    
<p>
    the <mark>toArray(</mark>) method; it <mark>comes in two flavors</mark>: one that <mark>returns a new
        Object array</mark>, and one that <mark>uses the array you send it as the destination array</mark>:
    </p>
    <pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
    
import java.util.ArrayList;
import java.util.List;
    
public class TesttoArray {
    
    public static void main(String[] args) {
        List&lt;Integer&gt; iL = new ArrayList&lt;Integer&gt;();
        for (int x = 0; x < 3; x++) {
            iL.add(x);
        }
        <mark>Object[] oa = iL.toArray();</mark> // create an Object array
        for (Object s : oa) {
            System.out.print(s + " ");
        }
        <mark>Integer[] ia2 = new Integer[3];
        ia2 = iL.toArray(ia2);</mark> // create an Integer array
        for (Integer s : ia2) {
            System.out.print(s + " ");
        }
    }
}
    
</pre>
 <pre class='out'>   
run:
0 1 2 0 1 2 BUILD SUCCESSFUL (total time: 0 seconds)    
</pre>

<h3>Using Lists</h3>
<p>    <mark>
        use a </mark>LinkedList to create a <mark>first-in, first-out</mark> queue    
    
</p>
<p>
    an <mark>ArrayList</mark> to
    <mark>keep track of what locations were visited</mark>, and in what order.
    
</p>
<p>
    duplicates might occur
</p>

<p>Iterator lets you <mark>loop through the collection</mark> step by
    step.</p>

<p>
    <mark> two Iterator</mark> methods you need to understand for the exam are
    
</p>


<p>boolean <b><mark>hasNext()</mark></b>- Returns true if there is at least one more element in
    the collection being traversed. <mark>Invoking hasNext() does NOT move</mark> you to
    the next element of the collection.</p>

<p>Object <b><mark>next()</mark></b> -This method returns the next object in the collection,
    AND <mark>moves you forward</mark> to the element after the element just returned.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
    
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
    
    
class Dog1 {
    
    public String name;
        
    Dog1(String n) {
        name = n;
    }
}
    
class ItTest {
    
    public static void main(String[] args) {
        List&lt;Dog1&gt; d = new ArrayList&lt;Dog1&gt;();
        Dog1 dog = new Dog1("aiko");
        d.add(dog);
        d.add(new Dog1("clover"));
        d.add(new Dog1("magnolia"));
        <mark>Iterator&lt;Dog1&gt;</mark> i3 = <mark>d.iterator(); </mark>// make an iterator
       <mark> while (i3.hasNext()) {</mark>
            Dog1 d2 = <mark>i3.next();</mark> // cast not required
            System.out.println(d2.name);
        }
        System.out.println("size " + d.size());
        System.out.println("get1 " + d.get(1).name);
        System.out.println("aiko " + d.indexOf(dog));
            
        d.remove(2);
        <mark>Object[] oa = d.toArray();</mark>
        for (Object o : oa) {
            Dog1 d2 = (Dog1) o;
            System.out.println("oa " + d2.name);
        }
            
    }
}
</pre>
<pre class='out'>
run:
aiko
clover
magnolia
size 3
get1 clover
aiko 0
oa aiko
oa clover
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.oldcollections;
import java.util.ArrayList;
    
public class ArrayListDemo {
    public static void main(String args[]) {
      // create an array list
      <mark>ArrayList al = new ArrayList();</mark>
      System.out.println("Initial size of al: " + al.size());
      // add elements to the array list
      al.add("C");
      al.add("A");
      al.add("E");
      al.add("B");
      al.add("C");
      al.add("E");
      al.add("D");
      al.add("F");
      al.add(1, "A2");
      System.out.println("Size of al after additions: " + al.size());
      // display the array list
      System.out.println("Contents of al: " + al);
      // Remove elements from the array list
      al.remove("F");
      al.remove(2);
      System.out.println("Size of al after deletions: " + al.size());
      System.out.println("Contents of al: " + al);
   }
}
    
</pre>
<pre class='out'>
run:
Initial size of al: 0
Size of al after additions: 9
Contents of al: [C, A2, A, E, B, C, E, D, F]
Size of al after deletions: 7
Contents of al: [C, A2, E, B, C, E, D]
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Vector</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.oldcollections;
    
import java.util.Enumeration;
import java.util.Vector;
    
    
public class VectorDemo {
    public static void main(String args[]) {
      // initial size is 3, increment is 2
      Vector v = new Vector<mark>(3, 2)</mark>;
      System.out.println("Initial size: " + v.size());
      System.out.println("Initial capacity: " +
      v.capacity());
      v.addElement(new Integer(1));
      v.addElement(new Integer(2));
      v.addElement(new Integer(3));
      v.addElement(new Integer(4));
      System.out.println("Capacity after four additions: " +
          v.capacity());
              
      v.addElement(new Double(5.45));
      System.out.println("Current capacity: " +
      v.capacity());
      v.addElement(new Double(6.08));
      v.addElement(new Integer(7));
      System.out.println("Current capacity: " +
      v.capacity());
      v.addElement(new Float(9.4));
      v.addElement(new Integer(10));
      System.out.println("Current capacity: " +
      v.capacity());
      v.addElement(new Integer(11));
      v.addElement(new Integer(12));
      System.out.println("First element: " +
         (Integer)v.firstElement());
      System.out.println("Last element: " +
         (Integer)v.lastElement());
      if(v.contains(new Integer(3)))
         System.out.println("Vector contains 3.");
      // enumerate the elements in the vector.
      Enumeration vEnum = v.elements();
      System.out.println("\nElements in vector:");
      while(vEnum.hasMoreElements())
         System.out.print(vEnum.nextElement() + " ");
      System.out.println();
   }
}
    
    
</pre>
<pre class='out'>
run:
Initial size: 0
Initial capacity: 3
Capacity after four additions: 5
Current capacity: 5
Current capacity: 7
Current capacity: 9
First element: 1
Last element: 12
Vector contains 3.

Elements in vector:
1 2 3 4 5.45 6.08 7 9.4 10 11 12 
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>LinkedList</h3>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.oldcollections;
    
import java.util.LinkedList;
    
    
public class LinkedListDemo {
    public static void main(String args[]) {
      // create a linked list
      LinkedList ll = new LinkedList();
      // add elements to the linked list
      ll.add("F");
      ll.add("B");
      ll.add("D");
      ll.add("E");
      ll.add("C");
      ll.<mark>addLast</mark>("Z");
      ll.<mark>addFirst</mark>("A");
      ll.add(1, "A2");
      System.out.println("Original contents of ll: " + ll);
          
      // remove elements from the linked list
      ll.remove("F");
      ll.remove(2);
      System.out.println("Contents of ll after deletion: "
       + ll);
           
      // remove first and last elements
      ll.<mark>removeFirst</mark>();
      ll.<mark>removeLast</mark>();
      System.out.println("ll after deleting first and last: "
       + ll);
           
      // get and set a value
      Object val = ll.get(2);
      ll.set(2, (String) val + " Changed");
      System.out.println("ll after change: " + ll);
   }
}
    
</pre>
<pre class='out'>
run:
Original contents of ll: [A, A2, F, B, D, E, C, Z]
Contents of ll after deletion: [A, A2, D, E, C, Z]
ll after deleting first and last: [A2, D, E, C]
ll after change: [A2, D, E Changed, C]
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>



<h3>Using Sets</h3>

<p><mark>don't want any duplicates</mark> in your collection
</p>

<p>
    <mark>HashSets</mark> tend to be <mark>very fast</mark> because, as we discussed earlier, they <mark>use hashcodes</mark>.
    
</p>
<p>
    can also <mark>create a TreeSet</mark>, which is a Set whose <mark>elements are sorted</mark>
    
</p>

<p>
    <mark>caution</mark> when using a <mark>TreeSet</mark>
</p>

<h3>HashSet</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
    
public class SetTest {
    
    public static void main(String[] args) {
       <mark> boolean[]</mark> ba = new boolean[5];
// insert code here
    
       <mark> Set s = new HashSet();</mark>
       // Set s = new TreeSet();
<mark>ba[0]</mark> = <mark>s.add</mark>("a");
ba[1] = s.add(new Integer(42));
ba[2] = s.add("b");
ba[3] = s.add("a");
ba[4] = s.add(new Object());
for(int x=0; x< ba.length; x++)
System.out.print(ba[x] + " ");
System.out.println("\n");
for(Object o : s)
System.out.print(o + " ");
    }
}
    
</pre>

<pre class='out'>
run:
true true true <mark>false</mark> true 

<mark>a b java.lang.Object@15db9742 42 </mark>BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    important to know that the order of objects printed in the second for loop
    is not predictable: <mark>HashSets do not guarantee any ordering</mark>.
</p>
<p>
    Also, notice that the
    fourth invocation of add() failed, because it attempted to insert a duplicate entry
    (a String with the value a) into the Set.
    
</p>

<h3>TreeSet</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
    
public class SetTest {
    
    public static void main(String[] args) {
        boolean[] ba = new boolean[5];
// insert code here
    
        //Set s = new HashSet();
       <mark> Set s = new TreeSet();</mark>
ba[0] = s.add("a");
ba[1] = s.add(<mark>new Integer(42)</mark>);
ba[2] = s.add("b");
ba[3] = s.add("a");
ba[4] = s.add(new Object());
for(int x=0; x < ba.length; x++)
System.out.print(ba[x] + " ");
System.out.println("\n");
for(Object o : s)
System.out.print(o + " ");
    }
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
	at java.lang.Integer.compareTo(Integer.java:52)
	at java.util.TreeMap.put(TreeMap.java:568)
	at java.util.TreeSet.add(TreeSet.java:255)
	at com.mvivekweb.ocpjp6.obj6.sect5.arraylist.SetTest.main(SetTest.java:16)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>

<p>
    The issue is that <mark>whenever you want a collection to be sorted</mark>, its <mark>elements must
    be mutually comparable</mark>.
    
</p>

<p>Remember that unless otherwise specified, <mark>objects of
    different types are not mutually comparable</mark>.</p>

    <pre>
package com.mvivekweb.ocpjp6.obj6.sect1.oldcollections;
    
import java.util.TreeSet;
    
    
public class TreeSetDemo {
    public static void main(String args[]) {
      // Create a tree set
      TreeSet ts = new TreeSet();
      // Add elements to the tree set
      ts.add("C");
      ts.add("A");
    <mark>  ts.add("A");</mark>
      ts.add("B");
      ts.add("E");
      ts.add("F");
      ts.add("D");
      System.out.println(<mark>ts</mark>);
   }
}
    
    </pre>
<pre class='out'>    
run:
[A, B, C, D, E, F]
BUILD SUCCESSFUL (total time: 0 seconds)    
    
</pre>
    
<h3>LinkedHashSet</h3>    
    
    <pre>
package com.mvivekweb.ocpjp6.obj6.sect1.oldcollections;
    
import java.util.*;
    
public class LinkedHashSetDemo {
    
    public static void main(String args[]) {
        // create a hash set
        LinkedHashSet hs = new LinkedHashSet();
        // add elements to the hash set
        hs.add("B");
        hs.add("A");
         hs.add("A");
        hs.add("D");
        hs.add("E");
        hs.add("C");
        hs.add("F");
        System.out.println(hs);
    }
}</pre>
<pre class='out'>    
run:
[B, A, D, E, C, F]
BUILD SUCCESSFUL (total time: 0 seconds)    </pre>

<h3>Using Maps</h3>

<p>
    Remember that when you use a class that implements Map, <mark>any classes that you
        use as a part of the keys</mark> for that map<mark> must override the hashCode() and equals()</mark>
    methods.
</p>
<p>
    you only have to <mark>override them</mark> <mark>if you're interested in retrieving</mark>
    stuff from your Map. Seriously, it's <mark>legal to use a class</mark> that <mark>doesn't override equals()
        and hashCode() as a key</mark> in a Map; your code will compile and run, <mark>you just won't
    find your stuff</mark>.)
</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashMap;
import java.util.Map;
    
class Dog {
    
    public Dog(String n) {
        name = n;
    }
    public String name;
        
    public boolean equals(Object o) {
        if ((o instanceof Dog)
                && (((Dog) o).name == name)) {
            return true;
        } else {
            return false;
        }
    }
        
    public int hashCode() {
        return <mark>name.length()</mark>;
    }
}
    
class Cat {
}
    
enum Pets {
    
    DOG, CAT, HORSE
}
    
class MapTest {
    
    public static void main(String[] args) {
        Map&lt;Object, Object&gt; m = new HashMap&lt;Object, Object&gt;();
        m.put("k1", new Dog("aiko")); // add some key/value pairs
        m.put("k2", Pets.DOG);
        m.put(Pets.CAT, "CAT key");
        <mark>Dog d1 = new Dog("clover");
        m.put(d1, "Dog key");</mark>
        m.put(<mark>new Cat()</mark>, "Cat key");
        System.out.println(m.get("k1")); // #1
        String k2 = "k2";
        System.out.println(m.get(k2)); // #2
        Pets p = Pets.CAT;
        System.out.println(m.get(p)); // #3
        System.out.println(m.get(d1)); // #4
        System.out.println(m.get(<mark>new Cat()</mark>)); // #5
        System.out.println(m.size()); // #6
    }
}
</pre>
<pre class='out'>
run:
com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Dog@4
DOG
CAT key
<mark>Dog key</mark>
<mark>null</mark>
5
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    The implication of this is that <mark>enums override equals() and hashCode()</mark>. And,
    if you look at the java.lang.Enum class in the API, you will see that, in fact, these
    <mark>methods have been overridden</mark>.
    
</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashMap;
import java.util.Map;
    
class Dog {
    
    public Dog(String n) {
        name = n;
    }
    public String name;
        
    public boolean equals(Object o) {
        System.out.println("name="+name);
        if ((o instanceof Dog)
                && (((Dog) o).name == name)) {
            return true;
        } else {
            return false;
        }
    }
        
    public int hashCode() {
        System.out.println("hashCode="+name.length());
        return <mark>name.length()</mark>;
    }
}
    
class Cat {
}
    
enum Pets {
    DOG, CAT, HORSE
}
    
class MapTest {
    public static void main(String[] args) {
          Map&lt;Object, Object&gt; m = new HashMap&lt;Object, Object&gt;();
        m.put("k1", new Dog("aiko")); // add some key/value pairs
        m.put("k2", Pets.DOG);
        m.put(Pets.CAT, "CAT key");
<mark>        Dog d1 = new Dog("clover");
        m.put(d1, "Dog key");</mark>
        m.put(new Cat(), "Cat key");
        System.out.println(m.get("k1")); // #1
        String k2 = "k2";
        System.out.println(m.get(k2)); // #2
        Pets p = Pets.CAT;
        System.out.println(m.get(p)); // #3
        System.out.println(<mark>m.get(d1)</mark>); // #4
        System.out.println(m.get(new Cat())); // #5
        System.out.println(m.size()); // #6




    }
}
</pre>
<pre class='out'>
run:
<mark>hashCode=6</mark>
hashCode=4
com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Dog@4
DOG
CAT key
<mark>hashCode=6
Dog key</mark>
null
5
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashMap;
import java.util.Map;
    
class Dog {
    
    public Dog(String n) {
        name = n;
    }
    public String name;
        
    public boolean equals(Object o) {
         System.out.println("equals");
        System.out.println("name="+name);
        System.out.println("(Dog) o).name="+((Dog) o).name);
        if ((o instanceof Dog)
                && (((Dog) o).name == name)) {
            return true;
        } else {
            return false;
        }
    }
        
    public int hashCode() {
        System.out.println("hashCode="+name.length());
        return name.length();
    }
}
    
class Cat {
}
    
enum Pets {
    DOG, CAT, HORSE
}
    
class MapTest {
    public static void main(String[] args) {
         Map&lt;Object, Object&gt; m = new HashMap&lt;Object, Object&gt;();
        m.put("k1", new Dog("aiko")); // add some key/value pairs
        m.put("k2", Pets.DOG);
        m.put(Pets.CAT, "CAT key");
        <mark>Dog d1 = new Dog("clover");</mark>
        <mark>m.put(d1, "Dog key");</mark>
        m.put(new Cat(), "Cat key");
               // d1.name =" "arthur";
        <mark>System.out.println("m.get(new Dog('clove1'))="+m.get(new Dog("clove1")));</mark>
            
        System.out.println(m.get("k1")); // #1
        String k2 = "k2";
        System.out.println(m.get(k2)); // #2
        Pets p = Pets.CAT;
        System.out.println(m.get(p)); // #3
        System.out.println(m.get(d1)); // #4
        System.out.println(m.get(new Cat())); // #5
        System.out.println(m.size()); // #6
            
        d1.name = "arthur";
        System.out.println(m.get(new Dog("clover")));
            
    }
}
    
</pre>
<pre class='out'>
run:
<mark>hashCode=6
hashCode=6
equals
name=clove1
(Dog) o).name=clover
m.get(new Dog('clove1'))=null</mark>
hashCode=4
com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Dog@4
DOG
CAT key
hashCode=6
Dog key
null
5
hashCode=6
equals
name=clover
(Dog) o).name=arthur
null
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.arraylist;
    
import java.util.HashMap;
import java.util.Map;
    
class Dog {
    
    public Dog(String n) {
        name = n;
    }
    public String name;
        
    public boolean equals(Object o) {
         System.out.println("equals");
        System.out.println("name="+name);
        System.out.println("(Dog) o).name="+((Dog) o).name);
        if ((o instanceof Dog)
                && (((Dog) o).name == name)) {
            return true;
        } else {
            return false;
        }
    }
        
    public int hashCode() {
        System.out.println("hashCode="+name.length());
        return name.length();
    }
}
    
class Cat {
}
    
enum Pets {
    DOG, CAT, HORSE
}
    
class MapTest {
    public static void main(String[] args) {
       Map&lt;Object, Object&gt; m = new HashMap&lt;Object, Object&gt;();
        m.put("k1", new Dog("aiko")); // add some key/value pairs
        m.put("k2", Pets.DOG);
        m.put(Pets.CAT, "CAT key");
        <mark>Dog d1 = new Dog("clover");
        m.put(d1, "Dog key");
        Dog d2 = new Dog("clove1");
        m.put(d2, "Dog key1");</mark>
        m.put(new Cat(), "Cat key");
               // d1.name =" "arthur";
        <mark>System.out.println("m.get(new Dog('clover'))="+m.get(new Dog("clover")));</mark>
            
        System.out.println(m.get("k1")); // #1
        String k2 = "k2";
        System.out.println(m.get(k2)); // #2
        Pets p = Pets.CAT;
        System.out.println(m.get(p)); // #3
        System.out.println(m.get(d1)); // #4
        System.out.println(m.get(new Cat())); // #5
        System.out.println(m.size()); // #6
            
        d1.name = "arthur";
        System.out.println(m.get(new Dog("clover")));
            
    }
}
    
</pre>
<pre class='out'>
run:
hashCode=6
<mark>hashCode=6
equals
name=clove1
(Dog) o).name=clover
hashCode=6
equals
name=clover
(Dog) o).name=clover
m.get(new Dog('clover'))=Dog key</mark>
hashCode=4
com.mvivekweb.ocpjp6.obj6.sect5.arraylist.Dog@4
DOG
CAT key
hashCode=6
Dog key
null
6
hashCode=6
equals
name=clover
(Dog) o).name=arthur
equals
name=clover
(Dog) o).name=clove1
null
BUILD SUCCESSFUL (total time: 0 seconds)


</pre>

<p>When
    you study a problem like this, it can be useful to think of the two stages of retrieval</p>
<pre>
1. Use the <mark>hashCode() method</mark> to <mark>find the correct bucket</mark>
2. Use the <mark>equals() method to find the object</mark> in the bucket
    
</pre>

<p>
    the hashcode test succeeds, but the equals() test
    fails because <mark>arthur is NOT equal to clover</mark>.
</p>





