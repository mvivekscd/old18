<h3>    Encapsulation, IS-A, HAS-A (Objective 5.1)</h3>
        <p> Encapsulation helps hide implementation behind an interface (or API).</p>
        <p> Encapsulated code has two features:</p>
        <p> Instance variables are kept protected (usually with the private modifier).</p>
        <p> Getter and setter methods provide access to instance variables.</p>
        <p> IS-A refers to inheritance or implementation.</p>
        <p> IS-A is expressed with the keyword extends.</p>
        <p> IS-A, "inherits from," and "is a subtype of " are all equivalent expressions.</p>
        <p> HAS-A means an instance of one class "has a" reference to an instance of
            another class or another instance of the same class.</p>

<h3>Coupling and Cohesion (Objective 5.1)</h3>
<p> Coupling refers to the degree to which one class knows about or uses members
    of another class.</p>
<p> Loose coupling is the desirable state of having classes that are well encapsulated,
    minimize references to each other, and limit the breadth of API usage.</p>
<p> Tight coupling is the undesirable state of having classes that break the rules of
    loose coupling.</p>
<p> Cohesion refers to the degree in which a class has a single, well-defined role
    or responsibility.</p>
<p> High cohesion is the desirable state of a class whose members support a
    single, well-focused role or responsibility.</p>
<p> Low cohesion is the undesirable state of a class whose members support multiple,
    unfocused roles or responsibilities.</p>


<h3>Polymorphism (Objective 5.2)</h3>
<p> Polymorphism means "many forms."</p>
<p> A reference variable is always of a single, unchangeable type, but it can refer
    to a subtype object.</p>
<p> A single object can be referred to by reference variables of many different
    types ?as long as they are the same type or a supertype of the object.</p>
<p> The reference variable's type (not the object's type), determines which
    methods can be called!</p>
<p> Polymorphic method invocations apply only to overridden instance methods.</p>

<h3>Reference Variable Casting (Objective 5.2)</h3>
<p> There are two types of reference variable casting: downcasting and upcasting.</p>
<p> Downcasting: If you have a reference variable that refers to a subtype object,
    you can assign it to a reference variable of the subtype. You must make an
    explicit cast to do this, and the result is that you can access the subtype's
    members with this new reference variable.</p>
<p> Upcasting: You can assign a reference variable to a supertype reference variable
    explicitly or implicitly. This is an inherently safe operation because the
    assignment restricts the access capabilities of the new variable.</p>


<h3>Constructors and Instantiation (Objectives 1.6 and 5.4)</h3>
<p> A constructor is always invoked when a new object is created.</p>
<p> Each superclass in an object's inheritance tree will have a constructor called.</p>
<p> Every class, even an abstract class, has at least one constructor.</p>
<p> Constructors must have the same name as the class.</p>
<p> Constructors don't have a return type. If you see code with a return type, it's a
    method with the same name as the class, it's not a constructor.</p>
<p> Typical constructor execution occurs as follows:</p>
<p> The constructor calls its superclass constructor, which calls its superclass
    constructor, and so on all the way up to the Object constructor.</p>
<p> The Object constructor executes and then returns to the calling
    constructor, which runs to completion and then returns to its calling
    constructor, and so on back down to the completion of the constructor of
    the actual instance being created.</p>
<p> Constructors can use any access modifier (even private!).</p>
<p> The compiler will create a default constructor if you don't create any constructors
    in your class.</p>
<p> The default constructor is a no-arg constructor with a no-arg call to super().</p>
<p> The first statement of every constructor must be a call to either this() (an
    overloaded constructor) or super().</p>
<p> The compiler will add a call to super() unless you have already put in a call
    to this() or super().</p>
<p> Instance members are accessible only after the super constructor runs.</p>
<p> Abstract classes have constructors that are called when a concrete
    subclass is instantiated.</p>
<p> Interfaces do not have constructors.</p>
<p> If your superclass does not have a no-arg constructor, you must create a constructor
    and insert a call to super() with arguments matching those
    of the superclass constructor.</p>
<p> Constructors are never inherited, thus they cannot be overridden.</p>
<p> A constructor can be directly invoked only by another constructor (using
    a call to super() or this()).</p>
<p> Issues with calls to this()</p>
<p> May appear only as the first statement in a constructor.</p>
<p> The argument list determines which overloaded constructor is called.</p>

<h3>Inheritance (Objective 5.5)</h3>
<p> Inheritance allows a class to be a subclass of a superclass, and thereby
    inherit public and protected variables and methods of the superclass.</p>
<p> Inheritance is a key concept that underlies IS-A, polymorphism, overriding,
    overloading, and casting.</p>
<p> All classes (except class Object), are subclasses of type Object, and therefore
    they inherit Object's methods.</p>
<p>
     Constructors can call constructors can call constructors, and so on, but
    sooner or later one of them better call super() or the stack will explode.</p>
<p> Calls to this() and super() cannot be in the same constructor. You can
    have one or the other, but never both.</p>
