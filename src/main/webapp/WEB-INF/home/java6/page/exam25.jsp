<pre>package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
public class Excep1 {
static String s = "";
    public static void main(String[] args) {
try { doStuff(args); }
 catch (Error e) { s += "e "; }
 s += "x ";
 System.out.println(s);
    }
static void doStuff(String[] args) {
<mark> if(args.length == 0){</mark>
     System.out.println("args = 0");
 throw new IllegalArgumentException();
 }
 s += "d ";
 }
}</pre>
<pre class='out'>
run:
args = 0
Exception in thread "main" java.lang.IllegalArgumentException
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Excep1.doStuff(Excep1.java:14)
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Excep1.main(Excep1.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
public class Ladder {
    
    public static void main(String[] args) {
try {
 System.out.println(doStuff(args));
}
 catch (Exception e) { System.out.println("exc"); }
doStuff(args);
    }
static int doStuff(String[] args) {
 return Integer.parseInt(args[0]);
 }
}
</pre>
<pre class='out'>
run:
<mark>exc
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0</mark>
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Ladder.doStuff(Ladder.java:13)
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Ladder.main(Ladder.java:10)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>





<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect5/exam/Ladder.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect5/exam/Ladder x</mark>
<mark>exc
Exception in thread "main" java.lang.NumberFormatException: For input string: "x"</mark>
        at java.lang.NumberFormatException.forInputString(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at com.mvivekweb.ocpjp6.obj2.sect5.exam.Ladder.doStuff(Ladder.java:13)
        at com.mvivekweb.ocpjp6.obj2.sect5.exam.Ladder.main(Ladder.java:10)

        C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
    
    
public class SticksMud {
    
    
    public static void main(String[] args) {
      try {
File dir = new File("dir");
dir.createNewFile();
}  
      catch (IllegalArgumentException arg) { } // flexible
          
      catch (FileNotFoundException fnf) { } // flexible
      catch (IOException io) { }
      catch (ClassCastException cc) { } // flexible
          
catch (NullPointerException np) { } // can't be FileNotFound
<mark>      catch (RuntimeException rt) { } // must go here
    
    
    
catch (Exception e) { } // must go here</mark>
    
catch (Throwable t) { }
    }
        
}
    
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
    
    
public class SticksMud {
    
    
    public static void main(String[] args) {
      try {
File dir = new File("dir");
dir.createNewFile();
}  
    
<mark>      catch (FileNotFoundException fnf) { } // flexible
      catch (IOException io) { }</mark>
        catch (NullPointerException np) { } // can't be FileNotFound
      catch (IllegalArgumentException arg) { } // flexible
       catch (ClassCastException cc) { } // flexible   
           
           
      catch (RuntimeException rt) { } // must go here
          
          
          
catch (Exception e) { } // must go here
    
catch (Throwable t) { }
    }
        
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
public class Houdini {
    
    public static void main(String[] args) throws Exception {
        <mark>throw new MyException();
        System.out.println("success");</mark>
    }
        
}
    
class MyException extends RuntimeException {
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect5/exam/Houdini.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect5\exam\Houdini.java:7: error: unreachable statement
        System.out.println("success");
        ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
import java.io.FileNotFoundException;
import java.io.IOException;
    
public class Changeup {
    
    public static void main(String[] args) throws IOException{
new Changeup().go();
 new Changeup().go2();
 new Changeup().go3();
    }
void go() { throw new IllegalArgumentException(); }
    
 void go2() throws FileNotFoundException { }
     
void go3() {
 try { throw new Exception(); }
 catch (Throwable th) { throw new NullPointerException(); }
 }
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.IllegalArgumentException
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Changeup.go(Changeup.java:13)
	at com.mvivekweb.ocpjp6.obj2.sect5.exam.Changeup.main(Changeup.java:9)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
public class Edgy {
    
    public static void main(String[] args) {
try {
wow();
 // throw new IOException();
 } finally {
  <mark>throw new Error();</mark>
 // throw new IOException();
 }
    }
static void wow() {
  <mark>throw new IllegalArgumentException();</mark>
 // throw new IOException();
 }
}
    
</pre>

<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect5/exam/Edgy.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect5/exam/Edgy
Exception in thread "main" java.lang.Error
        at com.mvivekweb.ocpjp6.obj2.sect5.exam.Edgy.main(Edgy.java:10)

        C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect5.exam;
    
public class Tolt {
public static void checkIt(int a) {
 if(a == 1) throw new IllegalArgumentException();
 }
    public static void main(String[] args) {
for(int x=0; x<2; x++)
 try {
 System.out.print("t ");
 checkIt(x);
 System.out.print("t2 ");
 }
 finally { System.out.print("f "); }
 }
     
     
}
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect5/exam/Tolt.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect5/exam/Tolt
t t2 f t f Exception in thread "main" java.lang.IllegalArgumentException
        at com.mvivekweb.ocpjp6.obj2.sect5.exam.Tolt.checkIt(Tolt.java:5)
        at com.mvivekweb.ocpjp6.obj2.sect5.exam.Tolt.main(Tolt.java:11)

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>
<p>
    As far as the exception goes, it's thrown during the second iteration of the
    for loop, and it's<mark> uncaught and undeclared</mark> (which is legal since it's a runtime exception).
    Remember, finally (almost) ALWAYS runs. In main(), the code is legal: the entire
    try-finally code is considered a single statement from the for loop's perspective, and
    of course the idea of a try-finally is legal; a catch statement is not required.</p>