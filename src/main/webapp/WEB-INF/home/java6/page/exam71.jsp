
<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import java.util.ArrayList;
import java.util.List;
    
    
public class MyPancake implements Pancake{
    
    
    public static void main(String[] args) {
        List&lt;String&gt; x = new ArrayList&lt;String&gt;();
 x.add("3"); x.add("7"); x.add("5");
List&lt;String&gt; y = new MyPancake().doStuff(x);
 y.add("1");
System.out.println(x);
    }
List&lt;String&gt; doStuff(List&lt;String&gt; z) {
z.add("9");
 return z;
 }
     
}
    
interface Pancake {
 List&lt;String&gt; doStuff(List&lt;String&gt; s);
 }
     
 </pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/exam/MyPancake.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\exam\MyPancake.java:19: error: doStuff(List&lt;String&gt;) in MyPancake cannot implement doStuff(List&lt;String&gt;) in Pancake
List&lt;String&gt; doStuff(List&lt;String&gt; z) {
             ^
  attempting to assign weaker access privileges; was public
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p><mark>MyPancake.doStuff() must be marked public</mark>. If it is, then C would be
    correct.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import java.util.ArrayList;
import java.util.List;
    
    
public class MyPancake implements Pancake{
    
    
    public static void main(String[] args) {
        List&lt;String&gt; x = new ArrayList&lt;String&gt;();
 x.add("3"); x.add("7"); x.add("5");
List&lt;String&gt; y = new MyPancake().doStuff(x);
 y.add("1");
System.out.println(x);
    }
<mark>public</mark> List&lt;String&gt; doStuff(List&lt;String&gt; z) {
z.add("9");
 return z;
 }
     
}
    
interface Pancake {
 List&lt;String&gt; doStuff(List&lt;String&gt; s);
 }
 </pre>
<pre class='out'>
run:
[3, 7, 5, 9, 1]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import java.util.Date;
<mark>import static java.lang.Short.*;
import static java.lang.Long.*;</mark>
    
public class MathBoy {
    
    
    public static void main(String[] args) {
        long x = 123456789;
short y = 22766; // maximum value of a short is 32767
 System.out.printf("%1$+10d %2$010d ", x, <mark>MAX_VALUE</mark> - y);
 System.out.println(new Date());
 }
     
     
     
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/exam/MathBoy.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\exam\MathBoy.java:15: error: reference to MAX_VALUE is ambiguous
 System.out.printf("%1$+10d %2$010d ", x, MAX_VALUE - y);
                                          ^
  <mark>both variable MAX_VALUE in Short and variable MAX_VALUE in Long match</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import java.util.Date;
//import static java.lang.Short.*;
<mark>import static java.lang.Long.*;</mark>
    
public class MathBoy {
    
    
    public static void main(String[] args) {
        long x = 123456789;
short y = 22766; // maximum value of a short is 32767
 System.out.printf("%1$+10d %2$010d ", x, MAX_VALUE - y);
 System.out.println(new Date());
 }
     
     
     
}
</pre>
<pre class='out'>
run:
+123456789 <mark>9223372036854753041</mark> Wed Aug 24 14:55:05 CDT 2016
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import java.util.Date;
<mark>import static java.lang.Short.*;</mark>
//import static java.lang.Long.*;
    
public class MathBoy {
    
    
    public static void main(String[] args) {
        long x = 123456789;
short y = 22766; // maximum value of a short is 32767
 System.out.printf("%1$+10d %2$010d ", x, MAX_VALUE - y);
 System.out.println(new Date());
 }
     
     
     
}
</pre>

<pre class='out'>run:
+123456789 <mark>0000010001</mark> Wed Aug 24 14:55:42 CDT 2016
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
<mark>import java.util.LinkedList;
import java.util.List;</mark>
    
    
public class Networking {
    
    
    public static void main(String[] args) {
        List&lt;Integer&gt; i = new LinkedList&lt;Integer&gt;();
 i.add(4); i.add(2); i.add(5);
int r = 1;
 doStuff(r);
System.out.println(i.get(r));
    }
static <mark>int</mark> doStuff(int x) {
 return ++x;
 }
}</pre>

<pre class='out'>run:
2
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>Of course, you need to import some stuff for this program to compile. This
    is an easy question if you spot the problem. If you don't, it'll seem tricky, so watch out for
    these on the real exam.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.*;
    
    
public class Launcher {
    
    
    public static void main(String[] args) {
        <mark>Utils u = new Utils();</mark>
 u.do1();
 u.do2();
 u.do3();
    }
        
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
    
<mark>class Utils {</mark>
    void do1() { System.out.print("do1 "); }
 protected void do2() { System.out.print("do2 "); }
 public void do3() { System.out.print("do3 "); }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/accesstomemebers/packageone/Launcher.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\accesstomemebers\packageone\Launcher.java:12: error: <mark>Utils is not public</mark> in com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo; cannot be accessed from outside package
        Utils u = new Utils();
        ^
com\mvivekweb\ocpjp6\obj7\sect1\accesstomemebers\packageone\Launcher.java:12: error: Utils is not public in com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo; cannot be accessed from outside package
        Utils u = new Utils();
                      ^
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.*;
    
    
public class Launcher {
    
    
    public static void main(String[] args) {
        Utils u = new Utils();
 u.do1();
 u.do2();
 u.do3();
    }
        
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
    
<mark>public</mark> class Utils {
    void do1() { System.out.print("do1 "); }
 protected void do2() { System.out.print("do2 "); }
 public void do3() { System.out.print("do3 "); }
}
    
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/accesstomemebers/packageone/Launcher.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\accesstomemebers\packageone\Launcher.java:13: error:<mark> do1() is not public in Utils; cannot be accessed from outside package</mark>
 u.do1();
  ^
com\mvivekweb\ocpjp6\obj7\sect1\accesstomemebers\packageone\Launcher.java:14: error: <mark>do2() has protected access in Utils</mark>
 u.do2();
  ^
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
class Robot { }
 interface Animal { }
 class Feline implements Animal { }
public class BarnCat extends Feline{
    
    
    public static void main(String[] args) {
        Animal af = new Feline();
 Feline ff = new Feline();
 BarnCat b = new BarnCat();
 Robot r = new Robot();
 if(af instanceof Animal) System.out.print("1 ");
 if(af instanceof BarnCat) System.out.print("2 ");
 if(b instanceof Animal) System.out.print("3 ");
 if(ff instanceof BarnCat) System.out.print("4 ");
 if(r instanceof Animal) System.out.print("5 ");
    }
        
}
</pre>
<pre class='out'>
run:
1 3 BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
class Robot { }
 interface Animal { }
 class Feline implements Animal { }
<mark>public class BarnCat {</mark>
    
    
    public static void main(String[] args) {
        Animal af = new Feline();
 Feline ff = new Feline();
 BarnCat b = new BarnCat();
 Robot r = new Robot();
 if(af instanceof Animal) System.out.print("1 ");
 if(af instanceof BarnCat) System.out.print("2 ");
 if(b instanceof Animal) System.out.print("3 ");
<mark> if(ff instanceof BarnCat) System.out.print("4 ");</mark>
 if(r instanceof Animal) System.out.print("5 ");
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/exam/BarnCat.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\exam\BarnCat.java:19: error: incompatible types: Feline cannot be converted to BarnCat
 if(ff instanceof BarnCat) System.out.print("4 ");
    ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
    
public class Utils_1 {
    void do1() { System.out.print("do1 "); }
 protected void do2() { System.out.print("do2 "); }
public void do3() { System.out.print("do3 "); }
}
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.Utils_1;
    
    
public class UPS extends Utils_1{
    
    
    public static void main(String[] args) {
        Utils_1 u = new Utils_1();
 u.do1();
u.do2();
 u.do3();
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/exam/UPS.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\exam\UPS.java:13: error: do1() is not public in Utils_1; cannot be accessed from outside package
 u.do1();
  ^
com\mvivekweb\ocpjp6\obj7\sect1\exam\UPS.java:14: error: do2() has protected access in Utils_1
u.do2();
 ^
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
    
public class Utils_1 {
    void do1() { System.out.print("do1 "); }
 protected void do2() { System.out.print("do2 "); }
public void do3() { System.out.print("do3 "); }
}
    
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.Utils_1;
    
    
public class UPS extends Utils_1{
    
    
    public static void main(String[] args) {
        Utils_1 u = new Utils_1();
 u.do1();
new UPS().do2();
 u.do3();
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect1/exam/UPS.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect1\exam\UPS.java:13: error: do1() is not public in Utils_1; cannot be accessed from outside package
 u.do1();
  ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
<mark>import static java.io.File.*;</mark>
    
    
public class Eieio {
    
    
    public static void main(String[] args) {
        try {
 String s = "subdir" + separator + "myFile.txt";
 java.io.File f = new java.io.File(s);
java.io.FileReader fr = new java.io.FileReader(f);
 java.io.File[] r = listRoots();
 fr.close();
 }
 catch(Exception e) { }
}
}
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
<mark>import static java.io.File.listRoots;
import static java.io.File.separator;</mark>
    
    
    
    
public class Eieio {
    
    
    public static void main(String[] args) {
        try {
 String s = "subdir" + separator + "myFile.txt";
 java.io.File f = new java.io.File(s);
java.io.FileReader fr = new java.io.FileReader(f);
 java.io.File[] r = listRoots();
 fr.close();
 }
 catch(Exception e) { }
}
}
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect1.exam;
    
<mark>import static java.io.File.separator;
import static java.io.File.*;</mark>
    
    
    
    
public class Eieio {
    
    
    public static void main(String[] args) {
        try {
 String s = "subdir" + separator + "myFile.txt";
 java.io.File f = new java.io.File(s);
java.io.FileReader fr = new java.io.FileReader(f);
 java.io.File[] r = listRoots();
 fr.close();
 }
 catch(Exception e) { }
}
}</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>