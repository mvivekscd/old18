<h3>Develop code that makes use of assertions, and distinguish appropriate from
            inappropriate uses of assertions.</h3>
        
        
        <p>assertions let you <mark>test your
            assumptions</mark> during development, <mark>without the expense</mark> (in both your time and
            program overhead) <mark>of writing exception handlers</mark> for exceptions that you assume
            will never happen once the program is out of development and fully deployed     </p>  
        
        <h3> Assertions Overview </h3>
        
        <p>you want to validate your
            assumption, but you d<mark>on't want</mark> to have to strip out print statements, runtime
            exception handlers, or if/else tests when you're done with development</p>
        
        <p>you <mark>don't want</mark> to take the time (or
            program performance hit) to <mark>write exception-handling code</mark></p>
        
        
        <p>the assertion
            code basically <mark>evaporates when the program is deployed</mark>, leaving behind no overhead
            or debugging code to track down and remove</p>
        
        <p>assertions let your code stay cleaner and tighter, but because assertions
            are inactive unless specifically "turned on" (enabled)</p>
        
        <p>if your assertion turns out to be wrong (false),
            then a stop-the-world <mark>AssertionError</mark> is thrown  </p>      
 
        <p>Assertions come in two flavors: <mark>really simple </mark>and <mark>simple</mark></p>        
        
        
        <p> Really simple:</p>
        <pre>
        private void doStuff() {
assert (y > x);
// more code assuming y is greater than x
}
        </pre>
        
        <p>Simple:</p>
        <pre> 
        private void doStuff() {
assert (y > x)<mark>:</mark> "y is " + y + " x is " + x;
// more code assuming y is greater than x
}
        </pre>
        
        <p>The difference between the two is that the simple version adds a second expression,
            separated from the first (boolean expression) <mark>by a colon</mark>, this expression's
            <mark>string value is added to the stack trace</mark>   </p> 
        
        <h3>Assertion Expression Rules    </h3>
        
        <p>Assertions can have either <mark>one or two expressions</mark> </p>       
        
        <p>The first expression must always result in
            a boolean value     </p>
        
        <p>If it's not true, however, then your assumption was wrong and
            you get an AssertionError.</p>
        
        
        <p> The second expression can be anything that <mark>results in a value</mark></p>
        
        <p>the second expression
            is used to <mark>generate a String message </mark>that displays in the stack trace to give you a
            little more debugging information. It works much like System.out.println() </p>
        
        <pre>
        package com.mvivekweb.ocpjp6.obj2.sect3.assertion;
            
public class asserttest {
    
    public static void main(String[] args) {
        int x = 1;
            
            
        assert (x == 1) : noReturn();
    }
        
    static void noReturn() {
    }
        
    static int aReturn() {
        return 1;
    }
}
        </pre>
        <pre class='out'>       
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - 'void' type not allowed here
	at com.mvivekweb.ocpjp6.obj2.sect3.assertion.asserttest.main(asserttest.java:9)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)        
        </pre>  
       
        <p>then always assume the word "expression" refers
            to expression1</p>
        
        <h3>Enabling Assertions</h3>
        
        <p>You can use assert as a keyword or as an identifier, but not both.</p>
        
        <p>Compiling Assertion-Aware Code</p>
        
        <p>the compiler will generate an error message if it finds the word assert
            used as an identifier   </p>
        
        <pre>-source 1.6 or -source 6.</pre>
        
        <p>beginning with version 1.4, assert is a keyword</p>
        
        <pre>javac -source 1.3 TestAsserts.java  </pre>
        
        <p>If assert Is an Identifier. <mark>Code compiles with warnings</mark>.</p>
        
        <p> If assert Is a Keyword.<mark>Compilation fails</mark>.</p>
        
        <h3>Running with Assertions </h3>
        
        <p>assertions are <mark>disabled by default</mark>.</p>
        
        <h3>Enabling Assertions at Runtime</h3>
        
        <pre>
package com.mvivekweb.ocpjp6.obj2.sect3.assertion;
    
public class assertTest1 {
    
    public static void main(String[] args) {
        int x = 1;
        System.out.println("testing...");
        assert (x==1): "Condition is true, so we won't see this";
        assert <mark>false</mark> : "If assertions are on, we'll see this";
    }
        
}
        </pre>
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect3/assertion/assertTest1.java

C:\vivek\docs\cert\ocpjp6\src>java <mark>-ea</mark> com/mvivekweb/ocpjp6/obj2/sect3/assertion/assertTest1
testing...
Exception in thread "main" java.lang.AssertionError: If assertions are on, we'll see this
        at com.mvivekweb.ocpjp6.obj2.sect3.assertion.assertTest1.main(assertTest1.java:12)

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect3/assertion/assertTest1
testing...

C:\vivek\docs\cert\ocpjp6\src>        
    </pre>
        <pre>       
 java -ea com.geeksanonymous.TestClass
or
java -enableassertions com.geeksanonymous.TestClass       
        </pre>   
        
        <h3>Disabling Assertions at Runtime   </h3>
        <pre>       
java -da com.geeksanonymous.TestClass
or
java -disableassertions com.geeksanonymous.TestClass        
        </pre> 
        
        <p>Because assertions are disabled by default, using the disable switches might seem
            unnecessary</p>    
        
        <h3>Selective Enabling and Disabling  </h3>
        
        <p><b>With no arguments</b> - Enables or disables
            assertions in <mark>all classes</mark>, except for the system classes   </p>
        
        <p><b>With a package name</b> - Enables or disables assertions in the <mark>package specified,
            and any packages below this package in the same directory hierarchy</mark></p>

        <p> <b>With a class name</b>- Enables or disables assertions <mark>in the class</mark> specified</p>
        
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>java -ea <mark>-da:com.mvivekweb...</mark> com/mvivekweb/ocpjp6/obj2/sect3/assertion/assertTest1
testing...

C:\vivek\docs\cert\ocpjp6\src>        
    </pre> 
        
        <p>The preceding command line tells the JVM to enable assertions in general, but
            <mark>disable them in the package , and all of its subpackages</mark> </p>       
        
        
        <pre>java -ea
java -enableassertions </pre>
        
        <pre>java -da
java -disableassertions</pre>
        
        <pre>java -ea:com.foo.Bar //Enable assertions in class com.foo.Bar.</pre>
        
        
        <pre>java <mark>-ea:com.foo...</mark> //Enable assertions in package com.foo and any of its subpackages.  </pre>      
        
        <pre>java -ea <mark>-dsa</mark> //Enable assertions in general, <mark>but disable assertions in system classes</mark>. </pre> 
        
        <pre> java -ea <mark>-da:com.foo...</mark> //Enable assertions in general, but disable assertions in package
com.foo and any of its subpackages.    </pre>   
        
        
        <h3>Using Assertions Appropriately </h3>

        <p>Not all legal uses of assertions are considered appropriate. </p>
        
        <p> Legally, however, AssertionError is a subclass
            of Throwable, <mark>so it can be caught. But just don't do it</mark> </p>      
        
        <h3> Don't Use Assertions to Validate Arguments to a Public Method    </h3>
        
        <pre><mark>public</mark> void doStuff(<mark>int x</mark>) {
<mark>assert (x > 0); </mark>// inappropriate !
// do things with x
}    </pre>   
        
        <h3>Do Use Assertions to Validate Arguments to a Private Method</h3>
        <pre>        
private void doMore(int x) {
assert (x > 0);
// do things with x
}        
        </pre>     
        
        <p>You're certainly free to compile
            assertion code with an inappropriate validation of public arguments, but for the
            exam (and real life) you need to know that you shouldn't do it.  </p>      
  
        
        <h3>  Don't Use Assertions to Validate Command-Line Arguments </h3>
        
        <p> Do Use Assertions, Even in Public Methods, to Check for Cases
            that You Know Are Never, Ever Supposed to Happen   </p>
        
        <pre>     switch(x) {
case 1: y = 3; break;
case 2: y = 9; break;
case 3: y = 27; break;
<mark>default: assert false;</mark> // we're never supposed to get here!
}</pre>
        
        <h3>        
            Don't Use Assert Expressions that Can Cause Side Effects! </h3>  
        
        <pre> assert (modifyThings());  </pre>
        
        <p> The rule is, an assert expression should leave the program in the same state it was
            in before the expression!</p>
        
        <p>If assertions are
            enabled, the only change to the way your program runs is that an AssertionError
            can be thrown if one of your assertions (think: assumptions) turns out to be false.</p>
        
        <p> Using assertions that cause side effects can cause some of the most maddening
            and <mark>hard-to-find bugs known</mark> to man    </p>   
        
