<pre>package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Excep1 {
    
    public static void main(String[] args) {
try {
 if(args.length == 0) throw new Exception();
 }
 catch (Exception e) {
 System.out.print("done ");
 doStuff(); // assume this method compiles
 }
 finally {
 System.out.println("finally ");
 }
    }
        
    private static void doStuff() {
        //System.exit(1);
    }
        
}
    
</pre>
<pre class='out'>
run:
<mark>done finally </mark>
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
public class Excep1 {
    
    public static void main(String[] args) {
try {
 if(args.length == 0) throw new Exception();
 }
 catch (Exception e) {
 System.out.print("done ");
 doStuff(); // assume this method compiles
 }
 finally {
 System.out.println("finally ");
 }
    }
        
    private static void doStuff() {
        <mark>System.exit(1);</mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
done C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/Excep1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect4/exam/Excep1 x</mark>
finally

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.io.IOException;
    
public class MacPro <mark>extends Laptop</mark>{
    
    public static void main(String[] args) {
new MacPro().crunch();
    }
<mark>void crunch(int x) throws Exception { }</mark>
}
class Laptop {
 void crunch() throws IOException { }
 }</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/MacPro.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect4\exam\MacPro.java:8: error: unreported exception IOException; must be caught or declared to be thrown
new MacPro().crunch();
                   ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.io.IOException;
    
public class MacPro extends Laptop{
    
    public static void main(String[] args) {
new MacPro().crunch();
    }
void crunch() { }
}
class Laptop {
 void crunch() throws IOException { }
 }
     
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>It's legal for an overriding method to <mark>throw fewer exceptions, and
        runtime exceptions</mark> are in a class hierarchy separate from checked exceptions.</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.io.IOException;
    
public class MacPro extends Laptop{
    
    public static void main(String[] args) {
new MacPro().crunch();
    }
<mark>void crunch() throws Exception { }</mark>
}
class Laptop {
 void crunch() throws IOException { }
 }
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/MacPro.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect4\exam\MacPro.java:10: error: crunch() in MacPro cannot override crunch() in Laptop
void crunch() throws Exception { }
     ^
  <mark>overridden method does not throw Exception</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.io.IOException;
    
public class MacPro extends Laptop{
    
    public static void main(String[] args) {
new MacPro().crunch();
    }
<mark>void crunch() throws RuntimeException { }</mark>
}
class Laptop {
 void crunch() throws IOException { }
 }
     
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.io.FileNotFoundException;
import java.io.IOException;
    
public class MacPro extends Laptop{
    
    public static void main(String[] args) {
new MacPro().crunch();
    }
void crunch()<mark> throws FileNotFoundException { }</mark>
}
class Laptop {
 void crunch() throws IOException { }
 }
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/MacPro.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect4\exam\MacPro.java:9: error: <mark>unreported exception FileNotFoundException; must be caught or declared to be thrown</mark>
new MacPro().crunch();
                   ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Excep2 {
    
    
static String s = "";
public static void main(String[] args) {
 <mark>try {</mark> doStuff(); }
 <mark>catch (Exception ex) {</mark> s += "c1 "; }
 System.out.println(s);
 }
 static void doStuff() throws RuntimeException {
 try {
 s += "t1 ";
 throw new IllegalArgumentException();
}
catch (IllegalArgumentException ie) { s += "c2 "; }
 throw new IllegalArgumentException();
 }
    }
        
</pre>
<pre class='out'>
run:
t1 c2 c1 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Errrrr {
static String a = null;
 static String s = "";
    public static void main(String[] args) {
try {
a = args[0];
System.out.print(a);
 s += "t1 ";
}
 catch (RuntimeException re) { s += "c1 "; }
finally { s += "f1 "; }
 System.out.println(" " + s);
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/Errrrr.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect4/exam/Errrrr</mark>
 c1 f1

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect4/exam/Errrrr x</mark>
x t1 f1

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Incomplete {
    
    public static void main(String[] args) {
        new Incomplete().doStuff();
    }
static void doStuff() throws Exception {
 throw new Exception();
}
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/Incomplete.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect4\exam\Incomplete.java:7: error: unreported exception Exception; <mark>must be caught or declared to be thrown</mark>
        new Incomplete().doStuff();
                                ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Incomplete {
    
    public static void main(String[] args) throws Exception {
        new Incomplete().doStuff();
    }
static void doStuff() throws Exception {
 throw new Exception();
}
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src><mark>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/Incomplete.java</mark>
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect4/exam/Incomplete</mark>
Exception in thread "main" java.lang.Exception
        at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.doStuff(Incomplete.java:10)
        at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.main(Incomplete.java:7)

        C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class Incomplete {
    
    public static void main(String[] args)  {
        <mark>try {</mark>
            new Incomplete().doStuff();
       <mark> } catch (Exception ex) {
            Logger.getLogger(Incomplete.class.getName()).log(Level.SEVERE, null, ex);
        }</mark>
    }
static void doStuff() throws Exception {
 throw new Exception();
}
}
</pre>
<pre class='out'>
run:
Aug 14, 2016 9:37:21 PM com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete main
SEVERE: null
java.lang.Exception
	at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.doStuff(Incomplete.java:17)
	at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.main(Incomplete.java:11)

BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class Incomplete {
    
    public static void main(String[] args)  {
        try {
            new Incomplete().doStuff();
        } 
        finally  {
            Logger.getLogger(Incomplete.class.getName()).log(Level.SEVERE, null, "finally");
        }
    }
static void doStuff() throws Exception {
 throw new Exception();
}
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect4/exam/Incomplete.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj2\sect4\exam\Incomplete.java:11: error: unreported exception Exception; must be caught or declared to be thrown
            new Incomplete().doStuff();
                                    ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class Incomplete {
    
    public static void main(String[] args)  {
        try {
            new Incomplete().doStuff();
        } 
        catch (Exception ex) {
            Logger.getLogger(Incomplete.class.getName()).log(Level.SEVERE, null, ex);
        }        finally  {
            Logger.getLogger(Incomplete.class.getName()).log(Level.SEVERE, null, "finally");
        }
    }
static void doStuff() throws Exception {
 throw new Exception();
}
}
    
</pre>
<pre class='out'>
run:
Aug 14, 2016 9:41:59 PM com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete main
SEVERE: null
java.lang.Exception
	at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.doStuff(Incomplete.java:20)
	at com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete.main(Incomplete.java:11)

Aug 14, 2016 9:42:00 PM com.mvivekweb.ocpjp6.obj2.sect4.exam.Incomplete main
SEVERE: null
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>an Exception that must either be
    handled (option C) or declared (option B)</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class GiveUp {
    
    public static void main(String[] args) throws Exception {
try {
 assert false;
System.out.print("t ");
 }
 catch (Error e) {
 System.out.print("c ");
 throw new Exception();
 }
 finally { System.out.print("f "); }
    }
        
}
    
</pre>
<pre class='out'>
run:
t f BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>package com.mvivekweb.ocpjp6.obj2.sect4.exam;
    
public class Excep3 {
    
    public static void main(String[] args) {
try {
 throw new Error();
 }
catch (Error e) {
try { throw new RuntimeException(); }
catch (Throwable t) { }
 }
 System.out.println("phew");
    }
        
}
</pre>

<pre class='out'>run:
phew
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>



