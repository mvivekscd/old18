 <h3>Develop code that implements all forms of loops and iterators, including the use of
            for, the enhanced for loop (for-each), do, while, labels, break, and continue; and explain
            the values taken by loop counter variables during and after loop execution.</h3>
        
        <p>the <mark>for</mark>
            loop has <mark>two variations</mark></p>
        
        <h3>Using while Loops</h3>
        
        <p>scenarios where you <mark>don't know how many times a
            block or statement should repeat</mark>, but you want to <mark>continue looping as long as some
            condition is true</mark>.</p>
            <pre>
            syntax
                
            while (expression) {
            // do stuff
            }
            </pre>
      
            <pre>        
   package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
       
public class while1 {
    
    public static void main(String[] args) {
        int x = 2;
        while (<mark>x == 2</mark>) {
            System.out.println(x);
            ++x;
        }
    }
}         
            </pre>    
<pre class='out'>            
run:
2
BUILD SUCCESSFUL (total time: 1 second)
    </pre>        
 
            <p>the expression (test) must evaluate to a <mark>boolean</mark>
                result</p>
            
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class while1 {
    
    public static void main(String[] args) {
        int x = 2;
<mark>        while (x == 2) 
            ++x;
        System.out.println(x);</mark>
    }
}
            </pre>
            <pre class='out'>           
run:
3
BUILD SUCCESSFUL (total time: 1 second)            
            </pre>
            <h3>Using do Loops   </h3>
            
            
            <p>that the <mark>expression</mark> is <mark>not evaluated
                until after the do loop's code is executed</mark>  </p> 
            
            <p>the code in a do loop is
                <mark>guaranteed</mark> to execute at least <mark>once</mark></p>  
            
            <pre>          
 package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
     
public class dowhile1 {
    
    public static void main(String[] args) {
        do {
            System.out.println("Inside loop");
        } while (false)<mark>;</mark>
    }
}
            </pre>    
 <pre class='out'>           
run:
Inside loop
BUILD SUCCESSFUL (total time: 1 second)            
    </pre> 
            
            
            <p> note the use of the <mark>semicolon</mark> at the
                end of the while expression.</p>           
            
            <h3>Using for Loops    </h3>
            
            <p>The basic for loop is <mark>more flexible</mark> than the enhanced for loop   </p>   
            
            
            <p>the
                <mark>enhanced for loop</mark> was designed to make <mark>iterating</mark> through <mark>arrays</mark> and <mark>collections</mark>
                easier to code  </p>  
            
            
            <h3>The Basic for Loop </h3>
            
            <p>when you already <mark>know how
                many times</mark> you need to execute   </p>    
            
            <p>three main parts</p>
            
a.Declaration and initialization of variables
<br>

b. The boolean expression (conditional test)
<br>

c. The iteration expression

<pre>
for (/*Initialization*/ <mark>;</mark> /*Condition*/<mark> ;</mark> /* Iteration */) {
/* loop body */
}
</pre>

<h3>The Basic for Loop: Declaration and Initialization</h3>
   
<p>lets you declare and initialize <mark>zero</mark>, <mark>one</mark>, or
    <mark>multiple variables</mark> of the <mark>same type</mark> inside the parentheses after the for keyword</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for1 {
    
    public static void main(String[] args) {
        for (int x = 10<mark>,</mark> y = 3; y > 3; y++) { }
            
    }
}
</pre>

<p>you declare more than one variable of the same type, then you'll need to separate
    them with<mark> commas</mark></p>

<p>the boolean test and the iteration expression‚??will
    run with each iteration of the loop</p>

<p>declaration and initialization happens just
    once, at the very beginning</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for2 {
    
    public static void main(String[] args) {
        for (int x = 1; x < 2; x++) {
            System.out.println(x); // Legal
        }
       <mark> System.out.println(x);</mark> // Not Legal! x is now out of scope
// and can't be accessed.
    }
}
    
</pre>
<pre class='out'>
run:
1
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - Erroneous tree type: <any>
	at com.mvivekweb.ocpjp6.obj2.sect2.whiledofor.for2.main(for2.java:20)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
</pre>

<h3>Basic for Loop: Conditional (boolean) Expression</h3>

<p><mark>must</mark> evaluate to a <mark>boolean</mark> value</p>

<p>only <mark>one</mark> logical expression, but it can be very <mark>complex</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for3 {
    
    public static void main(String[] args) {
        int y=0;
        for (int x = 0; ((((x < 10) && (y-- > 2)) | x == 3)); x++) { }
        for (int z = 0; <mark>(z > 5), (y < 2)</mark>; z++) { }
    }
}
</pre>
<pre class='out'>

C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\whiledofor>javac for3.java
for3.java:8: ';' expected
        for (int z = 0; (z > 5), (y < 2); z++) { }
                               ^
for3.java:8: ')' expected
        for (int z = 0; (z > 5), (y < 2); z++) { }
                                ^
for3.java:8: not a statement
        for (int z = 0; (z > 5), (y < 2); z++) { }
                                    ^
for3.java:8: ';' expected
        for (int z = 0; (z > 5), (y < 2); z++) { }
                                       ^
for3.java:8: ';' expected
        for (int z = 0; (z > 5), (y < 2); z++) { }
                                             ^
5 errors

C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\whiledofor>
    </pre>

<p>You can have <mark>only one</mark> test expression</p>

<p>you <mark>can't use multiple tests</mark> separated by commas, even though
    the <mark>other two parts of a for statement can have multiple parts</mark></p>

<h3>Basic for Loop: Iteration Expression</h3>

<p>Keep in mind that barring a <mark>forced exit</mark>, evaluating the <mark>iteration expression and
    then evaluating the conditional expression</mark> are always the <mark>last</mark> two things that
    happen in a for loop</p>

<p>Examples of forced exits include a <mark>break</mark>, a <mark>return</mark>, a <mark>System.exit()</mark>, or <mark>an
    exception</mark>, which will all cause a loop to terminate abruptly, <mark>without running the
    iteration expressio</mark>n</p>
    
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for6 {
    public static void main(String[] args) {
        for (<mark>int x = 0; x < 3</mark>; x++) {
            System.out.println("in for loop");
            break;
        }
    }
        
}
    </pre>
 <pre class='out'>   
run:
in for loop
BUILD SUCCESSFUL (total time: 1 second)
    </pre>

    <p>System.exit()- <mark>All program execution stops; the VM shuts down</mark>.</p>
    
    <h3>  Basic for Loop: for Loop Issues  </h3>
    
    <p>None of the three sections of the <mark>for</mark> declaration are required</p>
    <pre>  
for( ; ; ) {
System.out.println("Inside an endless loop");
}    
    </pre>

    <p> the loop will act like a <mark>while</mark> loop  </p> 
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for4 {
    public static void main(String[] args) {
        <mark>int i = 0;</mark>
        for (<mark>; i < 2;</mark>) {
            <mark>i++;</mark>
         //do some other work
            System.out.println("i="+i);
        }
    }
}    
    </pre>  
<pre class='out'>    
run:
i=1
i=2
BUILD SUCCESSFUL (total time: 0 seconds)    
    </pre> 
    
    <p>the variables declared in the for statement are all <mark>local to the for loop</mark>, and <mark>can't
        be used outside</mark> the scope of the loop </p>  

    <p><mark>three sections</mark> of the for loop <mark>are independent</mark> of
        each other   </p> 
    
    <p>you can <mark>put in virtually any arbitrary code</mark> statements that you want to
        happen with each iteration of the loop </p>  
    <pre>    
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class for5 {
    public static void main(String[] args) {
        int b = 3;
        for (int a = 1; b != 1; <mark>System.out.println("iterate")</mark>) {
            b = b - a;
        }
    }
}    
    </pre>   
 <pre class='out'>   
run:
iterate
iterate
BUILD SUCCESSFUL (total time: 1 second)
    </pre> 
    
    <h3>The Enhanced for Loop (for Arrays)  </h3>
    
    <p>the enhanced for has two <mark>components</mark>    </p>
    <pre>    
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class enfor1 {
    
    public static void main(String[] args) {
        <mark>int[]</mark> a = {1, 2, 3, 4};
        for (int x = 0; x < <mark>a.length</mark>; x++) // basic for loop
        {
            System.out.print(a[x]);
        }
        System.out.print("\n");
        for (<mark>int</mark> n : a) // enhanced for loop
        {
            System.out.print(n);
        }
        System.out.print("\n");
    }
}
    </pre>
 <pre class='out'>   
run:
1234
1234
BUILD SUCCESSFUL (total time: 1 second)    
    </pre> 
    <pre>   
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class enfor1 {
    
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4};
        for (int x = 0; x < a.length; x++) // basic for loop
        {
            System.out.print(a[x]);
        }
        System.out.print("\n");
        <mark>for (int n : a) // enhanced for loop
        System.out.print(n);
        System.out.print("\n");</mark>
    }
}
    </pre>
 <pre class='out'>   
run:
1234
1234
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    
    
    <pre> for(declaration : expression)  </pre> 
    
    <p>declaration- The newly declared block variable, of a <mark>type compatible with
        the elements of the array</mark> you are accessing </p>
    
    
    <p>expression- This must <mark>evaluate to the array</mark> you want to loop through</p>
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class enfor2 {
    public static void main(String[] args) {
        int [][] twoDee = {{1,2,3}, {4,5,6}, {7,8,9}};
        for(<mark>int[] n</mark> : twoDee){
            for(<mark>int v</mark>:n){
                System.out.println("v="+v);
            }
        }
    }
}    
    </pre>
 <pre class='out'>   
run:
v=1
v=2
v=3
v=4
v=5
v=6
v=7
v=8
v=9
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    

    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class enforlegal {
    public static void main(String[] args) {
        Animal [] animals = {new Dog(), new Cat()};
        for(<mark>Object</mark> s : animals) ;
        for(<mark>Dog</mark> d : animals) ;
    }
}
class Animal {
}
class Dog extends Animal {
    public Dog() {
    }
}
    
class Cat extends Animal {
    public Cat() {
    }
}    
    </pre> 
    
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\whiledofor>javac enforlegal.java
enforlegal.java:8: <mark>incompatible types</mark>
found   : com.mvivekweb.ocpjp6.obj2.sect2.whiledofor.<mark>Animal</mark>
required: com.mvivekweb.ocpjp6.obj2.sect2.whiledofor.<mark>Dog</mark>
        <mark>for(Dog d : animals) ;</mark>
                    ^
1 error

C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\whiledofor>    
    </pre>
    
    
    <h3>Using break and continue   </h3>
    
    <p>The <mark>break and continue</mark> keywords are used to <mark>stop</mark> either the <mark>entire loop
        (break)</mark> or just the <mark>current iteration (continue)</mark>  </p>  
    <p>continue statements must be <mark>inside a loop</mark>.<mark>for,while or do while</mark>. otherwise,
you'll get a <mark>compiler error</mark>.</p>
    
    <p>break statements must be used inside either a <mark>loop or
        switch</mark> statement.</p>
    <pre>   
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class continue1 {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            if (i == 1) {
               <mark> continue;</mark>
            }
             System.out.println("i="+i);
        }
    }
}
    </pre>
 <pre class='out'>   
run:
i=0
i=2
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class continue2 {
    public static void main(String[] args) {
        for (int i = 0; i < 3; <mark>i++</mark>) {
            System.out.println("Inside loop");
           <mark> continue;</mark>
        }
    }
}    
    </pre> 
    
<pre class='out'>    
run:
Inside loop
Inside loop
Inside loop
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    
    <p>When the continue
        statement is hit, the <mark>iteration expression</mark> still <mark>runs</mark> </p>   
    
    <h3>Unlabeled Statements</h3>
    
    <pre>    
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class continuewhile {
    public static void main(String[] args) {
        int x = 2;
        <mark>while (x == 2) {</mark>
            if(x==2){
                <mark>continue;</mark>
            }
            ++x;
        }
        System.out.println(x);
    }
}
    </pre>
 <pre class='out'>   
run:
2
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class continuewhile {
    public static void main(String[] args) {
        int x = 2;
        while (x < 5) {
            ++x;
            if(x==3){
               <mark> continue;</mark>
            }
            System.out.println("x="+x);
        }
    }
}    
    </pre>
<pre class='out'>    
run:
x=4
x=5
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>
    
    
    <p>Both the <mark>break</mark> statement and the <mark>continue</mark> statement can be<mark> unlabeled or
        labeled</mark>    </p>
    
    
    <p>far more common to use <mark>break and continue</mark> <mark>unlabeled</mark>, the
        exam expects you to know how <mark>labeled break and continue</mark> statements work</p>    
    
    
    <h3>Labeled Statements  </h3>
    
    
    <p>many statements in a Java program can be labeled</p>
    
    <p>it's most common to
        use labels with loop statements like <mark>for or while</mark>, in conjunction with <mark>break and
        continue</mark> statements.</p>
    
    
    <p>A label statement <mark>must be placed just before the statement
        being labeled</mark>, and it consists of a valid identifier that <mark>ends with a colon (:)</mark></p>
    
    <p>labeled varieties <mark>are needed only</mark> in situations where you have a
        <mark>nested loop</mark></p>
    
    <p>need to indicate <mark>which of the nested loops you want to break</mark> from,
        or from which of the <mark>nested loops you want to continue</mark> with the next iteration</p>
    
    <p>A
        <mark>break statement will exit out of the labeled loop</mark>, <mark>as opposed to the innermost loop</mark>,
        if the break keyword is combined with a label</p>
    
    <p> The label must adhere to the rules for a valid variable name and should adhere to the
        Java naming convention</p>
    
    
    <p>The syntax for the use of a label name in conjunction with a
        break statement is the <mark>break keyword</mark>, <mark>then the label name</mark>, <mark>followed by a semicolon</mark>.</p>
    
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class labeledbreak {
    
    public static void main(String[] args) {
        boolean isTrue = true;
        <mark>outer:</mark>
        for (int i = 0; i < 5; i++) {
            while (isTrue) {
                System.out.println("Hello");
                <mark>break outer;</mark>
            } // end of inner while loop
            System.out.println("Outer loop."); // Won't print
        } // end of outer for loop
        System.out.println("Good-Bye");
    }
}    
    </pre>
<pre class='out'>    
run:
Hello
Good-Bye
BUILD SUCCESSFUL (total time: 1 second)    
    </pre>  
    <pre>   
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class labelledContinue {
    
    public static void main(String[] args) {
       <mark> outer:</mark>
        <mark>for (int i = 0; i < 5; i++) {</mark>
            System.out.println("i="+i);
            for (int j = 0; j < 5; j++) {
                System.out.println("Hello");
               <mark> continue outer;</mark>
            } // end of inner loop
            System.out.println("outer"); // Never prints
        <mark>}</mark>
        System.out.println("Good-Bye");
    }
}    
    </pre>
    
<pre class='out'>    
run:
i=0
Hello
i=1
Hello
i=2
Hello
i=3
Hello
i=4
Hello
Good-Bye
BUILD SUCCESSFUL (total time: 0 seconds)    
    </pre>
    
    
    <p>After the continue statement is
        executed, the flow <mark>continues with the next iteration of the loop identified</mark> with the
        label.  </p>  
    
    
    <pre>
package com.mvivekweb.ocpjp6.obj2.sect2.whiledofor;
    
public class labelledcontinue2 {
    
    public static void main(String[] args) {
         outer:
        <mark>for (int i = 0; i < 5; i++) {</mark>
            System.out.println("i="+i);
            for (int j = 0; j < 5; j++) {
                 System.out.println("j="+j);
                for (int k = 0; k < 5; k++) {
                System.out.println("Hello");
                <mark>continue outer;</mark>
                }
            } // end of inner loop
            System.out.println("outer"); // Never prints
        <mark>}</mark>
        System.out.println("Good-Bye");
    }
        
}
    
    </pre>
  <pre class='out'>  
 run:
i=0
j=0
Hello
i=1
j=0
Hello
i=2
j=0
Hello
i=3
j=0
Hello
i=4
j=0
Hello
Good-Bye
BUILD SUCCESSFUL (total time: 1 second)   

    </pre>
