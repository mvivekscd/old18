<h3>Recognize situations that will result in any of the following being thrown:
    ArrayIndexOutOfBoundsException, ClassCastException, IllegalArgumentException,
    IllegalStateException, NullPointerException, NumberFormatException, AssertionError,
    ExceptionInInitializerError, StackOverflowError, or NoClassDefFoundError. Understand
    which of these are thrown by the virtual machine and recognize situations in which others
    should be thrown programmatically.</h3>

<p>Here's
    some code that just did something bad, which exception will be thrown</p>

<p>two broad categories of
    exceptions and errors:</p>

<p><b>JVM exceptions</b> Those exceptions or errors that are either exclusively
    or most logically <mark>thrown by the JVM</mark>.</p>

<p><b>Programmatic exceptions</b> Those exceptions that are thrown explicitly
    by <mark>application and/or API programmers</mark>.</p>

<h3>JVM Thrown Exceptions</h3>

<p><mark>NullPointerException</mark>

</p>
<p>this exception occurs when you attempt to <mark>access an object</mark>
    using a reference variable with a <mark>current value of null</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.Exceptions;
public class NPE {
    
<mark>static String s;</mark>
    public static void main(String[] args) {
        System.out.println(<mark>s.length()</mark>);
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" <mark>java.lang.NullPointerException</mark>
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.NPE.main(NPE.java:10)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<p>the stack resides in memory</p>

<p>It's
    possible to <mark>grow the stack so large that the OS runs out of space</mark> to store the call
    stack. When this happens you get (wait for it...), a <mark>StackOverflowError</mark>.</p>


<p>The
    most common way for this to occur is to create a <mark>recursive method</mark>.</p>

<p>it's a very common and useful technique for such things as <mark>searching and
    sorting algorithms</mark>.</p>
    
    <pre>    
package com.mvivekweb.ocpjp6.obj2.sect6.Exceptions;
    
public class stackoverflow {
    
    public static void main(String[] args) {
        go();
    }
        
    static void go() { // recursion gone bad
        go();
    }
}    
    </pre>
   <pre class='out'> 
 un:
Exception in thread "main" <mark>java.lang.StackOverflowError</mark>
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.Exceptions.stackoverflow.go(stackoverflow.java:10)
 
</pre>
    <h3> Programmatically Thrown Exceptions   </h3>
    
    <p><mark>NumberFormatException</mark> </p>   
    
    <p> String that could not be <mark>converted into a number</mark></p>
    
    
    <p>jvm exceptions</p>

ArrayIndexOutOfBoundsException- Thrown when attempting to access an array
with an invalid index value (either negative
or beyond the length of the array).
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
    
public class InitError {
	static int [] x = new int[4];
	static { x[<mark>4</mark>] = 5; }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
                    
	}
            
}
</pre>
<pre class='out'>
run:
java.lang.ExceptionInInitializerError
Caused by: java.lang.ArrayIndexOutOfBoundsException: 4
	at com.mvivekweb.ocpjp6.obj1.sect3.initializationblock.InitError.<clinit>(InitError.java:6)
Exception in thread "main" C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<br>ClassCastException- Thrown when attempting to cast a reference
variable to a type that fails the IS-A test.
<br>NullPointerException- Thrown when attempting to access an object
with a reference variable whose current value
is null.
<br>ExceptionInInitializerError- Thrown when attempting to initialize a static
variable or an initialization block.
<br>StackOverflowError- Typically thrown when a method recurses
too deeply. (Each invocation is added to the
stack.)
<br>NoClassDefFoundError- Thrown when the JVM can't find a class it
needs, because of a command-line error, a
classpath issue, or a missing .class file.

<p>Programmatically Thrown Exceptions   </p>

IllegalArgumentException- Thrown when a method receives an argument
formatted differently than the method
expects.
<br>IllegalStateException- Thrown when the state of the environment
doesn't match the operation being attempted,
e.g., using a Scanner that's been closed.
<br>NumberFormatException- Thrown when a method that converts a
String to a number receives a String that it
cannot convert.
<br>AssertionError- Thrown when a statement's boolean test
returns false.     





