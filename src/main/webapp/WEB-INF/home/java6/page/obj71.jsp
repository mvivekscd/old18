<h3>Given a code example and a scenario, write code that uses the appropriate access
            modifiers, package declarations, and import statements to interact with (through access or
            inheritance) the code in the example.</h3>
        
        <h3>Controlling Access to Members of a Class</h3>
        <p>two levels of access control:</p>
        
        a) At the top level-<mark>public</mark>, or <b>package-private</b> (no explicit modifier). <br>
        b)At the member level-<mark>public, private, protected</mark>, or <b>package-private</b> (no explicit modifier). 
                <br>   
                
                <p>        If a class has no modifier (the default, also known as package-private), it is visible only within its own package        
                </p>    
    
                <p>The <mark>private</mark> modifier specifies that the member can only be accessed in its own class</p>
                <p>The <mark>protected</mark> modifier specifies that the member can only be accessed within its own package (as with package-private) and, in addition, by a <b>subclass of its class</b> in <mark><b>another package</b></mark>. 
                </p>
    
    
      <table border="1" summary="This table defines levels of access conferred by a modifier">
<caption id="accesscontrol-levels"><b>Access Levels</b></caption>
<tbody><tr>
<th id="modifier">Modifier</th>
<th id="class">Class</th>
<th id="package">Package</th>
<th id="subclass">Subclass</th>
<th id="world">World</th>
</tr>
<tr>
<td headers="modifier"><code>public</code></td>
<td headers="class">Y</td>
<td headers="package">Y</td>
<td headers="subclass">Y</td>
<td headers="world">Y</td>
</tr>
<tr>
<td headers="modifier"><code>protected</code></td>
<td headers="class">Y</td>
<td headers="package">Y</td>
<td headers="subclass">Y</td>
<td headers="world">N</td>
</tr>
<tr>
<td headers="modifier"><i>no modifier</i></td>
<td headers="class">Y</td>
<td headers="package">Y</td>
<td headers="subclass">N</td>
<td headers="world">N</td>
</tr>
<tr>
<td headers="modifier"><code>private</code></td>
<td headers="class">Y</td>
<td headers="package">N</td>
<td headers="subclass">N</td>
<td headers="world">N</td>
</tr>
</tbody></table>          
                
        
                <pre>                
  package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone;
      
public class alpha {
    
    public void alphapub() {
        System.err.println("public method1");
        alphapriv();
        alphadef();
        alphapro();
    }
        
    private void alphapriv() {
        System.err.println("private method1");
    }
        
    void alphadef() {
        System.err.println("default method1");
    }
        
    protected void alphapro() {
        System.err.println("protected method1");
    }
        
    public static void main(String args[]) {
        alpha alinstance = new alpha();
        alinstance.alphapub();
        alinstance.alphapriv();
        alinstance.alphadef();
        alinstance.alphapro();
            
            
    }
}
    
                </pre>
                
                


<pre class='out'>
run:
public method1
private method1
default method1
protected method1
private method1
default method1
protected method1
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

                <pre>
            package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone;
                
public class alpha {
    
    public void alphapub() {
        System.err.println("public method1");
       <mark> //alphapriv();
        //alphadef();
        //alphapro();</mark>
    }
        
    private void alphapriv() {
        System.err.println("private method1");
    }
        
    void alphadef() {
        System.err.println("default method1");
    }
        
    protected void alphapro() {
        System.err.println("protected method1");
    }
        
    public static void main(String args[]) {
        alpha alinstance = new alpha();
        alinstance.alphapub();
        alinstance.alphapriv();
        alinstance.alphadef();
        alinstance.alphapro();
            
            
    }
}
    
                </pre>    
                
                
                
                <pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone;
    
    
public class beta {
    public void method1(){
        System.err.println("public method1");
                alpha alinstance = new alpha();
        alinstance.alphapub();
        <mark>alinstance.alphapriv();</mark>
        alinstance.alphadef();
        alinstance.alphapro();
    }
        
    public static void main(String args[]) {
        alpha alinstance = new alpha();
        alinstance.alphapub();
        <mark>alinstance.alphapriv();</mark>
        alinstance.alphadef();
        alinstance.alphapro();
            
            
    }
}
                </pre>       
  <pre class='out'>              
  run:
public method1
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - alphapriv() has private access in com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone.alpha
	at com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone.beta.main(beta.java:18)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 1 second)
              
    </pre>        
                
  
                <pre>
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone.alpha;
    
    
public class alphasub extends alpha{
        public void method1(){
       // System.err.println("public method1");
        //new alpha().alphapub();
        alphapub();
        alphapro();
        <mark>alphadef();
        alphapriv();</mark>
    }
        
        public static void main(String args[]) {
        alpha alinstance = new alpha();
        alinstance.alphapub();
        alphasub alphasub= new alphasub();
        alphasub.method1();
        <mark>alinstance.alphapriv();
        alinstance.alphadef();</mark>
            
            
    }
}
                </pre>         
                
  <pre class='out'> 
  run:
public method1
public method1
protected method1
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - Erroneous sym type: com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.alphasub.alphadef
	at com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.alphasub.method1(alphasub.java:13)
	at com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.alphasub.main(alphasub.java:21)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 1 second)
    </pre>        
                
      
<pre>                
    
package com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo;
    
import com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone.alpha;
    
    
public class gamma {
    
    public void method1() {
        System.err.println("public method1");
        new alpha().alphapub();
        <mark>alphapro();</mark>
    }
        
     public static void main(String args[]) {
        alpha alinstance = new alpha();
        alinstance.alphapub();
        <mark>alinstance.alphapriv();
        alinstance.alphadef();
        alinstance.alphapro();</mark>
        alphasub alphasub= new alphasub();
        alphasub.method1();
    }
}
</pre>
                
 <pre class='out'>               
 run:
public method1
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - alphapriv() has private access in com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packageone.alpha
	at com.mvivekweb.ocpjp6.obj7.sect1.accesstomemebers.packagetwo.gamma.main(gamma.java:25)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 1 second)
    </pre>        

  <h3> Static Imports   </h3>           
                
                <p> Static imports can
                    be used when you want to <mark>use a class's</mark> <mark><b>static</b></mark> members.  </p>      
                
                
                <pre>
package com.mvivekweb.ocpjp6.obj7.sect1.staticimports;
    
<mark>import static</mark> <mark>java.lang.System.out</mark>; // 1
<mark>import static</mark> <mark>java.lang.Integer.*</mark>;
    
public class TestStaticImport {
    
    
    public static void main(String[] args) {
        out.println(MAX_VALUE); // 3
        out.println(toHexString(42));
    }
}
                </pre>         
<pre class='out'> 
 run:
2147483647
2a
BUILD SUCCESSFUL (total time: 1 second)               
    </pre>   
                <p>                
                    <mark>import static</mark> followed by the <mark>fully qualified name</mark> of the static member                
                </p>    
                <p> <mark>shortcut</mark> for a <mark>static method</mark> AND a <mark>constant</mark>.</p>
                
                <p>  a little
                    harder to read</p>
                
                <p>You must say import static; you <mark>can't</mark> say <mark>static import</mark>.</p>
                
                
                <p>if you do
                    a static import for both the <mark>Integer</mark> class and the <mark>Long</mark> class, referring to
                    <mark>MAX_VALUE will cause a compiler error</mark>  </p>   
                
                <pre>               
 package com.mvivekweb.ocpjp6.obj7.sect1.staticimports;
     
import static java.lang.System.out; // 1
<mark>import static java.lang.Integer.*;
import static java.lang.Long.*;</mark>
    
public class TestStaticImport {
    
    
    public static void main(String[] args) {
        out.println(<mark>MAX_VALUE</mark>); // 3
        out.println(toHexString(42));
    }
}   </pre>
<pre class='out'>                
 run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - reference to MAX_VALUE is ambiguous, both variable MAX_VALUE in java.lang.Integer and variable MAX_VALUE in java.lang.Long match
	at com.mvivekweb.ocpjp6.obj7.sect1.staticimports.TestStaticImport.main(TestStaticImport.java:12)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)               
    </pre>    
                
                <p>You can do a static import on <mark>static object references</mark>, <mark>constants</mark> (remember
                    they're static and final), and <mark>static methods</mark>.  </p>                  
