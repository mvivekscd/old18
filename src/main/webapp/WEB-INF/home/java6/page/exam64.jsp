<pre>package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class A { }
 class B extends A { }
 class C extends B { }
public class Carpet<mark>&lt;V extends B&gt;</mark> {
    public <mark>&lt;X extends V&gt;</mark> Carpet<mark>&lt;? extends V&gt;</mark> method(Carpet<mark>&lt;? super X&gt;</mark> e) {
        return new Carpet&lt;A&gt;();
    }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Carpet.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Carpet.java:9: error: type argument A is not within bounds of type-variable V
        return new Carpet&lt;A&gt;();
                          ^
  where V is a type-variable:
    V extends B declared in class Carpet
com\mvivekweb\ocpjp6\obj6\sect4\exam\Carpet.java:9: error: incompatible types: Carpet&lt;A&gt; cannot be converted to Carpet&lt;? extends V&gt;
        return new Carpet&lt;A&gt;();
               ^
  where V is a type-variable:
    V extends B declared in class Carpet
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>The generic declaration at the class level says that Carpet can
    accept any type which is either B or a subtype of B. The generic declaration at the method
    level is "<mark>&lt;X extends V&gt;</mark>", which means that X is a type of V or a subtype of V, where
    the class type of V can vary at runtime-hence, the exact scopes of X and V are unknown
    at compile time. A and B are correct because X and V bind to the scope of &lt;? extends V&gt;, where X is known as a subtype of V as it's declared at the method level.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class A { }
 class B extends A { }
 class C extends B { }
public class Carpet<mark>&lt;V extends B&gt;</mark> {
    public <mark>&lt;X extends V&gt;</mark> Carpet<mark>&lt;? extends V&gt;</mark> method(Carpet<mark>&lt;? super X&gt;</mark> e) {
        return new Carpet&lt;C&gt;();
    }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Carpet.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Carpet.java:9: error: incompatible types: Carpet&lt;C&gt; cannot be converted to Carpet&lt;? extends V&gt;
        return new Carpet&lt;C&gt;();
               ^
  where V is a type-variable:
    V extends B declared in class Carpet
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;

class A { }
 class B extends A { }
 class C extends B { }
public class Carpet&lt;V extends B&gt; {
    public &lt;X extends V&gt; Carpet&lt;? extends V&gt; method(Carpet&lt;? super X&gt; e) {
        return new Carpet&lt;V&gt;();
    }
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;

class A { }
 class B extends A { }
 class C extends B { }
public class Carpet&lt;V extends B&gt; {
    public &lt;X extends V&gt; Carpet&lt;? extends V&gt; method(Carpet&lt;? super X&gt; e) {
       return new Carpet&lt;X&gt;();
    }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Carpet.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Hose &lt;E extends Hose&gt;{
    E innerE;
 public <mark>static</mark> E doStuff(E e, Hose&lt;E&gt; e2) {
 return e2.getE();
 }
 public E getE() {
 return innerE;
 }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Hose.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Hose.java:7: error: non-static type variable E cannot be referenced from a static context
 public static E doStuff(E e, Hose&lt;E&gt; e2) {
                         ^
com\mvivekweb\ocpjp6\obj6\sect4\exam\Hose.java:7: error: non-static type variable E cannot be referenced from a static context
 public static E doStuff(E e, Hose&lt;E&gt; e2) {
                                   ^
com\mvivekweb\ocpjp6\obj6\sect4\exam\Hose.java:7: error: non-static type variable E cannot be referenced from a static context
 public static E doStuff(E e, Hose&lt;E&gt; e2) {
               ^
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Hose &lt;E extends Hose&gt;{
    E innerE;
 public E doStuff(E e, Hose&lt;E&gt; e2) {
 <mark>return e2.getE();</mark>
 }
 public E getE() {
 return innerE;
 }
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Hose &lt;E extends Hose&gt;{
    E innerE;
 public E doStuff(E e, Hose&lt;E&gt; e2) {
<mark> return e;</mark>
 }
 public E getE() {
 return innerE;
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Hose.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

package com.mvivekweb.ocpjp6.obj6.sect4.exam;

<pre>
public class Hose &lt;E extends Hose&gt;{
    E innerE;
 public E doStuff(E e, Hose&lt;E&gt; e2) {
 return e.getE();
 }
 public E getE() {
 return innerE;
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Hose.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Hose.java:8: error: incompatible types: <mark>Hose cannot be converted to E</mark>
 return e.getE();
              ^
  where E is a type-variable:
    E extends Hose declared in class Hose
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Hose &lt;E extends Hose&gt;{
    E innerE;
 public E doStuff(E e, Hose&lt;E&gt; e2) {
return new Hose().getE();
 }
 public E getE() {
 return innerE;
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Hose.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Hose.java:8: error: incompatible types: <mark>Hose cannot be converted to E</mark>
return new Hose().getE();
                      ^
  where E is a type-variable:
    E extends Hose declared in class Hose
1 error
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return null;</mark>
 } 
}
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return new Mixer&lt;Dog&gt;();</mark>
 } 
}
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return new Mixer&lt;Animal&gt;();</mark>
 } 
}
    
</pre>

<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Mixer.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return new Mixer&lt;A&gt;();</mark>
 } 
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Mixer.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Mixer.java:10: error: incompatible types: <mark>Mixer&lt;A&gt; cannot be converted to Mixer&lt;? super Dog&gt;</mark>
return new Mixer&lt;A&gt;();
       ^
  <mark>where A is a type-variable:
    A extends Animal declared in class Mixer</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return new Mixer&lt;a&gt;();</mark>
 } 
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Mixer.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Mixer.java:10: error: cannot find symbol
return new Mixer&lt;a&gt;();
                 ^
  <mark>symbol:   class a</mark>
 <mark> location: class Mixer&lt;A&gt;</mark>
  where A is a type-variable:
    A extends Animal declared in class Mixer
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
class Animal { }
class Dog extends Animal { }
 class Cat extends Animal { }
public class Mixer&lt;A extends Animal&gt; {
   public &lt;C extends Cat&gt; Mixer&lt;? super Dog&gt; useMe(A a, C c) {
 //Insert Code Here
<mark> return new Mixer&lt;c&gt;();</mark>
 } 
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Mixer.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Mixer.java:10: error: cannot find symbol
return new Mixer&lt;c&gt;();
                 ^
  <mark>symbol:   class c</mark>
  location: class Mixer<A>
  where A is a type-variable:
    A extends Animal declared in class Mixer
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
import java.util.ArrayList;
import java.util.List;
    
    
public class gen1 {
    
    
    public static void main(String[] args) {
        List&lt;Integer&gt; x = new ArrayList&lt;Integer&gt;();
 x.add(new Integer(3));
 doStuff(x);
 for(Integer i: x)
 System.out.print(i + " ");
    }
static void doStuff(List y) {
 y.add(new Integer(4));
 y.add(new Float(3.14f));
 }
}</pre>
<pre class='out'>
run:
Exception in thread "main" <mark>java.lang.ClassCastException: </mark>java.lang.Float cannot be cast to java.lang.Integer
	at com.mvivekweb.ocpjp6.obj6.sect4.exam.gen1.main(gen1.java:16)
<mark>3 4</mark> C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>

<p>The doStuff() method doesn't know that "y" is a generic collection, <mark>so it
    allows the Float to be added at compile time</mark>. Once doStuff() completes, the for loop
    will succeed until it encounters the Float, at which point a ClassCastException will
    be thrown.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
import java.util.ArrayList;
import java.util.List;
    
interface Canine { }
 class Dog1 implements Canine { }
public class Collie extends Dog1{
    
    
    public static void main(String[] args) {
        List&lt;Dog1&gt; d = new ArrayList&lt;Dog1&gt;();
List&lt;Collie&gt; c = new ArrayList&lt;Collie&gt;();
 d.add(new Collie());
 c.add(new Collie());
 do1(d); do1(c);
 do2(d); do2(c);
    }
static void do1(<mark>List&lt;? extends Dog1&gt;</mark> d2) {
<mark>d2.add(new Collie());</mark>
 System.out.print(d2.size());
 }
 static void do2(List&lt;? extends Canine&gt; c2) { }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Collie.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Collie.java:22: error: no suitable method found for add(Collie)
d2.add(new Collie());
  ^
    method Collection.add(CAP#1) is not applicable
      (argument mismatch; Collie cannot be converted to CAP#1)
    method List.add(CAP#1) is not applicable
      (argument mismatch; Collie cannot be converted to CAP#1)
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Dog1 from capture of ? extends Dog1
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<p>When a method takes a wildcard generic type, the collection can be accessed
    or modified, but not both. The rest of the code is legal. It's legal to add a subclass element
    to a typed collection. It's legal for a method's argument to be a typed wildcarded interface.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Organic&lt;E&gt; {
void react(E e) { }
    
    public static void main(String[] args) {
        <mark>Organic&lt;? extends Organic&gt; compound = new Aliphatic&lt;Organic&gt;();</mark>
        compound.react(new Organic());
 compound.react(new Aliphatic());
 compound.react(new Hexane());
    }
        
}
class Aliphatic&lt;F&gt; extends Organic&lt;F&gt; { }
 class Hexane&lt;G&gt; extends Aliphatic&lt;G&gt; { }
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Organic.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Organic.java:11: error: incompatible types: Organic cannot be converted to CAP#1
        compound.react(new Organic());
                       ^
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Organic from capture of ? extends Organic
com\mvivekweb\ocpjp6\obj6\sect4\exam\Organic.java:12: error: incompatible types: Aliphatic cannot be converted to CAP#1
 compound.react(new Aliphatic());
                ^
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Organic from capture of ? extends Organic
com\mvivekweb\ocpjp6\obj6\sect4\exam\Organic.java:13: error: incompatible types: Hexane cannot be converted to CAP#1
 compound.react(new Hexane());
                ^
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Organic from capture of ? extends Organic
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Organic&lt;E&gt; {
void react(E e) { }
    
    public static void main(String[] args) {
       <mark> Organic&lt;? super Aliphatic&gt; compound = new Aliphatic&lt;Organic&gt;();</mark>
        compound.react(new Organic());
 compound.react(new Aliphatic());
 compound.react(new Hexane());
    }
        
}
class Aliphatic&lt;F&gt; extends Organic&lt;F&gt; { }
 class Hexane&lt;G&gt; extends Aliphatic&lt;G&gt; { }
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Organic.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Organic.java:11: error: incompatible types: Organic cannot be converted to CAP#1
        compound.react(new Organic());
                       ^
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Object super: Aliphatic from capture of ? super Aliphatic
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>The generic type of the reference &lt;? extends Organic&gt;
    says that the generic type of the instantiation can be either Organic, or a subtype of
    Organic. Since the compiler doesn't know this instantiation generic type (runtime type),
    it does NOT bind any value to its generic criteria, so A, B, and C are correct. On the other
    hand, the generic type of the reference &lt;? super Aliphatic&gt; says that the generic type
    of the instantiation can be either Aliphatic, or a supertype of Aliphatic. Although
    the compiler doesn't know the instantiation generic type, it knows that it will be either
    Aliphatic, or a supertype of Aliphatic-such types can bind any value that is either
    Aliphatic or a subtype of it.</p>
    
    
<pre> 
package com.mvivekweb.ocpjp6.obj6.sect4.exam;
    
    
public class Egg&lt;E extends Object&gt; {
    E egg;
 public Egg(E egg) {
 this.egg=egg;
 }
 public E getEgg() {
 return egg;
 }
 public static void main(String[] args) {
 Egg&lt;Egg&gt; egg1 = new Egg(42);
 Egg egg2 = new Egg&lt;Egg&gt;(egg1.getEgg());
 Egg egg3 = egg1.getEgg();
 }
}
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/exam/Egg.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
Note: com\mvivekweb\ocpjp6\obj6\sect4\exam\Egg.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj6/sect4/exam/Egg
Exception in thread "main" java.lang.ClassCastException: java.lang.Integer cannot be cast to com.mvivekweb.ocpjp6.obj6.sect4.exam.Egg
        at com.mvivekweb.ocpjp6.obj6.sect4.exam.Egg.main(Egg.java:16)

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 -Xlint:unchecked com/mvivekweb/ocpjp6/obj6/sect4/exam/Egg.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\exam\Egg.java:14: warning: [unchecked] unchecked call to Egg(E) as a member of the raw type Egg
 Egg&lt;Egg&gt; egg1 = new Egg(42);
                 ^
  where E is a type-variable:
    E extends Object declared in class Egg
com\mvivekweb\ocpjp6\obj6\sect4\exam\Egg.java:14: warning: [unchecked] unchecked conversion
 Egg&lt;Egg&gt; egg1 = new Egg(42);
                 ^
  required: Egg&lt;Egg&gt;
  found:    Egg
3 warnings

C:\vivek\java7\ocpjp6\ocpjp6\src>    
    
</pre>
<p>Compiler warnings are given when assigning a non-generic object
    to a reference with generic type.</p>

<p>the reference type
    of egg2 doesn't have any generic type specification.</p>

<p>assigns the return value of
    egg1.getEgg() to egg3. It compiles because the compiler assumes that the return value
    of egg1.getEgg() will be an Egg.</p>

<p>because at runtime, the actual return type
    will be an Integer, which causes a ClassCastException to be thrown.</p>

