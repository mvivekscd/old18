 <h3>Given the fully-qualified name of a class that is deployed inside and/or outside a JAR
file, construct the appropriate directory structure for that class. Given a code example and a
classpath, determine whether the classpath will allow the code to compile successfully.</h3>
        
        
        <h3>Searching for Other Classes in directories</h3>
        <p>search for <mark>other classes</mark> that will be <mark>necessary to complete</mark> the
            operation</p>
        
        <p>Both java and javac use the <mark>same</mark> basic <mark>search algorithm</mark></p>
        
        <p>As soon as they <mark>find the class</mark> they're looking for, they <mark>stop searching</mark> for that
            class. In the case that their <mark>search lists</mark> contain <mark>two or more files with the
            same name</mark>, the <mark>first file found</mark> will be the file that is used.</p>

        <p>The <b>first</b> place they look is in the <mark>directories that contain the classes that
            come standard</mark> with J2SE.</p>
        
        <p>The <b>second</b> place they look is in the directories defined by <mark>classpaths</mark>.</p>
        
        <p><mark>Classpaths</mark>-They are <mark>lists of
            directories</mark> in which classes might be found</p>
        
        <p> two places where classpaths can be declared</p>
        
             
a)A classpath can be declared as an operating system <mark>environment variable</mark>.
The classpath declared <mark>here</mark> is <mark>used by default</mark>, whenever java or javac are
invoked.
<br>
b)A classpath can be declared as a <mark>command-line</mark> option for either <mark>java</mark> or
<mark>javac</mark>.Classpaths declared as command-line options <mark>override</mark> the <mark>classpath declared
as an environment variable</mark>, but they persist only for the length of the invocation



<h3>Declaring and Using Classpaths</h3>

<p>Classpaths consist of a variable number of <mark>directory locations</mark>, <mark>separated by
    delimiters</mark></p>

    <p> For Unix-based operating systems</p>
    
    a)<mark>forward slashes(/)</mark> are used to
construct <mark>directory locations</mark>
    <br>    
    b)the <mark>separator</mark> is the <mark>colon (:)</mark>
    
    <pre>    
-classpath <mark>/com/foo/acct</mark>:<mark>/com/foo</mark>    
    </pre>
    
    <p>In both cases, these <mark>directories</mark> are absolutely <mark>tied to the root</mark> of the file
        system, which is specified by the leading <mark>forward slash</mark>.</p>

    <p>It's important to remember
        that when y<mark>ou specify a subdirectory</mark>, <b>you're NOT specifying the directories above</b> it.</p>    
    
    <p> exam will use <mark>Unix conventions</mark>.</p>
    
    <p>If you are a Windows user, your directories will be declared using <mark>backslashes (\)</mark> and the
        separator character you use will be a <mark>semicolon (;)</mark>.</p>
    
    <p> A very <mark>common situation</mark> occurs in which <mark>java or javac</mark> <mark>complains</mark> that it can't
        find a class file, and yet you can see that the <mark>file is IN the current directory</mark>!</p>
  
    <p>When
        searching for class files, the java and javac commands don't search the current
        directory by default. You must tell them to search there.The way to tell java or
        javac to <mark>search in the current directory</mark> is to <mark>add</mark> a dot <mark>(.)</mark> to the <mark>classpath</mark></p>
    
    <pre>    -classpath /com/foo/acct:/com/foo:<mark>.</mark>
    </pre>
    
    <p><mark>javac</mark> looks in the <mark>current directory by default</mark></p>
    
    <p>It's also important to remember that <mark>classpaths</mark> are <mark>searched</mark> from <mark>left to right</mark></p>
    
    <pre>-classpath /com:/foo:.
    -classpath .:/foo:/com</pre>
    
    <p>the <mark>java</mark> command allows you to abbreviate <mark>-classpath</mark> with <mark>-cp</mark>.On most machines it does, but there are <mark>no guarantees</mark></p>
 
    <h3>Packages and Searching</h3>
    
    <p>Once a class is in
        a package, the package part of its fully qualified name is atomic-it can never be
        divided. You <mark>can't split</mark> it up on the <mark>command line</mark>, and you can't split it up in an
        <mark>import statement</mark>.</p>
    
    <pre>import com.foo.MyClass; // either import will work
import com.foo.*;</pre>
    
    
    <p>the directory that has all of these attributes has to be accessible (via a
        classpath) in one of two ways</p>
    
    a)The path to the directory must be absolute, in other words, from the root
(<mark>the file system root</mark>, <mark>not the package root</mark>).
    <br>    
    b)The path to the directory has to be <mark>correct relative</mark> to the <mark>current directory</mark>.
    
    
    <h3>Relative and Absolute Paths</h3>
    
    <p>Each path in a classpath is either
        an <mark>absolute path</mark> or a <mark>relative path</mark></p>
    
    
    <p>An absolute path in Unix begins with a forward
        slash (/) (on Windows it would be something like c:\). The <mark>leading slash</mark> indicates
        that this <mark>path is starting from the root directory</mark> of the system</p>
    
    
    <p>A relative path is one that <mark>does NOT start with a slash</mark></p>
    
    <pre>
    / (root)
        |
        |--dirA
            |
            |-- dirB
                    |
                    |--dirC
                        
                        
    </pre>
    <pre>
    -cp <mark>dirB</mark>:<mark>dirB/dirC</mark>
    </pre>
    <p>dirB and dirB/dirC are <mark>relative paths</mark></p>
    <p>Both of these relative paths are <mark>meaningful only</mark> when the <mark>current directory
        is dirA</mark></p>
    
    
        <p>When the current directory is dirA, then <mark>dirB and dirC</mark> will be searched, but <mark>not
            dirA</mark> (remember, <mark>we didn't specify the current directory by adding a dot (.)</mark> to the
            classpath)</p>
        
        
        <p>When the current directory is <mark>root</mark>, since dirB is not a direct subdirectory
            of root, <mark>no directories will be searched</mark></p>

        <p>if the current directory
            is <mark>dirB</mark>? Again, <mark>no directories will be searched</mark>! This is because dirB doesn't have
            a subdirectory named dirB</p>

        
        <pre> -cp <mark>/</mark>dirB:<mark>/</mark>dirA/dirB/dirC</pre>
        
        
         <pre>interpret as -cp <mark>root/</mark>dirB:<mark>root/</mark>dirA/dirB/dirC</pre>
        
         <p> The first path <mark>(/dirB)</mark> is invalid since <mark>dirB is not a direct subdirectory of
             root</mark>, so <mark>dirB will never be searched</mark>.</p>
         
         <h3>JAR Files and Searching</h3>
         
         <pre> 
         test
            |--UseStuff.java
            |--ws
                |--(create MyJar.jar here)
                |--myApp
                    |--utils
                    |   |--Dates.class (package myApp.utils;)
                    |--engine
                        |--rete.class (package myApp.engine;)
                        |--minmax.class " "
    
         </pre>
         
         <p>All of the <mark>classes in the JAR</mark>
             file can be <mark>accessed, via classpaths</mark>, <mark>by java and javac</mark>, <mark>without ever unJARing</mark> the
             JAR file.</p>
         
         <p> <mark>make a JAR</mark> file called MyJar.jar</p>
         
         <pre>cd ws
        jar <mark>-cf</mark> <mark>MyJar.jar</mark> <mark>myApp</mark> // create file</pre>
         
         <p>The jar command will create a JAR file called MyJar.jar and it will contain the
             <mark>myApp directory and myApp's entire subdirectory tree and files</mark></p>
         
         
         <p>look at the
             contents of the JAR file</p>
         
         <pre>jar <mark>-tf</mark> MyJar.jar // test file</pre>
         
         <p><mark>java and javac</mark> will use the JAR like a <mark>normal directory tree</mark>.</p>
         
         <p>Finding a JAR file using a classpath is similar to finding a
             package file in a classpath</p>

         <p>The difference is that when you specify a path for a JAR
             file, <mark>you must include the name of the JAR file</mark> at the <mark>end of the path</mark></p>
         
         
         <p>compile UseStuff.java in the <mark>test</mark> directory, and UseStuff.java needs access
             to a class contained in myJar.jar.</p>
         
         <pre>cd test
javac -classpath <mark>ws/myJar.jar</mark> UseStuff.java</pre>
         
         <p> If UseStuff.java
             needed to <mark>use classes</mark> in the <mark>myApp.utils package</mark>, and the <mark>class was not in a JAR</mark>
         </p>

         <pre> cd test
javac -classpath <mark>ws</mark> UseStuff.java</pre>
         
         <p> if <mark>ws is on the classpath and ws is the super-directory of myApp</mark>,
             then classes in both the myApp.utils and myApp.engine packages will be found</p>
         
         <p>When you say import java.util.*; you are saying "Use the short name for all of the
             classes in the java.util package." You're <mark>NOT getting</mark> the <mark>java.util.jar</mark> classes or
             <mark>java.util.regex</mark> packages! Those packages are totally independent of each other</p>
         
         <p>you can't say import <mark>java.*;</mark></p>
         
         <p>Buried deep inside of your Java directory
             tree is a subdirectory tree named <mark>jre/lib/ext</mark>. If you put <mark>JAR files into the ext</mark>
             subdirectory, java and javac can find them, and use the class files they contain.
         
         <mark>don't have to mention these subdirectories in a classpath statement</mark>-searching
             this directory is a function that's built right into Java</p>

           
         <pre>         
package one;
    
public class One {
    
}
         </pre>   
         
         
         
         <pre>package two;
    
import one.One;
    
    
public class Two {
    public static void main(String args[]) {
    One one = new One();
        System.err.println("hi");
    }
}
    
         </pre>
         <pre class='out'>       
  C:\vivek\docs\cert\ocpjp6\src>javac one/One.java
      
C:\vivek\docs\cert\ocpjp6\src><mark>javac two/Two.java</mark>
    
C:\vivek\docs\cert\ocpjp6\src><mark>java two/Two</mark>
hi
    
         </pre>
         
         
         
<pre class='out'>


C:\vivek\docs\cert\ocpjp6\src\<mark>two</mark>>javac -classpath <mark>../</mark> Two.java

C:\vivek\docs\cert\ocpjp6\src\two><mark>cd ..</mark>

C:\vivek\docs\cert\ocpjp6\src>java two/Two
hi

C:\vivek\docs\cert\ocpjp6\src>
    </pre>
         
         
         
         <pre>         
foo
    |
    test
        |
        xcom
            |--A.class
            |--B.java         
                
         </pre> 
    
         <pre>         
package xcom;
public class A { }         
         </pre>   
         <pre>   
package xcom;
public class B extends <mark>A </mark>{ }         
         </pre>  
 
<pre class='out'> 

C:\foo\test>cd xcom

C:\foo\test\xcom>javac A.java

C:\foo\test\xcom><mark>javac B.java</mark>
B.java:2: cannot find symbol
symbol: class A
public class B extends A { }-->
                       ^
1 error

C:\foo\test\<mark>xcom</mark>>javac -classpath <mark>.</mark> B.java
B.java:2: cannot find symbol
symbol: class A
public class B extends A { }-->
                       ^
1 error

C:\foo\test\xcom>cd ..

C:\foo\<mark>test</mark>>javac -classpath <mark>xcom</mark> B.java
javac: file not found: B.java
Usage: javac <options> <source files>
use -help for a list of possible options

C:\foo\test>javac -classpath <mark>xcom:.</mark> B.java
javac: file not found: B.java
Usage: javac <options> <source files>
use -help for a list of possible options

C:\foo\test>javac -classpath <mark>xcom;.</mark> B.java
javac: file not found: B.java
Usage: javac <options> <source files>
use -help for a list of possible options

C:\foo\test>javac -classpath <mark>.</mark> <mark>xcom/B.java</mark>

C:\foo\test>   
    </pre>
         
         <p>Because <mark>A.class</mark> is in the
             <mark>xcom</mark> package the <mark>compiler</mark> won't find <mark>A.class</mark> <mark>if it's invoked from</mark> the <mark>xcom</mark> directory. </p>      

         <pre>         
Version 1 is in /foo/bar
Version 2 is in /foo/bar/baz         
    
         </pre>
         
 


         <pre>
public class MyClass {
    public static void main(String args[]) {
        
        System.err.println("foo bar");
            
    }
}        
         </pre>
         
         
         <pre> 


       
public class MyClass {
    public static void main(String args[]) {
        
        System.err.println("foo bar baz");
    }
}         
         </pre>     
         
         
<pre class='out'> 

C:\foo>java -classpath bar\baz;bar MyClass
foo bar baz

C:\foo>java -classpath bar;bar\baz MyClass
foo bar

C:\foo>java -classpath <mark>.bar</mark>;bar\baz MyClass
foo bar baz

C:\foo>java -classpath <mark>.</mark>;bar;bar\baz MyClass
foo bar

C:\foo>
    </pre>
         
         <p>        example 2</p>

         <pre> 
C:\foo\viv
        
public class One {
    
}         
         </pre>   
         <pre>

C:\foo\bar

public class MyClass {
    public static void main(String args[]) {
		One one= new One();
        System.err.println("foo bar");
            
    }
}         
         </pre>    

         
<pre class='out'>
C:\foo>cd viv

C:\foo\viv><mark>javac One.java</mark>

C:\foo\viv><mark>cd ..</mark>

C:\foo>javac -classpath c:\foo\viv
javac: no source files
Usage: javac <options> <source files>
use -help for a list of possible options

C:\foo><mark>cd  bar</mark>

C:\foo\bar><mark>javac -classpath c:\foo\viv MyClass.java</mark>

C:\foo\bar>javac -classpath \viv MyClass.java
MyClass.java:6: cannot find symbol
symbol  : class One
location: class MyClass
                One one= new One();
                ^
MyClass.java:6: cannot find symbol
symbol  : class One
location: class MyClass
                One one= new One();
                             ^
2 errors

C:\foo\bar>         
</pre>
         
         <p>common folder     
             <mark>C:\foo</mark></p>

         <pre>
folder structure
    
    C:\foo
        |    
        |-- viv
        |      |
        |      |--One.java
        |-- bar
              |  
              MyClass.java
                  
              <mark>remember no package is mentioned in java file </mark>      
         </pre>