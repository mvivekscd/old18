<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class VLA2 implements <mark>Comparator&lt;VLA2&gt;</mark>{
    
int dishSize;
    public static void main(String[] args) {
      VLA2[] va = {new VLA2(40), new VLA2(200), new VLA2(60)};
          
<mark> Arrays.sort(va, va[0]);</mark>
 <mark>int index = Arrays.binarySearch(va, new VLA2(40), va[0]);</mark>
 System.out.print(index + " ");
 index = Arrays.binarySearch(va, new VLA2(80), va[0]);
 System.out.print(index);  
    }
        
   <mark> public int compare(VLA2 a, VLA2 b) {</mark>
 return b.dishSize - a.dishSize;
 }
VLA2(int d) { dishSize = d; }
    
}
</pre>
<pre class='out'>
run:
2 -2BUILD SUCCESSFUL (total time: 0 seconds)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class Unturned {
    
    
    public static void main(String[] args) {
        String[] towns = {"aspen", "vail", "t-ride", "dillon"};
MySort ms = new MySort();
 Arrays.sort(towns, ms);
 System.out.println(Arrays.binarySearch(towns, "dillon"));
    }
static class MySort implements Comparator&lt;String&gt; {
 public int compare(String a, String b) {
 return b.compareTo(a);
 } }
}
</pre>
<pre class='out'>
run:
-1
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>The binarySearch() method gives meaningful results only if it uses the
    same Comparator as the one used to sort the array. The static inner class is legal, and on
    the real exam expect to find inner classes sprinkled throughout questions that are focused
    on other objectives.</p>

<pre>Given a partial API:
Final class Items implements no interfaces and has one constructor:
Items(String name, int value)
And given that you want to make collections of Items objects and sort them (using classes
and interfaces in java.lang or java.util), sometimes by name, and sometimes by value,
which are true? (Choose all that apply.)</pre>

<p>It's likely that you'll use the Collections class.</p>

<p>It's likely that you'll implement Comparator at least twice.</p>

<p>It's likely that you'll implement the compare() method at least twice.</p>

<p>The most natural way to use the Java API in this case would be to
    create two classes, each of which implements Comparator. Each class that implements
    Comparator will implement a compare() method. Once this is done, you'd use
    Collections.sort() to sort the collections.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
import java.util.Arrays;
import java.util.Comparator;
    
    
public class VLA implements Comparator&lt;VLA&gt;{
    
int dishSize;
    public static void main(String[] args) {
        VLA[] va = {new VLA(40), new VLA(200), new VLA(60)};
            
 for(VLA v: va) System.out.print(v.dishSize + " ");
 int index = Arrays.binarySearch(va, new VLA(60), va[0]);
 System.out.print(index + " ");
 Arrays.sort(va);
 for(VLA v: va) System.out.print(v.dishSize + " ");
 index = Arrays.binarySearch(va, new VLA(60), va[0]);
 System.out.println(index);
    }
public int compare(VLA a, VLA b) {
 return a.dishSize - b.dishSize;
 }
 VLA(int d) { dishSize = d; }
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.<mark>ClassCastException:</mark> com.mvivekweb.ocpjp6.obj6.sect5.exam.VLA cannot be cast to java.lang.Comparable
<mark>40 200 60 -2 </mark>	at java.util.ComparableTimSort.countRunAndMakeAscending(ComparableTimSort.java:320)
	at java.util.ComparableTimSort.sort(ComparableTimSort.java:188)
	at java.util.Arrays.sort(Arrays.java:1246)
	at com.mvivekweb.ocpjp6.obj6.sect5.exam.VLA.main(VLA.java:18)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<p>Arrays.sort() assumes that the elements of the array to be sorted
    implement Comparable unless you provide a Comparator. Note that Arrays
    .binarySearch() doesn't throw an exception when passed an unsorted array, it just
    returns an unpredictable (usually negative) result.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
import java.util.Collections;
import java.util.TreeSet;
    
    
public class Volleyball {
    
    
    public static void main(String[] args) {
        TreeSet&lt;String&gt; s = new TreeSet&lt;String&gt;();
 s.add("a"); s.add("f"); s.add("b");
 System.out.print(s + " ");
Collections.reverse(s);
 System.out.println(s);
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect5/exam/Volleyball.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect5\exam\Volleyball.java:16: error: incompatible types: <mark>TreeSet&lt;String&gt; cannot be converted to List&lt;?&gt;</mark>
Collections.reverse(s);
                    ^
Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>The Collections methods <mark>sort(), reverse(), and binarySearch()
        do not work on Sets.</mark></p>


        <pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
    
    
public class VC {
    
    
    public static void main(String[] args) {
        List&lt;Integer&gt; x = new ArrayList&lt;Integer&gt;();
 Integer[] a = {3, 1, 4, 1};
 x = Arrays.asList(a);
 a[3] = 2;
 x.set(0, 7);
for(Integer i: x) System.out.print(i + " ");
 x.add(9);
 System.out.println(x);
    }
        
}
    
</pre>
        
 <pre class='out'>  run:
Exception in thread "main" <mark>java.lang.UnsupportedOperationException</mark>
	at java.util.AbstractList.add(AbstractList.java:148)
	at java.util.AbstractList.add(AbstractList.java:108)
	at com.mvivekweb.ocpjp6.obj6.sect5.exam.VC.main(VC.java:20)
<mark>7 1 4 2</mark> C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)     </pre>
        
        <pre>
package com.mvivekweb.ocpjp6.obj6.sect5.exam;
    
    
public class MyStuff implements Comparable&lt;MyStuff&gt;{
   MyStuff(String n, int v) { name = n; value = v; }
String name; int value; 
public int compareTo(MyStuff m) {
<mark>return name.compareTo(m.name); // sort alphabetically</mark>
<mark>// return value - m.value; // 2nd answer, to sort ascending</mark>
}
public String toString() { return name + " " + value + " "; }
}
        </pre>