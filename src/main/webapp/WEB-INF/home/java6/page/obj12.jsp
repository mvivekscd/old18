<h3>Develop code that declares an interface. Develop code that implements or extends one
    or more interfaces. Develop code that declares an abstract class. Develop code that extends
    an abstract class.</h3>


<img src="/imag/jp6/interface.png" class="imgw">

<h3>Declaring an Interface</h3>

<p>you're defining a contract for what a class can do,
    without saying anything about how the class will do it. <mark>An interface is a contract</mark>.</p>

<p>interface as a <mark>100-percent abstract class</mark></p>

<p>an abstract class can define both abstract and non-abstract
    methods,<mark> an interface can have only abstract methods</mark></p>

<p>All interface methods are <mark>implicitly public </mark>and <mark>abstract</mark>. In other words,
    you <mark>do not need to actually type the public or abstract</mark> modifiers in the
    method declaration, but the <mark>method is still always public and abstract</mark>.</p>

<p>All variables defined in an interface must be <mark>public, static,</mark> and <mark>final</mark></p>

<p>Interface methods must <mark>not be static</mark>.</p>

<p>Because interface methods are abstract, they <mark>cannot be marked final,
    strictfp, or native</mark>.</p>

    <p>An interface can <mark>extend one or more other interfaces</mark>.</p>
    
    <p>An interface <mark>cannot extend anything but another interface</mark>.  </p>
    
    <p>An interface <mark>cannot implement another interface or class</mark>. </p>
    
    <p>An <mark>interface must be declared with the keyword</mark> interface.</p>

    <p>Interface types can be used polymorphically</p>
    
    <p> Typing in the abstract modifier is considered redundant; <mark>interfaces are
        implicitly abstract whether you type abstract or not</mark>.   </p>
    
    <p>The following interface method declarations won't compile:</p>
    <pre>
<mark>final</mark> void bounce(); // final and abstract can never be used
// together, and abstract is implied
<mark>static</mark> void bounce(); // interfaces define instance methods
<mark>private</mark> void bounce(); // interface methods are always public
<mark>protected</mark> void bounce(); // (same as above)    
    </pre>
    <pre> 
 package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;
     
<mark>public abstract</mark> interface Bounceable {
    <mark>public static final</mark> String vivek="v";
    <mark>public abstract</mark> void bounce();
    <mark>public abstract</mark> void setBounceFactor(int bf);
}   
    </pre>
    
    
    <h3>Declaring Interface Constants   </h3>
    
    
    <p>you guarantee that any
        class implementing the interface will have access to the same constant.  </p>
    
    <pre>
    package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;
        
public interface Foo {
    
    int BAR = 42;
        
    void go();
}
    
class Zap implements Foo {
    
    public void go() {
        BAR = 27;
    }
}
    </pre>
    
 <pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/interfaces/Foo.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect2\interfaces\Foo.java:13: error: <mark>cannot assign a value to final variable</mark> BAR
        BAR = 27;
        ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>  </pre>
    
    <p>You <mark>can't change the value of a constant</mark>! Once the value has been assigned, the
        value can never be modified.  </p>
    
    
    <h3>Implementing an Interface    </h3>
    <pre>
    package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;

import java.io.FileNotFoundException;

public abstract interface Bounceable {
    public static final String vivek="v";
    public abstract void bounce() throws FileNotFoundException;
    public abstract void setBounceFactor(int bf);
}
    </pre>
    <pre class='out'>
    package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;

public class Ball <mark>implements Bounceable</mark> {
    public void bounce() {
    }
    public void setBounceFactor(int bf) {
    }
}
</pre>
    
    
    
    <p>nonabstract implementation class must  Provide concrete (nonabstract) implementations for all methods from the
        declared interface.   </p>
    
    <p> Follow all the rules for legal overrides. </p>
    
    <p>must Declare <mark>no checked exceptions</mark> on implementation methods other than
        <mark>those declared by the interface method</mark>, or subclasses of those declared by
        the interface method. </p>
    
    
    <p><mark>Maintain the signature</mark> of the interface method, and maintain the same
        return type (or a subtype). (But it <mark>does not have to declare the exceptions
        declared in the interface method declaration</mark>.)    </p>
    
    <p>An implementation class can itself be abstract  </p>  
    
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;
    
    
public <mark>abstract</mark> class Ball1 implements Bounceable{
    
}   
    </pre>
    
    <pre>
import java.io.FileNotFoundException;
    
public class BeachBall <mark>extends Ball1</mark>{
    
    
    public void bounce() throws FileNotFoundException {
    }
        
        
    public void setBounceFactor(int bf) {
   }
       
}    </pre>
    
    
    <p>A class can <mark>implement more than one interface</mark> </p>
    
    <p>An interface can <mark>extend
            more than one interface</mark> </p>  
            <pre>  
package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;
    
interface Bounceable1 <mark>extends Moveable</mark>, <mark>Spherical</mark> {
}
    
interface Moveable {
    void moveIt();
}
    
interface Spherical {
    void doSphericalThing();
}    
            </pre>
    
    
    
 
<table class="pgt">
    <caption>interface</caption>
    <thead>
        <tr>
            <th>Local Variables</th>
            <th>Variables(non-local)</th>
            <th>Methods</th>
            <th>Class</th>
          
        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="Local Variables">
''
            </td>
 <td data-label="Variables(non-local)">
 <pre>public
<del>protected</del>
default
<del>private</del></pre>
            </td>
<td data-label="Methods">
 <pre>public
<del>protected</del>
default
<del>private</del></pre>
            </td>
             <td data-label="Class">
                <pre>public
default</pre>
            </td>
        </tr>
        <tr>
            <td data-label="Local Variables">
                <del>final</del>
            </td>
            <td data-label="Variables(non-local)">
<pre>
final
static
<del>transient
volatile</del>
</pre>
            </td>
<td data-label="Methods">
<pre>abstract
<del>final
native
static
strictfp
synchronized</del>
</pre>
            </td>
<td data-label="Class">
<pre>abstract
<del>final</del>
strictfp
</pre>
            </td>

        </tr>
        
</table>

<p>
Variable public static final <br>
int or Integer<br>
Method can throw exception

</p>


<pre>
interface Bounceable {
	public static final int vivek=0;
	public static final Integer vivek1=0;
	public static final String vivek2="s";
	public static final short viveks=1;
	public static final Short viveks1=1;
    public abstract void bounce() throws FileNotFoundException;
    public abstract List&lt;String&gt; setBounceFactor(int bf) throws Exception;
}

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.interfaces;

import java.io.FileNotFoundException;
import java.util.List;

strictfp abstract  interface Bounceable {
	
	int vivek=0;
	static int vivek1=0;
	public static final Integer vivek3=0;
	public static final String vivek2="s";
	public static final short viveks=1;
	public static final Short viveks1=1;
	void bounce() throws FileNotFoundException;
    public abstract void bounce1() throws FileNotFoundException;
    public abstract List&lt;String&gt; setBounceFactor(int bf) throws Exception;
    
}

</pre>


<p>Default variable--  public static final and valid return method(no void)</p>
<p>Default method - public abstract return method(void also)</p>
<p>Interface only extends</p>

<p>It is okay for an implementing method to throw either a narrower exception or runtime
exception.</p>

<p>It's okay for an implementing method to use a covariant return
Exam -2.</p>
<pre>
interface Risky {
void doInsane();
}
class Bungee implements Risky {

public void doInsane() throws NullPointerException {
throw new NullPointerException();
}
}

</pre>


<p>When a class implements and extends, the extends declaration comes first.</p>

<p>When an interface extends another interface, it doesn?t have to actually implement anything.</p>

<h3>
Abstract class/method</h3>

<table class="pgt">
    <caption>interface</caption>
    <thead>
        <tr>
            <th>Local Variables</th>
            <th>Variables(non-local)</th>
            <th>Methods</th>
            <th>Class</th>
          
        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="Local Variables">
''
            </td>
 <td data-label="Variables(non-local)">
 <pre>public
protected
default
private</pre>
            </td>
<td data-label="Methods">
 <pre>public
protected
default
<del>private</del></pre>
            </td>
             <td data-label="Class">
                <pre>public
default</pre>
            </td>
        </tr>
        <tr>
            <td data-label="Local Variables">
                <del>final</del>
            </td>
            <td data-label="Variables(non-local)">
<pre>
final
static
transient
volatile
</pre>
            </td>
<td data-label="Methods">
<pre>abstract
<del>final
native
static
strictfp
synchronized</del>
</pre>
            </td>
<td data-label="Class">
<pre>abstract
<del>final</del>
strictfp
</pre>
            </td>

        </tr>
        
</table>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect1;

strictfp abstract class abstractclass {
	 protected static final int vivek=0;
	 private int vivek1=0;
	 int vivek2=0;
	 transient volatile int vivek3=0;
	 public abstract void goFast();
	 abstract void goFast1();
	 protected abstract void goFast2();
	//public abstract static void goFast21();
	 private final static strictfp synchronized void goFast3(){
		 final int i=0;
		 int i1=0;
	 }
	 private final static synchronized native void goFast7();
	 public abstract void goFast4();
	 abstract void goFast5();
	 protected abstract void goFast6();
	 
}
</pre>

<p>Variable- final or volatile</p>

<h3>Non abstract method</h3>

<table class="pgt">
  <tr>

            <th>Methods</th>

          
        </tr>
<tr>
	<td>
	<pre>public
protected
default
private</pre>
	</td>
</tr>
<tr>
	<td>
	<pre>
	<del>abstract</del>
final
native
static
strictfp
synchronized
</pre>
	</td>
</tr>
</table>


<p>Native means no body in the method, similar to an abstract method</p>
<p>Method- native or synchronized</p>

<p>interfaces and
abstract classes do not need to fully implement the interfaces they extend or implement</p>   
    
    
    
    
    
    
    
    
    
    
    
    
    
    