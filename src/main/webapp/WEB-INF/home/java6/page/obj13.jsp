
<h3>Develop code that declares, initializes, and uses primitives, arrays, enums, and
    objects as static, instance, and local variables. Also, use legal identifiers for variable names.</h3>

<h3>Statics</h3>

<h3>Static Variables and Methods</h3>

<p>because then they'd become <mark>class variables</mark>.</p>

<p>there will be <mark>only one copy of
        a static member</mark> regardless of the number of instances of that class</p>


<p>Variables and methods marked static <mark>belong to the class, rather
    than to any particular instance</mark>.</p>
    
<p>you can use a static method or variable
        without having any instances of that class at all.  </p>
    
<p>static
    variables, too, can be accessed without having an instance of a class. </p>   
    
<p>if there are
    instances, <mark>a static variable of a class will be shared by all instances</mark> of that class;
    there is <mark>only one copy</mark>.</p>

<p>Things you can mark as static:</p>
<pre>
Methods
Variables
A class nested within another class, but not within a method 
Initialization blocks
    
</pre>




<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class Frog {
    
    <mark>static</mark> int frogCount = 0; // Declare and initialize
// static variable
    
    public Frog() {
        frogCount += 1; // Modify the value in the constructor
    }
        
    public static void main(String[] args) {
        new Frog();
        new Frog();
        new Frog();
        System.out.println("Frog count is now " + <mark>frogCount</mark>);
    }
        
}
    
</pre>
<pre class='out'>
run:
Frog count is now 3
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<p>the static frogCount variable is set to zero when the Frog
    class is first loaded by the JVM, before any Frog instances are created</p>


<p>you don't actually need to initialize a static variable to zero; static variables get the
    same default values instance variables get</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class Frog {
    
    <mark>static int frogCount;</mark> // Declare and initialize
// static variable
    
    public Frog() {
        System.out.println("frogCount="+frogCount);
        frogCount += 1; // Modify the value in the constructor
    }
        
    public static void main(String[] args) {
        new Frog();
        new Frog();
        new Frog();
        System.out.println("Frog count is now " + frogCount);
    }
        
}
    
</pre>

<pre class='out'>
run:
frogCount=0
frogCount=1
frogCount=2
Frog count is now 3
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>
    A static
    method can't access a nonstatic (instance) variable, <mark>because there is no instance</mark>
</p>

<p>a static method can't directly invoke a nonstatic
    method.</p>

<p>Think <mark>static = class</mark>, <mark>nonstatic = instance</mark></p>


<p>Making the method called by the
    JVM (main()) a static method means the <mark>JVM doesn't have to create an instance
    of your class just to start running code</mark>.</p>

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class Frog {
    
    <mark>int frogCount;</mark> // Declare and initialize
// static variable
    
    public Frog() {
        System.out.println("frogCount="+frogCount);
        frogCount += 1; // Modify the value in the constructor
    }
        
    public <mark>static</mark> void main(String[] args) {
        new Frog();
        new Frog();
        new Frog();
        System.out.println("Frog count is now " + frogCount);
    }
        
}
    
    </pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/statics/Frog.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect3\statics\Frog.java:17: error: <mark>non-static variable frogCount cannot be referenced from a static</mark> context
        System.out.println("Frog count is now " + frogCount);
                                                  ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>    </pre>
    
    
<h3>Accessing Static Methods and Variables</h3>    
    
    
<p>we <mark>access</mark> a static method (or static variable) is to use the <mark>dot operator on
        the class name</mark>  </p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class TestFrog {
    
    public static void main(String[] args) {
        new Frog1();
        new Frog1();
        new Frog1();
        System.out.print("frogCount:" + <mark>Frog1</mark>.frogCount);
    }
        
}
    
class Frog1 {
    
    <mark>static</mark> int frogCount = 0; // Declare and initialize
// static variable
    
    public Frog1() {
        frogCount += 1; // Modify the value in the constructor
    }
}    
    
</pre>

<pre class='out'>
run:
<mark>frogCount:3</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class TestFrog {
    
    public static void main(String[] args) {
        new Frog1();
        new Frog1();
        new Frog1();
        System.out.print("frogCount:" + <mark>Frog1</mark>.frogCount);
        Frog1 f=new Frog1();
        System.out.print("frogCount:" + <mark>f</mark>.frogCount);
    }
        
}
    
class Frog1 {
    
    <mark>static</mark> int frogCount = 0; // Declare and initialize
// static variable
    
    public Frog1() {
        frogCount += 1; // Modify the value in the constructor
    }
}
    
</pre>

<pre class='out'>
run:
frogCount:3frogCount:4BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>the Java language <mark>also allows you to use an
        object reference variable to access a static</mark> member</p>

<p>static methods <mark>can't be overridden</mark></p>


<p>This <mark>doesn't mean they
        can't be redefined</mark> in a subclass,</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.statics;
    
public class Dog extends Animal{
    
    <mark>static</mark> void doStuff() { // it's a redefinition,
// not an override
        System.out.print("d ");
    }
        
    public <mark>static</mark> void main(String[] args) {
        <mark>Animal[] a</mark> = {new Animal(), <mark>new Dog()</mark>, new Animal()};
        for (int x = 0; x < a.length; x++) {
            <mark>a[x].doStuff();</mark>
                
        }
        doStuff();
    }
        
}
    
class Animal {
    
    static void doStuff() {
        System.out.print("a ");
    }
}
    
</pre>
<pre class='out'>
run:
a a a d 

BUILD SUCCESSFUL (total time: 0 seconds)
</pre> 
    
    
<p>the syntax a[x].doStuff() <mark>is just a shortcut</mark> (the syntax trick)...the
    <mark>compiler is going to substitute something like Animal.doStuff()</mark> instead</p>

<h3>Variable Declarations </h3>

<p>There are two types of variables in Java:<mark>Primitives and Reference variables</mark>.</p>

<p>A reference variable is declared to be of a specific type and that type
    can never be changed.</p>


<h3>Declaring Reference Variables</h3>


<p>Reference variables can be declared as </p>
static variables, 
<br>instance variables, 
<br>method
parameters, 
<br>or local variables.

<p>
    You <mark>can declare one or more reference variables,
        of the same type, in a single line</mark></p>

        <pre>String s1, s2, s3;</pre>


<h3>Instance Variables</h3>

<p>Instance variables are defined <mark>inside the class, but outside of any method</mark>, and
    are <mark>only initialized when the class is instantiated</mark>.</p>

<p>Can be marked <mark>final</mark></p>
<p>Can be marked <mark>transient</mark></p>

<h3>Local (Automatic/Stack/Method) Variables</h3>

<p>Local variables are always <mark>on the stack</mark>, not the heap</p>

<p>local variables can be
    marked <mark>final</mark>.</p>


<p>before a
    local variable can be used, <mark>it must be initialized with a value</mark></p>

<p>It is possible to declare a <mark>local variable with the same name as an instance
        variable</mark>. It's known as <mark>shadowing</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class TestServer {
    
   <mark> int count = 9;</mark> // Declare an instance variable named count
        
    public void logIn() {
        <mark>int count = 10;</mark> // Declare a local variable named count
        System.out.println("local variable count is " + count);
    }
        
    public void count() {
        System.out.println("instance variable count is " + count);
    }
        
    public static void main(String[] args) {
        new TestServer().logIn();
        new TestServer().count();
    }
        
}
    
</pre>

<pre class='out'>
run:
local variable count is 10
instance variable count is 9
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>



<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
    
public class testlocalvariable {
    
 <mark>int count = 9;</mark> // Declare an instance variable named count
     
    public void logIn() {
        <mark>this.count</mark> = 10; // Declare a local variable named count
        System.out.println("local variable count is " + count);
    }
        
    public void count() {
        System.out.println("instance variable count is " + this.count);
    }
    public static void main(String[] args) {
         new testlocalvariable().logIn();
        new testlocalvariable().count();
    }
        
}
    
</pre>
<pre class='out'>
run:
local variable count is 10
instance variable count is 9
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
    
public class testlocalvariable {
    
 <mark>static int count = 9;</mark> // Declare an instance variable named count
     
    public void logIn() {
        <mark>count = 10;</mark> // Declare a local variable named count
        System.out.println("local variable count is " + count);
    }
        
    public void count() {
        System.out.println("instance variable count is " + this.count);
    }
    public static void main(String[] args) {
         new testlocalvariable().logIn();
        new testlocalvariable().count();
    }
        
}
    
</pre>
<pre class='out'>
run:
local variable count is 10
instance variable count is 10
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre class='out'>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;


public class testlocalvariable {

 <mark>static int count = 9;</mark> // Declare an instance variable named count

    public void logIn() {
        <mark>this.count = 10;</mark> // Declare a local variable named count
        System.out.println("local variable count is " + count);
    }

    public void count() {
        System.out.println("instance variable count is " + this.count);
    }
    public static void main(String[] args) {
         new testlocalvariable().logIn();
        new testlocalvariable().count();
    }

}

</pre>
<pre class='out'>
run:
local variable count is 10
instance variable count is 10
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
    
public class testlocalvariable {
    
 <mark>static int count = 9;</mark> // Declare an instance variable named count
     
    public void logIn() {
        <mark>int count = 10;</mark> // Declare a local variable named count
        System.out.println("local variable count is " + count);
    }
        
    public void count() {
        System.out.println("instance variable count is " + this.count);
    }
    public static void main(String[] args) {
         new testlocalvariable().logIn();
        new testlocalvariable().count();
    }
        
}
    
    
</pre>
<pre class='out'>
run:
local variable count is 10
instance variable count is 9
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<h3>Array Declarations</h3>


<p>arrays are objects that <mark>store multiple variables</mark> of the <mark>same type</mark>, or variables
    that are all <mark>subclasses of the same type</mark>.</p>



<p>Collection
    classes offer more flexible ways to access an object (for insertion, deletion,
    reading, and so on)</p>



package com.mvivekweb.ocpjp6.obj1.sect3.arrays;

<pre>
public class Arraysdeclaration {
    
    
    public static void main(String[] args) {
        <mark>byte[] anArrayOfBytes;</mark> // recommended
        byte anArrayOfBytes1[];
        String[][] names;
        String names1[][];
        <mark>String[] names2[][];</mark>
    }
        
}
    
</pre>


<p>It is <mark>never legal to include the size of the array</mark> in your declaration.</p>


<h3>Final Variables</h3>

<p>Declaring a variable with the final keyword makes it <mark>impossible to reinitialize that
        variable</mark> once it has been initialized with an explicit value</p>

<p>A reference variable
    marked final <mark>can't ever be reassigned to refer to a different object</mark>.</p>

<p>The <mark>data within
        the object can be modified</mark>, but the reference variable cannot be changed</p>

<p>there are <mark>no final objects</mark>, <mark>only final references</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class FinalTest {
    
    final static int size = 10;
        
    public static void main(String[] args) {
        size = 12;
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - cannot assign a value to final variable size
	at com.mvivekweb.ocpjp6.obj1.sect3.variables.FinalTest.main(FinalTest.java:8)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class FinalTest {
    
    <mark>final static int size;</mark>
        
    public static void main(String[] args) {
        System.err.println("size="+size);
       // size = 12;
    }
        
}
</pre>
<pre class='out'>
run:
java.lang.ExceptionInInitializerError
Caused by: java.lang.RuntimeException: Uncompilable source code - variable size not initialized in the default constructor
	at com.mvivekweb.ocpjp6.obj1.sect3.variables.FinalTest.<clinit>(FinalTest.java:5)
Exception in thread "main" C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class FinalTest {
    
    static int size;
        
    public static void main(String[] args) {
        System.err.println("size="+size);
       // size = 12;
    }
        
}</pre>
<pre class='out'>
run:
size=0
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<h3>Volatile Variables</h3>

<p><mark>volatile </mark>is that, as with<mark> transient</mark>, it can be applied only to instance variables.
</p>


<h3>Declaring Enums</h3>

<p>
    Java lets you <mark>restrict a variable to having one of only a few pre-defined</mark>
    values-in other words, one value from an enumerated list</p>


<p>Using enums can <mark>help reduce the bugs</mark> in your code</p>

<p>It's <mark>not required that enum constants be in all caps</mark></p>


<p>Enums can be declared as their own <mark>separate class</mark>, or <mark>as a class member</mark>,
    however they must <mark>not be declared within a method</mark>!</p>


<h3>Declaring an enum outside a class</h3>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
    
<mark>enum</mark> CoffeeSize {
    BIG, HUGE, OVERWHELMING, <mark>vivek</mark>
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public class CoffeeTest1 {
    
    public static void main(String[] args) {
        <mark>Coffee drink</mark> = new Coffee();
        drink.size = <mark>CoffeeSize.BIG</mark>;
        System.out.println("drink.size="+drink.size);
    }
        
}
    
class Coffee {
    
    <mark>CoffeeSize size;</mark>
}
    
</pre>
<pre class='out'>
run:
drink.size=BIG
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>declaring an enum inside a class</h3>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public class CoffeeTest2 {
    
    public static void main(String[] args) {
        Coffee2 drink = new Coffee2();
        drink.size = <mark>Coffee2.CoffeeSize.BIG;</mark>
        System.out.println("drink.size="+drink.size);
    }
        
}
    
class Coffee2 {
    
    <mark>enum</mark> CoffeeSize {
        BIG, HUGE, OVERWHELMING
    <mark>}</mark>
    <mark>CoffeeSize size;</mark>
}
</pre>
<pre class='out'>
run:
drink.size=BIG
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>optional to
    put a semicolon at the end of the enum declaration</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public class CoffeeTest2 {
    
    public static void main(String[] args) {
        Coffee2 drink = new Coffee2();
        drink.size = Coffee2.CoffeeSize.BIG;
        System.out.println("drink.size="+drink.size);
    }
        
}
    
class Coffee2 {
    
    enum CoffeeSize {
        BIG, HUGE, OVERWHELMING<mark>;</mark>
    }<mark>;</mark>
    CoffeeSize size;
}
    
</pre>

<p>The most important thing to
    remember is that <mark>enums are not Strings or ints</mark></p>

<p>Each of the enumerated CoffeeSize
    types are actually instances of CoffeeSize. In other words, <mark>BIG is of type CoffeeSize</mark></p>

<pre>
class CoffeeSize {
public static final CoffeeSize BIG =
<mark>new CoffeeSize("BIG", 0);</mark>
public static final CoffeeSize HUGE =
new CoffeeSize("HUGE", 1);
public static final CoffeeSize OVERWHELMING =
new CoffeeSize("OVERWHELMING", 2);
public CoffeeSize(String enumName, int index) {
// stuff here
}
public static void main(String[] args) {
System.out.println(CoffeeSize.BIG);
}
}
    
</pre>


<p>each enum <mark>value knows
        its index or position</mark></p>

<p>think of the CoffeeSize <mark>enums as existing in an array of type</mark>
            CoffeeSize</p>

<p>you can <mark>iterate through the values of
        an enum</mark> by invoking the <mark>values()</mark> method on any enum type</p>



<h3>Declaring Constructors, Methods, and Variables in an enum</h3>

<p>You can add <mark>constructors</mark>, <mark>instance variables</mark>, <mark>methods</mark>,
    and something really strange known as a <mark>constant specific class body</mark>.</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
    
public enum CoffeeSize1 {
    BIG("B"), HUGE, OVERWHELMING, vivek;    
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\enums>javac -source 1.6 CoffeeSize1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
CoffeeSize1.java:6: error: <mark>constructor CoffeeSize1 in enum CoffeeSize1 cannot be applied</mark> to given types;
    <mark>BIG("B")</mark>, HUGE, OVERWHELMING, vivek;
       ^
  required: no arguments
  found: String
  reason: actual and formal argument lists differ in length
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\enums>

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public enum CoffeeSize1 {
    BIG(<mark>"B"</mark>), HUGE("H"), OVERWHELMING("O"), vivek("v"); 
        
    <mark>CoffeeSize1(String ounces){// constructor</mark>
        this.ounces = ounces;
    }
        
    <mark>private String ounces;</mark> // an instance variable
        
    public String <mark>getOunces() {</mark>
        return ounces;
    }
        
    public void setOunces(String ounces) {
        this.ounces = ounces;
    }
}
    
</pre>
<pre class='out'>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;

public class Coffee1 {

    <mark>CoffeeSize1 size;</mark>// each instance of Coffee has an enum

    public static void main(String[] args) {
        Coffee1 drink1 = new Coffee1();
        drink1.size = <mark>CoffeeSize1.BIG;</mark>
        Coffee1 drink2 = new Coffee1();
        drink2.size = CoffeeSize1.OVERWHELMING;
        System.out.println(drink1.size.getOunces()); // prints 8
        for (CoffeeSize1 cs : <mark>CoffeeSize1.values()</mark>) {
            System.out.println(cs + " " + cs.getOunces());
        }
    }

}

</pre>

<pre class='out'>
run:
B
BIG B
HUGE H
OVERWHELMING O
vivek v
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>Every enum has a static method, <mark>values()</mark>, that returns an <mark>array of the enum's
        values</mark> in the order they're declared.
</p>


<p>You <mark>can NEVER invoke an enum constructor</mark> directly</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public enum CoffeeSize1 {
    BIG("B"), HUGE("H"), OVERWHELMING("O"), vivek("v"); 
        
    CoffeeSize1(String ounces){// constructor
        System.out.println("constructor called");
        this.ounces = ounces;
    }
        
    private String ounces; // an instance variable
        
    public String getOunces() {
        return ounces;
    }
        
    public void setOunces(String ounces) {
        this.ounces = ounces;
    }
}
    
</pre>
<pre class='out'>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;

public class Coffee1 {

    CoffeeSize1 size;// each instance of Coffee has an enum

    public static void main(String[] args) {
        Coffee1 drink1 = new Coffee1();
        drink1.size = CoffeeSize1.BIG;
        Coffee1 drink2 = new Coffee1();
        drink2.size = CoffeeSize1.OVERWHELMING;
        System.out.println(drink1.size.getOunces()); // prints 8
        for (CoffeeSize1 cs : CoffeeSize1.values()) {
            System.out.println(cs + " " + cs.getOunces());
        }
    }

}

</pre>
<pre class='out'>
run:
constructor called
constructor called
constructor called
constructor called
B
BIG B
HUGE H
OVERWHELMING O
vivek v
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<p>
    The enum <mark>constructor
        is invoked automatically</mark>, with the arguments you define after the constant
    value.
</p>
<p>
    You can <mark>define more than one argument to the constructor</mark>, and you can
    overload the enum constructors, just as you can overload a normal class
    constructor.
</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
    
public enum twoArgs {
    <mark>BIG("B",10);</mark>
<mark>    private int valu;
    private String name;</mark>
        
    public int getValu() {
        return valu;
    }
        
    public void setValu(int valu) {
        this.valu = valu;
    }
        
    public String getName() {
        return name;
    }
        
    public void setName(String name) {
        this.name = name;
    }
        
        
    <mark>twoArgs(String name,int valu){</mark>
        this.valu = valu;
        this.name = name;
    }
        
        
}</pre>

<p>an enum that <mark>looks like an
        anonymous inner class</mark> (which we talk about in Chapter 8). It's known as a <mark>constant
            specific class body</mark></p>

<h3>constant
                specific class body  </h3>
            
            
<p>use it when you <mark>need a particular constant</mark> to <mark>override a
                    method</mark> defined in the enum.</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.enums;
    
public enum CoffeeSize2 {
    BIG(8),
    HUGE(10),
    OVERWHELMING(16) <mark>{ // start a code block that defines
// the "body" for this constant
        public String getLidCode() { // override the method
// defined in CoffeeSize
            return "A";
        }
    }; // the semicolon is REQUIRED when more code follows</mark>
        
    CoffeeSize2(int ounces) {
        this.ounces = ounces;
    }
    private int ounces;
        
    public int getOunces() {
        return ounces;
    }
        
    public String <mark>getLidCode</mark>() { // this method is overridden
// by the OVERWHELMING constant
        return "B"; // the default value we want to return for
// CoffeeSize constants
    }
}
    
    
</pre>
<pre >
package com.mvivekweb.ocpjp6.obj1.sect3.enums;

public class ConstantSpecificClassbody {

    CoffeeSize2 size;

    public static void main(String[] args) {
        ConstantSpecificClassbody drink1 = new ConstantSpecificClassbody();
        drink1.size = CoffeeSize2.BIG;
        ConstantSpecificClassbody drink2 = new ConstantSpecificClassbody();
        drink2.size = CoffeeSize2.OVERWHELMING;
        System.out.println(drink1.size.getOunces()); // prints 8
        System.out.println(drink1.size.getLidCode()); // prints 8
        System.out.println(drink2.size.getOunces()); // prints 8
        System.out.println(drink2.size.getLidCode()); // prints 8
        

    }

}

</pre>
<pre class='out'>
run:
8
B
16
A
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<h3>Initialization Blocks</h3>

<p>two places in a class where you can put code that performs
    operations: <mark>methods</mark> and <mark>constructors</mark>. <mark>Initialization blocks</mark> are the third place in a
    Java program where operations can be performed
</p>


<p>Initialization blocks run when the
    c<mark>lass is first loaded</mark> (<mark>a static initialization block</mark>) or when an <mark>instance is created</mark> (an
    <mark>instance initialization block</mark>)</p>

<p>They <mark>don't
        have names</mark>, they <mark>can't take arguments</mark>, and they <mark>don't return anything</mark></p>

<p>A static
    initialization block runs once, when the class is first loaded</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
public class Init {
	Init(int x) {
		System.out.println("1-arg const");
	}
            
	Init() {
		System.out.println("no-arg const");
	}
            
	<mark>static {</mark>
		System.out.println("1st static init");
	}
	{
		System.out.println("1st instance init");
	}
	{
		System.out.println("2nd instance init");
	}
	<mark>static {</mark>
		System.out.println("2nd static init");
	}
            
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//new Init();
		System.out.println("-----");
		//new Init(7);
	}
            
}
    
</pre>
<pre class='out'>
run:
1st static init
2nd static init
-----
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>
    An instance initialization
    block runs once every time a new instance is created
    
</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
public class Init {
	Init(int x) {
		System.out.println("1-arg const");
	}
            
	Init() {
		System.out.println("no-arg const");
	}
            
	static {
		System.out.println("1st static init");
	}
	<mark>{</mark>
		System.out.println("1st instance init");
	}
	<mark>{</mark>
		System.out.println("2nd instance init");
	}
	static {
		System.out.println("2nd static init");
	}
            
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		<mark>new Init();</mark>
		System.out.println("-----");
		//new Init(7);
	}
            
}
    
</pre>
<pre class='out'>
run:
1st <mark>static</mark> init
2nd static init
1st <mark>instance init</mark>
2nd instance init
no-arg <mark>const</mark>
-----
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>
    Instance init block code runs right <mark>after the call to super() in a constructor</mark>, in other words, after all
    super-constructors have run.
    
</p>
<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
public class supercallTest extends superclass {
    
    {
        System.out.println("subclass instancs block");
    }
        
    static {
        System.out.println("subclass static block");
    }
        
    supercallTest() {
        System.out.println("subclass const");
    }
        
    public static void main(String[] args) {
        //new supercallTest();
    }
        
}
    
class superclass {
    
    {
        System.out.println("superclass instancs block");
    }
        
    static {
        System.out.println("superclass static block");
    }
        
    superclass() {
        System.out.println("superclass const");
    }
}
    
</pre>
<pre class='out'>
run:
<mark>superclass static block</mark>
subclass static block
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
public class supercallTest extends superclass {
    
    {
        System.out.println("subclass instancs block");
    }
        
    static {
        System.out.println("subclass static block");
    }
        
    supercallTest() {
        System.out.println("subclass const");
    }
        
    public static void main(String[] args) {
        <mark>new supercallTest();</mark>
    }
        
}
    
class superclass {
    
    {
        System.out.println("superclass instancs block");
    }
        
    static {
        System.out.println("superclass static block");
    }
        
    superclass() {
        System.out.println("superclass const");
    }
}
    
</pre>
<pre class='out'>
run:
superclass static block
subclass static block
<mark>superclass instancs block
superclass const</mark>
subclass instancs block
subclass const
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>When it's time for initialization blocks to run, if a class has more than one,
    <mark>they will run in the order in which they appear</mark> in the class file</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
public class Init {
	Init(int x) {
		System.out.println("1-arg const");
	}
            
	Init() {
            
            super();
		System.out.println("no-arg const");
	}
            
	static {
		System.out.println("1st static init");
	}
	{
		System.out.println("1st instance init");
	}
	{
		System.out.println("2nd instance init");
	}
	static {
		System.out.println("2nd static init");
	}
            
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Init();
		System.out.println("-----");
		new Init(7);
	}
            
}
    
</pre>
<pre class='out'>
run:
1st static init
2nd static init
1st instance init
2nd instance init
no-arg const
-----
1st instance init
2nd instance init
1-arg const
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>Instance init blocks run <mark>every time a class instance is created</mark>.</p>

<p>Instance init blocks <mark>run after the constructor's call to super()</mark>.</p>


<p>Instance init blocks are
    often <mark>used as a place to put code that all the constructors</mark> in a class should <mark>share</mark></p>


<p>if you make a mistake in your static init block, the JVM can throw an
    <mark>ExceptionInInitializationError</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.initializationblock;
    
    
public class InitError {
	static int [] x = new int[4];
	static { x[4] = 5; }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
                    
	}
            
}
    
</pre>
<pre class='out'>
run:
<mark>java.lang.ExceptionInInitializerError</mark>
Caused by: java.lang.ArrayIndexOutOfBoundsException: 4
	at com.mvivekweb.ocpjp6.obj1.sect3.initializationblock.InitError.<clinit>(InitError.java:6)
Exception in thread "main" C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>


<h3>Stack and Heap-Quick Review</h3>

<p>For the most part, the various pieces (methods, variables, and objects) of Java
    programs live in <mark>one of two places in memory: the stack or the heap</mark>.</p>

<p><mark>Instance variables</mark> and <mark>objects</mark> live on the heap.</p>

<p><mark>Local variables</mark> live on the <mark>stack</mark>.</p>

<img src="/imag/jp6/stackheap.png" class="imgw">

<h3>Literals, Assignments, and Variables</h3>

<h3>Integer Literals
</h3>


<p>There are three ways to represent integer numbers in the Java language: <mark>decimal</mark>
    (base 10), <mark>octal</mark> (base 8), and <mark>hexadecimal</mark> (base 16)</p>


<h3>Octal Literals</h3>

<p>Octal integers use only the digits 0 to 7. In Java, you represent
    an integer in octal form by placing a zero in front of the number</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Octal {
    
    public static void main(String[] args) {
        int six = 06; // Equal to decimal 6
        int seven = 07; // Equal to decimal 7
        <mark>int eight = 010; // Equal to decimal 8
        int nine = 011; // Equal to decimal 9</mark>
        System.out.println("Octal 010 = " + eight);
    }
        
}
    
</pre>

<pre class='out'>run:
<mark>Octal 010 = 8</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>You can have up to <mark>21 digits</mark> in an octal
    number, not including the leading zero</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Octal {
    
    public static void main(String[] args) {
        int six = 06; // Equal to decimal 6
        int seven = 07; // Equal to decimal 7
        int eight = 010; // Equal to decimal 8
        <mark>int nine = 012132131; //
        int nine1 = 12132131; // </mark>
        System.out.println("Octal 010 = " + eight);
        System.out.println("Octal 012132131 = " + nine);
        System.out.println("int 12132131 = " + nine1);
    }
        
}
    
</pre>

<pre class='out'>run:
Octal 010 = 8
<mark>Octal 012132131 = 2667609
int 12132131 = 12132131</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Hexadecimal Literals</h3>

<p>Hexadecimal (hex for short) numbers are constructed
    using 16 distinct symbols.</p>


<pre>0 1 2 3 4 5 6 7 8 9 a b c d e f</pre>

<p>Java will accept <mark>capital or lowercase letters</mark> for the extra digits</p>

<p>You are allowed up to <mark>16 digits</mark> in a hexadecimal
    number, <mark>not including the prefix</mark> 0x <mark>or the optional suffix</mark> extension <mark>L</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class HexTest {
    
    public static void main(String[] args) {
        int x = 0X0001;
        int y = 0x7fffffff;
        int z = 0xDeadCafe;
        long jo = 110599L;
        long so = 0xFFFFl; // Note the lowercase 'l'
        System.out.println("x = " + x + " y = " + y + " z = " + z);
        System.out.println("jo=" + jo);
        System.out.println("so=" + so);
    }
        
}
    
</pre>
<pre class='out'>
run:
x = 1 y = 2147483647 z = -559035650
jo=110599
so=65535
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Floating-Point Literals</h3>


<p>Floating-point literals are defined as double (64 bits) by default, so if you want to
    assign a floating-point literal to a variable of type float (32 bits), you must attach the
    suffix F or f to the number.</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
import java.math.BigDecimal;
import java.math.MathContext;
    
    
public class Floating {
    
    
    public static void main(String[] args) {
       double d = 11301874.98810243243243324324234232;
        System.out.println("d="+d);
        BigDecimal bd= new BigDecimal("11301874.98810243243243324324234232",MathContext.UNLIMITED);
        System.out.println("bd="+bd);    
        float f = 23.467890;
    }
        
}
    
</pre>
<pre class='out'>
run:
d=1.1301874988102432E7
bd=11301874.98810243243243324324234232
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - <mark>incompatible types: possible lossy conversion from double to float
	at com.mvivekweb.ocpjp6.obj1.sect3.literals.Floating.main(Floating.java:17)</mark>
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
import java.math.BigDecimal;
import java.math.MathContext;
    
public class Floating {
    
    public static void main(String[] args) {
        double d = 11301874.98810243243243324324234232<mark>D</mark>;
        System.out.println("d=" + d);
        BigDecimal bd = new BigDecimal("11301874.98810243243243324324234232", MathContext.UNLIMITED);
        System.out.println("bd=" + bd);
        float f = 23.467890F;
        System.out.println("f=" + f);
            
        <mark>double d1 = 110599.995011D; // Optional, not required
        double g = 987.897; // No 'D' suffix, but OK because the</mark>
// literal is a double by default
        System.out.println("d1=" + d1);
        System.out.println("g=" + g);
    }
        
}
    
</pre>

<pre class='out'>
run:
d=1.1301874988102432E7
bd=11301874.98810243243243324324234232
f=23.46789
d1=110599.995011
g=987.897
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Character Literals</h3>

<p>
    You can also type in the <mark>Unicode value</mark> of the character, using the Unicode
    notation of prefixing the value with <mark>\u</mark> </p>



<p>characters are just 16-bit unsigned integers under the hood. That
    means <mark>you can assign a number literal, assuming it will fit into the unsigned 16-bit
    range (65535 or less)</mark>.</p>

    <p>You can also use an escape code if you want to represent a character that can't be
        typed in as a literal, including the characters for <mark>linefeed</mark>, <mark>newline</mark>, <mark>horizontal tab</mark>,
        <mark>backspace</mark>, and <mark>single quotes</mark>.</p>


    <pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Char {
    
    public static void main(String[] args) {
        char a = 'a';
        char b = '@';
        System.out.println("a=" + a);
        System.out.println("b=" + b);
            
        char letterN = '\u004E';
        System.out.println("letterN=" + letterN);
        <mark>char a1 = 0x892; // hexadecimal literal
        char b1 = 982; // int literal
        char c = (char) 70000; // The cast is required; 70000 is
// out of char range</mark>
        System.out.println("a1=" + a1);
        System.out.println("b1=" + b1);
        System.out.println("c=" + c);
        <mark>char e = (char) -29; // Possible loss of precision; needs a cast
        System.out.println("e=" + e);
        char c1 = '\"'; // A double quote</mark>
        char d = '\n'; // A newline
        System.out.println("c1=" + c1);
        System.out.println("d=" + d);
    }
        
}
    
    </pre>  
<pre class='out'> run:
a=a
b=@
letterN=N
a1=?
b1=?
c=?
e=?
c1="
d=

BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
    <h3>Assignment Operators</h3>
    
    
    <p> If the reference variable has not
        been assigned a value, or has been explicitly assigned a value of <mark>null</mark>, the variable
        holds bits representing-you guessed it-null.   </p>
    
    
<pre>Button b = null;</pre>
    
<h3>Primitive Assignments
</h3>

<pre>byte b = 27;</pre>

<p>the compiler automatically narrows the literal value to a byte. In
    other words, the compiler puts in the cast. The preceding code is identical to the
    following:</p>

<pre>byte b = (byte) 27; // Explicitly cast the int literal to a byte</pre>

<p><mark>add two bytes</mark> together and <mark>you'll get an int</mark>-even if those two bytes are
    tiny.</p>


<p><mark>Multiply an int</mark> and <mark>a short</mark> and you'll get an <mark>int</mark>.</p>

<p><mark>Divide a short</mark> by a <mark>byte</mark>
    and you'll get an <mark>int</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Byte {
    
    public static void main(String[] args) {
        byte a = 3; // No problem, 3 fits in a byte
        byte b = 8; // No problem, 8 fits in a byte
        byte c = b + c; // Should be no problem, sum of the two bytes
// fits in a byte
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types: <mark>possible lossy conversion from int to byte</mark>
	at com.mvivekweb.ocpjp6.obj1.sect3.literals.Byte.main(Byte.java:8)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Byte {
    
    public static void main(String[] args) {
        byte a = 3; // No problem, 3 fits in a byte
        byte b = 8; // No problem, 8 fits in a byte
        byte c = <mark>(byte)</mark> (a + b); // Should be no problem, sum of the two bytes
// fits in a byte
        System.out.println("c=" + c);
    }
        
}
    
</pre>
<pre class='out'>
run:
c=11
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>have compiled if we'd done the <mark>explicit cast</mark></p>


<h3>Primitive Casting</h3>

<p>An <mark>implicit cast</mark> means you don't have to write
    code for the cast; the conversion happens automatically.</p>

<p>an implicit
    cast happens when you're doing a<mark> widening conversion</mark></p>

<p>In other words, putting a
    <mark>smaller thing (say, a byte) into a bigger container</mark> (like an int).</p>

<p>The <mark>large-value-into-small-container</mark> conversion
    is referred to as <mark>narrowing</mark> and requires an <mark>explicit cast</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class ExplicitCast {
    
    public static void main(String[] args) {
        float a = 100.001f;
        int b = <mark>(int)</mark> a; // Explicit cast, the float could lose info
        System.out.println("b="+b);
    }
        
}
    
</pre>
<pre class='out'>
run:
b=100
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<p>
    we want to assign
    a double value to an integer type, we're attempting a <mark>narrowing conversion</mark>
</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class CastingNarrow {
    
    public static void main(String[] args) {
        int x = 3957.229; // illegal
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types: <mark>possible lossy conversion from double to int</mark>
	at com.mvivekweb.ocpjp6.obj1.sect3.literals.CastingNarrow.main(CastingNarrow.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class CastingNarrow {
    
    public static void main(String[] args) {
        int x = <mark>(int)</mark>3957.229; // illegal
        System.out.println("x="+x);
    }
        
}
    
</pre>
<pre class='out'>
run:
x=3957
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>if the <mark>long value is
        larger than 127</mark> (the largest number a byte can store)</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class CastingNarrow1 {
    
    public static void main(String[] args) {
        long l = 130L;
        byte b = (byte) l;
        System.out.println("The byte is " + b);
    }
        
}
    
</pre>
<pre class='out'>
run:
The byte is -126
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>You don't get a runtime error, even when the value being narrowed is too large
    for the type. The <mark>bits to the left of the lower 8</mark> just go away. If the <mark>leftmost bit</mark> (the
    sign bit) in the byte (or any integer primitive) now <mark>happens to be a 1, the primitive
    will have a negative value</mark>.</p>


    
    <h3>Assigning Floating-Point Numbers</h3>    
    
    
<p>First, you must know that
    every <mark>floating-point literal is implicitly a double</mark> (64 bits), not a float. So the
        literal 32.3, for example, is considered a double.</p>
<pre>   
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Floating1 {
    
    public static void main(String[] args) {
        float f = <mark>(float)</mark> 32.3;
        float g = 32.3<mark>f</mark>;
        float h = 32.3<mark>F</mark>;
    }
        
}
    
</pre>
<pre>
In order to assign a floating-point literal to a float
variable, you must <mark>either cast the value</mark> or <mark>append an f to the end</mark> of the literal
</pre>


<h3>Assigning a Literal That Is Too Large for the Variable
</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Byte1 {
    
    public static void main(String[] args) {
        byte a = 3; // No problem, 3 fits in a byte
        byte b = 8; // No problem, 8 fits in a byte
<mark>        a = a + 1; // 
        b += 1;</mark>
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types: possible lossy conversion from int to byte
	at com.mvivekweb.ocpjp6.obj1.sect3.literals.Byte1.main(Byte1.java:12)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>   
package com.mvivekweb.ocpjp6.obj1.sect3.literals;
    
public class Byte1 {
    
    public static void main(String[] args) {
        byte a = 3; // No problem, 3 fits in a byte
        byte b = 8; // No problem, 8 fits in a byte
        //a = a + 1; // 
        a += 7; 
            
        b += 1;
        System.out.println("a="+a);
        System.out.println("b="+b);
         a =(byte)(a+ 1);
         System.out.println("a="+a);
    }
        
}
    
</pre>
<pre class='out'>
run:
a=10
b=9
a=11
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>The compound assignment operator <mark>+= lets you add to the value of b, without
    putting in an explicit cast</mark>.
</p>

<p><mark>+=, -=, *=, and /=</mark> will all put in an <mark>implicit cast</mark>.
</p>

<h3>Variable Scope</h3>

<p><mark>Static</mark> variables have the <mark>longest scope</mark>.they are created when the class is
loaded, and they survive as long as the class stays loaded in the Java Virtual
Machine (JVM).</p>

<p><mark>Instance variables</mark> are the <mark>next most long-lived</mark>; they are created when a new
    instance is created, and they live until the instance is removed.</p>

<p><mark>Local variables are next;</mark> they live as long as their method remains on the
    stack.</p>

<p><mark>Block variables</mark> live only as long as the code block is executing.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.scope;
    
public class Layout {
    
    static int s = 343; // static variable
    int x; // instance variable
        
    {
        x = 7;
        int x2 = 5;
    } //<mark> initialization block</mark>
        
    Layout() {
        x += 8;
        int x3 = 6;
    } // <mark>constructor</mark>
        
    void doStuff() { // method
        int y = 0; // local variable
        for (int z = 0; z < 4; z++) { // <mark>'for' code block</mark>
            y += z + x;
        }
    }
}
    
</pre>

<p>z is a <mark>block variable</mark>.</p>

<p>x2 is an <mark>init block variable</mark>, a flavor of local variable.</p>

<p>x3 is a <mark>constructor variable</mark>, a flavor of local variable.
</p>


<p><mark>Scoping errors</mark> come in many sizes and shapes. <mark>One common mistake happens
        when a variable is shadowed</mark> and two scopes overlap.</p>

<p>The most common reason for scoping errors is when
    you attempt to access a variable that is not in scope.</p>

<p>Attempting to <mark>access an instance variable from a static context</mark></p>

<p>Attempting to access a <mark>local variable from a nested method</mark>.</p>

<p>Attempting to use a block variable after the code block has completed.</p>

<p>the compiler will say something like this:
    <mark>cannot find symbol</mark></p>

<p>Pay <mark>extra attention to code block scoping errors</mark>. You might see them in
    switches, try-catches, for, do, and while loops.</p>


<h3>Using a Variable or Array Element That Is Uninitialized
    and Unassigned</h3>

<p>When we attempt to use the uninitialized variable, we can get
    different behavior depending on what type of variable or array we are dealing
    with (primitives or objects).</p>

<p>Local variables are sometimes called <mark>stack, temporary, automatic, or method
        variables</mark>,</p>

        <p>Although you can leave a local variable uninitialized, <mark>the compiler
                complains if you try to use a local variable</mark> <mark>before initializing</mark> it with a value</p>

<h3>Primitive and Object Type Instance Variables</h3>


<p>Instance variables are initialized to a
    default value each time a new instance is created, although they may be given
    an explicit value after the object's super-constructors have completed.
</p>

<pre>
Variable Type             Default  Value
Object reference          null (not referencing any object)
byte, short, int, long    0
float, double             0.0
boolean                   false
char                      '\u0000'
    
</pre>

<h3>Array Instance Variables</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class BirthDays {
    
    static int[] year;// = new int[10];
        
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println("year[" + i + "] = " + year[i]);
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.NullPointerException
	at com.mvivekweb.ocpjp6.obj1.sect3.variables.BirthDays.main(BirthDays.java:9)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class BirthDays {
    
    static int[] year = new int[10];
        
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println("year[" + i + "] = " + year[i]);
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
year[0] = 0
year[1] = 0
year[2] = 0
year[3] = 0
year[4] = 0
year[5] = 0
year[6] = 0
year[7] = 0
year[8] = 0
year[9] = 0
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>An <mark>array is an object</mark>; thus, an array instance variable that's declared but not
    explicitly initialized will have a value of <mark>null</mark></p>


<p>All array elements are given their default values-the same
    <mark>default values that elements of that type</mark> get when they're instance variables</p>


<h3>Local (Stack, Automatic) Primitives and Objects</h3>


<p>'Automatic' is just another term for 'local variable.' It does not mean
    the automatic variable is automatically assigned a value! The opposite is true. An
    <mark>automatic variable must be assigned a value in the code</mark>, or <mark>the compiler will complain</mark>.</p>

<h3>Local Primitives</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class TimeTravel {
    
    public static void main(String[] args) {
        int year ;
        System.out.println("The year is " + year);
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - <mark>variable year might not have been initialized</mark>
	at com.mvivekweb.ocpjp6.obj1.sect3.variables.TimeTravel.main(TimeTravel.java:7)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<p>Java <mark>does not give local variables a default value</mark>; you must explicitly initialize them
    with a value,</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class TimeTravel1 {
    
    public static void main(String[] args) {
<mark>        int year; // Declared but not initialized
        int day; // Declared but not initialized</mark>
        System.out.println("You step into the portal.");
        year = 2050; // Initialize (assign an explicit value)
        System.out.println("Welcome to the year " + year);
    }
        
}
    
</pre>
<pre class='out'>
run:
You step into the portal.
Welcome to the year 2050
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>Legally, you can declare a local
    variable without initializing it <mark>as long as you don't use the variable</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class TestLocal {
    
    public static void main(String[] args) {
        int x;
        if (args[0] != null) { // assume you know this will
// always be true 
            x = 7; // compiler can't tell that this
// statement will run
        }
        int y = x;
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\variables>javac -source 1.6 TestLocal.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
TestLocal.java:12: error: variable x might not have been initialized
        int y = x;
                ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\variables>

</pre>

<h3>Local Object References</h3>

<p>Locally declared references <mark>can't get away with checking for null</mark> before
    use, unless <mark>you explicitly initialize the local variable to null</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
import java.util.Date;
    
public class TimeTravel2 {
    
    public static void main(String[] args) {
        Date date;
        if (date == null) {
            System.out.println("date is null");
        }
    }
        
}
    
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\variables>javac -source 1.6 TimeTravel2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
TimeTravel2.java:9: error: variable date might not have been initialized
        if (date == null) {
            ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\variables>

</pre>

<p>If you don't initialize a local reference variable,
    then by default, its value is... well that's the whole point-<mark>it doesn't have any value
        at all</mark></p>
        <pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
import java.util.Date;
    
public class TimeTravel2 {
    
    public static void main(String[] args) {
        Date date<mark>=null;</mark>
        if (date == null) {
            System.out.println("date is null");
        }
    }
        
}
    
        </pre>
<pre class='out'>
run:
date is null
BUILD SUCCESSFUL (total time: 0 seconds)        
</pre>      
        
<h3>Local Arrays</h3>
        
<p>You <mark>do not, however, need to explicitly initialize</mark> the elements of an array.</p>

<p>array elements are given
    their default values (0, false, null, '\u0000', etc.) regardless of whether the array
    is declared as an instance or local variable.</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class ArrayLocal {
    
    public static void main(String[] args) {
        int[] year;//=new int[0];
        System.out.println("year[" + 0 + "] = " + year[0]);
            
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - variable year might not have been initialized
	at com.mvivekweb.ocpjp6.obj1.sect3.arrays.ArrayLocal.main(ArrayLocal.java:8)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class ArrayLocal {
    
    public static void main(String[] args) {
        int[] year=new int[<mark>0</mark>];
        System.out.println("year[" + 0 + "] = " + year[0]);
            
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0
	at com.mvivekweb.ocpjp6.obj1.sect3.arrays.ArrayLocal.main(ArrayLocal.java:7)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class ArrayLocal {
    
    public static void main(String[] args) {
        int[] year=new int[1];
        System.out.println("year[" + 0 + "] = " + year[0]);
            
    }
        
}
    
</pre>
<pre class='out'>
run:
year[0] = 0
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Assigning One Reference Variable to Another</h3>

<p>If we assign an existing instance of an object to a new reference
    variable, then two reference variables will hold the same bit pattern-a bit pattern
    referring to a specific object on the heap</p>




<pre>
import java.awt.Dimension;
    
public class ReferenceTest {
    
    public static void main(String[] args) {
        Dimension a = new Dimension(5, 10);
        System.out.println("a.height = " + a.height);
        <mark>Dimension b = a;</mark>
        <mark>b.height = 30;</mark>
        System.out.println("a.height = " + <mark>a.height</mark>
                + " after change to b");
    }
        
}
    
</pre>
<pre class='out'>
run:
a.height = 10
a.height = 30 after change to b
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p><mark>One exception</mark> to the way object references are assigned is <mark>String</mark>.</p>

<p><mark>String objects are immutable</mark>; you
    can't change the value of a String object</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.variables;
    
public class StringTest {
    
    public static void main(String[] args) {
        String x = "Java"; // Assign a value to x
        <mark>String y = x; </mark>// Now y and x refer to the same
// String object
        System.out.println("y string = " + y);
        x = x + " Bean"; // Now modify the object using
// the x reference
      <mark>  System.out.println("y string = " + y);</mark>
        System.out.println("x string = " + x);
    }
        
}
    
</pre>
<pre class='out'>
run:
y string = Java
y string = Java
x string = Java Bean
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<h3>Array Declaration, Construction, and Initialization</h3>

<p>Arrays are objects in Java that store multiple variables of the same type.
</p>

<p>Arrays can
    hold either <mark>primitives</mark> or <mark>object references</mark>, but the array itself will always be an
    object on the heap, even if the array is declared to hold primitive elements.</p>

<p>Arrays are <mark>efficient</mark></p>

<p>but most of the time you'll want to use one of the
    Collection types from java.util (including <mark>HashMap, ArrayList, TreeSet</mark>).</p>


<p>Collection classes offer more flexible ways to access an object (for insertion,
    deletion, and so on) and unlike arrays, <mark>can expand or contract dynamically
        as you add or remove elements</mark> (<mark>they're really managed arrays</mark>, since <mark>they use
            arrays behind the scenes</mark>)</p>


<h3>Declaring an Array</h3>

<p>It is never legal to include the size of the array in your declaration.</p>


<h3>Constructing an Array</h3>

<p>To create an array object, Java must know how much space to allocate on the heap, so you must specify the size of the array at
    creation time</p>


<h3>Constructing One-Dimensional Arrays</h3>


<pre>Thread[] threads = new Thread[5];</pre>

<p>the Thread constructor is not
    being invoked. <mark>We're not creating a Thread instance</mark>, but rather a single Thread
    array object.</p>

<h3>Constructing Multidimensional Arrays</h3>

<p>So a twodimensional
    array of type int is really an object of type int array (int []), with
    each element in that array holding a reference to another int array.</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class TwoDimensional {
    
    public static void main(String[] args) {
        int[][] myArray = new int[3][];
//        myArray[0] = new int[2];
//        myArray[0][0] = 6;
//        myArray[0][1] = 7;
//        myArray[1] = new int[3];
//        myArray[1][0] = 9;
//        myArray[1][1] = 8;
//        myArray[1][2] = 5;
        System.out.println("myArray=" + Arrays.deepToString(myArray));
            
    }
        
}
    
</pre>
<pre class='out'>
run:
myArray=[null, null, null]
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class TwoDimensional {
    
    public static void main(String[] args) {
        int[][] myArray = new int[3][];
        <mark>myArray[0] = new int[2];</mark>
        myArray[0][0] = 6;
        myArray[0][1] = 7;
        myArray[1] = new int[3];
        myArray[1][0] = 9;
        myArray[1][1] = 8;
        myArray[1][2] = 5;
        System.out.println("myArray=" + Arrays.deepToString(myArray));
            
    }
        
}
</pre>
<pre class='out'>
run:
myArray=[[6, 7], [9, 8, 5], null]
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class TwoDimensional1 {
    
    public static void main(String[] args) {
        int[][] myArray = new int[][3];
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: <mark>Uncompilable source code - Erroneous tree type:</mark> <any>
	at com.mvivekweb.ocpjp6.obj1.sect3.arrays.TwoDimensional1.main(TwoDimensional1.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class TwoDimensional1 {
    
    public static void main(String[] args) {
        int[][] myArray = new int[][<mark>3</mark>];
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\arrays>javac -source 1.6 TwoDimensional1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
TwoDimensional1.java:6: error: ']' expected
        int[][] myArray = new int[][3];
                                    ^
TwoDimensional1.java:6: error: ';' expected
        int[][] myArray = new int[][3];
                                     ^
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect3\arrays>

</pre>

<p><mark>only the first brackets</mark> are given a <mark>size</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class ThreeDimensional {
    
    public static void main(String[] args) {
        int[][][] myArray = new int[3][3][3];
        System.out.println("myArray=" + Arrays.deepToString(myArray));
        int[] tomyArray[] = new int[3][];
//        tomyArray[0] = new int[2];
//        tomyArray[0][0] = 6;
//        tomyArray[0][1] = 7;
//        tomyArray[1] = new int[3];
//        tomyArray[1][0] = 9;
//        tomyArray[1][1] = 8;
//        tomyArray[1][2] = 5;
       <mark> myArray[0] = tomyArray;</mark>
        System.out.println("myArray=" + Arrays.deepToString(myArray));
            
    }
        
}
    
</pre>
<pre class='out'>
run:
myArray=[[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
myArray=[[null, null, null], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
BUILD SUCCESSFUL (total time: 0 seconds)


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class ThreeDimensional {
    
    public static void main(String[] args) {
        int[][][] myArray = new int[3][3][3];
        System.out.println("myArray=" + Arrays.deepToString(myArray));
        int[] tomyArray[] = new int[3][];
<mark>        tomyArray[0] = new int[2];
        tomyArray[0][0] = 6;</mark>
        tomyArray[0][1] = 7;
        tomyArray[1] = new int[3];
        tomyArray[1][0] = 9;
        tomyArray[1][1] = 8;
        tomyArray[1][2] = 5;
      <mark>  myArray[0] = tomyArray;</mark>
        System.out.println("myArray=" + Arrays.deepToString(myArray));
            
    }
        
}
    
</pre>
<pre class='out'>
run:
myArray=[[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
myArray=[[[6, 7], [9, 8, 5], null], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>


<h3>Initializing an Array</h3>

<h3>Initializing Elements in a Loop</h3>

<p>Array objects have a single public variable, <mark>length that gives you the number of
        elements</mark> in the array.</p>
<pre>
import java.util.Arrays;
    
public class TwoDimensional2 {
    
    public static void main(String[] args) {
        int[] myDogs = new int[6]; // creates an array of 6
// Dog references
        for (int x = 0; x < myDogs.length; x++) {
            //myDogs[x] = 1; // assign a new Dog to the
// index position x
            System.out.println("myArray=" + myDogs[x]);
        }
            
    }
        
}
    
</pre>
<pre class='out'>
run:
myArray=0
myArray=0
myArray=0
myArray=0
myArray=0
myArray=0
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class TwoDimensional2 {
    
    public static void main(String[] args) {
        int[][] myArray = new int[6][]; // creates an array of 6
// Dog references
        myArray[0] = new int[2];
        myArray[0][0] = 6;
        myArray[0][1] = 7;
        myArray[1] = new int[3];
        myArray[1][0] = 9;
        myArray[1][1] = 8;
        myArray[1][2] = 5;
        System.out.println("myDogs.length=" + myArray.length);
        System.out.println("myArray=" + Arrays.deepToString(myArray));
        for (int x = 0; x < myArray.length; x++) {
            //myDogs[x] = 1; // assign a new Dog to the
// index position x
            if (null != (myArray[x])) {
                for (int j = 0; j < myArray[x].length; j++) {
                    System.out.println("myArray["+x+"]["+j+"]=" + myArray[x][j]);
                }
            }
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
myDogs.length=6
myArray=[[6, 7], [9, 8, 5], null, null, null, null]
myArray[0][0]=6
myArray[0][1]=7
myArray[1][0]=9
myArray[1][1]=8
myArray[1][2]=5
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class ArrayShortcut {
    
    public static void main(String[] args) {
        int[][] scores = {{5, 2, 4, 7}, {9, 2}, {3, 4}};
        System.out.println("" + Arrays.deepToString(scores));
    }
        
}
    
</pre>
<pre class='out'>
run:
[[5, 2, 4, 7], [9, 2], [3, 4]]
BUILD SUCCESSFUL (total time: 1 second)

</pre>


<h3>Constructing and Initializing an Anonymous Array
</h3>

<p>The second shortcut is called "<mark>anonymous array creation</mark>" and can be used
    to construct and initialize an array, and then assign the array to a previously
    declared array reference variable</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
import java.util.Arrays;
    
public class AnonymousArray {
    
    public static void main(String[] args) {
        int[] testScores;
        testScores = <mark>new int[]{4, 7, 2};</mark>
        for (int x = 0; x < testScores.length; x++) {
            System.out.println("testScores[x]="+testScores[x]);
        }
    }
        
}
    
</pre>
<pre class='out'>
run:
testScores[x]=4
testScores[x]=7
testScores[x]=2
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>Remember that <mark>you do not specify a size when using anonymous array
        creation syntax</mark>. The <mark>size is derived from the number of items</mark> (comma-separated)
    between the curly braces.</p>

<h3>Legal Array Element Assignments</h3>


<h3>Arrays of Object References</h3>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.arrays;
    
public class TestSportyThings {
    
    public static void main(String[] args) {
        Sporty[] sportyThings = new Sporty[3];
        sportyThings[0] = new Ferrari(); // OK, Ferrari
// implements Sporty
        sportyThings[1] = new RacingFlats(); // OK, RacingFlats
// implements Sporty
        sportyThings[2] = new GolfClub();
    }
        
}
    
interface Sporty {
    
    void beSporty();
}
    
class Ferrari extends Car implements Sporty {
    
    public void beSporty() {
// implement cool sporty method in a Ferrari-specific way
    }
}
    
class RacingFlats implements Sporty {
    
    public void beSporty() {
// implement cool sporty method in a RacingShoe-specific way
    }
}
class Car {}
class Subaru extends Car {}
    
class GolfClub { }
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types: <mark>com.mvivekweb.ocpjp6.obj1.sect3.arrays.GolfClub cannot be converted to com.mvivekweb.ocpjp6.obj1.sect3.arrays.Sporty</mark>
	at com.mvivekweb.ocpjp6.obj1.sect3.arrays.TestSportyThings.main(TestSportyThings.java:11)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<p>any object that passes the <mark>"IS-A" test for the declared
        array type can be assigned</mark> to an element of that array.</p>

<h3>Array Reference Assignments for One-Dimensional Arrays</h3>

<p>The rules for array assignment apply to <mark>interfaces</mark> as well as <mark>classes</mark>. An array
    declared as an interface type can reference an array of any type that implements the
    interface.</p>

<h3>Array Reference Assignments for Multidimensional Arrays</h3>









