<h3>Develop code that implements tight encapsulation, loose coupling, and high cohesion
            in classes, and describe the benefits.   </h3>
        
        <h3>Encapsulation</h3>
        
        <p> Encapsulation helps hide implementation behind an interface (or API).</p>
        
        <p>This scenario highlights two of the promises/benefits of Object Orientation (OO):
            <mark>flexibility and maintainability</mark></p>
        
        <p> The <mark>ability to make changes in your implementation code</mark> <mark>without breaking the
            code of others</mark> who use your code is a key benefit of encapsulation.</p>
        
        <p>You want to
            hide implementation details behind a public programming interface.  </p>
        
        <p>By hiding implementation details, you can
            rework your method code (perhaps also altering the way variables are used by your
            class) <mark>without forcing a change in the code that calls your changed method</mark>.  </p>  
        
        <p>Keep <mark>instance variables protected</mark> (with an access modifier, often <mark>private</mark>).  </p>      
        
        <p>Make <mark>public accessor methods</mark>, and <mark>force calling code to use those methods</mark>
            rather than directly accessing the instance variable.</p>
        
        <p>use the JavaBeans naming convention of
            set&lt;someProperty&gt; and get&lt;someProperty&gt;.</p>
                
        <pre>
package com.mvivekweb.ocpjp6.obj5.sect1.encapsulation;
    
public class Box {
    <mark>private int size;</mark>
// Provide public getters and setters
    public int getSize() {
        return size;
    }
    <mark>public void</mark> setSize(int newSize) {
       <mark> size = newSize;</mark>
    }
}        
        </pre>   
 
        <p>good OO design dictates
            that you plan for the future </p> 
        

        <pre>       
package com.mvivekweb.ocpjp6.obj5.sect1.encapsulation;
    
public class ExploitBadOO {
    
    public static void main(String[] args) {
        BadOO b = new BadOO();
        <mark>b.size = -5; // Legal but bad!!</mark>
    }
}
    
class BadOO {
    
    public int size;
    public int weight;
}        
        </pre>   
        
        <pre>       
class Foo {
public int left = 9;
<mark>public int right = 3;</mark>
public void setLeft(int leftNum) {
left = leftNum;
right = leftNum/3;
}
// lots of complex test code here
}        
        </pre>     
        
        <p>They can simply go straight to the instance
            variables and <mark>change them to any arbitrary int</mark> value  </p>   
        
        
        <h3>Coupling and Cohesion </h3>
        
        <p>  coupling and cohesion, have to do with the <mark>quality</mark> of an OO
            design.</p>
        
        <p>good OO design calls for <mark>loose coupling</mark>   </p>
        
        <p>good OO design calls for <mark>high cohesion</mark>   </p>
        
        
        <h3> Coupling </h3>
        
        <p>Coupling is the degree
            to which <mark>one class knows about another class</mark>   </p>    
        
        <p>If the only knowledge that <mark>class A</mark>
            <mark>has</mark> about <mark>class B</mark>, is what <mark>class B has exposed through its interface</mark>, then class A and
            class B are said to be loosely coupled  </p>      
        
        <p> class A relies on <mark>parts of class B that are not part of class B's interface</mark>, then the
            coupling between the classes is tighter  </p>     
        
        
        <p> class A
            knows non-API stuff about class B, and class B knows non-API stuff about class A�?�
            this is REALLY BAD. If either class is ever changed, there's a chance that the other
            class will break.</p>
        
        <pre>        
package com.mvivekweb.ocpjp6.obj5.sect1.coupling;
    
public class DoTaxes {
    
    float rate;
        
    float doColorado() {
        SalesTaxRates str = new SalesTaxRates();
        <mark>rate = str.salesRate;</mark> // ouch
// this should be a method call:
// rate = str.getSalesRate("CO");
// do stuff with rate
        return 0;
    }
}
    
class SalesTaxRates {
    
    public float salesRate; // should be private
    public float adjustedSalesRate; // should be private
    public float getSalesRate(String region) {
        salesRate = new DoTaxes().<mark>doColorado();</mark> // ouch again!
// do region-based calculations
        return adjustedSalesRate;
    }
}
        </pre>
        
        <p> the exam will test you on really <mark>obvious examples of tight coupling</mark> </p>      
        
        <h3> Cohesion</h3>
        
        <p> cohesion is all
            about <mark>how a single class is designed</mark> </p>
        
        <p>indicate the degree
            to which a class has a <mark>single, well-focused purpose</mark>  </p>     

        <p>The more focused a class is, the higher its cohesiveness�??a good
            thing.   </p>
        
        <p> much <mark>easier
            to maintain</mark> (and less frequently changed)</p>
        
        
        <p>more
            reusable than other classes  </p>
        
        <p>reusable classes, it'll be much easier
            to write    </p>    
        