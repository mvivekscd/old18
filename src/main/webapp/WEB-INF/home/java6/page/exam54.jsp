<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
    
public class Bunnies {
    
static int count = 0;
 Bunnies() {
sout    
 while(count < 10) new Bunnies(++count);
 }
 Bunnies(int x) { super(); }
    public static void main(String[] args) {
        new Bunnies();
 new Bunnies(count);
 System.out.println(count);
 System.out.println(count++);
 System.out.println(count);
    }
        
}
</pre>
<pre class='out'>
run:
10
10
11
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
    
public class Bunnies {
    
static int count = 0;
 Bunnies() {
     System.out.println("cons");
 while(count < 10) new Bunnies(++count);
 }
 Bunnies(int x) { super(); System.out.println("cons x");}
    public static void main(String[] args) {
        new Bunnies();
 new Bunnies(count);
 System.out.println(count);
 System.out.println(count++);
 System.out.println(count);
    }
        
}
</pre>
<pre class='out'>
run:
cons
cons x
cons x
cons x
cons x
cons x
cons x
cons x
cons x
cons x
cons x
cons x
10
10
11
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
class Game {
 static String s = "-";
 String s2 = "s2";
 Game(String arg) { s += arg; }
 }
public class Go {
Go() { super(s2); }
 { s += "i "; }
     
    public static void main(String[] args) {
       new Go();
 System.out.println(s); 
    }
static { s += "sb "; }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect4/exam/Go.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect4\exam\Go.java:11: error: cannot find symbol
Go() { super(s2); }
             ^
  symbol:   variable s2
  location: class Go
com\mvivekweb\ocpjp6\obj5\sect4\exam\Go.java:12: error: cannot find symbol
 { s += "i "; }
   ^
  symbol:   variable s
  location: class Go
com\mvivekweb\ocpjp6\obj5\sect4\exam\Go.java:16: error: cannot find symbol
 System.out.println(s);
                    ^
  symbol:   variable s
  location: class Go
com\mvivekweb\ocpjp6\obj5\sect4\exam\Go.java:18: error: cannot find symbol
static { s += "sb "; }
         ^
  symbol:   variable s
  location: class Go
4 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>The s2 variable is an instance variable that can't be used in the call to super
    because the instance hasn't been created yet.</p>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
class Game {
 static String s = "-";
 String s2 = "s2";
 Game(String arg) { s += arg; }
 }
public class Go <mark>extends Game</mark>{
Go() { super(s2); }
 { s += "i "; }
     
    public static void main(String[] args) {
       new Go();
 System.out.println(s); 
    }
static { s += "sb "; }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect4/exam/Go.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect4\exam\Go.java:11: error: cannot reference s2 before supertype constructor has been called
Go() { super(s2); }
             ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
class Game {
 static String s = "-";
 String s2 = "s2";
 Game(String arg) { s += arg; System.out.println("game cons"); }
 }
public class Go extends Game{
Go() { super(s);<mark>System.out.println("cons");</mark> }
<mark> { s += "i "; System.out.println("init");System.out.println("s="+s);}</mark>
     
    public static void main(String[] args) {
         System.out.println("--1--"); 
       new Go();
 System.out.println(s); 
    }
static { s += "sb ";System.out.println("static"); }
}
</pre>
<pre class='out'>
run:
static
--1--
game cons
init
s=-sb -sb i 
cons
-sb -sb i 
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
class Big {
void doStuff(int x) { }
 }
 class Heavy extends Big {
 <mark>void doStuff(byte b) { }</mark>
// protected void doStuff(int x) throws Exception { }
 }
public class Weighty extends Heavy{
    
<mark> void doStuff(int x) { }</mark>
 // String doStuff(int x) { return "hi"; }
 // public int doStuff(int x) { return 7; }
<mark>  private int doStuff(char c) throws Error { return 1; }</mark>
      
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
class Dog {
 void makeNoise() { System.out.print("bark "); }
 static void play() { System.out.print("catching "); }
 }
     
public class Bloodhound extends Dog{
    
void makeNoise() { System.out.print("howl "); }
    public static void main(String[] args) {
      new Bloodhound().go();
 super.play();
 super.makeNoise();  
    }
void go() {
 super.play();
makeNoise();
 super.makeNoise();
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect4/exam/Bloodhound.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect4\exam\Bloodhound.java:14: error: non-static variable super cannot be referenced from a static context
<mark> super.play();</mark>
 ^
com\mvivekweb\ocpjp6\obj5\sect4\exam\Bloodhound.java:15: error: non-static variable super cannot be referenced from a static context
<mark> super.makeNoise();</mark>
 ^
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
class GardenTool {
 static String s = "";
 String name = "Tool ";
 GardenTool(String arg) { this(); s += name; System.out.println("--4--");}
 GardenTool() { s += "gt "; System.out.println("--5--");}
 }
     
public class Rake extends GardenTool{
{ name = "Rake "; System.out.println("--1--");}
 Rake(String arg) { s += name;System.out.println("--2--"); }
     
    public static void main(String[] args) {
        new GardenTool("hey ");
 new Rake("hi ");
 System.out.println(s);
    }
{ name = "myRake "; System.out.println("--3--");}
}
</pre>
<pre class='out'>
run:
--5--
--4--
--5--
--1--
--3--
--2--
gt Tool gt myRake 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
class GardenTool {
 static String s = "";
 String name = "Tool ";
 GardenTool(String arg) { this(); s += name; System.out.println("--4--");}
 GardenTool() { s += "gt "; System.out.println("--5--");}
 }
     
public class Rake extends GardenTool{
{ name = "Rake "; System.out.println("--1--");}
 Rake(String arg) { <mark>super();</mark>s += name;System.out.println("--2--"); }
     
    public static void main(String[] args) {
        new GardenTool("hey ");
 new Rake("hi ");
 System.out.println(s);
    }
{ name = "myRake "; System.out.println("--3--");}
}
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
class OOthing { void doStuff() { System.out.print("oo "); } }
 class GuiThing extends OOthing {
 void doStuff() { System.out.print("gui "); }
 }
public class Button extends GuiThing{
void doStuff() { System.out.print("button "); }
    
    public static void main(String[] args) {
      new Button().go();  
    }
void go() {
 GuiThing g = new GuiThing();
<mark>  this.doStuff();
  super.doStuff();</mark>
 // g.super.doStuff();
 // super.g.doStuff();
 // super.super.doStuff();
 }
}
</pre>
<pre class='out'>
run:
button gui BUILD SUCCESSFUL (total time: 0 seconds)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
    
abstract class Thing { static String s = ""; Thing() { s += "t "; } }
 class Steel extends Thing {
 Steel() { s += "s "; }
 Steel(String s1) {
 s += s1;
 new Steel();
 }
 }
public class Tungsten extends Steel{
    
Tungsten(String s1) {
 s += s1;
 new Steel(s);
 }
    public static void main(String[] args) {
        new Tungsten("tu ");
 System.out.println(s);
    }
        
}
</pre>
<pre class='out'>
run:
t s tu t t s tu t s 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect4.exam;
class Explode {
 static String s = "";
 static { s += "sb1 "; }
 Explode() { s += "e "; }
 }
     
public class C4 extends Explode{
C4() {
 s += "c4 ";
 new Explode();
 }
static {
 new C4();
 System.out.print(s);
 }
 { s += "i "; }
    public static void main(String[] args) {
        
    }
        
}
</pre>
<pre class='out'>
run:
sb1 e i c4 e BUILD SUCCESSFUL (total time: 0 seconds)
</pre>