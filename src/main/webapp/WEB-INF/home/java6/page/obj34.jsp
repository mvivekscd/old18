<h3>Use standard J2SE APIs in the java.text package to correctly format or parse dates,
            numbers and currency values for a specific locale; and, given a scenario, determine the
            appropriate methods to use if you want to use the default locale or a specific locale. Describe
            the purpose and use of the java.util.Locale class.</h3>

        <p><mark>creating new Date</mark> and <mark>DateFormat objects</mark>, <mark>converting Strings to Dates</mark> and back
            again, performing <mark>Calendaring functions</mark>, printing properly <mark>formatted currency</mark>
            values, and doing all of this for <mark>locations</mark> around the globe</p>

        <h3>Working with Dates, Numbers, and Currencies</h3>

        <p>four classes from the <mark>java.text</mark> and <mark>java.util</mark>
            packages.</p>

        <br>java.util.Date - represents a <mark>mutable date and time</mark>, to a millisecond
        <br>java.util.Calendar - variety of methods that
        help you <mark>convert and manipulate dates</mark> and times
        <br>java.text.DateFormat- <mark>format dates </mark>for numerous locales around the world
        <br>java.text.NumberFormat - <mark>format numbers and
            currencies</mark> for locales
        <br>java.util.Locale - <mark>used in conjunction with DateFormat</mark>
        and <mark>NumberFormat</mark> to format dates, numbers and currency for specific locales

        <table class="pgt">
            <caption>Table 1</caption>
            <thead>
                <tr>
                    <th>Use Case</th>
                    <th>Steps</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-label="Use Case">Get the <mark>current date</mark> and
                        time.</td>
                    <td data-label="Steps"><div>1. Create a Date: Date d = <mark>new Date()</mark>;</div>
                        <div>2. Get its value: String s = d.toString();</div></td>
                </tr>
                <tr>
                    <td data-label="Use Case">Get an object that lets
                        you perform <mark>date and time
                            calculations</mark> in your locale.</td>
                    <td data-label="Steps"><div>1. Create a Calendar:
                            Calendar c = <mark>Calendar.getInstance();</mark></div>
                        <div>2. Use c.<mark>add</mark>(...) and c.<mark>roll</mark>(...) to <mark>perform date and time
                                manipulations</mark>.</div></td>
                </tr>
                <tr>
                    <td data-label="Use Case">Get <mark>an object</mark> that lets
                        you perform date and time
                        calculations in a <mark>different
                            locale</mark>.</td>
                    <td data-label="Steps"><div>1. <mark>Create a Locale</mark>:
                            Locale loc = <mark>new Locale</mark>(language); or
                            Locale loc = new Locale(<mark>language, country</mark>);</div>
                        <div>2. Create a Calendar for that locale:
                            Calendar c = Calendar.getInstance(<mark>loc</mark>);</div>
                        <div>
                            3. Use c.add(...) and c.roll(...) to perform date and time
                            manipulations.

                        </div>

                    </td>

                </tr>
                <tr>
                    <td data-label="Use Case">Get an object that lets
                        you perform date and time
                        calculations, and <mark>then format
                            it for output in different
                            locales</mark> with different date
                        styles.</td>
                    <td data-label="Steps"><div>1. Create a Calendar:
                            Calendar c = Calendar.getInstance();</div>
                        <div>2. Create a Locale for each location:
                            Locale loc = new Locale(...);</div>
                        <div>3. <mark>Convert your Calendar to a Date</mark>:
                            <mark>Date d = c.getTime()</mark>;</div>
                        <div>4.<mark> Create a DateFormat</mark> for each <mark>Locale</mark>:
                            DateFormat df = <mark>DateFormat.getDateInstance
                                (style, loc);</mark></div>
                        <div>5. Use the <mark>format()</mark> method to create formatted dates:
                            String s = <mark>df.format(d);</mark></div>



                    </td>
                </tr>
                <tr>
                    <td data-label="Use Case">Get an object that lets you
                        <mark>format numbers or currencies</mark>
                        across many different locales.</td>
                    <td data-label="Steps"><div>1. Create a Locale for each location:
                            Locale loc = <mark>new Locale(...);</mark></div>
                        <div>2. Create a NumberFormat:
                            NumberFormat nf = <mark>NumberFormat.getInstance(loc);</mark>
                            -or- NumberFormat nf = <mark>NumberFormat.
                                getCurrencyInstance(loc);</mark></div>
                        <div>3. Use the <mark>format()</mark> method to create formatted output:
                            String s = nf.format(someNumber);</div>

                    </td>
                </tr>

            </tbody>
        </table>

        <h3>The Date Class    </h3>


        <p>for most purposes you'll want to <mark>use the
                Calendar class</mark> instead of the Date class </p>   

        <p> Internally, the date and time is stored as a <mark>primitive long</mark></p>


        <pre>
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.<mark>util.Date</mark>;
    
    
public class TestDates {
    
    
    public static void main(String[] args) {
        
    Date d1 = <mark>new Date(1000000000000L);</mark> // a trillion!
    System.out.println("1st date " + <mark>d1.toString()</mark>);
    }
}
        </pre>
        <pre class='out'>
run:
1st date Sat Sep 08 20:46:40 CDT 2001
BUILD SUCCESSFUL (total time: 1 second)
        </pre> 

        <p>Although most of Date's methods have been <mark>deprecated</mark>, it's still acceptable to
            use the <mark>getTime and setTime</mark> methods</p>

        <pre>
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.util.Date;
    
    
public class TestDates1 {
    
    
    public static void main(String[] args) {
        Date d1 = new Date(1000000000000L); // a trillion!
        System.out.println("1st date " + d1.toString());
        d1.<mark>setTime</mark>(d1<mark>.getTime()</mark> + 3600000); // 3600000 millis / hour
        System.out.println("new time " + d1.toString());
    }
        
}
        </pre>
        <pre class='out'>   
    run:
1st date Sat Sep 08 20:46:40 CDT 2001
new time Sat Sep 08 21:46:40 CDT 2001
BUILD SUCCESSFUL (total time: 1 second)
        </pre>

        <p>create an instance of Date to represent "now," you use
            Date's no-argument constructor:    </p>

        <pre>Date now = new Date();   </pre> 

        <h3>The Calendar Class  </h3>  

        <p>The Calendar
            class is designed to make date manipulation easy    </p>

        <p>it's an
            <mark>abstract class</mark></p>


        <p>create a Calendar instance, you have to use <mark>one of the overloaded
                getInstance() static factory</mark> methods:</p>

        <pre>Calendar cal = <mark>Calendar.getInstance()</mark>;</pre>

        <p>your Calendar reference
            variable is actually referring to an instance of a <mark>concrete subclass of Calendar</mark>. You
            can't know for sure what subclass you'll get (<mark>java.util.GregorianCalendar
                is what you'll almost certainly get</mark>)</p>


        <pre>
    
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.util.Calendar;
import java.util.Date;
    
    
public class Dates2 {
    
    
    public static void main(String[] args) {
        
        Date d1 = new Date(1000000000000L);
        System.out.println("1st date " + d1.toString());
        Calendar c = Calendar.getInstance();
        <mark>c.setTime(d1);</mark> // #1
        if (Calendar.SUNDAY == <mark>c.getFirstDayOfWeek()</mark>) // #2
        {
            System.out.println("Sunday is the first day of the week");
        }
        System.out.println("trillionth milli day of week is "
                + <mark>c.get(Calendar.DAY_OF_WEEK)</mark>); // #3
        <mark>c.add(Calendar.MONTH, 1); </mark>// #4
        <mark>Date d2 = c.getTime();</mark> // #5
        System.out.println("new date " + d2.toString() );
            
    }
        
}
    
        </pre>
        <pre class='out'>        
run:
1st date Sat Sep 08 20:46:40 CDT 2001
Sunday is the first day of the week
trillionth milli day of week is 7
new date Mon Oct 08 20:46:40 CDT 2001
BUILD SUCCESSFUL (total time: 1 second) 
        </pre>  


        <p>We use <mark>Calendar's SUNDAY</mark> field to determine whether, for our JVM, SUNDAY
            is considered to be the first day of the week. (<mark>In some locales, MONDAY is the
                first day of the week. </mark>    </p>  


        <p>This very
            powerful method lets you <mark>add or subtract units of time</mark> appropriate for
            whichever Calendar field you specify  </p>     

        <pre>c.add(Calendar.HOUR, <mark>-4</mark>); // subtract 4 hours from c's value
c.add(Calendar.YEAR, 2); // add 2 years to c's value
c.add(Calendar.DAY_OF_WEEK, <mark>-2</mark>); // subtract two days from</pre>


        <p>The <mark>roll()</mark> method acts like the<mark> add() </mark>method, except that when a part
            of a Date gets incremented or decremented, <mark>larger parts of the Date will not get
                incremented or decremented.</mark></p>  



        <pre>
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.util.Calendar;
import java.util.Date;
    
    
public class Rolldates {
    
    
    public static void main(String[] args) {
        Date d1 = new Date(1000000000000L);
        System.out.println("1st date " + d1.toString());
        Calendar c = Calendar.getInstance();
        c.setTime(d1); // #1
        if (Calendar.SUNDAY == c.getFirstDayOfWeek()) // #2
        {
            System.out.println("Sunday is the first day of the week");
        }
        System.out.println("trillionth milli day of week is "
                + c.get(Calendar.DAY_OF_WEEK)); // #3
        <mark>c.roll(Calendar.MONTH, 9); // #4</mark>
        Date d2 = c.getTime(); // #5
        System.out.println("new date " + d2.toString() );
    }
        
}
        </pre>
        <pre class='out'>           
run:
1st date Sat <mark>Sep 08</mark> 20:46:40 CDT 2001
Sunday is the first day of the week
trillionth milli day of week is 7
new date Fri <mark>Jun 08</mark> 20:46:40 CDT 2001
BUILD SUCCESSFUL (total time: 1 second)            
        </pre>

        <h3>The DateFormat Class  </h3>          
        <pre>            
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.text.DateFormat;
import java.util.Date;
    
    
public class Dates3 {
    
    
    public static void main(String[] args) {
        Date d1 = new Date(1000000000000L);
        DateFormat[] dfa = new DateFormat[6]; //array
        dfa[0] = DateFormat.<mark>getInstance();</mark>
        dfa[1] = DateFormat.<mark>getDateInstance();</mark>
        dfa[2] = DateFormat.getDateInstance(DateFormat.SHORT);
        dfa[3] = DateFormat.getDateInstance(DateFormat.MEDIUM);
        dfa[4] = DateFormat.getDateInstance(DateFormat.LONG);
        dfa[5] = DateFormat.getDateInstance(<mark>DateFormat.FULL</mark>);
        for(DateFormat df : dfa)
            System.out.println(<mark>df.format(d1)</mark>);
    }
        
}
        </pre>
        <pre class='out'>            
run:
9/8/01 8:46 PM
Sep 8, 2001
9/8/01
Sep 8, 2001
September 8, 2001
Saturday, September 8, 2001
BUILD SUCCESSFUL (total time: 1 second)            
            
        </pre>     


        <p>DateFormat is another <mark>abstract class</mark>   </p>    

        <p>we used two factory methods, <mark>getInstance()</mark> and
            <mark>getDateInstance()</mark></p>

        <p>The
            <mark>parse()</mark> method takes a String formatted in the style of the <mark>DateFormat</mark> instance
            being used, and converts the<mark> String into a Date object</mark></p>         

        <p>this is
            a risky operation because the parse() method could easily<mark> receive a badly formatted
                String</mark>. Because of this, parse() can throw a <mark>ParseException</mark>.  </p>   

        <pre>            
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
    
public class parseToDate {
    
    public static void main(String[] args) {
        <mark>Date</mark> d1 = new Date(1000000000000L);
        System.out.println("d1 = " + d1.toString());
        DateFormat df = DateFormat.getDateInstance(
                <mark>DateFormat.SHORT</mark>);
        String s = <mark>df.format(d1);</mark>
        System.out.println(s);
        try {
            <mark>Date</mark> d2 = <mark>df.parse(s);</mark>
            System.out.println("parsed = " + d2.toString());
        } catch (ParseException pe) {
            System.out.println("parse exc");
        }
    }
}            
        </pre>

        <pre class='out'>            
run:
d1 = Sat Sep 08 20:46:40 CDT 2001
9/8/01
parsed = Sat Sep 08 00:00:00 CDT 2001
BUILD SUCCESSFUL (total time: 1 second)            
        </pre>        


        <p>If we'd wanted to retain the time along with the date we could have used
            the <mark>getDateTimeInstance()</mark> method, but it's not on the exam.        </p>    


        <p><mark>parse()</mark> isn't
            very lenient about the formatting of Strings it will successfully parse into
            dates    </p>        

        <h3>The Locale Class </h3>        

        <p>Both the <mark>DateFormat </mark>class and the <mark>NumberFormat</mark>
            class (which we'll cover next) <mark>can use an instance of Locale</mark> to customize formatted
            output to be specific to a locale</p>

        <p>locale is <mark>"a specific geographical, political, or cultural region."</mark>    </p>       

        <p>two Locale
            constructors   </p>

        <pre> Locale(String language)
Locale(String language, String country)    </pre>

        <p>There are over
            <mark>500 ISO Language</mark> codes    </p>   

        <pre>           
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
    
public class LocaleTest {
    
    public static void main(String[] args) {
        
        
        Calendar c = Calendar.getInstance();
        c.set(2010, 11, 14); // December 14, 2010
// (month is 0-based
        Date d2 = c.getTime();
        Locale locIT = new Locale("it", "IT"); // Italy
        Locale locPT = new Locale("pt"); // Portugal
        Locale locBR = new Locale("pt", "BR"); // Brazil
        Locale <mark>locIN = new Locale("hi", "IN"); // India</mark>
        Locale locJA = new Locale("ja"); // Japan
        DateFormat dfUS = DateFormat.getInstance();
        System.out.println("US " + dfUS.format(d2));
        DateFormat dfUSfull = DateFormat.getDateInstance(
                DateFormat.FULL);
        System.out.println("US full " + dfUSfull.format(d2));
        DateFormat dfIT = DateFormat.getDateInstance(
                DateFormat.FULL, locIT);
        System.out.println("Italy " + dfIT.format(d2));
        DateFormat dfPT = DateFormat.getDateInstance(
                DateFormat.FULL, locPT);
        System.out.println("Portugal " + dfPT.format(d2));
        DateFormat dfBR = DateFormat.getDateInstance(
                DateFormat.FULL, locBR);
        System.out.println("Brazil " + dfBR.format(d2));
        DateFormat dfIN = <mark>DateFormat.getDateInstance(
                DateFormat.FULL, locIN);</mark>
        System.out.println("India " + dfIN.format(d2));
        DateFormat dfJA = DateFormat.getDateInstance(
                DateFormat.FULL, locJA);
        System.out.println("Japan " + dfJA.format(d2));
    }
}
    
        </pre>
        <pre class='out'>            
run:
US 12/14/10 4:19 PM
US full Tuesday, December 14, 2010
Italy marted14 dicembre 2010
Portugal Ter-feira, 14 de Dezembro de 2010
Brazil Ter-feira, 14 de Dezembro de 2010
India ???????, ?? ??????, ????
Japan 2010?12?14? (???)
BUILD SUCCESSFUL (total time: 1 second)            
        </pre>        


        <p>Remember that both <mark>DateFormat and NumberFormat</mark> objects can have
            their locales set only at the time of instantiation. Watch for code that <mark>attempts to
                change the locale of an existing instance</mark> -no such methods exist!  </p>         

        <p><mark>getDisplayCountry()</mark> and
            <mark>getDisplayLanguage())</mark>  methods let
            you create Strings that represent a given <mark>locale's country and language</mark> in terms of
            both the <mark>default locale and any other locale</mark>:  </p>   

        <pre>            
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
    
public class DisplayCountryLanguage {
    
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.set(2010, 11, 14);
        Date d2 = c.getTime();
        Locale locBR = new Locale("pt", "BR"); // Brazil
        Locale locDK = new Locale("da", "DK"); // Denmark
        Locale locIT = new Locale("it", "IT"); // Italy
        System.out.println("def " + locBR.getDisplayCountry());
        System.out.println("loc " + locBR.getDisplayCountry(locBR));
        System.out.println("def " + locDK.getDisplayLanguage());
        System.out.println("loc " + locDK.getDisplayLanguage(locDK));
        System.out.println("D>I " + locDK.getDisplayLanguage(locIT));
            
            
            
    }
}            
        </pre>  
        <pre class='out'>            
run:
def Brazil
loc Brasil
def Danish
loc dansk
D>I Danese
BUILD SUCCESSFUL (total time: 1 second)            
        </pre>   

        <p>the default for the
            country Brazil is "Brazil", and the default for the Danish language is "Danish".
            In Brazil, the country is called "Brasil", and in Denmark the language is called
            "dansk". Finally, just for fun, we discovered that in Italy, the Danish language is
            called "danese".   </p>         



        <h3>The NumberFormat Class    </h3>

        <p>NumberFormat is <mark>abstract </mark></p>         

        <p><mark>getInstance()</mark> or <mark>getCurrencyInstance()</mark> to create a NumberFormat
            object</p>


        <pre>
package com.mvivekweb.ocpjp6.obj3.sec4.locale;
    
import java.text.NumberFormat;
import java.util.Locale;
    
    
public class NumberFormatTest {
    
    
    public static void main(String[] args) {
        float f1 = 123.4567f;
        Locale locFR = new Locale("fr"); // France
        NumberFormat[] nfa = new NumberFormat[4];
        nfa[0] = NumberFormat.<mark>getInstance();</mark>
        nfa[1] = NumberFormat.getInstance(locFR);
        nfa[2] = NumberFormat.<mark>getCurrencyInstance();</mark>
        nfa[3] = NumberFormat.<mark>getCurrencyInstance(locFR);</mark>
        for(NumberFormat nf : nfa)
            System.out.println(nf.format(f1));
    }
        
}
        </pre>
        <pre class='out'>            
run:
123.457
123,457
$123.46
123,46 ?
BUILD SUCCESSFUL (total time: 1 second)
        </pre>      


        <p><mark>getMaximumFractionDigits(),</mark>
            <mark>setMaximumFractionDigits()</mark>, parse(), and <mark>setParseIntegerOnly():</mark>  </p>          
        <pre>
import java.text.NumberFormat;
import java.text.ParseException;
    
    
public class FractionTest {
    
    
    public static void main(String[] args) {
       float f1 = 123.45678f;
        NumberFormat nf = NumberFormat.getInstance();
        System.out.print(nf.<mark>getMaximumFractionDigits()</mark> + " ");
        System.out.print(<mark>nf.format(f1)</mark> + " ");
        <mark>nf.setMaximumFractionDigits(5);</mark>
        System.out.println(nf.format(f1) + " ");
        try {
        System.out.println(nf.parse("1234.567"));
        <mark>nf.setParseIntegerOnly(true);</mark>
        System.out.println(nf.parse("1234.567"));
        } catch (ParseException pe) {
        System.out.println("parse exc");
        } 
            
            
    }
        
}            
        </pre>
        <pre class='out'>           
run:
3 123.457 123.45678 
1234.567
1234
BUILD SUCCESSFUL (total time: 1 second)            
        </pre>       

        <p><mark>setParseIntegerOnly()</mark> method takes a <mark>boolean</mark>  </p>          


        <table class="pgt">
            <caption>Table 1</caption>
            <thead>
                <tr>
                    <th>Class</th>
                    <th>Key Instance Creation Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-label="Class">util.Date</td>    
                    <td data-label="Options">
                        <div>new Date();</div>
                        <div>new Date(long millisecondsSince010170);</div>

                    </td>  
                </tr>
                <tr>
                    <td data-label="Class">util.Calendar</td>    
                    <td data-label="Options">
                        <div><mark>Calendar.getInstance();</mark></div>
                        <div>Calendar.getInstance(Locale);</div>
                    </td>  
                </tr>
                <tr>
                    <td data-label="Class">util.Locale</td>    
                    <td data-label="Options">
                        <div><mark>Locale.getDefault();</mark></div>
                        <div>new Locale(String language);</div>
                        <div>new Locale(String language, String country);</div>
                    </td>  
                </tr>
                <tr>
                    <td data-label="Class">text.DateFormat</td>    
                    <td data-label="Options">
                        <div><mark>DateFormat.getInstance();</mark></div>
                        <div><mark>DateFormat.getDateInstance();</mark></div>
                        <div>DateFormat.getDateInstance(style);</div>
                        <div>DateFormat.getDateInstance(style, Locale);</div>
                    </td>  
                </tr> 
                <tr>
                    <td data-label="Class">text.NumberFormat</td>    
                    <td data-label="Options">
                        <div><mark>NumberFormat.getInstance()</mark></div>
                        <div>NumberFormat.getInstance(Locale)</div>
                        <div><mark>NumberFormat.getNumberInstance()</mark></div>
                        <div>NumberFormat.getNumberInstance(Locale)</div>
                        <div><mark>NumberFormat.getCurrencyInstance()</mark></div>
                        <div>NumberFormat.getCurrencyInstance(Locale)</div>
                    </td>  
                </tr>                 
                
            </tbody>
        </table>
