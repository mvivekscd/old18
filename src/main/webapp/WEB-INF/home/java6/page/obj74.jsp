<h3>Given a code example, recognize the point at which an object becomes eligible for
            garbage collection, and determine what is and is not guaranteed by the garbage collection
            system, and recognize the behaviors of the Object finalize() method.</h3>
        
        
<h3>Overview of Memory Management and Garbage Collection </h3>
        
<p>Java's garbage collector provides an <mark>automatic solution to memory management</mark>. </p>

<p>The downside to automatic garbage collection is that <mark>you can't
    completely control</mark> when it runs and when it doesn't.</p>


<h3>Overview of Java's Garbage Collector</h3>

<p>but it's typical for memory to be
    used to <mark>create a stack</mark>, a <mark>heap</mark>, in Java's case <mark>constant pools</mark>, and <mark>method areas</mark>.</p>

<p>
    The
    heap is that part of <mark>memory where Java objects live</mark>, and it's the one and only part of
    memory that is in any way involved in the garbage collection process.
</p>

<p>there is
    <mark>one and only one heap</mark>.</p>

<p>When
    the garbage collector runs, its <mark>purpose is to find and delete objects that cannot be
    reached</mark>.</p>



<h3>When Does the Garbage Collector Run?</h3>
    
<p>The <mark>JVM</mark> <mark>decides</mark> when to
    run the garbage collector.   </p> 
    
<p>From within your Java program you can ask the JVM to
    run the garbage collector, <mark>but there are no guarantees, under any circumstances, that
    the JVM will comply</mark>.</p>

<p>
        the <mark>JVM will typically run the garbage
        collector</mark> when it senses that memory is running low.</p>
    
<h3>How Does the Garbage Collector Work?  </h3>

<p>You just can't be sure.</p>

<p>an <mark>object is eligible
    for garbage collection</mark> when <mark>no live thread can access</mark> it</p>

<p>the exam focuses its garbage collection questions <mark>on non-
    String objects</mark></p>

<h3>Writing Code That Explicitly Makes Objects Eligible for Collection</h3>

<h3>Nulling a Reference</h3>
<p>
    an object becomes <mark>eligible for garbage collection when
    there are no more reachable references</mark> to it.</p>

<p>The first way to remove a reference to an object is to <mark>set the reference variable
    that refers to the object to null</mark></p>

    <pre>
package com.mvivekweb.ocpjp6.obj7.sect4.garbageCollection;
    
public class GarbageTruck {
    
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("hello");
        System.out.println(sb);
        // The StringBuffer object is not eligible for collection
       <mark> sb = null;</mark>
        // Now the StringBuffer object is eligible for collection
    }
}
    
    </pre>
 <pre class='out'>   
 run:
hello
BUILD SUCCESSFUL (total time: 1 second)   
    
    </pre>


<h3>Reassigning a Reference Variable</h3>

<p>We can also decouple a reference variable from an object by setting the reference
    variable to refer to another object</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.garbageCollection;
    
public class GarbageTruck1 {
    
    public static void main(String[] args) {
        StringBuffer s1 = new StringBuffer("hello");
        StringBuffer s2 = new StringBuffer("goodbye");
        System.out.println(s1);
// At this point the StringBuffer "hello" is not eligible
       <mark> s1 = s2; // Redirects s1 to refer to the "goodbye" object</mark>
// Now the <mark>StringBuffer "hello" is eligible for collection</mark>
    }
}
    
</pre>

<pre class='out'>
run:
hello
BUILD SUCCESSFUL (total time: 1 second)

</pre>


<p>When a method
    is invoked, <mark>any local variables created exist only for the duration of the method</mark>.
</p>
<p>
    <mark>Once the method has returned</mark>, the objects created in the method are eligible for
    garbage collection.    
</p>

<p>
    If an object is returned
    from the method, <mark>its reference might be assigned to a reference variable in the
    method that called it; hence, it will not be eligible for collection.</mark>
        
        
</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.garbageCollection;
    
import java.util.Date;
    
public class GarbageFactory {
    
    public static void main(String[] args) {
        Date d = getDate();
        //doComplicatedStuff();
        System.out.println("d = " + d);
    }
        
    public static <mark>Date</mark> getDate() {
        Date d2 = new Date();
        <mark>StringBuffer now = new StringBuffer(d2.toString());
        System.out.println(now);</mark>
        return d2;
    }
}
    
</pre>

<pre class='out'>
run:
Fri Jun 03 16:28:09 CDT 2016
d = Fri Jun 03 16:28:09 CDT 2016
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>
    Since the method returns the <mark>Date object, it will not be
    eligible for collection</mark> even after the method has completed. <mark>The StringBuffer object,
    though, will be eligible, even though we didn't explicitly set the now variable to null</mark>.
</p>

<h3>Isolating a Reference</h3>

<p>There is another way in which objects can become eligible for garbage collection,
    <mark>even if they still have valid references</mark>! We call this scenario "islands of isolation."
</p>

<p>A simple example is a class that has an instance variable that is a reference
    variable to another instance of the same class. Now imagine that two such instances
    exist and that they refer to each other. <mark>If all other references to these two objects
    are removed</mark>, then even though each object still has a valid reference, there will be
    no way for any live thread to access either object.</p>
<p>
    When the garbage collector runs,
    it can <mark>usually discover any such islands of objects</mark> and <mark>remove</mark> them.
</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.garbageCollection;
    
public class Island {
    
    Island i;
        
    public static void main(String[] args) {
        Island i2 = new Island();
        Island i3 = new Island();
        Island i4 = new Island();
       <mark> i2.i = i3; // i2 refers to i3
        i3.i = i4; // i3 refers to i4
        i4.i = i2; // i4 refers to i2</mark>
        <mark>i2 = null;
        i3 = null;
        i4 = null;</mark>
    }
}
    
</pre>

<h3>Forcing Garbage Collection</h3>

<p>garbage collection <mark>cannot be forced</mark></p>

<p>the topic of using System.gc() has been <mark>removed
    from the exam.</mark>
</p>

<p> The garbage collection routines that Java provides are members of the <mark>Runtime
    class</mark>. The Runtime class is a special class that has a single object (a Singleton) for
    each main program.</p>

<p>The <mark>Runtime object provides a mechanism for communicating</mark>
    directly <mark>with the virtual machine</mark></p>

<p>To get the Runtime instance, you can use the
    method <mark>Runtime.getRuntime()</mark>, which <mark>returns the Singleton</mark>. Once you have
    the Singleton you can <mark>invoke</mark> the garbage collector <mark>using</mark> the <mark>gc() method</mark>.</p>

<pre>
Runtime.getRuntime().gc()
</pre>


<p>The simplest way
    to ask for garbage collection (remember-just a request) is</p>


<pre>System.gc();</pre>


<p>You just <mark>can't rely on System.gc()</mark> to free up enough memory so that
    you don't have to worry about running out of memory.</p>

<p>The following
    program lets us know how <mark>much total memory the JVM has available to it</mark> and how
    <mark>much free memory</mark> it has.</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.garbageCollection;
    
import java.util.Date;
    
public class CheckGC {
    
    public static void main(String[] args) {
        Runtime rt = <mark>Runtime.getRuntime();</mark>
        System.out.println("Total JVM memory: "
                + <mark>rt.totalMemory()</mark>);
                    
        System.out.println("Before Memory = "
                + <mark>rt.freeMemory()</mark>);
        Date d = null;
        for (int i = 0; i < 10000; i++) {
            d = new Date();
            d = null;
        }
        System.out.println("After Memory = "
                + <mark>rt.freeMemory()</mark>);
        <mark>rt.gc();</mark> // an alternate to System.gc()
        System.out.println("After GC Memory = "
                + <mark>rt.freeMemory()</mark>);
    }
}
    
</pre>
<pre class='out'>
run:
Total JVM memory: 4194304
Before Memory = 3034728
After Memory = 2630744
After GC Memory = 3618176
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>This program has <mark>only one user thread running</mark>, so there was nothing else
    going on when we called rt.gc().</p>


<p>Keep in mind that the <mark>behavior when gc() is
    called may be different</mark> for different JVMs, so there is <mark>no guarantee that the unused
    objects will be removed</mark> from memory.</p>

A<p>bout the<mark> only thing you can guarantee</mark> is
that <mark>if you are running very low on memory, the garbage collector will run</mark> <mark>before</mark> it
throws an <mark>OutOfMemoryException</mark>.</p>

<p>
    putting rt.gc() inside a loop.
    You might see that not all memory is released on any given run of the GC.
</p>


<h3>Cleaning Up Before Garbage Collection the finalize() Method</h3>

<p>Java provides you a mechanism to run some code just before your object is deleted
    by the garbage collector. This code is located in a method named finalize() that
    all classes inherit from class Object.</p>

<p>
    So, any code that you
    put into your class's <mark>overridden finalize()</mark> method is <mark>not guaranteed to run</mark>.
</p>
<p>
    we recommend
    that in general you <mark>don't override finalize()</mark> at all.
</p>

<h3>Tricky Little finalize() Gotcha's</h3>


<p>For <mark>any given object</mark>,<mark> finalize() will be called only once</mark> (at most) by the
    garbage collector.</p>


<p>Calling <mark>finalize() can actually result in saving an object</mark> <mark>from deletion</mark>.</p>


<p>code that you can put into a normal method you can put into finalize().</p>

<p><mark>Class Object</mark> has a finalize() method.</p>
