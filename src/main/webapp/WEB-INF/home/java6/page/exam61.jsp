<p>A given <mark>TreeSet's ordering cannot be changed</mark> once it's created.</p>

<p>It's a reasonable design choice to use a <mark>LinkedList when you want to design queue</mark>-like
    functionality.</p>

<p>the Properties class is very Map-like.</p>

<p><mark>Queues are most like Lists</mark>.</p>

<p>PriorityQueue class itself provides
    no non-destructive traversal methods.</p>

<p>Lists are much better than
    Sets at index-based retrieval.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.HashMap;
import java.util.Map;
    
public class Garage {
    
    public static void main(String[] args) {
        Map&lt;String, String&gt; hm = new <mark>HashMap</mark>&lt;String, String&gt;();
        String[] k = {<mark>null</mark>, "2", "3", <mark>null</mark>, "5"};
       String[] v = {"a", "b", "c", "d", "e"};
           
        for (int i = 0; i < 5; i++) {
            hm.put(k[i], v[i]);
             System.out.print(<mark>hm.get(k[i])</mark> + " ");
             }
        System.out.print(hm.size() + " " + <mark>hm.values()</mark> + "\n");
    }
        
}</pre>
<pre class='out'>
run:
a b c d e 4 [d, b, c, e]
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.Hashtable;
import java.util.Map;
    
    
public class exhashtable {
    
    
    public static void main(String[] args) {
        Map&lt;String, String&gt; h = new <mark>Hashtable</mark>&lt;String, String&gt;();
 String[] k = {"1", "2", "3", null};
 String[] v = {"a", "b", <mark>null</mark>, "d"};
     
 for(int i=0; i<4; i++) {
  h.put(k[i], v[i]);
 System.out.print(h.get(k[i]) + " ");
 }
 System.out.print(h.size() + " " + h.values() + "\n");
    }
        
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.<mark>NullPointerException</mark>
	at java.util.Hashtable.put(Hashtable.java:459)
	at com.mvivekweb.ocpjp6.obj6.sect1.exam.exhashtable.main(exhashtable.java:18)
        <mark>a b</mark> C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.Hashtable;
import java.util.Map;
    
    
public class exhashtable {
    
    
    public static void main(String[] args) {
        Map&lt;String, String&gt; h = new <mark>Hashtable</mark>&lt;String, String&gt;();
 String[] k = {"1", "2", "3", <mark>null</mark>};
 String[] v = {"a", "b", "c", "d"};
     
 for(int i=0; i<4; i++) {
  h.put(k[i], v[i]);
 System.out.print(h.get(k[i]) + " ");
 }
 System.out.print(h.size() + " " + h.values() + "\n");
    }
        
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.NullPointerException
	at java.util.Hashtable.put(Hashtable.java:464)
	at com.mvivekweb.ocpjp6.obj6.sect1.exam.exhashtable.main(exhashtable.java:18)
<mark>a b c </mark>C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
    
public class SetListex {
    
    public static void main(String[] args) {
        List&lt;String&gt; x = new <mark>LinkedList</mark>&lt;String&gt;();
        Set&lt;String&gt; hs = new <mark>HashSet</mark>&lt;String&gt;();
        String[] v = {"a", "b", "c", "b", "a"};
        for (String s : v) {
            x.add(s);
            hs.add(s);
        }
        System.out.print(hs.size() + " " + x.size() + " ");
        HashSet hs2 = new <mark>HashSet</mark>(x);
        LinkedList x2 = new <mark>LinkedList</mark>(hs);
        System.out.println(hs2.size() + " " + x2.size());
    }
        
}
</pre>

<pre class='out'>run:
3 5 3 3
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
    
    
public class Drawers {
    
    
    public static void main(String[] args) {
        List&lt;String&gt; desk = new ArrayList&lt;String&gt;();
 desk.add("pen"); desk.add("scissors"); desk.add("redStapler");
 System.out.print(desk.indexOf("redStapler"));
 <mark>Collection</mark>.reverse(desk);
 System.out.print(" " + desk.indexOf("redStapler"));
<mark>Collection</mark>.sort(desk);
 System.out.println(" " + desk.indexOf("redStapler"));
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect1/exam/Drawers.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect1\exam\Drawers.java:17: error: cannot find symbol
 Collection.reverse(desk);
           ^
  symbol:   method reverse(List&lt;String&gt;)
  location: interface Collection
com\mvivekweb\ocpjp6\obj6\sect1\exam\Drawers.java:19: error: cannot find symbol
Collection.sort(desk);
          ^
  symbol:   method sort(List&lt;String&gt;)
  location: interface Collection
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>
<p>
We had to throw at least one of these in here, sorry. The reverse() and
sort() methods are in the Collections class, not the Collection class-ouch.
Again, look for this kind of misdirection in the real exam. If the invocations had been
Collections.reverse(desk) and Collections.sort(desk), the result would
have been 2 0 1-remember, indexes are zero-based.</p>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
    
    
public class Drawers {
    
    
    public static void main(String[] args) {
        List&lt;String&gt; desk = new ArrayList&lt;String&gt;();
 desk.add("pen"); desk.add("scissors"); desk.add("redStapler");
 System.out.print(desk.indexOf("redStapler"));
<mark> Collections</mark>.reverse(desk);
 System.out.print(" " + desk.indexOf("redStapler"));
Collections.sort(desk);
 System.out.println(" " + desk.indexOf("redStapler"));
    }
        
}
    
</pre>
<pre class='out'>
run:
2 0 1
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
    
    
public class Piles {
    
    
    public static void main(String[] args) {
       TreeMap&lt;String, String&gt; tm = new <mark>TreeMap</mark>&lt;String, String&gt;();
TreeSet&lt;String&gt; ts = new <mark>TreeSet</mark>&lt;String&gt;();
 String[] k = {"1", "b", "4", "3"};
 String[] v = {"a", "d", "3", "b"};
 for(int i=0; i<4; i++) {
<mark> tm.put(k[i], v[i]);
 ts.add(v[i]);</mark>
 }
 System.out.print(tm.values() + " ");
 <mark>Iterator</mark> it2 = ts.iterator();
 while(it2.hasNext()) System.out.print(it2.next() + "-");
    }
        
}
</pre>
<pre class='out'>
run:
[a, b, 3, d] 3-a-b-d-BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
    
    
public class Bucket {
    
    
    public static void main(String[] args) {
       Set&lt;String&gt; hs = new <mark>HashSet</mark>&lt;String&gt;();
Set&lt;String&gt; lh = new <mark>LinkedHashSet</mark>&lt;String&gt;();
Set&lt;String&gt; ts = new <mark>TreeSet</mark>&lt;String&gt;();
 List&lt;String&gt; al = new <mark>ArrayList</mark>&lt;String&gt;();
 String[] v = {"1", "3", "1", "2"};
 for(int i=0; i< v.length; i++) {
 hs.add(v[i]); lh.add(v[i]); ts.add(v[i]); al.add(v[i]);
 }
 Iterator it = hs.iterator();
 while(it.hasNext()) System.out.print(it.next() + " ");
        System.out.println("\n");
Iterator it2 = lh.iterator();
 while(it2.hasNext()) System.out.print(it2.next() + " ");
  System.out.println("\n");
 Iterator it3 = ts.iterator();
while(it3.hasNext()) System.out.print(it3.next() + " ");
 System.out.println("\n");
 Iterator it5 = al.iterator();
 while(it5.hasNext()) System.out.print(it5.next() + " ");
    }
        
}
    
</pre>
<pre class='out'>
run:
1 2 3 

1 3 2 

1 2 3 

1 3 1 2 BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
import java.util.Arrays;
import java.util.PriorityQueue;
    
    
public class Ps {
    
    
    public static void main(String[] args) {
        PriorityQueue&lt;String&gt; pq = new <mark>PriorityQueue</mark>&lt;String&gt;();
 pq.add("4");
 pq.add("7");
 pq.add("2");
 Object[] pqa = <mark>pq.toArray();</mark>
Arrays.sort(pqa);
for(Object o: pqa) System.out.print(o + "-");
System.out.println();
    }
        
}
</pre>
<pre class='out'>
run:
2-4-7-
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.Arrays;
import java.util.PriorityQueue;
    
    
public class Ps {
    
    
    public static void main(String[] args) {
        PriorityQueue&lt;String&gt; pq = new PriorityQueue&lt;String&gt;();
 pq.add("4");
 pq.add("7");
 pq.add("2");
 String s = <mark>pq.poll();</mark>
while (s != null) {
System.out.print(s + "-");
s = pq.poll();
}
    }
        
}
</pre>
<pre class='out'>
run:
2-4-7-BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect1.exam;
    
import java.util.Arrays;
import java.util.PriorityQueue;
    
    
public class Ps {
    
    
    public static void main(String[] args) {
        PriorityQueue&lt;String&gt; pq = new PriorityQueue&lt;String&gt;();
 pq.add("4");
 pq.add("7");
 pq.add("2");
 String s = pq.<mark>peek();</mark>
while (s != null) {
System.out.print(s + "-");
s = pq.poll();
}
    }
        
}
</pre>
<pre class='out'>
run:
2-2-4-7-BUILD SUCCESSFUL (total time: 0 seconds)</pre>