<h3>Given a design scenario, determine which collection classes and/or interfaces should
    be used to properly implement that design, including the use of the Comparable interface.</h3>


<p>
    write object-oriented applications without using data
    structures like <mark>hashtables</mark> or <mark>linked lists</mark>
</p>

<p>
    maintain a sorted list
</p>
<p>
    gives you <mark>lists,
        sets, maps, and queues</mark> to satisfy most of your coding needs.
    
</p>
<p>
    Pick the best one for your job and you'll get-at the least-<mark>reasonable
        performance.</mark>
</p>
<p>
    a little more <mark>custom</mark>, the Collections
    Framework in the java.util package is <mark>loaded with interfaces and utilities</mark>.
    
</p>


<h3>What Do You Do with a Collection?</h3>

<p>Add <mark>objects</mark> to the collection.</p>

<p>Remove <mark>objects</mark> from the collection.</p>


<p>Find out if an <mark>object (or group of objects) is in</mark> the collection.</p>


<p><mark>Retrieve an object</mark> from the collection (<mark>without removing it</mark>).</p>

<p>
    Iterate through the collection, looking at each element (object) one
    after another.</p>


<h3>Key Interfaces and Classes of the Collections Framework</h3>

<p>you'll need to know <mark>which collection to choose based</mark> on a stated
    requirement.</p>



<p>The core interfaces you need to know for the
    exam (and life in general) are the following nine:</p>

<pre>
Collection
Set
<mark>SortedSet
    NavigableSet
</mark>
List
Map
<mark>SortedMap
NavigableMap</mark>
<mark>Queue</mark>
    
</pre>


<p>The core concrete implementation classes you need to know for the exam are the
    following 13</p>

<pre>
<mark>Collections
Arrays</mark>
</pre>
<pre>
<mark>PriorityQueue</mark>
</pre>
<pre>
ArrayList
Vector
LinkedList
    
</pre>
<pre>
HashSet
LinkedHashSet
TreeSet
    
</pre>
<pre>
HashMap
Hashtable
TreeMap
LinkedHashMap
    
</pre>


<p><mark>none of the Map</mark>-related classes and interfaces extend
    from <mark>Collection</mark>.</p>

<p>
    collection (lowercase c), which <mark>represents any of the data structures</mark> in
    which objects are stored and iterated over.
    
</p>
<p>
    Collection (capital C), which is actually the java.util.Collection <mark>interface
    from which Set, List, and Queue extend</mark>. (That's right, <mark>extend, not implement</mark>.
    There are no direct implementations of Collection.)
    
</p>
<p>
    Collections (capital C and ends with s) is the java.util.Collections class
    that <mark>holds a pile of static utility methods</mark> for use with collections.</p>

<p>Collection
    is an interface with declarations of the methods common to most collections
    including <mark>add(), remove(), contains(), size()</mark>, and <mark>iterator()</mark>.</p>

<p>Collections come in four basic flavors:</p>

<p>Lists - <mark>Lists of things by index</mark> (classes that implement List).</p>

<p>Sets - <mark>Unique</mark> things (classes that implement Set).</p>

<p>Maps - Things with a <mark>unique ID</mark> hashcode buckets(classes that implement Map).</p>

<p>Queues - Things <mark>arranged by the order</mark> in which they are to be processed.</p>


<p>But an implementation can <mark>never be sorted</mark> but <mark>unordered</mark> because sorting is a specific type of ordering</p>


<p>Iterating through a collection usually means
    walking through the elements one after another starting from the first element.</p>


<p>In a <mark>Hashtable</mark>, the elements
    are placed in a (seemingly)<mark> chaotic order based on the hashcode</mark> of the key.</p>


<h3>Ordered</h3>

<p>When a collection is ordered, it means you can iterate through the
    collection in a specific (not-random) order</p>

<p>A <mark>Hashtable</mark> collection is not ordered.</p>

<p>An <mark>ArrayList</mark>, however, keeps the order established
    by the elements' index position</p>

<p><mark>LinkedHashSet</mark> keeps
    the <mark>order established by insertion</mark>, so the last element inserted is the last element in
    the LinkedHashSet</p>


<p>there are some collections that keep an order referred
    to as the <mark>natural order of the elements</mark>, and those collections are then not just
    ordered, but <mark>also sorted</mark>.</p>

<h3>Sorted</h3>

<p>Sorting is done based on properties
    of the objects themselves.</p>

<p>Most commonly, the <mark>sort order
    used is something called the natural order</mark>.</p>

<p>For a collection of String objects, then, the <mark>natural order is alphabetical</mark>. For
    Integer objects, the <mark>natural order is by numeric value</mark></p>

<p>Aside <mark>from natural order</mark> as specified by the <mark>Comparable</mark> interface, it's also
    possible to define other, <mark>different sort orders</mark> using another interface: <mark>Comparator</mark>. </p> 
    

<h3>List Interface</h3>


<p>A List cares about the index</p>

<p>Those key methods include things like
    <mark>get(int index)</mark>, <mark>indexOf(Object o)</mark>, <mark>add(int index, Object obj)</mark>, and so
    on.</p>

<p>All three List implementations are <mark>ordered by index position</mark></p>

<h3>ArrayList</h3>

<p>Think of this as a <mark>growable array</mark>. It gives you <mark>fast iteration and fast
    random access</mark>.</p>

<p>it is an <mark>ordered collection</mark> (by index), but not
        sorted.</p>

<p>ArrayList now implements
    the new <mark>RandomAccess</mark> interface</p>

<p>this list supports <mark>fast (generally constant time) random</mark> access.
</p>


<h3>Vector</h3>


<p>A Vector is basically the <mark>same as an ArrayList</mark></p>

<p>Vector methods are <mark>synchronized
    for thread safety</mark>.</p>

<p>synchronized methods add a<mark> performance hit</mark> you might not need.
    </p>
    
<p>Vector is the only class other than ArrayList to implement RandomAccess.    
    </p>

<h3>LinkedList</h3>
    
<p>ordered by index position, like ArrayList, except
    that the <mark>elements are doubly-linked to one another</mark>.    
</p>

<p>This linkage gives you new
    methods (beyond what you get from the List interface) for adding and removing
    from the beginning or end, which makes it an <mark>easy choice for implementing a stack</mark>
    or <mark>queue</mark>.</p>

<h3>Set Interface</h3>


<p>it <mark>doesn't allow duplicates</mark></p>

<p><mark>equals()</mark> method determines <mark>whether two objects are identical</mark>
</p>

<h3>HashSet</h3>


<p>is an<mark> unsorted, unordered</mark> Set
</p>

<p>It <mark>uses the hashcode
        of the object being inserted</mark>, so the more efficient your hashCode() implementation
    the better access performance you'll get.</p>


<h3>LinkedHashSet</h3>

<p>maintains a doubly-linked List across all elements.
</p>

<p>a LinkedHashSet lets you <mark>iterate through the elements
    in the order in which they were inserted</mark>.</p>

<p>When using <mark>HashSet or LinkedHashSet</mark>, the <mark>objects</mark> you add to them
        <mark>must override hashCode()</mark>.</p>
    
<p>If they don't override hashCode(), the default Object.
    <mark>hashCode() method will allow multiple objects</mark> that you might consider "meaningfully
    equal" to be <mark>added to your "no duplicates allowed" set</mark>. </p>

<h3>TreeSet </h3>

<p>The TreeSet is one of two sorted collections</p>

<p>It uses a <mark>Red-Black tree</mark> structure</p>

<p>elements will be in <mark>ascending order, according to natural order</mark></p>

<p>you can
    construct a TreeSet with <mark>a constructor</mark> that lets you <mark>give the collection your own
        rules for what the order</mark> should be (rather than relying on the ordering defined by
        the elements' class) by <mark>using a Comparable or Comparator</mark></p>


<p>TreeSet
    implements <mark>NavigableSet</mark>.</p>

<h3>Map Interface</h3>

<p>A Map cares about <mark>unique identifiers</mark>.
    
</p>

<p>You map a unique key (the ID) to a specific
    value, where both the <mark>key and the value are</mark>, of course, <mark>objects</mark>.</p>


<p>data structures that use a <mark>key/value
    or name/value pair</mark>.</p>

<p>let you do things like <mark>search for a
            value</mark> based on the key, ask for a <mark>collection of just the values</mark>, or ask for a <mark>collection
        of just the keys</mark>.</p>
    
<p>Like Sets, <mark>Maps rely on the equals()</mark> method to determine whether
            two keys are the same or different. </p>

<h3>HashMap</h3>

<p>gives you an <mark>unsorted, unordered</mark> Map</p>

<p>the other maps add a little more overhead</p>

<p>Where the
    keys land in the Map is based on the <mark>key's hashcode</mark>, so, like HashSet, the <mark>more efficient
        your hashCode() implementation</mark>, the <mark>better access performance</mark> you'll get.</p>

<p>
    HashMap allows <mark>one null key</mark> and <mark>multiple null values</mark> in a collection.
</p>

<h3>Hashtable</h3>


<p>Hashtable is the <mark>synchronized counterpart to HashMap</mark>.</p>

<p>Vector and Hashtable are synchronized, we
    just mean that the <mark>key methods of the class are synchronized</mark></p>


<p>Hashtable <mark>doesn't let you have anything that's null</mark>.</p>

<h3>LinkedHashMap</h3>

<p>maintains insertion order (or, optionally, access order).</p>

<p><mark>slower than HashMap</mark> for adding and removing elements
</p>

<p>expect <mark>faster iteration</mark> with a LinkedHashMap.
</p>


<h3>TreeMap</h3>

<p>sorted by the natural order of
    the elements</p>

<p>Like TreeSet, TreeMap lets you define a <mark>custom sort order</mark> (via a
    <mark>Comparable or Comparator</mark>) when you construct a TreeMap, that specifies how the
    elements should be compared to one another when they're being ordered</p>

<p>TreeMap <mark>implements NavigableMap</mark></p>

<h3>Queue Interface</h3>

<p>designed to <mark>hold a list of "to-dos,"</mark> or things to be processed in some way.
</p>

<p>
    Although other <mark>orders are possible</mark>, queues are typically thought of as <mark>FIFO (first-in,
    first-out)</mark>.
</p>
    
<p>Queues support <mark>all of the standard Collection methods</mark> and they also add
    methods to <mark>add and subtract elements</mark> and <mark>review queue elements</mark>.</p>

<h3>PriorityQueue</h3>


<p>the <mark>LinkedList</mark> class has
    been <mark>enhanced to implement the Queue interface</mark>, <mark>basic queues</mark> can be handled with
    a LinkedList.</p>


<p>The purpose of a PriorityQueue is to create a "<mark>priority-in, priority out</mark>"
    queue as <mark>opposed to a typical FIFO</mark> queue.</p>

<p>A PriorityQueue's elements are <mark>ordered
        either by natural ordering</mark> (in which case the <mark>elements that are sorted first will be
            accessed first</mark>) or <mark>according to a Comparator</mark>.</p>

<p>In either case, the <mark>elements' ordering
    represents their relative priority</mark>.</p>











