<pre>package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
public class Fabric extends Thread{
    
    public static void main(String[] args) {
Thread t = new Thread(new Fabric());
 Thread t2 = new Thread(new Fabric());
 t.start();
 t2.start();
    }
<mark>public static void run() {</mark>
 for(int i = 0; i < 2; i++)
 System.out.print(Thread.currentThread().getName() + " ");
 }
}
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj4/sect1/exam/Fabric.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj4\sect1\exam\Fabric.java:11: error: run() in Fabric cannot implement run() in Runnable
public static void run() {
                   ^
  overriding method is static
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>Fabric does not correctly extend Thread because the run() method cannot
    be static.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
public class Fabric extends Thread{
    
    public static void main(String[] args) {
Thread t = new Thread(new Fabric());
 Thread t2 = new Thread(new Fabric());
 t.start();
 t2.start();
    }
public void run() {
 for(int i = 0; i < 2; i++)
 System.out.print(Thread.currentThread().getName() + " ");
 }
}
    
</pre>

<pre class='out'>run:
     <mark>Thread-3 Thread-3 Thread-1 Thread-1</mark> BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj4/sect1/exam/Fabric.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj4/sect1/exam/Fabric
<mark>Thread-3 Thread-1 Thread-3 Thread-1</mark>
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>



<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
    
public class Jump {
static Paratrooper p;
 static { p = new Paratrooper(); }
 { Thread t1 = new Thread(p, "bob"); t1.start(); }
     
    public static void main(String[] args) {
        new Jump();
 new Thread(new Runnable() { public void run()
{ ; }}, "carol").start();
 new Thread(new Paratrooper(), "alice").start();
    }
Jump() { Thread t2 = new Thread(p, "ted"); t2.start(); }
}
class Paratrooper implements Runnable {
 public void run() {
 System.out.print(Thread.currentThread().getName() + " ");
 } }</pre>
<pre class='out'>
run:
ted bob alice BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre class='out'>run:
bob ted alice BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>threads can be started in init blocks, constructors, and
    inner classes. The thread started in the inner class uses a run() method that doesn't print
    the thread's name, so "carol" is not in the output.</p>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
public class Self <mark>extends Thread</mark>{
    
    public static void main(String[] args) {
try {
Thread t = new Thread(new Self());
 t.start();
 <mark>t.start();</mark>
 } catch (Exception e) { System.out.print("e "); }
    }
public void run() {
 for(int i = 0; i < 2; i++)
 System.out.print(Thread.currentThread().getName() + " ");
 }
}</pre>

<pre class='out'>
run:
<mark>e Thread-1 Thread-1</mark> BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj4/sect1/exam/Self.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj4/sect1/exam/Self
<mark>Thread-1 Thread-1 e</mark>
C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
    
public class Race {
    
    
    public static void main(String[] args) {
        Horse h = new Horse();
Thread t1 = new Thread(h, "Andi");
 Thread t2 = new Thread(h, "Eyra");
 new Race().go(t2);
 t1.start();
 t2.start();
    }
void go(Thread t) { t.start(); }
}
class Horse implements Runnable {
 public void run() {
 System.out.print(Thread.currentThread().getName() + " ");
 } }
</pre>

<pre class='out'>
run:
Exception in thread "main" java.lang.IllegalThreadStateException
<mark>Andi Eyra </mark>	at java.lang.Thread.start(Thread.java:705)
	at com.mvivekweb.ocpjp6.obj4.sect1.exam.Race.main(Race.java:15)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)</pre>

<pre class='out'>run:
Exception in thread "main" java.lang.IllegalThreadStateException
	at java.lang.Thread.start(Thread.java:705)
	at com.mvivekweb.ocpjp6.obj4.sect1.exam.Race.main(Race.java:15)
Eyra Andi C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>

<p>Line 10 will throw an exception because you can't start the same
    thread (in this case, t2) more than once.</p>


<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
    
public class Juggler extends Thread{
    
    
    public static void main(String[] args) {
        try {
 Thread t = new Thread(new Juggler());
 Thread t2 = new Thread(new Juggler());
 } catch (Exception e) { System.out.print("e "); }
    }
public void run() {
 for(int i = 0; i < 2; i++) {
 try { Thread.sleep(500); }
 catch (Exception e) { System.out.print("e2 "); }
 System.out.print(Thread.currentThread().getName() + " ");
 } }
}
    
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>In main(), the start() method was never called to start "t" and "t2", so
    run() never ran.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
    
public class Wander {
    
    
    public static void main(String[] args) {
        Wanderer w = new Wanderer();
 Thread t1 = new Thread();
Thread t2 = new Thread(w);
 Thread t3 = new Thread(w, "fred");
t1.start(); t2.start(); t3.start();
    }
        
}
class Wanderer implements Runnable {
 public void run() {
 for(int i = 0; i < 2; i++)
System.out.print(Thread.currentThread().getName() + " ");
 } }
</pre>
<pre class='out'>
run:
Thread-1 fred fred Thread-1 BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre class='out'>run:
     fred Thread-1 fred Thread-1 BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>Only <mark>t2 and t3 have a runnable target</mark>, and t3's name will certainly be
    "fred",</p>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
public class Tshirt extends Thread{
    
    public static void main(String[] args) {
System.out.print(Thread.currentThread().getId() + " ");
 Thread t1 = new Thread(new Tshirt());
 Thread t2 = new Thread(new Tshirt());
 t1.start();
 t2.run();
    }
public void run() {
 for(int i = 0; i < 2; i++)
 System.out.print(Thread.currentThread().getId() + " " +Thread.currentThread().getName() +"\n");
 }
}
    
</pre>
<pre class='out'>
run:
1 1 main
1 main
11 Thread-1
11 Thread-1
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.exam;
    
public class Tshirt extends Thread{
    
    public static void main(String[] args) {
<mark>System.out.print(Thread.currentThread().getId() + " ");</mark>
 Thread t1 = new Thread(new Tshirt());
 Thread t2 = new Thread(new Tshirt());
 t1.start();
 t2.run();
    }
public void run() {
 for(int i = 0; i < 2; i++)
<mark> System.out.print(Thread.currentThread().getId() +"\n");</mark>
 }
}
</pre>
<pre class='out'>
run:
1 1
1
11
11
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>It's legal to get the ID of the "main" thread. It's also legal to call run()
    directly, but it won't start a new thread. In this case, it's just invoking run() on main's
    call stack. In this code, t2 is never started, so there are only two threads (main and t1),
    therefore only two IDs are in the output.</p>

