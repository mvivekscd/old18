<pre>
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import <mark>java.text</mark>.DateFormat;
import java.util.Date;
import <mark>java.util</mark>.Locale;
    
    
public class Testlocale {
    
    
    public static void main(String[] args) {
        Date d = new Date();
        DateFormat df;
        Locale[] la = {new Locale("it", "IT"), new Locale("pt")};
        for(Locale l: la) {
        df = DateFormat.getDateInstance(DateFormat.FULL, l);
        System.out.println(d.format(df));
 }
    }
        
}
    
        </pre>
        
        <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec4/exam/Testlocale.java
com\mvivekweb\ocpjp6\obj3\sec4\exam\Testlocale.java:19: cannot find symbol
symbol  : method format(java.text.DateFormat)
location: class java.util.Date
        System.out.println(d.format(df));
                            ^
1 error
    
C:\vivek\docs\cert\ocpjp6\src>       
    
        </pre>
<p>       
            we should <mark>use the DateFormat instance to invoke the format() method on the Date</mark>
            instance.       
</p>


<pre>
    
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
    
    
public class TestLocale1 {
    
    
    public static void main(String[] args) {
        Date d = new Date();
    DateFormat df = DateFormat.getDateInstance(DateFormat.FULL);
     Locale[] la = {new Locale("it", "IT"), new Locale("pt", "BR")};
     for(Locale l: la) {
     df.setLocale(l);
     System.out.println(df.format(d));
     }
    }
        
}
    
</pre>




<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec4/exam/Testlocale1.java
com\mvivekweb\ocpjp6\obj3\sec4\exam\Testlocale1.java:18: cannot find symbol
symbol  : method setLocale(java.util.Locale)
location: class java.text.DateFormat
     df.setLocale(l);
       ^
1 error

C:\vivek\docs\cert\ocpjp6\src>


    </pre>
        

<pre>
    
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
    
    
public class TestLocale1 {
    
    
    public static void main(String[] args) {
        Date d = new Date();
    DateFormat df = DateFormat.getDateInstance(DateFormat.FULL);
     Locale[] la = {new Locale("it", "IT"), new Locale("pt", "BR")};
     for(Locale l: la) {
     //df.setLocale(l);
     df = DateFormat.getDateInstance(DateFormat.FULL,l);
     System.out.println(df.format(d));
     }
    }
        
}
    
</pre>

<pre class='out'>
run:
mercoled20 luglio 2016
Quarta-feira, 20 de Julho de 2016
BUILD SUCCESSFUL (total time: 1 second)

</pre>

<p>Any instance of DateFormat <mark>has an immutable Locale</mark>-in other words,
    there is no setLocale() method.</p>

<pre>
    
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.util.Calendar;
    
    
public class Wise {
    
    
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.set(1999,11,25);
        c.roll(Calendar.MONTH, 3);
         System.out.println(c.getTime());
        c.add(Calendar.DATE, 10);
        System.out.println(c.getTime());
    }
        
}
    
</pre>
<pre class='out'>
run:
Thu <mark>Mar</mark> 25 15:56:33 CST 1999
Sun Apr <mark>04</mark> 15:56:33 CDT 1999
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.util.Calendar;
    
    
public class Wise {
    
    
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.set(1999,11,25);
        c.roll(Calendar.MONTH, <mark>0</mark>);
         System.out.println(c.getTime());
        c.add(Calendar.DATE, 10);
        System.out.println(c.getTime());
    }
        
}
    
    
</pre>
<pre class='out'>
run:
Sat Dec 25 15:58:07 CST 1999
Tue Jan 04 15:58:07 CST 2000
BUILD SUCCESSFUL (total time: 1 second)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.util.Calendar;
    
    
public class Wise {
    
    
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.set(1999,11,25);
        c.roll(Calendar.MONTH, <mark>2</mark>);
         System.out.println(c.getTime());
        c.add(Calendar.DATE, 10);
        System.out.println(c.getTime());
    }
        
}
    
    
</pre>
<pre class='out'>
run:
Thu Feb 25 16:00:16 CST 1999
Sun Mar 07 16:00:16 CST 1999
BUILD SUCCESSFUL (total time: 1 second)


    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.util.Date;
    
public class Testdate {
    
    public static void main(String[] args) {
        Date d1 = new Date();
         Date d2 = d1;
    System.out.println(d1);
    d2.setTime(d1.getTime() + (7 * 24 * 60 * 60));
    System.out.println(d2);     
    }
}
    
</pre>
<pre class='out'>
run:
Wed Jul 20 16:09:37 CDT 2016
Wed Jul 20 16:19:42 CDT 2016
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<p>
    When a Date is constructed with no arguments, the system's current time is
    used, not the Java "epoch." The code is correct, and the second date displayed is about 10
    minutes after the first date because Date objects and setTime() both use millisecond
    precision. In order to add a week, we would have also had to multiply by 1000 in line 27.
        
</p>


<pre>
    
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.text.NumberFormat;
import java.util.Locale;
    
    
public class TestNumberFormat {
    
    
    public static void main(String[] args) {
        NumberFormat nf, nf2;
 Number n;
 Locale[] la = NumberFormat.getAvailableLocales();
for(int x=0; x < 10; x++) {
nf = NumberFormat.getCurrencyInstance(la[x]);
 System.out.println(nf.format(123.456f));
 }
 nf2 = NumberFormat.getInstance();
 n = nf2.<mark>parse</mark>("123.456f");
 System.out.println(n);
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec4/exam/TestNumberFormat.java
com\mvivekweb\ocpjp6\obj3\sec4\exam\TestNumberFormat.java:21: unreported exception java.text.ParseException; must be caught or declared to be thrown
 n = nf2.parse("123.456f");
              ^
1 error
    
C:\vivek\docs\cert\ocpjp6\src>
    
    
</pre>
<p>
    The parse() method throws an Exception that must be handled or
    declared. . Remember that
    <mark> when you create a NumberFormat object</mark>, its <mark>Locale is immutable</mark>, but in this code we're
    creating a new NumberFormat object with each iteration of the loop.
        
</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec4.exam;
    
import java.text.DateFormat;
import java.util.*;
    
public class Vogue {
    
    public static void main(String[] args) {
        DateFormat df1 = DateFormat.getInstance();
    DateFormat df2 = DateFormat.<mark>getInstance</mark>(DateFormat.SHORT);
    DateFormat df3 = DateFormat.getDateInstance(DateFormat.FULL);
    DateFormat df4 = DateFormat.getDateInstance(DateFormat.EXTENDED);
    }
}
    
</pre>

<pre class='out'>

C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec4/exam/Vogue.java
com\mvivekweb\ocpjp6\obj3\sec4\exam\Vogue.java:9: getInstance() in java.text.DateFormat cannot be applied to (int)
    DateFormat df2 = DateFormat.getInstance(DateFormat.SHORT);
                               ^
com\mvivekweb\ocpjp6\obj3\sec4\exam\Vogue.java:11: cannot find symbol
symbol  : variable EXTENDED
location: class java.text.DateFormat
    DateFormat df4 = DateFormat.getDateInstance(DateFormat.EXTENDED);
                                                          ^
2 errors

C:\vivek\docs\cert\ocpjp6\src>


    </pre>
<p>
    The DateFormat class is in the java.text package. The
    <mark>getInstance() method takes no arguments,</mark> and it uses the SHORT style by default.
    There is no such style as EXTENDED. Note: We agree that this is largely a memorization
    question. For the most part the exam doesn't tend to be so memorization intensive, but
    this objective tends to be more so. Take the perspective that once you're done you can
    forget the details, and later on, when you�??re using these classes in practice, you'll have that
    nagging sensation that there are in fact a couple of gotchas in the API.
        
</p>
