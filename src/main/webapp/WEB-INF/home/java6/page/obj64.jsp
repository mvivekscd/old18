<h3>Write code that uses the generic versions of the Collections API, in particular the Set,
    List, and Map interfaces and implementation classes. Recognize the limitations of the nongeneric
    Collections API and how to refactor code to use the generic versions.</h3>


<h3>Develop code that makes proper use of type parameters in class/interface declarations,
    instance variables, method arguments, and return types; and write generic methods or
    methods that make use of wildcard types and understand the similarities and differences
    between these two approaches. Write code that uses the NavigableSet and NavigableMap
    interfaces. </h3>


<h3>Generics (Objective 6.4)</h3>
        
        <p> generics aren't just for collections</p>
        <p>A non-generic collection can hold <mark>any kind of object</mark>! A non-generic collection
            is quite happy to hold anything that is <mark>NOT a primitive.</mark></p>
        
        <pre>package com.mvivekweb.ocpjp6.obj6.sect4.generics;
            
import java.util.ArrayList;
import java.util.List;
    
public class gen01 {
    
    public static void main(String[] args) {
        // TODO code application logic here
        List myList = new ArrayList();
        myList.add("Fred");
        myList.add(new Dog());
        myList.add(new Integer(42));
String s = (String) myList.get(0);
    }
        
}  </pre>   
        <pre>        
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
class Dog {
    
    public Dog() {
    }
        
}
        </pre>
<p> Generics let you enforce compile-time type safety on Collections (or other
    classes and methods declared using generic type parameters).</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class gen02 {
    
    public static void main(String[] args) {
        List&lt;String&gt; myList = new ArrayList&lt;String&gt;();
        myList.add("Fred"); // OK, it will hold Strings
        <mark>myList.add(new Dog()); </mark>// compiler error!!
    }
        
}
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj6\sect4\generics>javac
gen02.java
gen02.java:11: error: cannot find symbol
        myList.add(new Dog()); // compiler error!!
                       ^
  symbol:   class Dog
  location: class gen02
1 error

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj6\sect4\generics>
</pre>
<pre>package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class gen02 {
    
    public static void main(String[] args) {
        List&lt;String&gt; myList = new ArrayList&lt;String&gt;();
        myList.add("Fred"); // OK, it will hold Strings
        <mark>String s = myList.get(0);</mark>
    }
        
}
</pre>
<p>The type in angle brackets is referred to as
    either the <mark>"parameterized type,"</mark> <mark>"type parameter,"</mark> or of course just old-fashioned
    <mark>"type."</mark></p>
<p> An ArrayList&lt;Animal&gt; can accept references of type Dog, Cat, or any other
    subtype of Animal (subclass, or if Animal is an interface, implementation).</p>
<p> When using generic collections, <b>a cast is not needed</b> to get (declared type) elements
    out of the collection. With non-generic collections, a cast is required:</p>
<pre>    List&lt;String&gt; gList = new ArrayList&lt;String&gt;();
        List list = new ArrayList();
        // more code
        String s = gList.get(0); // no cast needed
        String s = (String)list.get(0); // cast required
            
        </pre>
<p>method parameters</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.List;
    
    
public class gen03 {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            
    }
        
    void takeListOfStrings(<mark>List&lt;String&gt; strings</mark>) {
        strings.add("foo"); // no problem adding a String
    }
        
    void takeListOfStringsnew(<mark>List&lt;String&gt; strings</mark>) {
        strings.add(new Integer(42)); // NO!! strings is type safe
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj6\sect4\generics>javac
<mark>-source 6 </mark>gen03.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
gen03.java:22: error: no suitable method found for add(Integer)
        strings.add(new Integer(42)); // NO!! strings is type safe
               ^
    method List.add(int,String) is not applicable
      (actual and formal argument lists differ in length)
    method List.add(String) is not applicable
      (actual argument Integer cannot be converted to String by method invocatio
n conversion)
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj6\sect4\generics>
</pre>
  <p>return types  </p>
<pre>
public <mark>List&lt;Dog&gt;</mark> getDogList() {
List&lt;Dog&gt; dogs = new ArrayList&lt;Dog&gt;();
// more code to insert dogs
return dogs;
}
</pre>
  <p>ArrayList
      that could hold any kind of object.<mark>non-generic</mark>
collections and collections of type &lt;Object&gt; aren't entirely the same</p>
  <pre>  
  List myList = new ArrayList(); // old-style, non-generic
  </pre>
  <pre>  List&lt;Object&gt; myList = new ArrayList&lt;Object&gt;(); // holds ANY object type
    </pre>
  <p>  tricky issues</p>
<mark>  methods arguments 
  <br>  polymorphism
  <br>  integrating generic and non-generic
code</mark>
  <h3>Mixing Generic and Non-generic Collections</h3>
  <pre>  
  package com.mvivekweb.ocpjp6.obj6.sect4.generics;
      
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
    
    
public class TestLegacy {
    
    public static void main(String[] args) {
        <mark> List&lt;Integer&gt; myList = new ArrayList&lt;Integer&gt;();</mark>
// type safe collection
        myList.add(4);
        myList.add(6);
        Adder adder = new Adder();
        int total = adder.addAll(myList);
// pass it to an untyped argument
        System.out.println(total);
    }
}
    
class Adder {
    
    int addAll(<mark>List list</mark>) {
// method with a non-generic List argument,
// but assumes (with no guarantee) that it will be Integers
        Iterator it = list.iterator();
        int total = 0;
        while (it.hasNext()) {
            int i = ((Integer) it.next()).intValue();
            total += i;
        }
        return total;
    }
}
</pre>
<pre class='out'> 

C:\vivek\java7\ocpjp6\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj6/sect4/generics/
TestLegacy.java

C:\vivek\java7\ocpjp6\ocpjp6\src>java com.mvivekweb.ocpjp6.obj6.sect4.generics.T
estLegacy
10

C:\vivek\java7\ocpjp6\ocpjp6\src>

    </pre>
  <p> You can pass a generic collection into a method that takes a non-generic collection,
            but the results may be disastrous. The compiler can't stop the method
            from inserting the wrong type into the previously type safe collection.</p>
  
  <pre>  
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class TestBadLegacy {
    
    public static void main(String[] args) {
        List&lt;Integer&gt; myList = new ArrayList&lt;Integer&gt;();
        myList.add(4);
        myList.add(6);
        Inserter in = new Inserter();
        in.<mark>insert</mark>(myList); // pass List&lt;Integer&gt; to legacy code
    }
}
    
class Inserter {
// method with a non-generic List argument
    
    void insert(<mark>List list</mark>) {
        //list.add(new Integer(42)); // adds to the incoming list
        list.add(<mark>new String("42")</mark>);
            
    }
}
</pre>
  
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sec
t4/generics/TestBadLegacy.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
Note: com\mvivekweb\ocpjp6\obj6\sect4\generics\TestBadLegacy.java <mark>uses unchecked
 or unsafe operations.</mark>
Note: Recompile with -Xlint:unchecked for details.
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com.mvivekweb.ocpjp6.obj6.sect4.generics.T
estBadLegacy

C:\vivek\java7\ocpjp6\ocpjp6\src>  
  
    </pre>
  
        <p> If the compiler can recognize that non-type-safe code is potentially endangering
            something you originally declared as type-safe, you will get a <mark>compiler
            warning</mark>. For instance, if you pass a List&lt;String&gt; into a method declared as
            void foo(List aList) { aList.add(anInteger); }
            You'll get a warning because add() is potentially "unsafe".</p>
        <p> <mark>"Compiles without error" </mark>is not the same as <mark>"compiles without warnings."</mark>
            <b>A compilation warning is not considered a compilation error or failure.</b></p>
        
        <p>        when
            you are using generics, and mixing both typed and untyped code, <mark>warnings matter</mark>
        </p>
        
        <p>All your <mark>generic code</mark>
            is strictly <mark>for the compiler</mark>. Through a process called <mark><b>"type erasure,"</b></mark> the compiler
            does all of its verifications on your generic code and then strips the type information
            out of the class bytecode.   </p>
        <p><mark>At runtime</mark>, ALL collection code <mark>both legacy and new
                Java 5 code you write using generics</mark><b> looks </b>exactly like <mark>the pre-generic version of
                    collections. </mark>  </p>     
        
        <p> <b>Generic type information does not exist at runtime</b> . it is for <mark>compile-time
                safety</mark> only. Mixing generics with legacy code can create compiled code that
            may throw an exception at runtime.</p>
        
        <p> <b>arrays, which give you BOTH compile-time
                protection and runtime protection.</b> </p>  
                
                
                <p>compiler warning isn't very descriptive, but the second note suggests that
                    you recompile with -Xlint:unchecked.  </p>
                
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 <mark>-Xlint:unchecked</mark> com/mvivekwe
b/ocpjp6/obj6/sect4/generics/TestBadLegacy.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\TestBadLegacy.java:23: warning: [unchec
ked] <mark>unchecked call to add(E) as a member of the raw type List</mark>
        list.add(new String("42"));
                ^
  where E is a type-variable:
    E extends Object declared in interface List
2 warnings

C:\vivek\java7\ocpjp6\ocpjp6\src>                
    </pre>
                
                <h3> Polymorphism and Generics  </h3>             
                <p> <mark>Polymorphic</mark> assignments applies only to the <mark>base type</mark>, <mark>not the generic type</mark>
            parameter. You can say</p>
        <pre> List&lt;Animal&gt; aList = new ArrayList&lt;Animal&gt;(); // yes
                    </pre>  You can't say
                    <pre>List&lt;Animal&gt; aList = new ArrayList&lt;Dog&gt;(); // no
                                </pre>  
                    <p>        <mark>List and ArrayList</mark> are the <mark>base type </mark>and <mark>Animal</mark> is the <mark>generic type.</mark></p>
                    <p>So an ArrayList
                        can be assigned to a List, but a collection of <mark>&lt;JButton&gt; cannot be assigned</mark> to a reference of <mark>&lt;Object&gt;</mark>, even though JButton is a subtype of Object.  </p>                
                    <p>There's a very simple rule here.the <mark>type</mark> of the <mark>variable
                            declaration</mark> must match the type you pass to the actual <mark>object type</mark>.</p>
                    <p>If you declare
                        <mark>List&lt;Foo&gt; foo </mark>then whatever you assign to the foo reference MUST be of the generic type <mark>&lt;Foo&gt;</mark>. <mark>Not a subtype of &lt;Foo&gt;</mark>. <mark>Not a supertype of &lt;Foo&gt;</mark></p>
                    <h3>Generic Methods</h3>
                    
                    <p> The polymorphic assignment rule applies everywhere an assignment can be
                            made. The following are NOT allowed:</p>
                    <pre>void foo(List&lt;Animal&gt; aList) { } // cannot take a List&lt;Dog&gt;</pre>
                                <pre>List&lt;Animal&gt; bar() { } // cannot return a List&lt;Dog&gt;</pre>
<p>polymorphic array </p>


<pre>package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
abstract class Animal {
    
    public abstract void checkup();
}
    
class Dog1 extends Animal {
    
    public void checkup() { // implement Dog-specific code
        System.out.println("Dog checkup");
    }
}
    
class Cat extends Animal {
    
    public void checkup() { // implement Cat-specific code
        System.out.println("Cat checkup");
    }
}
    
class Bird extends Animal {
    
    public void checkup() { // implement Bird-specific code
        System.out.println("Bird checkup");
    }
}
    
public class AnimalDoctor {
// method takes an array of any animal subtype
    
    public void checkAnimals(<mark>Animal[] animals</mark>) {
        for (Animal a : animals) {
            a.checkup();
        }
    }
        
    public static void main(String[] args) {
// test it
        Dog1[] dogs = {<mark>new Dog1(), new Dog1()</mark>};
        Cat[] cats = {<mark>new Cat(), new Cat(), new Cat()</mark>};
        Bird[] birds = {<mark>new Bird()</mark>};
        AnimalDoctor doc = new AnimalDoctor();
        doc.checkAnimals(dogs); // pass the Dog[]
        doc.checkAnimals(cats); // pass the Cat[]
        doc.checkAnimals(birds); // pass the Bird[]
    }
}
</pre>
<pre class='out'>
Dog checkup
Dog checkup
Cat checkup
Cat checkup
Cat checkup
Bird checkup
    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class AnimalDoctorGeneric {
// method takes an array of any animal subtype
    
    public void checkAnimals(<mark>ArrayList&lt;Animal&gt; animals</mark>) {
        for (Animal a : animals) {
            a.checkup();
        }
    }
        
    public static void main(String[] args) {
// make ArrayLists instead of arrays for Dog, Cat, Bird
        <mark>List&lt;Dog1&gt; dogs = new ArrayList&lt;Dog1&gt;();</mark>
        dogs.add(new Dog1());
        dogs.add(new Dog1());
        <mark>List&lt;Cat&gt; cats = new ArrayList&lt;Cat&gt;();</mark>
        cats.add(new Cat());
        cats.add(new Cat());
       <mark>List&lt;Bird&gt; birds = new ArrayList&lt;Bird&gt;();</mark>
        birds.add(new Bird());
// this code is the same as the Array version
        AnimalDoctorGeneric doc = new AnimalDoctorGeneric();
// this worked when we used arrays instead of ArrayLists
doc.checkAnimals(dogs); // send a List&lt;Dog&gt; 
doc.checkAnimals(cats); // send a List&lt;Cat&gt; 
doc.checkAnimals(birds); // send a List&lt;Bird&gt;
    }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 -Xlint:unchecked com/mvivekwe
b/ocpjp6/obj6/sect4/generics/AnimalDoctorGeneric.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\AnimalDoctorGeneric.java:28: error: met
hod checkAnimals in class AnimalDoctorGeneric cannot be applied to given types;
       doc.checkAnimals(dogs); // send a List&lt;Dog&gt;
           ^
  required: ArrayList&lt;Animal&gt;
  found: List&lt;Dog1&gt;
  reason: actual argument List&lt;Dog1&gt; cannot be converted to ArrayList&lt;Animal&gt; by
 method invocation conversion
com\mvivekweb\ocpjp6\obj6\sect4\generics\AnimalDoctorGeneric.java:29: error: met
hod checkAnimals in class AnimalDoctorGeneric cannot be applied to given types;
        doc.checkAnimals(cats); // send a List&lt;Cat&gt;
           ^
  required: ArrayList&lt;Animal&gt;
  found: List&lt;Cat&gt;
  reason: actual argument List&lt;Cat&gt; cannot be converted to ArrayList&lt;Animal&gt; by
method invocation conversion
com\mvivekweb\ocpjp6\obj6\sect4\generics\AnimalDoctorGeneric.java:30: error: met
hod checkAnimals in class AnimalDoctorGeneric cannot be applied to given types;
        doc.checkAnimals(birds); // send a List&lt;Bird&gt;
           ^
  required: ArrayList&lt;Animal&gt;
  found: List&lt;Bird&gt;
  reason: actual argument List&lt;Bird&gt; cannot be converted to ArrayList&lt;Animal&gt; by
 method invocation conversion
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class AnimalDoctorGeneric {
    
    public void checkAnimals(<mark>List&lt;Animal&gt; animals</mark>) {
        for (Animal a : animals) {
            a.checkup();
        }
    }
        
    public static void main(String[] args) {
        <mark>List&lt;Animal&gt; animal = new ArrayList&lt;Animal&gt;();</mark>
        animal.add(<mark>new Dog1()</mark>);
        animal.add(new Dog1());
        AnimalDoctorGeneric doc = new AnimalDoctorGeneric();
            
        doc.checkAnimals(animal); // send a List&lt;Dog&gt;
            
    }
}
</pre>
<pre class='out'>run:
Dog checkup
Dog checkup
    </pre>

<p><mark>you can always add a Dog to an Animal collection</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class AnimalDoctorGeneric {
    
    public void addAnimal(<mark>List&lt;? extends Animal&gt; animals</mark>) {
        animals.add(<mark>new Dog1()</mark>); 
    }
        
    public static void main(String[] args) {
        List&lt;Dog1&gt; animals = new ArrayList&lt;Dog1&gt;();
        animals.add(new Dog1());
        animals.add(new Dog1());
        AnimalDoctorGeneric doc = new AnimalDoctorGeneric();
        doc.addAnimal(animals); // THIS is where it breaks!
    }
}
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 -Xlint:unchecked com/mvivekwe
b/ocpjp6/obj6/sect4/generics/AnimalDoctorGeneric.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\AnimalDoctorGeneric.java:9: error: no s
uitable method found for add(Dog1)
        animals.add(new Dog1());
               ^
    method List.add(int,CAP#1) is not applicable
      (actual and formal argument lists differ in length)
    method List.add(CAP#1) is not applicable
      (actual argument Dog1 cannot be converted to CAP#1 by method invocation co
nversion)
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Animal from capture of ? extends Animal
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
    </pre>
                                        <p> Wildcard syntax allows a generic method, accept subtypes (or supertypes) of
                                            the declared type of the method argument:</p>
<pre>void addD(List&lt;Dog&gt; d) {} // can take only &lt;Dog&gt;</pre>
<pre>void addD(List&lt;? extends Dog&gt;) {} // take a &lt;Dog&gt; or &lt;Beagle&gt;
</pre>
<p> The wildcard keyword <mark>extends</mark> is used to mean either <mark>"extends" or "implements."</mark>
So in &lt;? extends Dog&gt;, Dog can be a class or an interface.</p>
<p> When using a wildcard, List&lt;? extends Dog&gt;, the collection can be
    <mark>accessed</mark> but <mark>not modified</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class AnimalDoctorGeneric {
    
    public void addAnimal(<mark>List&lt;? super Dog1&gt; animals</mark>) {
        animals.add(new Dog1()); 
    }
        
    public static void main(String[] args) {
        List&lt;Dog1&gt; animals = new ArrayList&lt;Dog1&gt;();
        animals.add(new Dog1());
        animals.add(new Dog1());
        AnimalDoctorGeneric doc = new AnimalDoctorGeneric();
        doc.addAnimal(animals); // THIS is where it breaks!
    }
}
</pre>
<p>the <mark>super</mark> keyword in wildcard
    notation lets you have a restricted, but still possible way to add to a collection
</p>
<p> When using a wildcard, <mark>List&lt;?&gt;,</mark> any generic type can be assigned to the
    reference, but for <mark>access only</mark>, no modifications.</p>
<p> List&lt;Object&gt; refers only to a List&lt;Object&gt;, while <mark>List&lt;?&gt; or
        List&lt;? extends Object&gt;</mark> can hold any type of object, but for access only.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class TestWildcards {
    
    public static void main(String[] args) {
        List&lt;Integer&gt; myList = new ArrayList&lt;Integer&gt;();
        Bar bar = new Bar();
        bar.doInsert(myList);
    }
}
    
class Bar {
    
    void doInsert(<mark>List&lt;?&gt; list</mark>) {
        <mark>list.add(new Dog());</mark>
    }
}
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 -Xlint:unchecked com/mvivekwe
b/ocpjp6/obj6/sect4/generics/TestWildcards.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\TestWildcards.java:18: error: no suitab
le method found for add(Dog)
        list.add(new Dog());
            ^
    method List.add(int,CAP#1) is not applicable
      (actual and formal argument lists differ in length)
    method List.add(CAP#1) is not applicable
      (actual argument Dog cannot be converted to CAP#1 by method invocation con
version)
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Object from capture of ?
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class TestWildcards {
    
    public static void main(String[] args) {
        <mark>List&lt;Integer&gt; myList = new ArrayList&lt;Integer&gt;();</mark>
        Bar bar = new Bar();
        bar.doInsert(<mark>myList</mark>);
    }
}
    
class Bar {
    
    void doInsert(<mark>List&lt;Object&gt; list</mark>) {
        list.add(new Dog());
    }
}
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 -Xlint:unchecked com/mvivekwe
b/ocpjp6/obj6/sect4/generics/TestWildcards.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\TestWildcards.java:11: error: method do
Insert in class Bar cannot be applied to given types;
        bar.doInsert(myList);
           ^
  required: List&lt;Object&gt;
  found: List&lt;Integer&gt;
  reason: actual argument List&lt;Integer&gt; cannot be converted to List&lt;Object&gt; by m ethod invocation conversion
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>

<p>valid wildcard instantiations</p>
<pre><mark>List&lt;?&gt;</mark> list = new ArrayList&lt;Dog&gt;();
</pre>
<pre><mark>List&lt;? extends Animal&gt;</mark> aList = new ArrayList&lt;Dog&gt;();

</pre>
<pre><mark>List&lt;? super Dog&gt;</mark> bList = new ArrayList&lt;Animal&gt;();
</pre>
<p>you cannot use wildcard notation in the object creation.
</p>
<pre>
List&lt;?&gt; foo = <mark>new ArrayList&lt;? extends Animal&gt;();</mark>
</pre>

<h3>Generic Declarations</h3>
<p> Declaration conventions for generics use T for type and E for element:</p>
<pre>public interface List&lt;E&gt; // API declaration for List</pre>
<pre>boolean add(E o) // List.add() declaration</pre>

<p>E stands for "<mark>Element,</mark>" and it's used when the <mark>template is a collection</mark>
</p>
<p>T (stands for <mark>"type"</mark>), used for, well, things that are <mark>NOT
    collections</mark>.
</p>
<p> The generics type identifier can be used in class, method, and variable
declarations:</p>
<pre>class Foo&lt;t&gt; { } // a class</pre>
<pre>T anInstance; // an instance variable</pre>
<pre>Foo(T aRef) {} // a constructor argument</pre>
<pre>void bar(T aRef) {} // a method argument</pre>
<pre>T baz() {} // a return type</pre>
The compiler will substitute the actual type.

<h3>Making Your Own Generic Class</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.List;
    
public class RentalGeneric<mark>&lt;T&gt;</mark> { // &quot;T&quot; is for the type
// parameter
    
    private <mark>List&lt;T&gt;</mark> rentalPool; // Use the class type for the
// List type
    private int maxNum;
        
    public RentalGeneric(
            int maxNum, <mark>List&lt;T&gt;</mark> rentalPool) { // constructor takes a
// List of the class type
        this.maxNum = maxNum;
        this.rentalPool = rentalPool;
    }
        
    public <mark>T</mark> getRental() { // we rent out a T
// blocks until there's something available
        return rentalPool.get(0);
    }
        
    public void returnRental(<mark>T</mark> returnedThing) { // and the renter
// returns a T
        rentalPool.add(returnedThing);
    }
}
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class TestRental {
    
    public static void main(String[] args) {
//make some Cars for the pool
        Car c1 = new Car();
        Car c2 = new Car();
        <mark>List&lt;Car&gt;</mark> carList = new <mark>ArrayList&lt;Car&gt;();</mark>
        carList.add(c1);
        carList.add(c2);
       <mark> RentalGeneric&lt;Car&gt;</mark> carRental = new <mark>RentalGeneric&lt;Car&gt;(2, carList);</mark>
// now get a car out, and it won't need a cast
        Car carToRent = carRental.getRental();
        carRental.returnRental(carToRent);
// can we stick something else in the original carList?
//carList.add(new Cat("Fluffy"));
    }
}
</pre>
<p>generic class example2</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class TestGenerics&lt;T&gt; {// as the class type
    
    <mark>T</mark> anInstance; // as an instance variable type
    <mark>T</mark>[] anArrayOfTs; // as an array type
        
    TestGenerics(T anInstance) { // as an argument type
        this.anInstance = anInstance;
    }
        
    T getT() { // as a return type
        return anInstance;
    }
}
</pre>

<p>generic class example 3</p>

<pre>package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class UseTwo<mark>&lt;T, X&gt;</mark> {
    
    <mark>T</mark> one;
    <mark>X</mark> two;
        
    UseTwo(<mark>T</mark> one, <mark>X</mark> two) {
        this.one = one;
        this.two = two;
    }
        
    <mark>T</mark> getT() {
        return one;
    }
        
    <mark>X</mark> getX() {
        return two;
    }
// test it by creating it with &lt;String, Integer&gt;
    
    public static void main(String[] args) {
UseTwo<mark>&lt;String, Integer&gt;</mark> twos = new UseTwo<mark>&lt;String, Integer&gt;</mark>("foo", 42);
        String theT = twos.getT(); // returns a String
        int theX = twos.getX(); // returns Integer, unboxes to int
    }
}
</pre>

<p>generic class example 4</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class AnimalHolder<mark>&lt;T extends Animal&gt;</mark> {// use &quot;T&quot; instead
// of "?"
    
    T animal;
        
    public static void main(String[] args) {
        AnimalHolder&lt;Dog1&gt; dogHolder = new AnimalHolder&lt;Dog1&gt;(); // OK
            
    }
}
</pre>

<p> You can use more than one parameterized type in a declaration:</p>
<pre>public class UseTwo&lt;T, X&gt; { }</pre>

<h3>Creating Generic Methods</h3>

<p>generic method ex 1</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class CreateAnArrayList {
    
    public <mark>&lt;T&gt;</mark> void makeArrayList(<mark>T</mark> t) { // take an object of an
// unknown type and use a
// "T" to represent the type
       <mark>List&lt;T&gt;</mark> list = new <mark>ArrayList&lt;T&gt;</mark>(); // now we can create the
// list using "T"
        list.add(t);
    }
}
</pre>
<p> You can declare a generic method using a type not defined in the class:</p>
<pre>public &lt;T&gt; void makeList(T t) { }</pre>
    is NOT using T as the return type. This method has a void return type, but
    to use T within the method's argument you <mark>must declare</mark> the <mark>&lt;T&gt;</mark>, which
    happens <mark>before</mark> the <mark>return type</mark>.  

<p>generic method ex 2</p>    
<pre>    
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class CreateAnArrayList {
    
    public &lt;T&gt; void makeArrayList(T t) { // take an object of an
// unknown type and use a
// "T" to represent the type
        List&lt;T&gt; list = new ArrayList&lt;T&gt;(); // now we can create the
// list using "T"
        list.add(t);
    }
        
        public <mark>&lt;T extends Number&gt;</mark> void makeArrayNumberList(<mark>T</mark> t) { // take an object of an
// unknown type and use a
// "T" to represent the type
        List&lt;T&gt; list = new ArrayList&lt;T&gt;(); // now we can create the
// list using "T"
        list.add(t);
    }
}
</pre>
<p>constructor arguments? They, too, can be declared with a generic type,
    but then it looks even stranger since constructors have <mark>no return type</mark> at all</p>
<p>generic method ex 3 constructor</p>   

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class Radio {
    public <mark>&lt;T&gt;</mark> Radio(T t) { } // legal constructor
}
</pre>

<p>generic method ex 4 </p>   
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class X {
    public <X> X(X x) { }
}
</pre>

<p>invalid generics use
</p>

<pre>public class NumberHolder&lt;<mark>?</mark> extends Number&gt; { }
</pre>

<pre>public class NumberHolder&lt;<mark>?</mark>&gt; { <mark>?</mark> aNum; }
</pre>


<h3>
    Navigating (Searching) TreeSets and TreeMaps
    
</h3>


<p>searching
    TreeSets and TreeMaps.</p>


<p>Java 6 introduced (among others) two new interfaces:
    java.util.<mark>NavigableSet</mark> and java.util.<mark>NavigableMap</mark>.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
import java.util.TreeSet;
    
public class Ferry {
    public static void main(String[] args) {
        TreeSet&lt;Integer&gt; <mark>times</mark> = new TreeSet&lt;Integer&gt;();
        times.add(1205); // add some departure times
        times.add(1505);
        times.add(1545);
        times.add(1830);
        times.add(2010);
        times.add(2100);
// Java 5 version
        TreeSet&lt;Integer&gt; subset = new TreeSet&lt;Integer&gt;();
        <mark>subset = (TreeSet) times.headSet(1600);</mark>
        System.out.println("J5 - last before 4pm is: " + <mark>subset.last()</mark>);
       <mark>TreeSet&lt;Integer&gt; sub2 = new TreeSet&lt;Integer&gt;();</mark>
        <mark>sub2 = (TreeSet) times.tailSet(2000);</mark>
        System.out.println("J5 - first after 8pm is: " + <mark>sub2.first()</mark>);
// Java 6 version using the new lower() and higher() methods
        System.out.println("J6 - last before 4pm is: " + times.<mark><b>lower</b></mark>(1600));
        System.out.println("J6 - first after 8pm is: " + times.<mark><b>higher</b></mark>(2000));
    }
}
    
</pre>
<pre class='out'>
run:
J5 - last before 4pm is: 1545
J5 - first after 8pm is: 2010
J6 - last before 4pm is: 1545
J6 - first after 8pm is: 2010
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>
    On the other hand, using
    the new Java 6 methods <mark>lower() and higher()</mark>, the code <mark>becomes a lot cleaner</mark>.
    
</p>

<p>the NavigableSet methods</p>
<pre>
<mark>lower() than, floor(), higher() than, ceiling(),</mark>
</pre>


<p>NavigableMap methods are</p>
<pre>
lowerKey(), floorKey(), ceilingKey(), and
higherKey().
    
</pre>
<p>
    lower()
    returns the element <mark>less than the given element</mark>
</p>
<p>
    floor() returns the element
    <mark>less than or equal to the given element</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
import java.util.TreeSet;
    
public class Ferry {
    public static void main(String[] args) {
        TreeSet&lt;Integer&gt; <mark>times</mark> = new TreeSet&lt;Integer&gt;();
        times.add(1205); // add some departure times
        times.add(1505);
        times.add(1545);
        times.add(1830);
        times.add(2010);
        times.add(2100);
            
        System.out.println("J6 - last before 4pm is: floor=" + <mark>times.floor(1545)</mark>);
         System.out.println("J6 - last before 4pm is: lower=" + <mark>times.lower(1545)</mark>);
        System.out.println("J6 - first after 8pm is: " + times.higher(2000));
    }
}
</pre>
<pre class='out'>
run:
J6 - last before 4pm is: <mark>floor=1545</mark>
J6 - last before 4pm is: <mark>lower=1505</mark>
J6 - first after 8pm is: 2010
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
import java.util.TreeSet;
    
public class Ferry {
    public static void main(String[] args) {
          TreeSet&lt;String&gt; times1 = new TreeSet&lt;String&gt;();
          times1.add("viv");
          times1.add("manj");
          times1.add("maad");
              
<mark>          System.out.println(""+times1.floor("viv"));
          System.out.println(""+times1.lower("viv"));</mark>
        TreeSet&lt;Integer&gt; times = new TreeSet&lt;Integer&gt;();
        times.add(1205); // add some departure times
        times.add(1505);
        times.add(1545);
        times.add(1830);
        times.add(2010);
        times.add(2100);
            
        System.out.println("J6 - last before 4pm is: floor=" + times.floor(1545));
         System.out.println("J6 - last before 4pm is: lower=" + times.lower(1545));
        System.out.println("J6 - first after 8pm is: " + times.higher(2000));
    }
}
</pre>
<pre class='out'>
run:
<mark>viv
manj</mark>
J6 - last before 4pm is: floor=1545
J6 - last before 4pm is: lower=1505
J6 - first after 8pm is: 2010
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<h3>
    Other Navigation Methods</h3>

<h3>    Polling    </h3>
        
        <p>it is <mark>new</mark> to TreeSet and
            TreeMap.</p>
        
        <p>The idea of polling is that we want both to <mark>retrieve</mark> and <mark>remove</mark> an
            <mark>element</mark> from either the <mark>beginning or the end</mark> of a collection.</p>
        <pre>      
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
import java.util.TreeMap;
import java.util.TreeSet;
    
public class PollingTest {
    
    public static void main(String[] args) {
        TreeSet&lt;String&gt; times1 = new TreeSet&lt;String&gt;();
        times1.add("viv");
        times1.add("manj");
        times1.add("maad");
        System.out.println("times1=" + times1);
        times1<mark>.pollFirst();</mark>
        System.out.println("times1=" + times1);
        times1<mark>.pollLast();</mark>
        System.out.println("times1=" + times1);
            
       <mark> TreeMap&lt;String, String&gt; map = new TreeMap&lt;String, String&gt;();</mark>
        map.put("a", "ant");
        map.put("d", "dog");
        map.put("h", "horse");
        System.out.println("map=" + map);
        map<mark>.pollFirstEntry();</mark>
        System.out.println("map=" + map);
        map<mark>.pollLastEntry();</mark>
        System.out.println("map=" + map);
    }
}
</pre>
        <pre class='out'>       
run:
times1=[maad, manj, viv]
times1=[manj, viv]
times1=[manj]
map={a=ant, d=dog, h=horse}
map={d=dog, h=horse}
map={d=dog}
BUILD SUCCESSFUL (total time: 1 second)        
        </pre>
        
        <h3>Descending Order</h3>
        
        <p><mark>new</mark> to Java 6 for TreeSet and TreeMap are methods that return a collection
            in the <mark>reverse order of the collection on</mark> which the method was invoked.    </p>
 <pre>       
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;

import java.util.TreeMap;
import java.util.TreeSet;

public class descendingTest {
    public static void main(String[] args) {
        TreeSet&lt;String&gt; times1 = new TreeSet&lt;String&gt;();
        times1.add("viv");
        times1.add("manj");
        times1.add("maad");
        System.out.println("times1=" + times1);
        times1.descendingSet();
        <mark>System.out.println("times1=" + times1.descendingSet());</mark>

        <mark> TreeMap&lt;String, String&gt; map = new TreeMap&lt;String, String&gt;();</mark>
        map.put("a", "ant");
        map.put("h", "dog");
        map.put("b", "horse");
        System.out.println("map=" + map);
       <mark> System.out.println("map=" + map.descendingMap());</mark>
    }
}
</pre>
        
 <pre class='out'>       
run:
times1=[maad, manj, viv]
<mark>times1=[viv, manj, maad]</mark>
map={a=ant, b=horse, h=dog}
<mark>map={h=dog, b=horse, a=ant}</mark>
BUILD SUCCESSFUL (total time: 1 second)
        
    </pre> 
        
        <h3>Backed Collections  </h3>
        <pre>       
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
    
import java.util.SortedMap;
import java.util.TreeMap;
    
public class BackedCollection {
    
    
    public static void main(String[] args) {
        
        TreeMap&lt;String, String&gt; map = new TreeMap&lt;String, String&gt;();
        map.put("a", "ant");
        map.put("d", "dog");
        map.put("h", "horse");
        System.out.println("map="+map );
        <mark>SortedMap&lt;String, String&gt; submap;</mark>
        <mark>submap = map.subMap("b", "g"); // #1 create a backed collection</mark>
       <mark> System.out.println(map + " " + submap); // #2 show contents</mark>
        map.put("b", "bat"); // #3 add to original
        submap.put("f", "fish"); // #4 add to copy
        map.put("r", "raccoon"); // #5 add to original - out of range
// submap.put("p", "pig"); // #6 add to copy - out of range
        System.out.println(map + " " + submap); // #7 show final contents
    }
}
</pre>
<pre class='out'>        
run:
map={a=ant, d=dog, h=horse}
<mark>{a=ant, d=dog, h=horse} {d=dog}</mark>
<mark>{a=ant, b=bat, d=dog, f=fish, h=horse, r=raccoon} {b=bat, d=dog, f=fish}</mark>
BUILD SUCCESSFUL (total time: 1 second)        
    </pre> 
        
        
<p>the subMap() method is <mark>making a copy of a portion
            of the TreeMap</mark> named map.</p>        
        
<p><mark>copy between b and g </mark>   </p>    
        
<p>When we <mark>add key-value pairs</mark> to either the
    original<mark> TreeMap</mark> or the partial-copy <mark>SortedMap</mark>, the new entries were automatically
    added to the other collection-sometimes.  </p>
<p>
    When submap was created, <mark>we provided
    a value range</mark> for the new collection. This range defines not only what should be
    included when the partial copy is created, but also defines the range of values that
    can be added to the copy.
</p>

<p>If you attempt
    to add an out-of-range entry to the copied collection <mark>an exception will be thrown</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
    
import java.util.SortedMap;
import java.util.TreeMap;
    
public class BackedCollection {
    
    
    public static void main(String[] args) {
        
        TreeMap&lt;String, String&gt; map = new TreeMap&lt;String, String&gt;();
        map.put("a", "ant");
        map.put("d", "dog");
        map.put("h", "horse");
        System.out.println("map="+map );
        <mark>SortedMap&lt;String, String&gt; submap;</mark>
        submap = map.subMap("b", "g"); // #1 create a backed collection
        System.out.println(map + " " + submap); // #2 show contents
        map.put("b", "bat"); // #3 add to original
        submap.put("f", "fish"); // #4 add to copy
        map.put("r", "raccoon"); // #5 add to original - out of range
      <mark>submap.put("p", "pig");</mark> // #6 add to copy - out of range
        System.out.println(map + " " + submap); // #7 show final contents
    }
}
    
</pre>
<pre class='out'>
run:
map={a=ant, d=dog, h=horse}
{a=ant, d=dog, h=horse} {d=dog}
Exception in thread "main" <mark>java.lang.IllegalArgumentException</mark>
	at java.util.TreeMap$NavigableSubMap.put(Unknown Source)
	at com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap.BackedCollection.main(BackedCollection.java:23)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>three methods from TreeSet</p>
<pre>
TreeSet-(headSet(), subSet(), and
tailSet()
    
</pre>

<p>three methods from TreeMap</p>
<pre>
headMap(), subMap(), and
tailMap()
    
</pre>


<p>can see a lot of parallels between the TreeSet and the TreeMap methods</p>


<pre>


package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;

import java.util.TreeMap;
import java.util.TreeSet;


public class HeadtailTest {


    public static void main(String[] args) {
        TreeSet&lt;String&gt; times1 = new TreeSet&lt;String&gt;();
        times1.add("viv");
        times1.add("manj");
        times1.add("maad");
        System.out.println("times1=" + times1);
        System.out.println("times1.headSet(\"manj\")="+<mark>times1.headSet("manj")</mark>);
        System.out.println("times1.tailSet(\"manj\")="+<mark>times1.tailSet("manj")</mark>);
        System.out.println("times1.subSet(\"manj\",\"viv\")="+<mark>times1.subSet("manj","viv"))</mark>;
        System.out.println("times1.subSet(\"manj\",true,\"viv\",true="+<mark>times1.subSet("manj",true,"viv",true)</mark>);
        
        TreeMap&lt;String, String&gt; map = new TreeMap&lt;String, String&gt;();
        map.put("a", "ant");
        map.put("h", "dog");
        map.put("b", "horse");
        
        System.out.println("map=" + map);
        
        System.out.println("map.headMap(\"b\")="+<mark>map.headMap("b")</mark>);
         System.out.println("map.headMap(\"b\", true))="+<mark>map.headMap("b", true)</mark>);
        System.out.println("map.headMap(\"d\")="+<mark>map.tailMap("h")</mark>);
        System.out.println("map.headMap(\"d\")="+<mark>map.subMap("b","h")</mark>);
        
    }

}

    
</pre>
<pre class='out'>
run:
<mark>times1=[maad, manj, viv]</mark>
times1.headSet("manj")=<mark>[maad]</mark>
times1.tailSet("manj")=<mark>[manj, viv]</mark>
times1.subSet("manj","viv")=<mark>[manj]</mark>
times1.subSet("manj",true,"viv",true=<mark>[manj, viv]</mark>
map={a=ant, b=horse, h=dog}
map.headMap("b")=<mark>{a=ant}</mark>
map.headMap("b", true))=<mark>{a=ant, b=horse}</mark>
map.headMap("d")=<mark>{h=dog}</mark>
map.headMap("d")=<mark>{b=horse}</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>the question of whether the subsetted collection's end
    points are <mark>inclusive or exclusive</mark> is a little tricky.</p>

<p>you have to remember only that when these methods are invoked with endpoint
    and boolean arguments, the boolean always means "is inclusive?"
</p>
<p>
    unless specifically indicated by
    a boolean argument, a <mark>subset's starting point will always be inclusive</mark>.
    
</p>


<p>
    The older methods return either a
    SortedSet or a SortedMap, the new Java 6 methods return either a NavigableSet or a
    NavigableMap.
</p>

<pre>
headSet(e, b*) -Returns a subset ending at element e and exclusive of e
headMap(k, b*) -Returns a submap ending at key k and exclusive of key k
tailSet(e, b*) -Returns a subset starting at and inclusive of element e
tailMap(k, b*) -Returns a submap starting at and inclusive of key k
subSet(s, b*, e, b*) -Returns a subset starting at element s and ending just before element e
subMap(s, b*, e, b*) -Returns a submap starting at key s and ending just before key s
    
    
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
    
import java.util.SortedSet;
import java.util.TreeSet;
    
    
public class BackedCollectionTest {
    
    
    public static void main(String[] args) {
        TreeSet&lt;String&gt; times1 = new TreeSet&lt;String&gt;();
        times1.add("viv");
        times1.add("manj");
        times1.add("maad");
            
        <mark>SortedSet&lt;String&gt; subset= times1.subSet(&amp;quot;manj&amp;quot;, &amp;quot;viv&amp;quot;);</mark>
            
        System.out.println(times1 + " " + subset);
        times1.pollFirst();
        System.out.println(times1 + " " + subset);
        times1.pollFirst();
        System.out.println(times1 + " " + subset);
            
    }
        
}
    
</pre>
<pre class='out'>
run:
[maad, manj, viv] [manj]
<mark>[manj, viv] [manj]
[viv] []</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>
    Let's say that you've created a backed collection using either a tailXxx()
    or subXxx() method. Typically in these cases the original and copy collections have
    different "first" elements. For the exam it's important that you remember that the
    pollFirstXxx() methods will always remove the first entry from the collection on which
    they're invoked, but they will remove an element from the other collection only if it has
    same value. So it's most likely that <mark>invoking pollFirstXxx() on the copy will remove an
    entry from both collections, but invoking pollFirstXxx() on the original will remove only
    the entry from the original.</mark>
    
</p>


<h3>Using the PriorityQueue Class</h3>

<p>Unlike basic queue structures that are first-in, first-out by default, a PriorityQueue
    <mark>orders its elements using a user-defined priority</mark>.</p>
<p>
    The priority can be as simple as
    <mark>natural ordering</mark> (in which, for instance, an entry of <mark>1 would be a higher priority
    than an entry of 2</mark>)
</p>
<p>
    PriorityQueue <mark>can be ordered using a Comparator</mark>,
    which lets you define any ordering you want.
    
</p>
<p>
    Queues have a few methods not found
    in other collection interfaces: <mark>peek(), poll(), and offer()</mark>.
    
</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.navigablesetmap;
    
import java.util.*;
    
class PQ {
    
    static class PQsort
            implements Comparator&lt;Integer&gt; { // inverse sort
        public int compare(Integer one, Integer two) {
            return two - one; // unboxing
        }
    }
        
    public static void main(String[] args) {
        // TODO code application logic here
        int[] ia = {1, 5, 3, 7, 6, 9, 8}; // unordered data
        PriorityQueue&lt;Integer&gt; pq1 =
                new PriorityQueue&lt;Integer&gt;(); <mark>// use natural order</mark>
        for (int x : ia) {
            pq1.offer(x);<mark>// add</mark>
        }
        for (int x : ia) // review queue
        {
            System.out.print(pq1.poll() + " "); <mark>// highest and remove</mark>
        }
        System.out.println("");
        PQsort pqs = new PQsort(); // get a Comparator
        PriorityQueue&lt;Integer&gt; pq2 = new PriorityQueue&lt;Integer&gt;(10, pqs); // use Comparator
        for (int x : ia) // load queue
        {
            pq2.offer(x);
        }
        System.out.println("size " + pq2.size());
        System.out.println("peek " + pq2.peek());
        System.out.println("size " + pq2.size());
        System.out.println("poll " + pq2.poll());
        System.out.println("size " + pq2.size());
        for (int x : ia) // review queue
        {
            System.out.print(pq2.poll() + " ");
        }
    }
}
    
    
</pre>
<pre class='out'>
run:
1 3 5 6 7 8 9 
size 7
peek 9
size 7
poll 9
size 6
8 7 6 5 3 1 null BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<p>
    uses the offer() method to <mark>add elements to the PriorityQueue</mark> named pq1
</p>
<p>
    second for loop iterates through pq1 using the poll() method, which<mark> returns
    the highest priority entry in pq1 AND removes the entry from the queue</mark>.
    
</p>
<p>
    peek() returns
    the <mark>highest priority element</mark> in the queue <mark>without removing it</mark>,
    
</p>
<p>
    poll() returns
    the <mark>highest priority element, AND removes it</mark> from the queue.
</p>

<p>
    If you remember that <mark>spaces sort before characters</mark> and that <mark>uppercase
    letters sort before lowercase characters</mark>, you should be good to go for the exam.
    
</p>
<h3>Things to Know</h3>
<h3>Generic Class</h3>

<pre>
    package com.mvivekweb.ocpjp6.obj6.sect4.generics;
        
        
    public class ValidGen<mark>01&lt;E&gt;</mark> {
    public static void main(String[] args) {
        ValidGen01&lt;String&gt; v1= new ValidGen01&lt;String&gt;();
        ValidGen01&lt;Integer&gt; v2= new ValidGen01&lt;Integer&gt;();
                    }
                    }
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen<mark>01&lt;E extends vA&gt;</mark> {
    public static void main(String[] args) {
        ValidGen01&lt;vA&gt; v1= new ValidGen01&lt;vA&gt;();
        ValidGen01&lt;vB&gt; v2= new ValidGen01&lt;vB&gt;();
    }
}
    
class vA{
    
}
class vB extends vA{
    
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen01.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen01<mark>&lt;E extends vB&gt;</mark> {
    public static void main(String[] args) {
        <mark>ValidGen01&lt;vA&gt; v1= new ValidGen01&lt;vA&gt;();</mark>
        ValidGen01&lt;vB&gt; v2= new ValidGen01&lt;vB&gt;();
    }
}
    
class vA{
    
}
class vB extends vA{
    
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen01.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen01.java:8: error: type argument vA is not within bounds of type-variable E
        ValidGen01&lt;vA&gt; v1= new ValidGen01&lt;vA&gt;();
                   ^
  where E is a type-variable:
    E extends vB declared in class ValidGen01
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen01.java:8: error: type argument vA is not within bounds of type-variable E
        ValidGen01&lt;vA&gt; v1= new ValidGen01&lt;vA&gt;();
                                          ^
  where E is a type-variable:
    E extends vB declared in class ValidGen01
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen01<mark>&lt;? extends vA&gt;</mark> {
    public static void main(String[] args) {
        ValidGen01&lt;vA&gt; v1= new ValidGen01&lt;vA&gt;();
        ValidGen01&lt;vB&gt; v2= new ValidGen01&lt;vB&gt;();
    }
}
    
class vA{
    
}
class vB extends vA{
    
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen01.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen01.java:6: error: &lt;identifier&gt; expected
public class ValidGen01&lt;? extends vA&gt; {
                        ^
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen01.java:6: error: '{' expected
public class ValidGen01&lt;? extends vA&gt; {
                                    ^
2 errors
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
class vivektest02 {
    
}
interface Hungry<mark>&lt;E&gt;</mark> { void munch(<mark>E</mark> x); }
interface Carnivore<mark>&lt;E extends Animal1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
interface Herbivore<mark>&lt;E extends Wolf1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
abstract class Plant {}
class Grass extends Plant {}
abstract class Animal1 {}
    
class Wolf1 extends Animal1 implements Herbivore<mark>&lt;Wolf1&gt;</mark> {
public void munch(<mark>Wolf1</mark> x) {}
}
class Sheep1 extends Wolf1 implements Carnivore<mark>&lt;Sheep1&gt;</mark> {
public void munch(<mark>Sheep1</mark> x) {}
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/vivektest02.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\vivektest02.java:25: error: <mark>Hungry cannot be inherited with different arguments: &lt;com.mvivekweb.ocpjp6.obj6.sect4.generics.Sheep1&gt; and &lt;com.mvivekweb.ocpjp6.obj6.sect4.generics.Wolf1&gt;</mark>
class Sheep1 extends Wolf1 implements Carnivore&lt;Sheep1&gt; {
^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<img src="/imag/jp6/genex1.png" class="imgw"> 

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
class vivektest02 {
    
}
interface Hungry<mark>&lt;E&gt;</mark> { void munch(<mark>E</mark> x); }
interface Carnivore<mark>&lt;E extends Animal1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
interface Herbivore<mark>&lt;E extends Wolf1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
abstract class Plant {}
class Grass extends Plant {}
abstract class Animal1 {}
    
class Wolf1 extends Animal1 implements Herbivore&lt;Sheep1&gt; {
public void munch(Sheep1 x) {}
}
class Sheep1 extends Wolf1 implements Carnivore&lt;Sheep1&gt; {
public void munch(Sheep1 x) {}
}
</pre>

<pre>
class vivektest02 {
    
}
interface Hungry<mark>&lt;E&gt;</mark> { void munch(<mark>E</mark> x); }
interface Carnivore<mark>&lt;E extends Animal1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
interface Herbivore<mark>&lt;E extends Wolf1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
abstract class Plant {}
class Grass extends Plant {}
abstract class Animal1 {}
    
class Wolf1 extends Sheep1 implements Herbivore&lt;Wolf1&gt; {
    
    
}
class Sheep1 extends Animal1 implements Carnivore&lt;Wolf1&gt; {
public void munch(Wolf1 x) {}
}
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
class vivektest02 {
    
}
interface Hungry<mark>&lt;E&gt;</mark> { void munch(<mark>E</mark> x); }
interface Carnivore<mark>&lt;E extends Animal1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
interface Herbivore<mark>&lt;E extends Wolf1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
abstract class Plant {}
class Grass extends Plant {}
abstract class Animal1 {}
    
class Wolf1 <mark>extends Sheep1</mark> implements Herbivore<mark>&lt;Wolf1&gt;</mark> {
public void munch(Sheep1 x) {}
    
}
class Sheep1 extends Animal1 implements Carnivore<mark>&lt;Wolf1&gt;</mark> {
<mark>public void munch(Wolf1 x) {}</mark>
}
</pre>

<img src="/imag/jp6/genex2.png" class="imgw"> 

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
class vivektest02 {
    
}
interface Hungry<mark>&lt;E&gt;</mark> { void munch(<mark>E</mark> x); }
interface Carnivore<mark>&lt;E extends Animal1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
interface Herbivore<mark>&lt;E extends Wolf1&gt;</mark> extends Hungry<mark>&lt;E&gt;</mark> {}
abstract class Plant {}
class Grass extends Plant {}
abstract class Animal1 {}
    
class Wolf1 extends Animal1 implements Herbivore<mark>&lt;Wolf1&gt;</mark> {
public void munch(Wolf1 x) {}
    
}
class Sheep1 extends Animal1 implements Carnivore<mark>&lt;Wolf1&gt;</mark> {
    
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/vivektest02.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\vivektest02.java:19: error: Sheep1 is not abstract and does not override abstract method munch(Wolf1) in Hungry
class Sheep1 extends Animal1 implements Carnivore<mark>&lt;Wolf1&gt;</mark> {
^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<img src="/imag/jp6/genex3.png" class="imgw"> 

<h3>Generic Variable</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen02&lt;T&gt; {
    
    private <mark>T</mark> check;
    public static void main(String[] args) {
        ValidGen02&lt;String&gt; vs = new ValidGen02&lt;String&gt;();
        vs.check="vivek";
        System.out.println("vs="+vs.check);
        ValidGen02&lt;Integer&gt; vs1 = new ValidGen02&lt;Integer&gt;();
        vs1.check=3;
        System.out.println("vs1="+vs1.check);
    }
        
}
</pre>
<pre class='out'>
run:
vs=vivek
vs1=3
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
    
public class ValidGen02&lt;T&gt; {
    
    <mark>List&lt;T&gt;</mark> check;
    public static void main(String[] args) {
        ValidGen02&lt;String&gt; vs = new ValidGen02&lt;String&gt;();
        vs.check=new ArrayList&lt;String&gt;();
        System.out.println("vs="+vs.check);
        ValidGen02&lt;Integer&gt; vs1 = new ValidGen02&lt;Integer&gt;();
        vs1.check=new ArrayList&lt;Integer&gt;();
        System.out.println("vs1="+vs1.check);
    }
        
}
class A{
    
}</pre>
<pre class='out'>
run:
vs=[]
vs1=[]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>



<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
    
public class ValidGen02&lt;T&gt; {
    
    static List&lt;T&gt; check;
    T[] arr;
    A newv;
    public static void main(String[] args) {

    }
        
}
class A{
    
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen02.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen02.java:11: error: non-static type variable T cannot be referenced from a static context
    static List&lt;T&gt; check;
                ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<h3>Generic Method/Constructor</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen03&lt;T&gt; {
private T t;          
    
    public void add(T t) {
        this.t = t;
    }
        
    public T get() {
        return t;
    }
        
    public <mark>&lt;U&gt;</mark> void inspect(U u){
        System.out.println("T: " + t.getClass().getName());
        System.out.println("U: " + u.getClass().getName());
    }
        
        
    public static void main(String[] args) {
        ValidGen03&lt;Integer&gt; integerBox = new ValidGen03&lt;Integer&gt;();
        integerBox.add(new Integer(10));
        integerBox.inspect("some text");
            
    }
        
}
</pre>

<pre class='out'>
run:
T: java.lang.Integer
U: java.lang.String
BUILD SUCCESSFUL (total time: 1 second)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen03&lt;T&gt; {
private <mark>T t;</mark>          
    
    public void add(T t) {
        this.t = t;
    }
        
    public T get() {
        return t;
    }
        
    public <mark>&lt;T&gt; void inspect(T u){</mark>
        System.out.println("T: " + t.getClass().getName());
        System.out.println("T: " + u.getClass().getName());
    }
        
        
    public static void main(String[] args) {
        ValidGen03&lt;Integer&gt; integerBox = new ValidGen03&lt;Integer&gt;();
        integerBox.add(new Integer(10));
        integerBox.inspect("some text");
            
    }
        
}
</pre>
<pre class='out'>
run:
T: java.lang.Integer
T: java.lang.String
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen03&lt;T&gt; {
private <mark>T t</mark>;          
    
    public void add(T t) {
        this.t = t;
    }
        
    public T get() {
        return t;
    }
        
    public &lt;T&gt; void inspect(<mark>T t</mark>){
        System.out.println("T: " + t.getClass().getName());
        System.out.println("T: " + t.getClass().getName());
    }
        
        
    public static void main(String[] args) {
        ValidGen03&lt;Integer&gt; integerBox = new ValidGen03&lt;Integer&gt;();
        integerBox.add(new Integer(10));
        integerBox.inspect("some text");
            
    }
        
}
</pre>

<pre class='out'>run:
<mark>T: java.lang.String
T: java.lang.String</mark>
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen04&lt;T extends A1&gt; {
    
    
    public static void main(String[] args) {
        ValidGen04&lt;A1&gt; news= new ValidGen04&lt;A1&gt;();
    }
    <mark>&lt;T extends A1&gt;</mark> ValidGen04() {
        System.out.println("cons");
    }
        
}
class A1{
    
}
class B1 extends A1{
    
}</pre>

<pre class='out'>run:
cons
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
    
public class ValidGen04&lt;T extends B1&gt; {
    
    
    public static void main(String[] args) {
        ValidGen04&lt;B1&gt; news= new ValidGen04&lt;B1&gt;();
    }
    <mark>&lt;T extends B1&gt;</mark> ValidGen04() {
        System.out.println("cons");
    }
        
}
class A1{
    
}
class B1 extends A1{
    
}</pre>
<pre class='out'>
run:
cons
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        news.inspect(10);
         news.inspect("12");
    }
        
    public &lt;T&gt; T inspect(T u) {
        System.out.println("u="+u);
        return u;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
</pre>
<pre class='out'>
run:
u=10
u=12
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        news.inspect(a2);
        B2 b2 = new B2();
         news.inspect(b2);
    }
        
   public <mark>&lt;T extends A2&gt;</mark> T inspect(T u) {
        System.out.println("u="+u);
        return u;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
    
</pre>
<pre class='out'>
run:
u=com.mvivekweb.ocpjp6.obj6.sect4.generics.A2@15db9742
u=com.mvivekweb.ocpjp6.obj6.sect4.generics.B2@6d06d69c
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        news.inspect(a2);
        B2 b2 = new B2();
         news.inspect(b2);
    }
        
    public <mark>&lt;? extends A2&gt;</mark> T inspect(T u) {
        System.out.println("u="+u);
        return u;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen05.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: &lt;identifier&gt; expected
    public &lt;? extends A2&gt; T inspect(T u) {
            ^
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: illegal start of type
    public &lt;? extends A2&gt; T inspect(T u) {
              ^
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: '(' expected
    public &lt;? extends A2&gt; T inspect(T u) {
                        ^
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        news.inspect(a2);
        B2 b2 = new B2();
         news.inspect(b2);
    }
        
    public <mark>&lt;? super B2&gt;</mark> T inspect(T u) {
        System.out.println("u="+u);
        return u;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect4/generics/ValidGen05.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: &lt;identifier&gt; expected
    public &lt;? super B2&gt; T inspect(T u) {
            ^
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: illegal start of type
    public &lt;? super B2&gt; T inspect(T u) {
              ^
com\mvivekweb\ocpjp6\obj6\sect4\generics\ValidGen05.java:13: error: '(' expected
    public &lt;? super B2&gt; T inspect(T u) {
                      ^
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<pre>package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        ValidGen05&lt;? extends A2&gt; val=news.inspect(a2);
            
    }
        
    public &lt;U&gt; ValidGen05<mark>&lt;? extends A2&gt;</mark> inspect(U u) {
        System.out.println("u="+u);
        ValidGen05&lt;B2&gt; news1 = new ValidGen05&lt;B2&gt;();
        return news1;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
    
</pre>
<pre class='out'>
run:
u=com.mvivekweb.ocpjp6.obj6.sect4.generics.A2@15db9742
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
import java.util.ArrayList;
import java.util.List;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        <mark>ValidGen05&lt;? extends A2&gt;</mark> val=news.inspect(a2);
            
    }
        
   public &lt;U&gt; <mark>ValidGen05&lt;? super B2&gt;</mark> inspect(U u) {
        System.out.println("u="+u);
        ValidGen05&lt;B2&gt; news1 = new ValidGen05&lt;B2&gt;();
        return news1;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
    
</pre>
<pre class='out'>
run:
u=com.mvivekweb.ocpjp6.obj6.sect4.generics.A2@15db9742
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect4.generics;
    
import java.util.ArrayList;
import java.util.List;
    
public class ValidGen05&lt;T extends A2&gt; {
    
    public static void main(String[] args) {
        ValidGen05&lt;B2&gt; news = new ValidGen05&lt;B2&gt;();
        A2 a2 = new A2();
        ValidGen05&lt;? extends A2&gt; val=news.inspect(a2);
            
    }
        
    public &lt;U&gt; ValidGen05<mark>&lt;?&gt;</mark> inspect(U u) {
        System.out.println("u="+u);
        ValidGen05&lt;B2&gt; news1 = new ValidGen05&lt;B2&gt;();
        return news1;
    }
}
    
class A2 {
    
}
    
class B2 extends A2 {
    
}
    
</pre>
<pre class='out'>
run:
u=com.mvivekweb.ocpjp6.obj6.sect4.generics.A2@15db9742
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

