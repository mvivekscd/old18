<h3>Given a set of classes and superclasses, develop constructors for one or more of the
            classes. Given a class declaration, determine if a default constructor will be created, and
            if so, determine the behavior of that constructor. Given a nested or nonnested class listing,
            write code to instantiate the class.  </h3>
        
        
        <p>Objects are constructed</p>
        
        <p> can't make a new object without invoking a
            constructor.</p>
        
        <p> you can't make a new object without invoking not just the
            constructor of the object's actual class type, <mark>but also the constructor of each of its
            superclasses</mark>!</p>
        
        
            <p> Constructors are the code that runs whenever you use the keyword
                <mark>new</mark>. </p>

            <h3>Constructor Basics  </h3>
            
 
            <p> Every class, including <mark>abstract classes, MUST have a constructor</mark>   </p>
            
            <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class Foo {
   <mark> Foo(){}</mark>
}           
            </pre>   
            
            
            <p>Two key points to remember about
                constructors are that <mark>they have no return type</mark> and their names <mark>must exactly match
                the class name</mark>. </p>
            
            <p>constructors are used to <mark>initialize instance variable state</mark></p>
            <pre>           
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Foo1 {
    
<mark>    int size;
    String name;</mark>
        
    Foo1(String name, int size) {
        <mark>this.name = name;
        this.size = size;</mark>
    }
}
            </pre>        
            
            <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class TestConstructor {
    
    
    public static void main(String[] args) {
        Foo1 foo= <mark>new Foo1();</mark>
    }
        
}
    
            </pre>  
            
            <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/constructor/TestConstructor.java
com\mvivekweb\ocpjp6\obj1\sect6\constructor\TestConstructor.java:10: <mark>cannot find symbol</mark>
symbol  : <mark>constructor Foo1()</mark>
location: class com.mvivekweb.ocpjp6.obj1.sect6.constructor.Foo1
        Foo1 foo= new Foo1();
                  ^
1 error
    
C:\vivek\docs\cert\ocpjp6\src>            
    
            </pre>
            
            
            <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class TestConstructor {
    
    
    public static void main(String[] args) {
       <mark> Foo1 foo= new Foo1("vive", 4);</mark>
    }
        
}
    
            </pre>
        
            <p>occasionally you have a class where it <mark>makes no sense to create an instance without
                supplying information to the constructor</mark>.</p>
            
            
                <h3>Constructor Chaining  </h3>
                
                
                <p>Every constructor <mark>invokes the constructor
                    of its superclass</mark> with an (implicit) call to <mark>super()</mark>, <mark>unless the constructor
                    invokes an overloaded constructor of the same class</mark>  </p>              
        
        
 
                <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class TestConstructor1 {
    
    
    public static void main(String[] args) {
       Manju v= new Manju();
        Manju v1= new Manju("Maanvi");
    }
        
}
class Vivek{
    Vivek(){
        System.out.println("vivek def");
    }
    Vivek(String v){
        System.out.println("vivek String");
    }
}
class Manju extends Vivek{
    Manju(){
        System.out.println("manju");
    }
    Manju(String v){
        System.out.println("manju String");
    }
}       
    
                </pre>
   <pre class='out'>             
 run:
vivek def
manju
<mark>vivek def</mark>
manju String
BUILD SUCCESSFUL (total time: 1 second)               
    </pre> 
        
        

                <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class TestConstructor1 {
    
    
    public static void main(String[] args) {
       Manju v= new Manju();
        Manju v1= new Manju("Maanvi");
    }
        
}
class Vivek{
    Vivek(){
        System.out.println("vivek def");
    }
    Vivek(String v){
        System.out.println("vivek String");
    }
}
class Manju extends Vivek{
    Manju(){
        System.out.println("manju");
    }
    Manju(String v){
        <mark>super(v);</mark>
        System.out.println("manju String");
    }
}        
    
                </pre>
  <pre class='out'>              
run:
vivek def
manju
vivek String
manju String
BUILD SUCCESSFUL (total time: 1 second)                
                
    </pre>
                
                
                
                <p>Object constructor is invoked    </p>            
                
                
                <h3>Rules for Constructors  </h3>
                
                <p>Constructors <mark>can use any access modifier</mark>, including <mark>private</mark>. </p>               
                
                <p>The constructor name must match the name of the class. </p>
                
                <p>Constructors must <mark>not have a return type</mark>.</p>
                
                <p>It's legal (but stupid) to have a method with the same name as the class,
                    but that doesn't make it a constructor. I<mark>f you see a return type, it's a method
                    rather than a constructor.</mark> </p>               
                
                    <p>you could have both a <mark>method and a
                        constructor</mark> with the <mark>same name</mark> </p>               
                
                    <p>If you don't type a constructor into your class code, <mark>a default constructor will
                        be automatically generated</mark> by the compiler.</p>
                    
                    <p>The <mark>default constructor is ALWAYS a no-arg constructor</mark>. </p>                   
                
                    <p>if you've typed in a <mark>constructor
                        with arguments</mark>, you <mark>won't have a no-arg constructor unless you type it in</mark>
                        yourself! </p>               
                
                    <p>Every constructor has, as its first statement, either <mark>a call to an overloaded
                        constructor (this())</mark> or <mark>a call to the superclass constructor (super())</mark>, although
                        remember that this call can be inserted by the compiler.</p>
                    
                    <pre>             
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class ThiscallConstructor {
    
    
    public static void main(String[] args) {
        Maanvi mv = new Maanvi();
    }
        
}
class Maanvi{
    Maanvi(){
        <mark>this("vivek");</mark>
    }
    Maanvi(String v){
        System.out.println("v="+v );
    }
}
                    </pre>
 <pre class='out'>                   
run:
v=vivek
BUILD SUCCESSFUL (total time: 1 second)                    
    </pre>            
                    
                   
 <pre>                 
     
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class ThiscallConstructor {
    
    
    public static void main(String[] args) {
        Maanvi mv = new Maanvi();
    }
        
}
class Maanvi{
    Maanvi(){
        System.out.println("this.toString()="+<mark>this.toString()</mark>);
    }
}               
 </pre>
 <pre class='out'>                   
run:
this.toString()=<mark>com.mvivekweb.ocpjp6.obj1.sect6.constructor.Maanvi@43f143f1</mark>
BUILD SUCCESSFUL (total time: 1 second)                    
    </pre>         
                    <p>               
                        If you do type in a constructor (as opposed to relying on the compiler-generated
                        default constructor), and you do not type in the call to super() or a call
                        to this(), <mark>the compiler will insert a no-arg call to super() for you</mark>, as the very
                        first statement in the constructor.               
                    </p>
                
                
                    <p>A call to super() can be either a no-arg call <mark>or can include arguments passed
                        to the super constructor.</mark>  </p>
                    
                    <pre>
    
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class TestConstructor1 {
    
    
    public static void main(String[] args) {
      // Manju v= new Manju();
        Manju v1= new Manju("Maanvi");
    }
        
}
class Vivek{
    
    Vivek(String v){
        System.out.println("vivek String");
    }
}
class Manju extends Vivek{
    
    <mark>Manju(String v){</mark>
        System.out.println("manju String");
    }
}
    
                    </pre>           
 <pre class='out'>                   
 run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - Erroneous sym type: com.mvivekweb.ocpjp6.obj1.sect6.constructor.Vivek.<init>
	at com.mvivekweb.ocpjp6.obj1.sect6.constructor.Manju.<init>(TestConstructor1.java:23)
	at com.mvivekweb.ocpjp6.obj1.sect6.constructor.TestConstructor1.main(TestConstructor1.java:11)
Java Result: 1
BUILD SUCCESSFUL (total time: 0 seconds)                   
</pre>           
                    
                    <p>                   
                        The default constructor is the one the compiler provides! While the default
                        constructor is always a no-arg constructor, you're free to put in your own noarg
                        constructor.                    
                    </p>
                    
                    
                    <p>
                        You <mark>cannot make a call to an instance method</mark>, or <mark>access an instance variable</mark>,
                        <mark>until after the super constructor runs</mark>.                   
    
                    </p>
                    
                    
                    <p>Only <mark>static variables and methods</mark> can be <mark>accessed</mark> as part of the <mark>call to super()
                        or this()</mark>.   </p>    
                    
                    
                    <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class callStaticfromSuper {
    
    
    public static void main(String[] args) {
        Vivek1 ne= new Vivek1();
    }
        
}
class Vivek1 extends Manju1{
    <mark>static</mark> int v=10;
    Vivek1(){
        <mark>super(Vivek1.v);</mark>
    }
        
}
    
class Manju1{
    <mark>Manju1(int v1){</mark>
        System.out.println("v1="+v1);        
    }
}
                    </pre> 
  <pre class='out'>                  
 run:
v1=10
BUILD SUCCESSFUL (total time: 1 second)
    </pre>            
                    
                    
                    <p><mark>Abstract classes</mark> have constructors, and <mark>those constructors are always called
                        when a concrete subclass is instantiated</mark>.  </p>                
                    
                    <p>Interfaces do not have constructors </p>
                    
                    <p>The <mark>only way</mark> a constructor can be invoked <mark>is from within another constructor</mark>.  </p>                  
                    
                    <pre>                 
 package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
     
public class Horse {
    
    Horse() {
    } // constructor
    void doStuff() {
        <mark>Horse();</mark> // calling the constructor - illegal!
    }
}
                    </pre>   
                    
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/constructor/Horse.java
com\mvivekweb\ocpjp6\obj1\sect6\constructor\Horse.java:8: cannot find symbol
symbol  : method Horse()
location: class com.mvivekweb.ocpjp6.obj1.sect6.constructor.Horse
        Horse(); // calling the constructor - illegal!
        ^
1 error

C:\vivek\docs\cert\ocpjp6\src>                    
                    
    </pre>    
                
 
                    
                    <pre>
    
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
    
public class callStaticfromSuper {
    
    
    public static void main(String[] args) {
        Vivek1 ne= new Vivek1();
    }
        
}
class Vivek1 extends Manju1{
    static int v=10;
    Vivek1(){
        super(Vivek1.v);
        <mark>this.Manju1();</mark>
            
    }
    void Manju1(){
        System.out.println("method");        
    }
        
}
    
class Manju1{
    Manju1(int v1){
        System.out.println("v1="+v1);        
    }
}                    
                    </pre>
                    
 <pre class='out'>                   
run:
v1=10
method
BUILD SUCCESSFUL (total time: 1 second)                    
    </pre>     
                    
                    
                    
                    
        <h3>Determine Whether a Default Constructor Will Be Created </h3>
                    
        <p> The default constructor has the <mark>same access modifier as the class</mark> </p>
        
        <p>The default constructor <mark>has no arguments</mark>.</p>
        
        <p>The default constructor includes a <mark>no-arg call to the super constructor</mark> (<mark>super()</mark>). </p>
        
        <pre>      
 package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
     
public class defaultconstructor {
    
    
    
    <mark>protected</mark> defaultconstructor() {
        System.out.println("cons");
    }
        
    public void defaultconstructor() {
        System.out.println("cons1");
    }
        
        
    public static void main(String args[]) {
        defaultconstructor def = new defaultconstructor();
    }
}       
        </pre>   
        
 <pre class='out'>       
run:
<mark>cons</mark>
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>
                    
        <pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class defaultconstructor {
    
    
    
<mark>    protected defaultconstructor() {</mark>
        System.out.println("cons");
    }
        
   <mark> public defaultconstructor() {</mark>
        System.out.println("cons1");
    }
        
        
    public static void main(String args[]) {
        defaultconstructor def = new defaultconstructor();
    }
}
        </pre>
        
 <pre class='out'>       
run:
Exception in thread "main" java.lang.ExceptionInInitializerError
	at java.lang.J9VMInternals.initialize(J9VMInternals.java:222)
Caused by: java.lang.RuntimeException: Uncompilable source code - constructor defaultconstructor() is already defined in class com.mvivekweb.ocpjp6.obj1.sect6.constructor.defaultconstructor
	at com.mvivekweb.ocpjp6.obj1.sect6.constructor.defaultconstructor.<clinit>(defaultconstructor.java:11)
	at java.lang.J9VMInternals.initializeImpl(Native Method)
	at java.lang.J9VMInternals.initialize(J9VMInternals.java:200)
Java Result: 1
BUILD SUCCESSFUL (total time: 0 seconds)        
 </pre>
        
 
        <pre>      
 public class Foo { }       
        </pre>
        
        <pre>       
public class Foo {
<mark>public Foo() {
super();
}</mark>
}        
        </pre>   
        
        <pre>       
 class Foo {
void Foo() { }
}
    
        </pre>
        
        <pre>       
class Foo {
void Foo() { }
<mark>Foo() {
super();
}</mark>
}        
    
        </pre>   
        
        
        <p>if your super constructor (that is, the constructor of your immediate
            superclass/parent) has arguments, you must type in the call to super(),<mark> supplying
            the appropriate arguments</mark>.  </p>
        
        
if your superclass does not have a no-arg 
        
        
<p>if your superclass does not have a no-arg
    constructor, then in <mark>your subclass you will not be able to use the default constructor</mark>
    supplied by the compiler</p>

<pre>
 package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
     
     
public class Clothing {
    <mark>Clothing(String s) { }</mark>
}
class TShirt extends Clothing {<mark> </mark>}       
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/constructor/Clothing.java
com\mvivekweb\ocpjp6\obj1\sect6\constructor\Clothing.java:8:<mark> cannot find symbol</mark>
symbol  : constructor Clothing()
location: class com.mvivekweb.ocpjp6.obj1.sect6.constructor.Clothing
class TShirt extends Clothing { }
^
1 error

C:\vivek\docs\cert\ocpjp6\src>
    </pre> 
        
        
<p>constructors
    <mark>are never inherited </mark>   </p>    

<h3>
    Constructor Declarations</h3>

<p>
    a constructor <mark>can't ever, ever, ever, have a return type</mark>... ever
</p>

<p>Constructor declarations can however have all of the normal access modifiers, and
    they <mark>can take arguments (including var-args</mark>), just like methods.</p>


<p>Constructors <mark>can't be marked static</mark> (they
    are after all associated with object instantiation)</p>


<p>they c<mark>an't be marked final</mark>
    or <mark>abstract</mark> (because they can't be overridden).</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.constructor;
    
public class Foo2 {
    
    Foo2() {
    }
        
    <mark>private</mark> Foo2(byte b) {
    }
        
    Foo2(int x) {
    }
        
    Foo2(int x, <mark>int... y</mark>) {
    }
}
    
</pre>


