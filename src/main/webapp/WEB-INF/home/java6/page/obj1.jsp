
        <h3>Declaration Rules (Objective 1.1)</h3>
        <p>A source code file can have only one public class.</p>
        <p>If the source file contains a public class, the filename must match the
            public class name.</p>
        <p> A file can have only one package statement, but multiple imports.</p>
        <p> The package statement (if any) must be the first (non-comment) line in a
            source file.</p>
        <p> The import statements (if any) must come after the package and before
            the class declaration.</p>
        <p> If there is no package statement, import statements must be the first (noncomment)
            statements in the source file.</p>
        <p> package and import statements apply to all classes in the file.</p>
        <p> A file can have more than one nonpublic class.</p>
        <p> Files with no public classes have no naming restrictions.</p>
         <h3>Class Access Modifiers (Objective 1.1)</h3>
         <p> There are three access modifiers: public, protected, and private.</p>
         <p> There are four access levels: public, protected, default, and private.</p>
         <p>Classes can have only public or default access.</p>
         <p>A class with default access can be seen only by classes within the same package.</p>
         <p>A class with public access can be seen by all classes from all packages.</p>
         <p> Class visibility revolves around whether code in one class can
             <br> Create an instance of another class
              <br> Extend (or subclass), another class
               <br>Access methods and variables of another class</p>
         
         <h3>Inner Classes</h3>
         <p> A "regular" inner class is declared inside the curly braces of another class, but
             outside any method or other code block.</p>
         <p> An inner class is a full-fledged member of the enclosing (outer) class, so it
             can be marked with an access modifier as well as the abstract or final
             modifiers. (Never both abstract and final together? remember that
             abstract must be subclassed, whereas final cannot be subclassed).</p>
         <p> An inner class instance shares a special relationship with an instance of the
             enclosing class. This relationship gives the inner class access to all of the
             outer class's members, including those marked private.</p>
         <p> To instantiate an inner class, you must have a reference to an instance of the
             outer class.</p>
         <p> From code within the enclosing class, you can instantiate the inner class
             using only the name of the inner class, as follows:
             MyInner mi = new MyInner();</p>
         <p> From code outside the enclosing class's instance methods, you can
             instantiate the inner class only by using both the inner and outer class names,
             and a reference to the outer class as follows:
             MyOuter mo = new MyOuter();</p>
MyOuter.MyInner inner = mo.new MyInner();
<p> From code within the inner class, the keyword this holds a reference to
    the inner class instance. To reference the outer this (in other words, the
    instance of the outer class that this inner instance is tied to) precede the
    keyword this with the outer class name as follows: MyOuter.this;</p>
<h3>Method-Local Inner Classes</h3>
<p> A method-local inner class is defined within a method of the enclosing class.</p>
<p> For the inner class to be used, you must instantiate it, and that instantiation
    must happen within the same method, but after the class definition code.</p>
<p> A method-local inner class cannot use variables declared within the method
    (including parameters) unless those variables are marked final.</p>
<p> The only modifiers you can apply to a method-local inner class are abstract
    and final. (Never both at the same time, though.)</p>
<h3>Anonymous Inner Classes</h3>
<p> Anonymous inner classes have no name, and their type must be either a
    subclass of the named type or an implementer of the named interface.</p>
<p> An anonymous inner class is always created as part of a statement; don't
    forget to close the statement after the class definition with a curly brace. This
    is a rare case in Java, a curly brace followed by a semicolon.</p>
<p> Because of polymorphism, the only methods you can call on an anonymous
    inner class reference are those defined in the reference variable class (or
    interface), even though the anonymous class is really a subclass or implementer
    of the reference variable type.</p>
<p> An anonymous inner class can extend one subclass or implement one
    interface. Unlike non-anonymous classes (inner or otherwise), an anonymous
    inner class cannot do both. In other words, it cannot both extend a class and
    implement an interface, nor can it implement more than one interface.</p>
<p> An argument-defined inner class is declared, defined, and automatically
    instantiated as part of a method invocation. The key to remember is that the
    class is being defined within a method argument, so the syntax will end the
    class definition with a curly brace, followed by a closing parenthesis to end
    the method call, followed by a semicolon to end the statement: });</p>
<h3>Static Nested Classes</h3>
<p> Static nested classes are inner classes marked with the static modifier.</p>
<p> A static nested class is not an inner class, it's a top-level nested class.</p>
<p> Because the nested class is static, it does not share any special relationship
    with an instance of the outer class. In fact, you don't need an instance of the
    outer class to instantiate a static nested class.</p>
<p> Instantiating a static nested class requires using both the outer and nested
    class names as follows:
    BigOuter.Nested n = new BigOuter.Nested();</p>
<p> A static nested class cannot access non-static members of the outer class,
    since it does not have an implicit reference to any outer instance (in other
    words, the nested class instance does not get an outer this reference).  </p>       
         
         <h3>Class Modifiers (Nonaccess) (Objective 1.2)</h3>
         <p> Classes can also be modified with final, abstract, or strictfp.</p>
         <p> A class cannot be both final and abstract.</p>
         <p> A final class cannot be subclassed.</p>
         <p> An abstract class cannot be instantiated.</p>
         <p> A single abstract method in a class means the whole class must be abstract.</p>
         <p> An abstract class can have both abstract and nonabstract methods.</p>
         <p> The first concrete class to extend an abstract class must implement all of its
             abstract methods.</p>
<h3>Interface Implementation (Objective 1.2)</h3>
<p> Interfaces are contracts for what a class can do, but they say nothing about
    the way in which the class must do it.</p>
<p> Interfaces can be implemented by any class, from any inheritance tree.</p>
<p> An interface is like a 100-percent abstract class, and is implicitly abstract
    whether you type the abstract modifier in the declaration or not.</p>
<p> An interface can have only abstract methods, no concrete methods allowed.</p>
<p> Interface methods are by default public and abstract?explicit declaration
    of these modifiers is optional.</p>
<p> Interfaces can have constants, which are always implicitly public,
    static, and final.</p>
<p> Interface constant declarations of public, static, and final are optional
    in any combination.</p>
<p> A legal nonabstract implementing class has the following properties:</p>
<p> It provides concrete implementations for the interface's methods.</p>
<p> It must follow all legal override rules for the methods it implements.</p>
<p> It must not declare any new checked exceptions for an
    implementation method.</p>
<p> It must not declare any checked exceptions that are broader than
    the exceptions declared in the interface method.</p>
<p> It may declare runtime exceptions on any interface method
    implementation regardless of the interface declaration.</p>
<p> It must maintain the exact signature (allowing for covariant returns)
    and return type of the methods it implements (but does not have to
    declare the exceptions of the interface).</p>
<p> A class implementing an interface can itself be abstract.</p>
<p> An abstract implementing class does not have to implement the interface
    methods (but the first concrete subclass must).</p>
<p> A class can extend only one class (no multiple inheritance), but it can
    implement many interfaces.</p>
<p> Interfaces can extend one or more other interfaces.</p>
<p> Interfaces cannot extend a class, or implement a class or interface.</p>
<p> When taking the exam, verify that interface and class declarations are legal
    before verifying other code logic.</p>

<h3>Implementing an Interface (Objective 1.2)</h3>
<p> When you implement an interface, you are fulfilling its contract.</p>
<p> You implement an interface by properly and concretely overriding all of the
    methods defined by the interface.</p>
<p> A single class can implement many interfaces.</p>


<h3>Identifiers (Objective 1.3)</h3>
<p> Identifiers can begin with a letter, an underscore, or a currency character.</p>
<p> After the first character, identifiers can also include digits.</p>
<p> Identifiers can be of any length.</p>
<p> JavaBeans methods must be named using camelCase, and depending on the
    method's purpose, must start with set, get, is, add, or remove.</p>

<h3>Local Variables (Objective 1.3)</h3>
<p> Local (method, automatic, or stack) variable declarations cannot have
    access modifiers.</p>
<p> final is the only modifier available to local variables.</p>
<p> Local variables don't get default values, so they must be initialized before use.</p>
<h3>Other Modifiers?Members (Objective 1.3)</h3>
<p> final methods cannot be overridden in a subclass.</p>
<p> abstract methods are declared, with a signature, a return type, and
    an optional throws clause, but are not implemented.</p>
<p> abstract methods end in a semicolon?no curly braces.</p>
<p> Three ways to spot a non-abstract method:</p>
<p> The method is not marked abstract.
     The method has curly braces.
     The method has code between the curly braces.</p>
<p> The first nonabstract (concrete) class to extend an abstract class must
    implement all of the abstract class' abstract methods.</p>
<p> The synchronized modifier applies only to methods and code blocks.</p>
<p> synchronized methods can have any access control and can also be
    marked final.</p>
<p> abstract methods must be implemented by a subclass, so they must be
    inheritable. For that reason:</p>
<p> abstract methods cannot be private.
 abstract methods cannot be final.
 The native modifier applies only to methods.
 The strictfp modifier applies only to classes and methods.</p>

<h3>Variable Declarations (Objective 1.3)</h3>
 Instance variables can
 Have any access control
 Be marked final or transient
 Instance variables can't be abstract, synchronized, native, or strictfp.
<p> It is legal to declare a local variable with the same name as an instance
    variable; this is called "shadowing."</p>
<p> final variables have the following properties:</p>
<p> final variables cannot be reinitialized once assigned a value.</p>
<p> final reference variables cannot refer to a different object once the
    object has been assigned to the final variable.</p>
<p> final reference variables must be initialized before the constructor
    completes.</p>
<p> There is no such thing as a final object. An object reference marked final
    does not mean the object itself is immutable.</p>
<p> The transient modifier applies only to instance variables.</p>
<p> The volatile modifier applies only to instance variables.</p>

<h3>Array Declarations (Objective 1.3)</h3>
<p> Arrays can hold primitives or objects, but the array itself is always an object.</p>
<p> When you declare an array, the brackets can be to the left or right of the
    variable name.</p>
<p> It is never legal to include the size of an array in the declaration.</p>
<p> An array of objects can hold any object that passes the IS-A (or instanceof)
    test for the declared type of the array. For example, if Horse extends Animal,
    then a Horse object can go into an Animal array.</p>


<h3>Enums (Objective 1.3)</h3>
<p> An enum specifies a list of constant values assigned to a type.</p>
<p> An enum is NOT a String or an int; an enum constant's type is the enum
    type. For example, SUMMER and FALL are of the enum type Season.</p>
<p> An enum can be declared outside or inside a class, but NOT in a method.</p>
<p> An enum declared outside a class must NOT be marked static, final,
    abstract, protected, or private.</p>
<p> Enums can contain constructors, methods, variables, and constant class bodies.</p>
<p> enum constants can send arguments to the enum constructor, using the
    syntax BIG(8), where the int literal 8 is passed to the enum constructor.</p>
<p> enum constructors can have arguments, and can be overloaded.</p>
<p> enum constructors can NEVER be invoked directly in code. They are always
    called automatically when an enum is initialized.</p>
<p> The semicolon at the end of an enum declaration is optional. These are legal:
    enum Foo { ONE, TWO, THREE}
    enum Foo { ONE, TWO, THREE};</p>
<p> MyEnum.values() returns an array of MyEnum's values.</p>

<h3>Statics (Objective 1.3)</h3>
<p> Use static methods to implement behaviors that are not affected by the
    state of any instances.</p>
<p> Use static variables to hold data that is class specific as opposed to instance
    specific?there will be only one copy of a static variable.</p>
<p> All static members belong to the class, not to any instance.</p>
<p> A static method can't access an instance variable directly.</p>
<p> Use the dot operator to access static members, but remember that using a
    reference variable with the dot operator is really a syntax trick, and the compiler
    will substitute the class name for the reference variable, for instance:
    d.doStuff();
    becomes:
    Dog.doStuff();</p>
<p> static methods can't be overridden, but they can be redefined.</p>

<h3>Stack and Heap</h3>
<p> Local variables (method variables) live on the stack.</p>
<p> Objects and their instance variables live on the heap.</p>
<h3>Literals and Primitive Casting (Objective 1.3)</h3>
<p> Integer literals can be decimal, octal (e.g. 013), or hexadecimal (e.g. 0x3d).</p>
<p> Literals for longs end in L or l.</p>
<p> Float literals end in F or f, double literals end in a digit or D or d.</p>
<p> The boolean literals are true and false.</p>
<p> Literals for chars are a single character inside single quotes: 'd'.</p>

<h3>Scope (Objectives 1.3 and 7.6)</h3>
<p> Scope refers to the lifetime of a variable.</p>
<p> There are four basic scopes:</p>
<p> Static variables live basically as long as their class lives.</p>
<p> Instance variables live as long as their object lives.</p>
<p> Local variables live as long as their method is on the stack; however, if
    their method invokes another method, they are temporarily unavailable.</p>
<p> Block variables (e.g., in a for or an if) live until the block completes.</p>
<h3>Basic Assignments (Objectives 1.3 and 7.6)</h3>
<p> Literal integers are implicitly ints.</p>
<p> Integer expressions always result in an int-sized result, never smaller.</p>
<p> Floating-point numbers are implicitly doubles (64 bits).</p>
<p> Narrowing a primitive truncates the high order bits.</p>
<p> Compound assignments (e.g. +=), perform an automatic cast.</p>
<p> A reference variable holds the bits that are used to refer to an object.</p>
<p> Reference variables can refer to subclasses of the declared type but not to
    superclasses.</p>
<p> When creating a new object, e.g., Button b = new Button();, three
    things happen:</p>
<p> Make a reference variable named b, of type Button</p>
<p> Create a new Button object</p>
<p> Assign the Button object to the reference variable b</p>

<h3>Using a Variable or Array Element That Is Uninitialized and
    Unassigned (Objectives 1.3 and 7.6)</h3>
<p> When an array of objects is instantiated, objects within the array are not
    instantiated automatically, but all the references get the default value of null.</p>
<p> When an array of primitives is instantiated, elements get default values.</p>
<p> Instance variables are always initialized with a default value.</p>
<p> Local/automatic/method variables are never given a default value. If you
    attempt to use one before initializing it, you'll get a compiler error.</p>

<h3>Array Declaration, Construction, and Initialization (Obj. 1.3)</h3>
<p> Arrays can hold primitives or objects, but the array itself is always an object.</p>
<p> When you declare an array, the brackets can be left or right of the name.</p>
<p> It is never legal to include the size of an array in the declaration.</p>
<p> You must include the size of an array when you construct it (using new)
    unless you are creating an anonymous array.</p>
<p> Elements in an array of objects are not automatically created, although
    primitive array elements are given default values.</p>
<p> You'll get a NullPointerException if you try to use an array element in an
    object array, if that element does not refer to a real object.</p>

<p> Arrays are indexed beginning with zero.</p>
<p> An ArrayIndexOutOfBoundsException occurs if you use a bad index value.</p>
<p> Arrays have a length variable whose value is the number of array elements.</p>
<p> The last index you can access is always one less than the length of the array.</p>
<p> Multidimensional arrays are just arrays of arrays.</p>
<p> The dimensions in a multidimensional array can have different lengths.</p>
<p> An array of primitives can accept any value that can be promoted implicitly
    to the array's declared type;. e.g., a byte variable can go in an int array.</p>
<p> An array of objects can hold any object that passes the IS-A (or instanceof)
    test for the declared type of the array. For example, if Horse extends Animal,
    then a Horse object can go into an Animal array.</p>
<p> If you assign an array to a previously declared array reference, the array you're
    assigning must be the same dimension as the reference you're assigning it to.</p>
<p> You can assign an array of one type to a previously declared array reference of
    one of its supertypes. For example, a Honda array can be assigned to an array
    declared as type Car (assuming Honda extends Car).</p>
<h3>Initialization Blocks (Objectives 1.3 and 7.6)</h3>
<p> Static initialization blocks run once, when the class is first loaded.</p>
<p> Instance initialization blocks run every time a new instance is created. They
    run after all super-constructors and before the constructor's code has run.</p>
<p> If multiple init blocks exist in a class, they follow the rules stated above,
    AND they run in the order in which they appear in the source file.</p>


<h3>Member Access Modifiers (Objectives 1.3 and 1.4)</h3>
<p> Methods and instance (nonlocal) variables are known as "members."</p>
<p> Members can use all four access levels: public, protected, default, private.</p>
<p> Member access comes in two forms:</p>
<p> Code in one class can access a member of another class.</p>
<p> A subclass can inherit a member of its superclass.</p>
<p> If a class cannot be accessed, its members cannot be accessed.</p>
<p> Determine class visibility before determining member visibility.</p>
<p> public members can be accessed by all other classes, even in other packages.</p>
<p> If a superclass member is public, the subclass inherits it?regardless of package.</p>
<p> Members accessed without the dot operator (.) must belong to the same class.</p>
<p> this. always refers to the currently executing object.</p>
<p> this.aMethod() is the same as just invoking aMethod().</p>
<p> private members can be accessed only by code in the same class.</p>
<p> private members are not visible to subclasses, so private members cannot
    be inherited.</p>

<p> Default and protected members differ only when subclasses are involved:</p>
<p> Default members can be accessed only by classes in the same package.</p>
<p> protected members can be accessed by other classes in the same
    package, plus subclasses regardless of package.</p>
<p> protected = package plus kids (kids meaning subclasses).</p>
<p> For subclasses outside the package, the protected member can be
    accessed only through inheritance; a subclass outside the package cannot
    access a protected member by using a reference to a superclass instance
    (in other words, inheritance is the only mechanism for a subclass
    outside the package to access a protected member of its superclass).</p>
<p> A protected member inherited by a subclass from another package is
    not accessible to any other class in the subclass package, except for the
    subclass' own subclasses.</p>

<h3>Methods with var-args (Objective 1.4)</h3>
<p> As of Java 5, methods can declare a parameter that accepts from zero to
    many arguments, a so-called var-arg method.</p>
<p> A var-arg parameter is declared with the syntax type... name; for instance:
    doStuff(int... x) { }</p>
<p> A var-arg method can have only one var-arg parameter.</p>
<p> In methods with normal parameters and a var-arg, the var-arg must come last.</p>

<h3>Static Variables and Methods (Objective 1.4)</h3>
<p> They are not tied to any particular instance of a class.</p>
<p> No classes instances are needed in order to use static members of the class.</p>
<p> There is only one copy of a static variable / class and all instances share it.</p>
<p> static methods do not have direct access to non-static members.</p>

<h3>Overriding and Overloading (Objectives 1.5 and 5.4)</h3>
<p> Methods can be overridden or overloaded; constructors can be overloaded
    but not overridden.</p>
<p> Abstract methods must be overridden by the first concrete (non-abstract)
    subclass.</p>
<p> With respect to the method it overrides, the overriding method</p>
<p> Must have the same argument list.</p>
<p> Must have the same return type, except that as of Java 5, the return type
    can be a subclass?this is known as a covariant return.</p>
<p> Must not have a more restrictive access modifier.</p>
<p> May have a less restrictive access modifier.</p>
<p> Must not throw new or broader checked exceptions.</p>
<p> May throw fewer or narrower checked exceptions, or any
    unchecked exception.</p>
<p> final methods cannot be overridden.</p>
<p> Only inherited methods may be overridden, and remember that private
    methods are not inherited.</p>
<p> A subclass uses super.overriddenMethodName() to call the superclass
    version of an overridden method.</p>
<p> Overloading means reusing a method name, but with different arguments.</p>
<p> Overloaded methods</p>
<p> Must have different argument lists
     May have different return types, if argument lists are also different
     May have different access modifiers
     May throw different exceptions
     Methods from a superclass can be overloaded in a subclass.</p>
<p> Polymorphism applies to overriding, not to overloading.</p>
<p> Object type (not the reference variable's type), determines which overridden
    method is used at runtime.</p>
<p> Reference type determines which overloaded method will be used at
    compile time.</p>

<h3>Return Types (Objective 1.5)</h3>
<p> Overloaded methods can change return types; overridden methods cannot,
    except in the case of covariant returns.</p>
<p> Object reference return types can accept null as a return value.</p>
<p> An array is a legal return type, both to declare and return as a value.</p>
<p> For methods with primitive return types, any value that can be implicitly
    converted to the return type can be returned.</p>
<p> Nothing can be returned from a void, but you can return nothing. You're
    allowed to simply say return, in any method with a void return type, to bust
    out of a method early. But you can't return nothing from a method with a
    non-void return type.</p>
<p> Methods with an object reference return type, can return a subtype.</p>
<p> Methods with an interface return type, can return any implementer.</p>

<h3>Advanced Overloading (Objectives 1.5 and 5.4)</h3>
<p> Primitive widening uses the "smallest" method argument possible.</p>
<p> Used individually, boxing and var-args are compatible with overloading.</p>
<p> You CANNOT widen from one wrapper type to another. (IS-A fails.)</p>
<p> You CANNOT widen and then box. (An int can't become a Long.)</p>
<p> You can box and then widen. (An int can become an Object, via an Integer.)</p>
<p> You can combine var-args with either widening or boxing.</p>

   