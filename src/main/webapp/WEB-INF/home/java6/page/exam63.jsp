<pre>
import java.util.ArrayList;
import java.util.List;
    
class Cereal { }
public class Flakes {
    
    
    public static void main(String[] args) {
        List&lt;Flakes&gt; c0 = new List&lt;Flakes&gt;() {};
List&lt;Cereal&gt; c1 = new ArrayList&lt;Cereal&gt;();
 List&lt;Cereal&gt; c2 = new ArrayList&lt;Flakes&gt;(); 
List&lt;Flakes&gt; c3 = new ArrayList&lt;Cereal&gt;(); 
List&lt;Object&gt; c4 = new ArrayList&lt;Flakes&gt;(); 
ArrayList&lt;Cereal&gt; c5 = new ArrayList&lt;Flakes&gt;();
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect3/exam/Flakes.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect3\exam\Flakes.java:13: error: &lt;anonymous com.mvivekweb.ocpjp6.obj6.sect3.exam.Flakes$1&gt; is not abstract and does not override abstract method subList(int,int) in List
        List&lt;Flakes&gt; c0 = new List&lt;Flakes&gt;() {};
                                             ^
com\mvivekweb\ocpjp6\obj6\sect3\exam\Flakes.java:15: error: incompatible types: ArrayList&lt;Flakes&gt; cannot be converted to List&lt;Cereal&gt;
 List&lt;Cereal&gt; c2 = new ArrayList&lt;Flakes&gt;();
                   ^
com\mvivekweb\ocpjp6\obj6\sect3\exam\Flakes.java:16: error: incompatible types: ArrayList&lt;Cereal&gt; cannot be converted to List&lt;Flakes&gt;
 List&lt;Flakes&gt; c3 = new ArrayList&lt;Cereal&gt;();
                   ^
com\mvivekweb\ocpjp6\obj6\sect3\exam\Flakes.java:17: error: incompatible types: ArrayList&lt;Flakes&gt; cannot be converted to List&lt;Object&gt;
 List&lt;Object&gt; c4 = new ArrayList&lt;Flakes&gt;();
                   ^
com\mvivekweb\ocpjp6\obj6\sect3\exam\Flakes.java:18: error: incompatible types: ArrayList&lt;Flakes&gt; cannot be converted to ArrayList&lt;Cereal&gt;
 ArrayList&lt;Cereal&gt; c5 = new ArrayList&lt;Flakes&gt;();
                        ^
5 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
public class Vinegar {
    
    
    public static void main(String[] args) {
        Set&lt;Integer&gt; mySet = new HashSet&lt;Integer&gt;();
 do1(mySet, "0"); do1(mySet, "a");
do2(mySet, "0"); do2(mySet, "a");
    }
public static void do1(Set s, String st) {
s.add(st);
 s.add(Integer.parseInt(st));
 }
 public static void do2(<mark>Set&lt;Integer&gt; s</mark>, String st) {
 <mark>s.add(st);</mark>
 s.add(Integer.parseInt(st));
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect3/exam/Vinegar.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect3\exam\Vinegar.java:22: error: no suitable method found for add(String)
 s.add(st);
  ^
    method Collection.add(Integer) is not applicable
      (argument mismatch; String cannot be converted to Integer)
    method Set.add(Integer) is not applicable
      (argument mismatch; String cannot be converted to Integer)
Note: com\mvivekweb\ocpjp6\obj6\sect3\exam\Vinegar.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.HashSet;
import java.util.Set;
    
    
public class Vinegar {
    
    
    public static void main(String[] args) {
       Set&lt;Integer&gt; mySet = new HashSet&lt;Integer&gt;();
 do1(mySet, "0"); do1(mySet, "a");
do2(mySet, "0"); do2(mySet, "a");
    }
public static void do1(Set s, String st) {
s.add(st);
 s.add(Integer.parseInt(st));
 }
 public static void do2(<mark>Set&lt;Integer&gt; s</mark>, String st) {
     
 s.add(<mark>Integer.parseInt(st)</mark>);
 }
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.NumberFormatException: For input string: "a"
	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.lang.Integer.parseInt(Integer.java:580)
	at java.lang.Integer.parseInt(Integer.java:615)
	at com.mvivekweb.ocpjp6.obj6.sect3.exam.Vinegar.do1(Vinegar.java:19)
	at com.mvivekweb.ocpjp6.obj6.sect3.exam.Vinegar.main(Vinegar.java:14)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
    
    
public class Olives {
    
    
    public static void main(String[] args) {
        Set&lt;Integer&gt; s = new TreeSet&lt;Integer&gt;();
 s.add(23); s.add(42); s.add(new Integer(5));
 Iterator i = s.iterator();
     
<mark>  while(System.out.print(i.next())) { }
  for(Integer i2: i) System.out.print(i2);</mark>
 for(Integer i3: s) System.out.print(i3);
<mark> while(i.hasNext()) System.out.print(i.get());</mark>
 while(i.hasNext()) System.out.print(i.next());
    }
        
}
</pre>

<pre>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect3/exam/Olives.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect3\exam\Olives.java:18: error: incompatible types: <mark>void cannot be converted to boolean</mark>
  while(System.out.print(i.next())) { }
                        ^
com\mvivekweb\ocpjp6\obj6\sect3\exam\Olives.java:19: error: <mark>for-each not applicable to expression type</mark>
  for(Integer i2: i) System.out.print(i2);
                  ^
  required: array or java.lang.Iterable
  found:    Iterator
com\mvivekweb\ocpjp6\obj6\sect3\exam\Olives.java:21: error: <mark>cannot find symbol</mark>
 while(i.hasNext()) System.out.print(i.get());
                                      ^
  symbol:   method get()
  location: variable i of type Iterator
3 errors
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
    
    
public class Olives {
    
    
    public static void main(String[] args) {
        Set&lt;Integer&gt; s = new TreeSet&lt;Integer&gt;();
 s.add(23); s.add(42); s.add(new Integer(5));
 Iterator i = s.iterator();
     
     
 for(Integer i3: s) System.out.print(i3);
     
 while(i.hasNext()) System.out.print(i.next());
    }
        
}
</pre>
<pre class='out'>
run:
<mark>5234252342</mark>BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.Comparator;
    
class NameCompare implements Comparator&lt;Stuff&gt; {
 public int compare(Stuff a, Stuff b) {
 return b.name.compareTo(a.name);
 } }
class ValueCompare implements Comparator&lt;Stuff&gt; {
 public int compare(Stuff a, Stuff b) {
 return (a.value - b.value);
 } }</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
    
class Stuff {
    
    int value;
    String name;
        
}
</pre>

<p>These two classes properly implement the Comparator interface.</p>

<p>ValueCompare allows you to sort a collection of Stuff instances in ascending numeric order.</p>

<p>The code is legal and Collections.sort() can use the
    ValueCompare.compare() method to sort in ascending numeric order.</p>

<p>this code doesn't show us whether
    Stuff has overridden equals() and hashCode(), which would allow Stuff to be used
    successfully for keys.</p>

<p>NameCompare.compare() method would
    be used to create sorts in reverse-alphabetical order.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.ArrayList;
import java.util.List;
    
class Snack {
 static List&lt;String&gt; s1 = new ArrayList&lt;String&gt;();
 }
     
public class Chips extends Snack{
    
    
    public static void main(String[] args) {
        List c1 = new ArrayList();
 s1.add("1"); s1.add("2");
 c1.add("3"); c1.add("4");
 getStuff(s1, c1);
    }
static void getStuff(List&lt;String&gt; a1, List a2) {
 for(String s1: a1) System.out.print(s1 + " ");
 for(String s2: <mark>a2</mark>) System.out.print(s2 + " ");
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj6/sect3/exam/Chips.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj6\sect3\exam\Chips.java:23: error: incompatible types: <mark>Object cannot be converted to String</mark>
 for(String s2: a2) System.out.print(s2 + " ");
                ^
Note: com\mvivekweb\ocpjp6\obj6\sect3\exam\Chips.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>


</pre>

<p>When getting elements from a non-generic collection, a cast (from Object)
    is required. The rest of the code is legal.</p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.ArrayList;
import java.util.List;
    
class Snack {
 static List&lt;String&gt; s1 = new ArrayList&lt;String&gt;();
 }
     
public class Chips extends Snack{
    
    
    public static void main(String[] args) {
        List c1 = new ArrayList();
 s1.add("1"); s1.add("2");
 c1.add("3"); c1.add("4");
 getStuff(s1, c1);
    }
static void getStuff(List&lt;String&gt; a1, List a2) {
 for(String s1: a1) System.out.print(s1 + " ");
 for(<mark>Object</mark> s2: a2) System.out.print(s2 + " ");
 }
}
</pre>
<pre class='out'>
run:
1 2 3 4 BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
    
class Snack {
 static List&lt;String&gt; s1 = new ArrayList&lt;String&gt;();
 }
     
public class Chips extends Snack{
    
    
    public static void main(String[] args) {
        List c1 = new ArrayList();
 s1.add("1"); s1.add("2");
 c1.add("3"); c1.add("4");
 getStuff(s1, c1);
    }
static void getStuff(List&lt;String&gt; a1, List a2) {
 for(String s1: a1) System.out.print(s1 + " ");
        for (Iterator it = a2.iterator(); it.hasNext();) {
            <mark>String s2 = (String) it.next();</mark>
            System.out.print(s2 + " ");
        }
 }
}
</pre>
<pre class='out'>
run:
1 2 3 4 BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
    
    
public class Salt {
    
    
    public static void main(String[] args) {
        Set s1 = new HashSet();
 s1.add(0);
 s1.add("1");
 doStuff(s1);
    }
static void doStuff(Set&lt;Number&gt; s) {
 do2(s);
 Iterator i = s.iterator();
 while(i.hasNext()) System.out.print(i.next() + " ");
 Object[] oa = s.toArray();
 for(int x = 0; x < oa.length; x++)
 System.out.print(oa[x] + " ");
 System.out.println(s.contains(1));
 }
 static void do2(Set s2) { System.out.print(s2.size() + " "); }
}
    
</pre>

<pre class='out'>run:
2 0 1 0 1 false
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.TreeSet;
    
    
public class Corner {
    
    
    public static void main(String[] args) {
       TreeSet&lt;String&gt; t1 = new TreeSet&lt;String&gt;();
 TreeSet&lt;String&gt; t2 = new TreeSet&lt;String&gt;();
 t1.add("b"); t1.add("7");
 t2 = (TreeSet)t1.subSet("5", "c");
 try {
 t1.add("d");
 t2.add("6");
 t2.add("3");
 }
 catch (Exception e) { System.out.print("ex "); }
 System.out.println(t1 + " " + t2); 
    }
        
}
    
</pre>
<pre class='out'>
run:
ex [6, 7, b, d] [6, 7, b]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect3.exam;
    
import java.util.NavigableMap;
import java.util.TreeMap;
    
    
public class GIS {
    
    
    public static void main(String[] args) {
      TreeMap<String, String> m1 = new TreeMap<String, String>();
 m1.put("a", "amy"); m1.put("f", "frank");
 NavigableMap<String, String> m2 = m1.descendingMap();
 try {
 m1.put("j", "john");
 m2.put("m", "mary");
 }
 catch (Exception e) { System.out.print("ex "); }
  System.out.println(m1 + " " + m2);  
 m1.pollFirstEntry();
 System.out.println(m1 + " " + m2);  
    }
        
}
</pre>
<pre class='out'>
run:
{a=amy, f=frank, j=john, m=mary} {m=mary, j=john, f=frank, a=amy}
{f=frank, j=john, m=mary} {m=mary, j=john, f=frank}
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

