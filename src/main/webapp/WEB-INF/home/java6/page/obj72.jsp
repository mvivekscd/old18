<h3>Given an example of a class and a command-line, determine the expected runtime
behavior.</h3>
        
        <h3> Using the javac and java Commands</h3>
        
        <p>The <mark>javac </mark>command is used to invoke Java's <mark>compiler</mark>.</p>
        
        <p> understand the <mark>-classpath</mark> and <mark>-d</mark> options</p>
        
        <p>structural overview for javac</p>
        <pre>
        javac <mark>[options] [source files]</mark>
            
        </pre>
        
        <p>Both the [options] and the [source files]
            are optional parts of the command, and both <mark>allow multiple entries</mark></p>

        <pre>
        javac -help
        javac <mark>-classpath com:.</mark> <mark>-g</mark> <mark>Foo.java</mark> <mark> Bar.java</mark>
            
        </pre>

        <p> The second invocation passes the compiler two options (<mark>-classpath,
                which itself has an argument of com:.</mark> and <mark>-g</mark>), and passes the compiler two .java
            files to compile (Foo.java and Bar.java)</p>
        
        <p>Whenever you specify multiple options
            and/or files they should be separated by spaces.</p>
        
        <h3> Compiling with -d</h3>
        
        <p>The -d option lets you tell
            the compiler in which directory to put the .class file(s) it generates (<mark>d is for
                destination</mark>)</p>

             
                
                <pre> 
    myProject
        |
        |--source
        |   |
        |   |-- MyClass.java
        |
        |-- classes
                |
                |--   
                </pre>
                
                
                
                <p> The following command, issued from the <mark>myProject</mark> directory, will compile
                    MyClass.java and put the resulting MyClass.class file into the classes
                    directory.  </p>          
                
                <pre>cd myProject
javac -d classes source/MyClass.java  </pre>   
                
                
                
                
 <pre> 
    myProject
        |
        |--source
        |   |
        |   |--com
        |       |
        |       |--wickedlysmart
        |               |
        |               |--MyClass.java
        |
        |-- classes
        |       |
        |       |--com
        |           |
        |           |--wickedlysmart
        |               |
        |               |-- (MyClass.class goes here)  
                </pre>               
                
                <p> If you were in the <mark>source</mark> directory  </p>          
    
                <pre>  javac -d <mark>../classes</mark> com/wickedlysmart/MyClass.java   </pre>          
                
                <p><mark>cd</mark> back to
                    the <mark>myProject</mark> directory then <mark>cd</mark> into the classes directory, which will be your
                    destination    </p>            
 
                
  <pre> 
    myProject
        |
        |--source
        |   |
        |   |--com
        |       |
        |       |--wickedlysmart
        |               |
        |               |--MyClass.java
        |
        |-- classes
        |       |
 
                </pre>                  
                
                <pre>javac -d ../classes com/wickedlysmart/MyClass.java
                </pre>
                <p>the compiler will build two directories called <mark>com</mark> and
                    <mark>com/wickedlysmart</mark> in order to put the resulting MyClass.class file into the
                    correct package directory (com/wickedlysmart/) which it builds within the
                    existing .../classes directory.</p>


                
                <pre>                
package com.mvivekweb.ocpjp6.obj7.sect2.commandline.sources.com.wickedlysmart;
    
    
public class MyClass {
    
}
                </pre>
                
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj7\sect2\commandline\sources>javac <mark>-d ../classes</mark> <mark>-source 6</mark> com/wickedlysmart/MyClass.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj7\sect2\commandline\sources>javac <mark>-source 1.6</mark> <mark>-d ../classes</mark> com/wickedlysmart/MyClass.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj7\sect2\commandline\sources>
                
</pre>
                
                <p>if the
                    <mark>destination directory</mark> you specify <mark>doesn't exist</mark>, you'll get a <mark>compiler error</mark>. If, in the
                    previous example, the <mark>classe</mark> directory did NOT exist, the compiler would say       </p>        
                
    
<pre class='out'> 
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj7\sect2\commandline\sources>javac -source 1.6 -d ../classe com/wickedlysmart/MyClass.java
javac: directory not found: ../classe
Usage: javac <options> <source files>
use -help for a list of possible options

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj7\sect2\commandline\sources>
               
</pre>
 <p> Use -d to change the destination of a class file when it's first generated by the
            javac command.</p>
        <p> The -d option can build package-dependent destination classes on-the-fly if
            the root package directory already exists.</p>
        
        <h3> Launching Applications with java  </h3>     
        
        <p>The java command is used to <mark>invoke</mark> the <mark>Java virtual machine</mark>   </p>     
        
        <p>understand
            the <mark>-classpath</mark> (and its twin <mark>-cp</mark>) and <mark>-D</mark> options</p>
        
        <pre>java [options] <mark>class</mark> [args]  </pre>      
        
        <p>The [options] and [args] parts of the java command are optional, and they
            <mark>can</mark> both <mark>have multiple values</mark>.</p>

        <p><mark>must</mark> specify <b>exactly</b> <mark>one class</mark> file to execute</p>
        <pre>
        java <mark>-DmyProp=myValue</mark> <mark>MyClass</mark> x 1
        </pre>
        <p>Create a system
            property called <mark>myProp</mark> and set its value to <mark>myValue</mark></p>
        
        <h3>Using System Properties</h3>
        
        <p> <mark>add</mark> and <mark>retrieve</mark> your own properties</p>
        <pre>       
   package com.mvivekweb.ocpjp6.obj7.sect2.properties;
       
import java.util.Properties;
    
public class TestProps {
    
    public static void main(String[] args) {
        Properties p = <mark>System.getProperties();</mark>
        <mark>p.setProperty("myProp", "myValue");</mark>
        p.list(System.out);
    }
}
        </pre>  
 <pre class='out'>       
 run:
-- listing properties --
java.runtime.name=Java(TM) SE Runtime Environment
sun.boot.library.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vm.version=25.74-b02
java.vm.vendor=Oracle Corporation
java.vendor.url=http://java.oracle.com/
path.separator=;
java.vm.name=Java HotSpot(TM) 64-Bit Server VM
file.encoding.pkg=sun.io
user.script=
user.country=US
sun.java.launcher=SUN_STANDARD
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=C:\vivek\java7\ocpjp6\ocpjp6
java.runtime.version=1.8.0_74-b02
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
os.arch=amd64
java.io.tmpdir=C:\Users\manju\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
<mark>myProp=myValue</mark>
sun.jnu.encoding=Cp1252
java.library.path=C:\Program Files\Java\jdk1.8.0_74\bin...
java.specification.name=Java Platform API Specification
java.class.version=52.0
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\manju
user.timezone=
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=UTF-8
java.specification.version=1.8
user.name=manju
java.class.path=C:\vivek\java7\ocpjp6\ocpjp6\build\cl...
java.vm.specification.version=1.8
sun.arch.data.model=64
java.home=C:\Program Files\Java\jdk1.8.0_74\jre
sun.java.command=com.mvivekweb.ocpjp6.obj7.sect2.prope...
java.specification.vendor=Oracle Corporation
user.language=en
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_74
java.ext.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
sun.boot.class.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vendor=Oracle Corporation
file.separator=\
java.vendor.url.bug=http://bugreport.sun.com/bugreport/
sun.cpu.endian=little
sun.io.unicode.encoding=UnicodeLittle
sun.desktop=windows
sun.cpu.isalist=amd64
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
        
 
        <pre> java <mark>-DcmdProp=cmdVal</mark> TestProps       
        </pre>
<pre class='out'> 
 run:
-- listing properties --
java.runtime.name=Java(TM) SE Runtime Environment
sun.boot.library.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vm.version=25.74-b02
java.vm.vendor=Oracle Corporation
java.vendor.url=http://java.oracle.com/
path.separator=;
java.vm.name=Java HotSpot(TM) 64-Bit Server VM
file.encoding.pkg=sun.io
user.script=
user.country=US
sun.java.launcher=SUN_STANDARD
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=C:\vivek\java7\ocpjp6\ocpjp6
java.runtime.version=1.8.0_74-b02
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
os.arch=amd64
java.io.tmpdir=C:\Users\manju\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
<mark>cmdProp=cmdVal</mark>
sun.jnu.encoding=Cp1252
java.library.path=C:\Program Files\Java\jdk1.8.0_74\bin...
java.specification.name=Java Platform API Specification
java.class.version=52.0
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\manju
user.timezone=
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=UTF-8
java.specification.version=1.8
user.name=manju
java.class.path=C:\vivek\java7\ocpjp6\ocpjp6\build\cl...
<mark>myProp1=myValue</mark>
java.vm.specification.version=1.8
sun.arch.data.model=64
java.home=C:\Program Files\Java\jdk1.8.0_74\jre
sun.java.command=com.mvivekweb.ocpjp6.obj7.sect2.prope...
java.specification.vendor=Oracle Corporation
user.language=en
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_74
java.ext.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
sun.boot.class.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vendor=Oracle Corporation
file.separator=\
java.vendor.url.bug=http://bugreport.sun.com/bugreport/
sun.cpu.endian=little
sun.io.unicode.encoding=UnicodeLittle
sun.desktop=windows
sun.cpu.isalist=amd64
BUILD SUCCESSFUL (total time: 1 second)
       
    </pre>
        
        <p> myProp=myValue was added via the <mark>setProperty</mark>
            method, and cmdProp=cmdVal was added via the <mark>-D option at the command line</mark>. </p>  
        
        
        <pre>        
package com.mvivekweb.ocpjp6.obj7.sect2.properties;
    
import java.util.Properties;
    
public class TestProps {
    
    public static void main(String[] args) {
        Properties p = System.getProperties();
        p.setProperty("myProp1", "myValue");
        p.list(System.out);
            
        System.err.println("p.getProperty(\"myProp1\");= "+p.getProperty("myProp1"));
         System.err.println("p.getProperty(\"myProp1\");= "+p.getProperty("myProp","vivek"));
    }
}
        </pre>      
 <pre class='out'>       
 run:
-- listing properties --
java.runtime.name=Java(TM) SE Runtime Environment
sun.boot.library.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vm.version=25.74-b02
java.vm.vendor=Oracle Corporation
java.vendor.url=http://java.oracle.com/
path.separator=;
<mark>p.getProperty("myProp1");= myValue</mark>
java.vm.name=Java HotSpot(TM) 64-Bit Server VM
<mark>p.getProperty("myProp1");= vivek</mark>
file.encoding.pkg=sun.io
user.script=
user.country=US
sun.java.launcher=SUN_STANDARD
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=C:\vivek\java7\ocpjp6\ocpjp6
java.runtime.version=1.8.0_74-b02
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
os.arch=amd64
java.io.tmpdir=C:\Users\manju\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
<mark>cmdProp=cmdVal</mark>
sun.jnu.encoding=Cp1252
java.library.path=C:\Program Files\Java\jdk1.8.0_74\bin...
java.specification.name=Java Platform API Specification
java.class.version=52.0
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\manju
user.timezone=
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=UTF-8
java.specification.version=1.8
user.name=manju
java.class.path=C:\vivek\java7\ocpjp6\ocpjp6\build\cl...
<mark>myProp1=myValue</mark>
java.vm.specification.version=1.8
sun.arch.data.model=64
java.home=C:\Program Files\Java\jdk1.8.0_74\jre
sun.java.command=com.mvivekweb.ocpjp6.obj7.sect2.prope...
java.specification.vendor=Oracle Corporation
user.language=en
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_74
java.ext.dirs=C:\Program Files\Java\jdk1.8.0_74\jre...
sun.boot.class.path=C:\Program Files\Java\jdk1.8.0_74\jre...
java.vendor=Oracle Corporation
file.separator=\
java.vendor.url.bug=http://bugreport.sun.com/bugreport/
sun.cpu.endian=little
sun.io.unicode.encoding=UnicodeLittle
sun.desktop=windows
sun.cpu.isalist=amd64
BUILD SUCCESSFUL (total time: 1 second)
       
    </pre> 
        
        <p>The <mark>getProperty()</mark> method is used to retrieve a single property. It can be
            invoked with a single argument (a String that represents the name (or key)), or it
            can be invoked with two arguments, (a String that represents the name (or key),
            and a default String value to be used as the property if the property does not already
            exist). In both cases, getProperty() returns the property as a <mark>String</mark>.     </p> 
        
        
        
        <h3> Handling Command-Line Arguments</h3>
        <pre> 
        package com.mvivekweb.ocpjp6.obj7.sect2.commandlinearguments;
            
            
public class CmdArgs {
    
    public static void main(String[] args) {
        int x = 0;
        for (String s : args) {
            System.out.println(x++ + " element = " + s);
        }
    }
}
        </pre>
<pre class='out'>        
C:\vivek\java7\ocpjp6\ocpjp6\src>javac com\mvivekweb\ocpjp6\obj7\sect2\commandlinearguments\CmdArgs.java

C:\vivek\java7\ocpjp6\ocpjp6\src>java com.mvivekweb.ocpjp6.obj7.sect2.commandlinearguments.CmdArgs x 1
0 element = x
1 element = 1

C:\vivek\java7\ocpjp6\ocpjp6\src>        
    </pre>
        <pre>
package com.mvivekweb.ocpjp6.obj7.sect2.commandlinearguments;
    
    
public class CmdArgs {
    
    <mark>public static void main(String... args) {</mark>
        int x = 0;
        for (String s : args) {
            System.out.println(x++ + " element = " + s);
        }
    }
}
        </pre>
        <pre>
package com.mvivekweb.ocpjp6.obj7.sect2.commandlinearguments;
    
    
public class CmdArgs {
    
    <mark><b>static</b> public void   main(String args[]) {</mark>
        int x = 0;
        for (String s : args) {
            System.out.println(x++ + " element = " + s);
        }
    }
}
        </pre>     
        
        