<pre>
package com.mvivekweb.ocpjp6.obj5.sect5.exam;
    
class Employee {
 private String name;
 void setName(String n) { name = n; }
 String getName() { return name; }
 }
 interface Mungeable {
 void doMunging();
 }
public class MyApp implements Mungeable{
public void doMunging() { ; }
    
    public static void main(String[] args) {
        Employee e = new Employee();
 e.setName("bob");
 System.out.print(e.getName());
    }
        
}
</pre>

<p>MyApp is-a Mungeable.</p>

<p>The code is loosely coupled.
    The Employee class is well encapsulated.</p>

<p>MyApp class "uses" the Employee class, but MyApp isn't
    in Employee's class hierarchy, and MyApp doesn't "have" an Employee as part of its state.MyApp doesn't "have" a Mungeable as part of its state.</p>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect5.exam;
    
    
public class Glank implements Vonk { Jooker[] j; }
abstract class Bostron { String yoodle; Bostron b; }
interface Protefor { }
 interface Vonk extends Protefor { int x = 7; }
class Jooker { Bostron b; }
    
</pre>

<p>Glanks have a Bostron.</p>

<p>Jookers have a String.</p>

<p>Bostrons have a Bostron.</p>

<p>Glanks have
    Bostrons indirectly through Jookers. Jookers have Strings indirectly through Bostrons, and
    Bostrons have Bostrons because it's very common to want to make linked lists with your
    Bostrons.</p>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect5.exam;
abstract class Nameable {
String name;
}
class Animal extends Nameable {
<mark>Animal(String n) { name = n; }
String getName() { return name; }</mark>
}
    
public class Buddies extends Animal{
Buddies(String s) { super(s); }
    
    public static void main(String[] args) {
        Animal b1 = new Animal("Kara");
Buddies b2 = new Buddies("Charis");
System.out.println(b1.getName() + " " + b2.getName());
    }
        
}
    
</pre>

<pre class='out'>run:
Kara Charis
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>LinkedHashSet is-a Collection
Vector is-a List
LinkedList is-a Queue
TreeMap is-a SortedMap
Queue is-a Collection
</pre>

<p>If class A is-a class B, it's possible for them to still be loosely coupled.</p>

<p>If class A has-a class B, it's possible for them to still be loosely coupled.</p>

<p>Classes are loosely coupled if they interact with each other only
    through each other's APIs. It's possible to create is-a and has-a relationships that interact
    solely through APIs.</p>

<p>the concepts (is-a/has-a versus encapsulation) are more or
    less independent of each other.</p>

<p>the concepts (is-a/has-a
    versus cohesion) are more or less independent of each other.</p>

<pre>
import java.util.ArrayList;
import java.util.List;
    
enum Heroes { GANDALF, HANS, ENDER }
    
public class MyStuff {
    
    
    public static void main(String[] args) {
       List&lt;String&gt; stuff = new ArrayList&lt;String&gt;();
 stuff.add("Bob"); stuff.add("Fred");
 new MyStuff().go();
    }
Heroes myH = Heroes.ENDER;
 void go() {
 for(Heroes h: Heroes.values())
 if(h == myH) System.out.println(myH);
 }
}
    
</pre>
<pre class='out'>
run:
ENDER
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>MyStuff has-a Heroes</p>

<p>MyStuff's main() "uses" a List, but MyStuff doesn't "have" a List.</p>


