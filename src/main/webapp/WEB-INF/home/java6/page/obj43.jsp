 <h3>Given a scenario, write code that makes appropriate use of object locking to
            protect static or instance variables from concurrent access problems.</h3>
        
        
        <h3>Synchronizing Code</h3>
        
        <p>when two different threads are
            accessing the <mark>same account data</mark>  </p>
        
        
        
        <pre>
        package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
            
            
class Account {
    
    <mark>private int balance = 50;</mark>
        
    public int getBalance() {
        return balance;
    }
        
    public void withdraw(int amount) {
        balance = balance - amount;
    }
}
        </pre>
        
        
        <pre>
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
public class AccountDanger implements Runnable {
    
    private Account acct = new Account();
        
    public static void main(String[] args) {
        AccountDanger r = new AccountDanger();
        Thread one = new Thread(r);
        Thread two = new Thread(r);
        one.setName("Fred");
        two.setName("Lucy");
        one.start();
        two.start();
    }
        
    public void run() {
        for (int x = 0; x < 5; x++) {
            makeWithdrawal(10);
            if (acct.getBalance() < 0) {
                System.out.println("<mark>account is overdrawn!</mark>");
            }
        }
    }
        
    private /* synchronized */ void makeWithdrawal(int amt) {
        if (acct.getBalance() >= amt) {
            System.out.println(Thread.currentThread().getName()
                    + " is going to withdraw");
            try {
               <mark> Thread.sleep(500);</mark>
            } catch (InterruptedException ex) {
            }
            acct.withdraw(amt);
            System.out.println(Thread.currentThread().getName()
                    + " completes the withdrawal");
        } else {
            System.out.println("<mark>Not enough in account for </mark>"
                    + Thread.currentThread().getName()
                    + " to withdraw " + acct.getBalance());
        }
    }
}
        </pre>
   <pre class='out'>     
run:
Fred is going to withdraw
Lucy is going to withdraw
Lucy completes the withdrawal
Lucy is going to withdraw
Fred completes the withdrawal
Fred is going to withdraw
Fred completes the withdrawal
Lucy completes the withdrawal
Fred is going to withdraw
Lucy is going to withdraw
Fred completes the withdrawal
Lucy completes the withdrawal
<mark>account is overdrawn!
account is overdrawn!
Not enough in account for Fred to withdraw -10
Not enough in account for Lucy to withdraw -10
account is overdrawn!
account is overdrawn!
Not enough in account for Fred to withdraw -10
Not enough in account for Lucy to withdraw -10
account is overdrawn!
account is overdrawn!</mark>
BUILD SUCCESSFUL (total time: 2 seconds)
        
    </pre>
        

        <p> when two <mark>threads concurrently
            access the same object</mark>.  </p>   
        
        <p>This problem is known as a <mark>"race condition"</mark> where<mark> multiple threads can access
            the same resource</mark> (typically an object's instance variables), and can produce
            corrupted data if <mark>one thread "races in" too quickly before an operation that should be
            "atomic" has completed.</mark>   </p>    
        
        
        <h3> Preventing the Account Overdraw </h3>
        
        <p>We <mark>must guarantee that the two steps</mark> of the withdrawal-
            checking the balance and making the withdrawal-are <mark>never split</mark> apart</p>
        
        <p> We need
            them to always be performed as <mark>one operation, even when the thread falls asleep</mark> in
            between step 1 and step 2! </p>
        
        <p>We call this an <mark>"atomic operation" </mark> (although the physics
            is a little outdated, in this case "atomic" means "indivisible") because the operation,
            regardless of the number of actual statements (or underlying byte code instructions),
            <mark> is completed before any other thread code that acts on the same data.</mark>        
        </p>
        
        <p>You <mark>can't guarantee</mark> that a <mark>single thread</mark> will <mark>stay running throughout</mark> the entire
            atomic operation   </p>
        
        
        
        <p>But you can <mark>guarantee</mark> that even if the thread running the atomic
            operation moves in and out of the running state, <mark>no other running thread will be
            able to act on the same data.</mark>   </p>
        
        <p>protect the data </p>
        
        Mark the variables private.
        <br>        Synchronize the code <mark>that modifies the variables</mark>.
            
        
        
        <p>It's the <mark>method code that you must protect</mark>, so that only one thread at a
            time can be executing that code. You do this with the <mark>synchronized keyword</mark>.   </p>     
        <pre>       
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
    
class Account {
    
    private int balance = 50;
        
    public int getBalance() {
        return balance;
    }
        
    public void withdraw(int amount) {
        balance = balance - amount;
    }
}        
        </pre>   
        
<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;

public class AccountDanger implements Runnable {

    private Account acct = new Account();

    public static void main(String[] args) {
        AccountDanger r = new AccountDanger();
        Thread one = new Thread(r);
        Thread two = new Thread(r);
        one.setName("Fred");
        two.setName("Lucy");
        one.start();
        two.start();
    }

    public void run() {
        for (int x = 0; x < 4; x++) {
            makeWithdrawal(10);
            if (acct.getBalance() < 0) {
                System.out.println("account is overdrawn!");
            }
        }
    }

    private  <mark>synchronized</mark> void makeWithdrawal(int amt) {
        if (acct.getBalance() >= amt) {
            System.out.println(Thread.currentThread().getName()
                    + " is going to withdraw");
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
            acct.withdraw(amt);
            System.out.println(Thread.currentThread().getName()
                    + " completes the withdrawal");
        } else {
            System.out.println("Not enough in account for "
                    + Thread.currentThread().getName()
                    + " to withdraw " + acct.getBalance());
        }
    }
}
    </pre>
        
 <pre class='out'>       
run:
Fred is going to withdraw
Fred completes the withdrawal
Fred is going to withdraw
Fred completes the withdrawal
Fred is going to withdraw
Fred completes the withdrawal
Fred is going to withdraw
Fred completes the withdrawal
Lucy is going to withdraw
Lucy completes the withdrawal
Not enough in account for Lucy to withdraw 0
Not enough in account for Lucy to withdraw 0
Not enough in account for Lucy to withdraw 0
BUILD SUCCESSFUL (total time: 3 seconds)
    </pre>

        <h3>Synchronization and Locks   </h3>
        
        
        <p>object in <mark>Java has a built-in lock</mark>
            that only comes into play <mark>when the object has synchronized method code</mark>    </p>
        
  <img src="/imag/jp6/threadl.png" class="imgw">       
        <p>When
            we enter a <mark>synchronized non-static</mark> method, we automatically acquire the lock
            associated with the current instance of the class whose code we're executing    </p>    
        
        <p>We
            may also use the term <mark>monitor</mark> to refer to the object whose lock we're acquiring </p>
        
        <p>Since there is only one lock per object, if <mark>one thread</mark> has picked up the <mark>lock</mark>, <mark>no
            other thread can pick up the lock</mark> until the first thread releases (or returns) the lock.        </p>
        
        <p><mark>releasing a lock</mark> means the <mark>thread holding the lock</mark> <mark>exits the synchronized method</mark></p>
        
        <p>Only methods (or blocks) can be synchronized, <mark>not variables or classes</mark>.</p>
        
        <p>Each object has just one lock.</p>
        
        
        <p>Not all methods in a class need to be synchronized. A class can have both
            <mark>synchronized and non-synchronized</mark> methods. </p>
        
        
        <p>If two threads are about to execute a synchronized method in a class, and
            both threads are using the same instance of the class to invoke the method,
            <mark>only one thread</mark> at a time will be able to execute the method </p>       
        
        <p>If a class has both synchronized and non-synchronized methods, multiple
            threads can still access the class's non-synchronized methods </p>
        
        <p>Synchronization can cause a hit in some cases (or
            <mark>even deadlock</mark> if used incorrectly), so you should be careful <mark>not to overuse it</mark>.</p>
        
        <p>If a thread goes to sleep, it holds any locks it has-it doesn't release them. </p>
        
        <p>A <mark>thread</mark> can acquire <mark>more than one lock</mark>   </p>     
        
          <img src="/imag/jp6/threadlm.png" class="imgw">   
        <p>For example, a thread can enter a
            synchronized method, thus acquiring a lock, and then immediately invoke
            a synchronized method on a different object, thus acquiring that lock as
            well. As the stack unwinds, <mark>locks are released</mark> again. </p>
        
        <p>Also, if a thread acquires
            a lock and then <mark>attempts to call a synchronized method on that same
            object, no problem</mark>. </p>       
        
        
        <p>You can <mark>synchronize a block of code</mark> rather than a method. </p>
        
        
        <p> synchronization <mark>does hurt concurrency</mark>, you <mark>don't want to synchronize
            any more code</mark> than is necessary to protect your data.  </p>
        
        <p>  reduce the scope of the synchronized part to something
            less than a full method-to just a block.  <mark>synchronized block</mark>   </p>
        <pre>       
 <mark>synchronized(this)</mark> {
System.out.println("synchronized");
}       
        </pre>
        
        <p>The real question is, synchronized on what? Or,
            synchronized on which object's lock?</p>
        
        
        <p>When you synchronize a method, <mark>the object used to invoke the method</mark> is the
            <mark>object whose lock</mark> must be acquired.  </p>
        
        <p>But when you <mark>synchronize a block of code</mark>, you <mark>specify which object's lock you want to use as the lock</mark>, so you <mark>could, for example,
            use some third-party object</mark> as the lock for this piece of code   </p>
        
        <img src="/imag/jp6/threadlt.png" class="imgw"> 
        
        
        <p>That gives you the
            ability to have <mark>more than one lock for code synchronization within a single object</mark>.   </p>    
        
        <p>you can synchronize on the <mark>current instance</mark> (this)</p>
        
        <p> Since <mark>that's the same instance that synchronized methods lock on</mark>, it means that
            you <mark>could always replace a synchronized method with a non-synchronized
            method containing a synchronized block</mark> </p>
        
        
        
            <pre> public <mark>synchronized</mark> void doStuff() {
System.out.println("synchronized");
}       
            </pre>
            
            <p>or</p>
            <pre>           
 public void doStuff() {
<mark>synchronized(this)</mark> {
System.out.println("synchronized");
}
}  </pre>         
            
            <p>The first form is <mark>shorter and more familiar</mark>
                to most people, but the <mark>second </mark>can be <mark>more flexible</mark>.   </p>         
        
        
        
            <h3> So What About Static Methods? Can They Be Synchronized?   </h3>
            
            <p>static methods <mark>can be synchronized</mark>.   </p>         
        
            <p>There is only <mark>one copy of the static data</mark>
                you're trying to protect, so you only need <mark>one lock per class to synchronize static
                methods-a lock for the whole class</mark>. </p>      
        
There is such a lock; <mark>every class</mark> loaded in Java
has a corresponding <mark>instance of java.lang.Class</mark> representing that class.        
        
<p>It's that
    <mark> java.lang.Class</mark> instance whose lock is used to <mark>protect the static methods of
    the class</mark> (if they're synchronized).  </p>     
<pre>        
public <mark>static synchronized</mark> int getCount() {
return count;
}        
</pre>     

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
public class StaticMethodTest {
    static int  count=1;
        
    public static void main(String[] args) {
        System.out.println("getCount()="+getCount());
    }
        
    public static int getCount() {
        synchronized (this) {
            return count;
        }
    }
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - <mark>non-static variable this cannot be referenced from a static context</mark>
	at com.mvivekweb.ocpjp6.obj4.sect3.synchronizing.StaticMethodTest.getCount(StaticMethodTest.java:11)
	at com.mvivekweb.ocpjp6.obj4.sect3.synchronizing.StaticMethodTest.main(StaticMethodTest.java:7)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>  

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
public class StaticMethodTest {
    static int  count=1;
        
    public static void main(String[] args) {
        System.out.println("getCount()="+getCount());
    }
        
    public static int getCount() {
        <mark>synchronized (StaticMethodTest.class) {</mark>
            return count;
        }
    }
}
</pre>
<pre class='out'>
run:
getCount()=1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
<img src="/imag/jp6/threads.png" class="imgw">
<p>MyClass.class thing? That's called a <mark>class literal</mark></p>

<p>go
    and <mark>find me the instance of Class</mark> that represents the class called <mark>MyClass</mark></p>

<h3>What Happens If a Thread Can't Get the Lock?</h3>

<p>If a thread tries to enter a synchronized method and the <mark>lock is already taken</mark></p>

<p>There might be three threads waiting
    for a single lock, for example, and there's <mark>no guarantee that the thread that has
    waited the longest will get the lock first</mark>.</p>

    <p>Threads calling <mark>non-static synchronized methods</mark> in the same class will
        only <mark>block each other if they're invoked using the same instance</mark>. That's
        because they <mark>each lock on this instance</mark>, and if they're called using two different
        instances, <mark>they get two locks</mark>, which do not interfere with each other.</p>
    
    <p>Threads calling <mark>static synchronized</mark> methods in the same class will <mark>always
        block each other-they all lock on the same Class instance</mark>.</p>
    
        <p>A static synchronized method and a non-static synchronized method
            <mark>will not block each other</mark>, ever. The <mark>static method locks on a Class
            instance</mark> while the <mark>non-static method locks on the this instance</mark>-these
            actions <mark>do not interfere with each other</mark> at all.</p>
    
        <p>For synchronized blocks, you have to <mark>look at exactly what object has been
            used for locking</mark>. (What's <mark>inside the parentheses</mark> after the word synchronized?)</p>
        
        <p> wait () - <mark>Give Up Locks </mark>      </p>
        
        
        <p>notify() (Although the thread will probably
            exit the synchronized code shortly after this call,
            and thus <mark>give up its locks.</mark>)</p>
        
        <p>join(), sleep, yield <mark>Keep Locks</mark></p>
        
        <img src="/imag/jp6/threadrl.png" class="imgw">
        <h3>So When Do I Need to Synchronize?  </h3>
        
        <p>When we use threads, we usually need to use some synchronization somewhere to
            make sure <mark>our methods don't interrupt each other at the wrong time and mess up our
            data. </mark>  </p>
        
        <p>You don't need to worry about local variables-
            <mark> each thread gets its own copy of a local variable</mark></p>
        
        <p>Two threads executing the <mark>same
            method</mark> at the same time will <mark>use different copies of the local variables</mark>, and they
            won't bother each other.</p>
        
        <p>you do <mark>need to worry about static</mark> and <mark>nonstatic
            fields,</mark> if they<mark> contain data that can be changed</mark>. </p>      
        
        <p>For <mark>changeable data in a non-static field</mark>, you usually <mark>use a non-static method</mark>
            to access it.</p>
        
        <p>if you have a <mark>non-static method</mark> that <mark>accesses a static field</mark>?
            Or a <mark>static method</mark> that accesses a <mark>non-static field </mark>(using an instance)? In
            these cases <mark>things start to get messy</mark> quickly, and there's a very good chance that
            things will not work the way you want.   </p>
        
        <p>a static synchronized method and a
            non-static synchronized method <mark>will not block each other</mark>-they can run at
            the same time.</p>      
        
        <p>if you access a static field using a non-static method,
            two threads might invoke that method using <mark>two different</mark> <mark>this</mark> instances.they won't block each other, because <mark>they use different locks</mark>.</p>
        
        <p>in order to make a class thread-safe, methods that access
            changeable fields need to be synchronized.  </p>
        
        <p> Access to <mark>static fields should be done from static</mark> synchronized methods. Access
            to <mark>non-static fields should be done from non-static</mark> synchronized methods</p>
        <pre>      
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
public class Thing {
    
    private static int staticField;
    private int nonstaticField;
        
    public static synchronized int getStaticField() {
        return staticField;
    }
        
    public static synchronized void setStaticField(
            int staticField) {
        Thing.staticField = staticField;
    }
        
    public synchronized int getNonstaticField() {
        return nonstaticField;
    }
        
    public synchronized void setNonstaticField(
            int nonstaticField) {
        this.nonstaticField = nonstaticField;
    }
}
    
        </pre>
        
        
        <h3>Thread-Safe Classes   </h3>
        
        <p>When a class <mark>has been carefully synchronized </mark>to protect its data we say the class is "<mark>thread-safe.</mark>"</p>
        
        <p>However, even when a class is "thread-safe," it is <mark>often dangerous to rely on these
            classes to provide the thread protection</mark> you need. </p>    
        
        <p>The method Collections.synchronizedList() returns a List whose methods
            are all synchronized and "thread-safe" according to the documentation</p>
        <pre>     
 package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
     
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
    
public class NameList {
    
    private List names = Collections.<mark>synchronizedList</mark>(
            new LinkedList());
                
    public void add(String name) {
        names.add(name);
    }
        
    public String removeFirst() {
        if (<mark>names.size() </mark>> 0) {
            <mark>return (String) names.remove(0);</mark>
        } else {
            <mark>return null;</mark>
        }
    }
        
    public static void main(String[] args) {
        final NameList nl = new NameList();
            
            
        nl.add("Ozymandias");
            
        class NameDropper extends Thread {
            
            public void run() {
                String name = nl.removeFirst();
                System.out.println(name);
            }
        }
        Thread t1 = new NameDropper();
        Thread t2 = new NameDropper();
        t1.start();
        t2.start();
    }
}
        </pre>
        
        <pre class='out'>       
 run:
Ozymandias
null
BUILD SUCCESSFUL (total time: 1 second)
        </pre>  
        
 <pre class='out'>       
run:
names.size()=1
names.size()=1
Ozymandias
Exception in thread "Thread-2" java.lang.IndexOutOfBoundsException
	at java.util.LinkedList.remove(Unknown Source)
	at java.util.Collections$SynchronizedList.remove(Unknown Source)
	at com.mvivekweb.ocpjp6.obj4.sect3.synchronizing.NameList.removeFirst(NameList.java:19)
	at com.mvivekweb.ocpjp6.obj4.sect3.synchronizing.NameList$1NameDropper.run(NameList.java:34)
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
        <p>      
            So names.size()
            is synchronized, and names.remove(0) is synchronized. <mark>But nothing prevents
            another thread from doing something else to the list in between those two calls</mark>.       
        </p>
        
        
        <p><mark>don't rely on Collections.synchronizedList()</mark>.
            Instead, synchronize the code yourself:  </p>
        
        
        <pre>
package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
    
import java.util.LinkedList;
import java.util.List;
    
public class NameList1 {
    
    private List names = new LinkedList();
        
    public <mark>synchronized</mark> void add(String name) {
        names.add(name);
    }
        
    public <mark>synchronized</mark> String removeFirst() {
        System.out.println("names.size()="+names.size());
        if (names.size() > 0) {
            return (String) names.remove(0);
        } else {
            return null;
        }
    }
        
    public static void main(String[] args) {
        final NameList1 nl = new NameList1();
        nl.add("Ozymandias");
        class NameDropper extends Thread {
            
            public void run() {
                String name = nl.removeFirst();
                System.out.println(name);
            }
        }
        Thread t1 = new NameDropper();
        Thread t2 = new NameDropper();
        t1.start();
        t2.start();
    }
}
        </pre>
        
   <pre class='out'>     
 run:
names.size()=1
Ozymandias
names.size()=0
null
BUILD SUCCESSFUL (total time: 1 second)       
        
    </pre> 
        
        <p>once one thread
            starts it and calls names.size(), there's no way the other thread can cut in and
            steal the last name. The other thread will <mark>just have to wait </mark>until the first thread
            completes the removeFirst() method. </p>       
        
        
        <p>The moral here is that just because a class is described as "thread-safe" doesn't
            mean it is always thread-safe. </p>      
        
        
        <h3> Thread Deadlock    </h3>
        
        <p>Deadlock
occurs when <mark>two threads are blocked</mark>, with <mark>each waiting for the other's lock</mark>.</p>
        
        <pre>      
  package com.mvivekweb.ocpjp6.obj4.sect3.synchronizing;
      
public class DeadlockRisk {
    
    private <mark>static</mark> class Resource {
        
        public int value;
    }
    private Resource resourceA = new Resource();
    private Resource resourceB = new Resource();
        
    public int read() {
        <mark>synchronized (resourceA)</mark> { // May deadlock here
            synchronized (resourceB) {
                return resourceB.value + resourceA.value;
            }
        }
    }
        
    public void write(int a, int b) {
        <mark>synchronized (resourceB) {</mark> // May deadlock here
            synchronized (resourceA) {
                resourceA.value = a;
                resourceB.value = b;
            }
        }
    }
}      
        </pre>
        
        <p> Assume that <mark>read() is started by one thread</mark> and <mark>write() is started by another.</mark>
            If there are two different threads that may read and write independently,</p>
        
        <p>The <mark>reader </mark>thread will have <mark>resourceA</mark>, the <mark>writer</mark>
            thread will have <mark>resourceB</mark>, and both will get stuck waiting for the other</p>
        
        
        <p>just swap the order of locking for
            either the reader or the writer     </p>
        
        
        <p>if you deadlock, you're dead</p>
        
        <p>There are design approaches that can help avoid
            deadlock, including strategies for always acquiring locks in a predetermined order   </p>
        
