
<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
class c1 { }
class c2 { }
 interface i1 { }
 interface i2 { }
class A extends c2 implements i1 { }
class B implements i1 implements i2 { }
class C implements c1 { }
class D extends c1, implements i2 { }
 class E extends i1, i2 { }
 class F implements i1, i2 { }
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/exam/i1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:10: error: '{' expected
class B implements i1 implements i2 { }
                     ^
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:12: error: '{' expected
class D extends c1, implements i2 { }
                  ^
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:13: error: '{' expected
 class E extends i1, i2 { }
                   ^
3 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
class c1 { }
class c2 { }
 interface i1 { }
 interface i2 { }
class A extends c2 implements i1 { }
class B implements i1 implements i2 { }
class C implements c1 { }
class D extends c1, implements i2 { }
 class E extends i1, i2 { }
<mark> class F implements i1, i2 { }</mark>
class G extends c1,c2{
    
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/exam/i1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:10: error: '{' expected
class B implements i1 implements i2 { }
                     ^
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:12: error: '{' expected
class D extends c1, implements i2 { }
                  ^
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:13: error: '{' expected
 class E extends i1, i2 { }
                   ^
com\mvivekweb\ocpjp6\obj1\sect2\exam\i1.java:15: error: '{' expected
<mark>class G extends c1,c2{</mark>
                  ^
4 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>


<pre>
    package com.mvivekweb.ocpjp6.obj1.sect2.exam;
        
        
    class c1 { }
    class c2 { }
    interface i1 { }
    interface i2 { }
    class A extends c2 implements i1 { }
    class B implements i1 implements i2 { }
    class C implements c1 { }
    class D extends c1, implements i2 { }
    class E extends i1, i2 { }
    class F implements i1, i2 { }
    class G extends c1,c2{
        
    }
        <mark>   class H extends c1 implements i1,i2{
            
            }</mark>
                
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
import java.io.IOException;
    
public interface Risky {
    
    String doStuff() throws Exception;
        
    <mark>Risky</mark> doCrazy();
        
    void doInsane();
}
    
class Bungee implements Risky {
public String doStuff() throws IOException {
 throw new IOException();
 }
 public <mark>Bungee </mark>doCrazy() { return <mark>new Bungee()</mark>; }
 public void doInsane() <mark>throws NullPointerException</mark> {
 throw <mark>new NullPointerException()</mark>;
 } }
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/exam/Risky.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
public class ForReal extends TinkerBell{
    
    
    public static void main(String[] args) {
        new ForReal().sprinkle();
    }
public void sprinkle() { System.out.println(fly() + " " + dust); }
}
abstract interface Pixie {
 abstract void sprinkle();
 static int dust = 3;
 }
 abstract class TinkerBell implements Pixie {
 String fly() { return "flying "; }
 }
     
</pre>
<pre class='out'>
run:
flying  3
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
abstract class Tool {
    int SKU;
abstract void getSKU();
}
public class Hammer {
 // insert code here
<mark>    void getSKU() { ; }
    private void getSKU1() { ; }
    protected void getSKU2() { ; }
    public void getSKU3() { ; }</mark>
}
    
</pre>

<pre>
interface Machine { }
 interface Engine { }
abstract interface Tractor extends Machine, Engine {
 void pullStuff();
 }
 class Deere implements Tractor {
 public void pullStuff() { System.out.print("pulling "); }
 }
class LT255 <mark>implements Tractor extends Deere</mark> {
public void pullStuff() { System.out.print("pulling harder "); }
 }
 public class LT155 extends Deere implements Tractor, Engine { }
     
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/exam/LT155.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect2\exam\LT155.java:13: <mark>error: '{' expected</mark>
class LT255 implements Tractor extends Deere {
                              ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
<p>
    When a class implements and extends, <mark>the extends declaration comes first</mark>.
    
</p>
<pre>
Given:
2. interface Clickable { void click(); }
3. // insert code here
Which code, inserted independently at line 3, will compile? (Choose all that apply.)
A. interface Keyboard extends Clickable { }
B. interface Keyboard extends Clickable { void click(); }
C. interface Keyboard extends Clickable { void doClick(); }
D. interface Keyboard extends Clickable { void click() { ; } }
E. interface Keyboard extends Clickable { protected void click(); }
F. interface Keyboard extends Clickable { void click(); void doClick(); }
Answer (for Objective 1.2):
A, B, C, and F are correct. When an interface extends another interface, it doesn't have
to actually implement anything'in fact, it CANNOT implement anything.
D is incorrect because the extending interface is trying to implement a method. E is
incorrect because <mark>interface methods can be only public</mark>.
    
</pre>

<pre>

package com.mvivekweb.ocpjp6.obj1.sect2.exam;


public class Grass implements Plant{
// static int greenness = 5;
// int greenness = 5;

    public static void main(String[] args) {
        int greenness = 2;
        new Grass().grow();
    }
    public void grow() {
        System.out.println(<mark>++greenness</mark>);
     }
}
interface Plant {
    int greenness = 7;
    void grow();
 }
     
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect2/exam/Grass.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect2\exam\Grass.java:15: error: cannot assign a value to final variable greenness
        System.out.println(++greenness);
                             ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
public class Grass implements Plant{
 <mark>static int greenness = 5;</mark>
// int greenness = 5;
    
    public static void main(String[] args) {
        int greenness = 2;
        new Grass().grow();
    }
    public void grow() {
        System.out.println(++greenness);
     }
}
interface Plant {
    int greenness = 7;
    void grow();
 }
     
</pre>
<pre class='out'>
run:
6
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
public class Grass implements Plant{
 //static int greenness = 5;
 <mark>int greenness = 5;</mark>
     
    public static void main(String[] args) {
        int greenness = 2;
        new Grass().grow();
    }
    public void grow() {
        System.out.println(++greenness);
     }
}
interface Plant {
    int greenness = 7;
    void grow();
 }
     
</pre>
<pre class='out'>
run:
6
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect2.exam;
    
    
public class Grass implements Plant{
 //static int greenness = 5;
 //int greenness = 5;
     
    public static void main(String[] args) {
        int greenness = 2;
        new Grass().grow();
    }
    public void grow() {
        System.out.println(<mark>greenness</mark>);
     }
}
interface Plant {
    int greenness = 7;
    void grow();
 }
     
</pre>
<pre class='out'>
run:
7
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

