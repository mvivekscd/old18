 <h3>Given a scenario, develop code that demonstrates the use of polymorphism. Further,
            determine when casting will be necessary and recognize compiler vs. runtime errors related
            to object reference casting.</h3>
        
        <h3>Polymorphism</h3>
        
        <p>any Java object that can <mark>pass</mark> more than one<mark> IS-A test</mark> can be
            considered <mark>polymorphic</mark></p>
        
        <p><mark>Other than <b>objects</b> of type Object</mark>, <mark>all Java objects are
            polymorphic</mark> in that they pass the IS-A test for their own type and for class Object.</p>
        
        
        <h3>reference variable</h3>
        
        <p> the <mark>only way</mark> to <mark>access an object</mark> is through a <mark>reference variable</mark></p>
        
        
        
        
        <p>A <mark>reference variable</mark> can be of only <mark>one type</mark>, and <mark>once declared</mark>, that <mark>type
            can never be changed</mark>    </p>
        
        
        <p>A reference is a variable, so it can be <mark>reassigned to other objects</mark>, (unless the
            reference is declared <mark>final</mark>).</p>
        
        <p>A reference variable's <mark>type</mark> determines the <mark>methods</mark> that can be invoked on
            the <mark>object the variable is referencing</mark>. </p>      
        
        
        <p>A reference variable can refer to <mark>any object of the same type</mark> as the declared
            reference, or-this is the big one-<mark>it can refer to any subtype of the
            declared type</mark>!</p>
            
      <p> A reference variable can be declared as a <mark>class type</mark> or an <mark>interface type</mark></p>
     
      
      <p>If
          the variable is declared as an <mark>interface type</mark>, it can reference <mark>any object</mark> of any
          class that <mark>implements the interface</mark>.</p>
      
      <p>Java
          supports only <mark>single inheritance</mark>! That means a class <mark>can have only one immediate
          superclass</mark>.</p>
      
          <p>A class <mark>cannot extend more than one </mark>class.  </p>      
          
          <p>multiple inheritance can lead to a scenario known as the
              <mark>"Deadly Diamond of Death,"</mark> because of the <mark>shape of the class diagram</mark> that
              can be created in a multiple inheritance design</p>
          
          <p> simply put the
              <mark>method code in superclass</mark>, and<mark> then disable the method in classes</mark> that can't be
              used. But that's a <mark>bad design choice</mark> for many reasons, including it's more errorprone,
              it makes the superclass class less cohesive</p>
          
          <p> <mark>create an 
              interface</mark>, and have <mark>only the subclasses</mark> that can be used implement
              that interface.  </p>       
          
          
          <p><b>Polymorphic method invocations apply only to <mark>instance methods</mark>. You can
              always refer to an object with a more general reference variable type (a superclass
              or interface), but at runtime, the ONLY things that are dynamically
              selected based on the actual object (rather than the reference type) are instance
              methods. Not static methods. Not variables. <mark>Only overridden instance methods</mark>
              are dynamically invoked based on the real object's type.</b></p>
          
          
              <p> Subclasses of a class can define their <mark>own unique behaviors </mark>and yet <mark>share some of the same functionality</mark> of the parent class.
              </p>
              <pre>
              package com.mvivekweb.ocpjp6.obj5.sect2.polymorphism;
                  
public class GameShape {
    
    public void displayShape() {
        System.out.println("displaying game shape");
    }
}
              </pre>
              
              <pre>              
package com.mvivekweb.ocpjp6.obj5.sect2.polymorphism;
    
    
public interface Animatable {
    public void animate();
} 
              </pre>
              
              <pre>             
 package com.mvivekweb.ocpjp6.obj5.sect2.polymorphism;
     
public class PlayerPiece <mark>extends GameShape implements Animatable</mark> {
    
    public void animate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    <mark>public void movePiece() {</mark>
        System.out.println("moving game piece");
    }
        
}   </pre>  
              
              <p> PlayerPiece can be
                  treated polymorphically as one of four things </p>
              
              PlayerPiece is a <mark>Object</mark>
    <br>    PlayerPiece is a <mark>GameShape</mark>
    <br>    PlayerPiece is a <mark>PlayerPiece</mark>
    <br>    PlayerPiece is a Animatable

    <h3>Reference Variable Casting   </h3>
    
    <pre>
    package com.mvivekweb.ocpjp6.obj5.sect2.referencecasting;
        
public class CastTest2 {
    
    public static void main(String[] args) {
        <mark>Animal[] a = {new Animal(), new Dog(), new Animal()};</mark>
        for (Animal animal : a) {
            animal.makeNoise();
            if (animal instanceof Dog) {
                <mark>animal.playDead(); // try to do a Dog behavior ?</mark>
            }
        }
    }
        
}
    
class Animal {
    
    void makeNoise() {
        System.out.println("generic noise");
    }
}
    
class Dog extends Animal {
    
    void makeNoise() {
        System.out.println("bark");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}
    
    </pre>
    
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/referencecasting/CastTest2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect2\referencecasting\CastTest2.java:10: error: <mark>cannot find symbol</mark>
                animal.playDead(); // try to do a Dog behavior ?
                      ^
  <mark>symbol:   method playDead()
  location: variable animal of type Animal</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>    
    
</pre>
    
    
    <pre>  
 package com.mvivekweb.ocpjp6.obj5.sect2.referencecasting;
     
public class CastTest2 {
    
    public static void main(String[] args) {
        Animal[] a = {new Animal(), new Dog(), new Animal()};
        for (Animal animal : a) {
            animal.makeNoise();
            if (animal instanceof Dog) {
                <mark>Dog d = (Dog) animal;
                d.playDead(); // try to do a Dog behavior ?</mark>
            }
        }
    }
        
}
    
class Animal {
    
    void makeNoise() {
        System.out.println("generic noise");
    }
}
    
class Dog extends Animal {
    
    void makeNoise() {
        System.out.println("bark");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}
    </pre>
   <pre class='out'> 
 run:
generic noise
<mark>bark
roll over</mark>
generic noise
BUILD SUCCESSFUL (total time: 0 seconds)   
</pre>
    
    <p>The new and improved code block contains a cast, which in this case is
        sometimes called a <mark>downcast</mark>, because we're casting down the inheritance tree to a
        more specific class.  </p>
    
    
    <pre> 
  package com.mvivekweb.ocpjp6.obj5.sect2.referencecasting;
      
public class CastTest2 {
    
    public static void main(String[] args) {
        Animal[] a = {new Animal(), new Dog(), new Cat()};
        for (Animal animal : a) {
            animal.makeNoise();
                
<mark>            Dog d = (Dog) animal;
            d.playDead(); // try to do a Dog behavior ?
                
            Cat c = (Cat) animal;
            c.playDead(); // try to do a Dog behavior ?</mark>
                
        }
    }
        
}
    
class Animal {
    
    void makeNoise() {
        System.out.println("generic noise");
    }
}
    
class Dog extends Animal {
    
    void makeNoise() {
        System.out.println("bark");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}
    
class Cat extends Animal {
    
    void makeNoise() {
        System.out.println("miow");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}
    </pre>
    
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect2/referencecasting/CastTest2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj5/sect2/referencecasting/CastTest2
generic noise
<mark>Exception in thread "main" java.lang.ClassCastException:</mark> com.mvivekweb.ocpjp6.obj5.sect2.referencecasting.Animal cannot be cast to com.mvivekweb.ocpjp6.obj5.sect2.referencecasting.Dog
        at com.mvivekweb.ocpjp6.obj5.sect2.referencecasting.CastTest2.main(CastTest2.java:10)

C:\vivek\java7\ocpjp6\ocpjp6\src>    
    
</pre>
    
    <pre>   
 package com.mvivekweb.ocpjp6.obj5.sect2.referencecasting;
     
public class CastTest2 {
    
    public static void main(String[] args) {
        Animal[] a = {new Animal(), new Dog(), new Cat()};
        for (Animal animal : a) {
            animal.makeNoise();
            if (animal instanceof Dog) {
                Dog d = (Dog) animal;
                d.playDead(); // try to do a Dog behavior ?
            }
            <mark>if (animal instanceof Cat) {
                Cat c = (Cat) animal;
                c.playDead(); // try to do a Dog behavior ?
            }</mark>
        }
    }
        
}
    
class Animal {
    
    void makeNoise() {
        System.out.println("generic noise");
    }
}
    
class Dog extends Animal {
    
    void makeNoise() {
        System.out.println("bark");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}
    
class Cat extends Animal {
    
    void makeNoise() {
        System.out.println("miow");
    }
        
    void playDead() {
        System.out.println("roll over");
    }
}   
    </pre>
    
<pre class='out'>    
run:
generic noise
bark
roll over
miow
roll over
BUILD SUCCESSFUL (total time: 0 seconds) 
</pre>
    <pre>   
package com.mvivekweb.ocpjp6.obj5.sect2.referencecasting;
    
public class DogTest {
    
    public static void main(String[] args) {
        Dog d = new Dog();
        Animal a1 = d; // <mark>upcast ok with no explicit cast</mark>
        Animal a2 = (Animal) d; // upcast ok with an explicit cast
    }
        
}
    
class Animal1 {
}
    
class Dog1 extends Animal1 {
}    </pre>
    
    <p>   
        The compiler and JVM know it too, so the
        <mark>implicit upcast is always legal</mark> for assigning an object of a subtype to a reference of
        one of its supertype classes (or interfaces).</p>

<img src="/imag/jp6/polyinh.png" class="imgw"> 
<img src="/imag/jp6/polyinh1.png" class="imgw"> 
<img src="/imag/jp6/polyinh2.png" class="imgw"> 
<img src="/imag/jp6/polyinh3.png" class="imgw"> 
<img src="/imag/jp6/polyinh4.png" class="imgw"> 







    
    
    
    
    