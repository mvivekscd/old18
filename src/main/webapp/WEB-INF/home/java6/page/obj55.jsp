 <h3>Develop code that implements "is-a" and/or "has-a" relationships.</h3>
        
        <p>instanceof returns true
            if the reference variable being tested is of the <mark>type</mark> being compared to. and <mark>not the object</mark>.</p>
        
        <pre>
        package com.mvivekweb.ocpjp6.obj5.sect5.inheritance;
            
public class Test {
    
    public static void main(String[] args) {
        Test t1 = new Test();
        Test t2 = new Test();
        if (t1.<mark>equals</mark>(t2)) {
            System.out.println("they're equal");
        }else{
            System.out.println("they're not equal");
        }
        if (t1 <mark>instanceof </mark>Object) {
            System.out.println("t1's an Object");
        }
    }
}</pre>
 <pre class='out'>       
run:
they're not equal
t1's an Object
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>   
        
        <p>every class in Java is
            a<mark> subclass of class Object</mark>   </p>     
        
        <p>every
            class you'll ever use or ever <mark>write</mark> <mark>will inherit from class Object</mark>. You'll always have
            an <mark>equals</mark> method, a <mark>clone</mark> method, <mark>notify, wait</mark>, and others, available to use.</p>
        
        <p>Whenever you create a class, you automatically inherit all of class Object's methods   </p>
        
        <p>would be very common for Java programmers to want to compare
            instances of their classes to check for equality. <mark>If class Object didn't have an equals</mark>
            method, <mark>you'd have to write one yourself</mark>   </p>     
 
        <p>create inheritance relationships
            in Java by <mark>extending</mark> a class  </p>
        
        <p>reasons to use inheritance are <b>To promote code reuse </b>  and <b>To use polymorphism (<mark>"many forms"</mark>) </b>   </p>
        
        <pre> 
package com.mvivekweb.ocpjp6.obj5.sect5.inheritance;
    
public class TestShapes {
    
    public static void main(String[] args) {
        PlayerPiece shape = new PlayerPiece();// only one object
<mark>        shape.displayShape();
        shape.movePiece();</mark>
            
        if(shape <mark>instanceof</mark> PlayerPiece){
            System.out.println("playerPiece instance");
        }
         if(shape <mark>instanceof</mark> GameShape){
            System.out.println("GameShape instance");
        }
    }
}
    
class GameShape {
    
    public void displayShape() {
        System.out.println("displaying shape");
    }
// more code
}
    
class PlayerPiece extends GameShape {
    
    public void movePiece() {
        System.out.println("moving game piece");
    }
// more code
}        
        </pre>

 <pre class='out'>       
run:
displaying shape
moving game piece
playerPiece instance
GameShape instance
BUILD SUCCESSFUL (total time: 1 second)
    </pre>    
        
        <pre>
package com.mvivekweb.ocpjp6.obj5.sect5.inheritance;
    
public class TestShapes1 {
    
    public static void main(String[] args) {
        PlayerPiece1 player = new PlayerPiece1();
        TilePiece tile = new TilePiece();
        doShapes(<mark>player</mark>);
        doShapes(<mark>tile</mark>);
    }
        
    public static void doShapes(<mark>GameShape1 shape</mark>) {
        shape.<mark>displayShape();</mark>
    }
}
    
class GameShape1 {
    
    public void <mark>displayShape()</mark> {
        System.out.println("displaying gameshape shape");
    }
// more code
}
    
class PlayerPiece1 extends GameShape1 {
    
    public void movePiece() {
        System.out.println("moving game piece");
    }
// more code
}
    
class TilePiece extends GameShape1 {
    
    public void getAdjacent() {
        System.out.println("getting adjacent tiles");
    }
// more code
}     
        </pre>
 <pre class='out'>       
run:
displaying gameshape shape
displaying gameshape shape
BUILD SUCCESSFUL (total time: 2 seconds)        
    </pre>   
        
        
        <p> The key point is that the  method is declared with a 
            <mark>argument</mark> but <mark>can be passed any subtype</mark>    </p>   
       
        <p> means that only the
            <mark>methods of superclass</mark> can be invoked on it.unless <mark>over riden </mark>  </p>    
        
        <h3>IS-A and HAS-A Relationships</h3>
        
        <p>For the exam you need to be able to <mark>look at code and determin</mark>e whether the code
            demonstrates an IS-A or HAS-A relationship  </p>      
        
        <h3>IS-A  </h3>
        <p>        
            IS-A is a way of saying, "<mark>this thing is a type of that thing</mark>." </p>      
        <pre>       
public class Vehicle { ... }
public class Car <mark>extends</mark> Vehicle { ... }
public class Subaru <mark>extends</mark> Car { ... }        
        </pre>
        
        <pre>      Car <mark>IS-A</mark> Vehicle
        Subaru <mark>IS-A </mark> Car</pre>
        
        <pre> Car <mark>instanceOf</mark> Vehicle</pre>
        
        <h3>HAS-A </h3>
        
        <p>HAS-A relationships are <mark>based on usage</mark>, <mark>rather than inheritance</mark> </p>       
        
        <p> class A <mark>HAS-A</mark> B if code in class A has a reference to an instance of class B</p>
        <pre>
public class Animal { }
public class Horse extends Animal {
private <mark>Halter</mark> myHalter;
}        
        </pre>     
        
        <pre> A Horse IS-A Animal. A Horse <mark>HAS-A</mark> Halter.   </pre>    
        
        <p> HAS-A relationships allow you to design classes that follow good OO practices
            by <mark>not having monolithic classes</mark> that do a gazillion different things.</p>
        
        <p>The reason for the
            emphasis on proper design is simple: <mark>money</mark>   </p>   
        
        <p><mark>Unified Modeling Language (UML)</mark>, allow
            designers to design and easily modify classes
            without having to write code first,</p>
      
        <p>Another innovation
            in object-oriented design is <mark>design patterns</mark>.</p>
        
        <h3>delegation </h3>
        
        <pre>
public class Horse extends Animal {
private <mark>Halter</mark> myHalter = new Halter();
public void tie(LeadRope rope) {
<mark>myHalter.tie(rope); </mark>//<mark> Delegate</mark> tie behavior to the
// Halter object
}
}
public class Halter {
public void tie(LeadRope aRope) {
// Do the actual tie work here
}
}        
        </pre>    
        
        <p>we don't want callers to worry about which class or which object
            is actually doing the real work  </p>
        
        
        <p>the Horse class hides
            implementation details from Horse users</p>
        
        <p>Users of a Horse <mark>should not even need to know
            that there is such a thing as a Halter</mark> class </p>   
        
        