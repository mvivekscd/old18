<pre>Given that the current directory is bigApp, and the following directory structure:
bigApp
     |-- classes
           |-- com
                 |-- wickedlysmart
                            |-- BigAppMain.class
And the code:
package com.wickedlysmart;
public class BigAppMain {
public static void main(String[] args) {
System.out.println("big app");
}
}
Which will invoke BigAppMain? (Choose all that apply.)</pre>

<pre class='out'>java -cp classes com.wickedlysmart.BigAppMain</pre>

<pre class='out'>java -cp <mark>.:classes</mark> com.wickedlysmart.BigAppMain</pre>

<p>2nd will be a tiny bit slower than 1st, because the .: tells the JVM to
    look in the current directory (in this case bigApp), before looking in classes.</p>

<pre>
Given the current directory is bigApp, and the directory structure:
bigApp
      |-- classes
             |-- Cloned.class
And the file:
public class Cloned {
public static void main(String[] args) {
System.out.println("classes");
assert(Integer.parseInt(args[0]) > 0);
} }
Which will produce the output "classes" followed by an AssertionError? (Choose all
that apply.)
    
</pre> 

<pre class='out'>java -ea -cp classes Cloned -4</pre>

<pre class='out'><mark>java -cp classes -ea Cloned</mark></pre>
<p>throws
    an <mark>ArrayIndexOutOfBoundsException</mark></p>

<pre>
Given that the working directory is bigApp, and the following directory structure:
bigApp
    |-- classes
    |      |-- com
    |            |-- wickedlysmart
    |-- source
           |-- com
                 |-- wickedlysmart
                          |-- BigAppClass2.java
And the code:
1. public class BigAppClass2 { int doMore() { return 17; } }
And the following command-line invocations:
I. javac -d source/com/wickedlysmart/BigAppClass2.java
II. javac -d classes source/com/wickedlysmart/BigAppClass2.java
III. javac -d classes/com/wickedlysmart source/com/wickedlysmart/BigAppClass2.java
Which are true? (Choose all that apply.)</pre>

<pre class='out'>Invocation II will compile the file and place the .class file in the classes directory.</pre>
<pre class='out'>
Invocation III will compile the file and place the .class file in the wickedlysmart directory.
</pre>

<p> Note that the
   <mark> BigAppClass2 class doesn't have a package statement</mark>.</p>

<p>The -D flag is used in conjunction with a name-value pair.</p>

<p>The -d flag can be used with javac to specify where to place .class files.</p>

<p>-D is used with the <mark>java command</mark> to set a system property.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect2.exam;
    
import java.util.Properties;
    
    
public class PirateTalk {
    
    
    public static void main(String[] args) {
        Properties p = System.getProperties();
 p.setProperty("pirate", "scurvy");
 String s = p.getProperty("argProp") + " ";
 s += p.getProperty("pirate");
 System.out.println(s);
    }
        
}
    
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect2/exam/PirateTalk.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java -DargProp="dog," com/mvivekweb/ocpjp6/obj7/sect2/exam/PirateTalk
dog, scurvy

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj7/sect2/exam/PirateTalk</mark> -DargProp="dog,"
<mark>null scurvy</mark>

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect2.exam;
    
    
public class Avast {
    
static public void main(String[] scurvy) {
 System.out.print(scurvy[1] + " ");
 main(scurvy[2]);
 }
 public static void main(String dogs) {
 assert(dogs == null);
 System.out.println(dogs);
 }
     
}
</pre>

<pre>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect2/exam/Avast.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj7/sect2/exam/Avast</mark> -ea 1 2 3
1 2
    
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>
<pre>
Given the current directory is bigApp, and the directory structure:
bigApp
     |-- classes
            |-- Cloned.class
            |-- com
                  |-- Cloned.class
                  |-- wickedlysmart
                              |-- Cloned.class
And the three files:
public class Cloned {
public static void main(String[] args) { System.out.println("classes"); }
}
public class Cloned {
public static void main(String[] args) { System.out.println("com"); }
}
public class Cloned {
public static void main(String[] args) { System.out.println("ws"); }
}
Have been compiled into the classes, com, and wickedlysmart directories, respectively.
Which will produce the output "ws"? (Choose all that apply.)
    
</pre>

<pre class='out'>java -cp classes/com/wickedlysmart Cloned</pre>

<pre class='out'>java -cp classes/com/wickedlysmart:classes Cloned</pre>

<pre class='out'>java -cp .:classes/com/wickedlysmart:classes Cloned</pre>

<pre>
Given that the current directory is bigApp, and the following directory structure:
bigApp
     |-- classes
     |-- source
            |-- com
                  |-- wickedlysmart
                           |-- BigAppClass1.java
And the code:
package com.wickedlysmart;
public class BigAppClass1 {
int doStuff() { return 42; }
}
And the following command-line invocations:
I. javac -d source/com/wickedlysmart/BigAppClass1.java
<mark>II. javac -d classes source/com/wickedlysmart/BigAppClass1.java</mark>
III. javac -d classes/com/wickedlysmart source/com/wickedlysmart/BigAppClass1.java
Which are true? (Choose all that apply.)
</pre>

<pre class='out'>Under the bigApp/classes directory, invocation II will build com/wickedlysmart
     subdirectories, and place the .class file in wickedlysmart.</pre>


