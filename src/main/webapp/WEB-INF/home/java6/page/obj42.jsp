<h3>Recognize the states in which a thread can exist, and identify ways in which a thread
            can transition from one state to another.</h3>
        
        
        <p>  The thread scheduler's job is to move threads in and out of the
            <mark>running</mark> state</p>
        
        
        <p>While the thread scheduler can move a thread from the running state
            back to runnable</p>
        
        <p>when the thread's run()method completes, in
            which case the <mark>thread moves from the running state directly to the dead state</mark>.</p>
        
        <h3>   Thread States </h3>
        
        
        <p>A thread can be only in one of five states</p>
        
        
        <p><b>1. New</b>- This is the state the thread is in after the <mark>Thread instance has been
            created</mark>, but the <mark>start() method has not been invoked</mark> on the thread</p>
        
        <p><b>2. Runnable</b>- This is the state a thread is in when it's eligible to run, but the
            <mark>scheduler has not selected</mark> it to be the running thread.</p>
        
        
        <p>thread first <mark>enters
            the runnable state</mark> when the <mark>start() method is invoked</mark></p>
        
        <p> When the thread is in the <mark>runnable state</mark>,
            it is considered <mark>alive</mark>.</p>
 
        <p><b>3. Running</b>-    This is the state a
            thread is in when the <mark>thread scheduler selects </mark>it (from the runnable pool) to
            be the <mark>currently executing process</mark>.  </p>   
        
        <p>but <mark>only one way to</mark>  get to the <mark>running</mark>
            state: the scheduler chooses a thread <mark>from the runnable pool</mark></p>
        
    
        <p><b>4.Waiting/blocked/sleeping </b>- This is the state a thread is in when it's <mark>not
            eligible to run.</mark>  </p>
        
        
        <p>The important point is that <mark>one thread does not tell another thread to block</mark>. Some <mark>methods may look like they tell
            another thread to block, but they don't</mark>.</p>
        
            <p>If you have a reference t to another
                thread, you can write something like this: </p>
            <pre>
            t.sleep(); or t.yield()
            </pre>
            
            <p>But those are actually <mark>static methods of the Thread class</mark>-they <mark>don't affect the
                instance t;</mark> instead they are defined to always <mark>affect the thread that's currently
                executing.</mark></p>
        
                <p>Both <mark>suspend()</mark>
                    and <mark>stop()</mark> turned out to be <mark>very dangerous</mark>, so you shouldn't use them</p>
        
                <p>Note also that a thread in a blocked state is still
                    considered to be <mark>alive</mark>.</p>
                
                <p> <b>5. Dead</b>-   A thread is considered dead when its run() method completes  </p>           
     
                <p>It
                    may still be a <mark><b>viable Thread object</b></mark>, but it is <mark>no longer a separate <b>thread of
                    execution</b></mark>.   </p>
                
                <p> Once a thread is dead, it can never be brought back to life!</p>
                
                <h3> Preventing Thread Execution   </h3>            
                
                
<p>looking for your ability to recognize when a thread will get
                    kicked out of running but not be sent back to either runnable or dead. </p>
                
                
<h3>Sleeping   </h3>

<p>The sleep() method is a <mark>static</mark> method of <mark>class Thread</mark></p>
                
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.threadstates;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
class NameRunnable implements Runnable {
    
    @Override
    public void run() {
        for (int x = 1; x < 4; x++) {
            System.out.println("Run by "
                    + Thread.currentThread().getName());
            try {
                <mark>Thread.sleep(10*1000);</mark>
                    
            } catch (InterruptedException ex) {
            }
                
                
        }
    }
}
    
public class ManyNames {
    
    public static void main(String[] args) {
// Make one Runnable
        NameRunnable nr = new NameRunnable();
        Thread one = new Thread(nr);
        one.setName("Fred");
        Thread two = new Thread(nr);
        two.setName("Lucy");
        Thread three = new Thread(nr);
        three.setName("Ricky");
        one.start();
        two.start();
        three.start();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(ManyNames.class.getName()).log(Level.SEVERE, null, ex);
//        }
        System.out.println("one.getState()="+one.getState());
        System.out.println("two.getState()="+two.getState());
        System.out.println("three.getState()="+three.getState());
            
            
            
    }
}
    
</pre>
<pre class='out'>
run:
Run by Lucy
one.getState()=<mark>BLOCKED</mark>
Run by Ricky
<mark>two.getState()=TIMED_WAITING</mark>
Run by Fred
three.getState()=TIMED_WAITING
Run by Ricky
Run by Lucy
Run by Fred
Run by Lucy
Run by Ricky
Run by Fred
BUILD SUCCESSFUL (total time: 31 seconds)
    
</pre>          
 
<p>Still, using sleep()
    is the <mark>best way</mark> to help all threads get a <mark>chance to run</mark></p>


<p>Remember, when a thread wakes up, it simply goes back to
    the <mark>runnable state</mark>.</p>



<p>So the time specifi ed in sleep() is the minimum duration in which
    the thread won't run, but it is not the exact duration in which the thread won't run. So
    <mark>you can't, for example, rely on the sleep() method to give you a perfectly accurate
        timer.</mark></p>

<p>you must know that a sleep() time is <mark>not a guarantee</mark> that the thread will start running
again as soon as the time expires and the thread wakes</p>

<p><mark>don't be fooled</mark> into thinking that
    one thread can put another thread to sleep</p>

<p>sleep() call, it puts the currently running
    thread to sleep.</p>

<h3>Thread Priorities and yield()</h3>

<p>To understand yield(), you must understand the concept of thread priorities.</p>

<p>Threads always run with some priority, usually represented as a number between 1
    and 10</p>

<p>Although many JVMs do use <mark>time slicing</mark>, some may use
    a <mark>scheduler</mark> that <mark>lets one thread stay running until the thread</mark> completes its run()
    method.</p>

<p>In most JVMs, however, the <mark>scheduler</mark> does use thread priorities in one important
    way: If a thread enters the <mark>runnable state</mark>, and it has a higher priority than any of
    the threads in the pool and a higher priority than the currently running thread,
    the lower-priority running thread usually will be bumped back to runnable and the
    highest-priority thread will be chosen to run.</p>

<p>you must <mark>never rely</mark> on
    <mark>thread priorities</mark> to guarantee the correct behavior of your program.</p>

<p>use thread
    priorities as a way to <mark>improve the efficiency</mark> of your program, but just be sure
    your program doesn't depend on that behavior for correctness.</p>


<p>All priorities being equal, a JVM implementation of the scheduler is free to do
    just about anything it likes</p>


<p>Pick a thread to run, and run it there until it blocks or completes. or Time slice the threads in the pool to give everyone an equal opportunity to run</p>

<h3>Setting a Thread's Priority</h3>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.threadstates;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
class FooRunnable implements Runnable {
    @Override
    public void run() {
        for (int x = 1; x < 6; x++) {
            System.out.println("Runnable running");
        }
    }
}
    
public class TestThreads1 {
    public static void main(String[] args) {
        FooRunnable r = new FooRunnable();
        Thread t = new Thread(r);
        t.setPriority(8);
        t.start();
//        try {
//            t.join();
//        } catch (InterruptedException ex) {
//            Logger.getLogger(TestThreads1.class.getName()).log(Level.SEVERE, null, ex);
//        }
    
        System.out.println("t.getPriority()="+<mark>t.getPriority()</mark>);
        System.out.println(Thread.currentThread().getName()+ " current thread "+<mark>Thread.currentThread().getPriority()</mark>);
    }
}
    
</pre>
<pre class='out'>
run:
t.getPriority()=8
Runnable running
main current thread <mark>5</mark>
Runnable running
Runnable running
Runnable running
Runnable running
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>Such a JVM might
    merge values from 1 to 10 down to maybe values from 1 to 5</p>

<p>the default priority is <mark>5</mark></p>

<p>the Thread class has the three following
    <mark>constants </mark>(static final variables)</p>

<pre>
Thread.MIN_PRIORITY (1)
Thread.NORM_PRIORITY (5)
Thread.MAX_PRIORITY (10)
</pre>

<h3>The yield( ) Method</h3>

<p>What yield() is supposed to do is
    <mark> make the currently running thread head back to runnable</mark> to allow <mark>other threads of
    the same priority</mark> to get their turn</p>


<p>use yield() to promote
    graceful <mark>turn-taking among equal-priority threads</mark></p>

<p>there's <mark>no guarantee</mark> the yielding
    thread won't just be chosen again over all the others!</p>

<p>A yield() <mark>won't ever</mark> cause a thread to <mark>go to the waiting/sleeping/ blocking</mark>
    state.</p>

<p>a yield() will cause a thread to go from<mark> running to runnable</mark>, but
    again, it might have no effect at all.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.threadstates;
    
    
public class yieldtest {
    
    
    public static void main(String[] args) {
        yieldrunnable runa = new yieldrunnable();
        Thread th= new Thread(runa);
        th.start();
        Thread th1= new Thread(runa);
        th1.start();
    }
        
}
class yieldrunnable implements Runnable{
    
    public void run() {
       Thread.yield();
        System.out.println(Thread.currentThread().getName()+"= "+Thread.currentThread().getState());
    }
        
}
</pre>
<pre class='out'>
run:
Thread-2= RUNNABLE
Thread-3= RUNNABLE
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
<h3>The join( ) Method</h3>

<p>The <mark>non-static</mark> join() method of class Thread</p>

<p>lets one thread "<mark>join onto the end</mark>"
    of another thread</p>


<p>If you have a thread B that can't do its work until another thread
    A has completed its work, then you want thread B to "join" thread A</p>

<p>This means that
    thread B will not become runnable until A has finished (and <mark>entered the dead state</mark>).</p>



<pre>
    
package com.mvivekweb.ocpjp6.obj4.sect2.threadstates;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
class MyRunnable implements Runnable{
    
    public void run() {
        System.out.println("MyRunnable run");
         System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
    }
        
}
    
public class JoinThread {
    
    
    public static void main(String[] args) {
        MyRunnable myR = new MyRunnable();
        Thread t = new Thread(myR);
        t.start();
       // try {
         //   t.join();
            System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
      //  } catch (InterruptedException ex) {
       //     Logger.getLogger(JoinThread.class.getName()).log(Level.SEVERE, null, ex);
       // }
           
    }
        
}
    
</pre>
<pre class='out'>
run:
MyRunnable run
<mark>main state=RUNNABLE
Thread-2 state=RUNNABLE</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.threadstates;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
class MyRunnable implements Runnable{
    
    public void run() {
        System.out.println("MyRunnable run");
         System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
    }
        
}
    
public class JoinThread {
    
    
    public static void main(String[] args) {
        MyRunnable myR = new MyRunnable();
        Thread t = new Thread(myR);
        t.start();
        try {
            <mark>t.join();</mark>
            System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
       } catch (InterruptedException ex) {
            Logger.getLogger(JoinThread.class.getName()).log(Level.SEVERE, null, ex);
       }
           
    }
        
}
    
</pre>
<pre class='out'>
run:
MyRunnable run
<mark>Thread-2 state=RUNNABLE
main state=RUNNABLE</mark>
BUILD SUCCESSFUL (total time: 1 second)
    </pre> 

<p>code takes the currently running thread (if this were in the
    <mark>main()</mark> method, then that would be the main thread) and <mark>joins it to the end of the
    thread referenced by t</mark></p>
    
    <p><mark>overloaded versions</mark> of join() that takes a timeout duration </p>   
    
    <pre>
import java.util.logging.Level;
import java.util.logging.Logger;
    
class MyRunnable1 implements Runnable{
    
    public void run() {
        System.out.println("MyRunnable1 run");
         System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
    }
        
}
    
public class JoinThread1 {
    
    
    public static void main(String[] args) {
        MyRunnable1 myR = new MyRunnable1();
        Thread t = new Thread(myR);
        t.start();
        try {
            <mark>t.join(5000);</mark>
           <mark> System.out.println("t.getState()="+t.getState());</mark>
            System.out.println(Thread.currentThread().getName() +" state="+ Thread.currentThread().getState());
       } catch (InterruptedException ex) {
            Logger.getLogger(JoinThread.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
        
}
    
    </pre>
 <pre class='out'>   
 run:
MyRunnable1 run
Thread-2 state=RUNNABLE
<mark>t.getState()=TERMINATED</mark>
main state=RUNNABLE
BUILD SUCCESSFUL (total time: 1 second)   
    </pre>
    
     <img src="/imag/jp6/threadj.png" class="imgw">
    
    <p>   
        <mark>wait </mark> until thread t is done, but if it takes longer than 5,000
        milliseconds, then stop waiting and become runnable anyway    
    </p>
    
    <p>A call to <mark>sleep()</mark> <mark>Guaranteed</mark> to cause the current thread to stop executing
        for at least the specified sleep duration (although it <mark>might be interrupted</mark>
        before its specified time).  </p>
    
    <p>A call to <mark>yield()</mark> <mark>Not guaranteed</mark> to do much of anything, although
        typically it will cause the currently running thread to move back to runnable
        so that a thread of the same priority can have a chance.</p>
    
    
    <p> A call to <mark>join()</mark> <mark>Guaranteed</mark> to cause the <mark>current thread to stop executing
        until the thread it joins with</mark> (in other words, the thread it calls join() on) completes, or if the thread it's trying to join with is not alive, however,
        the current thread won't need to back out.</p>
    
    <p>following scenarios in which a thread might
        leave the running state:</p>
    
    The thread's <mark>run()</mark> method <mark>completes</mark>. Duh.
    <br>    <mark>A call to wait() on an object</mark> (<mark>we don't call</mark> wait() on a thread, as we'll
    see in a moment).
    <br>        A <mark>thread can't acquire the lock</mark> on the object whose method code it's
attempting to run.
    <br>The <mark>thread scheduler can decide</mark> to move the current thread from running
    to runnable in order to give another thread a chance to run. <mark>No reason is
needed</mark>-the thread scheduler can trade threads in and out whenever it likes.


