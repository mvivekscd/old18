 <h3>Develop code that uses the primitive wrapper classes (such as Boolean, Character, Double, Integer, and so on), and/or autoboxing & unboxing. Discuss the differences between the String, StringBuilder, and StringBuffer classes.</h3>
        <p> The wrapper classes correlate to the primitive types.</p>
        <p> Wrappers have two main functions:</p>
        a) To <mark>wrap primitives</mark> so that they can be handled like <mark>objects</mark><br>
        b) To provide utility methods for primitives (usually conversions)<br>
        converting <mark>primitives</mark> to and from <mark>String objects</mark> <br>
        converting <mark>primitives</mark> and <mark>String objects</mark> to and from different <mark>bases</mark><br>
        <p> <b>Autoboxing</b></p>
        <mark>wrapping operations</mark> that
        <mark>programmers used to do manually</mark> are now handled <mark>automatically</mark>.<br>
        <table class="alt">
<tbody><tr><th>Primitive Type</th><th>Wrapper class</th></tr>
<tr><td>boolean</td><td>Boolean</td></tr>
<tr><td>char</td><td>Character</td></tr>
<tr><td>byte</td><td>Byte</td></tr>
<tr><td>short</td><td>Short</td></tr>
<tr><td>int</td><td>Integer</td></tr>
<tr><td>long</td><td>Long</td></tr>
<tr><td>float</td><td>Float</td></tr>
<tr><td>double</td><td>Double</td></tr>
</tbody></table>
        <p>BCBSILFD</p>
        
        <p> three most common approaches for
            creating wrapper objects.</p>
        
        <p><b>NumberFormatException</b></p>
        if the <mark>String</mark> provided <mark>cannot</mark> be <mark>parsed into</mark> the appropriate <mark>primitive</mark>. For example
        "two" can't be parsed into "2".<br>
        <p> <b>Wrapper objects are immutable</b></p>
        Once they have
        been given a value, that <mark>value cannot be changed</mark>.<br>
        <p> The three most important method families are
            <br> xxxValue() Takes no arguments, returns a primitive
             <br>parseXxx() Takes a String, returns a primitive, throws NFE
             <br>valueOf() Takes a String, returns a wrapped object, throws NFE</p>

        <p> Wrapper constructors can take a <mark>String or a primitive</mark>, except for Character,
            which can only take a <mark>char</mark>.</p>
        <p>        There are times, however, when you need to use a <mark>char</mark> as an object-for example, as a method argument where an object is expected. The Java programming language provides a wrapper class that "wraps" the char in a Character object for this purpose.
        </p>
        
        <pre>     You can create a Character object with the Character constructor:
       
Character ch = new Character('a');</pre>
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
public class ex001 {
    
    public static void main(String[] args) {
        // TODO code application logic here
            
        Integer i1 = new Integer(42);
        Integer i2 = new Integer("42");
            
        Float f1 = new Float(3.14f);
        Float f2 = new Float("3.14f");
            
        Character c1 = new Character('c');
        Character a = new Character('3');
        System.out.println("c2="+a.charValue()); 
<mark>        Character a1 = new Character('31');
    
         Character c3 = new Character(c); // not allowed</mark>
    }
        
}
        </pre>     
<pre class='out'>        
run:
c2=3
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - Erroneous ctor sym type: <any>
	at com.mvivekweb.ocpjp6.obj3.sec1.wrappers.ex001.main(ex001.java:18)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)        
        
</pre> 
        <p>As of Java 5, a <b>Boolean object can be used in a boolean test</b>, because the compiler
            will automatically "unbox" the Boolean to a boolean.</p>
        <pre>   
   package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
       
public class bool01 {
    
    public static void main(String[] args) {
        // TODO code application logic here
        Boolean b = new Boolean("true");
       <mark> if (b){</mark>
            System.out.println("true");
        }
    }
}             
    
        </pre>     
        <pre class='out'>        
run:
true
BUILD SUCCESSFUL (total time: 2 seconds)
        </pre>  
        
<pre class='out'>        
C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj3\sec1\wrappers>javac <mark>-source 1.4</mark> bool01.java
bool01.java:9: incompatible types
found   : java.lang.Boolean
required: boolean
        if (b){
            ^
1 error

C:\vivek\docs\cert\ocpjp6\src\com\mvivekweb\ocpjp6\obj3\sec1\wrappers>
    </pre> 
        
        <table border="1"><tr>
                <td>primitive xxxValue() - to convert a Wrapper to a primitive</td><td>Object to number</td>
            </tr><tr>
                <td>primitive parseXxx(String) - to convert a String to a primitive throws NFE</td><td>String to number</td>
                </tr>
                <tr>
                <td>Wrapper <mark>valueOf</mark>(String) - to convert a String to a Wrapper throws NFE</td><td><mark>String to Object</mark></td>
            </tr></table>
        
        <p>  <mark>VOS</mark> - object</p>
        <pre>   

     
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
public class ex002 {
    
    
    public static void main(String[] args) {
        <mark>// valueOf(String) String to wrapper</mark>
        Integer i2 = Integer.<mark>valueOf</mark>("101011", <mark>2</mark>); // converts 101011
        System.err.println("i2="+i2.toString());
            
        Float f2 = Float.<mark>valueOf</mark>("3");
        System.err.println("f2="+f2.toString());
            
//        Integer i4 = Integer.valueOf("101011.1");
//        System.err.println("i4="+i4.toString());
    
        <mark>//xxxValue() Wrapper to primitive</mark>
            
        Integer i3 = new Integer(42); // make a new wrapper object
        byte b = i3.<mark>byteValue</mark>();
        System.err.println("b="+b);
        short s = i3.shortValue();
         System.err.println("s="+s);
        double d = i3.doubleValue();
        System.err.println("d="+d);
        Float f3 = new Float(3.14f);
        short s1 = f3.shortValue();
         System.err.println("s1="+s1);
             
             
       <mark>  //parseXxx() String to primitive</mark>
         double d4 = Double.<mark>parseDouble</mark>("3.14"); // convert a String
        // to a primitive
        System.out.println("d4 = " + d4);
        Double d5 = Double.valueOf("3.14"); // create a Double obj
        System.out.println(d5 instanceof Double);
            
        long L2 = Long.parseLong("101010", 2);
        Long L3 = Long.valueOf("101010", 2);
        System.out.println("L2 = " + L2);
        System.out.println("L3 value = " + L3);
            
            
            
    }
}
    
        </pre>
        <pre class='out'>      
        run:
i2=43
f2=3.0
b=42
d4 = 3.14
s=42
true
d=42.0
L2 = 42
s1=3
L3 value = 42
BUILD SUCCESSFUL (total time: 1 second)
        </pre>
        
        <pre>        
        package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
            
public class ex003 {
    
    
    public static void main(String[] args) {
        // NumberFormatException
            
       Integer i4 = Integer.<mark>valueOf("101011.1")</mark>;
//        System.err.println("i4="+i4.toString());
//        
        long L2 = Long.<mark>parseLong("101010.1</mark>");
            
            
    }
}
        </pre>     
<pre class='out'>        
run:
Exception in thread "main" <mark>java.lang.NumberFormatException</mark>: For input string: "101011.1"
	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
	at java.lang.Integer.parseInt(Integer.java:456)
	at java.lang.Integer.valueOf(Integer.java:553)
	at com.mvivekweb.ocpjp6.obj3.sec1.wrappers.ex003.main(ex003.java:10)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)        
        
    </pre>      
        <p><del>BC</del><mark>BSILFD</mark> can be applied to above.no boolean and character</p>
        
        <p> Radix refers to bases (typically) other than 10; octal is radix = 8, hex = 16.</p>
        
        <p> <b> toString()</b>
            <br> The idea of the toString() method is to allow you
            to get some meaningful representation of a given object.</p>
        
        <p> if you have a
            <mark>Collection</mark> of various types of <mark>objects</mark>, you can <mark>loop through</mark> the Collection and print
            out some sort of meaningful representation of each object using the <b>toString()</b>
            method, <mark>which is guaranteed to be in every class</mark>.</p>
        
        <p>All of the wrapper classes
            have a <mark>no-arg, nonstatic</mark>, instance version of toString()</p>
        
        <pre> Double d = new Double("3.14");
System.out.println("d = "+ d.toString() ); // result is d = 3.14</pre>
        
        <p>All of the numeric wrapper classes provide an <mark>overloaded, static</mark> toString()
            method that <mark>takes a primitive numeric</mark> of the appropriate type (<mark>Double.
            toString() takes a double</mark>, Long.toString() takes a long, and so on)</p>
        <pre>  String d = Double.toString(3.14); // d = "3.14"  </pre>    
        <p>   <mark>  toXxxString()  </mark>  
        
        
        
        </p>  
        
        <pre> String s3 = Integer.toHexString(254); // convert 254 to hex
System.out.println("254 is " + s3); // result: "254 is fe"
String s4 = Long.toOctalString(254); // convert 254 to octal
System.out.print("254(oct) ="+ s4); // result: "254(oct) =376"</pre>
        
<h3>Autoboxing (Objective 3.1)</h3>
<p> As of Java 5, boxing allows you to convert primitives to wrappers or to
    convert wrappers to primitives automatically.</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class autobox01 {
    
    
    public static void main(String[] args) {
        Integer y = new Integer(567); // make it
        <mark>y++; // unwrap it, increment it,</mark>
            // rewrap it
        System.out.println("y = " + y); // print it
    }
}
</pre>
<pre class='out'>
run:
y = 568
BUILD SUCCESSFUL (total time: 1 second)
    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class oldwayboxing {
    
    
    public static void main(String[] args) {
        
        Integer y = new Integer(567); // make it
        <mark>int x = y.intValue(); // unwrap it unbox
        x++; // use it
        y = new Integer(x); // re-wrap it box</mark>
        System.out.println("y = " + y); // print it
    }
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class autobox02 {
    
    
    public static void main(String[] args) {
        Integer y = 567; // make a wrapper
        Integer x = y; // assign a second ref
        // var to THE wrapper
        System.out.println(y==x);
            
        y++; // unwrap, use, "rewrap"
        System.out.println(x + " " + y); // print values
        System.out.println(y==x);
    }
}
</pre>

<p>Converting a primitive value (an int, for example) into an object of the corresponding wrapper class (Integer) is called autoboxing. 
</p>
<p>The Java compiler applies autoboxing when a <mark>primitive value</mark> is:
    
    <br>  a)  Passed as a <mark>parameter</mark> to a <mark>method</mark> that <mark>expects</mark> an object of the corresponding <mark>wrapper class</mark>.
    <br>  b)  <mark>Assigned</mark> to a <mark>variable</mark> of the corresponding <mark>wrapper class</mark>.</p>

<img src="/imag/jp6/autobox.png" class="imgw">

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;

import java.util.ArrayList;
import java.util.List;


public class autobox04 {


 public static void main(String[] args) {
 List&lt;Integer&gt; li = new ArrayList&lt;Integer&gt;();
 for (int i = 1; i &lt; 50; i += 2){
<mark> li.add(i);</mark>
 }
}
}
</pre>
<pre> li.add(<mark>Integer.valueOf(i)</mark>);
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
import java.util.ArrayList;
import java.util.List;
    
public class autobox03 {
    
    public static void main(String[] args) {
         System.err.println("1="+1);
        List&lt;Integer&gt; li = new ArrayList&lt;Integer&gt;();
        li.add(Integer.parseInt("21"));
        int val=sumEven(li);
        System.err.println("val="+val);
    }
    public static int sumEven(List&lt;Integer&gt; li) {
    int sum = 0;
    System.err.println("sum="+sum);
    for (Integer i: li)
        if (<mark>i % 2</mark> == 0)
            <mark>sum += i;</mark>
        return sum;
    }
}
</pre>
<pre class='out'>
run:
1=1
sum=0
val=20
BUILD SUCCESSFUL (total time: 1 second)
    </pre>

<pre>public static int sumEven(List&lt;Integer&gt; li) {
    int sum = 0;
    for (Integer i : li)
        if (<mark>i.intValue()</mark> % 2 == 0)
            sum += <mark>i.intValue()</mark>;
        return sum;
}</pre>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
import java.util.List;
    
    
public class autobox05 {
    
    
    public static void main(String[] args) {
        
        int x1= sumEven(<mark>10</mark>);
        System.err.println("x1="+x1);
    }
        
    public static int sumEven(<mark>Integer vi</mark>) {
            int vi1 = <mark>vi</mark>+1;
        return vi1;
        }
}
</pre>
<pre class='out'>
run:
x1=11
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
import java.util.List;
    
    
public class autobox05 {
    
    
    public static void main(String[] args) {
        
        int x1= sumEven(<mark>Integer.valueOf(10)</mark>);
        System.err.println("x1="+x1);
    }
        
    public static int sumEven(Integer vi) {
            int vi1 = <mark>vi.intValue()</mark>+1;
        return vi1;
        }
}
</pre>

<p>Converting an object of a wrapper type (Integer) to its corresponding primitive (int) value is called unboxing. 
</p>
<p> The Java compiler applies unboxing when an object of a wrapper class is:
    
    <br>   a) Passed as a parameter to a method that expects a value of the corresponding primitive type.
    <br>   b) Assigned to a variable of the corresponding primitive type.
</p>
<img src="/imag/jp6/unbox.png" class="imgw">
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
 
import java.util.ArrayList;
import java.util.List;
 
 
public class Unboxing {
 
 public static void main(String[] args) {
 Integer i = new Integer(-8);
 
<mark> // 1. Unboxing through method invocation</mark>
 int absVal = absoluteValue(i);
 System.out.println("absolute value of " + i + " = " + absVal);
 
 List&lt;Double&gt; ld = new ArrayList&lt;Double&gt;();
 ld.add(3.1416); // &#928; is autoboxed through method invocation.
 
<mark> // 2. Unboxing through assignment</mark>
 double pi = ld.get(0);
 System.out.println("pi = " + pi);
 }
 
 public static int <mark>absoluteValue(int i)</mark> {
 return (i &lt; 0) ? -i : i;
 }
}
</pre>
<pre class='out'>
run:
absolute value of -8 = 8
pi = 3.1416
BUILD SUCCESSFUL (total time: 1 second)
    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
import static com.mvivekweb.ocpjp6.obj3.sec1.wrappers.autobox05.sumEven;
    
    
public class unboxing01 {
    
    public static void main(String[] args) {
        Integer wrap = new Integer("20");
         int <mark>x1</mark>= sumEven(<mark>wrap</mark>);
        System.err.println("x1="+x1);
    }
        
    public static <mark>Integer</mark> sumEven(<mark>int vi</mark>) {
            Integer <mark>wrap1</mark> = new Integer(vi);
            int wrap2 = <mark>wrap1</mark>+1;
        return <mark>wrap2</mark>;
        }
}
</pre>
<pre class='out'>
run:
x1=21
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>

<p> Using == with wrappers created through boxing is tricky; those with the same
    small values (typically lower than 127), will be ==, larger values will not be ==.</p>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class boxing01 {
    
    
    public static void main(String[] args) {
        Integer i1 = 127;
        Integer i2 = 127;
        if(<mark>127 != i2</mark>) System.out.println("different objects");
        if(i1.equals(i2)) System.out.println("meaningfully equal");
    }
}
</pre>

<pre class='out'>run:
meaningfully equal
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class boxing01 {
    
    
    public static void main(String[] args) {
        Integer i1 = 128;
        Integer i2 = 128;
        if(<mark>i1 != i2</mark>) System.out.println("different objects");
        if(i1.equals(i2)) System.out.println("meaningfully equal");
    }
}
</pre>
<pre class='out'>
run:
different objects
meaningfully equal
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
    
public class boxing01 {
    
    
    public static void main(String[] args) {
        Integer i1 = 127;
        Integer i2 = 127;
        if(<mark>i1 != i2</mark>) System.out.println("different objects");
        if(i1.equals(i2)) System.out.println("meaningfully equal");
    }
}
</pre>
<pre class='out'>
run:
meaningfully equal
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>

<p>In order to save memory, two instances of the
    following wrapper objects (created through boxing), will always be == when their
    primitive values are the same:
    <br>     Boolean
    <br>    Byte
    <br>    Character from \u0000 to \u007f (7f is 127 in decimal)
    <br>    Short and Integer from <mark>-128 to 127</mark></p>

<p><b>wrapper reference variables can be null</b></p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;
    
public class Boxing2 {
    
    static Integer <mark>x;</mark>
        
    public static void main(String[] args) {
        doStuff(x);
    }
        
    static void doStuff(int z) {
        int z2 = 5;
        System.out.println(z2 + z);
    }
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.NullPointerException
	at com.mvivekweb.ocpjp6.obj3.sec1.wrappers.Boxing2.main(Boxing2.java:8)
Java Result: 1
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>
<p>
    That means that you
    have to watch out for code that appears to be doing safe primitive operations, but that
    could throw a <mark>NullPointerException:</mark>
</p>
<h3>Using String, StringBuffer, and StringBuilder (Objective 3.1)</h3>
<img src="/imag/jp6/stringequals.png" class="imgw">

<p>String object is created, it can <mark>never be changed</mark></p>
<p> String objects are <mark>immutable</mark>, and String <mark>reference variables are not</mark>.</p>
<p> If you <mark>create a new String without assigning it</mark>, it <b>will be lost</b> to your program.</p>
<p> If you redirect a String reference to a new String, the old String can be lost.</p>


<p>In Java, each character in a string is a 16-bit Unicode character</p>
<p>Once
    you have assigned a String a value, that value can never change.it's immutable</p>
<p><mark>No</mark> code in our program has any way to
    reference the <mark>lost String</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers.string;
    
public class String01 {
    
    public static void main(String[] args) {
        String x = "Java";
        x.concat(" Rules!");
        System.out.println("x = " + x);
    }
}
</pre>
<pre class='out'>
run:
x = Java
BUILD SUCCESSFUL (total time: 1 second)
</pre>
<p>The second String object is instantly lost;
    you can't get to it. The reference variable x still refers to the original String with the
    value "Java".
</p>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers.string;
    
public class String01 {
    
    public static void main(String[] args) {
        String x = "Java";
        x.concat(" Rules!");
        x.toUpperCase();
        x.replace('a', 'X');
        System.out.println("x = " + x);
    }
}
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers.string;
    
public class String02 {
    
    public static void main(String[] args) {
        String s1 = "spring ";
        String s2 = s1 + "summer ";
        s1.concat("fall ");
        s2.concat(s1);
        s1 += "winter ";
        System.out.println(s1 + " " + s2);
    }
}
    
</pre>




<p> String methods use zero-based indexes, except for the second argument of
    substring().</p>
<p> The String class is <mark>final</mark>.its methods can't be overridden.</p>
<p>N<mark>obody can override</mark> the
    behaviors of any of the <mark>String methods</mark></p>
<p> When the JVM finds a String literal, it is added to the String literal pool.</p>
<pre>
String s = "abc";
</pre>
<p>creates <mark>one</mark> String object and <mark>one</mark>
    reference variable</p>
<pre>
String s = new String("abc");
</pre>

<p>creates <mark>two </mark>objects,and <mark>one</mark> reference variable</p>
<p>Java will create a new String object
    in normal (nonpool) memory, and s will refer to it. In addition, the literal "abc" will
    be placed in the pool.
</p>

<p> Strings have a method: length(); arrays have an attribute named length.</p>

<p>The java.lang.<mark>StringBuffer </mark>and java.lang.<mark>StringBuilder</mark> classes should be used when
    you have to make a <mark>lot of modifications to strings of characters</mark>.
    
</p>

<p>objects of type StringBuffer and StringBuilder can be modified over and over again
    without leaving behind a great effluence of discarded String objects.</p>
<p>
    common use for StringBuffers and StringBuilders is <mark>file I/O when large</mark>,
    ever-changing streams of input are being handled by the program. In these
    cases, large blocks of characters are handled as units, and <mark>StringBuffer
    objects are the ideal way to handle a block of data</mark>, pass it on, and then
    reuse the same memory to handle the next block of data.
</p>
<p> The StringBuffer's API is the same as the new StringBuilder's API, except
    that <b>StringBuilder's methods are not synchronized for thread safety</b>.</p>
<p> <mark>StringBuilder</mark> methods should run <mark>faster</mark> than <mark>StringBuffer</mark> methods.</p>
<p><mark>use StringBuilder</mark> instead of StringBuffer whenever possible
    because StringBuilder will run faster</p>
<p>The exam might use these classes in the
        creation of thread-safe applications</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.StringBuffer;
    
public class StringBuffer01 {
    
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("abc");
        sb.append("def");
        System.out.println("sb = " + sb);
    }
}
</pre>
<pre class='out'>
run:
sb = abcdef
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>



<p>All of the following bullets apply to both StringBuffer and StringBuilder:</p>
<br>They are <mark>mutable</mark>-they can change without creating a new object.<br>
<br>StringBuffer methods act on the invoking object, and objects can change
   without an explicit assignment in the statement.
   <br>   <br>  <mark>StringBuffer equals() is not overridden; it doesn't compare values</mark>.
   <p> Remember that chained methods are evaluated from left to right.</p>
   <pre>   
   package com.mvivekweb.ocpjp6.obj3.sec1.StringBuilder;
       
public class StringBuilder01 {
    
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("abc");
        sb.append("def").reverse().insert(3, "---");
        System.out.println(sb);
    }
}
   </pre>
<pre class='out'>   
run:
fed---cba
BUILD SUCCESSFUL (total time: 0 seconds)   
    </pre> 
   
<p> String methods to remember: charAt(), concat(), equalsIgnoreCase(),
    length(), replace(), substring(), toLowerCase(), toString(),
    toUpperCase(), and trim().</p>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers.string;
    
public class String03 {
    
    public static void main(String[] args) {
        String x = "airplane";
        System.out.println(<mark>x.charAt(2)</mark>);
            
            
        String x1 = "taxi";
        System.out.println(<mark>x1.concat(" cab")</mark>);
            
        String x2 = "library";
        System.out.println(x2 + " card");
            
            
        String x3 = "Atlantic";
        x3 += " ocean";
        System.out.println(x3);
            
        String x4 = "Exit";
        System.out.println(x4.equalsIgnoreCase("EXIT")); // is "true"
        System.out.println(<mark>x4.equalsIgnoreCase("tixe")</mark>);
            
        String x5 = "01234567";
        System.out.println(<mark>x5.length()</mark>);
            
        String x6 = "oxoxoxox";
        System.out.println(<mark>x6.replace('x', 'X')</mark>);
            
        String x7 = "0123456789";
        System.out.println(x7.substring(5)); // output is "56789"
        System.out.println(<mark>x7.substring(5, 8)</mark>);
            
        String x8 = "A New Moon";
        System.out.println(<mark>x8.toLowerCase()</mark>);
            
        String x9 = "big surprise";
        System.out.println(<mark>x9.toString()</mark>);
            
            
        String x10 = "A New Moon";
        System.out.println(<mark>x10.toUpperCase()</mark>);
            
            
        String x11 = " hi ";
        System.out.println(x11 + "x");
        System.out.println(<mark>x11.trim()</mark> + "x");
    }
}
    
</pre>
<pre class='out'>
run:
r
taxi cab
library card
Atlantic ocean
true
false
8
oXoXoXoX
56789
567
a new moon
big surprise
A NEW MOON
 hi x
hix
BUILD SUCCESSFUL (total time: 0 seconds)
    
</pre>

<p> StringBuffer methods to remember: append(), delete(), insert(),
    reverse(), and toString().</p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.StringBuffer;
    
public class StringBuffer03 {
    
    public static void main(String[] args) {
        StringBuffer sb = new <mark>StringBuffer</mark>("set ");
        <mark>sb.append("point");</mark>
        System.out.println(sb); // output is "set point"
        StringBuffer sb2 = new StringBuffer("pi = ");
        sb2.append(3.14159f);
        System.out.println(sb2); // output is "pi = 3.14159"
            
            
        StringBuffer sb1 = new StringBuffer("0123456789");
        System.out.println(<mark>sb1.delete(4, 6)</mark>);
            
        StringBuffer sb3 = new StringBuffer("01234567");
        <mark>sb3.insert(4, "---");</mark>
        System.out.println(sb3);
            
        StringBuffer sb4 = new StringBuffer("A man a plan a canal Panama");
        <mark>sb4.reverse();</mark>
        System.out.println(sb4);
            
        StringBuffer sb5 = new StringBuffer("test string");
        System.out.println(<mark>sb5.toString()</mark>);
            
            
    }
}
    
</pre>
<pre class='out'>
run:
set point
pi = 3.14159
01236789
0123---4567
amanaP lanac a nalp a nam A
test string
BUILD SUCCESSFUL (total time: 0 seconds)

    </pre>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.StringBuilder;
    
    
public class StringBuilder02 {
    
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("set ");
        sb.append("point");
        System.out.println(sb); // output is "set point"
        StringBuilder sb2 = new StringBuilder("pi = ");
        sb2.append(3.14159f);
        System.out.println(sb2); // output is "pi = 3.14159"
            
            
        StringBuilder sb1 = new StringBuilder("0123456789");
        System.out.println(sb1.delete(4, 6));
            
        StringBuilder sb3 = new StringBuilder("01234567");
        sb3.insert(4, "---");
        System.out.println(sb3);
            
        StringBuilder sb4 = new StringBuilder("A man a plan a canal Panama");
        sb4.reverse();
        System.out.println(sb4);
            
        StringBuilder sb5 = new StringBuilder("test string");
        System.out.println(sb5.toString());
    }
        
}
</pre>
<pre class='out'>
run:
set point
pi = 3.14159
01236789
0123---4567
amanaP lanac a nalp a nam A
test string
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>



<pre>
<mark>par</mark>se(string)- string to <mark>prim</mark>
intValue()-  W-p
valueOf(Str)- S- W
        </pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;


public class boxing01 {


    public static void main(String[] args) {

        
        Boolean b1=false;
        Boolean b2=true;
        if(b1 != b2) System.out.println("different objects");
        
        Byte b3=127;
        Byte b4=127;
        if(b3 == b4) System.out.println("byte same objects");   
        
    }
}
</pre>

<pre class='out'>different objects
byte same objects
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.wrappers;


public class boxing01 {


    public static void main(String[] args) {

        
        Boolean b1=false;
        Boolean b2=true;
        if(b1 != b2) System.out.println("different objects");
        
        Byte b3=127;
        Byte b4=127;
        if(b3 == b4) System.out.println("byte same objects");   
        
        Short b5=128;
        Short b6=128;
        <mark>if(b5 != b6)</mark> System.out.println("Short not same objects");  
        
        Short b7=127;
        Short b8=127;
        if(b7 == b8) System.out.println("Short same objects"); 
        
        short b9=128;
        short b10=128;
        <mark>if(b9 == b10)</mark> System.out.println("short same value"); 
        
    }
}</pre>


<pre class='out'>different objects
byte same objects
Short not same objects
Short same objects
short same value</pre>


<pre>
String- replace, concat

Stringbuilder- reverse
app, del, insert, rever, toString
<mark>a dir</mark>
</pre>

<p>The StringBuffer class doesn't override the equals() method, so two
different StringBuffer objects with the same value will not be equal according to the
equals() method. On the other hand, the String class's equals() method has been
overridden so that two different String objects with the same value will be considered
equal according to the equals() method.</p>

<p>widening is preferred to boxing</p>

<pre>Java's widening conversions are

From a byte to a short, an int, a long, a float, or a double
From a short to an int, a long, a float, or a double
From a char to an int, a long, a float, or a double
From an int to a long, a float, or a double
From a long to a float or a double
From a float to a double
</pre>

<p> you <mark>can't widen
from one wrapper</mark> to another. In this case, Integer is-Not-a Long, but Integer <mark>is-a</mark> Number.</p>

<pre>

<mark>substring and delete</mark>

start begin with 0 , use the digit
end begin with 1,use the digit 

123456789
    
sub(3,4)


<mark>
insert, </mark>end begin with 1 and insert after digit

123456

insert(2,---)

12---23456



</pre>

<pre>
<b>package
</b>
<mark>java.lang.</mark>StringBuffer</pre>

<p><mark><b>string and wrappers</b></mark> have overriden equals</p>


<pre>
<mark>Wrapper </mark>
-----------
a)<mark> ==
128</mark> value integer


b) equals
<mark>equals overidden</mark> - example String and Wrapper

<mark>so two wrappers same value is equal
> 128 also</mark>

see exam 31
</pre>


<pre>
decimal to octal
    
99
    
    
  1  
  -
8|10   
   8
      
<mark>  1|2
  0|1</mark>
      
  ans-  012


<mark>99
    
   12|3
    1|4
    0|1
        
ans - 143</mark>
</pre>

<pre>Integer.parseInt("011") = 11</pre>

<p>the parseInt() method assumes a <mark>radix of 10</mark> if a radix is not specified. </p>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec1.exam;

public class Bertha {

	static String s = "";
	public static void main(String[] args) {
		int x = 4; Boolean y = true; short[] sa = {1,2,3};
		 doStuff(<mark>x, y</mark>);
	     doStuff(x);
	     <mark>doStuff(y);</mark>
		 doStuff(sa, sa);
		 System.out.println(s);
		 }

		 static void doStuff(Integer... i) { s += "3"; }
		 static void doStuff(Long L) { s += "4"; }
		 <mark>static void doStuff(Boolean L) { s += "5"; }</mark>
		 static void doStuff(Object o) { s += "1"; }
		 static void <mark>doStuff(Object... o) { s += "2"; }</mark>
}

</pre>

<pre class='out'>2152</pre>

<pre>package com.mvivekweb.ocpjp6.obj3.sec1.exam;

public class Bertha {

	static String s = "";
	public static void main(String[] args) {
		int x = 4; Boolean y = true; short[] sa = {1,2,3};
		 doStuff(x, y);
	     doStuff(x);
	     doStuff(y);
		 doStuff(sa, sa);
		 System.out.println(s);
		 }

		 static void doStuff(Integer... i) { s += "3"; }
		 static void doStuff(Long L) { s += "4"; }
		 static void doStuff(<mark>Boolean... L</mark>) { s += "5"; }
		 static void doStuff(Object o) { s += "1"; }
		 static void doStuff(Object... o) { s += "2"; }
}

</pre>

<pre class='out'>21<mark>12</mark></pre>

<p>It's <mark>legal to autobox and then widen</mark>. <mark>cannot widen and then
box</mark> .var-args method will be chosen only if no non-var-arg method is possible.</p>

<p>that the equals() method for the <mark>integer wrappers</mark>
will only return true <mark>if the two </mark><mark>primitive types</mark> and the <mark>two values </mark>are equal.</p>

<p>it's
okay to <mark>unbox and use ==</mark></p>

<pre>package com.mvivekweb.ocpjp6.obj3.sec1.exam;

public class Theory {


	public static void main(String[] args) {
		String s1 = "abc";
		 String s2 = s1;
		<mark>// s1 += "d";
		 s1=s1.concat("d");</mark>
		 System.out.println(s1 + " " + s2 + " " + (s1==s2));
		
		 StringBuffer sb1 = new StringBuffer("abc");
		 StringBuffer sb2 = sb1;
		 sb1.append("d");
		 System.out.println(sb1 + " " + sb2 + " " + (sb1==sb2));

	}

}
</pre>

<pre class='out'>abcd abc false
abcd abcd true
</pre>
<img src="/imag/jp6/stringconcat.png" class="imgw">
<p> boxing beats out var-args</p>

<p>But you can box <mark>shorts to Shorts and then
widen</mark> them to Numbers, and this takes priority over using a var-args method. The second
invocation uses a simple box from int to Integer.</p>
