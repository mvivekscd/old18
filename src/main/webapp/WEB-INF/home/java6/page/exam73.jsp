<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class Volume {
Volume v;
int size;
    
    public static void main(String[] args) {
        Volume myV = new Volume();
<mark>final</mark> Volume v2;
 v2 = myV.doStuff(myV);
 v2.v.size = 7;
 System.out.print(v2.size);
    }
Volume doStuff(Volume v3) {
 v3.size = 5;
 v3.v = new Volume();
 return v3;
 }
}
</pre>
<pre class='out'>run:
     <mark>5</mark>BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<p>A reference variable marked <mark>"final" cannot reference a different object</mark>,
    but the <mark>object it references can be changed</mark>.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.List;
import java.util.Vector;
    
    
public class HR {
    
    
    public static void main(String[] args) {
        List&lt;Integer&gt; i = new Vector&lt;Integer&gt;();
 i.add(3); i.add(2); i.add(5);
 int ref = 1;
 doStuff(ref);
System.out.println(i.get(ref));
    }
static int doStuff(int x) {
 return ++x;
 }
}
    
</pre>
<pre class='out'>
run:
2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>The variable <mark>"x" is a copy of ref</mark>, and when "x" gets incremented, ref does
    not. Also, the list index is zero-based and instances of Vector aren't automatically sorted.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class BackHanded {
    
int state = 0;
BackHanded(int s) { state = s; }
    public static void main(String[] args) {
        BackHanded b1 = new BackHanded(1);
 BackHanded b2 = new BackHanded(2);
 System.out.println(b1.go(b1) + " " + b2.go(b2));
    }
int go(BackHanded b) {
    System.out.println("--0--");
if(this.state == 2) {
     System.out.println("--1--");
 b.state = 5;
<mark> go(this);</mark>
  <mark>System.out.println("--2--");</mark>
 }
    System.out.println("end of if");
<mark> return ++this.state;</mark>
 }
}
</pre>

<pre class='out'>
run:
--0--
end of if
--0--
--1--
--0--
end of if
--2--
end of if
2 7
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class Kaput {
    
Kaput myK;
 String degree = "0";
    public static void main(String[] args) {
        Kaput k1 = new Kaput();
 go(k1);
 System.out.println(k1.degree);
    }
static Kaput go(Kaput k) {
 final Kaput k1 = new Kaput();
 k.myK = k1;
 k.myK.degree = "7";
 return k.myK;
 }
}
    
</pre>
<pre class='out'>
run:
0
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>The Kaput object referred to by k1 has a Kaput object, and that object's
    degree variable is updated, but the original Kaput object's degree variable is not updated.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class Kaput {
    
Kaput myK;
 String degree = "0";
    public static void main(String[] args) {
        Kaput k1 = new Kaput();
<mark> k1=go(k1);</mark>
 System.out.println(k1.degree);
    }
static Kaput go(Kaput k) {
 final Kaput k1 = new Kaput();
 k.myK = k1;
 k.myK.degree = "7";
 return k.myK;
 }
}
    
</pre>
<pre class='out'>
run:
7
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
    
    
public class Elway {
    
    
    public static void main(String[] args) {
        ArrayList[] ls = new ArrayList[3];
 for(int i = 0; i < 3; i++) {
 ls[i] = new ArrayList();
 ls[i].add("a" + i);
 }
 Object o = ls;
 do3(ls);
 for(int i = 0; i < 3; i++) {
 // insert code here
 System.out.print(<mark>o[i]</mark> + " ");
 }
    }
static Object do3(ArrayList[] a) {
 for(int i = 0; i < 3; i++) a[i].add("e");
 return a;
 }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect3/exam/Elway.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: <mark>error: array required, but Object found</mark>
 System.out.print(o[i] + " ");
                   ^
Note: com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
    
    
public class Elway {
    
    
    public static void main(String[] args) {
        ArrayList[] ls = new ArrayList[3];
 for(int i = 0; i < 3; i++) {
 ls[i] = new ArrayList();
 ls[i].add("a" + i);
 }
 Object o = ls;
 do3(ls);
 for(int i = 0; i < 3; i++) {
 // insert code here
 System.out.print(<mark>(ArrayList[])[i]</mark> + " ");
 }
    }
static Object do3(ArrayList[] a) {
 for(int i = 0; i < 3; i++) a[i].add("e");
 return a;
 }
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect3/exam/Elway.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: error: illegal start of expression
 System.out.print((ArrayList[])[i] + " ");
                               ^
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: error: ';' expected
 System.out.print((ArrayList[])[i] + " ");
                                ^
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: error: illegal start of expression
 System.out.print((ArrayList[])[i] + " ");
                                 ^
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: error: not a statement
 System.out.print((ArrayList[])[i] + " ");
                                   ^
com\mvivekweb\ocpjp6\obj7\sect3\exam\Elway.java:21: error: ';' expected
 System.out.print((ArrayList[])[i] + " ");
                                        ^
5 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
    
    
public class Elway {
    
    
    public static void main(String[] args) {
        ArrayList[] ls = new ArrayList[3];
 for(int i = 0; i < 3; i++) {
 ls[i] = new ArrayList();
 ls[i].add("a" + i);
 }
 Object o = ls;
 do3(ls);
 for(int i = 0; i < 3; i++) {
 // insert code here
 System.out.print( <mark>((Object[])o)[i]</mark> + " ");
 }
    }
static Object do3(ArrayList[] a) {
 for(int i = 0; i < 3; i++) a[i].add("e");
 return a;
 }
}
</pre>
<pre class='out'>
run:
[a0, e] [a1, e] [a2, e] BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
    
    
public class Elway {
    
    
    public static void main(String[] args) {
        ArrayList[] ls = new ArrayList[3];
 for(int i = 0; i < 3; i++) {
 ls[i] = new ArrayList();
 ls[i].add("a" + i);
 }
 Object o = ls;
 do3(ls);
 for(int i = 0; i < 3; i++) {
 // insert code here
 System.out.print(<mark>((ArrayList[])o)[i]</mark> + " ");
 }
    }
static Object do3(ArrayList[] a) {
 for(int i = 0; i < 3; i++) a[i].add("e");
 return a;
 }
}
    
</pre>
<pre class='out'>
run:
[a0, e] [a1, e] [a2, e] BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class Boggy {
    
final static int mine = 7;
 final static Integer i = 57;
    public static void main(String[] args) {
        int x = go(mine);
System.out.print(mine + " " + x + " ");
 x += mine;
<mark> Integer i2 = i;</mark>
 i2 = go(i);
 System.out.println(x + " " + i2);
 i2 = new Integer(60);
    }
static int go(int x) { return ++x; }
}
    
</pre>

<pre class='out'>run:
7 8 15 58
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre class='out'>
None of the final variables are modified. The "i2" variable initially refers
to the same object as the final "i" variable, but that object can be modified, and "i2"
can be referred to a different object. Autoboxing allows an Integer to work with go()'s
int argument.</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
    
    
public class Foggy {
    
    
    public static void main(String[] args) {
        final List&lt;String&gt; s = new ArrayList&lt;String&gt;();
 s.add("a"); s.add("f"); s.add("a");
 new Foggy().mutate(s);
 System.out.println(s);
    }
List&lt;String&gt; mutate(List&lt;String&gt; s) {
 List&lt;String&gt; ms = s;
 ms.add("c");
 return s;
 }
}
class Murky {
 final void mutate(Set s) { }
 }</pre>
<pre class='out'>
run:
7 8 15 58
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<p>
The List is "final", but that doesn't mean its contents can't change.
Lists can contain duplicates, and mutate() gets a copy of the reference variable, so it's
adding to the same List. Murky's final mutate() isn't being overridden, it's being
overloaded.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
    
    
public class Foggy {
    
    
    public static void main(String[] args) {
        final List&lt;String&gt; s = new ArrayList&lt;String&gt;();
 s.add("a"); s.add("f"); s.add("a");
 new Foggy().mutate(s);
 System.out.println(s);
    }
List&lt;String&gt; mutate(List&lt;String&gt; s) {
 List&lt;String&gt; ms = s;
<mark> ms.add("c");
  s.add("d");</mark>
    System.out.println("s="+s);
     System.out.println("ms="+ms);
 return s;
 }
}
class Murky {
 final void mutate(Set s) { }
 }
     
 </pre>
<pre class='out'>
run:
s=[a, f, a, c, d]
ms=[a, f, a, c, d]
[a, f, a, c, d]
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
    
public class Swanky {
Swanky s;
    
    public static void main(String[] args) {
        Swanky s1 = new Swanky();
s1.s = new Swanky();
go(s1.s);
<mark>Swanky s2 = go(s1);</mark>
if(s2.s.equals(s1)) System.out.print("1 ");
 if(s2.equals(s1)) System.out.print("2 ");
 if(s1.s.equals(s1)) System.out.print("3 ");
 if(s2.s.equals(s2)) System.out.print("4 ");
    }
static Swanky go(Swanky s) {
 Swanky gs = new Swanky();
 gs.s = s;
 return gs;
 }
}
</pre>
<pre class='out'>
run:
1 BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<p>
    Swanky doesn't override equals(), so equals() returns true only when
    both references are referring to the same object. Similar to garbage collection questions,
    the best way to approach this question is to make a diagram of the reference variables,
    objects, connections between them, and when connections or references are lost.</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
public class Swanky {
    Swanky s;
    static int instanceCounter = 0;
    int counter = 0;
    Swanky() {
        instanceCounter++;
        counter = instanceCounter;
    }
    public static void main(String[] args) {
        Swanky s1 = new Swanky();
        System.out.println("counter=" + s1.instanceCounter);
        s1.s = new Swanky();
        System.out.println("counter1=" + s1.instanceCounter);
        go(s1.s);
        System.out.println("counter2=" + s1.instanceCounter);
        Swanky s2 = go(s1);
        System.out.println("counter3=" + s1.instanceCounter);
        if (s2.s.equals(s1)) {
            System.out.println("s2.s.counter=" + s2.s.counter);
            System.out.println("s1.counter=" + s1.counter);
            System.out.print("1 ");
        }
        System.out.println("\ns2.counter=" + s2.counter);
        System.out.println("s1.s.counter=" + s1.s.counter);
        if (s2.equals(s1)) {
            System.out.print("2 ");
        }
        if (s1.s.equals(s1)) {
            System.out.print("3 ");
        }
        if (s2.s.equals(s2)) {
            System.out.print("4 ");
        }
    }
    static Swanky go(Swanky s) {
        Swanky gs = new Swanky();
        gs.s = s;
        return gs;
    }
}
</pre>
<pre class='out'>
run:
counter=1
counter1=2
counter2=3
counter3=4
s2.s.counter=1
s1.counter=1
1 
s2.counter=4
s1.s.counter=2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.exam;
    
public class Swanky {
    Swanky s;
    static int instanceCounter = 0;
    int counter = 0;
    Swanky() {
        instanceCounter++;
        counter = instanceCounter;
    }
    public static void main(String[] args) {
        Swanky s1 = new Swanky();
        System.out.println("counter=" + s1.counter);
        s1.s = new Swanky();
        System.out.println("counter1=" + s1.s.counter);
        go(s1.s);
            
        Swanky s2 = go(s1);
        System.out.println("counter3=" + s1.counter);
        if (s2.s.equals(s1)) {
            System.out.println("s2.s.counter=" + s2.s.counter);
            System.out.println("s1.counter=" + s1.counter);
            System.out.print("1 ");
        }
        System.out.println("\ns2.counter=" + s2.counter);
        System.out.println("s1.s.counter=" + s1.s.counter);
        if (s2.equals(s1)) {
            System.out.print("2 ");
        }
        if (s1.s.equals(s1)) {
            System.out.print("3 ");
        }
        if (s2.s.equals(s2)) {
            System.out.print("4 ");
        }
    }
    static Swanky go(Swanky s) {
        Swanky gs = new Swanky();
        gs.s = s;
        System.out.println("<mark>go counter=</mark>" + gs.counter);
        return gs;
    }
}
</pre>
<pre class='out'>
run:
counter=1
counter1=2
go counter=3
go counter=4
counter3=1
s2.s.counter=1
s1.counter=1
1 
s2.counter=4
s1.s.counter=2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>