
<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
    
public class Thistle extends Weed{
<mark>    void grow() { s += "t-grow "; }
 void growFast() { s+= "t-fast "; }</mark>
}
class Weed {
 protected static String s = "";
 final void grow() { s += "grow "; }
 static final void growFast() { s += "fast "; }
 }</pre>
<pre class='out'>
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\manju>cd C:\vivek\java7\ocpjp6\ocpjp6\src

C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect3/exam/Thistle.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect3\exam\Thistle.java:6: error: grow() in Thistle cannot override grow() in Weed
    void grow() { s += "t-grow "; }
         ^
  overridden method is <mark>final</mark>
com\mvivekweb\ocpjp6\obj5\sect3\exam\Thistle.java:7: error: growFast() in Thistle cannot override growFast() in Weed
 void growFast() { s+= "t-fast "; }
      ^
  overridden method is <mark>static,final</mark>
2 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>Thistle can already access s. If Weed.grow() is NOT final, then
    Thistle can override it. In order for Weed.growFast() to be overridden, two changes
    (D and E), must occur, so for this question, it would require FEWER changes to remove
    Thistle.growFast() and not attempt an override.</p>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
import java.util.ArrayList;
    
    
public class MyUtil extends ArrayList{
    
    
    public static void main(String[] args) {
        MyUtil m = new MyUtil();
m.add("w"); m.add("x"); m.add("y"); m.add("z");
m.removeRange(1,3);
System.out.print(m.size() + " ");
MyUtil m2 = new MyUtil2().go();
 System.out.println(m2.size());
    }
        
}
class MyUtil2 {
MyUtil go() {
 MyUtil m2 = new MyUtil();
 m2.add("1"); m2.add("2"); m2.add("3");
 m2.removeRange(1,2);
return m2;
 } }
     
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect3/exam/MyUtil.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect3\exam\MyUtil.java:25: error: <mark>removeRange(int,int) has protected access</mark> in ArrayList
 m2.removeRange(1,2);
   ^
Note: com\mvivekweb\ocpjp6\obj5\sect3\exam\MyUtil.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
import java.util.ArrayList;
    
    
public class MyUtil extends ArrayList{
    
    
    public static void main(String[] args) {
        MyUtil m = new MyUtil();
m.add("w"); m.add("x"); m.add("y"); m.add("z");
m.removeRange(1,3);
System.out.print(m.size() + " ");
<mark>MyUtil2 m2 = new MyUtil2().go();</mark>
 System.out.println(m2.size());
    }
        
}
class MyUtil2 <mark>extends MyUtil</mark>{
<mark>MyUtil2</mark> go() {
 MyUtil2 m2 = new MyUtil2();
 m2.add("1"); m2.add("2"); m2.add("3");
 m2.removeRange(1,2);
return m2;
 } }</pre>
<pre class='out'>
run:
2 2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
import java.util.ArrayList;
    
    
public class MyUtil extends ArrayList{
    
    
    public static void main(String[] args) {
        MyUtil m = new MyUtil();
m.add("w"); m.add("x"); m.add("y"); m.add("z");
m.removeRange(1,3);
System.out.print(m.size() + " ");
//MyUtil m2 = new MyUtil2().go();
 //System.out.println(m2.size());
    }
        
}
class MyUtil2 extends ArrayList{
void go() {
<mark> MyUtil m2 = new MyUtil();</mark>
 m2.add("1"); m2.add("2"); m2.add("3");
 m2.removeRange(1,2);
//return m2;
 } }</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect3/exam/MyUtil.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect3\exam\MyUtil.java:25: error: <mark>removeRange(int,int) has protected access in ArrayList</mark>
 m2.removeRange(1,2);
   ^
Note: com\mvivekweb\ocpjp6\obj5\sect3\exam\MyUtil.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
import java.util.ArrayList;
    
    
public class MyUtil extends ArrayList{
    
    
    public static void main(String[] args) {
        MyUtil m = new MyUtil();
m.add("w"); m.add("x"); m.add("y"); m.add("z");
m.removeRange(1,3);
System.out.print(m.size() + " ");
//MyUtil m2 = new MyUtil2().go();
 //System.out.println(m2.size());
    }
        
}
class MyUtil2 extends ArrayList{
void go() {
 MyUtil2 m2 = new MyUtil2();
 m2.add("1"); m2.add("2"); m2.add("3");
 m2.removeRange(1,2);
//return m2;
 } }
</pre>
<pre class='out'>
run:
2 BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class MotorVehicle {
<mark>protected</mark> int doStuff(int x) { return x * 2; }
 }
 class Bicycle {
 void go(MotorVehicle m) {
 System.out.print(m.doStuff(21) + " ");
 } }
     
public class Beemer extends MotorVehicle{
    
    
    public static void main(String[] args) {
       System.out.print(new Beemer().doStuff(11) + " ");
 new Bicycle().go(new Beemer());
 new Bicycle().go(new MotorVehicle()); 
    }
int doStuff(int x) { return x * 3; }
}
    
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj5/sect3/exam/Beemer.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj5\sect3\exam\Beemer.java:21: error: doStuff(int) in Beemer cannot override doStuff(int) in MotorVehicle
int doStuff(int x) { return x * 3; }
    ^
  <mark>attempting to assign weaker access privileges; was protected</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>


<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class Grandfather {
 static String name = "gf ";
 String doStuff() { return "grandf "; }
 }
 class Father extends Grandfather {
 static String name = "fa ";
 String doStuff() { return "father "; }
 }
public class Child extends Father{
static String name = "ch ";
 String doStuff() { return "child "; }
     
    public static void main(String[] args) {
        Father f = new Father();
 <mark>System.out.print(((Grandfather)f).name
+ ((Grandfather)f).doStuff()) ;</mark>
 Child c = new Child();
<mark> System.out.println(((Grandfather)c).name
+ ((Grandfather)c).doStuff() + ((Father)c).doStuff());</mark>
    }
        
}
</pre>
<pre class='out'>
run:
gf father gf child child 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p><mark>Overriding applies only to instance methods</mark>, therefore the reference variables'
    <mark>type is used to access the static Strings called "name"</mark>. During overriding, <mark>the object's
        type is used to determine which overridden method is used.</mark></p>

        <pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
import java.util.ArrayList;
import java.util.List;
    
class Radio {
 String getFreq() { return "97.3"; }
<mark>static</mark> String getF() { return "97.3"; }
 }
public class Ham extends Radio{
    
String getFreq() { return "50.1"; }
 <mark>static</mark> String getF() { return "50.1"; }
    public static void main(String[] args) {
        List&lt;Radio&gt; radios = new ArrayList&lt;Radio&gt;();
 radios.add(new Radio());
 radios.add(new Ham());
 for(Radio r: radios)
 System.out.print(r.getFreq() + " " + r.getF() + " ");
    }
        
}
</pre>
<pre class='out'>        
run:
97.3 97.3 50.1 97.3 BUILD SUCCESSFUL (total time: 0 seconds)    </pre>    
        
        
        <pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class Car {
 private Car() { }
 protected Car(int x) { }
 }
public class MG extends Car{
 //MG(int x) { }
 // MG(int x) { super(); }
  <mark>MG(int x) { super(x); }</mark>
 // private MG(int x) { super(x); }
  //MG() { }
 // MG() { this(); }
 // MG() { this(6); }
  <mark>MG() { super(7); }</mark>
      
    public static void main(String[] args) {
        new MG(7);
 new MG();
    }
        
}
        </pre>
        
        <pre>
class Car {
 private Car() { }
 protected Car(int x) { }
 }
public class MG extends Car{
 //MG(int x) { }
  //MG(int x) { super(); }
  //MG(int x) { super(x); }
<mark>  private MG(int x) { super(x); }</mark>
  //MG() { }
 // MG() { this(); }
<mark>  MG() { this(6); }</mark>
 // MG() { super(7); }
     
    public static void main(String[] args) {
        new MG(7);
 new MG();
    }
        
}
        </pre>
        
        <pre>
     
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class Car {
 private Car() { }
 protected Car(int x) { }
 }
public class MG extends Car{
 7//MG(int x) { }
  8//MG(int x) { super(); }
  9//MG(int x) { super(x); }
<mark>  private MG(int x) { super(x); }</mark>
  11//MG() { }
 12// MG() { this(); }
  13//MG() { this(6); }
<mark>  MG() { super(7); }</mark>
      
    public static void main(String[] args) {
        new MG(7);
 new MG();
    }
        
}
        </pre>
        
<p>None of the constructors can chain to Car's private constructor,
            they must all get to Car's protected constructor somehow. Lines 7 and 8 won't ever
            work because Car's protected constructor needs an argument. Line 13 would work with
            either line 9 or line 10. Line 14 would work with either line 9 or line 10. </p>   


<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class MySuper { protected MySuper() { System.out.print("ms "); } }
public class MyTester extends MySuper{
    
private MyTester() { System.out.print("mt "); }
    public static void main(String[] args) {
     new MySuper();
// class MyInner {
// private MyInner() { System.out.print("mi "); }
// { new MyTester(); }
// { new MySuper(); }
// }
// new MyInner();   
    }
        
}
    
</pre>
<pre class='out'>
run:
ms BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class MySuper { protected MySuper() { System.out.print("ms "); } }
public class MyTester extends MySuper{
    
private MyTester() { System.out.print("mt "); }
    public static void main(String[] args) {
     new MySuper();
 class MyInner {
 private MyInner() { System.out.print("mi "); }
// { new MyTester(); }
// { new MySuper(); }
 }
 new MyInner();   
    }
        
}
</pre>
<pre class='out'>
run:
ms mi BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class MySuper { protected MySuper() { System.out.print("ms "); } }
public class MyTester extends MySuper{
    
private MyTester() { System.out.print("mt "); }
    public static void main(String[] args) {
     new MySuper();
 class MyInner {
<mark> private MyInner() { System.out.print("mi "); }</mark>
 <mark>{ new MyTester();}</mark> 
// { new MySuper(); }
 }
 new MyInner();   
    }
        
}
</pre>
<pre class='out'>
run:
ms <mark>ms mt</mark> mi BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect3.exam;
    
class MySuper { protected MySuper() { System.out.print("ms "); } }
public class MyTester extends MySuper{
    
private MyTester() { System.out.print("mt "); }
    public static void main(String[] args) {
     new MySuper();
 class MyInner {
 private MyInner() { System.out.print("mi "); }
 { new MyTester(); }
 { new MySuper(); }
 }
 new MyInner();   
    }
        
}
    
</pre>
<pre class='out'>
run:
ms ms mt ms mi BUILD SUCCESSFUL (total time: 0 seconds)</pre>

