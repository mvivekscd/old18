 <h3>Write code that correctly applies the appropriate operators including assignment
            operators (limited to: =, +=, -=), arithmetic operators (limited to: +, -, *, /, %, ++, --),
            relational operators (limited to: <, <=, >, >=, ==, !=), the instanceof operator, logical
            operators (limited to: &, |, ^, !, &&, ||), and the conditional operator (? :), to produce
            a desired result. Write code that determines the equality of two objects or two primitives.</h3>
        
        
<p> <mark>operands </mark>are the things on the right or left side of the operator  </p>
        
<p> <mark>result</mark> of most operations is either a <mark>boolean</mark> or <mark>numeric</mark> value</p>


<p>operators that come overloaded </p>

<p>The <mark>+</mark> operator can be used to add two numeric primitives together, or to
    perform a concatenation operation if either operand is a String.</p>

<p>The<mark> &, |, and ^ operators can all be used in two different</mark> ways, although as
    of this version of the exam, their bit-twiddling capabilities won't be tested.
</p>

<h3>Assignment Operators</h3>

<p>
    When assigning a value to a primitive, <mark>size matters</mark>. Be sure you know <mark>when
    implicit casting will occur</mark>, <mark>when explicit casting is necessary</mark>, and when <mark>truncation
    might </mark>occur.
</p>

<p>a reference variable isn't an object; it's a way to get to an
    object.</p>


<p>When assigning a value to a reference variable, <mark>type matters</mark>. Remember the
    <mark>rules</mark> for <mark>supertypes, subtypes</mark>, and <mark>arrays</mark>.</p>

<p>the Java 5 exam differs from the 1.4 exam by <mark>moving away from bits</mark>, and
    towards the API.</p>


<p>you WON'T see</p>
<pre>
bit shifting operators
bitwise operators
two's complement
divide by zero stuff
    
</pre>

<h3>Compound Assignment Operators</h3>

<p>
    only the four
    most commonly used (<mark>+=, -=, *=, and /=</mark>), are on the exam
</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class compoundAssignmentTest {
    
    public static void main(String[] args) {
        int x=5;
        <mark>x *= 2 + 5;</mark>
        System.out.println("x="+x);
    }
        
}
</pre>
<pre class='out'>
run:
<mark>x=35</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>the
    <mark>expression on the right</mark> is <mark>always placed inside parentheses</mark>. it is evaluated like this:</p>
<pre>
x = <mark>x * (2 + 5)</mark>;
</pre>

<h3>
    Relational Operators</h3>

<p>The exam covers six relational operators (<mark><, <=, >, >=, ==, and !=</mark>)
</p>

<p>
    Relational
    operators always <mark>result in a boolean</mark> (true or false) value
    
</p>

<p>Java has four relational operators that can be <mark>used to compare any combination</mark> of
    <mark>integers, floating-point numbers, or characters</mark>:
</p>
<pre>
> greater than
>= greater than or equal to
< less than
<= less than or equal to
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class GuessAnimal {
    
    public static void main(String[] args) {
        String animal = "unknown";
        int weight = 700;
        <mark>char sex = 'f';</mark>
        <mark>double colorWaveLength = 1.630;</mark>
        if (weight >= 500) {
            animal = "elephant";
        }
        if (<mark>colorWaveLength > 1.621</mark>) {
            animal = "gray " + animal;
        }
        if (<mark>sex <= 'm'</mark>) {
            animal = "female " + animal;
        }
        System.out.println("The animal is a " + animal);
    }
}
    
</pre>

<pre class='out'>
run:
The animal is a female gray elephant
BUILD SUCCESSFUL (total time: 1 second)
    </pre>

<p>
    It's also
    <mark>legal to compare a character primitive with any number</mark>.(though it isn't great
    programming style)</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class GuessAnimal {
    
    public static void main(String[] args) {
        String animal = "unknown";
        char sex = 'f';
        if (<mark>sex >= 10</mark>) {
            animal = "female " + animal;
        }
        System.out.println("The animal is a " + animal);
    }
}
</pre>
<pre class='out'>
run:
The animal is a female unknown
BUILD SUCCESSFUL (total time: 1 second)
    </pre>  

<p>
    When
    comparing a character with a character, or a character with a number, Java will use
    the <mark>Unicode value of the character as the numerical value</mark>, for comparison.
    
</p>

<h3>"Equality" Operators</h3>

<pre>
== equals (also known as "equal to")
!= not equals (also known as "not equal to")
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class Equalitytest {
    
    public static void main(String[] args) {
        if(<mark>10=='s'</mark>){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }
            
        if(<mark>true=='s'</mark>){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }
            
    }
        
}
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/operators/Equalitytest.java
com\mvivekweb\ocpjp6\obj7\sect6\operators\Equalitytest.java:14: incomparable types: <mark>boolean and char</mark>
        if(true=='s'){
               ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>

<p>
    Each individual comparison can involve <mark>two numbers</mark> (including char), <mark>two
    boolean values</mark>, or <mark>two object reference variables</mark>.
</p>
<p>
    You <mark>can't compare incompatible
    types</mark>
</p>


<p><mark>cannot</mark> compare a <mark>boolean to a char</mark></p>

<p>There are four different types of things that can be tested</p>
<pre>
numbers
characters
boolean primitives
Object reference variables
</pre>
<p>
    what does == look at? The <mark>value in the variable-in other words, the bit pattern</mark>
</p>

<h3>Equality for Primitives</h3>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class ComparePrimitives {
    
    public static void main(String[] args) {
        System.out.println("char 'a' == 'a'? " + ('a' == 'a'));
        System.out.println("char 'a' == 'b'? " + ('a' == 'b'));
        System.out.println("5 != 6? " + (5 != 6));
        System.out.println("5.0 == 5L? " + (<mark>5.0 == 5L</mark>));
        System.out.println("true == false? " + (true == false));
    }
}
    
</pre>

<pre class='out'>
run:
char 'a' == 'a'? true
char 'a' == 'b'? false
5 != 6? true
5.0 == 5L? true
true == false? false
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>usually <mark>if a floating-point number is compared with an integer</mark> and
    the values are the same, the == operator returns true as expected.</p>


<h3>Equality for Reference Variables</h3>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
import javax.swing.JButton;
    
public class ReferenceTest {
    
    public static void main(String[] args) {
        JButton a = new JButton("Exit");
        JButton b = a;
        if (<mark>a == b</mark>) {
            System.out.println("equal");
        }
    }
}
    
</pre>
<pre class='out'>
run:
equal
BUILD SUCCESSFUL (total time: 2 seconds)

    </pre>
<p>
    Reference variables <mark>can be tested to see if they
    refer to the same object</mark> by <mark>using the ==</mark> operator
</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class BooleanIf {
    
    public static void main(String[] args) {
        boolean b= false;
        if(<mark>b=true</mark>){
            System.out.println("b is true");
        }else{
            System.out.println("b is false");
        }
    }
}
    
</pre>
<pre class='out'>
run:
b is true
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
<p>
    The result
    of any assignment expression is the value of the variable following the assignment. <mark>This
    substitution of = for == works only with boolean variables</mark>, since the if test can be done
    only on boolean expressions.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
import javax.swing.JButton;
    
public class CompareReference {
    
    public static void main(String[] args) {
        JButton a = new JButton("Exit");
        JButton b = new JButton("Exit");
        JButton c = a;
        System.out.println("Is reference a == b? " + (<mark>a == b</mark>));
        System.out.println("Is reference a == c? " + (a == c));
    }
}
</pre>
<pre class='out'>
run:
Is reference a == b? false
Is reference a == c? true
BUILD SUCCESSFUL (total time: 3 seconds)

    </pre>


<p>The ==
    operator <mark>will not test whether two objects are "meaningfully equivalent,"</mark> a concept
</p>

<h3>Equality for Enums</h3>

<p>Once you've declared an enum, it's not expandable. <mark>At runtime, there's no way to
    make additional enum constants.</mark></p>

<p>You <mark>can use either</mark> the <mark>== operator</mark> <mark>or the equals()</mark> method to determine
        if <mark>two variables</mark> are referring to the same enum constant:</p>
    
<pre>  
  package com.mvivekweb.ocpjp6.obj7.sect6.operators;
      
public class EnumEqual {
    enum Color {
        RED, BLUE
    }
    public static void main(String[] args) {
        Color c1 = Color.RED;
        Color c2 = Color.RED;
        if (<mark>c1 == c2</mark>) {
            System.out.println("==");
        }
        if (<mark>c1.equals(c2)</mark>) {
            System.out.println("dot equals");
        }
    }
}  
    
</pre>
<pre class='out'>
run:
<mark>==
dot equals</mark>
BUILD SUCCESSFUL (total time: 1 second)

</pre>

<h3>instanceof Comparison</h3>

<p>The instanceof operator is <mark>used for object reference variables only</mark>, and you can
    use it to <mark>check whether an object is of a particular type</mark>.</p>

<p>By type, we mean class or
    interface type-in other words, if the object referred to by the variable on the left
    side of the operator passes the IS-A test for the class or interface type on the right
    side</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class instanceofTest {
    
    public static void main(String[] args) {
        String s = new String("foo");
        if (<mark>s instanceof String</mark>) {
            System.out.print("s is a String");
        }
    }
}
    
</pre>
<pre class='out'>
run:
s is a String
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class B extends A {
    public static void main(String[] args) {
        A myA = new B();
        m2(myA);
    }
    public static void m2(A a) {
        if (<mark>a instanceof B</mark>) {
           <mark> ((B) a)</mark>.doBstuff(); //<mark> downcasting </mark>an A reference
        }// to a B reference
    }
    public static void doBstuff() {
        System.out.println("'a' refers to a B");
    }
}
    
class A {
}
    
</pre>
<pre class='out'>
run:
'a' refers to a B
BUILD SUCCESSFUL (total time: 0 seconds)

    </pre>

<p>the use of the instanceof operator <mark>protects the program from
    attempting an illegal downcast</mark>.
</p>
<p>
    You can <mark>test an object reference against its own class type</mark>, or <mark>any of its
    superclasses</mark>.
</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class instanceofTest {
    
    public static void main(String[] args) {
        String s = new String("foo");
        if (s instanceof String) {
            System.out.print("s is a String\n");
        }
        if (<mark>s instanceof Object</mark>) {
            System.out.print("s is a Object\n");
        }
    }
}
    
</pre>
<pre class='out'>
run:
s is a String
s is a Object
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class interfaceinstanceof {
    
    public static void main(String[] args) {
        A1 a = new A1();
        B1 b = new B1();
            
        System.out.println("(a instanceof Foo)" + <mark>(a instanceof Foo)</mark>);
        System.out.println("(a instanceof A1)" + (a instanceof A1));
        System.out.println("(b instanceof A1)" + <mark>(b instanceof A1)</mark>);
        System.out.println("(b instanceof Foo)" + (b instanceof Foo));
    }
}
    
interface Foo {
}
    
class A1 implements Foo {
}
    
class B1 extends A1 {
}
    
</pre>

<pre class='out'>
run:
(a instanceof Foo)true
(a instanceof A1)true
(b instanceof A1)true
(b instanceof Foo)true
BUILD SUCCESSFUL (total time: 0 seconds)
 </pre>
<p>
    it is <mark>legal to test </mark>whether the <mark>null reference is an instance of a class</mark>.
    This will <mark>always</mark> result in <mark>false</mark>, of course.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class InstanceTest {
    
    public static void main(String[] args) {
        String a = null;
        boolean b = <mark>null instanceof String</mark>;
        boolean c = <mark>a instanceof String</mark>;
        System.out.println(b + " " + c);
    }
}
    
</pre>
<pre class='out'>
run:
false false
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<h3>instanceof Compiler Error</h3>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class Dog {
    
    public static void main(String[] args) {
        Dog d = new Dog();
        System.out.println(<mark>d instanceof Cat</mark>);
    }
}
    
class Cat {
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/operators/Dog.java
com\mvivekweb\ocpjp6\obj7\sect6\operators\Dog.java:7: <mark>inconvertible types</mark>
found   : com.mvivekweb.ocpjp6.obj7.sect6.operators.Dog
required: com.mvivekweb.ocpjp6.obj7.sect6.operators.Cat
        System.out.println(d instanceof Cat);
                           ^
1 error
    
C:\vivek\docs\cert\ocpjp6\src>
    
</pre>

<p>Remember that <mark>arrays are objects</mark>, even if the array is an array of
    primitives.</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class ArrayInstanceof {
    
    public static void main(String[] args) {
        int[] nums = new int[0];
        if (<mark>nums instanceof Object</mark>) {
            System.out.println("Array is object");
        } // result is true
    }
}
    
</pre>
<pre class='out'>
run:
Array is object
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class ArrayInstanceof1 {
    
    public static void main(String[] args) {
        <mark>Foo1[] fooarr= new Foo1[0];</mark>
        System.out.println("fooarr instanceof Foo1[]="+(<mark>fooarr instanceof Foo1[]</mark>));
        System.out.println("fooarr instanceof Bar[]="+(fooarr instanceof Bar[]));
         System.out.println("fooarr instanceof Face[]="+(fooarr instanceof Face[]));
         System.out.println("fooarr instanceof Object="+(fooarr instanceof Object));
    }
        
}
interface Face { }
class Bar implements Face{ }
class Foo1 extends Bar { }
    
</pre>
<pre class='out'>
run:
fooarr instanceof Foo1[]=true
fooarr instanceof Bar[]=true
fooarr instanceof Face[]=true
fooarr instanceof Object=true
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<h3>Arithmetic Operators</h3>

<h3>The Remainder (%) Operator</h3>

<p>The
    remainder operator divides the left operand by the right operand, and the <mark>result is
    the remainder</mark></p>
    <pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class MathTest {
    
    public static void main(String[] args) {
        int x = 15;
        int y = x % 4;
        System.out.println("The result of 15 % 4 is the "
                + "remainder of 15 divided by 4. The remainder is " + y);
    }
}
    
    </pre>
  <pre class='out'>  
run:
The result of 15 % 4 is the remainder of 15 divided by 4. The remainder is 3
BUILD SUCCESSFUL (total time: 1 second)    

    </pre>

    <p>remember that the<mark> * , / ,
        and % operators</mark> have a <mark>higher precedence</mark> than the <mark>+ and -</mark> operators.</p>
    
<h3>String Concatenation Operator </h3>
<pre> 
 package com.mvivekweb.ocpjp6.obj7.sect6.operators;
     
public class StringTest {
    
    public static void main(String[] args) {
        String a = "String";
        int b = 3;
        int c = 7;
        System.out.println(<mark>a + b + c</mark>);
        System.out.println(a + (b + c));
        System.out.println(<mark>b + a + c</mark>);
    }
}
    
</pre>
<pre class='out'>
run:
String37
String10
3String7
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>
    
<h3>Increment and Decrement Operators
</h3>

<p>two operators that will increment or decrement a variable by exactly one.</p>


++ increment (prefix and postfix)
<br>-- decrement (prefix and postfix)

<p>Whether the operator comes <mark>before or after the operand</mark> can <mark>change the
    outcome</mark> of an expression</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class incrementTest {
    static int players = 0;
    public static void main(String[] args) {
        System.out.println("players online: " + <mark>players++</mark>);
        System.out.println("The value of players is " + players);
        System.out.println("The value of players is now " + <mark>++players</mark>);
    }
}
    
</pre>
<pre class='out'>
run:
players online: 0
The value of players is 1
The value of players is now 2
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>players++ incremented by one but <mark>only after the value of players is used
    in the expression</mark>.</p>
    
    <p>  <mark> use first and increment later</mark> </p>

<p>++players the increment happens<mark> before the
        value of the variable is used</mark></p>

<p><mark>increment first and use later</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class incrementTest1 {
    
    public static void main(String[] args) {
        int x = 2;
        int y = 3;
        if ((y == x++) | (x < ++y)) {  <mark>// 3==2 | 3<4</mark>
            System.out.println("x = " + x + " y = " + y);
        }
    }
}
    
</pre>
<pre class='out'>
run:
<mark>x = 3 y = 4</mark>
BUILD SUCCESSFUL (total time: 1 second)
</pre>

<p>You can read the code as follows: "If<mark> 3 is equal to 2 OR 3 < 4</mark>"</p>

<p><mark>final variables can't be changed</mark></p>

<p>the increment and decrement
    operators can't be used with them, and <mark>any attempt to do so will result in a compiler
    error.</mark></p>
    <pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class finalincrement {
    
    public static void main(String[] args) {
        final int x = 5;
        int y = x++;
    }
}
    
    </pre>
    
 <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/operators/finalincrement.java
com\mvivekweb\ocpjp6\obj7\sect6\operators\finalincrement.java:7: <mark>cannot assign a value to final variable x</mark>
        int y = x++;
                ^
1 error

C:\vivek\docs\cert\ocpjp6\src>   

    </pre>
    
<p>   expect a violation like this to be<mark> buried deep in a complex piece
        of code.</mark></p>
    
<h3>Conditional Operator  </h3>
        
<p>The conditional operator is a <mark>ternary operator</mark> (it has three operands)  </p>  
        
<pre>x = (boolean expression) ? value to assign if true : value to assign if false
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class Salary {
    
    public static void main(String[] args) {
        int numOfPets = 3;
        String status = numOfPets < 4 <mark>?</mark> "Pet limit not exceeded" <mark>:</mark> "too many pets";
        System.out.println("This pet status is " + status);
    }
}
    
</pre>
<pre class='out'>
run:
This pet status is Pet limit not exceeded
BUILD SUCCESSFUL (total time: 1 second)
</pre>


<p>You can even <mark>nest
    conditional operators</mark> into one statement:
</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class AssignmentOps {
    
    public static void main(String[] args) {
        int sizeOfYard = 10;
        int numOfPets = 4;
        String status = numOfPets < 4 <mark>?</mark> "Pet count OK" <mark>:</mark> sizeOfYard > 10 <mark>?</mark> "Pet limit on the edge" <mark>: </mark>"too many pets";
        System.out.println("Pet status is " + status);
    }
}
    
</pre>
<pre class='out'>
run:
Pet status is too many pets
BUILD SUCCESSFUL (total time: 1 second)

</pre>


<p>conditional operators are <mark>sometimes confused with assertion</mark> statements
</p>

<h3>Logical Operators</h3>

<p>exam objectives specify six "logical" operators (<mark>&, |, ^, !, &&, and ||</mark>).</p>

<h3>Bitwise Operators (Not on the Exam!)</h3>
<pre>
byte b1 = 6 & 8;
byte b2 = 7 | 9;
byte b3 = 5 ^ 4;
System.out.println(b1 + " " + b2 + " " + b3);
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class BitwiseTest {
    
    public static void main(String[] args) {
        byte b1 = 6 & 8;
        byte b2 = 7 | 9;
        byte b3 = 5 ^ 4;
        System.out.println(b1 + " " + b2 + " " + b3);
    }
}
    
</pre>
<pre class='out'>
run:
0 15 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>
    Bitwise operators compare two variables bit by bit, and return a variable
    whose bits have been set based on whether the two variables being compared had
    respective bits that were either both "on" (&), one or the other "on" (|), <mark>or exactly
    one "on" (^)</mark>.
</p>

<p><mark>BITWISE OPERATORS ARE NOT ON THE EXAM!</mark></p>

<h3>Short-Circuit Logical Operators</h3>


<p>two short-circuit logical operators</p>
<pre>
&& short-circuit AND
|| short-circuit OR
    
</pre>


<p><mark>both operands must be true</mark></p>

<p>The || operator is similar to the && operator, except that it evaluates to true if
    EITHER of the operands is true.</p>


<p>The || and && operators <mark>work only with boolean operands.</mark>
</p>


<h3>Logical Operators (Not Short-Circuit)</h3>

<p>There are <mark>two non-short-circuit logical operators</mark>.</p>
<pre>
& non-short-circuit AND
| non-short-circuit OR
    
</pre>
<p>
    <mark>they evaluate both sides
        of the expression</mark>, <mark>always</mark>! They're inefficient.
</p>
<p>
    For example, even if the first operand
    (left side) in an & expression is false, <mark>the second operand will still be evaluated</mark>-
    even though it's now impossible for the result to be true
</p>
<p>
    And the | is just as
    inefficient: if the first operand is true, <mark>the Java Virtual Machine (JVM) still plows
    ahead and evaluates the second operand even when it knows the expression will be
    true</mark> regardless.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class NonShortcircuitTest {
    
    public static void main(String[] args) {
        int z = 5;
        if (++z > 5 <mark>|</mark> ++z > 6) {
            z++;
        }
        System.out.println("z=" + z);
    }
}
    
</pre>
<pre class='out'>
run:
z=<mark>8</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<h3>Logical Operators ^ and !</h3>


<p>The last two logical operators on the exam are</p>
<pre>
^ exclusive-OR (XOR)
! boolean invert
    
</pre>

<p>The ^ (exclusive-OR) operator <mark>evaluates only boolean values</mark>.</p>

<p>For an <mark>exclusive-OR (^)
    expression to be true</mark>, EXACTLY <mark>one operand must be true</mark></p>

<p>a logical operation that <mark>outputs true only when inputs differ</mark> </p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.operators;
    
public class XorTest {
    
    public static void main(String[] args) {
        System.out.println("xor " + ((2 < 3) <mark>^</mark> (4 > 3)));
        System.out.println("xor " + ((4 < 3) <mark>^</mark> (4 > 3)));
        System.out.println("xor " + ((2 < 3) ^ (4 > 5)));
        System.out.println("xor " + ((4 < 3) ^ (2 > 3)));
    }
}
    
</pre>
<pre class='out'>
run:
xor false
<mark>xor true
xor true</mark>
xor false
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
