<p>It's possible for objects, on whom finalize() has been invoked by the JVM, to avoid
    the GC.</p>

<p>For every object that the GC considers collecting, the <mark>GC remembers whether
    finalize() has been invoked for that specific object</mark>.</p>


<p>while the JVM will invoke finalize() no more than once per
    object, <mark>your code is free to invoke finalize()</mark></p>
    
<pre>   
Given:
1. import java.util.*;
2. public class Drunken {
3. public static void main(String[] args) {
4. Set&lt;Stuff&gt; s = new HashSet&lt;Stuff&gt;();
5. s.add(new Stuff(3)); s.add(new Stuff(4)); s.add(new Stuff(4));
6. s.add(new Stuff(5)); s.add(new Stuff(6));
7. s = null;
8. // do more stuff
9. }
10. }
11. class Stuff {
12. int value;
13. Stuff(int v) { value = v; }
14. }
When line 8 is reached, how many objects are eligible for garbage collection?</pre>

<pre class='out'>6</pre>


<p>Because Stuff doesn't override equals(), there are five objects in the
    HashSet, plus the HashSet is also an object. The int primitive associated with each
    Stuff object is not an object. Note: Even if equals() was overridden, six objects would
    be eligible. The duplicate Stuff object, while not in the Set, would still be eligible.</p>

<p>A Java program can request that the GC runs, but such a request does NOT guarantee that
    the GC will actually run.</p>

<p>If the GC decides to delete an object, and if finalize() has never been invoked for that
    object, it is guaranteed that the GC will invoke finalize() for that object before the
    object is deleted.</p>

<p>If Object X has a reference to Object Y, then Object Y can be GCed.</p>

<p>if no Object has a reference to Object X, then both X and Y can
    be GCed.</p>

<p>it's possible for finalize() to send a reference to itself
    back to an object in a live thread, making the GC candidate "ineligible" for GC</p>


<p>When the GC runs, it decides whether to remove objects from the heap</p>

<p>GC never touches the stack</p>
<pre>
Given:
3. public class Fiji {
4. static Fiji base;
5. Fiji f;
6. public static void main(String[] args) {
7. new Fiji().go();
8. // do more stuff
9. }
10. void go() {
11. Fiji f1 = new Fiji();
12. base = f1;
13. Fiji f2 = new Fiji();
14. f1.f = f2;
15. Fiji f3 = f1.f;
16. f2.f = f1;
17. base = null; f1 = null; f2 = null;
18. // do stuff
19. } }
Which are true? (Choose all that apply.)
    
</pre>

<p>At line 8, three objects are eligible for garbage collection.</p>

<p>At line 18, 0 objects are eligible for garbage collection.</p>

<p>A total of three Fiji objects are created. At line 18, the <mark>anonymous
        (main()'s), Fiji can still be accessed via "this"</mark>, and the other <mark>two Fiji objects can be
            accessed via f3</mark> (f3 and f3.f).</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.exam;
class Account { Long acctNum, password; }
    
public class Banker {
    
    
    public static void main(String[] args) {
        new Banker().go();
            
    }
void go() {
 Account a1 = new Account();
 a1.acctNum = new Long("1024");
 Account a2 = a1;
 Account a3 = a2;
 a3.password = a1.acctNum.longValue();
 a2.password = 4455L;
 System.out.println("a1.password="+a1.password);
 System.out.println("a2.password="+a2.password);
 System.out.println("a3.password="+a3.password);
 System.out.println("a1.acctNum="+a1.acctNum);
 System.out.println("a1.acctNum.longValue="+a1.acctNum.longValue());
     
 }
}
    
</pre>
<pre class='out'>
run:
a1.password=4455
a2.password=4455
a3.password=4455
a1.acctNum=1024
a1.acctNum.longValue=1024
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>because only one Account object is created, and a1, a2, and a3 all refer to it.
    The Account object has two Long objects associated with it, and finally the anonymous
    Banker object is also eligible.</p>
<pre>
Given:
2. public class Toolbox {
3. static Toolbox st;
4. public static void main(String[] args) {
5. new Toolbox().go();
6. // what's eligible?
7. }
8. void go() {
9. MyInner in = new MyInner();
10. Integer i3 = in.doInner();
11. Toolbox t = new Toolbox();
12. st = t;
13. System.out.println(i3);
14. }
15. class MyInner {
16. public Integer doInner() { return new Integer(34); }
17. }
18. }
When the code reaches line 6, which are eligible for garbage collection? (Choose all that
apply.)
</pre>
<pre class='out'>
D. The object created on line 5.
E. The object created on line 9.
F. The object created on line 10.</pre>

<p>because main() could still access line 11's Toolbox object through st.</p>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.exam;
    
    
    
public class BigData {
    
static BigData bd;
    public static void main(String[] args) {
        new BigData().doStuff();
    }
void doStuff() { }
 // insert code here
<mark> protected void finalize() throws Throwable {</mark>
 bd = this;
 }
}
class MyException extends Exception { }
    
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.exam;
    
    
    
public class BigData {
    
static BigData bd;
    public static void main(String[] args) {
        new BigData().doStuff();
    }
void doStuff() { }
 // insert code here
 <mark>protected void finalize() {</mark>

 bd = this;
 }
}
class MyException extends Exception { }
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.exam;
    
    
    
public class BigData {
    
static BigData bd;
    public static void main(String[] args) {
        new BigData().doStuff();
    }
void doStuff() { }
 // insert code here
 protected void finalize() throws MyException {
 bd = this;
 }
}
class MyException extends Exception { }
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect4.exam;
    
    
    
public class BigData {
    
static BigData bd;
    public static void main(String[] args) {
        new BigData().doStuff();
    }
void doStuff() { }
 // insert code here
<mark>void finalize() {</mark>
 bd = this;
 }
}
class MyException extends Exception { }
</pre>
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect4/exam/BigData.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect4\exam\BigData.java:15: error: finalize() in BigData cannot override finalize() in Object
void finalize() {
     ^
  <mark>attempting to assign weaker access privileges; was protected</mark>
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<p>the GC will collect any given object after the JVM has called
    finalize() on that object.<mark>incorrect because finalize()</mark>
    copies a reference of the object to the static variable bd</p>

<p>Because of the way finalize() has been <mark>overridden</mark>, the <mark>GC can collect eligible</mark>
    objects of type BigData.</p>
<pre>
Given:
1. import java.util.*;
2. public class Analyzer {
3. static List&lt;Exception&gt; me;
4. Exception myEx;
5. public static void main(String[] args) {
6. Analyzer[] aa = {new Analyzer(), new Analyzer()};
7. me = new ArrayList&lt;Exception&gt;();
8. for(int i = 0; i < 2; i++) {
9. try {
10. if(i == 0) throw new Exception();
11. if(i == 1) throw new MyException();
12. }
13. catch(Exception e) {
14. me.add(e);
15. aa[i].myEx = e;
16. }
17. }
18. System.out.println(aa.length + " " + me.size());
19. aa = null; me = null;
20. // do more stuff
21. } }
22. class MyException extends Exception { }
When line 20 is reached, how many objects are eligible for garbage collection?</pre>

<pre class='out'>6</pre>

<p>Line 6 creates three objects: an array and two Analyzer objects. Line 7
    creates an ArrayList object. Line 10 and 11 each create some sort of Exception object.
    While it's true that each Analyzer also has a reference to an Exception object, those
    references are referring to the same exceptions that are in the array.</p>



