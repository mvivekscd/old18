<table class="pgt">
    <caption>Determining Access to Class Members</caption>
    <thead>
        <tr>
            <th>Visibility</th>
            <th>Public</th>
            <th>Protected</th>
            <th>Default</th>
            <th>Private</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Visibility">From the same class</td>
            <td data-label="Public">Yes</td>
            <td data-label="Protected">Yes</td>
            <td data-label="Default">Yes</td>
            <td data-label="Private">Yes</td>
        </tr>
        <tr>
            <td data-label="Visibility">From any class in the same
                package</td>
            <td data-label="Public">Yes</td>
            <td data-label="Protected">Yes</td>
            <td data-label="Default">Yes</td>
            <td data-label="Private">No</td>
        </tr>
        <tr>
            <td data-label="Visibility">From a subclass in the same
                package</td>
            <td data-label="Public">Yes</td>
            <td data-label="Protected">Yes</td>
            <td data-label="Default">Yes</td>
            <td data-label="Private">No</td>
        </tr>
        <tr>
            <td data-label="Visibility">From a subclass outside the
                same package</td>
            <td data-label="Public">Yes</td>
            <td data-label="Protected">Yes, through
                inheritance</td>
            <td data-label="Default">NO</td>
            <td data-label="Private">No</td>
        </tr>
        <tr>
            <td data-label="Visibility">From any non-subclass class
                outside the package</td>
            <td data-label="Public">Yes</td>
            <td data-label="Protected">No</td>
            <td data-label="Default">No</td>
            <td data-label="Private">No</td>
        </tr>           
    </tbody>
</table>
<br>
<br>
<table class="pgt">
    <caption>Differences Between Overloaded and Overridden Methods</caption>
    <thead>
        <tr>
            <th>s.no</th>
            <th>Overloaded Method</th>
            <th>Overridden Method</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="s.no">Argument(s)</td>
            <td data-label="Overloaded Method">Must change.</td>
            <td data-label="Overridden Method">Must not change.</td>

        </tr>
        <tr>
            <td data-label="s.no">Return type</td>
            <td data-label="Overloaded Method">Can change.</td>
            <td data-label="Overridden Method">Can't change except for
                covariant returns.</td>

        </tr>
        <tr>
            <td data-label="s.no">Exceptions</td>
            <td data-label="Overloaded Method">Can change.</td>
            <td data-label="Overridden Method">Can reduce or eliminate.
                Must not throw new
                or broader checked
                exceptions.</td>

        </tr>
        <tr>
            <td data-label="s.no">Access</td>
            <td data-label="Overloaded Method">Can change.</td>
            <td data-label="Overridden Method">Must not make more
                restrictive (can be less
                restrictive).</td>

        </tr>  
        <tr>
            <td data-label="s.no">Invocation</td>
            <td data-label="Overloaded Method">Reference type determines which overloaded version (based
                on declared argument types) is selected. Happens at compile
                time. The actual method that's invoked is still a virtual method
                invocation that happens at runtime, but the compiler will
                already know the signature of the method to be invoked. So at
                runtime, the argument match will already have been nailed
                down, just not the class in which the method lives.</td>
            <td data-label="Overridden Method">Object type (in other
                words, the type of the
                actual instance on the
                heap) determines which
                method is selected.
                Happens at runtime.</td>

        </tr>

    </tbody>
</table>

<br>
<br>
<table class="pgt">
    <caption>Compiler-Generated Constructor Code</caption>
    <thead>
        <tr>
            <th>Class Code</th>
            <th>Compiler Generated Constructor Code</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Class Code">class Foo { }</td>
            <td data-label="Compiler Generated Constructor Code">
                <pre> 
             class Foo {
                <mark>Foo() {
                super();
                }</mark>
                }   
                </pre>

            </td>
        </tr>
        <tr>
            <td data-label="Class Code">
                <pre>
                class Foo {
Foo() { }
}
                </pre>
            </td>
            <td data-label="Compiler Generated Constructor Code">
                <pre> 
             class Foo {
                Foo() {
                <mark>super();</mark>
                }
                }   
                </pre>

            </td>
        </tr>
        <tr>
            <td data-label="Class Code">
                <pre>
                public class Foo { }
                </pre>
            </td>
            <td data-label="Compiler Generated Constructor Code">
                <pre> 
             public class Foo {
<mark>public Foo() {
super();
}</mark>
}  
                </pre>

            </td>
        </tr>       
        <tr>
            <td data-label="Class Code">
                <pre>
                class Foo {
Foo(String s) { }
}
                </pre>
            </td>
            <td data-label="Compiler Generated Constructor Code">
                <pre> 
             class Foo {
Foo(String s) {
<mark>super();</mark>
}
}
                </pre>

            </td>
        </tr>              
        <tr>
            <td data-label="Class Code">
                <pre>
               class Foo {
Foo(String s) {
super();
}
}
                </pre>
            </td>
            <td data-label="Compiler Generated Constructor Code">
                Nothing, compiler doesn't need to insert
                anything.

            </td>
        </tr>    
        <tr>
            <td data-label="Class Code">
                <pre>
               class Foo {
void Foo() { }
}
                </pre>
            </td>
            <td data-label="Compiler Generated Constructor Code">
                <pre> 
             class Foo {
void Foo() { }
<mark>Foo() {
super();
}</mark>
}
<mark>(void Foo() is a method, not a constructor.)</mark>
                </pre>

            </td>
        </tr>              
    </tbody></table>
<br><br>

<table class="pgt">
    <caption>Default Values for Primitives and Reference Types</caption>
    <thead>
        <tr>
            <th>Variable Type</th>
            <th>Default Value</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Variable Type">Object reference</td>
            <td data-label="Default Value">null (not referencing any object)</td>

        </tr>
        <tr>
            <td data-label="Variable Type">byte, short, int, long</td>
            <td data-label="Default Value">0</td>

        </tr>
        <tr>
            <td data-label="Variable Type">float, double</td>
            <td data-label="Default Value">0.0</td>

        </tr>
        <tr>
            <td data-label="Variable Type">boolean</td>
            <td data-label="Default Value">false</td>

        </tr> 
        <tr>
            <td data-label="Variable Type">char</td>
            <td data-label="Default Value">'\u0000'</td>

        </tr>

    </tbody></table>  

<br><br>

<table class="pgt">
    <caption>Wrapper Classes and Their Constructor Arguments</caption>
    <thead>
        <tr>
            <th>Primitive</th>
            <th>Wrapper Class</th>
            <th>Constructor Arguments</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Primitive">boolean</td>
            <td data-label="Wrapper Class">Boolean</td>
            <td data-label="Constructor Arguments">boolean or String</td>
        </tr>
        <tr>
            <td data-label="Primitive">byte</td>
            <td data-label="Wrapper Class">Byte</td>
            <td data-label="Constructor Arguments">byte or String</td>
        </tr>        
        <tr>
            <td data-label="Primitive">char</td>
            <td data-label="Wrapper Class">Character</td>
            <td data-label="Constructor Arguments">char</td>
        </tr>
        <tr>
            <td data-label="Primitive">double</td>
            <td data-label="Wrapper Class">Double</td>
            <td data-label="Constructor Arguments">double or String</td>
        </tr>         
        <tr>
            <td data-label="Primitive">float</td>
            <td data-label="Wrapper Class">Float</td>
            <td data-label="Constructor Arguments">float, double, or String</td>
        </tr>
        <tr>
            <td data-label="Primitive">int</td>
            <td data-label="Wrapper Class">Integer</td>
            <td data-label="Constructor Arguments">int or String</td>
        </tr>        
        <tr>
            <td data-label="Primitive">long</td>
            <td data-label="Wrapper Class">Long</td>
            <td data-label="Constructor Arguments">long or String</td>
        </tr>
        <tr>
            <td data-label="Primitive">short</td>
            <td data-label="Wrapper Class">Short</td>
            <td data-label="Constructor Arguments">short or String</td>
        </tr>           
    </tbody>
</table>

<br><br>

<table class="pgt">
    <caption>Common Wrapper Conversion Methods</caption>
    <thead>
        <tr>
            <th>Method</th>
            <th>Boolean</th>
            <th>Byte</th>
            <th>Character</th>
            <th>Double</th>
            <th>Float</th>
            <th>Integer</th>
            <th>Long</th>
            <th>Short</th>            
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Method">byteValue</td>
            <td data-label="Boolean">&nbsp;</td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">doubleValue</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>        
        <tr>
            <td data-label="Method">floatValue</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">intValue</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>        
        <tr>
            <td data-label="Method">longValue</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">shortValue</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>        
        <tr>
            <td data-label="Method">parseXxx s,n</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">parseXxx s,n
                (with radix)</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double"></td>
            <td data-label="Float"></td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>        
        <tr>
            <td data-label="Method">valueOf s,n</td>
            <td data-label="Boolean">s</td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">valueOf s,n
                (with radix)</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte">x</td>
            <td data-label="Character"></td>
            <td data-label="Double"></td>
            <td data-label="Float"></td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>        
        <tr>
            <td data-label="Method">toString</td>
            <td data-label="Boolean">x</td>
            <td data-label="Byte">x</td>
            <td data-label="Character">x</td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr>
        <tr>
            <td data-label="Method">toString s
                (primitive)</td>
            <td data-label="Boolean">x</td>
            <td data-label="Byte">x</td>
            <td data-label="Character">x</td>
            <td data-label="Double">x</td>
            <td data-label="Float">x</td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short">x</td>            
        </tr> 
        <tr>
            <td data-label="Method">toString s
                (primitive, radix)</td>
            <td data-label="Boolean"></td>
            <td data-label="Byte"></td>
            <td data-label="Character"></td>
            <td data-label="Double"></td>
            <td data-label="Float"></td>
            <td data-label="Integer">x</td>
            <td data-label="Long">x</td>
            <td data-label="Short"></td>            
        </tr>          
    </tbody>
</table>
<br><br>
<table class="pgt">
    <caption>Descriptions and Sources of Common Exceptions.</caption>
    <thead>
        <tr>
            <th>Exception</th>
            <th>Description</th>
            <th>Typically
                Thrown</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Exception">ArrayIndexOutOfBoundsException
                (Chapter 3, "Assignments")</td>
            <td data-label="Description">Thrown when attempting to access an array
                with an invalid index value (either negative
                or beyond the length of the array).</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr>
        <tr>
            <td data-label="Exception">ClassCastException
                (Chapter 2, "Object Orientation")</td>
            <td data-label="Description">Thrown when attempting to cast a reference
                variable to a type that fails the IS-A test.</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr>
        <tr>
            <td data-label="Exception">IllegalArgumentException
                (This chapter)</td>
            <td data-label="Description">Thrown when a method receives an argument
                formatted differently than the method
                expects.</td>
            <td data-label="Typically
                Thrown">Programmatically</td>
        </tr>
        <tr>
            <td data-label="Exception">IllegalStateException
                (Chapter 6, "Formatting")</td>
            <td data-label="Description">Thrown when the state of the environment
                doesn't match the operation being attempted,
                e.g., using a Scanner that's been closed.</td>
            <td data-label="Typically
                Thrown">Programmatically</td>
        </tr>
        <tr>
            <td data-label="Exception">NullPointerException
                (Chapter 3, "Assignments")</td>
            <td data-label="Description">Thrown when attempting to access an object
                with a reference variable whose current value
                is null.</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr>
        <tr>
            <td data-label="Exception">NumberFormatException
                (Chapter 3, "Assignments")</td>
            <td data-label="Description">Thrown when a method that converts a
                String to a number receives a String that it
                cannot convert.</td>
            <td data-label="Typically
                Thrown">Programmatically</td>
        </tr>
        <tr>
            <td data-label="Exception">AssertionError
                (This chapter)</td>
            <td data-label="Description">Thrown when a statement's boolean test
                returns false.</td>
            <td data-label="Typically
                Thrown">Programmatically</td>
        </tr>
        <tr>
            <td data-label="Exception">ExceptionInInitializerError
                (Chapter 3, "Assignments")</td>
            <td data-label="Description">Thrown when attempting to initialize a static
                variable or an initialization block.</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr>
        <tr>
            <td data-label="Exception">StackOverflowError
                (This chapter)</td>
            <td data-label="Description">Typically thrown when a method recurses
                too deeply. (Each invocation is added to the
                stack.)</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr> 
        <tr>
            <td data-label="Exception">NoClassDefFoundError
                (Chapter 10, "Development")</td>
            <td data-label="Description">Thrown when the JVM can't find a class it
                needs, because of a command-line error, a
                classpath issue, or a missing .class file.</td>
            <td data-label="Typically
                Thrown">By the JVM</td>
        </tr>  
    </tbody>
</table>
<br>
<br>

<table class="pgt">
    <caption>Methods of Class Object Covered on the Exam</caption>
    <thead>
        <tr>
            <th>Method</th>
            <th>Description</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Method">boolean equals (Object obj)</td>
            <td data-label="Description">Decides whether two objects are meaningfully equivalent.</td>

        </tr>
        <tr>
            <td data-label="Method">void finalize()</td>
            <td data-label="Description">Called by garbage collector when the garbage collector sees that
                the object cannot be referenced.</td>

        </tr>
        <tr>
            <td data-label="Method">int hashCode()</td>
            <td data-label="Description">Returns a hashcode int value for an object, so that the object can
                be used in Collection classes that use hashing, including Hashtable,
                HashMap, and HashSet.</td>

        </tr>
        <tr>
            <td data-label="Method">final void notify()</td>
            <td data-label="Description">Wakes up a thread that is waiting for this object's lock.</td>

        </tr>
        <tr>
            <td data-label="Method">final void notifyAll()</td>
            <td data-label="Description">Wakes up all threads that are waiting for this object's lock.</td>

        </tr>
        <tr>
            <td data-label="Method">final void wait()</td>
            <td data-label="Description">Causes the current thread to wait until another thread calls
                notify() or notifyAll() on this object.</td>

        </tr>  
        <tr>
            <td data-label="Method">String toString()</td>
            <td data-label="Description">Returns a 'text representation' of the object.</td>

        </tr>         
    </tbody>
</table>
<br>
<br>
<table class="pgt">
    <caption>Collection Interface Concrete Implementation Classes</caption>
    <thead>
        <tr>
            <th>Class</th>
            <th>Map</th>
            <th>Set</th>
            <th>List</th>
            <th>Ordered</th>
            <th>Sorted</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Class">HashMap</td>
            <td data-label="Map">x</td>
            <td data-label="Set"></td>
            <td data-label="List"></td>
            <td data-label="Ordered">No</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">Hashtable</td>
            <td data-label="Map">x</td>
            <td data-label="Set"></td>
            <td data-label="List"></td>
            <td data-label="Ordered">No</td>
            <td data-label="Sorted">No</td>            
        </tr> 
        <tr>
            <td data-label="Class">TreeMap</td>
            <td data-label="Map">x</td>
            <td data-label="Set"></td>
            <td data-label="List"></td>
            <td data-label="Ordered">Sorted</td>
            <td data-label="Sorted">By natural order or
                custom comparison rules</td>            
        </tr>
        <tr>
            <td data-label="Class">LinkedHashMap</td>
            <td data-label="Map">x</td>
            <td data-label="Set"></td>
            <td data-label="List"></td>
            <td data-label="Ordered">By insertion order
                or last access order</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">HashSet</td>
            <td data-label="Map"></td>
            <td data-label="Set">x</td>
            <td data-label="List"></td>
            <td data-label="Ordered">No</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">TreeSet</td>
            <td data-label="Map"></td>
            <td data-label="Set">x</td>
            <td data-label="List"></td>
            <td data-label="Ordered">Sorted</td>
            <td data-label="Sorted">By natural order or
                custom comparison rules</td>            
        </tr> 
        <tr>
            <td data-label="Class">LinkedHashSet</td>
            <td data-label="Map"></td>
            <td data-label="Set">x</td>
            <td data-label="List"></td>
            <td data-label="Ordered">By insertion order</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">ArrayList</td>
            <td data-label="Map"></td>
            <td data-label="Set"></td>
            <td data-label="List">x</td>
            <td data-label="Ordered">By index</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">Vector</td>
            <td data-label="Map"></td>
            <td data-label="Set"></td>
            <td data-label="List">x</td>
            <td data-label="Ordered">By index</td>
            <td data-label="Sorted">No</td>            
        </tr>
        <tr>
            <td data-label="Class">LinkedList</td>
            <td data-label="Map"></td>
            <td data-label="Set"></td>
            <td data-label="List">x</td>
            <td data-label="Ordered">By index</td>
            <td data-label="Sorted">No</td>            
        </tr> 
        <tr>
            <td data-label="Class"><mark>PriorityQueue</mark></td>
            <td data-label="Map"></td>
            <td data-label="Set"></td>
            <td data-label="List"></td>
            <td data-label="Ordered">Sorted</td>
            <td data-label="Sorted">By to-do order</td>            
        </tr>

    </tbody></table>

<br>
<br>

<table class="pgt">
    <caption>Key Methods in Arrays</caption>
    <thead>
        <tr>
            <th>Arrays</th>
            <th>Description</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-label="Arrays">static List asList(T[])</td>
            <td data-label="Description">Convert an array to a List (and bind them).</td>

        </tr>
        <tr>
            <td data-label="Arrays">
                <pre> 
static int binarySearch(Object[], key)
static int binarySearch(primitive[], key)</pre>
            </td>
            <td data-label="Description">Search a sorted array for a given value, return
                an index or insertion point.</td>

        </tr>
        <tr>
            <td data-label="Arrays">
                <pre> 
static int binarySearch(T[], key, Comparator)</pre>
            </td>
            <td data-label="Description">Search a Comparator-sorted array for a value.</td>

        </tr> 
        <tr>
            <td data-label="Arrays">
                <pre> 
static boolean equals(Object[], Object[])
static boolean equals(primitive[], primitive[])</pre>
            </td>
            <td data-label="Description">Compare two arrays to determine if their
                contents are equal.</td>

        </tr>
        <tr>
            <td data-label="Arrays">
                <pre> 
public static void sort(Object[ ] )
public static void sort(primitive[ ] )</pre>
            </td>
            <td data-label="Description">Sort the elements of an array by natural
                order.</td>

        </tr>
        <tr>
            <td data-label="Arrays">
                <pre> 
public static void sort(T[], Comparator)</pre>
            </td>
            <td data-label="Description">Sort the elements of an array using a
                Comparator.</td>

        </tr>
        <tr>
            <td data-label="Arrays">
                <pre> 
public static String toString(Object[])
public static String toString(primitive[])</pre>
            </td>
            <td data-label="Description">Create a String containing the contents of
                an array.</td>

        </tr>        
    </tbody>
</table>
<br>
<br>

<table class="pgt">
    <caption>Key Methods in Collections</caption>
    <thead>
        <tr>
            <th>Collections</th>
            <th>Description</th>


        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="Collections">
                <pre> 
static int binarySearch(List, key)
static int binarySearch(List, key, Comparator)</pre>
            </td>
            <td data-label="Description">Search a "sorted" List for a given value,
                return an index or insertion point.</td>

        </tr>
        <tr>
            <td data-label="Collections">
                <pre> 
static void reverse(List)</pre>
            </td>
            <td data-label="Description">Reverse the order of elements in a List.</td>

        </tr>   
        <tr>
            <td data-label="Collections">
                <pre> 
static Comparator reverseOrder()
static Comparator reverseOrder(Comparator)</pre>
            </td>
            <td data-label="Description">Return a Comparator that sorts the reverse of
                the collection's current sort sequence.</td>

        </tr>
        <tr>
            <td data-label="Collections">
                <pre> 
static void sort(List)
static void sort(List, Comparator)</pre>
            </td>
            <td data-label="Description">Sort a List either by natural order or by a
                Comparator.</td>

        </tr>          
    </tbody>
</table>

<br>
<br>

<table class="pgt">
    <caption>Key Methods in List, Set, and Map</caption>
    <thead>
        <tr>
            <th>Key Interface Methods</th>
            <th>List</th>
            <th>Set</th>
            <th>Map</th>
            <th>Descriptions</th>            
        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
boolean add(element)
boolean add(index, element)</pre>
            </td>
            <td data-label="List">
                <pre>
X
X</pre> </td>
            <td data-label="Set">
                <pre>
X
""                </pre> </td>
            <td data-label="Map">
                <pre>
                </pre> </td>
            <td data-label="Descriptions">Add an element. For Lists, optionally
                add the element at an index point.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
boolean contains(object)
boolean containsKey(object key)
boolean containsValue(object value)</pre>
            </td>
            <td data-label="List">
                <pre>
X
''
''</pre> </td>
            <td data-label="Set">
                <pre>
X
''
''</pre> </td>
            <td data-label="Map">
                <pre>
''
X
X</pre>  </td>
            <td data-label="Descriptions">Search a collection for an object (or,
                optionally for Maps a key), return the
                result as a boolean.
            </td>
        </tr>  
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
object get(index)
object get(key)</pre>
            </td>
            <td data-label="List">
                <pre>
X
''</pre> </td>
            <td data-label="Set">
                <pre>

                </pre> </td>
            <td data-label="Map">
                <pre>
''
X                </pre> </td>
            <td data-label="Descriptions">Get an object from a collection, via an
                index or a key.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
int indexOf(object)</pre>
            </td>
            <td data-label="List">
                <pre>
X</pre> </td>
            <td data-label="Set">
                <pre>
                </pre> </td>
            <td data-label="Map">
                <pre>
                </pre> </td>
            <td data-label="Descriptions">Get the location of an object in a List.
            </td>
        </tr>                
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
Iterator iterator() X X</pre>
            </td>
            <td data-label="List">
                <pre>
X</pre> </td>
            <td data-label="Set">
                <pre>
X                </pre> </td>
            <td data-label="Map">
                <pre>
                </pre> </td>
            <td data-label="Descriptions">Get an Iterator for a List or a Set.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
Set keySet()</pre>
            </td>
            <td data-label="List">
                <pre>
                </pre> </td>
            <td data-label="Set">
                <pre>
                </pre> </td>
            <td data-label="Map">
                <pre>
X                </pre> </td>
            <td data-label="Descriptions">Return a Set containing a Map's keys.
            </td>
        </tr>        
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
put(key, value)</pre>
            </td>
            <td data-label="List">
                <pre>
                </pre> </td>
            <td data-label="Set">
                <pre>
                </pre> </td>
            <td data-label="Map">
                <pre>
X                </pre> </td>
            <td data-label="Descriptions">Add a key/value pair to a Map.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
remove(index)
remove(object)
remove(key)</pre>
            </td>
            <td data-label="List">
                <pre>
X
X
''                </pre> </td>
            <td data-label="Set">
                <pre>
''
X
''                </pre> </td>
            <td data-label="Map">
                <pre>
''
'' 
X                </pre> </td>
            <td data-label="Descriptions">Remove an element via an index, or
                via the element's value, or via a key.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
int size()</pre>
            </td>
            <td data-label="List">
                <pre>
X</pre> </td>
            <td data-label="Set">
                <pre>
X                </pre> </td>
            <td data-label="Map">
                <pre>
X                </pre> </td>
            <td data-label="Descriptions">Return the number of elements in a
                collection.
            </td>
        </tr>
        <tr>
            <td data-label="Key Interface Methods">
                <pre> 
Object[] toArray()
T[] toArray(T[])</pre>
            </td>
            <td data-label="List">
                <pre>
X
''                </pre> </td>
            <td data-label="Set">
                <pre>
X
''                </pre> </td>
            <td data-label="Map">
                <pre>
''
''               </pre> </td>
            <td data-label="Descriptions">Return an array containing the
                elements of the collection.
            </td>
        </tr>               
    </tbody>
</table>


<table class="pgt">
    <caption>modifiers</caption>
    <thead>
        <tr>
            <th>Local Variables</th>
            <th>Variables(non-local)</th>
            <th>Methods</th>
            <th>Class</th>
          
        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="Local Variables">
''
            </td>
 <td data-label="Variables(non-local)">
 <pre>public
protected
private</pre>
            </td>
<td data-label="Methods">
 <pre>public
protected
private</pre>
            </td>
             <td data-label="Class">
                <pre>public
default</pre>
            </td>
        </tr>
        <tr>
            <td data-label="Local Variables">
                final
            </td>
            <td data-label="Variables(non-local)">
<pre>
final
static
transient
volatile
</pre>
            </td>
<td data-label="Methods">
<pre>abstract
final
native
static
strictfp
synchronized
</pre>
            </td>
<td data-label="Class">
<pre>abstract
final
strictfp
</pre>
            </td>

        </tr>
        
</table>



<table class="pgt">
    <caption>access overriding</caption>
    <thead>
        <tr>
            <th>type</th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
             <th>4</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td data-label="type">
superclass
            </td>
 <td data-label="1">
 <pre>public
</pre>
            </td>
<td data-label="2">
 <pre>
protected
</pre>
            </td>
             <td data-label="3">
                <pre>
default</pre>
            </td>
                         <td data-label="4">
                <pre>
private</pre>
            </td>
        </tr>
                <tr>
            <td data-label="type">
subclass
            </td>
 <td data-label="1">
 <pre>public
</pre>
            </td>
<td data-label="2">
 <pre>
protected
public
</pre>
            </td>
             <td data-label="3">
                <pre>
default
protected
public                </pre>
            </td>
                         <td data-label="4">
                <pre>
private
default
protected
public                </pre>
            </td>
        </tr>

        </tr>
        
</table>
