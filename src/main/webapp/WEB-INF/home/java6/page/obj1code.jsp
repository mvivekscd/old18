<h3>1.a Create an interface that has methods and a constant. Create an abstract
    class with an abstract method, a non-abstract method, and an instance variable.
    Create a concrete class that uses both of the above.</h3>

<h3>1.b Create a directory structure, and a couple of classes, in different
    packages, to demonstrate the differences between default and protected
    access rules.</h3>
<h3> 1.c Create a class, a subclass, and a sub-subclass. Create at least two
    constructors in each class and use super() and this() to access (one
    way or another) all of the constructors, based on invoking new() on the
    grandchild class.</h3>
<h3> 1.d Create a class that uses static initialization blocks, instance initialization
    blocks and a constructor, and prove the sequence in which they get called
    when the class is invoked and a new object is created.</h3>
<h3> 1.e Create a 2D array and a 3D array. Copy the 2D array into a part of the
    3D array.</h3>
<h3>1.f Create an enum that has a variable, a constructor, a method, and a
    constant class body. Create a class that exercises each of the enum's members.</h3>
<h3> 1.g Create a class, a subclass, and a sub-subclass:
    n Make overridden versions of methods in those classes.
    n Make an array of instances of the three and loop through them
    polymorphically.
    n Do the same with a collection instead of an array.</h3>
<h3> 1.h Create a class that has a static inner class and a method local inner
    class. Make instances of each inner class. Create code that accesses the
    inner classes' members from the outer class. Create code that accesses the
    outer class?s members from within the inner classes.</h3>
<h3> 1.i Write a program that has variables of all four scopes. Try to access them
    out of their scope.</h3>

<script>

function openclose(value1, filename)
{
//document.getElementByClassName("menu1").style.display = 'none';
   	ddmenuitem1 = document.getElementById(value1);
   	//alert("ddmenuitem.style.display="+ddmenuitem.style.display);
   	if(ddmenuitem1.style.display =='none'){
		ddmenuitem1.style.display = '';
		ddmenuitem1.style.visibility = 'visible';
	}
	else{
	  ddmenuitem1.style.display = 'none';

	}




//var filename = "c://vivek//menu//X1.html";






}
</script>

<a href="javascript:openclose('first', 't.html')">users</a>
<div class="menu1" id="first" style="display:none"> hi</div>

