

<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class WeatherTest {
    
    <mark>static</mark> Weather w;
        
    public static void main(String[] args) {
        System.out.print(<br>w.RAINY.count + " " + w.Sunny.count + " ");
    }
        
}
    
enum Weather {
    RAINY, Sunny, vivek;
    int count = 0;
        
    Weather() {
        System.out.print("c ");
        count++;
    }
}</pre>
<pre class='out'>
run:
c c c 1 1 BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class WeatherTest {
    
    static Weather w;
        
    public static void main(String[] args) {
        
    }
        
}
    
enum Weather {
    RAINY, Sunny, vivek;
    int count = 0;
        
    Weather() {
        System.out.print("c ");
        count++;
    }
}
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class WeatherTest {
    
    static Weather w;
        
    public static void main(String[] args) {
        System.out.print(<mark>w</mark> + " " + w + " ");
    }
        
}
    
enum Weather {
    RAINY, Sunny, vivek;
    int count = 0;
        
    Weather() {
        System.out.print("c ");
        count++;
    }
}
    
</pre>
<pre class='out'>
run:
null null BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>All of an enum's values are initialized at the same time, and <mark>an enum's
    variables are treated as if they were instance variables, not static variables</mark>.</p>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class Fellowship {
    
    public static void main(String[] args) {
        int h1 = <mark>Numinor.Members.DWARVES.getHeight();</mark>
    }
        
}
    
class Numinor {
    
    enum Members {
        HOBBITS(48), ELVES(74), DWARVES(50);
        int height;
            
        Members(int h) {
            height = h;
        }
            
        int getHeight() {
            return height;
        }
            
    }
}
    
</pre>
<pre>
17. Given:
1. interface Syrupable {
2. void getSugary();
3. }
4. abstract class Pancake implements Syrupable { }
5.
6. class BlueBerryPancake <mark>implements</mark> Pancake {
7. public void getSugary() { ; }
8. }
9. class SourdoughBlueBerryPancake extends BlueBerryPancake {
10. void getSugary(int s) { ; }
11. }
Which are true? (Choose all that apply.)
A. Compilation succeeds.
B. Compilation fails due to an error on line 2.
C. Compilation fails due to an error on line 4.
D. Compilation fails due to an error on line 6.
E. Compilation fails due to an error on line 7.
F. Compilation fails due to an error on line 9.
G. Compilation fails due to an error on line 10.
Answer (for Objective 1.1):
D is correct. Classes extend abstract classes, they don't implement them.</pre>

    <pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class Limits {
    
<mark>    private int x = 2;
    protected int y = 3;
    private static int m1 = 4;
    protected static int m2 = 5;</mark>
        
    public static void main(String[] args) {
        int x = 6;
        int y = 7;
        int m1 = 8;
        int m2 = 9;
        <mark>new Limits().new Secret().go();</mark>
    }
        
    class Secret {
        
        void go() {
            System.out.println(x + " " + y + " " + m1 + " " + m2);
        }
    }
}</pre>
<pre class='out'>   
run:
2 3 4 5
BUILD SUCCESSFUL (total time: 0 seconds)    
</pre>
    <pre>   
 package com.mvivekweb.ocpjp6.obj1.sect1.exam;
     
public class MyClass {
    
    public static void howdy() {
        System.out.print("howdy ");
    }
    public static final int myConstant = 343;
    public static final MyClass mc = new MyClass();
    <mark>public int instVar = 42;</mark>
}
    
    </pre>
    <pre>   
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
import static com.mvivekweb.ocpjp6.obj1.sect1.exam.MyClass.*;
    
public class TestImports {
    
    public static void main(String[] args) {
        System.out.print(myConstant + " ");
        howdy();
        System.out.print(mc.instVar + " ");
        System.out.print(<mark>instVar</mark> + " ");
    }
        
}    
    </pre>
    
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect1/exam/TestImports.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect1\exam\TestImports.java:11: error: cannot find symbol
        System.out.print(instVar + " ");
                         ^
  symbol:   variable instVar
  location: class TestImports
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<p>It's legal to use static imports to access static methods, constants (which are
        static and final), and static object references.
    </p>
    <pre>    
Given:
1. interface Horse { public void nicker(); }
Which will compile? (Choose all that apply.)
A. public class Eyra implements Horse { public void nicker() { } }
B. public class Eyra implements Horse { public void nicker(int x) { } }
C. public class Eyra implements Horse {
public void nicker() { System.out.println("huhuhuhuh..."); }
}
D. public abstract class Eyra implements Horse {
public void nicker(int loud) { }
}
E. public abstract class Eyra implements Horse {
<mark>public void nicker(int loud) ;</mark>
}
Answer (for Objective 1.1):
A, C, and D correctly implement the Horse interface.
    
    </pre>

    
<p>overloaded nicker() method <mark>is not marked abstract</mark>, AND is
        not implemented.   </p> 

<pre>
9. Given two files:
1. package com;
2. public class Extramuros {
3. public static void howdy() { System.out.print("howdy "); }
4. public static final Extramuros ex = new Extramuros();
5. public int instVar = 42;
6. public enum avout {OROLO, JAD};
7. }
1. // insert code here
...
6. public class Theorics {
7. public static void main(String[] args) {
8. Extramuros.howdy();
9. System.out.print(Extramuros.avout.OROLO + " ");
10. howdy();
11. System.out.print(ex.instVar + " ");
12. } }
    
    
Which are the minimum line(s) of code to add at "insert code here" for the files to
compile? (Choose all that apply.)
A. import static com.*;
B. import com.Extramuros;
C. import com.Extramuros.*;
D. import static com.Extramuros;
E. import static com.Extramuros.*;
F. Even with correct imports, the code will not compile due to other errors.
Answer (for Objective 1.1):
B and E are the import statements that correctly allow access to the elements in
Extramuros.
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;
    
public class MyClass {
    
    public static void howdy() {
        System.out.print("howdy ");
    }
    public static final int myConstant = 343;
    public static final MyClass mc = new MyClass();
    public int instVar = 42;
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.exam;

public class TestImports2 {

    public static void main(String[] args) {
        MyClass.howdy();
        System.out.print(MyClass.myConstant + " ");
        <mark>System.out.print(myConstant + " ");
        howdy();
        System.out.print(mc.instVar + " ");
        System.out.print(instVar + " ");</mark>
    }

}

</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect1/exam/TestImports2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect1\exam\TestImports2.java:8: error: cannot find symbol
        System.out.print(myConstant + " ");
                         ^
  symbol:   variable myConstant
  location: class TestImports2
com\mvivekweb\ocpjp6\obj1\sect1\exam\TestImports2.java:9: error: cannot find symbol
        howdy();
        ^
  symbol:   method howdy()
  location: class TestImports2
com\mvivekweb\ocpjp6\obj1\sect1\exam\TestImports2.java:10: error: cannot find symbol
        System.out.print(mc.instVar + " ");
                         ^
  symbol:   variable mc
  location: class TestImports2
com\mvivekweb\ocpjp6\obj1\sect1\exam\TestImports2.java:11: error: cannot find symbol
        System.out.print(instVar + " ");
                         ^
  symbol:   variable instVar
  location: class TestImports2
4 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>
