<h3>Using Wrappers (Objective 3.1)</h3>
        <p> The wrapper classes correlate to the primitive types.</p>
        <p> Wrappers have two main functions:
             To wrap primitives so that they can be handled like objects
             To provide utility methods for primitives (usually conversions)</p>
        <p> The three most important method families are
             xxxValue() Takes no arguments, returns a primitive
             parseXxx() Takes a String, returns a primitive, throws NFE
             valueOf() Takes a String, returns a wrapped object, throws NFE</p>

        <p> Wrapper constructors can take a String or a primitive, except for Character,
            which can only take a char.</p>
        <p> Radix refers to bases (typically) other than 10; octal is radix = 8, hex = 16.</p>
<h3>Boxing (Objective 3.1)</h3>
<p> As of Java 5, boxing allows you to convert primitives to wrappers or to
    convert wrappers to primitives automatically.</p>
<p> Using == with wrappers created through boxing is tricky; those with the same
    small values (typically lower than 127), will be ==, larger values will not be ==.</p>

<h3>Using String, StringBuffer, and StringBuilder (Objective 3.1)</h3>
<p> String objects are immutable, and String reference variables are not.</p>
<p> If you create a new String without assigning it, it will be lost to your program.</p>
<p> If you redirect a String reference to a new String, the old String can be lost.</p>
<p> String methods use zero-based indexes, except for the second argument of
    substring().</p>
<p> The String class is final?its methods can't be overridden.</p>
<p> When the JVM finds a String literal, it is added to the String literal pool.</p>
<p> Strings have a method: length(); arrays have an attribute named length.</p>
<p> The StringBuffer's API is the same as the new StringBuilder's API, except
    that StringBuilder's methods are not synchronized for thread safety.</p>
<p> StringBuilder methods should run faster than StringBuffer methods.</p>
<p> All of the following bullets apply to both StringBuffer and StringBuilder:</p>
<p> They are mutable?they can change without creating a new object.</p>
<p> StringBuffer methods act on the invoking object, and objects can change
    without an explicit assignment in the statement.</p>
<p> StringBuffer equals() is not overridden; it doesn't compare values.</p>
<p> Remember that chained methods are evaluated from left to right.</p>
<p> String methods to remember: charAt(), concat(), equalsIgnoreCase(),
    length(), replace(), substring(), toLowerCase(), toString(),
    toUpperCase(), and trim().</p>
<p> StringBuffer methods to remember: append(), delete(), insert(),
    reverse(), and toString().</p>

<h3>File I/O (Objective 3.2)</h3>
<p> The classes you need to understand in java.io are File, FileReader,
    BufferedReader, FileWriter, BufferedWriter, PrintWriter, and Console.</p>
<p> A new File object doesn't mean there's a new file on your hard drive.</p>
<p> File objects can represent either a file or a directory.</p>

<p> The File class lets you manage (add, rename, and delete) files and directories.</p>
<p> The methods createNewFile() and mkdir() add entries to your file system.</p>
<p> FileWriter and FileReader are low-level I/O classes. You can use them to
    write and read files, but they should usually be wrapped.</p>
<p> Classes in java.io are designed to be "chained" or "wrapped." (This is a
    common use of the decorator design pattern.)</p>
<p> It's very common to "wrap" a BufferedReader around a FileReader
    or a BufferedWriter around a FileWriter, to get access to higher-level
    (more convenient) methods.</p>
<p> PrintWriters can be used to wrap other Writers, but as of Java 5 they can be
    built directly from Files or Strings.</p>
<p> Java 5 PrintWriters have new append(), format(), and printf() methods.</p>
<p> Console objects can read non-echoed input and are instantiated using
    System.console().</p>

<h3>Serialization (Objective 3.3)</h3>
<p> The classes you need to understand are all in the java.io package; they
    include: ObjectOutputStream and ObjectInputStream primarily, and
    FileOutputStream and FileInputStream because you will use them to create
    the low-level streams that the ObjectXxxStream classes will use.</p>
<p> A class must implement Serializable before its objects can be serialized.</p>
<p> The ObjectOutputStream.writeObject() method serializes objects, and
    the ObjectInputStream.readObject() method deserializes objects.</p>
<p> If you mark an instance variable transient, it will not be serialized even
    thought the rest of the object's state will be.</p>
<p> You can supplement a class's automatic serialization process by implementing
    the writeObject() and readObject() methods. If you do this, embedding
    calls to defaultWriteObject() and defaultReadObject(), respectively,
    will handle the part of serialization that happens normally.</p>
<p> If a superclass implements Serializable, then its subclasses do automatically.</p>
<p> If a superclass doesn't implement Serializable, then when a subclass object
    is deserialized, the superclass constructor will be invoked, along with its
    superconstructor(s).</p>
<p> DataInputStream and DataOutputStream aren't actually on the exam, in
    spite of what the Sun objectives say.</p>

<h3>Dates, Numbers, and Currency (Objective 3.4)</h3>
<p> The classes you need to understand are java.util.Date, java.util.Calendar,
    java.text.DateFormat, java.text.NumberFormat, and java.util.Locale.</p>
<p> Most of the Date class's methods have been deprecated.</p>
<p> A Date is stored as a long, the number of milliseconds since January 1, 1970.</p>
<p> Date objects are go-betweens the Calendar and Locale classes.</p>
<p> The Calendar provides a powerful set of methods to manipulate dates,
    performing tasks such as getting days of the week, or adding some number of
    months or years (or other increments) to a date.</p>
<p> Create Calendar instances using static factory methods (getInstance()).</p>
<p> The Calendar methods you should understand are add(), which allows you
    to add or subtract various pieces (minutes, days, years, and so on) of dates,
    and roll(), which works like add() but doesn't increment a date's bigger
    pieces. (For example: adding 10 months to an October date changes the
    month to August, but doesn't increment the Calendar's year value.)</p>
<p> DateFormat instances are created using static factory methods
    (getInstance() and getDateInstance()).</p>
<p> There are several format "styles" available in the DateFormat class.</p>
<p> DateFormat styles can be applied against various Locales to create a wide
    array of outputs for any given date.</p>
<p> The DateFormat.format() method is used to create Strings containing
    properly formatted dates.</p>
<p> The Locale class is used in conjunction with DateFormat and NumberFormat.</p>
<p> Both DateFormat and NumberFormat objects can be constructed with a
    specific, immutable Locale.</p>
<p> For the exam you should understand creating Locales using language, or a
    combination of language and country.</p>
<h3>Parsing, Tokenizing, and Formatting (Objective 3.5)</h3>
<p> regex is short for regular expressions, which are the patterns used to search for
    data within large data sources.</p>
<p> regex is a sub-language that exists in Java and other languages (such as Perl).</p>
<p> regex lets you to create search patterns using literal characters or
    metacharacters. Metacharacters allow you to search for slightly more abstract
    data like "digits" or "whitespace".</p>

<p> Study the \d, \s, \w, and . metacharacters</p>
<p> regex provides for quantifiers which allow you to specify concepts like: "look
    for one or more digits in a row."</p>
<p> Study the ?, *, and + greedy quantifiers.</p>
<p> Remember that metacharacters and Strings don't mix well unless you
    remember to "escape" them properly. For instance String s = "\\d";</p>
<p> The Pattern and Matcher classes have Java's most powerful regex capabilities.</p>
<p> You should understand the Pattern compile() method and the Matcher
    matches(), pattern(), find(), start(), and group() methods.</p>
<p> You WON'T need to understand Matcher's replacement-oriented methods.</p>
<p> You can use java.util.Scanner to do simple regex searches, but it is primarily
    intended for tokenizing.</p>
<p> Tokenizing is the process of splitting delimited data into small pieces.</p>
<p> In tokenizing, the data you want is called tokens, and the strings that separate
    the tokens are called delimiters.</p>
<p> Tokenizing can be done with the Scanner class, or with String.split().</p>
<p> Delimiters are single characters like commas, or complex regex expressions.</p>
<p> The Scanner class allows you to tokenize data from within a loop, which
    allows you to stop whenever you want to.</p>
<p> The Scanner class allows you to tokenize Strings or streams or files.</p>
<p> The String.split() method tokenizes the entire source data all at once, so
    large amounts of data can be quite slow to process.</p>
<p> New to Java 5 are two methods used to format data for output. These
    methods are format() and printf(). These methods are found in the
    PrintStream class, an instance of which is the out in System.out.</p>
<p> The format() and printf() methods have identical functionality.</p>
<p> Formatting data with printf() (or format()) is accomplished using
    formatting strings that are associated with primitive or string arguments.</p>
<p> The format() method allows you to mix literals in with your format strings.</p>
<p> The format string values you should know are</p>
<p> Flags: -, +, 0, "," , and (</p>
<p> Conversions: b, c, d, f, and s</p>
<p> If your conversion character doesn't match your argument type, an exception
    will be thrown.</p>