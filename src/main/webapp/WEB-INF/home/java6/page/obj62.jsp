<h3> Distinguish between correct and incorrect overrides of corresponding hashCode and
            equals methods, and explain the difference between == and the equals method.</h3>
        
        <h3>The toString() Method </h3>
        
        <p><mark>Override toString()</mark> when you want a mere mortal
            to be able to <mark>read something meaningful about the objects</mark> of your class.</p>
        
        <pre>       
            
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
    
public class HardToRead {
    
    public static void main(String[] args) {
        HardToRead h = new HardToRead();
        System.out.println(h);
    }
}
        </pre>
        
        <pre class='out'>       
run:
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.HardToRead@41454145
BUILD SUCCESSFUL (total time: 2 seconds)
        </pre>    
        
        <pre>      
            
package com.mvivekweb.ocpjp6.obj6.sect2.tostring;
    
    
public class BobTest {
    
    public static void main(String[] args) {
        Bob f = new Bob("GoBobGo", 19);
        System.out.println(<mark>f</mark>);
    }
}
class Bob {
    
    int shoeSize;
    String nickName;
        
    Bob(String nickName, int shoeSize) {
        this.shoeSize = shoeSize;
        this.nickName = nickName;
    }
        
    <mark>public String toString() {</mark>
        return ("I am a Bob, but you can call me " + nickName
                + ". My shoe size is " + shoeSize);
    }
}        
        </pre>
  <pre class='out'>      
  run:
I am a Bob, but you can call me GoBobGo. My shoe size is 19
BUILD SUCCESSFUL (total time: 1 second)      
    </pre>  
        
        
        <p>        
            most common implementations of toString() simply spit out
            the object's state (in other words, the <mark>current values of the important instance</mark>
            variables)   </p>
        
 
        <h3>Overriding equals()   </h3>
        
        <p>comparing two object references using the
            <mark> == operator evaluates to true</mark> only <mark>when both references refer to the same object</mark>  </p>      
        
        <p>== simply <mark>looks at the bits</mark> in the variable, and they're either identical or
            they're not  </p>
        
        <p> know <mark>if two references are identical, use ==</mark>.   </p>
        
        
        <p> if the <mark>objects</mark> themselves (not the references) <mark>are equal, use the
            equals() method</mark> </p>      
        
            <p>For each class you write, you <mark>must decide if it makes sense to consider two different instances</mark> equal</p>
            
<h3>What It Means If You Don't Override equals()</h3>            
            
<p>if you don't override a class's equals()
    method, you <mark>won't be able to use those objects as a key</mark> in a hashtable and you
    probably <mark>won't get accurate Sets</mark>, such that there are no conceptual duplicates.</p>

<p>The equals() method in class Object uses only the == operator for comparisons,
    so unless you override equals(), two objects are considered equal only if the two
    references refer to the same object.</p>

<p>if you want <mark>objects</mark> of your class to <mark>be used as keys for a
    hashtable</mark>, then you must override equals() so that two
    different instances can be considered the same.</p>

<p>if you override equals you <mark>can use one
    instanc</mark>e when you add it to a Collection, and essentially<mark> re-create an identical
    instance</mark> when you want to do a search based on that object as the key.</p>

<h3>Implementing an equals() Method</h3>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
    
public class EqualsTest {
    
    public static void main(String[] args) {
        Moof one = new Moof(8);
        Moof two = new Moof(8);
        if (one.<mark>equals</mark>(two)) {
            System.out.println("one and two are equal");
        }
    }
}
    
class Moof {
    
    private int moofValue;
        
    Moof(int val) {
        moofValue = val;
    }
        
    public int getMoofValue() {
        return moofValue;
    }
        
    <mark>public boolean</mark> equals(<mark>Object o</mark>) {
        if ((<mark>o instanceof Moof</mark>) && ((<mark>(Moof) o</mark>).getMoofValue()
                == <mark>this.moofValue</mark>)) {
//            if (((Moof)o).getMoofValue() == this.moofValue){
            return true;
        } else {
            return false;
        }
    }
}
    
</pre>
<pre class='out'>
run:
one and two are equal
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>Logically, we have to do <mark>two things</mark> in order to
    make a valid equality comparison.</p>


<p>First, be sure that the <mark>object being tested is of the correct type</mark></p>

<p>if the <mark>object doesn't pass the instanceof</mark> test, then
    you'll get a <mark>runtime ClassCastException</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
public class EqualsTest {
    
    public static void main(String[] args) {
        Moof one = new Moof(8);
        Moof1 two = new Moof1(8);
        if (one.equals(two)) {
            System.out.println("one and two are equal");
        }
    }
}
    
class Moof {
    
    private int moofValue;
        
    Moof(int val) {
        moofValue = val;
    }
        
    public int getMoofValue() {
        return moofValue;
    }
        
    public boolean equals(Object o) {
       // if ((o instanceof Moof) && (((Moof) o).getMoofValue()
        //        == this.moofValue)) {
            <mark>if (((Moof)o).getMoofValue() == this.moofValue){</mark>
            return true;
        } else {
            return false;
        }
    }
}
    
class Moof1 {
    
    Moof1(int val) {
    }
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" j<mark>ava.lang.ClassCastException</mark>: com.mvivekweb.ocpjp6.obj6.sect2.hashcode.Moof1 incompatible with com.mvivekweb.ocpjp6.obj6.sect2.hashcode.Moof
	at com.mvivekweb.ocpjp6.obj6.sect2.hashcode.Moof.equals(EqualsTest.java:29)
	at com.mvivekweb.ocpjp6.obj6.sect2.hashcode.EqualsTest.main(EqualsTest.java:8)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>Second, <mark>compare the attributes</mark> we care about</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
public class EqualsTest {
    
    public static void main(String[] args) {
        Moof one = new Moof(8);
        Moof1 two = new Moof1(8);
        if (one.equals(two)) {
            System.out.println("one and two are equal");
        }
    }
}
    
class Moof {
    
    private int moofValue;
        
    Moof(int val) {
        moofValue = val;
    }
        
    public int getMoofValue() {
        return moofValue;
    }
        
    public boolean equals(Object o) {
        <mark>if ((o instanceof Moof) && (((Moof) o).getMoofValue()
                == this.moofValue)) {</mark>
                    
            return true;
        } else {
            return false;
        }
    }
}
    
class Moof1 {
    
    Moof1(int val) {
    }
}
    
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>Remember that the equals(), hashCode(), and toString() methods are
    all <mark>public</mark>.</p>
<pre>
equals override syntax
    
<mark>public boolean</mark> equals(<mark>Object</mark> o){
    
</pre>    

<h3>The equals() Contract</h3>

<p>It is <mark>reflexive</mark>. For any reference value x, <mark>x.equals(x) should return true</mark>.</p>

<p>It is <mark>symmetric</mark>. For any reference values x and y, <mark>x.equals(y) should
    return true if and only if y.equals(x) returns true</mark>.</p>

<p>It is <mark>transitive</mark>. For any reference values <mark>x, y, and z</mark>, if x.equals(y) returns
        true and y.equals(z) returns true, then <mark>x.equals(z) must return true</mark>.</p>


<p>It is <mark>consistent</mark>. For any reference values x and y, multiple invocations of
    x.equals(y) consistently return true or consistently return false, provided
    no information used in equals comparisons on the object is modified.</p>

<p>For <mark>any non-null</mark> reference value x, <mark>x.equals(null) should return false</mark>.</p>



<p>equals() and hashCode() are bound together by a joint contract that
    specifies <mark>if two objects are considered equal</mark> using the equals() method, then they
    <mark>must have identical hashcode</mark> values.</p>

<p>if you <mark>override equals(), override hashCode()</mark> as well</p>


<h3>Overriding hashCode()</h3>


<p>Hashcodes are typically used to <mark>increase the performance of large collections</mark> of
    data.</p>

<p>The <mark>hashcode value of an object</mark> is used by some collection classes</p>

<p>Collections such as <mark>HashMap</mark> and
    <mark>HashSet</mark> use the hashcode value of an object to <mark>determine how the object should
    be stored in the collection</mark></p>


    <p>the hashcode is used again to <mark>help locate the object
        in the collection</mark></p>
    
<p>You must also be able to <mark>recognize an appropriate or
            correct implementation of hashCode()</mark>. </p>   
    
<p>It's perfectly legal to <mark>have a terribly inefficient hashcode method</mark>
            in your class, as long as it doesn't violate the contract specified in the Object class
            documentation</p>

<h3>Understanding Hashcodes</h3>

<p>Imagine a set of buckets lined up on the floor.</p>


<pre>
Hashcode Algorithm          Hashcode
    
<mark>A(1)</mark> + L(12) + E(5) + X(24) = 42
<mark>B(2)</mark> + O(15) + B(2)         = 19
D(4) + I(9) + R(18) + K(11) = 42
F(6) + R(18) + E(5) + D(4)  = 33
    
</pre>


<p>The hashcode tells you
    <mark>in which bucket you should look to find the name</mark>.</p>

<p>The hashcode tells <mark>you only which bucket to go into</mark>,
    but <mark>not how to locate the name once we're in that bucket</mark>.</p>

<p>Hashing retrieval is a two-step process.</p>

1. Find the right bucket (using hashCode())
<br>2. <mark>Search the bucket</mark> for the right element (using equals() ).


<p>the <mark>default
    hashcode</mark> method in class Object virtually always <mark>comes up with a unique number
    for each object</mark>, even if the equals() method is overridden in such a way that two
    or more objects are considered equal.</p>

<p><mark>If two objects are equal</mark>, their
    <mark>hashcodes must be equal</mark> as well.</p>

<h3>Implementing hashCode()</h3>

<p>hashCode()
    implementation <mark>should use the same instance variables as equals</mark></p>

<pre>
    
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
    
class HasHash {
    
    public int x;
        
    HasHash(int xVal) {
        x = xVal;
    }
        
    public boolean equals(Object o) {
        HasHash h = (HasHash) o; // Don't try at home without
// instanceof test
        if (h.x == this.x) {
            return true;
        } else {
            return false;
        }
    }
        
    <mark>public int hashCode() {</mark>
        return (x * 17);
    }
}
    
</pre>

<p>This equals() method says <mark>two objects are equal if they have the same x</mark> value,
    so objects with the same x value will have to <mark>return identical hashcodes</mark>.</p>

<p>A hashCode() <mark>that returns the same value</mark> for all instances whether
    they're equal or not is <mark>still a legal</mark>-even appropriate-hashCode() method</p>
<pre>
public int hashCode() { return 1492; }
    
</pre>

<p>This <mark>does not violate</mark> the contract</p>

<p>In any case, while the <mark>goal
    is to get a wide and random distribution of objects</mark> across buckets, <mark>the contract</mark>
    (and whether or not an object can be found)<mark> requires only that two equal objects
    have equal hashcodes</mark>.</p>

<p>The exam does not expect you to rate the efficiency of a
        hashCode() method, but you must be able to <mark>recognize which ones will and will not
        work</mark></p>
    
    
<p>So in order for an object to be located, <mark>the search object</mark> and the <mark>object in
            the collection</mark> must have both <mark>identical hashcode values</mark> and <mark>return true for the
            equals() method</mark>.   </p>
        
  

<p>So there's just <mark>no way out</mark> <mark>of</mark> overriding both methods to be
    absolutely certain that your objects can be used in Collections that use hashing</p>


<h3>The hashCode() Contract</h3>


<p>Whenever it is invoked on the same object more than once during an execution
    of a Java application, the <mark>hashCode() method must consistently return
    the same integer</mark>, <mark>provided no information used in equals()</mark> comparisons
    on the object <mark>is modified</mark>.</p>
        

<p>If two objects are equal according to the equals(Object) method, then
    calling the hashCode() method on each of the two objects must produce the
    same integer result.</p>


<p>It is NOT required that if two objects are unequal according to the
    equals(java.lang.Object) method, then calling the hashCode() method
    on each of the two objects must produce distinct integer results.</p>


<p>the programmer should be aware that producing distinct integer results for
    unequal objects may improve the performance of hashtables.</p>


<pre>
if <mark>x.equals(y) == true</mark>
then <mark>must be</mark> x.hashCode() == y.hashCode()
    
</pre>

<pre>
if <mark>x.hashCode() == y.hashCode()</mark>
    
then <mark>can be</mark> x.equals(y) == true
</pre>

<pre>
if <mark>x.equals(y) == false</mark>
    
no hashcode requirements
</pre>
<pre>
if
<mark>x.hashCode() !=
y.hashCode()</mark>
    
then must be
    
<mark>x.equals(y) == false</mark>
</pre>

<p><mark>transient variables</mark> can really <mark>mess with your equals() and
    hashCode()</mark> implementations.</p>

<p>Keep <mark>variables non-transient</mark> or, if they must
    be marked transient, <mark>don't use them to determine hashcodes or equality</mark>.
</p>

<p>
    <mark>If you override the equals() method</mark>, you change the way two objects are equated and <mark>Object's implementation of hashCode() is no longer valid</mark>.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
    
public class MapEQ {
    
    public static void main(String[] args) {
        Map<ToDos, String> m = new HashMap<ToDos, String>();
       <mark> ToDos t1 = new ToDos("Monday");</mark>
        System.out.println("1");
        <mark>ToDos t2 = new ToDos("Monday");</mark>
        System.out.println("2");
            
        <mark>ToDos t3 = new ToDos("Monday");</mark>
        System.out.println("3");
            
        m.put(t1, "doLaundry");
        System.out.println("4");
        m.put(t2, "payBills");
         System.out.println("5");
        m.put(t3, "cleanAttic");
        System.out.println("6");
        System.out.println(m.size());
        // Get a set of the entries
      Set set = m.entrySet();
      // Get an iterator
      Iterator i = set.iterator();
      // Display elements
      while(i.hasNext()) {
         Map.Entry me = (Map.Entry)i.next();
         System.out.print(me.getKey() + ": ");
         System.out.println(me.getValue());
      }
    }
}
    
class ToDos {
    
    String day;
        
    ToDos(String d) {
        day = d;
    }
        
    public boolean equals(Object o) {
        System.out.println("equals called");
        System.out.println("(ToDos) o).day="+((ToDos) o).day);
        System.out.println("this.day="+this.day);
       return ((ToDos) o).day == this.day;
    }
        
    public int hashCode() {
          System.out.println("hashcode called");
              
              
              
        return 9;
    }
}
    
</pre>
<pre class='out'>
run:
1
2
3
hashcode called
4
hashcode called
<mark>equals called</mark>
(ToDos) o).day=Monday
this.day=Monday
5
hashcode called
<mark>equals called</mark>
(ToDos) o).day=Monday
this.day=Monday
6
1
hashcode called
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@9: cleanAttic
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
    
public class MapEQ {
    
    public static void main(String[] args) {
        Map<ToDos, String> m = new HashMap<ToDos, String>();
        <mark>ToDos t1 = new ToDos("Monday");</mark>
        System.out.println("1");
        <mark>ToDos t2 = new ToDos("Monday");</mark>
        System.out.println("2");
            
        ToDos t3 = new ToDos("Tuesday");
        System.out.println("3");
            
       <mark> m.put(t1, "doLaundry");</mark>
        System.out.println("4");
        <mark>m.put(t2, "payBills");</mark>
         System.out.println("5");
       <mark> m.put(t3, "cleanAttic");</mark>
        System.out.println("6");
        System.out.println(m.size());
        // Get a set of the entries
      Set set = m.entrySet();
      // Get an iterator
      Iterator i = set.iterator();
      // Display elements
      while(i.hasNext()) {
         Map.Entry me = (Map.Entry)i.next();
         System.out.print(me.getKey() + ": ");
         System.out.println(me.getValue());
      }
    }
}
    
class ToDos {
    
    String day;
        
    ToDos(String d) {
        day = d;
    }
        
    public boolean equals(Object o) {
        System.out.println("equals called");
        System.out.println("(ToDos) o).day="+((ToDos) o).day);
        System.out.println("this.day="+this.day);
       return ((ToDos) o).day == this.day;
    }
        
    public int hashCode() {
          System.out.println("hashcode called");
              
              
              
        return 9;
    }
}
    
</pre>
<pre class='out'>
run:
1
2
3
hashcode called
4
hashcode called
equals called
(ToDos) o).day=Monday
this.day=Monday
5
hashcode called
equals called
(ToDos) o).day=Monday
this.day=Tuesday
6
2
hashcode called
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@9: cleanAttic
hashcode called
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@9: payBills
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj6.sect2.hashcode;
    
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
    
public class MapEQ {
    
    public static void main(String[] args) {
        Map<ToDos, String> m = new HashMap<ToDos, String>();
<mark>        ToDos t1 = new ToDos("Monday");</mark>
            
        System.out.println("1");
<mark>        ToDos t2 = new ToDos("Monday");</mark>
        System.out.println("2");
        ToDos t3 = new ToDos("Tuesday");
        System.out.println("3");
        m.put(t1, "doLaundry");
        System.out.println("4");
        m.put(t2, "payBills");
         System.out.println("5");
        m.put(t3, "cleanAttic");
        System.out.println("6");
        System.out.println(m.size());
        // Get a set of the entries
      Set set = m.entrySet();
      // Get an iterator
      Iterator i = set.iterator();
      // Display elements
      while(i.hasNext()) {
         Map.Entry me = (Map.Entry)i.next();
         System.out.print(me.getKey() + ": ");
         System.out.println(me.getValue());
      }
    }
}
    
class ToDos {
    
    String day;
        
    ToDos(String d) {
        day = d;
    }
        
    public boolean equals(Object o) {
        System.out.println("equals called");
        System.out.println("(ToDos) o).day="+((ToDos) o).day);
        System.out.println("this.day="+this.day);
       return ((ToDos) o).day == this.day;
    }
        
<mark>//    public int hashCode() {
//          System.out.println("hashcode called");
//       
//
//      
//        return 9;
//    }</mark>
}
    
</pre>
<pre class='out'>
run:
1
2
3
4
5
6
<mark>3</mark>
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@45324532: payBills
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@44e244e2: doLaundry
com.mvivekweb.ocpjp6.obj6.sect2.hashcode.ToDos@45464546: cleanAttic

    </pre> 

<p>the <mark>String class and the wrapper</mark> classes<mark> have overridden
        the equals() method and hashcode as well</mark> (inherited from class Object), so that you could compare
    two different objects (of the same type) to see if their contents are meaningfully
    equivalent.</p>




