 <h3> Develop code that implements an if or switch statement; and identify legal argument
            types for these statements.</h3>
        
        
        <p> commonly referred to as <mark>decision</mark> statements</p>
        
        <h3>if-else Branching</h3>
        
        <p>basic format of an if statement</p>
        <pre>  
        if (booleanExpression) {
System.out.println("Inside if statement");
}
        </pre>
        
        <p> else block is optional</p>
        
        <pre>       
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
public class ifex4 {
    public static void main(String[] args) {
        int y = 0, z = 0, a = 0;
        int x = 3;
<mark>    if (x > 3)
            y = 2;
            z += 8;
            a = y + x;</mark>
        System.out.println("y=" + y);
        System.out.println("a=" + a);
    }
}

        </pre>
        <pre class='out'>       
 run:
y=0
a=3
BUILD SUCCESSFUL (total time: 1 second)
   
        </pre> 
        
        <p>You can have zero or one else for a given if, and it must come after any
            else ifs.</p>
        
        <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class ifex2 {
//without brackets
 public static void main(String[] args) {
        int x = 1;
        <mark>if ( x == 3 )
             ;</mark>
        <mark>else if (x < 4) 
            System.out.println("<4"); </mark>
        else if (x < 2) {System.out.println("<2"); }
        else { System.out.println("else"); }
    }
}
        </pre>
 <pre class='out'>       
run:
<4
BUILD SUCCESSFUL (total time: 1 second)
    </pre> 
        
        
        <p><mark>else</mark> clause belongs to the <mark>innermost</mark> if statement to which it might
            possibly belong (in other words, the closest preceding if that doesn't have an else).        
        </p>      
        <pre>     
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class innerif {
    
    
    public static void main(String[] args) {
        int x=0;
        if (true)
        if (x > 0.61)
        System.out.println("Try again.");
        // Which if does this belong to?
        <mark>else</mark> System.out.println("Java master!");
    }
        
}        
        </pre>
 <pre class='out'>       
run:
Java master!
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>  
        
        
        <h3> Legal Expressions for if Statements  </h3>     
        <pre>      
 package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;

public class ifex3 {

    public static void main(String[] args) {
        int y = 5;
        int x = 2;
        if (<mark>(x > 3</mark>) && <mark>(y < 2) | true</mark>) {
            System.out.println("true");
        }
        
        if (((x > 3) && (y < 2)) | true) {
            System.out.println("true1");
        }
    }
}
        </pre>
 <pre class='out'>       
run:
true1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>  
        
        <p> Because of the<mark> short-circuit
            &&, the expression</mark> is evaluated as though there were parentheses around (y < 2) |
            doStuff() </p>      

        <pre>       
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class ifex3 {
    
    public static void main(String[] args) {
        int y = 5;
        int x = 2;
        if ((x > 3) <mark>&</mark> (y < 2) | true) {
            System.out.println("true");
        }
            
        if (((x > 3) & (y < 2)) | true) {
            System.out.println("true1");
        }
    }
}
    
        </pre>
<pre class='out'>
run:
true
true1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>  
        
        <pre>      
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class ifboolean {
    public static void main(String[] args) {
        boolean boo = false;
        if (<mark>boo</mark> = true) { System.out.println("hi");}
    }
        
}
        </pre>   
<pre class='out'>        
run:
hi
BUILD SUCCESSFUL (total time: 1 second)        
    </pre> 
        
        <p>The code compiles and runs fine and the if test succeeds because
            <mark>boo is SET to true (rather than TESTED for true)</mark> in the if argument!      </p>  

        <h3>switch Statements   </h3> 
        
        
        <p>simulate the use of <mark>multiple if</mark> statements is with the switch statement.    </p>    
        <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch1 {
    public static void main(String[] args) {
        int x = 3;
        switch (<mark>x</mark>) <mark>{</mark>
            <mark>case 1:</mark>
                System.out.println("x is equal to 1");
                <mark>break;</mark>
            case 2:
                System.out.println("x is equal to 2");
                break;
            case 3:
                System.out.println("x is equal to 3");
                break;
            <mark>default:</mark>
                System.out.println("Still no idea what x is");
        <mark>}</mark>
    }
}        
        </pre>
<pre class='out'>        
run:
x is equal to 3
BUILD SUCCESSFUL (total time: 1 second)        
    </pre>
        
        
        <p><mark>break</mark> statements are <mark>optional</mark> .their inclusion or
exclusion <mark>causes huge changes</mark> in how a switch statement will execute. </p>   
       
        <p> The general form of the switch statement is: </p>      
        <pre>       
switch (<mark>expression</mark>) {
case <mark>constant1</mark>: code block
case <mark>constant2</mark>: code block
<mark>default</mark>: code block
}        
        </pre>   
        
        <p>A switch's expression <mark>must evaluate</mark> to a <mark>char</mark>, <mark>byte</mark>, <mark>short</mark>, <mark>int</mark>, or, as of Java
            6, an <mark>enum</mark>.   </p>     
        
        <pre>
        package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
            
            
public class switchchar {
    
    
    public static void main(String[] args) {
        char ch='a';
            
        switch (ch){
            case <mark>'a'</mark>:
                System.out.println("a is correct");
        }
    }
        
}
        </pre>
        
 <pre class='out'>run:
a is correct
BUILD SUCCESSFUL (total time: 1 second)       
    </pre>
        
        <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switchenum {
<mark>enum name{viv,manj};</mark>
    
    public static void main(String[] args) {
        <mark>name namev = name.viv;</mark>
        switch(namev){
            case <mark>viv</mark>:
                System.out.println("hi vivek");
        }
    }
        
}
    
        </pre>     
        
<pre class='out'>        
 run:
hi vivek
BUILD SUCCESSFUL (total time: 1 second)
    </pre>   
        
        <p>if <mark>you're not</mark> using an enum, only variables and values
            that can be automatically promoted (in other words, implicitly cast) <mark>to an int are
            acceptable</mark></p>       
        
            <p>won't be able to compile <mark>long, float,boolean, string and double</mark>. </p>    
            
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switchboolean {
    
    public static void main(String[] args) {
        boolean ch=true;
            
        switch (ch){
            case 1:
                System.out.println("a is correct");
        }
    }
        
}
            </pre>      
   <pre class='out'>         
 run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types
  <mark>required: int
  found:    boolean</mark>
	at com.mvivekweb.ocpjp6.obj2.sect1.ifswitch.switchboolean.main(switchboolean.java:12)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)           
    </pre>   
            
            <p>the <mark>case</mark> constant <mark>must be</mark> a
                <mark>compile time constant</mark>  </p>      
            
            
            <p>Since the case argument <mark>has to be resolved at compile
                time</mark>, that means you can use only a <mark>constant</mark> or <mark>final</mark> variable that is assigned a
                literal value.  </p>      

            <p>It is not enough to be final, it must be a <mark>compile time constant</mark>        </p>    
            <pre>            
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch2 {
    
    
    public static void main(String[] args) {
        final int a = 1;
        int b;
        System.out.println(<mark>b</mark>);
        b = 2;
        System.out.println(b);
        int x = 0;
        switch (x) {
            case a: // ok
            case b: // compiler error
        }
    } 
            </pre>      
  <pre class='out'>          
 run:
Exception in thread "main" java.lang.RuntimeException: <mark>Uncompilable source code - variable b might not have been initialized</mark>
	at com.mvivekweb.ocpjp6.obj2.sect1.ifswitch.switch2.main(switch2.java:20)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)           
    </pre>     
            
            <p>switch can only <mark>check for equality</mark>   </p>         
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch3 {
    
    public static void main(String[] args) {
        byte g = 2;
        switch (g) {
            case 23:
            <mark>case 128:</mark>
        }
    }
}
            </pre> 
<pre class='out'>            
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - <mark>possible loss of precision</mark>
  required: byte
  found:    int
	at com.mvivekweb.ocpjp6.obj2.sect1.ifswitch.switch3.main(switch3.java:7)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>        
            
            <p>byte is
                implicitly cast to an int-the second case argument (128) is too large for a byte</p>            
     
            <p>       byte <mark>-128 to 127</mark>    
                short <mark>-32,768 to 32,767</mark> </p>   

            <p> <mark> illegal</mark> to have <mark>more than one case</mark> label using the same value. </p>         

            <pre>            
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch4 {
    
    public static void main(String[] args) {
        int temp = 90;
        switch (temp) {
           <mark> case 80:
                System.out.println("80");
            case 80:
                System.out.println("80"); // won't compile!</mark>
            case 90:
                System.out.println("90");
            default:
                System.out.println("default");
        }
    }
}
            </pre>
 <pre class='out'>           
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - duplicate case label
	at com.mvivekweb.ocpjp6.obj2.sect1.ifswitch.switch4.main(switch4.java:7)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>       
 
            
            <p>It is legal to <mark>leverage the power of boxing</mark> in a switch expression</p>
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch5 {
    
    public static void main(String[] args) {
        switch (<mark>new Integer(4)</mark>) {
            case 4:
                System.out.println("boxing is OK");
        }
    }
}
            </pre>
<pre class='out'>            
run:
boxing is OK
BUILD SUCCESSFUL (total time: 1 second)
    </pre>        

            <h3>Break and Fall-Through in switch Blocks</h3>            

            <p>flow of execution through a switch statement </p>           
      
            <p>case constants are evaluated from the <mark>top down</mark>, and <mark>the first case constant</mark>
                that matches the switch's expression is the <mark>execution entry point</mark>.   </p>         
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch7 {
    
    public static void main(String[] args) {
        Color c = Color.green;
        switch (c) {
            case red:
                System.out.print("red ");
            <mark>case green:
                System.out.print("green ");
            case blue:
                System.out.print("blue ");
            default:
                System.out.println("done");</mark>
        }
    }
}
    
enum Color {
    
    red, green, blue
}            
            </pre>
            
  <pre class='out'>          
 run:
green blue done
BUILD SUCCESSFUL (total time: 1 second)           
    </pre> 
            
            <p> keyword break during the execution
                of a switch statement, execution will immediately <mark>move out of the switch block</mark>
                to the next statement after the switch. </p>          
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch8 {
    
    public static void main(String[] args) {
        int x = 1;
        switch (x) {
            case 1:
                System.out.println("x is one");
            case 2:
                System.out.println("x is two");
            case 3:
                System.out.println("x is three");
        }
        <mark>System.out.println("out of the switch");</mark>
    }
}
            </pre>   
 <pre class='out'>           
run:
x is one
x is two
x is three
out of the switch
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>  
            <pre>           
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch9 {
    public static void main(String[] args) {
       <mark>int x = 1;</mark>
        switch (x) {
<mark>        case 2:
        case 4:
        case 6:</mark>
        case 8:
        case 10: {
            System.out.println("x is an even number"); break;
        }
        }
    }
}            
            </pre>   
<pre class='out'>            
run:
BUILD SUCCESSFUL (total time: 1 second)
    </pre>         
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch9 {
    public static void main(String[] args) {
       <mark>int x = 4;</mark>
        switch (x) {
        case 2:
        <mark>case 4:
        case 6:
        case 8:
        case 10: {
            System.out.println("x is an even number"); break;
        }</mark>
        }
    }
}            
            </pre>       
<pre class='out'>
run:
x is an even number
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>  
            
            <h3>The Default Case</h3>
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch10 {
    
    public static void main(String[] args) {
        <mark>int x = 7;</mark>
        switch (x) {
            case 2:
            case 4:
            case 6:
            case 8:
            case 10: {
                System.out.println("x is an even number");
                break;
            }
            <mark>default:
                System.out.println("x is an odd number");</mark>
        }
    }
}
            </pre>
 <pre class='out'>           
 run:
x is an odd number
BUILD SUCCESSFUL (total time: 1 second)           
    </pre>  
            <pre>            
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switch11 {
    
    public static void main(String[] args) {
        <mark>int x = 5;</mark>
        switch (x) {
            <mark>default:
                System.out.println("default");
            case 2:
                System.out.println("2");
                    
            case 3:
                System.out.println("3");
            case 4:
                System.out.println("4");</mark>
        }
    }
}            
            </pre>
            
<pre class='out'>            
run:
default
2
3
4
BUILD SUCCESSFUL (total time: 1 second)            
    </pre>   
            
            <pre>
package com.mvivekweb.ocpjp6.obj2.sect1.ifswitch;
    
public class switchenum {
enum name{viv,manj};
    
    public static void main(String[] args) {
        name namev = name.viv;
        switch(namev){
            case viv:
                System.out.println(<mark>namev</mark>);
        }
    }
        
}
            </pre>   
            
<pre class='out'>            
run:
viv
BUILD SUCCESSFUL (total time: 1 second)
    </pre>        


<pre>switch 
<mark>cbsei</mark></pre>

<pre>
static Boolean t1() { return new Boolean("howdy"); }
if(<mark>!t2()</mark>) System.out.print("t2 ");
return true
</pre>

<p>Unboxing automatically converts the Boolean objects to boolean
primitives. The Boolean(String) constructor views null Strings AND Strings
that don't have the value of "true" (case-insensitive) to be false. It's legal for "if"
tests to be based on method calls that return a boolean.</p>

<pre>
switch
final static short x = 2;
case<mark> x-1:</mark> System.out.print("1 "); break;
</pre>

<pre> switch(x1) {

<mark>condition can be a wrapper</mark>

case 

<mark>value cannot be a wrapper</mark></pre>

<pre>package com.mvivekweb.ocpjp6.obj2.sect1.exam;

public class Switch13 {


	public static void main(String[] args) {

		final int x2 = 7;
		final Integer x4 = 8;
		Integer <mark>x1 = 5;</mark>
		 String s = "a";
		if(x1 < 9) s += "b";
		 switch<mark>(x1) {</mark>
		 case 5: s += "c";
		 case x2-1: s += "d";
		 <mark>case x4: s += "e"; // error</mark>
		 }
		 System.out.println(s);
	}

}
</pre>
            