<pre>package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class BoomBox <mark>extends Stereo</mark>{
    
    public static void main(String[] args) {
new BoomBox().go(args);
    }
void go(String[] args) {
<mark> if(args.length > 0) makeNoise();</mark>
 if(!args[0].equals("x")) System.out.println("!x");
 }
}
class Stereo { void makeNoise() { assert false; } }
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.BoomBox.go(BoomBox.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.BoomBox.main(BoomBox.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class BoomBox2 extends Stereo1{
    
    public static void main(String[] args) {
new BoomBox2().go(args);
    }
void go(String[] args) {
 if(args.length > 0) makeNoise();
if(args[0].equals("x")) System.out.print("x ");
 if(args[0] == "x") System.out.println("x2 ");
 }
}
class Stereo1 { void makeNoise() { assert true; } }
    
</pre>

<pre>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect6/exam/BoomBox2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning
    
C:\vivek\java7\ocpjp6\ocpjp6\src><mark>java com/mvivekweb/ocpjp6/obj2/sect6/exam/BoomBox2 x</mark>
x
C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Boombox2 x
Error: Could not find or load main class com.mvivekweb.ocpjp6.obj2.sect6.exam.Boombox2
    
C:\vivek\java7\ocpjp6\ocpjp6\src>java<mark> -ea</mark> com/mvivekweb/ocpjp6/obj2/sect6/exam/<mark>Boombox2</mark> x
Error: Could not find or load main class com.mvivekweb.ocpjp6.obj2.sect6.exam.Boombox2
    
C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>
<p>
    A NoClassDefFoundError is thrown. It's lame, but you have to watch out
    for this kind of thing. The invocation should be java -ea BoomBox2 x (both B's are
    capitalized).</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class Chopper {
String a = "12b";
    public static void main(String[] args) {
        System.out.println(new Chopper().chop(args[0]));
    }
int chop(String a) {
 if(a == null) throw new IllegalArgumentException();
 return Integer.parseInt(a);
 }
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.main(Chopper.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>
<p>
    Working backwards, if somehow we passed parseInt() the String
    "12b", it would throw a <mark>NumberFormatException</mark>. If somehow we passed
    a null to chop(), we would get an <mark>IllegalArgumentException</mark>. But
    before any of that can happen, we attempt to access args[0], which throws an
    <mark>ArrayIndexOutOfBoundsException</mark>.</p>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect6/exam/Chopper.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Chopper 12b
Exception in thread "main" java.lang.NumberFormatException: For input string: "12b"
        at java.lang.NumberFormatException.forInputString(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.chop(Chopper.java:10)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.main(Chopper.java:6)

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Chopper null
Exception in thread "main" java.lang.NumberFormatException: For input string: "null"
        at java.lang.NumberFormatException.forInputString(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.chop(Chopper.java:10)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.main(Chopper.java:6)

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Chopper NULL
Exception in thread "main" java.lang.NumberFormatException: For input string: "NULL"
        at java.lang.NumberFormatException.forInputString(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at java.lang.Integer.parseInt(Unknown Source)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.chop(Chopper.java:10)
        at com.mvivekweb.ocpjp6.obj2.sect6.exam.Chopper.main(Chopper.java:6)

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>


package com.mvivekweb.ocpjp6.obj2.sect6.exam;

<pre>
public class RainCatcher {
static StringBuffer s;
    
    public static void main(String[] args) {
        Integer i = new Integer(42);
for(int j = 40; j < i; i--)
 switch(i) {
case 41: s.append("41 ");
 default: s.append("def ");
 case 42: s.append("42 ");
 }
 System.out.println(s);
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.<mark>NullPointerException</mark>
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.RainCatcher.main(RainCatcher.java:15)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
public class RainCatcher {
static StringBuffer s;
    
    public static void main(String[] args) {
        Integer i = new Integer(42);
       <mark> s= new StringBuffer();</mark>
for(int j = 40; j < i; i--)
 switch(i) {
case 41: s.append("41 ");
 default: s.append("def ");
 case 42: s.append("42 ");
 }
 System.out.println(s);
    }
        
}
    
</pre>
<pre class='out'>
run:
42 41 def 42 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>A NullPointerException is thrown because no instance of
    StringBuffer was ever created. If the StringBuffer had been created, autoboxing
    allows the Integer to be used in the for and the switch, and the switch logic would
    produce the output in answer</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class Covalent extends Bonds{
Covalent force() { return new Covalent(); }
    public static void main(String[] args) {
new Covalent().go(new Covalent());
    }
void go(Covalent c) {
 go2(new Bonds().force(), c.force());
 }
 void go2(Bonds b, Covalent c) {
 Covalent c2 = (Covalent)b;
 Bonds b2 = (Bonds)c;
 }
}
class Bonds {
 Bonds force() { return new Bonds(); }
 }
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.ClassCastException: <mark>com.mvivekweb.ocpjp6.obj2.sect6.exam.Bonds cannot be cast to com.mvivekweb.ocpjp6.obj2.sect6.exam.Covalent</mark>
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.Covalent.go2(Covalent.java:12)
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.Covalent.go(Covalent.java:9)
	at com.mvivekweb.ocpjp6.obj2.sect6.exam.Covalent.main(Covalent.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>
<pre>
Given:
42. void go() {
43. int cows = 0;
44. int[] twisters = {1,2,3};
45. for(int i = 0; i < 4; i++)
46. switch(twisters[i]) {
47. case 2: cows++;
48. case 1: cows += 10;
49. case 0: go();
50. }
51. System.out.println(cows);
52. }
What is the result?
A. 11
B. 21
C. 22
D. Compilation fails.
<mark>E. A StackOverflowError is thrown at runtime.</mark>
F. An ArrayIndexOutOfBoundsException is thrown at runtime.</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class Begin {
    
static int x;
 { int[] ia2 = {4,5,6}; }
 static {
 int[] ia = {1,2,3};
 for(int i = 0; i < 3; i++)
System.out.print(ia[i] + " ");
 x = 7;
 System.out.print(x + " ");
 }
     
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj2/sect6/exam/Begin.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Begin
Error: Main method not found in class com.mvivekweb.ocpjp6.obj2.sect6.exam.Begin, please define the main method as:
   public static void main(String[] args)
or a JavaFX application class must extend javafx.application.Application

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj2/sect6/exam/Begin.class
Error: Could not find or load main class com.mvivekweb.ocpjp6.obj2.sect6.exam.Begin.class

C:\vivek\java7\ocpjp6\ocpjp6\src>

</pre>

<p>C is correct. If you invoke a .class file with no main(), the static init blocks will
    execute before throwing a NoSuchMethodError exception. (Note that you don't need to
    know the name of the NoSuchMethodError exception to get this question correct!)</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect6.exam;
    
public class Boot {
<mark>static String s;</mark>
 static { <mark>s = "";</mark> }
 { System.out.print("shinier "); }
<mark>static { System.out.print(s.concat("better ")); }</mark>
 Boot() { System.out.print(s.concat("bigger ")); }
    public static void main(String[] args) {
new Boot();
 System.out.println("boot");
    }
        
}
    
</pre>
<pre class='out'>
run:
better shinier bigger boot
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>static init blocks run before instance init blocks (in the order in which
    they appear, respectively), and init blocks run hard on the heels of a constructor's call to
    super().</p>
