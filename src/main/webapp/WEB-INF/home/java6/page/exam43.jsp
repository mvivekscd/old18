<pre>package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class BeSafe {
    
}
class SafeDeposit {
private static SafeDeposit singleton;
public static SafeDeposit getInstance(int code) {
if(singleton == null)
singleton = new SafeDeposit(code);
return singleton;
 }
 private int code;
 private SafeDeposit(int c) { code = c; }
 int getCode() { return code; }
 }</pre>


<p>Class BeSafe can create many instances of SafeDeposit.</p>

<p>Once class BeSafe has created an instance of SafeDeposit, it cannot change the value
    of the instance's "code" variable.</p>

<p>
    It's legal to have a private constructor.<mark> As long as class BeSafe
        doesn't use multiple threads</mark>, it can create only one instance of SafeDeposit. If BeSafe
    is multithreaded, it's possible for SafeDeposit's unsynchronized getInstance()
    method to return more than one instance of the class. F is correct because "code" is
    private and there is no setter. Note: expect to see questions that cover more than one
    objective!</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
public class DMV implements Runnable{
    
    public static void main(String[] args) {
DMV d = new DMV();
 new Thread(d).start();
 Thread t1 = new Thread(d);
 t1.start();
    }
public void run() {
 for(int j = 0; j < 4; j++) { do1(); do2(); }
 }
 void do1() {
 try { Thread.sleep(1000); }
 catch (Exception e) { System.out.print("e "); }
 }
 synchronized void do2() {
 try { Thread.sleep(1000); }
 catch (Exception e) { System.out.print("e "); }
 }
}
</pre>

<p>The program's minimum execution time is about 9 seconds.</p>

<p>Both thread's first invocation of do1() will start at about the same time.
    After that second has elapsed, one of the threads will invoke do2(), and the other will
    have to wait. Once the thread that ran do2() first is done with do2(), the other thread
    can run do2(). Thereafter, the threads will alternate running do1() and do2(), but they
    are able to run mostly simultaneously.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
public class Locker extends Thread{
private static Thread t;
 public void run() {
 if(Thread.currentThread() == t) {
 System.out.print("1 ");
synchronized(t) { doSleep(2000); }
System.out.print("2 ");
} else {
 System.out.print("3 ");
 synchronized(t) { doSleep(1000); }
 System.out.print("4 ");
 }
 }
 private void doSleep(long delay) {
 try { Thread.sleep(delay); } catch(InterruptedException ie) { ; }
 }
    public static void main(String[] args) {
t = new Locker();
 t.start();
 new Locker().start();
    }
        
}
    
</pre>
<pre class='out'>
run:
3 1 4 2 BUILD SUCCESSFUL (total time: 3 seconds)
</pre>

<pre class='out'>run:
1 3 2 4 BUILD SUCCESSFUL (total time: 3 seconds)
</pre>
<pre class='out'>
The output could be 1 3 4 2
E. The output could be 1 3 2 4
F. The output could be 3 1 4 2
G. The output could be 3 1 2 4</pre>

<p>When one thread prints "1", the other thread prints "3" and
    then either thread could hold the monitor lock of "t". The other thread will wait until
    the lock is released and both threads will continue to normal completion.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class Hug implements Runnable{
static Thread t1;
 static Hold h, h2;
 public void run() {
 if(Thread.currentThread().getId() == t1.getId()) h.adjust();
 else h2.view();
 }
     
    public static void main(String[] args) {
        h = new Hold();
 h2 = new Hold();
 t1 = new Thread(new Hug());
 t1.start();
 new Thread(new Hug()).start();
    }
        
}
class Hold {
 static int x = 5;
 synchronized void adjust() {
 System.out.print(x-- + " ");
 try { Thread.sleep(200); } catch (Exception e) { ; }
  System.out.println("--1");
 view();
     System.out.println("--3");
 }
 synchronized void view() {
     System.out.println("--2");
 try { Thread.sleep(200); } catch (Exception e) { ; }
 if(x > 0) adjust();
 } }</pre>
<pre class='out'>
run:
--2
5 --1
4 --2
--1
--2
3 --1
--2
2 --1
1 --2
--1
--2
--3
--3
--3
--3
--3
BUILD SUCCESSFUL (total time: 1 second)
</pre>

<pre>

package com.mvivekweb.ocpjp6.obj4.sect3.exam;


public class Stubborn implements Runnable{

static Thread t1;
 static int x = 5;
 public void run() {
     System.out.println("Thread.currentThread().getId()="+Thread.currentThread().getId());
       System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName());
     System.out.println("t1.getId()="+t1.getId());
 if(Thread.currentThread().getId() == t1.getId()) shove();
 else push();
 }
 static synchronized void push() { System.out.println("push");shove(); }
 static void shove() {
 synchronized(Stubborn.class) {
 System.out.print(x-- + " ");
 try { System.out.println("sleep" + "Thread.currentThread().getName()="+ Thread.currentThread().getName());Thread.sleep(2000); } catch (Exception e) { ; }
 if(x > 0) push();
 } }
    public static void main(String[] args) {
        t1 = new Thread(new Stubborn());
 t1.start();
        System.out.println("--1");
 new Thread(new Stubborn()).start();
   System.out.println("--2");
    }

}

</pre>
<pre class='out'>
run:
--1
Thread.currentThread().getId()=10
Thread.currentThread().getName()=Thread-0
t1.getId()=10
--2
5 sleepThread.currentThread().getName()=Thread-0
Thread.currentThread().getId()=11
Thread.currentThread().getName()=Thread-1
t1.getId()=10
push
4 sleepThread.currentThread().getName()=Thread-0
push
3 sleepThread.currentThread().getName()=Thread-0
push
2 sleepThread.currentThread().getName()=Thread-0
push
1 sleepThread.currentThread().getName()=Thread-0
push
0 sleepThread.currentThread().getName()=Thread-1
BUILD SUCCESSFUL (total time: 12 seconds)

</pre>

<pre>
Given:
2. public class Buffalo {
3. static int x;
4. int y;
5. public static int getX() { return x; }
6. public static void setX(int newX) { x = newX; }
7. public int getY() { return y; }
8. public void setY(int newY) { y = newY; }
9. }
Which lines of code need to be changed to make the class thread safe? (Choose all that apply.)
A. Line 2
B. Line 3
C. Line 4
D. Line 5
E. Line 6
F. Line 7
G. Line 8</pre>

<pre class='out'>B-G are correct. The variables need to be private, and even methods that don't change
     a variable's value need to be synchronized if they access the variable.</pre>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class Stone implements Runnable{
    
static int id = 1;
 public void run() {
 try {
 id = 1 - id;
 if(id == 0) { pick(); } else { release(); }
 } catch(Exception e) { }
 }
 <mark>private static synchronized void pick() throws Exception {</mark>
 System.out.print("P "); System.out.print("Q ");
 }
 private synchronized void release() throws Exception {
 System.out.print("R "); System.out.print("S ");
 }
    public static void main(String[] args) {
        Stone st = new Stone();
 new Thread(st).start();
 new Thread(st).start();
    }
        
}
</pre>
<pre class='out'>
run:
R P Q S BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre class='out'>
run:
P Q R S BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre class='out'>
run:
P R Q S BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>Since pick() is static and release() is non-static, there
    are two locks. <mark>If pick() was non-static</mark>, only A would be correct.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class Stone implements Runnable{
    
static int id = 1;
 public void run() {
 try {
 id = 1 - id;
 if(id == 0) { pick(); } else { release(); }
 } catch(Exception e) { }
 }
 <mark>private synchronized void pick() throws Exception {</mark>
 System.out.print("P "); System.out.print("Q ");
 }
 private synchronized void release() throws Exception {
 System.out.print("R "); System.out.print("S ");
 }
    public static void main(String[] args) {
        Stone st = new Stone();
 new Thread(st).start();
 new Thread(st).start();
    }
        
}
</pre>

<pre class='out'>run:
     P Q R S BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class Stone implements Runnable{
    
static int id = 1;
 public void run() {
 try {
 id = 1 - id;
 if(id == 0) { pick(); } else { release(); }
 } catch(Exception e) { }
 }
 private static synchronized void pick() throws Exception {
 System.out.print("P "); System.out.print("Q ");
 }
 private static synchronized void release() throws Exception {
 System.out.print("R "); System.out.print("S ");
 }
    public static void main(String[] args) {
        Stone st = new Stone();
 new Thread(st).start();
 new Thread(st).start();
    }
        
}
</pre>
<pre class='out'>
run:
P Q R S BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
public class Pregnant extends Thread{
int x = 0;
    public static void main(String[] args) {
Runnable r1 = new Pregnant();
 new Thread(r1).start();
 new Thread(r1).start();
    }
public void run() {
 for(int j = 0; j < 3; j++) {
 x = x + 1;
 x = x + 10;
 System.out.println(x + " ");
 x = x + 100;
 } }
}
</pre>
<pre class='out'>
run:
22 
22 
133 
244 
355 
466 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>The variable <mark>x is 'shared' by both threads</mark>, and
    collectively the two threads will iterate through the for loop six times. Remember that
    either thread can pause any number of times, and at any point within the run() method.</p>
<pre>
Given:
2. class Grab {
3. static int x = 5;
4. synchronized void adjust(Grab y) {
5. System.out.print(x-- + " ");
6. y.view(y);
7. }
8. synchronized void view(Grab z) { if(x > 0) z.adjust(z); }
9. }
10. public class Grapple implements Runnable {
11. static Thread t1;
12. static Grab g, g2;
13. public void run() {
14. if(Thread.currentThread().getId() == t1.getId()) g.adjust(g2);
15. else g2.view(g);
16. }
17. public static void main(String[] args) {
18. g = new Grab();
19. g2 = new Grab();
20. t1 = new Thread(new Grapple());
21. t1.start();
22. new Thread(new Grapple()).start();
23. } }
Which are true? (Choose all that apply.)
A. Compilation fails.
B. The output could be 5 4 3 2 1
C. The output could be 5 4 3 2 1 0
D. The program could produce thousands of lines of output.
E. The program could deadlock before producing any output.
F. The output could be "5 ", followed by the program deadlocking.</pre>

<p>B, C, and F are correct. This program could deadlock with "g" waiting to invoke view()
    while "g2" waits to invoke adjust(). If the code doesn't deadlock, then B and C are
    correct, B being the more common output.</p>


<pre>
    
package com.mvivekweb.ocpjp6.obj4.sect3.exam;
    
    
public class Checkout2 implements Runnable{
void doStuff() { }
 synchronized void doSynch() {
 try { Thread.sleep(1000); }
 catch (Exception e) { System.out.print("e "); }
 }
     
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
 new Thread(new Checkout2()).start();
 Thread t1 = new Thread(new Checkout2());
 t1.start();
 try { t1.join(); }
 catch (Exception e) { System.out.print("e "); }
 System.out.println("elapsed: "
+ (System.currentTimeMillis() - start));
    }
public void run() {
 for(int j = 0; j < 4; j++) {
 doStuff();
 try { Thread.sleep(1000); }
 catch (Exception e) { System.out.print("e "); }
 doSynch();
 } }
}
    
    
</pre>
<pre class='out'>
run:
elapsed: 8125
BUILD SUCCESSFUL (total time: 8 seconds)
</pre>

<p>Even when doSynch() is synchronized, the two run() invocations
    aren't running against the same Checkout2 object. This code creates two distinct
    Checkout2 objects, so there is no synchronization.</p>

