<h3>Write code to define, instantiate, and start new threads using both java.lang.Thread
    and java.lang.Runnable.</h3>


<p>"thread" means two different things:</p>

An instance of class java.lang.Thread
<br>A thread of execution


<p>instance of Thread is just...an <mark>object</mark>.it has
    variables and methods, and lives and dies on the heap</p>


<p>But a <mark>thread of execution</mark> is
    an individual <mark>process</mark> (a "lightweight" process) that <mark>has its own call stack</mark></p>

<p>there is <mark>one thread per call stack</mark></p>


<p>The main() method, that starts the whole ball rolling, runs in one thread, called
    (surprisingly) the <mark>main thread</mark></p>


<p>But as soon as you create a new thread, a new stack
    materializes and methods called from that thread run in a <mark>call stack</mark> that's <mark>separate
    from the main() call stack</mark>.</p>

    <p>That second new call stack is said to run <mark>concurrently</mark>
        with the main thread</p>

    <p>When it comes to threads, very little is guaranteed.</p>
    
    <p>The thread questions are among the most difficult questions on the exam </p>
    
    <p>"user" threads and daemon thread  </p>  

    <p>The difference between these two
        types of threads (user and daemon) is that the JVM exits an application only when
        all user threads are complete-the JVM doesn't care about letting daemon threads
        complete, so once all user threads are complete, the JVM will shut down, regardless
        of the state of any daemon threads.</p>
    
    <h3>Making a Thread </h3>
    
    <p>The action happens in the run() method</p>
    
    <p>what you really want is that job to be executed
        in its own thread    </p>
    
    <p>So if the work you want done is the job, the <mark>one doing the work</mark>
        (actually executing the job code) <mark>is the thread</mark> </p>
    
    <p> job always starts from a
        run() method  </p>
    <pre>    
public void run() {
// your job code goes here
}    
    </pre>
    
    <p>can define and instantiate a thread in one of two ways: </p>
    
    <mark>Extend</mark> the java.lang.<mark>Thread class</mark>.

    <br><mark>Implement</mark> the <mark>Runnable interface</mark>.

    <p>real world you're
        <mark>much more likely to implement Runnable</mark> than extend Thread </p>   

    <p>Extending the
        Thread class is the easiest, but it's usually not a good OO practice</p>
    
    <h3>Defining a Thread  </h3>
    
    <h3>Extending java.lang.Thread </h3>
    
    <p>    simplest way to define code to run in a separate thread</p>
    
Extend the java.lang.Thread class. 
<br>Override the run() method.

<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class MyThread <mark>extends Thread </mark>{
    
    <mark>public void run() {</mark>
        System.out.println("Important job running in MyThread");
    }
        
    public void run(String s) {
        System.out.println("String in run is " + s);
    }
}
    
</pre>  
 
<p>if you extend Thread, you <mark>can't extend anything</mark> else </p>

<p>you're <mark>free to overload the run()</mark> method</p>

<h3>Implementing java.lang.Runnable</h3>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class MyRunnable <mark>implements Runnable</mark>{
    
    <mark>public void run() {</mark>
        System.out.println("Important job running in MyRunnable");
    }
        
}
</pre>


<h3>Instantiating a Thread</h3>
        
        
<p> Regardless of whether your run() method is in a Thread subclass or a Runnable
    implementation class, <mark>you still need a Thread object</mark> to do the work.    </p>

<p>
    To have
    code run by a separate thread, you still <mark>need a Thread instance</mark>.    </p>


<p>rather than
    <mark>combining</mark> both the <mark>thread and the job</mark> (the code in the run()method) into one
    class, you've split it into two classes-<mark>the Thread class for the thread-specific code</mark>
    and your <mark>Runnable implementation class for your job-that-should-be-run-by-athread
    code</mark>. (Another common way to think about this is that the <mark>Thread is the
    "worker,"</mark> and the <mark>Runnable is the "job"</mark> to be done.)  </p>      
        
        
<p>but when you use Runnable, you need to tell the new thread to
    use your <mark>run()method rather than its own</mark>. </p>       
        
<p>The Runnable you pass to the Thread
    constructor is called the <mark>target</mark> or the <mark>target Runnable</mark>. </p>       
        
<p> You can pass a <mark>single Runnable instance</mark> to <mark>multiple Thread objects</mark>, so that the
    same Runnable becomes the target of multiple threads    </p>


<pre>        
    
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class MyRunnable implements Runnable{
    
    public void run() {
        System.out.println("Important job running in MyRunnable");
    }
        
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class TestThreads {
    
    public static void main(String[] args) {
        MyRunnable <mark>r</mark> = new MyRunnable();
        Thread foo = new Thread(<mark>r</mark>);
        Thread bar = new Thread(r);
        Thread bat = new Thread(r);
    }
}
    
</pre>    
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 1 second)
    </pre>    
        
 <p>The <mark>Thread class</mark> itself <mark>implements Runnable</mark></p>
 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
public class MyThread extends <mark>Thread</mark> {
    
    public void run() {
        System.out.println("Important job running in MyThread");
    }
        
    public void run(String s) {
        System.out.println("String in run is " + s);
    }
}
 </pre>
 
 <pre> 
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class TestThreads {
    
    public static void main(String[] args) {
        MyRunnable r = new MyRunnable();
        Thread foo = new Thread(r);
        Thread bar = new Thread(r);
        Thread bat = new Thread(r);
            
<mark>        MyThread r1 = new MyThread();
        Thread foo1 = new Thread(r1);</mark>
            
            
    }
}
    
 </pre>
 
 <p>In this case, you really just need a
     Runnnable, and <mark>creating a whole other Thread is overkill</mark>.</p>
 
 
 <p> The constructors we care about are</p>
 
 Thread()
 <br> Thread(Runnable <mark>target</mark>)
 <br>Thread(Runnable <mark>target</mark>, String <mark>name</mark>)    
 <br> Thread(String <mark>name</mark>)   
    
 <p> When a thread has been instantiated but not started the thread is
     said to be in the <mark>new state</mark></p>
 
 <p>Once the start() method is <mark>called</mark>, the thread is considered to be <mark>alive</mark> </p>
 
 
 <p>A
     thread is considered <mark>dead</mark> (no longer alive)<mark> after the run()</mark> method completes </p>
 
 <p> <mark>isAlive()</mark> method is the best way to determine if a thread has been started but has
     not yet completed its run() method</p>
 
 <p> The <mark>getState()</mark> method is very useful
     for <mark>debugging</mark></p>
 
 <pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class TestThreads {
    
    public static void main(String[] args) {
        MyRunnable r = new MyRunnable();
        Thread foo = new Thread(r);
        Thread bar = new Thread(r);
        Thread bat = new Thread(r);
            
            
        MyThread r1 = new MyThread();
        Thread foo1 = new Thread(r1);
            
        System.out.println("foo1 alive="+<mark>foo1.isAlive()</mark>);
        System.out.println("foo1 state="+<mark>foo1.getState()</mark>);
            
    }
}
 </pre>
 
  <img src="/imag/jp6/threadext.png" class="imgw">
 <pre class='out'>
 run:
foo1 alive=false
<mark>foo1 state=NEW</mark>
BUILD SUCCESSFUL (total time: 1 second)
    </pre> 
 
 
  <pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
    
public class TestThreadextends {
    
    
    public static void main(String[] args) {
        new MyThread().start();
    }
        
}
  </pre>
  
<pre class='out'>  run:
Important job running in MyThread
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
  
    <img src="/imag/jp6/threadext1.png" class="imgw">
  <pre> 
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class MyThread extends Thread {
    
   // public void run() {
   //     System.out.println("Important job running in MyThread");
   // }
       
    public void run(String s) {
        System.out.println("String in run is " + s);
    }
}
    
  </pre>
  
    <pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
    
public class TestThreadextends {
    
    
    public static void main(String[] args) {
        new MyThread().start();
    }
        
}
    </pre>
    
  <pre class='out'>
  run:
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
    
  <img src="/imag/jp6/threadext2.png" class="imgw">   
 <h3> Starting a Thread</h3>
 
 <p> You call
     start() on a Thread instance,<mark> not on a Runnable instance</mark>
 </p>
 
 <p> A new thread of execution starts (with a new call stack).</p>
 
 <p>The thread moves from the <mark>new</mark> state to the <mark>runnable</mark> state.</p>
 
 <p>When the thread gets a chance to execute, its target run() method will run.</p>
 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
class FooRunnable implements Runnable {
    public void run() {
        for (int x = 1; x < 6; x++) {
            System.out.println("Runnable running");
        }
    }
}
    
public class TestThreads1 {
    public static void main(String[] args) {
        FooRunnable r = new FooRunnable();
        Thread t = new Thread(r);
        System.out.println("t.getState()="+<mark>t.getState()</mark>);
        t.start();
       System.out.println("t.getState()1="+<mark>t.getState()</mark>);
    }
}
 </pre>
 <pre class='out'>
run:
<mark>t.getState()=NEW</mark>
Runnable running
<mark>t.getState()1=RUNNABLE</mark>
Runnable running
Runnable running
Runnable running
Runnable running
BUILD SUCCESSFUL (total time: 1 second) 
    </pre>
 
 <img src="/imag/jp6/thread1.png" class="imgw"> 
 
 <p>
     But it doesn't mean the
     run() method will run in a separate thread! Calling a run() method directly just means
     you're invoking a method from whatever thread is currently executing, and the <mark>run()</mark>
     method goes onto the <mark>current call stack</mark> rather than at the <mark>beginning of a new call stack</mark>. 
 </p>

 <p>print out which thread is executing. <mark>getName()</mark> method of <mark>class Thread</mark>.</p>
 
 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
class NameRunnable implements Runnable {
    
    public void run() {
        System.out.println("NameRunnable running");
<mark>        System.out.println("Run by "
                + Thread.currentThread().getName());</mark>
    }
}
    
public class NameThread {
    
    public static void main(String[] args) {
<mark>        System.out.println("Run by "
                + Thread.currentThread().getName());</mark>
        NameRunnable nr = new NameRunnable();
        Thread t = new Thread(nr);
        //t.setName("Fred");
        t.start();
    }
}
 </pre>
 <pre class='out'>
run:
<mark>Run by main</mark>
NameRunnable running
<mark>Run by Thread-2</mark>
BUILD SUCCESSFUL (total time: 1 second) 
    </pre>
 
 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
class NameRunnable implements Runnable {
    
    public void run() {
        System.out.println("NameRunnable running");
        System.out.println("Run by "
                + Thread.currentThread().getName());
    }
}
    
public class NameThread {
    
    public static void main(String[] args) {
        System.out.println("Run by "
                + Thread.currentThread().getName());
        NameRunnable nr = new NameRunnable();
        Thread t = new Thread(nr);
        <mark>t.setName("Fred");</mark>
        t.start();
    }
}
    
 </pre>
 <pre class='out'>
run:
Run by main
NameRunnable running
<mark>Run by Fred</mark>
BUILD SUCCESSFUL (total time: 1 second) 
    </pre>
 
 
 <p> create thread stack diagram here pg 712</p>
 
 <h3>Starting and Running Multiple Threads </h3>
 
 <p> because the main() method starts in a
     thread of its own, and then <mark>t.start()</mark> <mark>started</mark> a second thread</p>
 
 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
class NameRunnable1 implements Runnable {
    
    public void run() {
        for (int x = 1; x <= 3; x++) {
            System.out.println("Run by " 
                    + Thread.currentThread().getName() + ", x is " + x);
        }
    }
}
    
public class ManyNames {
    
    public static void main(String[] args) {
// Make one Runnable
        NameRunnable1 nr = new NameRunnable1();
        Thread one = new Thread(nr);
        Thread two = new Thread(nr);
        Thread three = new Thread(nr);
        one.setName("Fred");
        two.setName("Lucy");
        three.setName("Ricky");
        one.start();
        two.start();
        three.start();
    }
}
    
 </pre>
 <pre class='out'>
run:
Run by Fred, x is 1
Run by Fred, x is 2
<mark>Run by Ricky, x is 1</mark>
Run by Fred, x is 3
Run by Ricky, x is 2
Run by Ricky, x is 3
Run by Lucy, x is 1
Run by Lucy, x is 2
Run by Lucy, x is 3
BUILD SUCCESSFUL (total time: 1 second) 
 
    </pre>
 
 <img src="/imag/jp6/threadm.png" class="imgw"> 
 
 <p> the behavior you see above is <mark>not guaranteed</mark></p>
 
 <p> And there is no guarantee that once a thread
     starts executing, it will <mark>keep executing until it's done</mark>. Or that a loop will <mark>complete
     before another thread</mark> begins.</p>
 
 
 <p> Each thread will start, and each thread <mark>will run to completion</mark>.</p>
 
 <p>Just because a series of threads are started in a
     particular order <mark>doesn't mean they'll run in that order</mark>. </p>
 
 <p> <mark>Within</mark> each thread, things will happen in a predictable order</p>
 
 <p>
     There is a way, however, to start a thread but tell it <mark>not to
     run until some other thread has finished</mark>. You can do this with the <mark>join()</mark> method
 </p>
 
 <p> A thread is done being a thread when its target run() method completes.</p>
 
 <p> The <mark>stack</mark> for that thread <mark>dissolves</mark>, and the thread is considered <mark>dead</mark>.</p>
 
 <p> 
     Not dead and gone, however, just dead. It's still a Thread object, just not a
     thread of execution. So if you've got a reference to a Thread instance, then even when
     that Thread instance is no longer a thread of execution, you can still call methods
     on the Thread instance, just like any other Java object.
 </p>
 
 <p>Once a thread <mark>has been started</mark>, it <mark>can never be started</mark> again.</p>
 
 
 <pre>
package com.mvivekweb.ocpjp6.obj4.sect1.threads;
    
public class Testthreadstart {
    public static void main(String[] args) {
        Thread.currentThread().start();
    }
}
    
 </pre>
 <pre class='out'>
 run:
Exception in thread "main" java.lang.<mark>IllegalThreadStateException</mark>: <mark>Thread is already started</mark>
	at java.lang.Thread.start(Thread.java:881)
	at com.mvivekweb.ocpjp6.obj4.sect1.threads.Testthreadstart.main(Testthreadstart.java:10)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
 <p>
     If you call
     start() a second time, it will cause an exception.an <mark>IllegalThreadStateException</mark>
 </p>
 
 <p> A runnable
     thread or a dead thread <mark>cannot be restarted</mark>.</p>

 <pre>
 package com.mvivekweb.ocpjp6.obj4.sect1.threads;
     
public class TestDeadThread {
    
    public static void main(String[] args) {
        NameRunnable2 newthread = new NameRunnable2();
        Thread th = new Thread(newthread);
        th.start();
       <mark> th.stop();</mark>
        System.out.println("th.getState()="+th.getState());
    }
}
    
class NameRunnable2 implements Runnable {
    
    public void run() {
        System.out.println("NameRunnable running");
        System.out.println("Run by "
                + Thread.currentThread().getName());
    }
}
 </pre>
<pre class='out'> 
run:
<mark>th.getState()=TERMINATED</mark>
BUILD SUCCESSFUL (total time: 1 second) 
    </pre>
 <p>
The <mark>getld()</mark> method returns a positive, unique, long number, and that
number will be that thread's only ID number for the thread's entire life. 
 </p>
 
 
 <h3> The Thread Scheduler</h3>
 
 <p><mark>Any </mark>thread in the <mark>runnable state</mark> can be chosen by the scheduler to be the one and
     only running thread</p>
 
 <p> The<mark> order </mark>in which runnable threads are chosen to run is <mark>not guaranteed</mark>.</p>
 
 <p> <b>Methods from the java.lang.Thread Class:</b> Some of the methods that can
     help us influence thread scheduling are as follows:<mark>sleep, yield, join, setPriority</mark></p>
 <pre>
 public <mark>static</mark> void sleep(long millis) <mark>throws InterruptedException</mark>
public <mark>static</mark> void yield()
public final void join() <mark>throws InterruptedException</mark>
public final void setPriority(int newPriority)
 </pre>
 
 
 <p> Methods from the java.lang.Object Class: <mark>wait, notify, notifyAll</mark></p>
 
 <pre>public final void wait() <mark>throws InterruptedException</mark>
public final void notify()
public final void notifyAll()</pre>
 
<p>run() method cannot
be static.</p>

<p>look for two <mark>t.start</mark> and also <mark>try </mark>catch</p>

<p>start() and run() are defined by the Thread class</p>
 