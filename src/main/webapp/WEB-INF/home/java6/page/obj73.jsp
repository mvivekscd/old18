<h3>Determine the effect upon object references and primitive values when they are passed
            into methods that perform assignments or other modifying operations on the parameters.  </h3>
        
        
        <p>The
            <mark>difference</mark> between <mark>object reference and primitive variables</mark>, when passed into
            methods, is <mark>huge and important</mark>.     </p>   
        
<h3>Passing Object Reference Variables </h3>
        
<p>When you pass an object variable into a method, you must keep in mind that you're
    passing the object reference, and not the actual object itself   </p>

<p>More importantly, you must remember
    that <mark>you aren't even passing the actual reference variable, but rather a copy</mark> of the
    reference variable.</p>

<p>both the <mark>caller and the
    called method</mark> will now have <mark>identical copies of the reference</mark>, and thus both will
    refer to the same exact (not a copy) object on the heap.</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.passMethods;
    
import java.awt.Dimension;
    
public class ReferenceTest {
    
    public static void main(String[] args) {
        Dimension d = new Dimension(5, 10);
        ReferenceTest rt = new ReferenceTest();
        System.out.println("Before modify() d.height = "
                + d.height);
        rt.modify(<mark>d</mark>);
        System.out.println("After modify() d.height = "
                + d.height);
    }
        
    <mark>void </mark>modify(<mark>Dimension dim</mark>) {
        dim.height = dim.height + 1;
        System.out.println("dim = " + dim.height);
    }
}
</pre>

<pre class='out'>
run:
Before modify() d.height = 10
dim = 11
After modify() <mark>d.height = 11</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>  
<p>        
    any <mark>changes to the object that occur inside the method are being made to the object</mark>
    whose reference was passed.        
</p>

<h3>Does Java Use Pass-By-Value Semantics?</h3>

<p>Java is actually pass-by-value for all variables running within a single
    VM. Pass-by-value means pass-by-variable-value. And that means, pass-by-copy-ofthe-
    variable! (There's that word copy again!)</p>

<p>It makes <mark>no difference</mark> if you're <mark>passing primitive or reference variables</mark>, you are
    <mark>always passing a copy of the bits</mark> in the variable.</p>

<p>if you pass an int
    variable with the value of 3, you're p<mark>assing a copy of the bits representing 3</mark>.</p>
<p>
    the <mark>called method can't change the caller's
    variable</mark>, although for <mark>object reference variables</mark>, the <mark>called method can change the
    object</mark> the variable referred to.
</p>

<p>
    For object references, it means the <mark>called method can't
    reassign the caller's original reference variable</mark> and make it refer to a <mark>different object,
    or null</mark>.
</p>
<pre>
void bar() {
Foo f = new Foo();
doStuff(f);
}
void doStuff(Foo g) {
g.setName("Boo");
<mark>g = new Foo();</mark>
}
    
</pre>

<p><mark>g does not reassign f</mark></p>

<h3>Passing Primitive Variables</h3>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.passMethods;
    
public class ReferenceTest1 {
    
    public static void main(String[] args) {
        int a = 1;
        ReferenceTest1 rt = new ReferenceTest1();
        System.out.println("Before modify() a = " + a);
        rt.modify(a);
        System.out.println("After modify() a = " + a);
    }
        
    void modify(int number) {
        number = number + 1;
        System.out.println("number = " + number);
    }
}
    
</pre>
<pre class='out'>
run:
Before modify() a = 1
number = 2
<mark>After modify() a = 1</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>Shadowing involves <mark>redeclaring a variable
    that's already been declared somewhere</mark> else.</p>
<p>
    The effect of shadowing is to hide the
    previously declared variable in such a way
    that it may look as though you're using the
    hidden variable, but you're actually using the
    shadowing variable.</p>
<pre>
package com.mvivekweb.ocpjp6.obj7.sect3.passMethods;
    
public class Foo {
    
    Bar myBar = new Bar();
        
    void changeIt(Bar myBar) {
        myBar.barNum = 99;
        System.out.println("myBar.barNum in changeIt is " + myBar.barNum);
        <mark>myBar = new Bar();</mark>
        myBar.barNum = 420;
        System.out.println("myBar.barNum in changeIt is now " + myBar.barNum);
    }
        
    public static void main(String[] args) {
        Foo f = new Foo();
        System.out.println("f.myBar.barNum is " + f.myBar.barNum);
        <mark>f.changeIt(f.myBar);</mark>
        System.out.println("f.myBar.barNum after changeIt is "
                + <mark>f.myBar.barNum</mark>);
    }
}
    
class Bar {
    
    int barNum = 28;
}
    
</pre>

<pre class='out'>
run:
f.myBar.barNum is 28
myBar.barNum in changeIt is 99
<mark>myBar.barNum in changeIt is now 420
f.myBar.barNum after changeIt is 99</mark>
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>You can see that the <mark>shadowing variable </mark>(the local parameter myBar in changeIt()) <mark>can still
    affect the myBar instance variable</mark>, because the myBar parameter receives a reference to the same
    Bar object.</p>
