<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class Endless {
public static void main(String[] args) {
 int i = 0;
 short s = 0;
 for(int j = 0, k = 0; j < 3; j++) ;
 for(int j = 0; j < 3; counter(j)) ;
 for(<mark>int j = 0, int k = 0</mark>; j < 3; j++) ;
 for(; i < 5; counter(5), i++) ;
for(i = 0; i < 3; i++, System.out.print("howdy ")) ;
 }
 static int counter(int y) { return y + 1; }
}
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\exam>javac -source 1.6 Endless.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
Endless.java:9: error: <identifier> expected
 for(int j = 0, int k = 0; j < 3; j++) ;
               ^
Endless.java:9: error: not a statement
 for(int j = 0, int k = 0; j < 3; j++) ;
                             ^
Endless.java:9: error: ')' expected
 for(int j = 0, int k = 0; j < 3; j++) ;
                                ^
Endless.java:9: error: ';' expected
 for(int j = 0, int k = 0; j < 3; j++) ;
                                     ^
4 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\exam>
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class Endless {
public static void main(String[] args) {
 int i = 0;
 short s = 0;
 for(int j = 0, k = 0; j < 3; j++) ;
 for(int j = 0; j < 3; <mark>counter(j)</mark>) ;
     
 for(; i < 5; counter(5), i++) ;
for(i = 0; i < 3; i++, System.out.print("howdy ")) ;
 }
 static int counter(int y) { return y + 1; }
}
</pre>


<p>would cause an endless loop
    because the counter() method is <mark>not incrementing the for loop's "j" variable</mark>.</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class OffRamp {
    
    public static void main(String[] args) {
int [] exits = {0,0,0,0,0,0};
int x1 = 0;
    
for(int x = 0; x < 4; x++) exits[0] = x;
for(int x = 0; x < 4; ++x) exits[1] = x;
    
 x1 = 0; while(x1++ < 3) exits[2] = x1;
x1 = 0; while(++x1 < 3) exits[3] = x1;
    
x1 = 0; do { exits[4] = x1; } while(x1++ < 7);
x1 = 0; do { exits[5] = x1; } while(++x1 < 7);
    
for(int x: exits)
System.out.print(x + " ");
    }
        
}
</pre>

<pre class='out'>
run:
3 3 3 2 7 6 BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class OffRamp {
    
    public static void main(String[] args) {
int [] exits = {0,0,0,0,0,0};
int x1 = 0;
    
for(int x = 0; x < 4; <mark>x++</mark>) {exits[0] = x;System.out.println("x="+x);}
for(int x = 0; x < 4; <mark>++x</mark>){ exits[1] = x;System.out.println("x="+x);}
    
 x1 = 0; while(<mark>x1++</mark> < 3) {exits[2] = x1;System.out.println("x1="+x1);}
x1 = 0; while(<mark>++x1</mark> < 3) {exits[3] = x1;System.out.println("x1="+x1);}
    
x1 = 0; do { exits[4] = x1; System.out.println("x11="+x1);} while(x1++ < 7);
x1 = 0; do { exits[5] = x1; System.out.println("x12="+x1);} while(++x1 < 7);
    
for(int x: exits)
System.out.print(x + " ");
    }
        
}
    
</pre>
<pre class='out'>
run:
x=0
x=1
x=2
x=3
x=0
x=1
x=2
x=3
x1=1
x1=2
x1=3
x1=1
x1=2
x11=0
x11=1
x11=2
x11=3
x11=4
x11=5
x11=6
x11=7
x12=0
x12=1
x12=2
x12=3
x12=4
x12=5
x12=6
3 3 3 2 7 6 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>With "for" loops, the last things that happen are the iteration expression
    and then the conditional expression. With "while" loops, the body only executes if
    the expression is true. With "do" loops, the body executes first, before the expression is
    evaluated.</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class Ice {
Long[] stockings = {new Long(3L), new Long(4L), new Long(5L)};
static int count = 0;
    public static void main(String[] args) {
new Ice().go();
    System.out.println(count);
 }
void go() {
 for(short x = 0; x < 5; x++) {
 if(x == 2) return;
 for(long ell: stockings) {
 count++;
 if(ell == 4) break;
 } } } }
     
</pre>
<pre class='out'>
run:
4
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
    
public class Fortran {
   static int bump(int i) { return i + 2; }
       
    public static void main(String[] args) {
        for(int x = 0; x < 5; bump(x))
           System.out.print(x + " ");
    }
        
}
    
</pre>
<p>
    In a for loop, the iteration expression can be most any code statement that
    you want, such as the bump() method call shown on line 6. In this case, the result of the
    addition is not captured, so x's value never changes, therefore the for loop runs infinitely.
    If the iteration expression read "x = bump(x)"
    
</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
    
public class Fortran {
   static int bump(int i) { return i + 2; }
       
    public static void main(String[] args) {
        for(int x = 0; x < 5; <mark>x=bump(x)</mark>)
           System.out.print(x + " ");
    }
        
}
    
</pre>
<pre class='out'>
run:
0 2 4 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
    
public class forLoop {
    
    
    public static void main(String[] args) {
        boolean[] ba = {true, false};
 short[][] gr = {{1,2}, {3,4}};
 int i = 0;
 for( ; i < 10; ) i++;
 for(short s: gr) ;
 for(int j = 0, k = 10; k > j; ++j, k--) ;
 for(int j = 0; j < 3; System.out.println(j++)) ;
 for(Boolean b: ba) ;
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - incompatible types: short[] cannot be converted to short
	at com.mvivekweb.ocpjp6.obj2.sect2.exam.forLoop.main(forLoop.java:14)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class Coyote {
    
    public static void main(String[] args) {
int x = 4;
 int y = 4;
 while((x = jump(x)) < 8)
 do {
 System.out.print(x + " ");
 } while ((y = jump(y)) < 6);
    }
static int jump(int x) { <mark>return ++x;</mark> }
}
    
</pre>
<pre class='out'>
run:
5 5 6 7 BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
import java.util.List;
    
public class AndOver {
    
    public static void main(String[] args) {
List g = new ArrayList();
 g.add(new Gaited("Eyra"));
 g.add(new Gaited("Vafi"));
 g.add(new Gaited("Andi"));
 Iterator i2 = g.iterator();
    while(i2.hasNext()) {
 System.out.print(i2.next().name + " ");
 } } }
class Gaited {
 public String name;
 Gaited(String n) { name = n; }
 }
     
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\exam>javac -source 1.6 AndOver.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
AndOver.java:16: error: cannot find symbol
 System.out.print(i2.next().name + " ");
                           ^
  symbol:   variable name
  location: class Object
Note: AndOver.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect2\exam>

</pre>


<p>Iterator's next() method returns an Object, which in this case needs to
    be cast to a Gaited before its name property can be used.</p>
<pre>
import java.util.List;
    
public class AndOver {
    
    public static void main(String[] args) {
List g = new ArrayList();
 g.add(new Gaited("Eyra"));
 g.add(new Gaited("Vafi"));
 g.add(new Gaited("Andi"));
 Iterator i2 = g.iterator();
    while(i2<mark>.hasNext()</mark>) {
 System.out.print(<mark>((Gaited)i2.next()).name</mark> + " ");
 } } }
class Gaited {
 public String name;
 Gaited(String n) { name = n; }
 }
     
</pre>
<pre class='out'>
run:
Eyra Vafi Andi BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect2.exam;
    
public class labeltest {
    
    public static void main(String[] args) {
int count = 0;
 outer:
for(int x = 0; x < 5; x++) {
middle:
for(int y = 0; y < 5; y++) {
if(y == 1) continue middle;
if(y == 3) break middle;
 count++;
 System.out.println("count1: " + count);
 }
 if(x > 2) continue outer;
 count = count + 10;
     
 System.out.println("count2: " + count);
}
 System.out.println("count3: " + count);
    }
        
}
</pre>
<pre class='out'>
run:
count1: 1
count1: 2
count2: 12
count1: 13
count1: 14
count2: 24
count1: 25
count1: 26
count2: 36
count1: 37
count1: 38
count1: 39
count1: 40
count3: 40
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

