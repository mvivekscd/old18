<p>Coupling is the OO principle most closely associated with making sure classes know about
    other classes only through their APIs.</p>


<pre>
package com.mvivekweb.ocpjp6.obj5.sect1.exam;
    
    
public class Contact {
    private String name;
 private String city;
 String getName() { return name; }
 void setName(String n) { name = n; }
 void setCity(String c) {
 <mark>if(c == null) throw new NullPointerException();</mark>
 city = c;
 }
 String getCity() { return city; }
}
    
</pre>

<p>The class is well encapsulated.</p>

<p>The setCity() method includes argument validation, which is a good thing.
    It's not, by itself, an example of coupling or cohesion. Also, <mark>validation doesn't make a
    method more encapsulated</mark>.</p>

    <pre>Given the following pseudo-code design for a new accounting system:

class Employee
maintainEmployeeInfo()
connectToRDBMS()

class Payroll
setStateTaxCodes()
findEmployeesByState()

class Utilities
getNetworkPrinter()</pre>
    
<p>    These classes appear to have low cohesion.</p>
    
<p>These classes appear to have weak encapsulation. </p>
    
<pre>
package com.mvivekweb.ocpjp6.obj5.sect1.exam;
    
public class Payroll {
    int salary;
 int getSalary() { return salary; }
 void setSalary(int s) {
 assert(s > 30000);
 salary = s;
 }
}
</pre>

<p>If the salary variable was private, the class would be well encapsulated.</p>

<p><mark>Removing assert line</mark>  would make the class's use of assertions more appropriate.</p>

<p>assert line  currently uses the assert mechanism to
    validate a non-private method's argument, which is not considered appropriate.</p>

<p>assert mechanism is mainly independent of the OO
    concepts of cohesion and encapsulation.</p>

<pre>Your company makes compute-intensive, 3D rendering software for the movie industry. Your
chief scientist has just discovered a new algorithm for several key methods in a commonly used
utility class. The new algorithm will decrease processing time by 15 percent, without having
to change any method signatures. After you change these key methods, and in the course of
rigorous system testing, you discover that the changes have introduced no new bugs into the
software.
In terms of your software's overall design, which are probably true? (Choose all that apply.)</pre>

<p>Your software is well encapsulated.</p>

<p>Your software demonstrated loose coupling.</p>

<p>A hallmark of well-encapsulated code is that methods can be
    re-implemented without causing side effects.</p>

<p>A hallmark of loose coupling is
    when one class can change without adversely affecting the workings of classes that interact
    with it.</p>

<pre>
    
package com.mvivekweb.ocpjp6.obj5.sect1.exam;
    
    
public class Bicycle {
    
    
    public static void main(String[] args) {
      Wheel[] wa = {new Wheel(15), new Wheel(17)};
for(Wheel w: wa)
 w.spin();  
    }
        
}
class Wheel {
Wheel(int s) { size = s; }
 int size;
 void spin() { System.out.print(size + " inch wheel spinning, "); }
 }</pre>

<p>The Bicycle class is loosely coupled with the Wheel class.</p>

<p>If size was private, the degree of encapsulation would change.</p>

<p>Bicycle accesses Wheel only
    <mark>through its informal API, the method and the constructor</mark>.</p>

<pre>
Given the design implied by this partially implemented class:
2. public class RobotDog {
3. int size;
4. void bark() { /* do barking */ }
5. int getSize() { return size; }
6. { size = 16; }
7. int getNetworkPrinterID() {
8. /* do lookup */
9. return 37;
10. }
11. void printRobotDogStuff(int printerID) { /* print RobotDog stuff */ }
12.
</pre>

<p>To improve cohesion, getNetworkPrinterID() should be moved to a different class.</p>

<p>it's okay for a class to print
    its own state</p>

<p>If a class's member's <mark>values can be retrieved, but not changed, without</mark> using the class's API,
    <mark>tight coupling</mark> could occur.</p>

<p>If a class's member's values can be retrieved, but not changed, without using the class's API,
    the class is not well encapsulated.</p>

<p>If a class's member's values can be updated only through the use of its API, or by an inner
    class, the class is well encapsulated.</p>




