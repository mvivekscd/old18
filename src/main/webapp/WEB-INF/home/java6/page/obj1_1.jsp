<h3> Develop code that declares classes (including abstract and all forms of nested classes),
    interfaces, and enums, and includes the appropriate use of package and import statements
    (including static imports).</h3>


<h3>Java Refresher</h3>

<p>Java program is mostly a collection of objects talking to other objects by invoking
    each other's methods.</p>

<p>Every <mark>object</mark> is of a certain <mark>type</mark>, and that type is defined by a
    class or an interface.</p>

<h3>Class</h3>

<p>A template that describes the kinds of state and behavior that objects
    of its type support.</p>


<img src="/imag/jp6/class.png" class="imgw"> 


<h3>Object</h3>

<p>when the Java Virtual Machine (JVM) <mark>encounters the
        new keyword</mark>, it will use the appropriate class to make an object <mark>which is an
            instance of that</mark> class.</p>


<img src="/imag/jp6/object.png" class="imgw"> 

<h3>State (instance variables)</h3>

<p>Each object (instance of a class) will have its
    own unique set of instance variables as defined in the class.</p>


<h3>Behavior (methods)</h3>

<p>Methods are where the class' logic is stored</p>

<p>
    They are where <mark>algorithms get executed</mark>, and
    <mark>data gets manipulated</mark>.
</p>



<h3>Identifiers and Keywords</h3>

<p>classes, variables, and methods-
    need names. In Java <mark>these names</mark> are called <mark>identifiers</mark></p>


<h3>Inheritance</h3>

<p>allows <mark>code defined in one class to be reused</mark> in other classes.
</p>

<p>The superclass knows nothing of the classes that inherit from
    it, but all of the subclasses that inherit from the superclass must explicitly declare the
    inheritance relationship.</p>


<p>A subclass that inherits from a superclass is <mark>automatically
        given accessible instance variables and methods</mark> defined by the superclass, but <mark>is also
            free to override superclass methods</mark> to define more specific behavior.</p>











<h3>Declaration Rules (Objective 1.1)</h3>
               <p>a <mark>public method</mark> can be accessed from code running anywhere in your
                   application</p>
               <p>A source code <mark>file</mark> can have <b>only one public class</b>.</p>
               
               
               <pre>
package com.mvivekweb.ocpjp6.obj1.sect1;

public class pubClass {
    
}

<mark>public</mark> class pubClass1 {
    
}
               </pre>
<pre class="out">C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>javac *.java
pubClass.java:8: error: class pubClass1 is public, should be declared in a file
named pubClass1.java
public class pubClass1 {
       ^
1 error

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>               
</pre>
               
               
        <p>If the source file contains a public class, the filename must match the
            public class name.</p>
        <p> A <mark>file</mark> can have only <mark>one package</mark> statement, but <mark>multiple imports</mark>.</p>
        <p> The <mark>package</mark> statement (if any) must be the <mark>first (non-comment) line</mark> in a
            source file.</p>
        <p> The import statements (if any) must come after the package and before
            the class declaration.</p>
        <p> If there is no package statement, import statements must be the first (noncomment)
            statements in the source file.</p>
        <p> <b>package and import statements apply to all classes in the file.</b></p>
        <p> A file can have more than one nonpublic class.</p>
        <p> <mark>Files </mark>with <mark>no public classes </mark>can have a <b>name that does not match any of the
                classes</b> in the file.</p>
         <h3>Class Access Modifiers (Objective 1.1)</h3>
         
         <p>Access control in Java is a little tricky because there are <mark>four</mark>
             <mark>access controls</mark> (levels of access) but only <mark>three access modifiers</mark>.</p>
         
         
         <p>Modifiers fall into two categories:</p>
         <br>    Access modifiers: <mark>public, protected, private</mark>.
         <br>Non-access modifiers (including <mark>strictfp, final, and abstract</mark>).

         <p> There are four <mark>access controls</mark> levels: public, protected, default, and private.</p>
         
         <p>In other words, <mark>every class, method, and instance
                 variable</mark> you declare <mark>has an access control</mark>, whether you explicitly type one or not.</p>         
         
         
         <p>Classes can have only<mark> <b>public</b> or <b>default</b></mark> access.</p>
         
         
         <h3>Class Access   </h3>
         
         <p> access <mark>means visibility</mark> </p>
         
         

         <pre>package com.mvivekweb.ocpjp6.obj1.sect1;
             
private class pubClass {
    
}
         </pre>       

<pre class="out">C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>javac *.java
pubClass.java:4: error: modifier private not allowed here
private class pubClass {
        ^
1 error

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>   </pre>      
         <p>A class with <mark>default access</mark> can be seen only by classes within the <mark>same package</mark>.</p>
         
         <p>compile package. <b>go to the root</b></p>
         <pre>package com.mvivekweb.ocpjp6.obj1.sect1.superclss;
             
public class Beverage {
    
}
         </pre>    
         
         <pre>         
package com.mvivekweb.ocpjp6.obj1.sect1.subclass;
    
import com.mvivekweb.ocpjp6.obj1.sect1.superclss.Beverage;
    
public class Tea extends Beverage{
    
}
         </pre>       

         <pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src><mark>javac com/mvivekweb/ocpjp6/obj1/sect1/subclass/
Tea.java</mark>

C:\vivek\java7\ocpjp6\ocpjp6\src>   </pre>    
         
         
         
<pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src>cd C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekw
eb\ocpjp6\obj1\sect1\subclass

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\subclass>dir
 Volume in drive C is Windows8_OS
 Volume Serial Number is 4029-6C4C

Directory of C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\subclass

10/18/2015  06:48 PM    <DIR>          .
10/18/2015  06:48 PM    <DIR>          ..
10/18/2015  06:48 PM               255 Tea.class
10/18/2015  06:42 PM               162 Tea.java
               2 File(s)            417 bytes
               2 Dir(s)  422,075,940,864 bytes free

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\subclass>cd ..

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>cd superclss

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\superclss>dir
 Volume in drive C is Windows8_OS
 Volume Serial Number is 4029-6C4C

Directory of C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\s
uperclss

10/18/2015  06:48 PM    <DIR>          .
10/18/2015  06:48 PM    <DIR>          ..
10/18/2015  06:48 PM               232 Beverage.class
10/18/2015  06:42 PM                90 Beverage.java
               2 File(s)            322 bytes
               2 Dir(s)  422,076,964,864 bytes free

               C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\superclss></pre>
         
         <p>A class with <mark>public access</mark> can be seen by all classes from <mark>all packages</mark>.</p>
         <p> Class visibility revolves around whether code in one class can
             <br> Create an instance of another class
              <br> Extend (or subclass), another class
               <br>Access methods and variables of another class</p>
         
         <h3>Default Access  </h3>
         
         <p>         Think of default access as <mark>package-level access</mark>,because a class with
default access can be seen <mark>only by classes within the same package</mark>.</p>
         
         <p>For example, if
             class A and class B are in <mark>different packages</mark>, and <mark>class A has default access</mark>, class B
             won't be able to create an instance of class A, or even declare a variable or return
             type of class A.    </p>
         
         <pre> package cert;
<mark>class Beverage</mark> { }   </pre>
         
<pre>package exam.stuff;
<mark>import cert.Beverage;</mark>
class Tea extends Beverage { }         
         </pre>
 <pre class='out'>        
Can't access class cert.Beverage. Class or interface must be
public, in same package, or an accessible member class.
import cert.Beverage;         
 </pre>       
         
         
  <img src="/imag/jp6/defaultclass.png" class="imgw">  
  
  <pre>
package com.mvivekweb.ocpjp6.obj1.sect1.superclss;
    
class Beverage {
    int i1=10;
}
  </pre>
  
  <pre>  
package com.mvivekweb.ocpjp6.obj1.sect1.superclss;
    
public class defaultclassTest <mark>extends Beverage</mark> {
    
    public static void main(String[] args) {
        Beverage bev = <mark>new Beverage();</mark>
        System.out.println("bev.i1="+<mark>bev.i1</mark>);
    }
        
}
  </pre>
 <pre class='out'> 
run:
<mark>bev.i1=10</mark>
BUILD SUCCESSFUL (total time: 0 seconds)  
</pre>
  
  
  
  <img src="/imag/jp6/publicclass.png" class="imgw">     
<img src="/imag/jp6/publicaccess.png" class="imgw">   
<img src="/imag/jp6/objectcreate.png" class="imgw">           
         
         <h3>Class Modifiers (Nonaccess) (Objective 1.1)</h3>
         <p> Classes can also be modified with final, abstract, or strictfp.</p>
         <p> A <mark>class cannot be both final and abstract</mark>.</p>
    
         <h3>strictfp class </h3>        
         
         <p><mark>strictfp</mark> is a keyword and can be used to <mark>modify a class or a method</mark>, but never a
             variable.   </p>
         
         <p>Marking a class as strictfp means that any method code in the class will
             <mark>conform to the IEEE 754 standard rules</mark> for <mark>floating points</mark>.</p>
         
         
         
         
         <pre>package com.mvivekweb.ocpjp6.obj1.sect1;
             
<mark>final abstract</mark> class pubClass {
    
} </pre>        
<pre class="out">C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>javac *.java
pubClass.java:4: error: illegal combination of modifiers: abstract and final
final abstract class pubClass {
               ^
               1 error    </pre> 
         
<h3>Final Classes</h3>    


<img src="/imag/jp6/finalclass.png" class="imgw"> 






         
         <p> A final class cannot be subclassed.<b>can be instantiated</b></p>
         
         <pre>
package com.mvivekweb.ocpjp6.obj1.sect1;
    
<mark>final</mark>  class pubClass {
    
}
    
class subclss <mark>extends</mark> pubClass{
    
}
    
class subclass1 {
    pubClass newinsatnce = <mark>new</mark> pubClass();
} 
         </pre>  
         

<pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>javac *.java
pubClass.java:8: error: cannot inherit from final pubClass
class subclss extends pubClass{
                      ^
1 error

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>  </pre>   
         
         <p><mark>String</mark> class <mark>cannot be subclassed</mark></p>
         
         <p>Marking a class final means, in essence, your class <mark>can't ever be
                 improved upon </mark>  </p>   
         
         
         <p>A <mark>benefit of having nonfinal</mark> classes is this scenario: Imagine you find a problem
             with a method in a class you're using, but <mark>you don't have the source code</mark>. So you
             can't modify the source to improve the method, but you can extend the class and
             override the method in your new subclass, and <mark>substitute the subclass everywhere
             the original superclass is expected</mark>.</p>

<p>In practice, you'll almost never make a final class</p>
             
<p>A final class <mark>obliterates</mark> a key
    benefit of OO-<mark>extensibility</mark>.  </p>           
         
<h3>Abstract Classes </h3>

<img src="/imag/jp6/abstractclass.png" class="imgw">          
         <p> An abstract class cannot be instantiated.<b>can be subclassed</b></p>
         
         <pre>         
package com.mvivekweb.ocpjp6.obj1.sect1;
    
<mark>abstract</mark>  class pubClass {
    
}
    
class subclss <mark>extends</mark> pubClass{
    
}
    
class subclass1 {
    pubClass newinsatnce = <mark>new </mark> pubClass();
} 
         </pre>      

<pre class='out'>C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>javac *.java
pubClass.java:13: error: pubClass is abstract; cannot be instantiated
    pubClass newinsatnce = new pubClass();
                           ^
1 error

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1>  </pre>       
         <p> A single abstract method in a class means the whole class must be abstract.</p>
         
         <pre>
package com.mvivekweb.ocpjp6.obj1.sect1;
    
    
abstract class abstractclass {
    abstract void goFast()<mark>;</mark>
}  </pre>
         
         <pre>         
package com.mvivekweb.ocpjp6.obj1.sect1.abstractclass1;
    
public class abstractTest <mark>extends Car</mark> {
    
    public static void main(String[] args) {
    }
        
    <mark>public void goFast() {
    }
        
    public void goUpHill() {
    }
        
    public void impressNeighbors() {
    }</mark>
        
}
    
abstract class Car {
    
    private double price;
    private String model;
    private String year;
        
    public abstract void goFast();
        
    public abstract void goUpHill();
        
    public abstract void impressNeighbors();
// Additional, important, and serious code goes here
}
         </pre>
         
         
         
         <pre>
package com.mvivekweb.ocpjp6.obj1.sect1;
    
    
abstract class abstractclass {
    abstract String goFast();
}         
         </pre>       
         <p>   methods marked abstract <b>end in a semicolon</b> rather than
             curly braces.</p>
         <p> An abstract class can have <mark>both abstract and nonabstract</mark> methods.</p>
         <p> Coding with <b>abstract</b> class types (including <b>interfaces</b>, discussed later in this
             chapter) lets you take <b>advantage of polymorphism</b>, and gives you the greatest degree
             of <b>flexibility</b> and <b>extensibility</b></p>
         <p> The first concrete class to extend an abstract class must implement all of its
             abstract methods.</p>
         
<p>You might get a question that
             asks how you could fix a code sample that includes a method ending in a semicolon </p>        
         
         
         
         
         <h3>Nested Classes</h3>
         
         <p>Nested classes are divided into two categories: <mark>static and non-static</mark>. Nested classes that are declared static are simply called static nested classes. <mark>Non-static</mark> nested classes are called <mark>inner classes</mark>. </p>
         
         <p> code that's <mark>easier to read and maintain</mark></p>
         
         <p>"<mark>reuseless</mark>": code that's useless over and over again.</p>
         
         <p> Just
             as classes have member variables and methods, a class can also have member classes.</p>
         
         <h3>Why Use Nested Classes?</h3>
         
         <p><b>Logical grouping of classes</b>-If a class is useful to only one other class, then it is logical to embed it in that class and keep the two together. Nesting such "<mark>helper classes</mark>" makes their package more streamlined.</p>
         
         <p><b>Increased encapsulation</b>-Consider two top-level classes, A and B, where B needs access to members of A that would otherwise be declared private. By hiding class B within class A, <mark>A's members can be declared private and B can access them</mark>. In addition, B itself can be hidden from the outside world.          </p>

         <p><b>More readable, maintainable code</b>-<mark>Nesting small classes</mark> within top-level classes places the code closer to where it is used.</p>

         
         
         
         <h3> Inner Classes</h3>
         
         <p> the inner class is a part of the outer class   </p>   
         
         <p>Yes, an
             inner class <mark>instance</mark> has <mark>access to all members of the outer class</mark>, <mark>even</mark> those marked
             <mark>private</mark>. because the inner
class is also a member of the outer class.</p>        

         <h3>Coding a "Regular" Inner Class </h3>
         
         <pre>        
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
     
public class MyOuter {
    <mark>class MyInner { }</mark>
}        
         </pre>
         
         <p> you'll end up with two class files: </p>
<pre class='out'>         
MyOuter.class
<mark>MyOuter$MyInner.class  </mark>       
</pre>
         
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\innerclass>javac -source 1.6 MyOuter.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\innerclass><mark>java MyOuter$MyInner</mark>
Error: Could not find or load main class MyOuter$MyInner

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj1\sect1\innerclass>         
</pre>     
         

         <p>The only way you can <mark>access the inner class</mark>
             is through a <mark>live instance of the outer class</mark></p>


         <h3>Instantiating an Inner Class</h3>
         
         <p>To create an instance of an inner class, you <mark>must have an instance of the outer class</mark>
             to tie to the inner class </p>        


         <h3>Instantiating an Inner Class from Within the Outer Class</h3>
         
         <p>it
             is the outer class that creates instances of the inner class, since it is <mark>usually the outer
                 class wanting to use the inner instance as a helper</mark> for its own personal use   </p>      
         <pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter {
    
    <mark>private</mark> int x = 7;
        
    public void makeInner() {
        <mark>MyInner in = new MyInner();</mark> // make an inner instance
       <mark> in.seeOuter();</mark>
    }
        
    class MyInner {
        
        public void seeOuter() {
            System.out.println("Outer x is " + <mark>x</mark>);
        }
            
    }
}         
         </pre>
         <pre>        
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class TestInner {
    
    public static void main(String[] args) {
        MyOuter outer =new MyOuter();
        outer.makeInner();
    }
        
}
         </pre>
 <pre class='out'>        
run:
Outer x is 7
BUILD SUCCESSFUL (total time: 1 second)         
</pre>      

         <p>MyOuter code <mark>treats MyInner</mark> just as
             though MyInner were <mark>any other accessible class</mark>  </p>   
         
         
<img src="/imag/jp6/innerwithin.png" class="imgw">         
         
         <h3> Creating an Inner Class Object from Outside the Outer Class
             Instance Code  </h3>  
         <pre>       
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
     
public class MyOuter {
    
    private int x = 7;
        
    public void makeInner() {
        MyInner in = new MyInner(); // make an inner instance
        in.seeOuter();
    }
        
    class MyInner {
        
        public void seeOuter() {
            System.out.println("Outer x is " + x);
        }
            
    }
}  </pre>
         
<pre >         
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;

public class TestInner {

    public static void main(String[] args) {
        MyOuter outer =new MyOuter();
        <mark>MyOuter.MyInner</mark> inner = <mark>outer.new MyInner()</mark>;
        inner.seeOuter();
        
        MyOuter.MyInner inner1 = <mark>new MyOuter().new MyInner()</mark>;
        inner1.seeOuter();
        
        
    }

}
</pre>   
  <pre class='out'>       
run:
Outer x is 7
Outer x is 7
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>   

<img src="/imag/jp6/inneroutsidea.png" class="imgw">  

<img src="/imag/jp6/inneroutsideb.png" class="imgw">  

<h3>Referencing the Inner or Outer Instance
    from Within the Inner Class</h3>

<p>an <mark>object refer to itself</mark> normally? By using the <mark>this</mark> reference</p>

<p>The keyword this <mark>can be used only from within instance code</mark>.
    In other words, <mark>not within static code</mark>.</p>

<p>The this reference is a reference to the currently executing object</p>

<p>The this reference is the way an object can pass a reference to itself to some
    other code, as a method argument:</p>

<pre>
public void myMethod() {
MyClass mc = new MyClass();
mc.doStuff(this); // pass a ref to object running myMethod
}
</pre>

<p>Within an inner class code, the <mark>this</mark> reference refers to the <mark>instance of the inner</mark>
    class</p>

<p>normally the inner class code doesn't need a reference to
    the outer class</p>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter {
    
    private int x = 7;
        
    public void makeInner() {
        MyInner in = new MyInner(); // make an inner instance
        in.seeOuter();
    }
        
    class MyInner {
        
        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + <mark>this</mark>);
            System.out.println("Outer class ref is " + <mark>MyOuter.this</mark>);
        }
            
    }
}
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class TestInner {
    
    public static void main(String[] args) {
        MyOuter outer =new MyOuter();
        outer.makeInner();
        MyOuter.MyInner inner = outer.new MyInner();
        inner.seeOuter();
            
        MyOuter.MyInner inner1 = new MyOuter().new MyInner();
        inner1.seeOuter();
            
            
    }
        
}
    
</pre>
<pre class='out'>
run:
Outer x is 7
Inner class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.<mark>MyOuter$MyInner@15db9742</mark>
Outer class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.<mark>MyOuter@6d06d69c</mark>
Outer x is 7
Inner class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.MyOuter$MyInner@7852e922
Outer class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.MyOuter@6d06d69c
Outer x is 7
Inner class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.<mark>MyOuter$MyInner@4e25154f</mark>
Outer class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.<mark>MyOuter@70dea4e</mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<p>To reference the <mark>"outer this"</mark> (the outer class instance) from within the inner
    class code, use <mark>NameOfOuterClass.this</mark> (example, MyOuter.this).</p>


<p>A regular inner class is a
    member of the outer class just as instance variables and methods are, so the
    following modifiers can be applied to an inner class:
</p>

final
<br>abstract
<br>public
<br>private
<br>protected
<br>static-but static turns it into a static nested class not an inner class
<br>strictfp

<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter {
    
    private int x = 7;
        
    public void makeInner() {
        MyInner in = new MyInner(); // make an inner instance
        in.seeOuter();
    }
        
    <mark>private</mark> class MyInner {
        
        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + this);
            System.out.println("Outer class ref is " + MyOuter.this);
        }
            
    }
}</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class TestInner {
    
    public static void main(String[] args) {
<mark>        MyOuter outer =new MyOuter();
        outer.makeInner();// works</mark>
        MyOuter.MyInner inner = outer.new MyInner();
        inner.seeOuter();
            
        MyOuter.MyInner inner1 = new MyOuter().new MyInner();
        inner1.seeOuter();
            
            
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj1/sect1/innerclass/MyOuter.java

C:\vivek\java7\ocpjp6\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj1/sect1/innerclass/TestInner.java
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\TestInner.java:8: error: MyOuter.MyInner has private access in MyOuter
        MyOuter.MyInner inner = outer.new MyInner();
               ^
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\TestInner.java:8: error: MyOuter.MyInner has private access in MyOuter
        MyOuter.MyInner inner = outer.new MyInner();
                                          ^
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\TestInner.java:11: error: MyOuter.MyInner has private access in MyOuter
        MyOuter.MyInner inner1 = new MyOuter().new MyInner();
               ^
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\TestInner.java:11: error: MyOuter.MyInner has private access in MyOuter
        MyOuter.MyInner inner1 = new MyOuter().new MyInner();
                                                   ^
4 errors

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter {
    
    private int x = 7;
        
    public void makeInner() {
        MyInner in = new MyInner(); // make an inner instance
        in.seeOuter();
    }
        
    private <mark>abstract</mark> class MyInner {
        
        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + this);
            System.out.println("Outer class ref is " + MyOuter.this);
        }
            
    }
}</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect1/innerclass/MyOuter.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\MyOuter.java:8: error: MyOuter.MyInner is abstract; cannot be instantiated
        MyInner in = new MyInner(); // make an inner instance
                     ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<h3>Method-Local Inner Classes</h3>

<p>define an <mark>inner class within a method</mark></p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter2 {
    private String x = "Outer2";
    void doStuff() {
        class MyInner {
            public void seeOuter() {
                System.out.println("Outer x is " + x);
            } // close inner class method
        } // close inner class definition
    } // close outer class method doStuff()
}
    
</pre>

<p>The code above is completely <mark>useless</mark>,</p>

<p>you must make an instance
    of it somewhere within the method but below the inner class definition</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter2 {
    
    private String x = "Outer2";
        
    void doStuff() {
        class MyInner {
            
            public void seeOuter() {
                System.out.println("Outer x is " + x);
            } // close inner class method
        } // close inner class definition
       <mark> MyInner mi = new MyInner(); // This line must come
// after the class
        mi.seeOuter();</mark>
    } // close outer class method doStuff()
}
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;

public class TestMethodlocal {
    public static void main(String[] args) {
                MyOuter2 outer =new MyOuter2();
                outer.doStuff();
    }
}
</pre>
<pre class='out'>
run:
Outer x is Outer2
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<h3>What a Method-Local Inner Object Can and Can't Do</h3>

<p>can access its private (or
    any other) members of outer class</p>


<p>the inner class object <mark>cannot use the local variables</mark>
    of the method the inner class is in</p>

<p>The <mark>local variables</mark> of the method <mark>live on the stack</mark>, and exist only for
    the lifetime of the method</p>

<p>But even after the method
    completes, <mark>the inner class object created within it might still be alive on the heap</mark> if,
    for example, a reference to it was passed into some other code and then stored in an
    instance variable.</p>

<p><mark>Unless</mark>
    the local variables are <mark>marked final</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter2 {
    
    private String x = "Outer2";
        
    void doStuff() {
        <mark>String z = "local variable";</mark>
        class MyInner {
            
            public void seeOuter() {
                System.out.println("Outer x is " + x);
                <mark>System.out.println("Local variable z is " + z); // Won't Compile!</mark>
            } // close inner class method
        } // close inner class definition
        MyInner mi = new MyInner(); // This line must come
// after the class
        mi.seeOuter();
    } // close outer class method doStuff()
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect1/innerclass/MyOuter2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\MyOuter2.java:13: error: local variable z is accessed from within inner class; needs to be declared final
                System.out.println("Local variable z is " + z); // Won't Compile!
                                                            ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter2 {
    
    private String x = "Outer2";
        
    void doStuff() {
        <mark>final</mark> String z = "local variable";
        class MyInner {
            
            public void seeOuter() {
                System.out.println("Outer x is " + x);
                System.out.println("Local variable z is " + z); // Won't Compile!
            } // close inner class method
        } // close inner class definition
        MyInner mi = new MyInner(); // This line must come
// after the class
        mi.seeOuter();
    } // close outer class method doStuff()
}
    
</pre>
<pre class='out'>
run:
Outer x is Outer2
Local variable z is local variable
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>the only modifiers you can apply to a
    method-local inner class are <mark>abstract and final</mark>, but as always, <mark>never both at the
    same time</mark>.</p>
    
    <p>that a <mark>local class declared in a static method</mark> has access
        to <mark>only static members of the enclosing class</mark>, since there is no associated instance of the
        enclosing class   </p>
    <pre>    
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyOuter2 {
    
    private <mark>static</mark> String x = "Outer2";
        
    <mark>static</mark> void doStuff() {
        final String z = "local variable";
        class MyInner {
            
            public void seeOuter() {
                System.out.println("Outer x is " + x);
                System.out.println("Local variable z is " + z); 
            } // close inner class method
        } // close inner class definition
        MyInner mi = new MyInner(); // This line must come
// after the class
        mi.seeOuter();
    } // close outer class method doStuff()
}    </pre>
  <pre class='out'>  
    run:
Outer x is Outer2
Local variable z is local variable
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
    
    <pre>   
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
     
public class MyOuter2 {
    
    private <mark>static</mark> String x = "Outer2";
        
     <mark>static</mark> void doStuff() {
        final String z = "local variable";
            
        class MyInner {
            
            public void seeOuter() {
                System.out.println("Outer x is " + x);
                System.out.println("Local variable z is " + z); // Won't Compile!
                <mark>System.out.println("Inner class ref is " + this);</mark>
                    
            //System.out.println("Outer class ref is " + MyOuter2.this);
            } // close inner class method
        } // close inner class definition
        MyInner mi = new MyInner(); // This line must come
// after the class
        mi.seeOuter();
    } // close outer class method doStuff()
}
    </pre>
    
 <pre class='out'>   
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;

public class TestMethodlocal {
    public static void main(String[] args) {
                MyOuter2 outer =new MyOuter2();
                outer.doStuff();
    }
    }   </pre>
    
 <pre class='out'>   
run:
Outer x is Outer2
Local variable z is local variable
Inner class ref is com.mvivekweb.ocpjp6.obj1.sect1.innerclass.MyOuter2$1MyInner@15db9742
BUILD SUCCESSFUL (total time: 0 seconds)    
</pre>
    <p>    
        If you're in a static method there is no this, so an inner class in a static
        method is subject to the <mark>same restrictions as the static method</mark>. In other words, no access
        to instance variables.    <mark>//contradiction</mark>
    </p>
    
    
    <img src="/imag/jp6/methodlocal.png" class="imgw"> 
    
    
    <h3>Anonymous Inner Classes    </h3>
    
    <p>inner classes declared <mark>without
            any class name</mark> at all (hence the word anonymous)</p>
    
    
    <p>can define these classes <mark>not just within a method</mark>, but <mark>even within an argument
        to a method</mark>.   </p>
    
    <h3>Plain-Old Anonymous Inner Classes, Flavor One  </h3> 
    
    <pre>   
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
     
public class Popcorn {
    
    public void pop() {
        System.out.println("popcorn");
    }
}
    
class Food {
    
    Popcorn p = new <mark>Popcorn</mark>() <mark>{</mark>
        public void pop() {
            System.out.println("anonymous popcorn");
        }
    <mark>};</mark>
}
    </pre>
    
    <p> Food has <mark>one instance variable p</mark>, declared as type Popcorn. That's it for Food.
        Food has no methods. </p>  
    
    <p> The Popcorn reference variable<mark> refers not</mark> to an instance of Popcorn, but to <mark>an
            instance of an anonymous</mark> (unnamed) <mark>subclass</mark> of Popcorn.   </p>
    
    <p>  declare a new class that
        has <mark>no name</mark>, but that is a <mark>subclass</mark> of Popcorn </p>
    
    
    <p>whole point of making an anonymous inner class-to<mark> override
            one or more methods of the superclass</mark>   </p> 
    
 
    <p>includes a curly brace
        closing off the anonymous class definition. the<mark> semicolon</mark> that ends the statement</p>
    
    
    <p>Polymorphism is in play when anonymous inner classes are involved</p>
    
    <p>You can
        <mark>only call methods</mark> on an anonymous inner class <mark>reference that are defined in the
            reference variable type</mark> </p>
    
            <pre>   
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class PolymorphicTest {
    public static void main(String[] args) {
        Animal h = new Horse();
        h.eat(); // Legal, class <mark>Animal has an eat() method</mark>
        h.buck(); // Not legal! Class <mark>Animal doesn't have buck()</mark>
    }
}
    
class Horse extends Animal {
    void buck() {
    }
}
    
class Animal {
    void eat() {
    }
}    
            </pre>
 <pre class='out'>           
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - Erroneous sym type: com.mvivekweb.ocpjp6.obj1.sect1.innerclass.Animal.buck
	at com.mvivekweb.ocpjp6.obj1.sect1.innerclass.PolymorphicTest.main(PolymorphicTest.java:7)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>      

            <p>the real issue is how do you invoke
                that new method </p>   
            <pre>           
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class Popcorn {
    
    public void pop() {
        System.out.println("popcorn");
    }
}
    
class Food {
    
    Popcorn p = new Popcorn() {
        public void pop() {
            System.out.println("anonymous popcorn");
        }
            
        public void sizzle() {
            System.out.println("anonymous sizzling popcorn");
        }
    };
        
    public void popIt() {
        p.pop(); // OK, <mark>Popcorn has a pop()</mark> method
        //p.sizzle(); // Not Legal! Popcorn does not have sizzle()
    }
}            
            </pre> 
            
<pre >
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;

public class TestAnonymus {


    public static void main(String[] args) {
        Food food= new Food();
        food.popIt();
    }

}
</pre>  
 <pre class='out'>           
 run:
anonymous popcorn
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>  
            <pre>           
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class Popcorn {
    
    public void pop() {
        System.out.println("popcorn");
    }
}
    
class Food {
    
    Popcorn p = new Popcorn() {
        public void pop() {
            System.out.println("anonymous popcorn");
        }
            
        public void sizzle() {
            System.out.println("anonymous sizzling popcorn");
        }
    };
        
    public void popIt() {
        p.pop(); // OK, Popcorn has a pop() method
        <mark>p.sizzle();</mark> // Not Legal! <mark>Popcorn does not have sizzle()</mark>
    }
}     </pre>   
            
 <pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect1/innerclass/Popcorn.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect1\innerclass\Popcorn.java:24: error: cannot find symbol
        p.sizzle(); // Not Legal! Popcorn does not have sizzle()
         ^
  symbol:   method sizzle()
  location: variable p of type Popcorn
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>     </pre>    
            
            
            <h3>Plain-Old Anonymous Inner Classes, Flavor Two    </h3>        
            
            <p>creates an
                anonymous implementer of the specified interface type   </p>         
            <p>           
                the new
                <mark>anonymous class</mark> would be an <mark>implementer of the interface</mark> rather than a subclass of
                the class.           
            </p>
            
            <pre>
  package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
      
public interface Cookable {
    public void cook();
}
    
class Food1 {
    Cookable c = new Cookable() {
        public void cook() {
            System.out.println("anonymous cookable implementer");
        }
    };
        
    public void callmeth(){
        c.cook();// <mark>cookable has cook</mark>
    }
}          
            </pre>
            <pre>           
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class TestAnonymousinterface {
    public static void main(String[] args) {
        Food1 food= new Food1();
        food.callmeth();
    }
}
            </pre>

<pre class='out'>
run:
anonymous cookable implementer
BUILD SUCCESSFUL (total time: 0 seconds)            
            
</pre>     
            <p>            
                it's <mark>not instantiating a Cookable</mark> object, it's <mark>creating an
                    instance of a new</mark>, anonymous, implementer of Cookable.    </p>      
            
            <p>they
                can <mark>implement only one interface</mark> </p>   
            
            <p>anonymous inner class <mark>can't even extend a class</mark> and <mark>implement an interface</mark> at the
                same time.  </p> 
            
            <h3>Argument-Defined Anonymous Inner Classes  </h3>   
            
            <p>you simply <mark>define an
                    anonymous inner class, right inside the argument</mark>      </p>
            
            <pre>            
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class MyWonderfulClass {
    
    void go() {
        Bar b = new Bar();
        b.doStuff(<mark>new Foo() {
            public void foof() {
                System.out.println("foofy");
            }
                
        }</mark>); 
}
}
interface Foo {
    void foof();
}
    
class Bar {
    void doStuff(<mark>Foo f</mark>) {
        f.<mark>foof()</mark>;
    }
}            
            </pre>      
            
            
            <p>we implement the <mark>foof() method</mark> </p>           
            
            <h3>Static Nested Classes</h3>

            <p>It is simply a <mark>non-inner</mark> (also called "top-level") class
                scoped within another.   </p>
            
            <p>a class that's a <mark>static member</mark> of the <mark>enclosing(outer)</mark> class </p>
            
            
            <p> The class itself isn't really "static"; there's <mark>no such thing as a static class</mark> </p>
            
            
            <p>That means it <mark>can be accessed</mark>, as with other static members, <mark>without having
                an instance of the outer class</mark>.   </p>
            
            
            <h3>  Instantiating and Using Static Nested Classes </h3>
            <pre>        
 package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
     
public class Broom {
    
    public static void main(String[] args) {
        BigOuter.Nest n = new BigOuter.<mark>Nest()</mark>; // both class names
        n.go();
            
    }
        
}
    
class BigOuter {
    
    static class Nest {
        
        void go() {
            System.out.println("hi");
        }
    }
}           
            </pre>
  <pre class='out'>          
 run:
hi
BUILD SUCCESSFUL (total time: 0 seconds)           
            
</pre>      
            
            <pre>            
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class Broom1 {
    static class B2 {
        void goB2() {
            System.out.println("hi 2");
        }
    }
        
    public static void main(String[] args) {
       <mark>B2 b= new B2(); // static class needs instantiating if in outer class </mark>
       b.goB2();
    }
        
}            
            </pre>
  <pre class='out'>          
 run:
hi 2
BUILD SUCCESSFUL (total time: 0 seconds)           
</pre>   
            <pre>            
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class Broom1 {
    <mark>static</mark> int x=7;
    <mark>static</mark> class B2 {
        void goB2() {
            System.out.println("hi 2");
            System.out.println("x="+x);
        }
    }
        
    public static void main(String[] args) {
       B2 b= new B2();
       b.goB2();
    }
        
}            
            </pre>
  <pre class='out'>          
run:
hi 2
x=7
BUILD SUCCESSFUL (total time: 0 seconds)     </pre>      
            
            <p> a static nested class<mark> does not have access to the instance</mark>
                variables and <mark>nonstatic methods</mark> of the outer class      </p>     
            <pre>           
package com.mvivekweb.ocpjp6.obj1.sect1.innerclass;
    
public class Broom1 {
    static int x=7;
    static class B2 {
        void goB2() {
            System.out.println("hi 2");
            System.out.println("x="+x);
            callme();
        }
            
            
    }
            <mark>static</mark> void callme() {
                System.out.println("hello");
        }
            
    public static void main(String[] args) {
       B2 b= new B2();
       b.goB2();
    }
        
}
            </pre>
 <pre class='out'>           
run:
hi 2
x=7
hello
BUILD SUCCESSFUL (total time: 0 seconds)            
</pre>    














            