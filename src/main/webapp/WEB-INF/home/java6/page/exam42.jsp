<pre>package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Stroll {
    
    public static void main(String[] args) {
Thread t1 = new Thread(new Mosey());
t1.setPriority(1);
<mark>new Mosey().run();</mark>
t1.start();
    }
        
}
class Mosey implements Runnable {
 public void run() {
 for(int i = 0; i < 1000; i++) {
 System.out.print(Thread.currentThread().getName() + "-" + i + " "+"\n");
 } } }
</pre>
<pre class='out'>
run:
main-0 
main-1 
main-2 
main-3 
main-4 
main-5 
main-6 
main-7 
.....
......
Thread-0-992 
Thread-0-993 
Thread-0-994 
Thread-0-995 
Thread-0-996 
Thread-0-997 
Thread-0-998 
<mark>Thread-0-999 </mark>
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Stroll {
    
    public static void main(String[] args) {
Thread t1 = new Thread(new Mosey());
t1.setPriority(9);
new Mosey().run();
t1.start();
    }
        
}
class Mosey implements Runnable {
 public void run() {
 for(int i = 0; i < 1000; i++) {
 System.out.print(Thread.currentThread().getName() + "-" + i + " "+"\n");
 } } }</pre>

<pre class='out'>
Thread-0-988 
Thread-0-989 
Thread-0-990 
Thread-0-991 
Thread-0-992 
Thread-0-993 
Thread-0-994 
Thread-0-995 
Thread-0-996 
Thread-0-997 
Thread-0-998 
<mark>Thread-0-999 </mark>
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Stroll {
    
    public static void main(String[] args) {
Thread t1 = new Thread(new Mosey());
<mark>t1.setPriority(1);</mark>
t1.start();
<mark>new Mosey().run();</mark>
    }
        
}
class Mosey implements Runnable {
 public void run() {
 for(int i = 0; i < 1000; i++) {
 System.out.print(Thread.currentThread().getName() + "-" + i + " "+"\n");
 } } }
     
</pre>
<pre class='out'>
Thread-0-988 
Thread-0-989 
Thread-0-990 
Thread-0-991 
Thread-0-992 
Thread-0-993 
Thread-0-994 
Thread-0-995 
Thread-0-996 
Thread-0-997 
Thread-0-998 
<mark>Thread-0-999 </mark>
BUILD SUCCESSFUL (total time: 0 seconds)


</pre>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Stroll {
    
    public static void main(String[] args) {
Thread t1 = new Thread(new Mosey());
<mark>t1.setPriority(8);</mark>
<mark>t1.start();</mark>
new Mosey().run();
    }
        
}
class Mosey implements Runnable {
 public void run() {
 for(int i = 0; i < 1000; i++) {
 System.out.print(Thread.currentThread().getName() + "-" + i + " "+"\n");
 } } }</pre>
<pre class='out'>
main-987 
main-988 
main-989 
main-990 
main-991 
main-992 
main-993 
main-994 
main-995 
main-996 
main-997 
main-998 
<mark>main-999 </mark>
BUILD SUCCESSFUL (total time: 0 seconds)</pre>


<p>will probably <mark>run
        most (or all) of the main thread's run() method invocation before</mark> running most of the t1
    thread's run() method invocation?</p>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Salmon extends Thread{
public static long id;
public void run() {
 for(int i = 0; i < 4; i++) {
 // insert code here
  System.out.print("id="+ id + "\n");
  System.out.print("Thread.currentThread().getId()="+ Thread.currentThread().getId() + "\n");
 if(i == 2 && id == Thread.currentThread().getId()) {
 new Thread(new Salmon()).start();
 throw new Error();
 }
 System.out.print("i="+ i + " \n");
 } }
    public static void main(String[] args) {
Thread t1 = new Salmon();
 id = t1.getId();
 t1.start();
    }
        
}
</pre>
<pre class='out'>
run:
id=10
Thread.currentThread().getId()=10
i=0 
id=10
Exception in thread "Thread-0" java.lang.Error
Thread.currentThread().getId()=10
i=1 
id=10
Thread.currentThread().getId()=10
id=10
Thread.currentThread().getId()=13
	at com.mvivekweb.ocpjp6.obj4.sect2.exam.Salmon.run(i=0 
Salmon.java:12)
id=10
Thread.currentThread().getId()=13
i=1 
id=10
Thread.currentThread().getId()=13
i=2 
id=10
Thread.currentThread().getId()=13
i=3 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Bang extends Thread{
static Thread t1, t2, t3;
    
    public static void main(String[] args) {
        t1 = new Thread(new Bang());
 t2 = new Thread(new Bang());
 t3 = new Thread(new Bang());
 t1.start(); t2.start(); t3.start();
    }
public void run() {
 for(int i = 0; i < 500; i++) {
 System.out.print(Thread.currentThread().getId() + " \n");
 if(i == 250)
 try {
 System.out.print("**" + t1.getId() + "**\n");
 t1.sleep(600);
 }
 catch (Exception e) { }
 } }
}
</pre>
<pre class='out'>
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
15 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>Bang will execute for a second or two.</p>

<p>It's difficult to predict which thread will be the last to finish.</p>

<p><mark>sleep will be called once on each of the three threads</mark>, not on Thread t1 three
    times. Remember, the sleep() method is static and operates on whichever thread is
    the currently running thread.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Jiggy extends Thread{
Jiggy(String n) { super(n); }
 public void run() {
for(int i = 0; i < 100; i++) {
 if("t1".equals(Thread.currentThread().getName()) && i == 5) {
 new Jiggy("t3").start();
 throw new Error();
 }
 if("t2".equals(Thread.currentThread().getName()) && i == 5) {
 new Jiggy("t4").start();
 throw new Error();
 }
 System.out.print(Thread.currentThread().getName() + "-");
 }
 }
     
    public static void main(String[] args) {
        Thread t1 = new Jiggy("t1");
 Thread t2 = new Jiggy("t2");
 t1.setPriority(1); t2.setPriority(9);
 t2.start(); t1.start();
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "t2" java.lang.Error
	at com.mvivekweb.ocpjp6.obj4.sect2.exam.Jiggy.run(Jiggy.java:16)
Exception in thread "t1" java.lang.Error
	at com.mvivekweb.ocpjp6.obj4.sect2.exam.Jiggy.run(Jiggy.java:12)
t2-t2-t2-t2-t2-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t4-t1-t1-t1-t1-t1-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-t3-BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>After throwing error(s), t4 will most likely complete before t3.</p>

<p>The errors will stop t1 and t2, but
    t3 and t4 will continue. The thread t4 will complete before t3 because it more or less
    "inherits" its priority from the thread that created it, t2.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Zingseng extends Thread{
    
    
    public static void main(String[] args) {
        Thread t1 = new Thread(new Zingseng());
 t1.start();
 // insert code here
 for(int i = 0; i < 1000; i++) // Loop #1
 System.out.print(Thread.currentThread().getName() + " \n");
    }
public void run() {
 for(int i = 0; i < 1000; i++) // Loop #2
 System.out.print(Thread.currentThread().getName() + " \n");
 }
}
</pre>
<pre class='out'>
Thread-1 
main 
Thread-1 
main 
Thread-1 
main 
Thread-1 
Thread-1 
Thread-1 
Thread-1 
Thread-1 
Thread-1 
Thread-1 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>
<pre class='out'>
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
public class Zingseng extends Thread{
    
    
    public static void main(String[] args) <mark>throws Exception</mark> {
        Thread t1 = new Thread(new Zingseng());
 t1.start();
<mark> t1.join();</mark>
 // insert code here
 for(int i = 0; i < 1000; i++) // Loop #1
 System.out.print(Thread.currentThread().getName() + " \n");
    }
public void run() {
 for(int i = 0; i < 1000; i++) // Loop #2
 System.out.print(Thread.currentThread().getName() + " \n");
 }
}
</pre>
<pre class='out'>
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
main 
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Zingseng extends Thread{
    
    
    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread(new Zingseng());
 t1.start();
t1.sleep(1000);
 // insert code here
 for(int i = 0; i < 1000; i++) // Loop #1
 System.out.print(Thread.currentThread().getName() + "2 \n");
    }
public void run() {
 for(int i = 0; i < 1000; i++) // Loop #2
 System.out.print(Thread.currentThread().getName() + "1 \n");
 }
}
</pre>
<pre class='out'>
main2 
main2 
main2 
main2 
main2 
main2 
main2 
main2 
main2 
main2 
main2 
main2 
BUILD SUCCESSFUL (total time: 1 second)</pre>


<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Drip extends Thread{
    
    
    public static void main(String[] args) {
        Thread t1 = new Thread(new Drip());
 t1.start();
<mark> t1.join();</mark>
 for(int i = 0; i < 1000; i++) // Loop #1
 System.out.print(Thread.currentThread().getName() + " ");
    }
public void run() {
 for(int i = 0; i < 1000; i++) // Loop #2
 System.out.print(Thread.currentThread().getName() + " ");
 }
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj4/sect2/exam/Drip.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj4\sect2\exam\Drip.java:12: error: <mark>unreported exception InterruptedException; must be caught or declared to be thrown</mark>
 t1.join();
        ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
    
public class Skip {
    
    
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Jump());
Thread t2 = new Thread(new Jump());
 t1.start(); t2.start();
 t1.join(500);
new Jump().run();
    }
}
class Jump implements Runnable {
public void run() {
 for(int i = 0; i < 5; i++) {
 try { Thread.sleep(200); }
 catch (Exception e) { System.out.print("e "); }
 System.out.print(Thread.currentThread().getName() + "-" + i + " \n");
 } }
}
</pre>
<pre class='out'>
run:
Thread-1-0 
Thread-0-0 
Thread-1-1 
Thread-0-1 
Thread-1-2 
Thread-0-2 
main-0 
Thread-1-3 
Thread-0-3 
main-1 
Thread-1-4 
Thread-0-4 
main-2 
main-3 
main-4 
BUILD SUCCESSFUL (total time: 1 second)
</pre>

<p>The main thread's execution will overlap with t1 and t2's execution.</p>

<p>The key to this question is join(500). This means that the main thread will
    try to join to the end of t1, but it will only wait 500 milliseconds for t1 to complete. If t1
    doesn't complete (and it will take at least 1000 milliseconds for t1 to complete), the main
    thread will start after waiting 500 milliseconds.</p>

<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Cult extends Thread{
static int count = 0;
 public void run() {
 for(int i = 0; i < 100; i++) {
 if(i == 5 && count < 3) {
 Thread t = new Cult(names[count++]);
 t.start();
 // insert code here
 }
 System.out.print(Thread.currentThread().getName() + " \n");
 }
 }
    public static void main(String[] args) {
new Cult("t0").start();
    }
Cult(String s) { super(s); }
 String[] names = {"t1", "t2", "t3"};
}
    
    
</pre>
<pre class=''out'>
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
t3 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Cult extends Thread{
static int count = 0;
 public void run() {
 for(int i = 0; i < 100; i++) {
 if(i == 5 && count < 3) {
 Thread t = new Cult(names[count++]);
 t.start();
<mark> try { t.join(); } catch(Exception e) { }</mark>
 }
 System.out.print(Thread.currentThread().getName() + " \n");
 }
 }
    public static void main(String[] args) {
new Cult("t0").start();
    }
Cult(String s) { super(s); }
 String[] names = {"t1", "t2", "t3"};
}
</pre>
<pre class='out'>
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
t0 
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p>With fragment I, t0 joins to t1, which joins to t2, which joins to
    t3. Next, t3 completes and returns to t2, which completes and returns to t1, which
    completes and returns to t0.</p>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Cult extends Thread{
static int count = 0;
 public void run() {
 for(int i = 0; i < 100; i++) {
 if(i == 5 && count < 3) {
 Thread t = new Cult(names[count++]);
 t.start();
<mark>try { Thread.currentThread().join(); } catch(Exception e) { }</mark>
 }
 System.out.print(Thread.currentThread().getName() + " \n");
 }
 }
    public static void main(String[] args) {
new Cult("t0").start();
    }
Cult(String s) { super(s); }
 String[] names = {"t1", "t2", "t3"};
}
    
</pre>

<p>With fragment II, t0 starts t1 and then joins to itself. Then,
    t1 starts t2 and joins to itself. Then, t2 starts t3 and joins to itself. Finally, t3 runs and
    when it completes, t2 hangs, waiting for itself to finish.</p>
<pre>
package com.mvivekweb.ocpjp6.obj4.sect2.exam;
    
public class Marathon {
    
    public static void main(String[] args) throws Exception{
Jog j1 = new Jog();
 Thread t1 = new Thread(j1);
 t1.start();
 t1.sleep(500);
System.out.print("pre ");
t1.interrupt();
 t1.sleep(500);
 System.out.print("post ");
    }
        
}
class Jog implements Runnable {
 public void run() {
 for(int i = 0; i < 8; i++) {
 try { Thread.sleep(200); }
 catch (Exception e) { System.out.print("exc "); }
 System.out.print(i + " ");
 } } }</pre>
<pre class='out'>
run:
0 1 pre exc 2 3 4 post 5 6 7 BUILD SUCCESSFUL (total time: 1 second)
</pre>

