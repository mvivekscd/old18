<h3>  Collections (Objective 6.1)</h3>
        <p> Common collection activities include adding objects, removing objects, verifying
            object inclusion, retrieving objects, and iterating.</p>
        <p> Three meanings for "collection":</p>
        <p> collection Represents the data structure in which objects are stored</p>
        <p> Collection java.util interface from which Set and List extend</p>
        <p> Collections A class that holds static collection utility methods</p>
        <p> Four basic flavors of collections include Lists, Sets, Maps, Queues:</p>
        <p> Lists of things Ordered, duplicates allowed, with an index.</p>
        <p> Sets of things May or may not be ordered and/or sorted; duplicates
            not allowed.</p>
        <p> Maps of things with keys May or may not be ordered and/or sorted;
            duplicate keys are not allowed.</p>
        <p> Queues of things to process Ordered by FIFO or by priority.</p>
        <p> Four basic sub-flavors of collections Sorted, Unsorted, Ordered, Unordered.</p>
        <p> Ordered Iterating through a collection in a specific, non-random order.</p>
        <p> Sorted Iterating through a collection in a sorted order.</p>
        <p> Sorting can be alphabetic, numeric, or programmer-defined.</p>
<h3>Key Attributes of Common Collection Classes (Objective 6.1)</h3>
<p> ArrayList: Fast iteration and fast random access.</p>
<p> Vector: It's like a slower ArrayList, but it has synchronized methods.</p>
<p> LinkedList: Good for adding elements to the ends, i.e., stacks and queues.</p>
<p> HashSet: Fast access, assures no duplicates, provides no ordering.</p>
<p> LinkedHashSet: No duplicates; iterates by insertion order.</p>
<p> TreeSet: No duplicates; iterates in sorted order.</p>
<p> HashMap: Fastest updates (key/values); allows one null key, many
    null values.</p>
<p> Hashtable: Like a slower HashMap (as with Vector, due to its synchronized
    methods). No null values or null keys allowed.</p>
<p> LinkedHashMap: Faster iterations; iterates by insertion order or last accessed;
    allows one null key, many null values.</p>
<p> TreeMap: A sorted map.</p>
<p> PriorityQueue: A to-do list ordered by the elements' priority.  </p>     
        
        <h3>Overriding hashCode() and equals() (Objective 6.2)</h3>
        <p> equals(), hashCode(), and toString() are public.</p>
        <p> Override toString() so that System.out.println() or other
            methods can see something useful, like your object's state.</p>
        <p> Use == to determine if two reference variables refer to the same object.</p>
        <p> Use equals() to determine if two objects are meaningfully equivalent.</p>
        <p> If you don't override equals(), your objects won't be useful hashing keys.</p>
        <p> If you don't override equals(), different objects can't be considered equal.</p>
        <p> Strings and wrappers override equals() and make good hashing keys.</p>
        <p> When overriding equals(), use the instanceof operator to be sure you're
            evaluating an appropriate class.</p>
        <p> When overriding equals(), compare the objects' significant attributes.</p>
        <p> Highlights of the equals() contract:</p>
        <p> Reflexive: x.equals(x) is true.</p>
        <p> Symmetric: If x.equals(y) is true, then y.equals(x) must be true.</p>
        <p> Transitive: If x.equals(y) is true, and y.equals(z) is true,
            then z.equals(x) is true.</p>
        <p> Consistent: Multiple calls to x.equals(y) will return the same result.</p>
        <p> Null: If x is not null, then x.equals(null) is false.</p>
        <p> If x.equals(y) is true, then x.hashCode() == y.hashCode() is true.</p>
        <p> If you override equals(), override hashCode().</p>
        <p> HashMap, HashSet, Hashtable, LinkedHashMap, & LinkedHashSet use hashing.</p>
        <p> An appropriate hashCode() override sticks to the hashCode() contract.</p>
        <p> An efficient hashCode() override distributes keys evenly across its buckets.</p>
        <p> An overridden equals() must be at least as precise as its hashCode() mate.</p>
        <p> To reiterate: if two objects are equal, their hashcodes must be equal.</p>
        <p> It's legal for a hashCode() method to return the same value for all instances
            (although in practice it's very inefficient).</p>

        <p> Highlights of the hashCode() contract:</p>
        <p> Consistent: multiple calls to x.hashCode() return the same integer.</p>
        <p> If x.equals(y) is true, x.hashCode() == y.hashCode() is true.</p>
        <p> If x.equals(y) is false, then x.hashCode() == y.hashCode() can
            be either true or false, but false will tend to create better efficiency.</p>
        <p> transient variables aren't appropriate for equals() and hashCode().</p>

<h3>Using Collection Classes (Objective 6.3)</h3>
<p> Collections hold only Objects, but primitives can be autoboxed.</p>
<p> Iterate with the enhanced for, or with an Iterator via hasNext() & next().</p>
<p> hasNext() determines if more elements exist; the Iterator does NOT move.</p>
<p> next() returns the next element AND moves the Iterator forward.</p>
<p> To work correctly, a Map's keys must override equals() and hashCode().</p>
<p> Queues use offer() to add an element, poll() to remove the head of the
    queue, and peek() to look at the head of a queue.</p>
<p> As of Java 6 TreeSets and TreeMaps have new navigation methods like
    floor() and higher().</p>
<p> You can create/extend "backed" sub-copies of TreeSets and TreeMaps.</p>

<h3>Generics (Objective 6.4)</h3>
<p> Generics let you enforce compile-time type safety on Collections (or other
    classes and methods declared using generic type parameters).</p>
<p> An ArrayList<Animal> can accept references of type Dog, Cat, or any other
    subtype of Animal (subclass, or if Animal is an interface, implementation).</p>
<p> When using generic collections, a cast is not needed to get (declared type) elements
    out of the collection. With non-generic collections, a cast is required:
    List<String> gList = new ArrayList<String>();
        List list = new ArrayList();
        // more code
        String s = gList.get(0); // no cast needed
        String s = (String)list.get(0); // cast required</p>
        <p> You can pass a generic collection into a method that takes a non-generic collection,
            but the results may be disastrous. The compiler can't stop the method
            from inserting the wrong type into the previously type safe collection.</p>
        <p> If the compiler can recognize that non-type-safe code is potentially endangering
            something you originally declared as type-safe, you will get a compiler
            warning. For instance, if you pass a List<String> into a method declared as
            void foo(List aList) { aList.add(anInteger); }
            You'll get a warning because add() is potentially "unsafe".</p>
        <p> "Compiles without error" is not the same as "compiles without warnings."
            A compilation warning is not considered a compilation error or failure.</p>
        <p> Generic type information does not exist at runtime?it is for compile-time
            safety only. Mixing generics with legacy code can create compiled code that
            may throw an exception at runtime.</p>
        <p> Polymorphic assignments applies only to the base type, not the generic type
            parameter. You can say
            List<Animal> aList = new ArrayList<Animal>(); // yes
                You can't say
                List<Animal> aList = new ArrayList<Dog>(); // no
                        </p>
                        <p> The polymorphic assignment rule applies everywhere an assignment can be
                            made. The following are NOT allowed:
                            void foo(List<Animal> aList) { } // cannot take a List<Dog>
                                List<Animal> bar() { } // cannot return a List<Dog></p>
                                        <p> Wildcard syntax allows a generic method, accept subtypes (or supertypes) of
                                            the declared type of the method argument:
                                        <pre>void addD(List<Dog> d) {} // can take only <Dog>
void addD(List<? extends Dog>) {} // take a <Dog> or <Beagle>
</pre></p>
<p> The wildcard keyword extends is used to mean either "extends" or "implements."
So in <? extends Dog>, Dog can be a class or an interface.</p>
<p> When using a wildcard, List<? extends Dog>, the collection can be
accessed but not modified.</p>
<p> When using a wildcard, List<?>, any generic type can be assigned to the
reference, but for access only, no modifications.</p>
<p> List<Object> refers only to a List<Object>, while List<?> or
List<? extends Object> can hold any type of object, but for access only.</p>
<p> Declaration conventions for generics use T for type and E for element:
public interface List<E> // API declaration for List
boolean add(E o) // List.add() declaration</p>
<p> The generics type identifier can be used in class, method, and variable
declarations:
class Foo<t> { } // a class
T anInstance; // an instance variable
Foo(T aRef) {} // a constructor argument
void bar(T aRef) {} // a method argument
T baz() {} // a return type
The compiler will substitute the actual type.</p>
<p> You can use more than one parameterized type in a declaration:
public class UseTwo<T, X> { }</p>
<p> You can declare a generic method using a type not defined in the class:
public <T> void makeList(T t) { }
is NOT using T as the return type. This method has a void return type, but
to use T within the method's argument you must declare the <T>, which
happens before the return type. </p>       
        

<h3>Sorting and Searching Arrays and Lists (Objective 6.5)</h3>
<p> Sorting can be in natural order, or via a Comparable or many Comparators.</p>
<p> Implement Comparable using compareTo(); provides only one sort order.</p>
<p> Create many Comparators to sort a class many ways; implement compare().</p>
<p> To be sorted and searched, a List's elements must be comparable.</p>
<p> To be searched, an array or List must first be sorted.</p>
<h3>Utility Classes: Collections and Arrays (Objective 6.5)</h3>
<p> Both of these java.util classes provide</p>
<p> A sort() method. Sort using a Comparator or sort using natural order.</p>
<p> A binarySearch() method. Search a pre-sorted array or List.</p>

<p> Arrays.asList() creates a List from an array and links them together.</p>
<p> Collections.reverse() reverses the order of elements in a List.</p>
<p> Collections.reverseOrder() returns a Comparator that sorts in reverse.</p>
<p> Lists and Sets have a toArray() method to create arrays.</p>
