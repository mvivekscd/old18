<pre>       
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class Jail {
    private int x = 4;
    public static void main(String[] args) {
        <mark>protected int x = 6;</mark>
        new Jail().new Cell().slam();
    }
    class Cell {
        void slam() {
            System.out.println("throw away key " + x);
        }
    }
}
    
        </pre>
        
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com.mvivekweb.ocpjp6.obj1.sect3.exam.Jail.java
javac: file not found: com.mvivekweb.ocpjp6.obj1.sect3.exam.Jail.java
Usage: javac <options> <source files>
use -help for a list of possible options
    
C:\vivek\docs\cert\ocpjp6\src>        
    
</pre>
        
<p>local variables <mark>cannot have access
            modifiers. </mark>    </p>   
        

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
public class Grids {
   public static void main(String[] args) {
        int [][] ia2;
        int [] ia1 = {1,2,3};
        Object o = ia1;
        ia2 = new int[3][3];
        ia2[0] = (int[])o;
       ia2[0][0] = (int[])o;
    }
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/exam/Grids.java
com\mvivekweb\ocpjp6\obj1\sect3\exam\Grids.java:15: incompatible types
found   : int[]
required: int
       ia2[0][0] = (int[])o;
                   ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>
        
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class GaitedPony extends Horse {
    
    static String hands = "14";
        
    public static void main(String[] args) {
        String hands = "13.2";
        String result = new GaitedPony().getSize(hands);
        System.out.println(" " + result);
    }
        
    String getSize(String s) {
        System.out.print("hands: " + s);
        return hands;
    }
}
    
class Horse {
    
    String hands = "15";
}
    
</pre>
<pre class='out'>
run:
hands: 13.2 14
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class GaitedPony extends Horse {
    
    static String hands = "14";
        
    public static void main(String[] args) {
        <mark>static</mark> String hands = "13.2";
        String result = new GaitedPony().getSize(hands);
        System.out.println(" " + result);
    }
        
    String getSize(String s) {
        System.out.print("hands: " + s);
        return hands;
    }
}
    
class Horse {
    
    String hands = "15";
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/exam/GaitedPony.java
com\mvivekweb\ocpjp6\obj1\sect3\exam\GaitedPony.java:8: illegal start of expression
        static String hands = "13.2";
        ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class ArrayTest {
    
    public static void main(String[] args) {
        Integer[][] la = {{1, 2}, {3, 4, 5}};
        Number[] na = la[1];
        Number[] na2 = (Number[]) la[0];
        Object o = na2;
        la[1] = (Number[]) o;
        la[0] = (Integer[]) o;
    }
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/exam/ArrayTest.java
com\mvivekweb\ocpjp6\obj1\sect3\exam\ArrayTest.java:10: incompatible types
found   : java.lang.Number[]
required: java.lang.Integer[]
        la[1] = (Number[]) o;
                ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>

<p><mark>Integer extends Number</mark>, so you can't refer an Integer[] reference to a
    Number[] (an Integer can do things a Number can't.)
</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class PassEnum {
    
    public static void main(String[] args) {
        PassEnum p = new PassEnum();
      <mark>  MyEnum[] v = MyEnum.values();</mark>
       <mark> v = MyEnum.getValues();</mark>
        for (MyEnum me : MyEnum.values()) {
            p.getEnum(me);
        }
        for (int x = 0; x < <mark>MyEnum.values().length</mark>; x++) {
            p.getEnum(v[x]);
        }
        for (int x = 0; x < <mark>MyEnum.length</mark>; x++) {
            p.getEnum(v[x]);
        }
        for (MyEnum me : v) {
            p.getEnum(me);
        }
    }
        
    public void getEnum(MyEnum e) {
        System.out.print(e + " ");
    }
}
    
enum MyEnum {
    
    HI, ALOHA, HOWDY
};
    
    
</pre>

<pre class='out'>

C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/exam/PassEnum.java
com\mvivekweb\ocpjp6\obj1\sect3\exam\PassEnum.java:8: cannot find symbol
symbol  : method getValues()
location: class com.mvivekweb.ocpjp6.obj1.sect3.exam.MyEnum
        v = MyEnum.getValues();
                  ^
com\mvivekweb\ocpjp6\obj1\sect3\exam\PassEnum.java:15: cannot find symbol
symbol  : variable length
location: class com.mvivekweb.ocpjp6.obj1.sect3.exam.MyEnum
        for (int x = 0; x < MyEnum.length; x++) {
                                  ^
2 errors

C:\vivek\docs\cert\ocpjp6\src>

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
public class Mouthwash {
    
    static int x = 1;
        
    public static void main(String[] args) {
        int x = 2;
        for (int i = 0; i < 3; i++) {
            if (i == 1) {
                System.out.print(x + " ");
            }
        }
        go();
        System.out.print(x + " " + i);
    }
        
    static void go() {
        int x = 3;
    }
}
    
</pre>


<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect3/exam/Mouthwash.java
com\mvivekweb\ocpjp6\obj1\sect3\exam\Mouthwash.java:15: cannot find symbol
symbol  : variable i
location: class com.mvivekweb.ocpjp6.obj1.sect3.exam.Mouthwash
        System.out.print(x + " " + i);
                                   ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>

<pre>
17. Given:
4. <mark>class Electricity { int getCharge() { return 24; } }</mark>
5. public class Voltage extends Electricity {
6. enum volts {twelve, twentyfour, oneten};
7. public static void main(String[] args) {
8. volts v = volts.twentyfour;
9. switch (v) {
10. case twelve:
11. System.out.print("12 ");
12. default:
13. System.out.print(getCharge() + " ");
14. case oneten:
15. System.out.print("110 ");
16. } } }
What is the result? (Choose all that apply.)
A. 24
B. 24 110
C. 24 110 12
D. Compilation fails due to a misuse of enums
E. Compilation fails due to a non-enum issue.
Answer (for Objective 1.3):
E is correct. The code fails because the instance method <mark>getCharge() is called from a
static method.</mark> The enums are used correctly. If getCharge() was a static method,
the output would be "24 110".
    
    
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect3.exam;
    
    
public class Hemlock {
static StringBuffer sb;
StringBuffer sb2;
    
    public static void main(String[] args) {
        sb = sb.append(new Hemlock().go(new StringBuffer("hey")));
System.out.println(sb);
    }
<mark>{ sb2 = new StringBuffer("hi "); }</mark>
StringBuffer go(StringBuffer s) {
 System.out.print(s + " oh " + sb2);
 return new StringBuffer("ey");
 }
 <mark>static { sb = new StringBuffer("yo "); }</mark>
}
    
</pre>
<pre class='out'>
run:
hey oh hi yo ey
BUILD SUCCESSFUL (total time: 1 second)

    </pre>

