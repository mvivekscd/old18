    <pre>      
  package com.mvivekweb.ocpjp6.obj3.sec5.exam;
      
public class RegexTest {
    
    public static void main(String[] args) {
        boolean b = false;
 int i = 7;
 double d = 1.23;
 float f = 4.56f;
 System.out.printf(" %b", b);
<mark>System.out.printf(" %i", i);
System.out.format(" %d", d);</mark>
System.out.format(" %d", i);
 System.out.format(" %f", f);
    }
}
        </pre>
        
        <pre class='out'>        
run:
Exception in thread "main" java.util.UnknownFormatConversionException: Conversion is 'i'
	at java.util.Formatter$Transformer.transform(Unknown Source)
	at java.util.Formatter.format(Unknown Source)
	at java.io.PrintStream.format(PrintStream.java:933)
	at java.io.PrintStream.printf(PrintStream.java:834)
	at com.mvivekweb.ocpjp6.obj3.sec5.exam.RegexTest.main(RegexTest.java:11)
 falseJava Result: 1
BUILD SUCCESSFUL (total time: 1 second)
        </pre>
        <pre>       
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
public class RegexTest {
    
    public static void main(String[] args) {
        boolean b = false;
 int i = 7;
 double d = 1.23;
 float f = 4.56f;
 System.out.printf(" %b", b);
//System.out.printf(" %i", i);
<mark>System.out.format(" %d", d);</mark>
System.out.format(" %d", i);
 System.out.format(" %f", f);
    }
}
    
        </pre>
        
<pre class='out'>        
run:
Exception in thread "main" java.util.IllegalFormatConversionException: d is incompatible with java.lang.Double
	at java.util.Formatter$Transformer.transformFromInteger(Unknown Source)
	at java.util.Formatter$Transformer.transform(Unknown Source)
	at java.util.Formatter.format(Unknown Source)
	at java.io.PrintStream.format(PrintStream.java:933)
	at com.mvivekweb.ocpjp6.obj3.sec5.exam.RegexTest.main(RegexTest.java:12)
 falseJava Result: 1
BUILD SUCCESSFUL (total time: 1 second)
    </pre>   
        
        <p><mark>integers</mark> should use <mark>%d</mark>.  <mark>floats and doubles should use %f</mark>.
        </p>
        
 
<pre>       
 package com.mvivekweb.ocpjp6.obj3.sec5.exam;
     
import java.text.NumberFormat;
import java.text.ParseException;
    
public class Gazillion {
    
    public static void main(String[] args) throws ParseException {
       <mark> String s</mark> = "123.456xyz";
NumberFormat nf = NumberFormat.getInstance();
 System.out.println(nf.parse(s));
 nf.setMaximumFractionDigits(2);
 System.out.println(nf.format(s));
    }
}
    
</pre>   
<pre class='out'>        
run:
123.456
<mark>Exception in thread "main" java.lang.IllegalArgumentException: Cannot format given Object as a Number</mark>
	at java.text.DecimalFormat.format(DecimalFormat.java:498)
	at java.text.Format.format(Format.java:151)
	at com.mvivekweb.ocpjp6.obj3.sec5.exam.Gazillion.main(Gazillion.java:13)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
        
    </pre>   
        
        <pre>       
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
import java.text.NumberFormat;
import java.text.ParseException;
    
public class Gazillion {
    
    public static void main(String[] args) throws ParseException {
        String s = "123.456";
NumberFormat nf = NumberFormat.getInstance();
 System.out.println(nf.parse(s));
 nf.setMaximumFractionDigits(2);
 System.out.println(nf.format(s));
    }
}        
        </pre>   
<pre class='out'>        
run:
123.456
Exception in thread "main" java.lang.IllegalArgumentException: Cannot format given Object as a Number
	at java.text.DecimalFormat.format(DecimalFormat.java:498)
	at java.text.Format.format(Format.java:151)
	at com.mvivekweb.ocpjp6.obj3.sec5.exam.Gazillion.main(Gazillion.java:13)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
  
    </pre>
 <pre>       
package com.mvivekweb.ocpjp6.obj3.sec5.exam;

import java.text.NumberFormat;
import java.text.ParseException;

public class Gazillion {

    public static void main(String[] args) throws ParseException {
        String s = "123.456";
NumberFormat nf = NumberFormat.getInstance();
 System.out.println(nf.parse(s));
 nf.setMaximumFractionDigits(2);
 System.out.println(nf.format(s));
 System.out.println(nf.format(Double.parseDouble(s)));
    }
}        
    </pre> 
 
<pre class='out'>        
run:
123.456
Exception in thread "main" java.lang.IllegalArgumentException: Cannot format given Object as a Number
	at java.text.DecimalFormat.format(DecimalFormat.java:498)
	at java.text.Format.format(Format.java:151)
	at com.mvivekweb.ocpjp6.obj3.sec5.exam.Gazillion.main(Gazillion.java:13)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)
        
    </pre> 
        
        <pre>
 package com.mvivekweb.ocpjp6.obj3.sec5.exam;
     
import java.util.regex.Matcher;
import java.util.regex.Pattern;
    
public class Decaf {
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile(args[0]);
 Matcher m = p.matcher(args[1]);
 while(m.find())
 System.out.print(m.group() + " ");
    }
}       
</pre>  
        

<pre class='out'>

C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj3/sec5/exam/Decaf.java

C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/exam/Decaf "0([0-7])?" "1012 0208 430"
<mark>01 02 0 0</mark>
C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/exam/Decaf "0([0-7])*" "1012 0208 430"
<mark>012 020 0</mark>
C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/exam/Decaf "0([0-7])+" "1012 0208 430"
<mark>012 020</mark>
C:\vivek\docs\cert\ocpjp6\src>
        
    </pre>
        <pre>       
 package com.mvivekweb.ocpjp6.obj3.sec5.exam;
     
import java.util.Scanner;
    
public class ScannerTest {
    
    public static void main(String[] args) {
        
 String in = "1234,77777,689";
Scanner sc = new Scanner(in);
<mark> sc.useDelimiter(",");</mark>
while(<mark>sc.hasNext()</mark>)
    System.out.print(<mark>sc.nextInt()</mark> + " ");
while(sc.hasNext())
 System.out.print(sc.nextShort() + " ");       
    }
}
    
        </pre>
 <pre class='out'>       
 run:
1234 77777 689 BUILD SUCCESSFUL (total time: 1 second)
    </pre>   

        
        <pre>        
            
  package com.mvivekweb.ocpjp6.obj3.sec5.exam;
      
public class SplitTest {
    
    public static void main(String[] args) {
        String s = "4.5x4.a.3";
String[] tokens = <mark>s.split("\\s");</mark>
for(String o: tokens)
System.out.print(o + " ");
    
System.out.print(" ");
tokens = <mark>s.split("\\..");</mark>
for(String o: tokens)
 System.out.print(o + " ");
    }
}      
    
        </pre>
        
 <pre class='out'>       
run:
4.5x4.a.3  4 x4 BUILD SUCCESSFUL (total time: 1 second)
    </pre>
        
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
import java.util.Scanner;
    
public class ScannerTest1 {
    
    public static void main(String[] args) {
    String s = "123 888888 x 345 -45";
 Scanner sc = new Scanner(s);
while(sc.hasNext())
 if(sc.hasNextShort())
 System.out.print(sc.nextShort() + " ");    
     
     
    }
}
        </pre>
<pre class='out'>        
run:
123         
    </pre>     
        <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
import java.util.Scanner;
    
public class ScannerTest1 {
    
    public static void main(String[] args) {
        String s = "123 888888 x 345 -45";
        Scanner sc = new Scanner(s);
        while (sc.hasNext()) {
            if (sc.hasNextShort()) {
                System.out.print(sc.nextShort() + " ");
            } else {
               // sc.next();
                System.out.println("1");
            }
        }
            
    }
}        
        </pre>
 <pre class='out'>  
123     
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1        
    </pre>   

        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
import java.util.Scanner;
    
public class ScannerTest1 {
    
    public static void main(String[] args) {
        String s = "123 888888 x 345 -45";
        Scanner sc = new Scanner(s);
        while (sc.hasNext()) {
            if (sc.hasNextShort()) {
                System.out.print(sc.nextShort() + " ");
            } else {
               sc.next();
                System.out.println("1");
            }
        }
            
    }
}        
    
        </pre>
<pre class='out'>        
run:
123 1
1
345 -45 BUILD SUCCESSFUL (total time: 1 second)
        
    </pre> 
        <pre>       
 package com.mvivekweb.ocpjp6.obj3.sec5.exam;
     
import java.util.Scanner;
    
public class ScannerTest1 {
    
    public static void main(String[] args) {
        String s = "123 888888 x 345 -45";
        Scanner sc = new Scanner(s);
        while (sc.hasNext()) {
            if (sc.hasNextShort()) {
                System.out.print(sc.nextShort() + " ");
            } else {
               sc.next();
            }
        }
            
    }
}
        </pre>
<pre class='out'>        
run:
123 345 -45 BUILD SUCCESSFUL (total time: 1 second)        
        
    </pre>
        <pre>
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
public class SplitTest1 {
    
    public static void main(String[] args) {
        
       String s = "dogs. with words.";
 // insert code here
      // String[] output = s.split("s");
     //  String[] output = s.split("d");
       String[] output = s.split("\\d");
       //String[] output = s.split("\\s");
     //  String[] output = s.split("\\w");
     //  String[] output = s.split("\\.");
         
         
 for(String o: output)
 System.out.print(o + " "); 
     
     
    }
}
        </pre>    
<pre class='out'>        
run:
dogs. with words. BUILD SUCCESSFUL (total time: 1 second)
        
</pre>     
        <pre> 
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
public class SplitTest1 {
    
    public static void main(String[] args) {
        
       String s = "dogs. with words.";
 // insert code here
      // String[] output = s.split("s");
     //  String[] output = s.split("d");
      // String[] output = s.split("\\d");
       //String[] output = s.split("\\s");
       String[] output = s.split("\\w");
     //  String[] output = s.split("\\.");
         
         
 for(String o: output)
 System.out.print(o + " "); 
     
     
    }
}
    
</pre>
<pre class='out'>        
run:
    .           . BUILD SUCCESSFUL (total time: 1 second)
        
</pre>
        
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
public class SplitTest1 {
    
    public static void main(String[] args) {
        
       String s = "dogs. with words.";
 // insert code here
      // String[] output = s.split("s");
     //  String[] output = s.split("d");
      // String[] output = s.split("\\d");
       //String[] output = s.split("\\s");
       //String[] output = s.split("\\w");
       String[] output = s.split("\\.");
           
           
 for(String o: output)
 System.out.print(o + " "); 
     
     
    }
}
</pre>
<pre class='out'>        
run:
dogs  with words BUILD SUCCESSFUL (total time: 2 seconds)        
</pre>
        <pre>        
package com.mvivekweb.ocpjp6.obj3.sec5.exam;
    
public class LogSplitter {
    
    public static void main(String[] args) {
        for(int x = 1; x < args.length; x++)
System.out.print(args[0].split(args[x]).length + " ");
    }
}
        </pre> 
        
        
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>java com/mvivekweb/ocpjp6/obj3/sec5/exam/LogSplitter "x1 23 y #" "\d" "\s" "\w"
s1=x
s1=
s1=
s1= y #
4 s1=x1
s1=23
s1=y
s1=#
4 s1=
s1=
s1=
s1=
s1=
s1= #
6
C:\vivek\docs\cert\ocpjp6\src>        
    </pre> 

        