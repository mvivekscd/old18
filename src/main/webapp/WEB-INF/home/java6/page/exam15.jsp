 <pre>  
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
import java.io.*;
class Author {
 protected void write() throws IOException { }
 }
public class Salinger extends Author {
 private void write(int x) { }
 protected void write(long x) throws FileNotFoundException { }
 protected void write(boolean x) throws Exception { }
 protected int write(short x) { return 7; }
 public void write() { }
     
}
    
        </pre>
        
 <pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Salinger.java

C:\vivek\docs\cert\ocpjp6\src>       
        
    </pre>    
        

        <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
    
public class LaSelva extends Beach{
    LaSelva() { s = "LaSelva"; }
        
    public static void main(String[] args) {
        new LaSelva().go();
    }
    void go() {
        Beach[] ba = { new Beach(), new LaSelva(), (Beach) new LaSelva() };
        for(Beach b: ba) System.out.print(b.getBeach().s + " ");
     }
    LaSelva getBeach() { return this; }
}
class Beach {
    String s;
 Beach() { s = "Beach"; }
 Beach getBeach() { return this; }
}       
    
        </pre>
        
 <pre class='out'>       
run:
Beach LaSelva LaSelva BUILD SUCCESSFUL (total time: 1 second)        
        
    </pre> 
        
        <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
public class LaSelva extends Beach{
    public static void main(String[] args) {
        new LaSelva().go();
    }
    void go() {
        Beach[] ba = { new Beach(), new LaSelva(), (Beach) new LaSelva() };
        for(Beach b: ba) System.out.print(b.getBeach().s + " ");
     }
    LaSelva getBeach() { return this; }
}
class Beach {
    String s;
 Beach() { s = "Beach"; }
 Beach getBeach() { return this; }
}        
        </pre>
<pre class='out'>        
run:
Beach Beach Beach BUILD SUCCESSFUL (total time: 2 seconds)        
    </pre>     

        
        <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
class Sport {
    Sport play() { System.out.print("play "); return new Sport(); }
      Sport play(int x) { System.out.print("play x "); return new Sport(); }
 }
     
public class Baseball extends Sport{
    <mark>Baseball play()</mark> { System.out.print("baseball "); return new Baseball(); }
    Sport play(int x) { System.out.print("sport "); return new Sport(); }
    public static void main(String[] args) {
        new Baseball().play();
        new Baseball().play(7);
        super.play(7);
         new Sport().play();
        Sport s = new Baseball();
         s.play();
    }
        
}
        </pre>
        
  
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Baseball.java
com\mvivekweb\ocpjp6\obj1\sect5\exam\Baseball.java:16: <mark>non-static variable super cannot be referenced from a static context</mark>
        super.play(7);
        ^
1 error

C:\vivek\docs\cert\ocpjp6\src>        
        
        
    </pre>
<p>        
    <mark>A call to super cannot be made from a static
        context</mark>. The <mark>overridden play()</mark> method on line 8 is an example of a <mark>covariant return</mark>,
    which is a legal override as of Java 5.
</p>     
        
<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
class Sport {
    Sport play() { System.out.print("play "); return new Sport(); }
      Sport play(int x) { System.out.print("play x "); return new Sport(); }
 }
     
public class Baseball extends Sport{
    Baseball play() { System.out.print("baseball "); return new Baseball(); }
    Sport play(int x) { System.out.print("sport "); return new Sport(); }
    public static void main(String[] args) {
        new Baseball().play();
        new Baseball().play(7);
       // super.play(7);
         new Sport().play();
        Sport s = new Baseball();
         s.play();
    }
        
}
</pre>
<pre class='out'>
run:
baseball sport play baseball BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>       
Given that:
Exception is the superclass of IOException, and
IOException is the superclass of FileNotFoundException, and
3. import java.io.*;
4. class Physicist {
5. void think() throws IOException { }
6. }
7. public class Feynman extends Physicist {
8. public static void main(String[] args) {
9. new Feynman().think();
10. }
11. // insert method here
12. }
Which of the following methods, inserted independently at line 11, compiles? (Choose all
that apply.)
A. void think() throws Exception { }
B. void think() throws FileNotFoundException { }
C. public void think() { }
D. protected void think() throws IOException { }
E. private void think() throws IOException { }
F. void think() { int x = 7/0; }
    
</pre>
<pre class='out'>
Answer (for Objective 1.4):
B, C, D, and F are correct. <mark>It's legal for overridden methods to have less restrictive
access modifiers</mark>, to <mark>have fewer or narrower checked exceptions</mark>, and to <mark>have unchecked
exceptions</mark>. (Note: Of course, F would throw an exception at runtime.)
A is incorrect because Exception is broader. E is incorrect because private is more
restrictive.

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
class IcelandicHorse {
    void tolt() { System.out.print("4-beat "); }
}
    
public class Vafi extends IcelandicHorse{
    
    
    public static void main(String[] args) {
        new Vafi().go();
        new IcelandicHorse().tolt();
    }
    void go() {
        IcelandicHorse h1 = new Vafi();
        h1.tolt();
        Vafi v = (Vafi) h1;
        v.tolt();
     }
     void tolt() { System.out.print("pacey "); }
}
    
    
</pre>
<pre class='out'>
run:
pacey pacey 4-beat BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
class Department {
 Department getDeptName() { return new Department(); }
 }
public class Accounting extends Department {
    Accounting getDeptName() { return new Accounting(); }
    void getDeptName(Department d) { ; }
    String getDeptName(int x) { return "mktg"; }
    void getDeptName(long x) throws NullPointerException {
        throw new NullPointerException();
    }
    <mark>Department getDeptName() throws NullPointerException {</mark>
       <mark> throw new NullPointerException();</mark>
        return new Department();
    }
}
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Accounting.java
com\mvivekweb\ocpjp6\obj1\sect5\exam\Accounting.java:15: <mark>unreachable statement</mark>
        return new Department();
        ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>

<pre class=''out'>

package com.mvivekweb.ocpjp6.obj1.sect5.exam;
class Department {
 Department getDeptName() { return new Department(); }
 }
public class Accounting extends Department {
    //Accounting getDeptName() { return new Accounting(); }
    void getDeptName(Department d) { ; }
    String getDeptName(int x) { return "mktg"; }
    void getDeptName(long x) throws NullPointerException {
        throw new NullPointerException();
    }
    Department getDeptName() throws NullPointerException {
        
        return new Department();
    }
}

    </pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/Accounting.java

C:\vivek\docs\cert\ocpjp6\src>

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
interface Gadget {
    int patent = 12345;
    Gadget doStuff();
 }
public class TimeMachine implements Gadget{
    int patent = 34567;
        
    public static void main(String[] args) {
        new TimeMachine().doStuff();
    }
    TimeMachine doStuff() {
     System.out.println( ++patent);
     return new TimeMachine();
    }
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/TimeMachine.java
com\mvivekweb\ocpjp6\obj1\sect5\exam\TimeMachine.java:15: doStuff() in com.mvivekweb.ocpjp6.obj1.sect5.exam.TimeMachine cannot implement doStuff() in com.mvivekweb.ocpjp6.obj1.sect5.exam.Gadget; attempting to assign weaker access privileges
; was public
    TimeMachine doStuff() {
                ^
1 error

C:\vivek\docs\cert\ocpjp6\src>


    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;

interface Gadget {
    int patent = 12345;
    Gadget doStuff();
 }
public class TimeMachine implements Gadget{
    int patent = 34567;

    public static void main(String[] args) {
        new TimeMachine().doStuff();
    }
    <mark>public</mark> TimeMachine doStuff() {
     System.out.println( ++patent);
     return new TimeMachine();
    }
}

    </pre>

<p>Regardless of the Java version, the <mark>implementing doStuff() method must
    be declared public</mark>.</p>

<pre class='out'>
run:
34568
BUILD SUCCESSFUL (total time: 1 second)
    </pre>


    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
interface Gadget {
    int patent = 12345;
    Gadget doStuff();
 }
public class TimeMachine implements Gadget{
    int patent = 34567;
        
    public static void main(String[] args) {
        new TimeMachine().doStuff();
    }
    public TimeMachine doStuff() {
     System.out.println( ++patent);
     return new TimeMachine();
    }
}
    </pre>
    
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect5/exam/TimeMachine.java

C:\vivek\docs\cert\ocpjp6\src>javac -source 1.4 com/mvivekweb/ocpjp6/obj1/sect5/exam/TimeMachine.java
com\mvivekweb\ocpjp6\obj1\sect5\exam\TimeMachine.java:9: com.mvivekweb.ocpjp6.obj1.sect5.exam.TimeMachine is not abstract and does not override abstract method doStuff() in com.mvivekweb.ocpjp6.obj1.sect5.exam.Gadget
public class TimeMachine implements Gadget{
       ^
com\mvivekweb\ocpjp6\obj1\sect5\exam\TimeMachine.java:15: doStuff() in com.mvivekweb.ocpjp6.obj1.sect5.exam.TimeMachine cannot implement doStuff() in com.mvivekweb.ocpjp6.obj1.sect5.exam.Gadget; attempting to use incompatible return type
found   : com.mvivekweb.ocpjp6.obj1.sect5.exam.TimeMachine
required: com.mvivekweb.ocpjp6.obj1.sect5.exam.Gadget
    public TimeMachine doStuff() {
                       ^
2 errors

C:\vivek\docs\cert\ocpjp6\src>
    
</pre>
    
    
    <pre>
package com.mvivekweb.ocpjp6.obj1.sect5.exam;
    
class One {
    void go1() { System.out.print("1 "); }
    <mark>final void go2() { System.out.print("2 "); }</mark>
    <mark>private void go3() { System.out.print("3 "); }</mark>
 }
     
public class OneB extends One{
    
    void go1() { System.out.print("1b "); }
    void go3() { System.out.print("3b "); }
    public static void main(String[] args) {
        new OneB().go1();
        new One().go1();
        <mark>new OneB().go2();</mark>
        new OneB().go3();
        <mark> new One().go3();</mark>
    }
        
}
    </pre>
    
<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.4 com/mvivekweb/ocpjp6/obj1/sect5/exam/OneB.java
com\mvivekweb\ocpjp6\obj1\sect5\exam\OneB.java:20: <mark>go3() has private access in</mark> com.mvivekweb.ocpjp6.obj1.sect5.exam.One
         new One().go3();
                  ^
1 error

C:\vivek\docs\cert\ocpjp6\src>    
    
    </pre>
    
    
    <p>Class <mark>OneB cannot find class One's private
        go3() method.</mark>  </p>
    
    