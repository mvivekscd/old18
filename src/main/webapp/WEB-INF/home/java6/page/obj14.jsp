<h3>Develop code that declares both static and non-static methods, and-if appropriate-
    use method names that adhere to the JavaBeans naming standards. Also develop code that
    declares and uses a variable-length argument list.</h3>




<h3>Access Modifiers</h3>

<h3>Public Members</h3>

<p>When a method or variable member is declared public, it means all other classes,
    regardless of the package they belong to, can access the member<mark>(assuming the class
        itself is visible)</mark></p>

<img src="/imag/jp6/publicmethod.png" class="imgw"> 
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods;
    
public class Roo {
    
    public String doRooThings() {
// imagine the fun code that goes here
        return "fun";
    }
        
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods;
    
public class Cloo extends Roo {
    
    public void testCloo() {
        System.out.println(<mark>doRooThings()</mark>);
    }
        
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods;

public class publictest {

    public static void main(String[] args) {
        Cloo co = new Cloo();
        co.testCloo();
        System.out.println("co.doRooThings()="+co.doRooThings());
    }

}

    
</pre>
<pre class='out'>
run:
fun
co.doRooThings()=fun
BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<img src="/imag/jp6/inheritance.png" class="imgw"> 

<h3>Private Members</h3>


<img src="/imag/jp6/privatemethod.png" class="imgw"> 

<p>Members marked private <mark>can't be accessed by code in any class</mark> other than the
    class in which the private member was declared</p>

<p>a
    method marked private <mark>cannot be overridden</mark></p>


<h3>Protected and Default Members</h3>

<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage;
    
public class OtherClass {
    
    <mark>void testIt() {</mark> // No modifier means method has default
// access
        System.out.println("OtherClass");
    }
}
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods;
    
import com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage.OtherClass;
    
public class AccessClass {
    
    public static void main(String[] args) {
        OtherClass o = new OtherClass();
<mark>        o.testIt();
</mark>    }
        
}
    
</pre>

<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - testIt() is not public in com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage.OtherClass; cannot be accessed from outside package
	at com.mvivekweb.ocpjp6.obj1.sect4.methods.AccessClass.main(AccessClass.java:9)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>


<img src="/imag/jp6/defaultmethod.png" class="imgw"> 


<h3>Protected Members</h3>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage;
    
    
public class protectedMethod {
    protected void callProtectedMethod(){
        System.out.println("protected");
    }
    protected void callProtectedMethod1(){
        callProtectedMethod();
    }
}
    
</pre>

<pre>
    
package com.mvivekweb.ocpjp6.obj1.sect4.methods;
    
    
import com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage.protectedMethod;
    
public class protectedTest extends protectedMethod{
    
    
    public static void main(String[] args) {
        
        protectedMethod pr= new  protectedMethod();
       <mark> pr.callProtectedMethod();</mark>
        protectedTest pt= new protectedTest();
        <mark>pt.callProtectedMethod();</mark>
            
    }
        
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.RuntimeException: Uncompilable source code - callProtectedMethod() has protected access in com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage.protectedMethod
	at com.mvivekweb.ocpjp6.obj1.sect4.methods.protectedTest.main(protectedTest.java:14)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods;
    
    
import com.mvivekweb.ocpjp6.obj1.sect4.methods.newpackage.protectedMethod;
    
public class protectedTest extends protectedMethod{
    
    
    public static void main(String[] args) {
        
        protectedMethod pr= new  protectedMethod();
        //pr.callProtectedMethod();
       <mark> protectedTest pt= new protectedTest();
        pt.callProtectedMethod();</mark>
            
    }
        
        
}
    
</pre>
<pre class='out'>
run:
protected
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<img src="/imag/jp6/protectedmember.png" class="imgw"> 

<h3>Nonaccess Member Modifiers</h3>

<h3>Final Methods</h3>

<p>The final keyword prevents a method from being overridden in a subclass</p>

<p>Preventing a subclass
    from overriding a method stifles many of the benefits of OO including extensibility
    through polymorphism.
</p>

<h3>Final Arguments</h3>


<pre>public Record getRecord(int fileNumber, <mark>final</mark> int recordNumber) {}
    
</pre>

<p>the variable recNumber is declared as final, which of course
    means <mark>it can't be modified</mark> within the method</p>

<h3>Abstract Methods</h3>

<p>
    You can, however, <mark>have an abstract class with no abstract</mark> methods.
</p>

<p>The first concrete subclass of an abstract class must implement all abstract
    methods of the superclass.</p>

<p>A method can <mark>never</mark>, ever, ever be marked as both <mark>abstract and final</mark>, or both
    <mark>abstract and private</mark>.</p>

<p>you need to know that the <mark>abstract modifier can never be combined
        with the static</mark> modifier.</p>



<h3>Identifiers & JavaBeans</h3>

<p>The <mark>three aspects</mark> of Java identifiers</p>

<p><b>a. Legal Identifiers</b>-The rules the compiler uses to determine whether a
    name is legal</p>

<p><b>b.Sun's Java Code Conventions</b>-
    You won't be asked questions about the Java Code Conventions,
    but we strongly recommend that programmers use them.
</p>


<p><b>c. JavaBeans Naming Standards</b>- 
    You don't need to study the JavaBeans spec for the exam.need to <mark>know a few basic JavaBeans naming rules</mark></p>

<h3>Legal Identifiers</h3>

<p>legal identifiers must be composed of only <mark>Unicode characters</mark>,
    <mark>numbers</mark>, <mark>currency symbols</mark>, and <mark>connecting characters (like underscores</mark>).</p>


<p>Identifiers must start with a <mark>letter</mark>, a <mark>currency character ($)</mark>, or a <mark>connecting
    character such as the underscore ( _ )</mark>. Identifiers <mark>cannot start with a number</mark>!</p>
<pre>
int _a;
int $c;
int ______2_w;
int _$;
int this_is_a_very_detailed_name_for_an_identifier;
    
</pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.identifiersJavabeans;
    
public class identifierTest {
    
    public static void main(String[] args) {
        int e#=0;
    }
        
}
    
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect4/identifiersJavabeans/identifierTest.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj1\sect4\identifiersJavabeans\identifierTest.java:6: error: illegal character: '#'
        int e#=0;
             ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>
</pre>


<p>After the first character, identifiers can contain any combination of <mark>letters,
    currency characters, connecting characters, or numbers</mark>.</p>

<p>no limit to the number of characters an identifier can
        contain.</p>
    
<p>Identifiers in Java are case-sensitive; foo and FOO are two different identifiers. </p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.identifiersJavabeans;
    
public class identifierTest {
    
    public static void main(String[] args) {
        int _999x$$$____=0;
<mark>        int foo;
        int FOO;</mark>
    }
        
}
    
    
</pre>
<pre>
int _a;
int $c;
int ______2_w;
int _$;
int this_is_a_very_detailed_name_for_an_identifier;</pre>

<h3>keywords </h3>
<pre>
native
goto
const</pre>

<h3>Sun's Java Code Conventions</h3>


<p>Sun has created
    a set of coding standards for Java, and published those standards in a document
    cleverly titled "<mark>Java Code Conventions</mark>," which you can find at java.sun.com</p>

<p>The one standard that is followed as much
    as possible in the real exam are the naming standards</p>

<p>the naming standards
    that Sun recommends, and that we use in the exam</p>


<h3>Classes and interfaces</h3>

<p>The first letter should be capitalized, and if several
    words are linked together to form the name, the first letter of the inner words
    should be uppercase</p>

<p>For
    classes, <mark>the names should typically be nouns</mark></p>
<pre>
Dog
Account
PrintWriter
</pre>

<p>For interfaces, the names should typically be adjectives like</p>
<pre>
Runnable
Serializable
    
</pre>

<h3>Methods</h3>

<p>The first letter should be lowercase, and then normal camelCase
    rules should be used</p>


<p>In addition, the names should typically be verb-noun
    pairs.</p>
<pre>
getBalance
doCalculation
setCustomerName
    
</pre>

<h3>Variables</h3>

<p>Like methods, the camelCase format should be used, starting with
    a lowercase letter.</p>

<p>Sun recommends short, meaningful names, which sounds
    good to us.</p>
<pre>
buttonWidth
accountBalance
myString
</pre>

<h3>Constants</h3>

<p>Java constants are created by marking variables <mark>static and
    final</mark>.</p>

<p>They should be named using uppercase letters with underscore
        characters</p>
    
<pre>MIN_HEIGHT  </pre>  

<h3>JavaBeans Standards</h3>
<p>JavaBeans are Java classes that have properties</p>

<p>think of
    properties as <mark>private instance variables</mark>. Since they're private, the only way
    they can be accessed from outside of their class <mark>is through methods</mark> in the class.</p>




<h3>JavaBean Property Naming Rules</h3>

<p>If the property is <mark>not a boolean</mark>, the getter method's prefix must be <mark>get</mark>.</p>

<p>If the property <mark>is a boolean</mark>, the getter method's prefix is either <mark>get</mark> or <mark>is</mark>.</p>

<p>The setter method's prefix must be <mark>set</mark>.</p>

<p>Setter method signatures must be marked <mark>public</mark>, with a <mark>void</mark> return type
    and an <mark>argument that represents the property type</mark>.</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.identifiersJavabeans;
    
public class JavaBeanClass {
    private int vivcount=0;
        
    public int getVivcount() {
        <mark>return vivcount;</mark>
    }
        
    public void setVivcount(<mark>int vivcount</mark>) {
        <mark>this.vivcount = vivcount;</mark>
    }
}
    
</pre>

<p>
    Getter method signatures must be marked <mark>public</mark>, take <mark>no arguments</mark>, and
    have <mark>a return type that matches the argument type</mark> of the setter method for
    that property.
</p>

<p>Second, the<mark> JavaBean spec supports events</mark>, which allow components to <mark>notify</mark>
    each other when something happens.</p>

<p>The event model is <mark>often used in GUI
        applications</mark></p>

<p>The objects that receive
            the information that an event occurred are called <mark>listeners</mark>.</p>

<p>For the exam, you need to
    know that the <mark>methods that are used to add or remove listeners</mark> from an event must
    also follow JavaBean naming standards:</p>



<h3>JavaBean Listener Naming Rules</h3>

<p>Listener method names used to "register" a listener with an event source
    <mark>must use</mark> the prefix <mark>add</mark>, followed by the <mark>listener type</mark>.</p>


<p>Listener method names used to remove ("unregister") a listener must use
    the prefix <mark>remove</mark>, followed by the <mark>listener type</mark></p>


<p>The <mark>type of listener</mark> to be added or removed must be <mark>passed as the argument
    to the method</mark>.</p>


    <p>Listener method names <mark>must end with</mark> the word "<mark>Listener</mark>".</p>


    <pre>public void addMyListener(<mark>MyListener</mark> m)
public void removeMyListener(<mark>MyListener</mark> m)</pre>
    
    
<p>
        If the exam question is asking about naming conventions-not just
        whether an identifier will compile-JavaBeans will be mentioned explicitly.</p>


<h3>Methods with Variable Argument Lists (var-args)</h3>

<p>Java allows you to create methods that can take a <mark>variable number of
    arguments</mark>.</p>

<p>this capability referred to
    as "<mark>variable-length argument lists</mark>," "<mark>variable arguments</mark>," "var-args," "<mark>varargs</mark>," or our
        personal favorite (from the department of obfuscation), "variable arity parameter."
        They're all the same thing, </p>

<p><b>arguments</b>- The things you specify between the parentheses when you're
    invoking a method:</p>
<pre>
doStuff("a", 2); // invoking doStuff, so <mark>a & 2 are arguments</mark>
</pre>

<p><b>parameters</b>-The things in the method's signature that indicate what the
    method must receive when it's invoked:</p>
<pre>
void doStuff(String s, int a) { } // we're expecting <mark>two
// parameters: String and int</mark>
    
</pre>
<h3>declaration rules for var-args:</h3>

<p>When you declare a var-arg parameter, you must specify the
    type of the argument(s) this parameter of your method can receive. (This can
    be a <mark>primitive type or an object type</mark>.)</p>

<p>To declare a method using a var-arg parameter, <mark>you follow the
        type with an ellipsis (...)</mark>, a <mark>space</mark>, and then the <mark>name of the array</mark> that will
    hold the parameters received.</p>


<p>It's <mark>legal to have other parameters</mark> in a method that uses
    a var-arg.</p>

<p>The <mark>var-arg must be the last parameter</mark> in the method's
    signature,</p>

<p>you can have <mark>only one var-arg in a method</mark></p>
<pre>
Legal:
void doStuff(<mark>int...</mark> x) { } // expects from 0 to many ints
// as parameters
void doStuff2(char c, <mark>int...</mark> x) { } // expects first a char,
// then 0 to many ints
void doStuff3(<mark>Animal...</mark> animal) { } // 0 to many Animals
    
</pre>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods.varargs;
    
class Animal {
    
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect4.methods.varargs;
    
    
public class VarargClass {
    void doStuff(<mark>int... x</mark>) { } // expects from 0 to many ints
// as parameters
void doStuff2(char c, int... x) { } // expects first a char,
// then 0 to many ints
void doStuff3(Animal... animal) { } //
}
    
</pre>



