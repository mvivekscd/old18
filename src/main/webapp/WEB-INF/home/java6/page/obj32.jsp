<h3> Given a scenario involving navigating file systems, reading from files, writing to files, or interacting with the user, develop the correct solution using the following classes (sometimes in combination), from java.io: BufferedReader, BufferedWriter, File, FileReader, FileWriter, PrintWriter, and Console.</h3>
   
        <p> exam are fairly well restricted to
            file I/O for <mark>characters</mark>, and serialization.</p>
        <p> The classes you need to understand in java.io are <mark>File</mark>, <mark>FileReader</mark>,
            <mark>BufferedReader</mark>, <mark>FileWriter</mark>, <mark>BufferedWriter</mark>, <mark>PrintWriter</mark>, and <mark>Console</mark>.</p>
        <p>        File is "An abstract representation of file
            and directory <mark>pathnames</mark>."</p>
        <p> The <b>File</b> class <mark>isn't used to actually read or write</mark>
            data; it's used to work at a higher level   </p>    
<p> A new File object doesn't mean there's a new file on your hard drive.</p>
<p> <b>File</b> objects can represent either a file or a directory.</p>

<p> The File class lets you manage (add, rename, and delete) files and directories.</p>

<p><b>FileReader</b>-is used to <mark>read character files</mark></p>

<p>Its read() methods are
    fairly <mark>low-level</mark>, allowing you to <mark>read single characters</mark>, the whole stream of
    characters, or a fixed number of characters.</p>

<p>FileReaders are usually <mark>wrapped</mark>
    by higher-level objects such as <mark>BufferedReaders</mark>, which improve <mark>performance</mark></p>

<p><b>BufferedReader</b>-read relatively <mark>large chunks</mark> of data from a file at once, and
    keep this <mark>data in a buffer</mark>.</p>
<p>When you ask for the <mark>next character</mark> or <mark>line of data</mark>,
    it is <mark>retrieved from the buffer</mark>, which minimizes the number of times that
    time-intensive, file read operations are performed.</p>
<p><mark>readLine()</mark> allow you to get the <mark>next line of characters</mark> from a file</p>

<p><b>FileWriter</b>-is used to <mark>write to character</mark> files.<mark>write()</mark>
methods allow you to <mark>write character(s) or Strings</mark> to a file</p>

<p>FileWriters are
    usually wrapped by <mark>BufferedWriters</mark> or
    <mark>PrintWriters</mark></p>
<p><b>BufferedWriter</b>-<mark>write</mark> relatively <mark>large chunks of data</mark> to a file at once,
    <mark>minimizing the number of times</mark> that slow, file writing operations are
    performed.</p>
<p>newLine()
    method to create platform-specific <mark>line separators automatically</mark></p>

<p><b>PrintWriter</b>-<mark>format(), printf(), and append()</mark>
    make PrintWriters very flexible and powerful</p>
<p><b>Console</b>-<mark>read input</mark>
    from the <mark>console</mark> and <mark>write</mark> formatted output to the <mark>console</mark></p>

<p><b>Stream</b> classes are used to read and write <mark>bytes</mark>.it is probably about <mark>serialization</mark></p>
<p><b>Readers and Writers</b>
    are used to read and write <mark>characters</mark>.</p>

<p>When you make a new instance of the class File, <mark>you're not yet
    making an actual file, you're just creating a filename</mark></p>
<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
    
public class Writer1 {
    
    public static void main(String[] args) {
        File file = new File("fileWrite1.txt"); // There's no
// file yet!
    }
}
</pre>
<p>there
    are several ways to make an actual file.</p>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
import java.io.IOException;
    
public class Writer2 {
    
    public static void main(String[] args) {
        try { // warning: exceptions possible
            boolean newFile = false;
            File file = new File // it's only an object
                    ("fileWrite1.txt");
            System.out.println(<mark>file.exists()</mark>); // look for a real file
            newFile = file.<mark>createNewFile()</mark>; // maybe create a file!
            System.out.println(<mark>newFile</mark>); // already there?
            System.out.println(file.exists()); // look again
        } catch (IOException e) {
        }
    }
}
    
</pre>
<pre class='out'>
run:
false
true
true
BUILD SUCCESSFUL (total time: 0 seconds)
    </pre>

<p><b>Second execution</b> The first call to exists() returns true because we
    built the file during the first run. Then the call to <mark>createNewFile() returns
    false</mark> since the method didn't create a file this time through.</p>
<pre class='out'>
run:
true
false
true
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
<h3>Using FileWriter and FileReader</h3>

<p>you probably won't use the FileWriter and FileReader classes <mark>without
    wrapping</mark> them</p>

<p>
    "naked" file I/O .classes without
    wrapping them</p>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
    
public class Writer3 {
    
    public static void main(String[] args) {
        char[] in = new char[<mark>50</mark>]; // to store input
        int size = 0;
        try {
            <mark>File</mark> file = new File( // just an object
                    "fileWrite2.txt");
            <mark>FileWriter</mark> fw = new FileWriter(file); // create an actual file
// & a FileWriter obj
            fw.<mark>write</mark>("howdy<mark>\n</mark>folks\n"); // write characters to
// the file
            fw.<mark>flush</mark>(); // flush before closing
           <mark> fw.close();</mark>
            <mark>FileReader</mark> fr = new FileReader(file); // create a FileReader
// object
            size = fr.<mark>read</mark>(in); // read the whole file!
            System.out.print(size + " "); // how many bytes read
            for (char c : in) // print the array
            {
                System.out.print(c);
            }
            <mark>fr.close()</mark>; // again, always close
        } catch (IOException e) {
        }
    }
}
</pre>
<pre class='out'>
run:
12 howdy
folks
    </pre>
<p>The <mark>read()</mark> method read the whole file, a <mark>character at a time</mark>, and put it into
    the char[] in.</p>


<p>When you <mark>write
    data</mark> out to a <mark>stream</mark>, <mark>some amount of buffering</mark> will occur, and you never know for
    sure exactly when the last of the data will actually be sent</p>


<p>invoking the <mark>flush()</mark> method
    <mark>guarantees that the last of the data</mark> you thought you had already written actually
    gets out to the file.</p>
<p>Whenever you're <mark>done using a file, either reading it or writing</mark>
    to it, <mark>you should invoke the close()</mark> method.</p>


<p>doing file I/O you're
    using expensive and limited operating system resources, and so when you're done,
    invoking <mark>close()</mark> will <mark>free up </mark>those resources.</p>

<p><mark>manually inserted line separators</mark>
    (in this case \n), into our data.When we were reading data back in, we put it into a <mark>character array</mark>. It
    being an array and all, we had to d<mark>eclare its size beforehand</mark>, so we'd have
    been in trouble if we hadn't made it big enough!</p>

<p>Because of these limitations, we'll typically need to <mark>use higher-level I/O classes</mark>
    like <mark>BufferedWriter</mark> or <mark>BufferedReader</mark> in <mark>combination with FileWriter or FileReader</mark>.</p>

<h3>Combining I/O classes</h3>

<p>Combining I/O classes is called <mark>wrapping</mark> or  <mark>chaining</mark></p>
<p>The java.io package contains about 50 classes, 10 interfaces, and
    15 exceptions</p>

<table class="pgt">
	<caption>Table 1</caption>
	<thead>
	<tr>
		<th>java.io Class</th>
		<th>Extends From</th>
		<th>Key Constructor(s) Arguments</th>
		<th>Key Methods</th>
	</tr>
	</thead>
	<tbody>
            <tr>
                <td data-label="Class">File</td>
                <td data-label="Extends">Object</td>
                <td data-label="Constructor">
                    File, String
                    <br>String
                    <br>                    String, String
                </td>
                <td data-label="Methods">
                    createNewFile()
                    <br><mark>delete()</mark>
                    <br>                    exists()
                    <br>                    isDirectory()
                    <br>                    isFile()
                    <br>                    <mark>list()</mark>
                    <br>                    mkdir()
                    <br>                    <mark>renameTo()</mark></td>
            </tr>
            <tr>
                <td data-label="Class">FileWriter</td>
                <td data-label="Extends">Writer</td>
                <td data-label="Constructor">File
                    <br>String</td>
                <td data-label="Methods">close()
                    <br>flush()
                    <br>                    write()</td>
            </tr>
	<tr>
		<td data-label="Class">BufferedWriter</td>
		<td data-label="Extends">Writer</td>
		<td data-label="Constructor">Writer</td>
		<td data-label="Methods">close()
                    <br>flush()
                    <br> <mark>newLine()</mark>
                    <br>write()</td>
        </tr>
	<tr>
		<td data-label="Class">PrintWriter</td>
		<td data-label="Extends">Writer</td>
		<td data-label="Constructor"><mark>File (as of Java 5)</mark>
                    <br><mark>String (as of Java 5)</mark>
                    <br>                    OutputStream
                    <br>                    Writer</td>
                <td data-label="Methods">close()
                    <br>flush()
                    <br>                    <mark>format()*, printf()*
                        <br>                        print(), println()</mark>
                    <br>                    write()</td>
        </tr>
	<tr>
		<td data-label="Class">FileReader</td>
		<td data-label="Extends">Reader</td>
		<td data-label="Constructor">File
                    <br>String</td>
                <td data-label="Methods">read()</td>
	</tr>
	<tr>
		<td data-label="Class">BufferedReader</td>
		<td data-label="Extends">Reader</td>
		<td data-label="Constructor">Reader</td>
		<td data-label="Methods">read()
                    <br><mark>readLine()</mark></td>
        </tr>        
	</tbody>
</table>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
    
    
public class CombineIO {
    
    
    public static void main(String[] args) throws IOException {
        File file = new File("fileWrite3.txt"); // create a File object
        FileWriter fw = new FileWriter(file);
        PrintWriter pw = new PrintWriter(fw);
        pw.println("howdy"); // write the data
        pw.println("folks");
        pw.flush();
        pw.close();
            
        FileReader fr =new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String data = br.readLine();
        System.err.println("data="+data);
            
    }
        
}
</pre>
<pre class='out'>
run:
data=howdy
BUILD SUCCESSFUL (total time: 1 second)
    
</pre>


<pre>
    
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
    
    
public class CombineIO {
    
    
    public static void main(String[] args) throws IOException {
        File file = new File("fileWrite3.txt"); // create a File object
        FileWriter fw = new FileWriter(file);
        PrintWriter pw = new PrintWriter(fw);
        pw.println("howdy"); // write the data
        pw.println("folks");
        pw.flush();
        pw.close();
            
        <mark>fw.flush();</mark>
        fw.close();
            
            
        FileReader fr =new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String data = br.readLine();
        System.err.println("data="+data);
            
        br.close();
        fr.close();
            
            
    }
        
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.io.IOException: Stream closed
	at sun.nio.cs.StreamEncoder.ensureOpen(StreamEncoder.java:49)
	at sun.nio.cs.StreamEncoder.flush(StreamEncoder.java:174)
	at java.io.OutputStreamWriter.flush(OutputStreamWriter.java:223)
	at com.mvivekweb.ocpjp6.obj3.sec2.file.CombineIO.main(CombineIO.java:25)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<p>have knowledge of how I/O classes can be chained</p>
<p>write code to
    experiment with which chaining combinations are legal and which are illegal</p>

<p> The methods createNewFile() and mkdir() add entries to your file system.</p>
<p> FileWriter and FileReader are low-level I/O classes. You can use them to
    write and read files, but they should usually be wrapped.</p>
<p> Classes in java.io are designed to be "chained" or "wrapped." (This is a
    common use of the decorator design pattern.)</p>
<p> It's very common to "wrap" a BufferedReader around a FileReader
    or a BufferedWriter around a FileWriter, to get access to higher-level
    (more convenient) methods.</p>

<h3>Working with Files and Directories</h3>

<p><mark>object</mark> of type <mark>File</mark> is used to represent either a <mark>file or a directory</mark></p>

<p>There are two ways to create a file:</p>

<br>1. Invoke the createNewFile() method on a File object.

<br>2. Create a Writer or a Stream. Specifically, create a FileWriter, a PrintWriter,
or a FileOutputStream. Whenever you create an instance of one of these
classes, you automatically create a file, unless one already exists,

<pre>File file = new File("foo"); // no file yet
PrintWriter pw =new PrintWriter(file);</pre>


<p>Creating a directory is similar to creating a file</p>

<p><mark>Directory File object</mark>, capital D</p>

<p>call an actual directory
    on a computer a directory, small <mark>d</mark></p>


<p>creating a directory
    is a <mark>two-step process</mark>; first we <mark>create a Directory (File) object</mark>, then we create an
    actual directory using the following <mark>mkdir()</mark> method</p>


<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
import java.io.IOException;
    
    
public class DirectoryCreation {
    
    
    public static void main(String[] args) throws IOException {
        
        <mark>File myDir = new File("mydir");</mark>
        <mark>File myFile = new File(myDir, "myFile.txt");</mark>
        myFile.createNewFile();
            
    }
        
}
</pre>
<pre class='out'>
run:
Exception in thread "main" java.io.IOException: The system cannot find the path specified.
	at java.io.WinNTFileSystem.createFileExclusively(Native Method)
	at java.io.File.createNewFile(File.java:894)
	at com.mvivekweb.ocpjp6.obj3.sec2.file.DirectoryCreation.main(DirectoryCreation.java:16)
Java Result: 1
BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<p>This will generate an exception something like java.io.IOException: No such file or directory</p>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
import java.io.IOException;
    
    
public class DirectoryCreation {
    
    
    public static void main(String[] args) throws IOException {
        
        File myDir = new File("mydir");
        <mark>myDir.mkdir();</mark>
        File myFile = new File(myDir, "myFile.txt");
        myFile.createNewFile();
            
    }
        
}
    
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 1 second)
    </pre>
<pre>
existingDir
        |
        existingDirFile.txt
            
            
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
    
public class ReadFilefromSubDriectory {
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File existingDir = new File("existingDir"); // assign a dir
        System.out.println(existingDir.<mark>isDirectory()</mark>);
            
        File existingDirFile = new File(existingDir, "existingDirFile.txt"); // assign a file
        System.out.println(existingDirFile.<mark>isFile()</mark>);
        FileReader fr = new <mark>FileReader(existingDirFile);</mark>
        BufferedReader br = new <mark>BufferedReader(fr);</mark> // make a Reader
        String s;
        while ((s = br.<mark>readLine()</mark>) != null) // read data
        {
            System.out.println(s);
        }
        br.close();
            
    }
}
</pre>
<pre class='out'>
run:
true
true
existing sub-dir data
line 2 of text
line 3 of text
BUILD SUCCESSFUL (total time: 1 second)
</pre>

<p>When there is no
    more data to read, <mark>readLine()</mark> returns a null-this is our signal to stop reading the
    file.</p>

<p>When reading a file, no
    flushing is required</p>
<p>You <mark>can't delete a directory</mark> if it's not empty</p>

<h3>search for a file</h3>

<pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.File;
    
    
public class directorySearch {
    
    //only current directory <mark>not sub directories</mark>
    public static void main(String[] args) {
        String[] files = new String[100];
            File search = new File("searchThis");
            files = search.<mark>list();</mark>
            for(String fn : files) // iterate through it
            System.out.println("found " + fn);
                
    }
        
}
</pre>
<pre class='out'>
run:
found dir1
found dir2
found newtext.txt
BUILD SUCCESSFUL (total time: 3 seconds)
    </pre>

<p>It's okay to <mark>rename a directory</mark>, even if <mark>it isn't empty</mark></p>


<p> PrintWriters can be used to wrap other Writers, but as of Java 5 they can be
    built directly from Files or Strings.</p>
<p> Java 5 PrintWriters have new append(), format(), and printf() methods.</p>


<h3>The java.io.Console Class</h3>

<p>New to Java 6</p>

<p>The Console class makes it easy to accept input from the <mark>command line</mark>, both
    echoed and nonechoed (such as a password), and <mark>makes it easy to write formatted
    output</mark> to the command line.</p>


<p> Console objects can read non-echoed input and are instantiated using
    System.console().</p>


<p>handy way to write test engines for <mark>unit testing</mark>
    or if you want to support a simple but <mark>secure user interaction</mark> and <mark>you don't need
    a GUI</mark>.</p>
    
    <p>The <mark>readLine</mark> method <mark>returns a string</mark> containing whatever the
        user keyed in-that's pretty intuitive.</p>
    
    <p>the <mark>readPassword</mark> method <mark>returns a character array</mark></p>
    
    
    <p>Once you've
        got the password, you can verify it and then absolutely remove it from memory.<mark>If a
        string was returned</mark>, it <mark>could exist in a pool somewhere in memory</mark> and perhaps some
        nefarious hacker could find it.</p>
    <pre>
package com.mvivekweb.ocpjp6.obj3.sec2.file;
    
import java.io.Console;
    
    
public class NewConsole {
    
    public static void main(String[] args) {
        Console c = System.console(); // #1: get a Console
        char[] pw;
        pw = c.<mark>readPassword</mark>("%s", "pw: "); // #2: return a char[]
        for (char ch : pw) {
            c.format("%c ", ch); // #3: format output
        }
        c.format("\n");
        MyUtility mu = new <mark>MyUtility</mark>();
        while (true) {
            String name = c.<mark>readLine</mark>("%s", "input?: "); // #4: return a String
            c.format("output: %s \n", mu.doStuff(name));
        }
    }
}
    
class MyUtility { // #5: class to test
    
    String doStuff(String arg1) {
// stub code
        return "result is " + arg1;
    }
}
    </pre>
    
 <pre class='out'>   
C:\vivek\docs\cert\ocpjp6\src>javac -classpath . com\mvivekweb\ocpjp6\obj3\sec2\file\NewConsole.java

C:\vivek\docs\cert\ocpjp6\src>java com.mvivekweb.ocpjp6.obj3.sec2.file.NewConsole
pw:
m a n j u
input?: hello
output: result is hello
input?:
    </pre>
    
<pre class='out'>    
C:\vivek\docs\cert\ocpjp6\src>javac com/mvivekweb/ocpjp6/obj3/sec2/file/NewConsole.java

C:\vivek\docs\cert\ocpjp6\src><mark>java</mark> com<mark>/</mark>mvivekweb/ocpjp6/obj3/sec2/file/NewConsole
pw:

    </pre>
    
    <pre>    
    we can't say this:
<mark>Console c = new Console();</mark>
    </pre>
    
    <p>the password you enter isn't echoed
        on the screen.</p>
    
    <p>when you're
        studying <mark>regex</mark> and <mark>formatting</mark>, we recommend that you use something like
        NewConsole <mark>to test</mark> the concepts that you're learning.  </p> 