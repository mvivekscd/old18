<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
class Feline { }
public class BarnCat2 extends Feline{
    
    
    public static void main(String[] args) {
        Feline ff = new Feline();
 BarnCat2 b = new BarnCat2();
if(ff instanceof BarnCat2) System.out.print("1 ");
if(ff instanceof Feline) System.out.print("2 ");
if(ff instanceof Object) System.out.print("3 ");
if(b instanceof BarnCat2) System.out.print("4 ");
if(b instanceof Feline) System.out.print("5 ");
if(<mark>b.instanceof(Object)</mark>) System.out.print("6 ");
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/exam/BarnCat2.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
com\mvivekweb\ocpjp6\obj7\sect6\exam\BarnCat2.java:17: error: &lt;identifier&gt; expected
if(b.instanceof(Object)) System.out.print("6 ");
     ^
com\mvivekweb\ocpjp6\obj7\sect6\exam\BarnCat2.java:17: error: illegal start of type
if(b.instanceof(Object)) System.out.print("6 ");
               ^
com\mvivekweb\ocpjp6\obj7\sect6\exam\BarnCat2.java:17: error: not a statement
if(b.instanceof(Object)) System.out.print("6 ");
                ^
com\mvivekweb\ocpjp6\obj7\sect6\exam\BarnCat2.java:17: error: ';' expected
if(b.instanceof(Object)) System.out.print("6 ");
                      ^
4 errors
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src></pre>

<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Dropkick {
    
    
    public static void main(String[] args) {
        boolean test = false;
String[] s = {"duck", null, "frog"};
if((s[1] == null) <mark>||</mark> (s[1].length() == 0)) System.out.print("1 ");
if((s[2] == null) <mark>&</mark> (test = true)) System.out.print("2 ");
if((s[0].equals("duck")) <mark>^</mark> (args[0].equals("fish")))
System.out.print("3 ");
if((args[0] != null) <mark>&&</mark> (test)) System.out.print("4 ");
    }
        
}
</pre>

<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/exam/Dropkick.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java com/mvivekweb/ocpjp6/obj7/sect6/exam/Dropkick fish
1 4
C:\vivek\java7\ocpjp6\ocpjp6\src></pre>


<pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Stealth {
    
    
    public static void main(String[] args) {
        Integer i = 420;
 Integer i2;
 Integer i3;
<mark> i2 = i.intValue();
 i3 = i.valueOf(420);</mark>
 System.out.println((i == i2) + " " + (i == i3));
    }
        
}
</pre>

<pre class='out'>run:
false false
BUILD SUCCESSFUL (total time: 0 seconds)</pre>

<p>lines 8 and 9 each <mark>create a new Integer object</mark>, and <mark>== tests
    whether two references refer to the same object</mark>.</p>

    <pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Oper1 {
    
    
    public static void main(String[] args) {
        String s = "-";
 boolean b = false;
 int x = 7, y = 8;
 if((x < 8) ^ (b = true)) s += "^";
 if(!(x > 8) | ++y > 5) s += "|";
 if(++y > 9 && b == true) s += "&&";
if(y % 8 > 1 || y / (x - 7) > 1) s += "%";
 System.out.println(s);
    }
        
}
    </pre>
<pre class='out'>    
run:
-|&&%
BUILD SUCCESSFUL (total time: 0 seconds)
</pre> 
    
<p>The first if test fails because both comparisons are true ("b = true" is
        an assignment, not a test!) and the ^ is XOR-only one condition can be true. In the
        second if test, y is incremented because even though the comparison on the left side of
        the "|" is true, "|" is NOT a short-circuit operator, so the right side will be evaluated.
        The third if test is true because y has been incremented twice, and b was set to true
        in the first if test. The fourth if test is true because the left-hand side is true, so the
        short-circuit operator never tests the right-hand side. If it did, a divide by zero exception
        would be thrown.    
    </p>

    <pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Wacky {
    
    
    public static void main(String[] args) {
        int x = 5;
 int y = (x < 6) ? 7 : 8;
 System.out.print(y + " ");
 boolean b = (x < 6) ? false : true;
 System.out.println(b + " ");
<mark>assert(x < 6)</mark> : "bob";
 <mark>assert( ((x < 6) ? false : true) ) : "fred";</mark>
    }
        
}
    </pre>
    
<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj7/sect6/exam/Wacky.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src>java -ea com/mvivekweb/ocpjp6/obj7/sect6/exam/Wacky
<mark>7 false</mark>
Exception in thread "main" java.lang.AssertionError: <mark>fred</mark>
        at com.mvivekweb.ocpjp6.obj7.sect6.exam.Wacky.main(Wacky.java:16)

C:\vivek\java7\ocpjp6\ocpjp6\src>    
</pre>
    
    
    <pre>
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Oper2 {
    
    
    public static void main(String[] args) {
        int x = 3;
 for(int i = 0; i < 3; i++) {
if(i == 1) <mark>x = x++;</mark>
     System.out.println("i % 2="+i % 2);
     System.out.println("x % 2="+x % 2);
     System.out.println("x="+x);
 if(i % 2 == 0 && x % 2 == 0) System.out.print(".");
 if(i % 2 == 0 && x % 2 == 1) System.out.print("-");
 if(i == 2 ^ x == 4) System.out.print(",");
 }
 System.out.println("<");
    }
        
}
    </pre>
 <pre class='out'>   
run:
i % 2=0
x % 2=1
x=3
-i % 2=1
x % 2=1
x=3
i % 2=0
x % 2=1
x=3
-,<
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>
    
    <pre>
        
package com.mvivekweb.ocpjp6.obj7.sect6.exam;
    
    
public class Honcho {
    
static boolean b1 = false;
 static int z = 7;
 static Long y;
    public static void main(String[] args) {
        for(int i = 0; i < 4; i++)
 go(i);
    }
static void go(int x) {
 try {
 if((x == 0) && (!b1 && z == 7)) System.out.print("0 ");
 if(x < 2 ^ x < 10) System.out.print("1 ");
 if((x == 2) &&
(y == null | (<mark>y.longValue()</mark> == 0))) System.out.print("2 ");
 if(z <= (x + 4)) System.out.print("3 ");
 }
catch(Exception e) { System.out.print("e "); }
 }
}
    </pre>
<pre class='out'>    
run:
0 1 e 1 3 BUILD SUCCESSFUL (total time: 0 seconds)</pre>    
    
<p>The ^ is true only when exactly one of the tests is true. The | does NOT short
        circuit, so longValue() is invoked and throws a NullPointerException</p> 

