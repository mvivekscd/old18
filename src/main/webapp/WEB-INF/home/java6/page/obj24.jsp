<h3>2.4 Develop code that makes use of exceptions and exception handling clauses (try, catch,
    finally), and declares methods and overriding methods that throw exceptions.</h3>
<h3>2.5 Recognize the effect of an exception arising at a specific point in a code fragment.
    Note that the exception may be a runtime exception, a checked exception, or an error.</h3>


<h3>Catching an Exception Using try and catch</h3>

<p>The term "<mark>exception</mark>" means
    "exceptional condition" and is an occurrence that <mark>alters</mark> the normal program flow.</p>

<p>things can lead to exceptions, including <mark>hardware failures</mark>, <mark>resource
        exhaustion</mark>, and <mark>good old bugs</mark></p>

<p>The <mark>try</mark> is used to define a
    block of code in which exceptions may occur. This block of code is called a <mark>guarded
        region</mark></p>


<p><mark>One or more catch</mark> clauses match a specific exception  to a block of code that handles it.</p>

<p>if you have one or more catch blocks, they <mark>must
        immediately follow the try block</mark></p>
        
        <p>the catch blocks <mark>must all follow
                each other</mark> </p> 


<h3>Using finally</h3>
                
<p>A finally block encloses code that is always executed at some point after the
    try block, <mark>whether an exception was thrown or not</mark>.  </p>            

<p>Even if there is a return
    statement in the try block, the <mark>finally block executes right after the return</mark>
    statement is encountered, and <mark>before the return executes</mark></p>

<p>This is the <mark>right place to close your files</mark>, <mark>release your network sockets</mark>, and
    <mark>perform any other cleanup</mark> your code requires.</p>

<p>If the try block executes with
    no exceptions, the finally block is executed immediately after the try block
    completes. If there was an exception thrown, the finally block executes
    immediately after the proper catch block completes.</p>

<p>finally <mark>always runs</mark></p>

<p>few scenarios in which <mark>finally might not run or complete</mark></p>

<p>finally clauses are <mark>not required</mark></p>

<p>compiler doesn't even require catch clauses, sometimes you'll run across code that
    has a <mark>try block immediately followed by a finally</mark> block</p>
<pre>
try {
// do stuff
} finally {
//clean up
}
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
    
public class try1 {
    public static void main(String[] args) {
        try{
        }
    }
        
}
</pre>

<pre class='out'>

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect4\trycatch>javac <mark>-source 1.6</mark> try1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
try1.java:8: error: 'try' without 'catch' or 'finally'
        try{
        ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect4\trycatch>
</pre>

<p>It is illegal to use a try clause without either a catch clause or a finally
    clause</p>

<p>It is legal
    to <mark>omit</mark> either the <mark>catch clause or the finally</mark> clause, but <mark>not both</mark>.</p>

<h3>Propagating Uncaught Exceptions</h3>

<p>If a method doesn't provide a catch clause for a particular
    exception, that method is said to be <mark>"ducking"</mark> the exception</p>


<p>An exception that's
    never caught will<mark> cause your application to stop </mark>running</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
public class TestEx {
    public static void main(String[] args) {
        doStuff();
    }
    static void doStuff() {
        doMoreStuff();
    }
    static void doMoreStuff() {
        int x = 5 / 0; // Can't divide by zero!
// ArithmeticException is thrown here
    }
}
</pre>
<pre class='out'>
run:
Exception in thread "<mark>main</mark>" <mark>java.lang.ArithmeticException: / by zero</mark>
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx.doMoreStuff(TestEx.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx.doStuff(TestEx.java:7)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx.main(TestEx.java:4)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
</pre>


<h3>Defining Exceptions</h3>

<h3>Exception Hierarchy</h3>

<p>All exception classes are subtypes of class <mark>Exception</mark></p>

<p>This class derives from the
    class Throwable</p>

<pre>
            object
               |
            Throwable
            |       |
           Error    Exception 
                    /               \
                  RuntimeException</pre>


<p>Error represent unusual situations that are
    not caused by program errors.such as the <mark>JVM running out of memory</mark></p>

<p>application won't be able to recover from an Error</p>

<p>You are expected
    to know that <mark>Exception, Error, RuntimeException, and Throwable</mark> types can all be
    <mark>thrown using the throw keyword</mark>, and can all be caught (although you rarely will
    catch anything other than Exception subtypes).</p>

<h3>Handling an Entire Class Hierarchy of Exceptions</h3>

<p>You can <mark>actually catch</mark> <mark>more than one type</mark> of exception in a
    <mark>single catch</mark> clause</p>


<p>By specifying an <mark>exception
        class's superclass</mark> in your catch clause, you're discarding valuable information about
    the exception.</p>

<p>Exception handlers that trap many errors at once
    will probably <mark>reduce the reliability</mark> of your program because it's likely that an
    exception will be caught that the handler does not know how to handle</p>

<h3>Exception Matching</h3>
<pre>
catch(subclass){
}catch(superclass){
}</pre>

<h3>Exception Declaration and the Public Interface</h3>

<p>the exceptions that a method can throw must be declared (unless
    the exceptions are subclasses of RuntimeException)</p>
<pre>
void myFunction() throws MyException1, MyException2 {
// code for the method here
}</pre>

<p>all non-
    RuntimeExceptions are considered <mark>"checked" exceptions</mark></p>

<p>Each method must either handle all checked exceptions by supplying a <mark>catch clause</mark> or
    list each unhandled checked exception as a <mark>thrown exception</mark>.</p>

<p>This rule is referred to as Java's <mark>"handle or declare"</mark> requirement</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class throw1 {
    
    public static void main(String[] args) {
        throw1 th = new throw1();
        <mark>try {</mark>
            th.doStuff();
        } catch (<mark>IOException ex</mark>) {
            Logger.getLogger(throw1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    void doStuff() <mark>throws IOException</mark> {
        doMore();
    }
        
    void doMore() <mark>throws IOException</mark> {
        <mark>throw new IOException();</mark>
    }
}
    
</pre>
<pre class='out'>
run:
May 08, 2016 12:42:44 PM com.mvivekweb.ocpjp6.obj2.sect4.trycatch.throw1 main
SEVERE: null
<mark>java.io.IOException</mark>
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.throw1.doMore(throw1.java:23)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.throw1.doStuff(throw1.java:19)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.throw1.main(throw1.java:12)

BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<p><mark>RuntimeException, Error, and all of their
        subtypes</mark> are <mark>unchecked exceptions</mark> and unchecked exceptions do not have to be
    specified or handled</p>

<p><mark>NullPointerException</mark>, it is an
    <mark>unchecked exception</mark> and need not be declared</p>

<p>If you invoke a
    method that <mark>throws a checked exception</mark> but you <mark>don't catch</mark> the checked exception somewhere, your code <mark>will not compile</mark></p>

<p>To create your <mark>own
        exception</mark>, you <mark>simply subclass Exception</mark> (or one of its subclasses)</p>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class TestEx1 {
    
    public static void main(String[] args) {
        TestEx1 test = new TestEx1();
        <mark>try {</mark>
            test.doStuff();
        } catch (MyException ex) {
            Logger.getLogger(TestEx1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    void doStuff()<mark> throws MyException</mark> {
        <mark>throw</mark> new MyException(); // Throw a checked exception
    }
}
    
class <mark>MyException</mark> extends <mark>Exception</mark> {
    
    public MyException() {
    }
}
    
</pre>
<pre class='out'>
run:
May 08, 2016 1:01:47 PM com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx1 main
SEVERE: null
<mark>com.mvivekweb.ocpjp6.obj2.sect4.trycatch.MyException</mark>
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx1.doStuff(TestEx1.java:18)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.TestEx1.main(TestEx1.java:11)

BUILD SUCCESSFUL (total time: 0 seconds)

</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
public class MyException1 extends Exception {
    void someMethod() {
        doStuff();
    }
        
    void doStuff() throws MyException1 {
        try {
            throw new MyException1();
        } catch (MyException1 me) {
            throw me;
        }
    }
}
</pre>


<pre class='out'>
C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect4\trycatch>javac -source 1.6 MyException1.java
warning: [options] bootstrap class path not set in conjunction with -source 1.6
MyException1.java:9: error: unreported exception MyException1; must be caught or declared to be thrown
        doStuff();
               ^
1 error
1 warning

C:\vivek\java7\ocpjp6\ocpjp6\src\com\mvivekweb\ocpjp6\obj2\sect4\trycatch>


</pre>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
public class MyException1 extends Exception {
    void someMethod() <mark>throws MyException1</mark> {
        doStuff();
    }
        
    void doStuff() <mark>throws MyException1</mark> {
        try {
            <mark>throw new MyException1()</mark>;
        } catch (MyException1 me) {
            <mark>throw me;</mark>
        }
    }
}
    
</pre>

<p>Both <mark>Exception and Error</mark> share a common
    superclass, Throwable, thus both can be thrown using the <mark>throw</mark> keyword</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
public class ErrorTest {
    
    public static void main(String[] args) {
        badMethod();
    }
        
    static void badMethod() { // No need to declare an Error
        doStuff();
    }
        
    static void doStuff() { //No need to declare an Error
        try {
            <mark>throw new Error();</mark>
        } catch (Error me) {
            <mark>throw me; // We catch it, but then rethrow it</mark>
        }
    }
}
    
</pre>
<pre class='out'>
run:
Exception in thread "main" java.lang.Error
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.doStuff(ErrorTest.java:15)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.badMethod(ErrorTest.java:10)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.main(ErrorTest.java:6)
C:\Users\manju\AppData\Local\NetBeans\Cache\8.1\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)

</pre>

<pre>
import java.util.logging.Logger;
    
public class ErrorTest {
    
    public static void main(String[] args) {
        badMethod();
    }
        
    static void badMethod() { // No need to declare an Error
        doStuff();
    }
        
    static void doStuff() { //No need to declare an Error
        try {
            <mark>throw new Exception();</mark>
        } catch (Error me) {
            throw me; // We catch it, but then rethrow it
       <mark> } catch (Exception ex) {</mark>
            Logger.getLogger(ErrorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}</pre>

<pre class='out'>
run:
May 08, 2016 4:18:32 PM com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest doStuff
SEVERE: null
<mark>java.lang.Exception</mark>
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.doStuff(ErrorTest.java:18)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.badMethod(ErrorTest.java:13)
	at com.mvivekweb.ocpjp6.obj2.sect4.trycatch.ErrorTest.main(ErrorTest.java:9)

BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class MyException1 extends Exception {
    void someMethod() throws MyException1 {
        doStuff();
    }
        
    void doStuff()<mark>  </mark>{// no throws declaration
        try {
            <mark>throw new MyException1();</mark>
        } catch (<mark>MyException1 me</mark>) {
            <mark>try {
                throw me;
            } catch (MyException1 ex) {</mark>
                Logger.getLogger(MyException1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
    
</pre>
<pre>
try {
callBadMethod();
} catch (<mark>Exception ex</mark>) { }
</pre>
<p>
    You won't even be able to tell that the exception occurred,
    because <mark>you'll never see the stack trace</mark>
</p>
<pre>
package com.mvivekweb.ocpjp6.obj2.sect4.trycatch;
    
public class ExceptionTest {
    public static void main(String[] args) {
        try{
        int x= 5/0;
        }<mark>catch(Exception e){
        }</mark>
    }
}
</pre>
<pre class='out'>
run:
BUILD SUCCESSFUL (total time: 0 seconds)
</pre>

<h3>Rethrowing the Same Exception</h3>

<p>If you throw a checked exception from a catch
    clause, you must also declare that exception! In other words, you must handle and
    declare, as opposed to handle or declare</p>