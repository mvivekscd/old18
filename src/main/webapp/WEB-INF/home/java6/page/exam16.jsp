<pre>      
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
<mark>abstract</mark> class Sand {
    
    <mark>Sand() {</mark>
        System.out.print("s ");
    }
}
    
class Concrete extends Sand {
    
    Concrete() {
        System.out.print("c ");
    }
        
    <mark>private</mark> Concrete(String s) {
    }
}
    
public class RediMix extends Concrete {
    
    RediMix() {
        System.out.println("r ");
    }
        
    public static void main(String[] args) {
        new RediMix();
    }
}
    
        </pre>  
<pre class='out'>        
run:
s c r 
BUILD SUCCESSFUL (total time: 1 second)        
</pre>  
        
<p>It's legal for abstract classes to have constructors, and it's legal for a
            constructor to be private. Normal constructor chaining is the result of this code.        </p>
        
<pre>        
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
abstract class Sand {
    
    Sand() {
        System.out.print("s ");
    }
    <mark>void Vivek(){
        System.out.print("vivek abstract class");
    }</mark>
}
    
class Concrete extends Sand {
    
    Concrete() {
        System.out.print("c ");
            
    }
        
    private Concrete(String s) {
    }
}
    
public class RediMix extends Concrete {
    
    RediMix() {
        System.out.println("r ");
    }
        
    public static void main(String[] args) {
        new RediMix();
        <mark>new RediMix().Vivek();</mark>
    }
}        
</pre>
<pre class='out'>
run:
s c r 
s c r 
vivek abstract classBUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
public class Internet {
    
    private int y = 8;
        
    public static void main(String[] args) {
        new Internet().go();
    }
        
    void go() {
        int x = 7;
        TCPIP ip = new TCPIP();
        class TCPIP {
            
            void doit() {
                System.out.println(y + x);
            }
        }
        ip.doit();
    }
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/exam/Internet.java
com\mvivekweb\ocpjp6\obj1\sect6\exam\Internet.java:13:<mark> cannot find symbol</mark>
symbol  : class TCPIP
location: class com.mvivekweb.ocpjp6.obj1.sect6.exam.Internet
        TCPIP ip = new TCPIP();
        ^
com\mvivekweb\ocpjp6\obj1\sect6\exam\Internet.java:13: cannot find symbol
symbol  : class TCPIP
location: class com.mvivekweb.ocpjp6.obj1.sect6.exam.Internet
        TCPIP ip = new TCPIP();
                       ^
com\mvivekweb\ocpjp6\obj1\sect6\exam\Internet.java:17: <mark>local variable x is accessed from within inner class; needs to be declared final</mark>
                System.out.println(y + x);
                                       ^
3 errors

C:\vivek\docs\cert\ocpjp6\src>


    </pre>

<p>Method-local inner classes can use variables from their enclosing
    methods, <mark>only if they are marked "final"</mark>. A method-local inner class can be instantiated
    only after it has been declared. It is <mark>legal to use private members</mark> from the enclosing class.</p>



<p>Fill in the blanks using the fragments below, so that the code compiles and produces the output:
    "1 3 2 3 2 "
    Note: You might not need to fill in all of the blanks. Also, you won't use all of the fragments,
    and each fragment can be used only once.
</p>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
public class Timer {
    
    public static void main(String[] args) {
        Watch w = new Watch();
        Watch.Workings ww = w.new Workings();
        ww.tick();
    }
}
interface Gadget { }
class Watch {
class Workings implements Gadget {
Workings() { System.out.print("2 "); }
void tick() { Workings in = new Workings(); }
{ System.out.print("3 "); }
}
Watch() { System.out.print("1 "); }
}
    
</pre>
<pre class='out'>
run:
1 3 2 3 2 BUILD SUCCESSFUL (total time: 1 second)</pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
    
public class Kant extends Philosopher{
Kant() { <mark>this("Bart");</mark> }
Kant(String s) { super(s); }
    
    public static void main(String[] args) {
        new Kant("Homer");
 new Kant();
    }
        
}
class Philosopher {
 Philosopher(String s) { System.out.print(s + " "); }
 }
     
</pre>
<pre class='out'>
run:
Homer Bart BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
    
public class Kant extends Philosopher{
Kant() {<mark> super("Bart");</mark> }
Kant(String s) { super(s); }
    
    public static void main(String[] args) {
        new Kant("Homer");
 new Kant();
    }
        
}
class Philosopher {
 Philosopher(String s) { System.out.print(s + " "); }
 }
     
</pre>
<pre class='out'>
run:
Homer Bart BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
class Tire {
 private static int x = 6;
 public static class Wheel {
 void go() { System.out.print("roll " + x++); }
 } }
     
public class Car {
    
    public static void main(String[] args) {
        new Tire.Wheel().go();
    }
}
    
</pre>
<pre class='out'>
run:
roll 6BUILD SUCCESSFUL (total time: 1 second)

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
class Tire {
 private static int x = 6;
 public <mark>static class</mark> Wheel {
 void go() { System.out.print("roll " + x++); }
 } }
     
public class Car {
    
    public static void main(String[] args) {
        Tire t = new Tire(); 
        t.Wheel().go();
    }
}
    
</pre>

<pre class='out'>
C:\vivek\docs\cert\ocpjp6\src>javac -source 1.6 com/mvivekweb/ocpjp6/obj1/sect6/exam/Car.java
com\mvivekweb\ocpjp6\obj1\sect6\exam\Car.java:13: cannot find symbol
symbol  : method Wheel()
location: class com.mvivekweb.ocpjp6.obj1.sect6.exam.Tire
        t.Wheel().go();
         ^
1 error

C:\vivek\docs\cert\ocpjp6\src>

    </pre>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
class Tire {
 private static int x = 6;
 public static class Wheel {
 void go() { System.out.print("roll " + x++); }
 } }
     
public class Car {
    
    public static void main(String[] args) {
Tire.Wheel w = new Tire.Wheel(); w.go();
    }
}
    
</pre>

<pre class='out'>
run:
roll 6BUILD SUCCESSFUL (total time: 1 second)

    </pre>



<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
    
public class LaoTzu extends Philosopher1{
    
    
    public static void main(String[] args) {
        new LaoTzu();
 new LaoTzu("Tigger");
    }
LaoTzu() { this("Pooh"); }
 LaoTzu(String s) { super(s); }
}
class Philosopher1 {
 Philosopher1(String s) { System.out.print(s + " "); }
 }
     
</pre>
<pre class='out'>
run:
Pooh Tigger BUILD SUCCESSFUL (total time: 1 second)

    </pre>


<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
    
    
public class Auto {
    
    
    public static void main(String[] args) {
        Engine e = new Engine();
 <mark>Engine.Piston</mark> p = e.getPiston();
//e.Piston p = e.getPiston();
 p.go(); p.go();
    }
        
}
class Engine {
public class Piston {
 <mark>int count = 0;</mark>
 void go() { System.out.print(" pump " + ++count); }
}
public Piston getPiston() { return new Piston(); }
 }
     
</pre>
<pre class='out'>
run:
 pump 1 pump 2BUILD SUCCESSFUL (total time: 1 second)

    </pre>

<p>Regular inner classes <mark>cannot have static declarations</mark>, and line 12
    (not line 13), uses the correct syntax to create an inner class instance from outside the
    enclosing class.
</p>
<pre>
package com.mvivekweb.ocpjp6.obj1.sect6.exam;
abstract class Vibrate {
 static String s = "-";
<mark> Vibrate() { s += "v"; }</mark>
 }
public class Echo extends Vibrate{
    Echo() { this(7); s += "e"; }
    Echo(int x) { s += "e2"; }
        public static void main(String[] args) {
            System.out.print("made " + s + " ");
        }
        static {
        Echo e = new Echo();
        System.out.print("block " + s + " ");
     }
}
    
</pre>
<pre class='out'>
run:
block -ve2e made -ve2e BUILD SUCCESSFUL (total time: 1 second)
    </pre>

<p>The <mark>static initialization block is the only place where an instance of Echo
    is created.</mark> When the Echo instance is created, Echo's no-arg constructor calls its 1-arg
    constructor, which then calls Vibrate's constructor (which then secretly calls Object's
    constructor). At that point, the various constructors execute, starting with Object's
    constructor and working back down to Echo's no-arg constructor.</p>



