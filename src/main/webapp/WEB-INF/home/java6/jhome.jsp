<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="css/base.css"/>
        <link rel="stylesheet" href="css/java.css"/>
        <script src="js/basemenu.js"></script>

    </head>
    <body>

        
    <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
    <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
        <jsp:param name="title" value="JAVA 6 HOME"/>
        <jsp:param name="enableLogout" value="true"/>
    </jsp:include>
    
<table class="pgt">
<tr>
	<td>33</td>
	<td><mark>13</mark></td>
	<td>30</td>
	<td>14</td>
	<td>19</td>
	<td></td>

</tr>
<tr>
    <td><mark>16</mark></td>
	<td>17</td>
	<td>17</td>
	<td>17</td>
	<td>17</td>
	<td>16</td>

</tr>
<tr>
    <td><mark>19</mark></td>
	<td>20</td>
	<td>14</td>
	<td>18</td>
	<td>9</td>
	<td></td>

</tr>
<tr>
    <td><mark>18</mark></td>
	<td>17</td>
	<td>21</td>
	<td>9</td>
	<td></td>
	<td></td>

</tr>
<tr>
	<td>15</td>
	<td>17</td>
	<td>15</td>
	<td>17</td>
	<td>14</td>
	<td></td>

</tr>
<tr>
	<td>15</td>
	<td>16</td>
	<td>23</td>
	<td>18</td>
	<td>19</td>
	<td></td>

</tr>
<tr>
	<td>17</td>
	<td>18</td>
	<td>16</td>
	<td>16</td>
	<td>16</td>
	<td>25</td>

</tr>
</table>
    <button type="button" onclick="location.href = 'quest6/h'">quest</button>

    <h1>Objectives</h1> 
    <button type="button" onclick="location.href = 'obj1'">obj1</button>
    <button type="button" onclick="location.href = 'obj2'">obj2</button>
    <button type="button" onclick="location.href = 'obj3'">obj3</button>
    <button type="button" onclick="location.href = 'obj4'">obj4</button>
    <button type="button" onclick="location.href = 'obj5'">obj5</button>
    <button type="button" onclick="location.href = 'obj6'">obj6</button>
    <button type="button" onclick="location.href = 'obj7'">obj7</button>
    
    <button type="button" onclick="location.href = 'otables'">tables</button>
    <h3>Obj1</h3>
    <button type="button" onclick="location.href = 'obj1_1'">obj1.1 classes, nested classes</button>
    <button type="button" onclick="location.href = 'obj12'">obj1.2 Abstract Class ,Interface</button>
    <button type="button" onclick="location.href = 'obj13'">obj1.3 Variables,Arrays, Enum, Static</button>
    <button type="button" onclick="location.href = 'obj14'">obj1.4 var Variable Args, static methods,naming standards</button>
    <button type="button" onclick="location.href = 'obj15'">obj1.5 Overload and Overriding</button>
    <button type="button" onclick="location.href = 'obj16'">obj1.6 constructor</button>
    <h3>Obj1 Coding</h3>
    <button type="button" onclick="location.href = 'obj1code'">obj1 practise</button>
    <h3>Obj1 Exam</h3>
    <button type="button" onclick="location.href = 'exam11'">exam 11</button>
    <button type="button" onclick="location.href = 'exam12'">exam 12</button>
    <button type="button" onclick="location.href = 'exam13'">exam 13</button>
    <button type="button" onclick="location.href = 'exam14'">exam 14</button>
    <button type="button" onclick="location.href = 'exam15'">exam 15</button>
    <button type="button" onclick="location.href = 'exam16'">exam 16</button>
    
    
    <h3>Obj2</h3>
    <button type="button" onclick="location.href = 'obj21'">obj2.1 if switch</button>
    <button type="button" onclick="location.href = 'obj22'">obj2.2 do while for</button>
    <button type="button" onclick="location.href = 'obj23'">obj2.3 Assertion</button>
    <button type="button" onclick="location.href = 'obj24'">obj2.4,5,6 Exception</button>
    <button type="button" onclick="location.href = 'obj26'">obj2.6 Common Exceptions</button>
        <h3>Obj2 Exam</h3>
    <button type="button" onclick="location.href = 'exam21'">exam 21</button>
    <button type="button" onclick="location.href = 'exam22'">exam 22</button>
    <button type="button" onclick="location.href = 'exam23'">exam 23</button>
    <button type="button" onclick="location.href = 'exam24'">exam 24</button>
    <button type="button" onclick="location.href = 'exam25'">exam 25</button>
    <button type="button" onclick="location.href = 'exam26'">exam 26</button>
    <h3>Obj3</h3>
    <button type="button" onclick="location.href = 'obj31'">obj3.1 wrapper strings</button>
    <button type="button" onclick="location.href = 'obj32'">obj3.2 File I/O</button>
    <button type="button" onclick="location.href = 'obj33'">obj3.3 Serialization</button>
    <button type="button" onclick="location.href = 'obj34'">obj3.4 Locale</button>
    <button type="button" onclick="location.href = 'obj35'">obj3.5 Parsing,Token,Format</button>
        <h3>Obj3 Exam</h3>
    <button type="button" onclick="location.href = 'exam31'">exam 31</button>
    <button type="button" onclick="location.href = 'exam32'">exam 32</button>
    <button type="button" onclick="location.href = 'exam33'">exam 33</button>
    <button type="button" onclick="location.href = 'exam34'">exam 34</button>
    <button type="button" onclick="location.href = 'exam35'">exam 35</button>
  
    <h3>Obj4</h3>
    <button type="button" onclick="location.href = 'obj41'">obj4.1 starting threads</button>
    <button type="button" onclick="location.href = 'obj42'">obj4.2 states</button>
    <button type="button" onclick="location.href = 'obj43'">obj4.3 object lock</button>
    <button type="button" onclick="location.href = 'obj44'">obj4.4 wait, notify</button>
    
            <h3>Obj4 Exam</h3>
    <button type="button" onclick="location.href = 'exam41'">exam 41</button>
    <button type="button" onclick="location.href = 'exam42'">exam 42</button>
    <button type="button" onclick="location.href = 'exam43'">exam 43</button>
    <button type="button" onclick="location.href = 'exam44'">exam 44</button>

    
    <h3>Obj5</h3>
    <button type="button" onclick="location.href = 'obj51'">obj5.1 Encapsulation</button>
    <button type="button" onclick="location.href = 'obj52'">obj5.2 polymorphism</button>
    <button type="button" onclick="location.href = 'obj53'">obj5.3 constructor</button>
    <button type="button" onclick="location.href = 'obj54'">obj5.4 invoke methods constructors</button>
    <button type="button" onclick="location.href = 'obj55'">obj5.5 is a , has a</button>   
            <h3>Obj5 Exam</h3>
    <button type="button" onclick="location.href = 'exam51'">exam 51</button>
    <button type="button" onclick="location.href = 'exam52'">exam 52</button>
    <button type="button" onclick="location.href = 'exam53'">exam 53</button>
    <button type="button" onclick="location.href = 'exam54'">exam 54</button>  
    <button type="button" onclick="location.href = 'exam55'">exam 55</button>  
    <h3>Obj6</h3>
    <button type="button" onclick="location.href = 'obj61'">obj6.1 collection</button>
    <button type="button" onclick="location.href = 'obj62'">obj6.2 equals and hashcode</button>                
    <button type="button" onclick="location.href = 'obj64'">obj6.3 ,6.4 generics</button>
    <button type="button" onclick="location.href = 'obj65'">obj6.5 sort search convert</button>
    
            <h3>Obj6 Exam</h3>
    <button type="button" onclick="location.href = 'exam61'">exam 61</button>
    <button type="button" onclick="location.href = 'exam62'">exam 62</button>
    <button type="button" onclick="location.href = 'exam63'">exam 63</button>
    <button type="button" onclick="location.href = 'exam64'">exam 64</button>  
    <button type="button" onclick="location.href = 'exam65'">exam 65</button>      
    <h3>Obj7</h3>
    <button type="button" onclick="location.href = 'obj71'">obj7.1 access modifier,package ,import</button>
    <button type="button" onclick="location.href = 'obj72'">obj7.2 command-line</button>
    <button type="button" onclick="location.href = 'obj73'">obj7.3 Passing Methods</button>
    <button type="button" onclick="location.href = 'obj74'">obj7.4 Garbage collection</button>
    <button type="button" onclick="location.href = 'obj75'">obj7.5 JAR, directory structure,classpath</button>
    <button type="button" onclick="location.href = 'obj76'">obj7.6 operators</button>  
    
                <h3>Obj7 Exam</h3>
    <button type="button" onclick="location.href = 'exam71'">exam 71</button>
    <button type="button" onclick="location.href = 'exam72'">exam 72</button>
    <button type="button" onclick="location.href = 'exam73'">exam 73</button>
    <button type="button" onclick="location.href = 'exam74'">exam 74</button>  
    <button type="button" onclick="location.href = 'exam75'">exam 75</button> 
    <button type="button" onclick="location.href = 'exam76'">exam 76</button>
    <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
</body>
</html>
