<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/base.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/spass.css"/>
         <script src="<%=request.getContextPath()%>/js/basemenu.js"></script>
    </head>
    <body>
                <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="UserPass Record"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>
        <div class="spst">
         <table border="0" class="sph" width="100%" >
                <tr class="headtab"  >
                    <th rowspan="2">ID</th>
                    <th rowspan="2">S.No</th>
                    <th colspan="2"><mark>Name</mark></th>
                    <th>PIN</th>
                    <th>SecondDetail</th>

                    <th>Update TimeStamp</th>
                    <th>updated By</th>
                    <th>autologin</th>


                </tr>
                <tr class="sbc">
                    <th>username</th>
                    <th>firstpass</th>

                    <th>url</th>
                    <th colspan="4"> Comment</th>
                </tr>
               
                    <tr class="rowsp">
                        <td rowspan="2">
                            <c:out value="${storepassOneRow.spSeqId}"/>
                        </td>
                        <td rowspan="2">
                            <c:out value="${storepassOneRow.spSno}"/>
                        </td>
                        <td colspan="2">
                            <mark><c:out value="${storepassOneRow.name}"/></mark>
                        </td>
                        <td>
                            <c:out value="${storepassOneRow.pin}"/>
                        </td>

                        <td>
                            <c:out value="${storepassOneRow.secondDetail}"/>
                        </td>
                        <td>
                            <c:out value="${storepassOneRow.upts}"/>
                        </td>
                        <td>
                            <c:out value="${storepassOneRow.upBy}"/>
                        </td>
                        <td>
                            <c:out value="${storepassOneRow.autologin}"/>
                        </td>
                    </tr>

                    <tr class="rows sbc">
                        <td>
                            <c:out value="${storepassOneRow.username}"/>
                        </td>
                        <td>
                            <c:out value="${storepassOneRow.firstpass}"/>
                        </td>

                        <td>
                            <c:out value="${storepassOneRow.url}"/>
                        </td>
                        <td colspan="4">
                            <c:out value="${storepassOneRow.comments}"/>
                        </td>
                    </tr>
             
            </table>
                        <div>
            <button onClick="window.location.href = '<%=request.getContextPath()%>/Spass/editSP?date='+new Date();return false;">Edit</button>

<button onclick="myFunction()">Delete</button>  
            <button type="button" onclick="location.href = '<%=request.getContextPath()%>/Spass?d='+new Date();">Cancel</button>
<script>
function myFunction() {
    var r = confirm("Delete the current Record");
    if (r == true) {
    window.location.href = '<%=request.getContextPath()%>/Spass/deleteSP';
} else {
    txt = "You pressed Cancel!";
}
}

</script>
</div>
                        <c:if test="${not empty storepassHistory}">
                        <p>History</p>
                        
                        
              <table class="sph" border="0" width="100%">
                <tr class="headtab"  >
                    <th rowspan="2">ID</th>
                    <th rowspan="2">S.No</th>
                    <th colspan="2"><mark>Name</mark></th>
                    <th>PIN</th>
                    <th>SecondDetail</th>

                    <th>Update TimeStamp</th>
                    <th>updated By</th>
                    <th>autologin</th>


                </tr>
                <tr class="sbc">
                    <th>username</th>
                    <th>firstpass</th>

                    <th>url</th>
                    <th colspan="4"> Comment</th>
                </tr>
                <c:forEach var="splist" items="${storepassHistory}">
                    <tr class="rowsp">
                        <td rowspan="2">
                            <c:out value="${splist.spSeqId}"/>
                        </td>
                        <td rowspan="2">
                            <c:out value="${splist.spSno}"/>
                        </td>
                        <td colspan="2">
                            <mark><c:out value="${splist.name}"/></mark>
                        </td>
                        <td>
                            <c:out value="${splist.pin}"/>
                        </td>

                        <td>
                            <c:out value="${splist.secondDetail}"/>
                        </td>
                        <td>
                            <c:out value="${splist.upts}"/>
                        </td>
                        <td>
                            <c:out value="${splist.upBy}"/>
                        </td>
                        <td>
                            <c:out value="${splist.autologin}"/>
                        </td>
                    </tr>

                    <tr class="rows sbc">
                        <td>
                            <c:out value="${splist.username}"/>
                        </td>
                        <td>
                            <c:out value="${splist.firstpass}"/>
                        </td>

                        <td>
                            <c:out value="${splist.url}"/>
                        </td>
                        <td colspan="4">
                            <c:out value="${splist.comments}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>           
                        </c:if> 
                        
        </div>
    </body>
</html>
