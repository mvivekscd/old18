<%-- 
    Document   : sphome
    Created on : Mar 26, 2017, 9:34:55 PM
    Author     : manju
--%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Add New User Detail</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/base.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/spass.css"/>
        <script src="<%=request.getContextPath()%>/js/basemenu.js"></script>

    </head>
    <body>


        <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="UserPass Add"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>


        <form id="spadd" class="spst" method="POST">
            <table border="0">

                <tbody>
                    <tr>
                        <td>Name: </td>
                        <td><input type="text" name="name" required/></td>
                    </tr>
                    <tr>
                        <td>Username: </td>
                        <td><input type="text" name="username" required/></td>
                    </tr>
                    <tr>
                        <td>first Password:</td>
                        <td><input type="text" name="firstpass" required/></td>
                    </tr>
                    <tr>
                        <td> Second Password:</td>
                        <td><input type="text" name="secondDetail" /></td>
                    </tr>
                    <tr>
                        <td>Url:</td>
                        <td><input type="text" name="url" required/></td>
                    </tr>
                    <tr>
                        <td>PIN:</td>
                        <td><input type="number" name="pin" value="0"/></td>
                    </tr>
                    <tr>
                        <td>AutoLogin:</td>
                        <td>
                            <input type="radio" name="autologin" value="true"  /> Yes<br>
                            <input type="radio" name="autologin" value="false" checked/> No
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:</td>
                        <td><textarea rows="4" name="comments" style="width:100%"></textarea></td>
                    </tr>
                </tbody>
            </table>



            <button type="submit" formaction="<%=request.getContextPath()%>/Spass/addNew" formmethod="post" id="addnew" value="Submit">Submit</button>
                        <button type="button" onclick="location.href = '<%=request.getContextPath()%>/Spass?d='+new Date();">Cancel</button>
        </form>



        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>
