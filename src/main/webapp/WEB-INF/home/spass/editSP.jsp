<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Edit User Detail</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/base.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/spass.css"/>
        <script src="<%=request.getContextPath()%>/js/basemenu.js"></script>

    </head>
    <body>


        <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="UserPass Edit"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>


        <form id="spadd" class="spst" method="POST">
            <table border="0">

                <tbody>
                    <tr>
                        <td>ID: </td>
                        <td><c:out value="${storepassOneRow.spSeqId}"/></td>
                    </tr>
                    <tr>
                        <td>S. No: </td>
                        <td><input type="text" name="spSno" value="<c:out value="${storepassOneRow.spSno}"/>"/></td>
                    </tr>
                    <tr>
                        <td>Name: </td>
                        <td><input type="text" name="name" value="<c:out value="${storepassOneRow.name}"/>"/></td>
                    </tr>
                    <tr>
                        <td>Username: </td>
                        <td><input type="text" name="username" value="<c:out value="${storepassOneRow.username}"/>"/></td>
                    </tr>
                    <tr>
                        <td>first Password:</td>
                        <td><input type="text" name="firstpass" value="<c:out value="${storepassOneRow.firstpass}"/>"/></td>
                    </tr>
                    <tr>
                        <td> Second Password:</td>
                        <td><input type="text" name="secondDetail" value="<c:out value="${storepassOneRow.secondDetail}"/>"/></td>
                    </tr>
                    <tr>
                        <td>Url:</td>
                        <td><input type="text" name="url" value="<c:out value="${storepassOneRow.url}"/>"/></td>
                    </tr>
                    <tr>
                        <td>PIN:</td>
                        <td><input type="number" name="pin" value="<c:out  value="${storepassOneRow.pin}"/>" ></td>
                    </tr>
                    <tr>
                        <td>AutoLogin:</td>
                        <td>
                            <c:choose>
                                <c:when test="${storepassOneRow.autologin == 'true'}">
                                    <input type="radio" name="autologin" value="true" checked /> Yes<br>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" name="autologin" value="true" /> Yes<br>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${storepassOneRow.autologin == 'false'}">
                                    <input type="radio" name="autologin" value="false" checked /> No<br>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" name="autologin" value="false" /> No<br>
                                </c:otherwise>
                            </c:choose>

                        </td>
                    </tr>
                    <tr>
                        <td>Comments:</td>
                        <td><textarea rows="4" name="comments" style="width:100%"><c:out value="${storepassOneRow.comments}"/></textarea></td>
                    </tr>
                </tbody>
            </table>



            <button type="submit" formaction="<%=request.getContextPath()%>/Spass/upDateSp" formmethod="post" id="addnew" value="submit">Update</button>
            <button type="button" onclick="location.href = '<%=request.getContextPath()%>/Spass?d='+new Date();">Cancel</button>
        </form>



        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>

