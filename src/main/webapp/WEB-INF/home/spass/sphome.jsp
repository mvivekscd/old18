<%-- 
    Document   : sphome
    Created on : Mar 26, 2017, 9:34:55 PM
    Author     : manju
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/base.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/java.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/spass.css"/>
        <script src="<%=request.getContextPath()%>/js/basemenu.js"></script>
        <script>
            window.onload = function () {


                var eachrow = document.querySelectorAll(".rowsp");
                for (var i = 0; i < eachrow.length; i++) {
                    eachrow[i].addEventListener('click', function (event) {
                        console.log(this.childNodes[1].innerText);
                        sendPost("<%=request.getContextPath()%>/Spass/View", this.childNodes[1].innerText);
                    });
                }
            }
            function sendPost(url, param) {
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for older browsers
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                       // if (xmlhttp.responseText == "success") {
                            window.location.href = "<%=request.getContextPath()%>/Spass/vi1ewPage" + "?date=" + new Date();
                      //  }
                    }
                };
                xmlhttp.open("POST", url, false);
                console.log(param);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.setRequestHeader("Content-length", param.length);
                xmlhttp.setRequestHeader("Connection", "close");
                xmlhttp.send("imp=" + param);

            }

        </script>
    </head>
    <body>


        <jsp:include page="/WEB-INF/home/common/menu/menu03.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="UserPass Store"/>
            <jsp:param name="enableLogout" value="true"/>
        </jsp:include>
        <div class="spst">

<button type="button" onclick="location.href = 'Spass?d='+new Date();">Refresh</button>
           

            <span>Sort By</span>
            <select>
                <option value="id">id</option>
                <option value="saab">S.No</option>
                <option value="mercedes">username</option>
                <option value="audi">url</option>
            </select>
            <select>
                <option value="id">Asc</option>
                <option value="saab">Dsc</option>

            </select>    

            <button>Sort</button>


            <table class="sph" border="0" width="100%">
                <tr class="headtab"  >
                    <th rowspan="2">ID</th>
                    <th rowspan="2">S.No</th>
                    <th colspan="2"><mark>Name</mark></th>
                    <th>PIN</th>
                    <th>SecondDetail</th>

                    <th>Update TimeStamp</th>
                    <th>updated By</th>
                    <th>autologin</th>


                </tr>
                <tr class="sbc">
                    <th>username</th>
                    <th>firstpass</th>

                    <th>url</th>
                    <th colspan="4"> Comment</th>
                </tr>
                <c:forEach var="splist" items="${storepass}">
                    <tr class="rowsp">
                        <td rowspan="2">
                            <c:out value="${splist.spSeqId}"/>
                        </td>
                        <td rowspan="2">
                            <c:out value="${splist.spSno}"/>
                        </td>
                        <td colspan="2">
                            <mark><c:out value="${splist.name}"/></mark>
                        </td>
                        <td>
                            <c:out value="${splist.pin}"/>
                        </td>

                        <td>
                            <c:out value="${splist.secondDetail}"/>
                        </td>
                        <td>
                            <c:out value="${splist.upts}"/>
                        </td>
                        <td>
                            <c:out value="${splist.upBy}"/>
                        </td>
                        <td>
                            <c:out value="${splist.autologin}"/>
                        </td>
                    </tr>

                    <tr class="rows sbc">
                        <td>
                            <c:out value="${splist.username}"/>
                        </td>
                        <td>
                            <c:out value="${splist.firstpass}"/>
                        </td>

                        <td>
                            <c:out value="${splist.url}"/>
                        </td>
                        <td colspan="4">
                            <c:out value="${splist.comments}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <div>
                Displaying page <c:out value="${sphome.pageno}"/> with <c:out value="${counttest}"/> of <c:out value="${getTotalcount}"/> records
            </div>
            </div>
            <form:form action="Spass1" class="spst" method="post" modelAttribute="sphome">
            
             <div>
                <span>display record per page</span>
                <form:select path="perpage">
                    <form:option value="10">10</form:option>
                    <form:option value="20">20</form:option>
                    <form:option value="30">30</form:option>
                    <form:option value="50">50</form:option>
                </form:select>
            </div>
            <div>
                <span>search By</span>
                <form:select path="searchsel">
                    <form:option value="any">any</form:option>
                    <form:option value="id">id</form:option>
                    <form:option value="sno">S.No</form:option>
                    <form:option value="username">username</form:option>
                    <form:option value="url">url</form:option>
                </form:select>

                <form:input path="searchval" id="searchval" value="" ></form:input>
                <button>Search</button>

            </div>
            <div>
 <form:hidden path="pageno"></form:hidden>
                go page <input type="text" id="pageno1" value="1" /> of 10
                <button>submit</button>
            </div>

            <div>
            <c:if test="${sphome.pageno > 1}"> 
                <button type="submit" name="previous" value="true">previous</button>
                </c:if>
                <c:if test="${sphome.pageno*sphome.perpage < getTotalcount}"> 
                <button type="submit" name="next" value="true">Next</button>
                </c:if>
            </div>

            <div>
                <button>Export to Excel</button>

                <button id="myButton" onClick="window.location.href = 'Spass/addNew';return false;">Add New Record</button>


            </div>
        </form:form>

        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />

    </body>
</html>
