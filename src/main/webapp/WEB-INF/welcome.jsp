<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/WEB-INF/home/common/meta/meta01.jsp" />
        <title>Welcome to mvivekweb</title>
        <link rel="stylesheet" href="css/base.css"/>
        <script src="js/basemenu.js"></script>
    </head>
    <body>
        
        
            <jsp:include page="/WEB-INF/home/common/menu/menu01.jsp" />
        <jsp:include page="/WEB-INF/home/common/header/header01.jsp" >
            <jsp:param name="title" value="MVIVEKWEB.COM"/>
        </jsp:include>
    
            <section id="mainpgsec">
                Welcome to mvivekweb
            </section>
            
        
        <jsp:include page="/WEB-INF/home/common/footer/footer01.jsp" />
    </body>
</html>