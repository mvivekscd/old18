function myFunction() {
    var x = document.getElementsByClassName("flipper");

    //x.style.display = "none";
    for (i = 0; i < x.length; i++)
    {
        x[i].style.display = "none";
    }

    var inputvalue = document.getElementById("quest").value;
    document.getElementById("cou").innerHTML = x.length;
    x[inputvalue - 1].style.display = "";

}

function incr() {

    var inputvalue = 0;
    inputvalue = parseInt(document.getElementById("quest").value);
    var x = document.getElementsByClassName("flipper");
    for (i = 0; i < x.length; i++)
    {
        x[i].classList.remove("vivek");
    }
    if (inputvalue < x.length) {
        document.getElementById("quest").value = inputvalue + 1;
    }

    myFunction();
}

function decr() {

    var inputvalue = 0;
    inputvalue = parseInt(document.getElementById("quest").value);
    var x = document.getElementsByClassName("flipper");
    for (i = 0; i < x.length; i++)
    {
        x[i].classList.remove("vivek");
    }
    if (inputvalue > 1) {
        document.getElementById("quest").value = inputvalue - 1;
    }
    myFunction();
}

function gotop() {
    var x = document.getElementsByClassName("flipper");
    for (i = 0; i < x.length; i++)
    {
        x[i].classList.remove("vivek");
    }
    var inputvalue = 0;
    inputvalue = parseInt(document.getElementById("quest").value);
    if (inputvalue <= x.length) {
        myFunction();
    } else {
        alert('no FlashCard');
    }
}

function clickflip() {
    var x = document.getElementsByClassName("flipper");
    for (i = 0; i < x.length; i++)
    {
        var ele = x[i].style.display;
        if (ele == '') {

            x[i].click();
        }
    }
}

function toggleflip(e) {
    e.classList.toggle('vivek');
}

document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;

    if (e.keyCode == '38') {
        clickflip();
    } else if (e.keyCode == '40') {
        clickflip();
    } else if (e.keyCode == '37') {
        decr();
    } else if (e.keyCode == '39') {
        incr();
    } else if (e.keyCode == '13') {
        clickflip();
    }
}

function disableflash() {
    var element = document.getElementsByClassName("front");
    for (i = 0; i < element.length; i++)
    {
        element[i].classList.add("disfront");
    }
    var element1 = document.getElementsByClassName("back");
    for (i = 0; i < element1.length; i++)
    {
        element1[i].classList.add("disback");
    }
        var element = document.getElementsByClassName("disfront");
    for (i = 0; i < element.length; i++)
    {
        element[i].classList.remove("front");
    }
    var element2 = document.getElementsByClassName("disback");
    for (i = 0; i < element2.length; i++)
    {
         element2[i].classList.remove("back");
    }
    var x = document.getElementsByClassName("flipper");
        for (i = 0; i < x.length; i++)
    {
        x[i].style.display = "";
    }

}

